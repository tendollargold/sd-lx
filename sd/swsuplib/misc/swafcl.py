# swacfl.py  -  Archive-to-Source Name translation lists.
#
#   Copyright (C) 1999  Jim Lowe
#   Copyright (C) 2023-2024 Paul Weber Conversion to Python
#   All rights reserved.
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
import os
import ctypes
from sd.swsuplib.misc.malloc_fprintf import malloc_fprintf
from sd.swsuplib.cplob import cplob_open, cplob_additem
from sd.swsuplib.strob import strob_str
from sd.swsuplib.misc.swlib import swlib_strdup, swlib_return_no_leading, swlib_compare_8859

# use these variables as temp for now
__FILE__ = os.path.realpath(__file__)
__LINE__ = len(__FILE__)
__FUNCTION__ = os.path.basename(__FILE__)

class SwafclEntry:

    def __init__(self):
        #instance fields found by C++ to Python Converter:
        self.from_name_ = '\0'
        self.source_code_ = 0
        self.archiveName = None

class SWACFL:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.entry_array_ = None

class SwafclConstants:
    SWACFL_SRCCODE_ARCHIVE_STREAM = 0
    SWACFL_SRCCODE_FILESYSTEM = 1
    SWACFL_SRCCODE_RPMHEADER = 2
    SWACFL_SRCCODE_PSF = 3

swafcl = SwafclConstants()
def swacfl_open():
    swacfl = malloc_fprintf(ctypes.sizeof(SWACFL), "MALLOC  %18s:%-5d , %s : ", __FILE__, __LINE__, __FUNCTION__)
    if swacfl is None:
        return None
    swacfl.entry_array_ = cplob_open(8)
    temp_ref_null = None
    cplob_additem(swacfl.entry_array_, 0, temp_ref_null)
    return swacfl

def swacfl_close(swacfl):
    for en in swacfl.entry_array_:
        if en:
            free_entry(en)
    swacfl.entry_array_ = None

# void		swacfl_print (SWACFL * swacfl, FILE * fp); 

def swacfl_add(swacfl, archive_name, source_name, type_):
    print(" archive_name=[%s] source_name=[%s]", archive_name.arg_value, source_name.arg_value)
    en = swacfl_make_entry(archive_name, source_name, type_)
    swacfl_add_entry(swacfl, en)


def swacfl_add_entry(swacfl, en):
    swacfl.entry_array_.insert(-1, en)
    swacfl.entry_array_.append(None)


def swacfl_make_entry(archive_name, source_name, type_):
    en = SwafclEntry()
    en.archiveName = archive_name
    en.from_name_ = swlib_strdup(source_name)
    en.source_code_ = type_
    return en

# void		swacfl_set_archive_name(swacfl_entry * entry, char * name); 

def swacfl_set_source_name(en, name):
    if en.from_name_:
        del en.from_name_
    en.from_name_ = swlib_strdup(name.arg_value)


def swacfl_set_type(en, type_):
    en.source_code_ = type_


def swacfl_get_archive_name(en):
    return strob_str(en.archiveName)


def swacfl_get_source_name(en):
    return en.from_name_


def swacfl_get_type(en):
    return en.source_code_


def swacfl_find_entry(swacfl, archive_name):
    archive_name = swlib_return_no_leading(archive_name)
    for en in swacfl.entry_array_:
        if en:
            p = en.archiveName
            p = swlib_return_no_leading(p)
            if swlib_compare_8859(archive_name, p) == 0:
                return en
    return None


def free_entry(en):
    en.archiveName = None
    if en.from_name_:
        en.from_name_ = None


def swacfl_print(swacfl, fp):
    for en in swacfl.entry_array_:
        if en:
            p = en.archiveName
            fp.write(f"{en.source_code_}:")
            if en.from_name_:
                fp.write(f"  {en.from_name_}")
            if p:
                fp.write(f"  {en.archiveName}\n")

