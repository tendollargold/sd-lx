# swicat.py -- installed software catalog routines
#
#   Copyright (C) 2004 Jim Lowe
#   Copyright (C) 2024 Paul Weber conversion to Python
#   All Rights Reserved.
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

import time
import ctypes
import os
import sys

from sd.debug import E_DEBUG2, E_DEBUG
from sd.swcommon import sw_e_msg, sw_l_msg
from sd.swsuplib.cplob import (vplob_open, vplob_close, vplob_add, vplob_val, vplob_delete_store,
                               cplob_val, cplob_add_nta, cplob_close)
from sd.swsuplib.strob import STROB
from sd.swsuplib.versionid import (swvidc, swverid_get_verid, swverid_print, swverid_open, swverid_copy,
                                   swverid_close, swverid_u_parse_swspec, swverid_show_object_debug)
from sd.swsuplib.misc.cstrings import strstr, strcmp, strchr, strrchr, strncmp
from sd.swsuplib.misc.shlib import shlib_get_function_text_by_name
from sd.swsuplib.misc.swextopt import get_opta_isc
from sd.swsuplib.misc.swdef import swdef_write_keyword_to_buffer, swdef_write_value_to_buffer
from sd.swsuplib.swi.swi import swic, SWI_BASE, swi_control_script_posix_result, swi_afile_write_script_cases
from sd.swsuplib.misc.strar import strar_add, strar_get, strar_close, strar_num_elements, strar_open
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.swheader import (swheader_store_state, swheader_set_current_offset, swheader_get_next_attribute,
                                       swheaderline_get_keyword, swheaderline_get_value, swheaderline_get_level,
                                       swheader_restore_state)

from sd.swsuplib.misc.swlib import (swlibc, swlib_strdup, swlib_squash_double_slash, swlib_check_clean_absolute_path, swlib_atoi,
                                    swlib_unix_dircat, swlib_squash_trailing_slash, SWLIB_FATAL, SWLIB_ASSERT, swlib_check_clean_path,
                                    swlib_append_synct_eof)
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.strob import strob_str, strob_open, strob_strtok, strob_sprintf, strob_strcpy, \
    strob_close, STROB_DO_APPEND, strob_chr_index, strob_strcat
from sd.swsuplib.swi.swi import (swi_find_product_by_swsel, swi_package_get_product, swi_find_fileset_by_swsel)

from sd.swsuplib.uxfio import (uxfio_ftruncate, uxfio_get_fd_mem)

class SWICAT_EX_SET:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.ex = None
        self.ex_violated = None


class SWICAT_PRE_SET:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.pre = None
        self.pre_fullfilled = None


class SWICAT_REQ:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.pre_result = 0
        self.ex_result = 0
        self.co_result = 0
        self.failed_pre = None
        self.failed_co = None
        self.passed_ex = None
        self.ex_set_array = None
        self.pre_set_array = None
        self.sl = None


# Array indices for prerequisistes, corequisites, exrequisites

class SwicatConstants:
    SWINSTALL_INCAT_VERSION = "0.8"
    SWINSTALL_INCAT_NAME = "export"
    SWICAT_FORM_DIR1 = "lform_dir1" # bundle product revision vendor_tag
    SWICAT_FORM_B1 = "lform_b1" # bundle product revision vendor_tag
    SWICAT_FORM_P1 = "lform_p1" # product revision vendor_tag
    SWICAT_FORM_DEP1 = "lform_dep1" # contains information on what field matched
    SWICAT_FORM_TAR1 = "lform_tar1" # tar archive
    SWICAT_REQ_TYPE_P = "P" # prerequisite
    SWICAT_REQ_TYPE_C = "C" # corequisite
    SWICAT_REQ_TYPE_E = "E" # exrequisite
    SWICAT_REQ_RESULT_LEN = 3
    SWICAT_SC_STATUS_UNSET = 0
    SWICAT_SC_STATUS_NOT_FOUND = 1
    SWICAT_SC_STATUS_FOUND = 2
    SWICAT_SC_STATUS_DELETE = 3

swicatc = SwicatConstants()

"""
  Array indices for prerequisistes, corequisites, exrequisites 
  Query/Response Format made by swpg_get_catalog_entries()
  Q0:P:swbis,r<0.585.20070324a
  R0:P:...:
  Q1:P:swbis,r==0.585.20070324a
  R1:P:BP.:swbis:swbis                    r=0.585.20070324a       v=GNU   i=0   
  Q2:P:swbis,r>0.585.20070324a
  R2:P:...:

"""

def swicat_pre_set_create():
    pre_set = SWICAT_PRE_SET()
    pre_set.pre = strob_open(12)
    pre_set.pre_fullfilled = strar_open()
    return pre_set



def swicat_ex_set_create():
    ex_set = SWICAT_EX_SET()
    ex_set.ex = strob_open(12)
    ex_set.ex_violated = strar_open()
    return ex_set



def swicat_pre_set_delete(pre_set):
    strob_close(pre_set.pre)
    strar_close(pre_set.pre_fullfilled)



def get_req_type(line):
    s = line
    s = strchr(s, ':')
    if (not s) != '\0':
        return s
    s += 1
    return s



def check_requisite_response(line):
    s = line
    #	 Example	
    #	R1:P:BP.:abiword:abiword                r=1.0.2         v=6
    #		and an empty (non matching) response looks like:
    #	R1:P:...:
    #	
    s = strchr(s, ':')
    if (not s) != '\0':
        return s
    s += 1
    s = strchr(s, ':')
    if (not s) != '\0':
        return s
    s += 1
    s = strchr(s, ':')
    if (not s) != '\0':
        return s
    s += 1
    s = strchr(s, ':')
    if (not s) != '\0':
        return s
    s += 1
    return s



def add_path_code(buf):
    #	 
    # aa="${aa:="$bb"}"
    #	 
   #strob_sprintf(buf, STROB_DO_APPEND, swlibc.CSHID) "export SWPATH\n" + "gpath=$(getconf PATH)\n" + "case \"$gpath\" in\n" + "	\"\")\n" + "		SWPATH=\"$PATH\"\n" + "		;;\n" + "	*)\n" + "		SWPATH=\"${gpath}:${PATH}\"\n" + "		;;\n" + "esac\n")
    pass

def evaluate_query(G, req, spec, text, first_line, err, swicat_sl):
    ret = 0
    sc = None
    pre_set = None
    ex_set = None
    tmp = strob_open(12)
    line = first_line
    s = line
    s += 1
    num = swlib_atoi(s, ret)
    if ret != 0:
        err = 1
        return None

    if swicat_sl:
        sq = swicat_sq_create()
        sc = swicat_sc_create()
        swicat_sq_parse(sq, line)
        swicat_sc_set_sq(sc, sq)
        swicat_sl_add_sc(swicat_sl, sc)

    type = get_req_type(line)

    if type is None:
        err = 1
        return None

    if type[0] == 'P':
        pre_set = swicat_pre_set_create()
        strob_strcpy(pre_set.pre, spec)
        vplob_add(req.pre_set_array, pre_set)
    elif type[0] == 'E':
        ex_set = swicat_ex_set_create()
        strob_strcpy(ex_set.ex, spec)
        vplob_add(req.ex_set_array, ex_set)
    elif type[0] == 'C':
        pass
    else:
        err = 1
        return None

    line = strob_strtok(text, None, "\r\n")
    while line != '\0':
        E_DEBUG2("line=[%s]", line)
        if swicat_sl:
            sr = swicat_sr_create()
            swicat_sr_parse(sr, line)
            swicat_sc_add_sr(sc, sr)
        temp_ref_line2 = (line)
        resp = check_requisite_response(temp_ref_line2)
        line = temp_ref_line2
        # for exrequisites NULL is good,
        # for prerequisites NULL is bad
        E_DEBUG2("resp=[%s]", resp)
        if type[0] == 'P':  # prerequisites
            if resp is None:
                strar_add(req.failed_pre, spec)
                req.pre_result = 1
            else:
                strar_add(pre_set.pre_fullfilled, resp)
        elif type[0] == 'E':  # exrequisites
            if resp is not None:
                strar_add(ex_set.ex_violated, resp)
                req.ex_result = 1
            else:
                strar_add(req.passed_ex, spec)
        elif type[0] == 'C':  # corequisites
            if resp is None:
                strar_add(req.failed_co, spec)
                req.co_result = 1

        line = strob_strtok(text, None, "\r\n")
        if line != '\0' and swlib_atoi(line[1:], ret) == num and ret == 0:
            continue
        else:
            break
    strob_close(tmp)
    return line


def shpat_to_regex(buf, shell_pattern):
    i = 0
    strob_strcpy(buf, "")
    s = shell_pattern
    strob_chr_index(buf, i, ord('^'))
    i += 1
    while s != '\0':
        if s == '?':
            strob_chr_index(buf, i, ord('.'))
            i += 1
        elif s == '+':
            strob_chr_index(buf, i, ord('\\'))
            i += 1
            strob_chr_index(buf, i, ord('+'))
            i += 1
        elif s == '*':
            strob_chr_index(buf, i, ord('.'))
            i += 1
            strob_chr_index(buf, i, ord('*'))
            i += 1
        else:
            strob_chr_index(buf, i, ord(s))
            i += 1
        s += 1
    strob_chr_index(buf, i, ord('$'))
    i += 1


def write_query_blob(G, swverid, buf, tmp, query_num, req_type):
    #	
    #	 * A query blob is a block of shell variable assignments
    #	 * similar to the example in ../shell_lib/test_isc_03.sh
    #
    #		echo SWBIS_SOC_SPEC1="\"swbis\""
    #		echo SWBIS_SOC_SPEC2="\".*\""
    #		echo SWBIS_VENDOR_TAG="\".*\""
    #		echo SWBIS_REVISION_SPEC="\"0.600\""
    #		echo SWBIS_REVISION_RELOP="\">\""
    #		echo SWBIS_QUALIFIER_SPEC="\".*\""
    #		echo SWBIS_LOCATION_SPEC="\".*\""
    #		echo SWBIS_INSTANCE=0
    #	 
    i = 0
    s = '\0'
    awkregex = '\0'
    tmpval = '\0'
    verid = None

    strob_sprintf(buf, 1, "SW_SOC_SPEC=\"%s\"\n", swverid.source_copyM)

    strob_sprintf(buf, 1, "SWBIS_UTIL_NAME=\"%s\"\n" + "SWBIS_CL_TARGET_TARGET=\"%s\"\n", swlib_utilname_get(),
                  (G.g_cl_target_targetM if G.g_cl_target_targetM is not None else "(unset)"))

    # set the defaults to include everything 
    strob_sprintf(buf, 1, "SWBIS_SOC_SPEC1=\".*\"\n")
    strob_sprintf(buf, 1, "SWBIS_SOC_SPEC2=\".*\"\n")

    i = 0
    while s := cplob_val(swverid.taglist, i):
        if i < 2:
            strob_strcpy(tmp, s)
            #			 Now translate the shell pattern matching chars
            #			   to awk regex patterns. 
            temp_ref_s = s
            shpat_to_regex(tmp, temp_ref_s)
            s = temp_ref_s
            awkregex = strob_str(tmp)
            strob_sprintf(buf, 1, "SWBIS_SOC_SPEC%d=\"%s\"\n", i + 1, awkregex)
        else:
            # If the swspec is bundle.prod.fileset
            # i.e. more the 2, then just use the first two
            # because the third has to be a fileset or subproduct
            # and these are not matched by method used here. 
            pass
        i += 1

    # Now set the vendor tag 

    verid = swverid_get_verid(swverid, swvidc.SWVERID_VERIDS_VENDOR_TAG, 1)
    if verid is None:
        strob_sprintf(buf, 1, "SWBIS_VENDOR_TAG=\".*\"\n")
    else:
        tmpval = verid.valueM
        temp_ref_tmpval = (tmpval)
        shpat_to_regex(tmp, temp_ref_tmpval)
        tmpval = temp_ref_tmpval
        strob_sprintf(buf, 1, "SWBIS_VENDOR_TAG=\"%s\"\n", strob_str(tmp))

    if req_type:
        strob_sprintf(buf, 1, "SWBIS_REQ_TYPE=\"%s\"\n", req_type)

    if query_num >= 0:
        strob_sprintf(buf, 1, "SWBIS_QUERY_NUM=\"%d\"\n", query_num)

    # Now set the revsion and revision relop 

    verid = swverid_get_verid(swverid, swvidc.SWVERID_VERIDS_REVISION, 1)
    if verid is None:
        strob_sprintf(buf, 1, "SWBIS_REVISION_SPEC=\".*\"\n")
    else:
        tmpval = verid.valueM
        if strcmp(tmpval, "*") == 0:
            strob_sprintf(buf, 1, "SWBIS_REVISION_SPEC=\".*\"\n")
        else:
            strob_strcpy(tmp, tmpval)
            strob_sprintf(buf, 1, "SWBIS_REVISION_SPEC=\"%s\"\n", strob_str(tmp))
        strob_sprintf(buf, 1, "SWBIS_REVISION_RELOP=\"%s\"\n", verid.rel_opM)

    # Now set the second revision and revision relop 
    verid = swverid_get_verid(swverid, swvidc.SWVERID_VERIDS_REVISION, 2)
    if verid is None:
        pass
    else:
        # A second revision such as in a bracketed range 
        tmpval = verid.valueM
        if strcmp(tmpval, "*") == 0:
            strob_sprintf(buf, 1, "SWBIS_REVISION2_SPEC=\".*\"\n")
        else:
            strob_strcpy(tmp, tmpval)
            strob_sprintf(buf, 1, "SWBIS_REVISION2_SPEC=\"%s\"\n", strob_str(tmp))
        strob_sprintf(buf, 1, "SWBIS_REVISION2_RELOP=\"%s\"\n", verid.rel_opM)

    verid = swverid_get_verid(swverid, swvidc.SWVERID_VERIDS_CATALOG_INSTANCE, 1)
    if verid is None:
        tmpval = "*"
    else:
        tmpval = verid.valueM
    temp_ref_tmpval2 = (tmpval)
    shpat_to_regex(tmp, temp_ref_tmpval2)
    tmpval = temp_ref_tmpval2
    strob_sprintf(buf, 1, "SWBIS_INSTANCE=\"%s\"\n", strob_str(tmp))

    verid = swverid_get_verid(swverid, swvidc.SWVERID_VERIDS_LOCATION, 1)
    if verid is None:
        tmpval = "*"
    else:
        tmpval = verid.valueM
    if strcmp(tmpval, "/") == 0 or len(tmpval) == 0:
        strob_sprintf(buf, 1, "SWBIS_LOCATION_SPEC=\"^/$|^$\"\n")
    else:
        temp_ref_tmpval3 = (tmpval)
        shpat_to_regex(tmp, temp_ref_tmpval3)
        tmpval = temp_ref_tmpval3
        strob_sprintf(buf, 1, "SWBIS_LOCATION_SPEC=\"%s\"\n", strob_str(tmp))

    verid = swverid_get_verid(swverid, swvidc.SWVERID_VERIDS_QUALIFIER, 1)
    if verid is None:
        strob_sprintf(buf, 1, "SWBIS_QUALIFIER_SPEC=\".*\"\n")
    else:
        tmpval = verid.valueM
        temp_ref_tmpval4 = (tmpval)
        shpat_to_regex(tmp, temp_ref_tmpval4)
        tmpval = temp_ref_tmpval4
        strob_sprintf(buf, 1, "SWBIS_QUALIFIER_SPEC=\"%s\"\n", strob_str(tmp))

    return 0


def write_shell_assign1(buf, varname, value):
    #	 
    #	 * aa="${aa:="$bb"}"
    #	 
    strob_sprintf(buf, STROB_DO_APPEND, "%s=\"${%s:=\"%s\"}\"\n", varname, varname,
                  value)


def write_swbis_env_vars(buf, swi):
    return


def form_absolute_control_dir(tmp, swi, control_script_pkg_dir, script_name):
    s = '\0'

    strob_strcpy(tmp, swi.swi_pkg.target_path)
    swlib_unix_dircat(tmp, swi.swi_pkg.catalog_entry)
    swlib_unix_dircat(tmp, swvidc.SWINSTALL_INCAT_NAME)
    swlib_unix_dircat(tmp, control_script_pkg_dir)

    #	
    # Now make some sanity checks
    #	 
    swlib_squash_trailing_slash(strob_str(tmp))
    swlib_squash_double_slash(strob_str(tmp))
    if swlib_check_clean_absolute_path(strob_str(tmp)):
        SWLIB_FATAL("internal error: form_absolute_control_dir-1")

    #	
    # Now take the dirname of this.
    #	 
    s = strrchr(strob_str(tmp), '/')
    if s != '\0':
        s = '\0'
        s += 1
        script_name[0] = s
    else:
        SWLIB_FATAL("internal error: form_absolute_control_dir-2")
    return


def write_state(buf, xx, do_if_active):
    if do_if_active and not xx.base.is_active:
        return
    if xx.state != "SW_STATE_UNSET":
        buf.append(f"    {swc.SW_A_state} {xx.state}\n")
    return


def write_times(buf, xx, do_if_active):
    #	if (do_if_active && !(xx->is_activeM)) return
    #	
    caltime = time.localtime(xx.create_time)
    buf.append("    " + "SW_A_create_time" + " " + str(xx.create_time) + " # " + time.ctime(caltime) + "\n")
    caltime = time.localtime(xx.mod_time)
    buf.append("    " + "SW_A_mod_time" + " " + str(xx.mod_time) + " # " + time.ctime(caltime) + "\n")


def add_env_entry_localdefault(name, value, buf):
    if value:
        s = value
    else:
        s = os.getenv(name)
    if (not s) != '\0':
        s = ""
    write_shell_assign1(buf, name, s)


def add_env_entry(name, value, buf):
    s = value if value else os.getenv(name)
    if s:
        buf.str_ += f"{name}=\"{s}\"\n"
        buf.str_ += f"export {name}\n"

def write_dependency_assertion_script(swi, dep_spec, buf):
    return -1


def squash_null_block(start, length):
    if length <= 0:
        return 0
    first_null = start + len(start)
    s = first_null
    nn = 0
    while (s - start) < length and s == '\0':
        nn += 1
        s += 1
    done = int((s - start))
    if done >= length:
        # finished, the whole file is NUL clean 
        return 0
    ctypes.memmove(first_null, s, (start + length) - s)
    ctypes.memset(start + (length - nn), 0, nn)
    return nn


def write_attributes(buf, swheader, index):
    line = '\0'

    swheader_store_state(swheader, None)
    swheader_set_current_offset(swheader, index)
    while line == swheader_get_next_attribute(swheader):
        keyword = swheaderline_get_keyword(line)
        value = swheaderline_get_value(line, None)
        level = swheaderline_get_level(line)
        swdef_write_keyword_to_buffer(buf, keyword, level, swlibc.SWPARSE_MD_TYPE_ATT)
        swdef_write_value_to_buffer(buf, value)
    swheader_restore_state(swheader, None)


def write_isf_excludes(ibuf, swi):
    fd = swi.pending_file_conflicts_fd
    if fd < 0:
        return
    buf = strob_open(32)
    os.lseek(fd, 0, os.SEEK_SET)
    while True:
        line = os.read(fd, buf)
        if not line:
            break
        s = line.decode().strip()
        while s.endswith('\n'):
            s = s[:-1]
        ibuf.str_ += swlibc.SW_A_excluded_from_install + ' "' + s + '"\n'

def swicat_write_installed_software(swi, ofd):
    """
      Write the installed software file

      Example:

        installed_software
        path /
        catalog var/spool/sw/catalog

        product
        create_time 1059628779
        mod_time 1059629009
        all_filesets BIN DOC
        filesets BIN DOC
        create_time 1059628779
        mod_time 1059629009
        tag foo

        control_file
        tag postinstall
        result 0

        fileset
        create_time 1059628779
        mod_time 1059629009
        tag BIN
        state installed

        fileset
        tag DOC
        create_time 1059628779
        mod_time 1059629009
        state installed
    """
    buf = strob_open(10)
    swicat_isf_installed_software(buf, swi)
    # ret = atomicio(write, ofd, strob_str(buf), strob_strlen(buf))
    ret = os.write(ofd, buf.str_)
    strob_close(buf)
    return ret


def swicat_isf_all_scripts(buf, xx, do_if_active):
    i = 0
    while i < swic.SWI_MAX_OBJ and xx.swi_co[i]:
        swicat_isf_control_script(buf, xx.swi_co[i], do_if_active)
        i += 1
    return 0

    
def swicat_isf_control_script(buf, xx, do_if_active):
    buf.tok_ += "SW_A_control_file\n"
    buf.tok_ += "\t" + "SW_A_tag" + " " + xx.base.b_tag + "\n"
    buf.tok_ += "\t" + "SW_A_result" + " " + swi_control_script_posix_result(xx) + "\n"
    if xx.result >= 0:
        buf.tok_ += "\t" + "SW_A_return_code" + " " + str(xx.result) + "\n"
    return 0

def SWI_GET_PRODUCT(W, N):
    return swi_package_get_product(W.swi_pkg, N)

def SWI_INDEX_HEADER(W):
    return W.swi_pkg.base.global_header


def swicat_isf_fileset(swi, buf, xfile, do_if_active):
    buf.tok_ += "SW_A_fileset\n"
    buf.tok_ += "    " + "SW_A_location" + " " + (swi.swi_pkg.location if swi.swi_pkg.location else "/") + "\n"
    write_attributes(buf, SWI_INDEX_HEADER(swi), xfile.base.header_index)
    write_state(buf, xfile, do_if_active)
    write_times(buf, SWI_BASE(), 1)
    swicat_isf_all_scripts(buf, xfile.swi_sc, do_if_active)
    write_isf_excludes(buf, swi)
    buf.tok_ += "\n"
    return 0


def swicat_isf_product(swi, buf, prod, do_if_active):
    i = 0
    swheader = SWI_INDEX_HEADER(swi)
    strob_sprintf(buf, STROB_DO_APPEND, swlibc.SW_A_product + "\n")
    strob_sprintf(buf, STROB_DO_APPEND, swlibc.SW_A_location + " %s\n" % (swi.swi_pkg.location if swi.swi_pkg.location else "/"))
    write_attributes(buf, SWI_INDEX_HEADER(swi), prod.p_base.header_index)
    write_times(buf, prod.p_base, 1)
    swicat_isf_all_scripts(buf, prod.xfile.swi_sc, do_if_active)
    while i < swic.SWI_MAX_OBJ and prod.swi_co[i]:
        swicat_isf_fileset(swi, buf, prod.swi_co[i], do_if_active)
        i += 1
    return 0


def swicat_isf_installed_software(buf, swi):
    prod = swi.swi_pkg.swi_co[0]
    buf.str_ = "SW_A_installed_software\n"
    swicat_isf_product(swi, buf, prod, 1)
    if swi.swi_pkg.swi_co[1]:
        print("multiple products not supported")
        return 1
    return 0


def swicat_env(buf, swi, control_script_pkg_dir, tag):
    tmp = strob_open(10)
    tmp_s = '\0'

    add_env_entry("LC_CTYPE", "", buf)
    add_env_entry("LC_MESSAGES", "", buf)
    add_env_entry("LC_TIME", "", buf)
    add_env_entry("TZ", "", buf)
    add_env_entry("LANG", "C", buf)
    add_env_entry("LC_ALL", "C", buf)

    if swi.swi_pkg.target_path:
        add_env_entry("SW_ROOT_DIRECTORY", swi.swi_pkg.target_path, buf)
    else:
        sys.stderr.write("warning: swi->swi_pkg->target_path is null, using /")
        add_env_entry("SW_ROOT_DIRECTORY", "/", buf)

    add_env_entry("SW_PATH", "$PATH", buf)

    #	
    # SWBIS_CATALOG_ENTRY is what is after the <installed_software_catalog> path
    # fragment
    #	 

    SWLIB_ASSERT(len(swi.swi_pkg.installed_software_catalog))
    tmp_s = strstr(swi.swi_pkg.catalog_entry, swi.swi_pkg.installed_software_catalog)

    SWLIB_ASSERT(tmp_s is not None)
    tmp_s += len(swi.swi_pkg.installed_software_catalog)
    while tmp_s[0] == '/':
        tmp_s += 1

    add_env_entry("SWBIS_CATALOG_ENTRY", tmp_s, buf)

    #	
    # Wrong.. SW_CATALOG same as installed_software.catalog and is a
    # Wrong.. a relative path (It is not the installed_software_catalog option value)
    #
    #	 Actually, SW_CATALOG is really the path such as, for example:
    #
    #		 var/adm/sw/catalog/foo/foo/1.1/0/
    #
    #	   prior to version 1.12, the value of SW_CATALOG was set by the following line:
    #		add_env_entry("SW_CATALOG", swi->swi_pkg->installed_software_catalog, buf)
    #	


    add_env_entry("SW_CATALOG", swi.swi_pkg.catalog_entry, buf)

    # SWLIB_ASSERT(swi.swi_pkg.location is not None)
    assert swi.swi_pkg.location is not None
    add_env_entry("SW_LOCATION", swi.swi_pkg.location, buf)
    add_path_code(buf)

    if swi.swi_pkg.installed_software_catalog == '/':
        # The installed_software_catalog was specified as
        # an absolute path
        add_env_entry("SW_SESSION_OPTIONS", "${SW_CATALOG}/"+ swlibc.SW_A_session_options , buf)
    else:
        add_env_entry("SW_SESSION_OPTIONS", "${SW_ROOT_DIRECTORY}/${SW_CATALOG}/"+ swlibc.SW_A_session_options , buf)

        """
          add SW_SOFTWARE_SPEC 

          FIXME, don't assume first product 
          FIXME , add tag for fileset when writing the fileset 

          This is a provisional value for  SW_SOFTWARE_SPEC at the top
          of the file.  It is defined again before each script invocation
          so as to reflect the tag of the fileset 
        """
        strob_strcpy(tmp, "")
        swverid_print(SWI_GET_PRODUCT(swi, 0).p_base.swverid, tmp)
        add_env_entry("SW_SOFTWARE_SPEC", strob_str(tmp), buf)
        strob_strcat(buf, ". \"${SW_SESSION_OPTIONS}\"\n")
        return 0


def swicat_make_options_file(buf):
    strob_strcpy(buf, "")
    return 0


def swicat_write_auto_comment(buf, filename):
    caltime = time.time()
    buf.str_ += f"# {filename}\n"
    buf.str_ += f"# Automatically generated by {swlib_utilname_get()} version {swlibc.SW_RELEASE} on {time.ctime(caltime)}#\n"

def swicat_construct_controlsh_taglist(swi, sw_selections, list):
    i = 0
    j = 0
    package = swi.swi_pkg
    tmp = strob_open(100)
    strob_strcpy(list, "")
    while i < swic.SWI_MAX_OBJ:
        product = package.swi_co[i]
        if product:
            assert product.p_base.b_tag is not None
            strob_strcpy(tmp, product.p_base.b_tag)
            if swlib_check_clean_path(product.p_base.b_tag):
                assert False
            strob_sprintf(list, STROB_DO_APPEND, "%s", strob_str(tmp))
            while j < swic.SWI_MAX_OBJ:
                fileset = product.swi_co[j]
                if fileset:
                    assert fileset.base.b_tag is not None
                    if swlib_check_clean_path(fileset.base.b_tag):
                        assert False
                    strob_strcpy(tmp, product.p_base.b_tag)
                    strob_strcat(tmp, ".")
                    strob_strcat(tmp, fileset.base.b_tag)
                    strob_sprintf(list, STROB_DO_APPEND, " %s", strob_str(tmp))
                else:
                    break
                j += 1
        else:
            break
        strob_sprintf(list, STROB_DO_APPEND, " ")
        i += 1
    strob_close(tmp)


def swicat_write_script_cases(swi, buf, sw_selection):

    tmp = strob_open(32)
    tmp.str_ = [0] * 32
    """
    # the first step is to find the object implied by
    # sw_selection.  sw_selection has the form:
    #   <prod_tag>.<fileset_tag>
    """

    E_DEBUG2("sw_selection=[%s]", sw_selection)
    # E_DEBUG2("[%s]", swi_dump_string_s(swi, "AAAAA"));

    swverid = swverid_open(swlibc.SW_A_product, None)
    taglist = swverid_u_parse_swspec(swverid, sw_selection)
    E_DEBUG2("swverid_print = [%s]", swverid_show_object_debug(swverid, None, ""))

    """	 NOT
      do a sanity check,
      only 2 dot delimited fields are allowed.

      if (cplob_get_nused(taglist) > 3) {
     SWLIB_ASSERT(0)
       }
    """

    product_tag = cplob_val(taglist, 0)
    fileset_tag = cplob_val(taglist, 1)  # may be NUL
    """
      FIXME
      Assumes one fileset
    """

    product = swi_find_product_by_swsel(swi.swi_pkg, product_tag, None, None)
    assert product is not None  # fatal

    if fileset_tag != '\0':
        xfile = swi_find_fileset_by_swsel(product, fileset_tag, None)
    else:
        #
        # the scripts reside in the pfiles object
        #
        xfile = product.xfile
    assert xfile is not None  # fatal
    scripts = xfile.swi_sc
    assert scripts is not None  # fatal

    # make a copy of the swverid to avoid harming the original

    swverid_spec = swverid_copy(SWI_GET_PRODUCT(swi, 0).p_base.swverid)

    if fileset_tag != '\0':
        # add fileset_tag to the '.' delimited list
        cplob_add_nta(swverid_spec.taglist, fileset_tag)
    swverid_print(swverid_spec, tmp)

    #
    # OK, we now have the scripts for the product or fileset
    # Loop over them and write the shell code fragment for each
    # POSIX script.
    #
    buf.str_ = "\t#\n" \
               "\t# write script cases here\n" \
               "\t#\n"
    add_env_entry("SW_SOFTWARE_SPEC", strob_str(tmp), buf)
    ret = swi_afile_write_script_cases(scripts, buf, swi.swi_pkg.installed_software_catalog)
    assert ret == 0
    swverid_close(swverid)
    swverid_close(swverid_spec)
    cplob_close(taglist)
    strob_close(tmp)
    return 0


def swicat_r_get_installed_catalog(G, swspecs, target_path):

    # the main target script should now be waiting at a "bash -s" command.
    # We need to write a task script that gets the portion of installed
    # software catalog

    return -1

#
# Write the task script for getting a list of catalog_entries or
# a tar archive of those entries based on the software selections
# in swspecs.

#  Return:
#  0: success, script_buf has script
#    -1: error
#


def swicat_write_isc_script(script_buf, G, swspecs, swspecs_pre, swspecs_co, swspecs_ex, list_vform):
    tmp = STROB()
    shell_lib_buf = STROB()
    tmp.str_ = ""
    shell_lib_buf.str_ = ""
    script_buf.str_ = ""
    isc_path = get_opta_isc(G.opta, swlibc.SW_E_installed_software_catalog)

    # Create a tar archive of the selections

    # lie, set the form so that a list of directories will be
    # printed to be used by a archive creation function as the list of
    # directories to archive
    if list_vform == swicatc.SWICAT_FORM_TAR1:
        list_lform = swicatc.SWICAT_FORM_DIR1
    else:
        # Use the form specified by the argument 
        list_lform = list_vform

    script_buf.str_ += swlibc.CSHID
    script_buf.str_ += "export sw_retval\n"
    script_buf.str_ += "export SW_SOC_SPEC\n"
    script_buf.str_ += "export SW_SOC_SPEC1\n"
    script_buf.str_ += "export SW_SOC_SPEC2\n"
    script_buf.str_ += "export SW_VENDOR_TAG\n"
    script_buf.str_ += "export SW_REVISION_SPEC\n"
    script_buf.str_ += "export SW_REVISION_RELOP\n"
    script_buf.str_ += "export SW_REVISION2_SPEC\n"
    script_buf.str_ += "export SW_REVISION2_RELOP\n"
    script_buf.str_ += "export SW_LOCATION_SPEC\n"
    script_buf.str_ += "export SW_QUALIFIER_SPEC\n"
    script_buf.str_ += "export SW_INSTANCE\n"
    script_buf.str_ += "export SW_QUERY_NUM\n"
    script_buf.str_ += "export SW_LIST_FORM\n"
    script_buf.str_ += "export SW_REQ_TYPE\n"
    script_buf.str_ += "export SW_UTIL_NAME\n"
    script_buf.str_ += "export SW_CL_TARGET_TARGET\n"
    script_buf.str_ += shlib_get_function_text_by_name("shls_false_", tmp, None)
    script_buf.str_ += shlib_get_function_text_by_name("shls_apply_socspec", tmp, None)
    script_buf.str_ += shlib_get_function_text_by_name("shls_get_verid_list", tmp, None)

    # put shell functions here needed for writing the tar format here
    if list_vform == swicatc.SWICAT_FORM_TAR1:
        script_buf.str_ += shlib_get_function_text_by_name("shls_false_", shell_lib_buf, None)
        script_buf.str_ += shlib_get_function_text_by_name("shls_check_for_gnu_tar", shell_lib_buf, None)
        script_buf.str_ += shlib_get_function_text_by_name("shls_missing_which", shell_lib_buf, None)
        script_buf.str_ += shlib_get_function_text_by_name("shls_write_cat_ar", shell_lib_buf, None)
        #	 Now loop over swspecs and make a query blob
        #	   for each swspec.  A query blob is a block of
        #	   shell variables that form the interface to the
        #	   selection routines from shell_lib/shell_lib.sh 

        # Beginning subshell 

        if isc_path:
            script_buf.str_ += "cd \"" + isc_path + "\"\n"
        else:
            script_buf.str_ += "(exit 1);\n"

        script_buf.str_ += "case $? in\n"
        script_buf.str_ += "0)\n"

        E_DEBUG("")
        script_buf.str_ += "sw_retval=0\n"
        script_buf.str_ += "(\n"

        # Set the LIST_FORM variable. This set the output form 
        script_buf.str_ += "SW_LIST_FORM=\"" + list_lform + "\"\n"

        #	 Following line reads the block of NULs for this task
        #	 the block of NULs is sent is that some dd's can't successfully
        #	 read 0 blocks, hence all task scripts read atleast 1 block,
        #	 even task scripts that require no input.

        script_buf.str_ += "dd bs=512 count=1 of=/dev/null 2>/dev/null\n"

        i = 0
        script_buf.str_ += "(\n"  # subshell

        if swspecs:
            # write listing or query blob 
            E_DEBUG("")
            if len(swspecs.list) <= 0:
                # add a "*" spec 
                E_DEBUG("")
                swverid = swverid_open(None, "*")
                if swverid is None:
                    return -1
                else:
                    swspecs.list.append(swverid)

            E_DEBUG("Looping over swspecs")
            while i < len(swspecs.list):
                swverid = swspecs.list[i]
                i += 1
                E_DEBUG2("in swpec number [%d]", i)
                write_query_blob(G, swverid, script_buf, tmp, -1, None)

                # Here is the command that reads the shell variables 
                script_buf.str_ += "shls_get_verid_list | shls_apply_socspec\n\n"
                i += 1

        elif swspecs_pre or swspecs_co or swspecs_ex:
            # write requisite checks blobs 
            E_DEBUG("")
            query_num = 0
            i = 0
            if swspecs_pre:
                E_DEBUG("have swspecs_pre")
                while i < len(swspecs_pre.list):
                    swverid = swspecs_pre.list[i]
                    write_query_blob(G, swverid, script_buf, tmp, query_num, swicatc.SWICAT_REQ_TYPE_P)
                    script_buf.str_ += "shls_get_verid_list | shls_apply_socspec\n\n"
                    query_num += 1
                    i += 1
            i = 0
            if swspecs_co:
                E_DEBUG("have swspecs_co")
                while i < len(swspecs_co.list):
                    swverid = swspecs_co.list[i]
                    write_query_blob(G, swverid, script_buf, tmp, query_num, swicatc.SWICAT_REQ_TYPE_C)
                    script_buf.str_ += "shls_get_verid_list | shls_apply_socspec\n\n"
                    query_num += 1
                    i += 1
            i = 0
            if swspecs_ex:
                E_DEBUG("have swspecs_ex")
                while i < len(swspecs_ex.list):
                    swverid = swspecs_ex.list[i]
                    write_query_blob(G, swverid, script_buf, tmp, query_num, swicatc.SWICAT_REQ_TYPE_E)
                    script_buf.str_ += "shls_get_verid_list | shls_apply_socspec\n\n"
                    query_num += 1
                    i += 1
            if query_num == 0:
                E_DEBUG("have no blobs whatsoever")
                """
                 			 here NO query blobs were written, which means we owe the script syntax
                 				a command (symmetric with 
                 					shls_get_verid_list | shls_apply_socspec
                 
                				Use dd if=/dev/null 2>/dev/null
                """
                script_buf.str_ += "dd if=/dev/null 2>/dev/null\n"
        else:
            # This should never happen 
            sw_e_msg(G, "internal error in swicat_write_isc_script()\n")
            E_DEBUG("")
            return

        #	 echo TRAILER!!! as a terminator so that a reading process on the
        #	   management host knows when to stop reading. 

        E_DEBUG("")
        if list_vform == swicatc.SWICAT_FORM_TAR1:
            script_buf.str_ += ")\n"
            script_buf.str_ += ") | shls_write_cat_ar\n"
        else:
            swlib_append_synct_eof(script_buf)
        script_buf.str_ += ";;\n"
        script_buf.str_ += "*)\n"
        script_buf.str_ += "echo \"internal error: bad installed_software_catalog path in swicat_write_isc_script\"\n"
        script_buf.str_ += "sw_retval=1\n"
        script_buf.str_ += ";;\n"
        script_buf.str_ += "esac\n"
        strob_close(tmp)
        strob_close(shell_lib_buf)
        return 0


def swicat_squash_null_bytes(fd):
    # squash interjected NULs caused by dd used with
    # the sync conversion such as dd obs=512 conv=sync

    current_pos = os.lseek(fd, 0, os.SEEK_CUR)
    assert current_pos >= 0
    length = os.lseek(fd, 0, os.SEEK_END)
    assert length >= 0
    mem = uxfio_get_fd_mem(fd, 0,None)
    assert mem is not None
    nn = 0
    newlen = length
    while (nn == squash_null_block(mem, newlen)) > 0:
        newlen -= nn
    if nn < 0:
        return -1
    ret = os.lseek(fd, current_pos, os.SEEK_SET)
    assert ret >= 0
    ret = uxfio_ftruncate(fd, 0, len(mem))
    assert ret >= 0
    return 0


def swicat_req_get_pre_result(req):
    return req.pre_result


def swicat_req_get_ex_result(req):
    return req.ex_result


def swicat_req_print(G, req):
    i = 0
    s = None
    if req.pre_result != 0:
        # failed dependencies 
        while s == strar_get(req.failed_pre, i):
            sw_l_msg(G, swlibc.SWC_VERBOSE_1, "prerequisite failed: %s\n", s)
            i += 1
    else:
        # fulfilled dependencies
        i = 0
        pre_set = swicat_pre_set_create()
        pre = ""
        while pre_set == vplob_val(req.pre_set_array, i):
            nn = 0
            while pre == strar_get(pre_set.pre_fullfilled, nn):
                nn += 1
                sw_l_msg(G, swlibc.SWC_VERBOSE_1, "prerequisite '%s' filled by: %s:\n",
                         strob_str(pre_set.pre), pre)
                nn += 1
            i += 1
    if req.ex_result != 0:
        # failed ex-requisites
        ex_set = swicat_ex_set_create()
        ex = '\0'
        i = 0
        while ex_set == vplob_val(req.ex_set_array, i):
            nn = 0
            while ex == strar_get(ex_set.ex_violated, nn):
                sw_l_msg(G, swlibc.SWC_VERBOSE_1, "ex-requisite '%s' failed: %s:\n",
                         strob_str(ex_set.ex), ex)
                nn += 1
            i += 1
    else:
        # succeeded ex-requisites
        i = 0
        while s == strar_get(req.passed_ex, i):
            sw_l_msg(G, swlibc.SWC_VERBOSE_3, "ex-rerequisite passed: %s\n", s)
            i += 1

    return 0


def swicat_req_analyze(G, req, query_text, swicat_sl_p):
    #	
    #	Form of result format: colon delimited line of ascii text
    #
    #	Field 1:  {Q|R}N   Query or Response with Id Number
    #	Field 2:  {P|C|E}  Prerequisite Corequisite or Exrequisite
    #	Field 3:  {B|.}{P|.}{F|.}   Which object matched, Bundle, Product or Fileset
    #	Field 4:  Bundle Tag
    #	Field 5:  Entry 
    #
    #	Example of result format:
    #	
    #	Q0:P:sddf*,r>3
    #	R0:P:...:
    #	Q1:P:ab*,r>=1.0
    #	R1:P:BP.:abiword:abiword                r=1.0.2         v=6
    #	


    E_DEBUG2("query text=[%s]", query_text)

    text = strob_open(10)
    if swicat_sl_p:
        swicat_sl_p[0] = None
        swicat_sl = swicat_sl_create()
    else:
        swicat_sl = None
    err = 0
    line = strob_strtok(text, query_text, "\r\n")
    while line:
        if line[0] != 'Q':
            # internal error 
            return -1
        #		 evaluate_query() returns the first line of the
        #		   next query 
        spec = line
        spec = strchr(spec, ':')
        if spec is None:
            return -1
        spec = strchr(++spec, ':')
        if spec is None:
            return -1
        spec += 1
        # spec now points to the software_spec 

        line = evaluate_query(G, req, spec, text, line, err, swicat_sl)

    strob_close(text)
    if err != 0:
        if swicat_sl:
            swicat_sl_delete(swicat_sl)
        if swicat_sl_p:
            swicat_sl_p[0] = None
        return 1
    if swicat_sl_p:
        swicat_sl_p[0].copy_from(swicat_sl)
    return 0


def swicat_req_create():
    req = SWICAT_REQ()
    req.failed_pre = strar_open()
    req.failed_co = strar_open()
    req.passed_ex = strar_open()
    req.pre_result = 0
    req.ex_result = 0
    req.co_result = 0
    req.ex_set_array = vplob_open()
    req.pre_set_array = vplob_open()
    req.sl = None
    return req


def swicat_req_delete(req):
    strar_close(req.failed_pre)
    strar_close(req.failed_co)
    strar_close(req.passed_ex)
    vplob_close(req.ex_set_array)
    vplob_close(req.pre_set_array)
    req = None

"""
  swicat_s -- catalog query parsing routines
 
  Copyright (C) 2007 Jim Lowe
  Copyright (C) 2024 Paul Weber conversion to Python
  All Rights Reserved.
  
  This file contains routines to parse the blob of text output by the
  following command:
 
  swlist -x verbose=3 --deps --prereq swbis
 
  which, depending on the contents of your catalog results in something
  like this (This output is made by the awk code in shell_lib.sh):

"""
#  Query/Response Format made by swpg_get_catalog_entries()
#Q0:P:swbis,r<0.585.20070324a
#R0:P:...:
#R1:P:BP.:swbis:swbis                    r=0.585.20070324a       v=GNU   i=0   
#Q2:P:swbis,r>0.585.20070324a
#R2:P:...:
#


class SWICAT_SQ:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.line = '\0'
        self.number = 0
        self.level = [0] * 2
        self.swspec_string = None
        self.swspec = None

    #	 Example:
    #		Q1:P:swbis,r==0.585.20070324a
    #	

class SWICAT_SE:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.entry = '\0'
        self.tag = '\0'
        self.revision = '\0'
        self.vendor_tag = '\0'
        self.location = '\0'
        self.sequence = '\0'
        self.qualifier = '\0'

    #	 Example:
    #		swbis                    r=0.585.20070324a       v=GNU   i=0   
    #	

class SWICAT_SR:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.number = 0
        self.level = str(['\0' for _ in range(2)])
        self.matches_bundle = 0
        self.matches_product = 0
        self.matches_fileset = 0
        self.bundle = '\0'
        self.line = '\0'
        self.entry_line = '\0'
        self.se = None
        self.swspec = None
        self.catalog_entry_path = None
        self.found = 0

#	 Example:
#		R1:P:BP.:swbis:swbis                    r=0.585.20070324a       v=GNU   i=0
#

# Query/Response pair 
class SWICAT_SC:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.sq = None
        self.sr= None
        self.swverid_uuid = 0
        self.status = 0


class SWICAT_SL:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.sc = None

def swicat_se_create():
    se = SWICAT_SE()
    se.entry = None
    se.tag = None
    se.revision = None
    se.vendor_tag = None
    se.location = None
    se.sequence = None
    se.qualifier = None
    return se

def swicat_se_delete(se):
    if se.entry is not None:
        del se.entry
    if se.tag is not None:
        del se.tag
    if se.revision is not None:
        del se.revision
    if se.vendor_tag is not None:
        del se.vendor_tag
    if se.location is not None:
        del se.location
    if se.sequence is not None:
        del se.sequence
    if se.qualifier is not None:
        del se.qualifier
    del se

def sr_construct_swverid(sr, se):
    buf = STROB()
    buf.str_ = strob_open(30)

    if sr.bundle:
        buf.str_ += sr.bundle
    if len(buf.str_):
        buf.str_ += "."
    if se.tag:
        buf.str_ += se.tag
    if se.vendor_tag:
        buf.str_ += "," + swvidc.SWVERID_VERIDS_VENDOR_TAG + swvidc.SWVERID_RELOP_EQ2 + se.vendor_tag
    if se.revision:
        buf.str_ += "," + swvidc.SWVERID_VERIDS_REVISION + swvidc.SWVERID_RELOP_EQ + se.revision
    if se.location:
        buf.str_ += "," + swvidc.SWVERID_VERIDS_LOCATION + swvidc.SWVERID_RELOP_EQ2 + se.location
    if se.qualifier:
        buf.str_ += "," + swvidc.SWVERID_VERIDS_QUALIFIER + swvidc.SWVERID_RELOP_EQ2 + se.qualifier

    E_DEBUG2("fully qualified swspec=[%s]", strob_str(buf))
    swverid = swverid_open(swlibc.SW_A_product, strob_str(buf))

    if not swverid:
        print(f"{swlib_utilname_get()}: construction of version id failed for {buf.str_}", file=sys.stderr)

    sr.swspec = swverid
    strob_close(buf)
    return 1 if swverid is None else 0

def swicat_se_parse(se, entry):
    tmp = STROB()
    tmp.str_ = [0] * 80
    # Example:
    #   wbis                    r=0.585.20070324a       v=GNU   i=0
    #	
    E_DEBUG2("Entering: [%s]", entry)
    se.entry = swlib_strdup(entry)
    fn = 0
    s = strob_strtok(tmp, entry, " \t\n\r")
    while s:
        if fn == 0:
            E_DEBUG2("assigning tag: [%s]", s)
            se.tag = swlib_strdup(s)
        else:
            if strncmp(s, "r=", 2) == 0:
                s += 2
                E_DEBUG2("assigning revision: [%s]", s)
                se.revision = swlib_strdup(s)
            elif strncmp(s, "v=", 2) == 0:
                s += 2
                E_DEBUG2("assigning vendor_tag: [%s]", s)
                se.vendor_tag = swlib_strdup(s)
            elif strncmp(s, "i=", 2) == 0:
                s += 2
                E_DEBUG2("assigning sequence: [%s]", s)
                se.sequence = swlib_strdup(s)
            elif strncmp(s, "l=", 2) == 0:
                s += 2
                E_DEBUG2("assigning location: [%s]", s)
                se.location = swlib_strdup(s)
            elif strncmp(s, "q=", 2) == 0:
                s += 2
                E_DEBUG2("assigning qualifier: [%s]", s)
                se.qualifier = swlib_strdup(s)
            else:
                # error 
                return -1
        fn += 1
        s = strob_strtok(tmp, None, " \t\r\n")
    strob_close(tmp)
    return 0

# Query 
def swicat_sq_create():
    sq = SWICAT_SQ()
    sq.number = 0
    sq.level[0] = 0
    sq.level[1] = 0
    sq.line = None
    sq.swspec_string = None
    sq.swspec = None
    return sq


def swicat_sq_delete(sq):
    if sq.line is not None:
        del sq.line
    if sq.swspec_string is not None:
        del sq.swspec_string
    if sq.swspec is not None:
        swverid_close(sq.swspec)
    del sq


def swicat_sq_parse(sq, line):
    status = 0
    field_no = 0
    tmp = strob_open(80)
    sq.line = swlib_strdup(line)

    #	 Example:
    #		Q1:P:swbis,r==0.585.20070324a
    #	

    E_DEBUG2("line=[%s]", line)

    s = strob_strtok(tmp, line, ":\r\n")
    while s != '\0':
        if field_no == 0:
            # first field 
            sq.number = swlib_atoi(s[1:], status)
            if status != 0:
                return -1
        elif field_no == 1:
            # second field 
            sq.level[0] = s[0]
        elif field_no == 2:
            # third field 
            sq.swspec_string = swlib_strdup(s)
            sq.swspec = swverid_open(None, s)
            if sq.swspec is None:
                return -1
        s = strob_strtok(tmp, None, ":\r\n")
        field_no += 1
    return 0

# Response 
def swicat_sr_create():
    sr = SWICAT_SR()
    sr.line = None
    sr.bundle = None
    sr.entry_line = None
    sr.swspec = None
    sr.catalog_entry_path = strob_open(48)
    sr.se = None
    sr.found = 0
    return sr


def swicat_sr_delete(sr):
    if sr.line is not None:
        del sr.line
    if sr.bundle is not None:
        del sr.bundle
    if sr.swspec is not None:
        swverid_close(sr.swspec)
    del sr


def swicat_sr_parse(sr, line):
    tmp = STROB()
    field_no = 0
    status = 0
    retval = 0
    #         Example:
    #                R1:P:BP.:swbis:swbis                    r=0.585.20070324a       v=GNU   i=0
    #
    E_DEBUG2("Entering: [%s]" , line)
    sr.line = swlib_strdup(line)
    E_DEBUG2("sr_parse line=[%s]", line)

    s = strob_strtok(tmp, line, ":\n\r")
    while s:
        E_DEBUG2("[%s]", s)
        if field_no == 0:
            # first field 
            E_DEBUG("case 0")
            sr.number = swlib_atoi(s[1:], status)
            if status != 0:
                return -1
        elif field_no == 1:
            # second field 
            E_DEBUG("case 1")
            sr.level[0] = s[0]
        elif field_no == 2:
            # third field 
            E_DEBUG("case 2")
            if len(s) != 3:
                return -3
            sr.matches_bundle = 0
            sr.matches_product = 0
            sr.matches_fileset = 0
            if s[0] == 'B':
                sr.matches_bundle = 1
            if s[1] == 'P':
                sr.matches_product = 1
            if s[2] == 'F':
                sr.matches_fileset = 1
        elif field_no == 3:
            # fourth field 
            E_DEBUG("case 4")
            sr.bundle = swlib_strdup(s)
        elif field_no == 4:
            # fifth field 
            # NOTE: the bundle tag is stripped from  the string 
            E_DEBUG("case 5")
            E_DEBUG2("parsing (_se_parse) [%s]", s)
            se = swicat_se_create()
            ret = swicat_se_parse(se, s)
            if ret:
                return -4
            ret = sr_construct_swverid(sr, se)
            retval = ret
            sr.se = se
        s = strob_strtok(tmp, None, ":\r\n")
        field_no += 1
        E_DEBUG2("field number: %d", field_no)
    strob_close(tmp)
    return retval

# Query/Response Combination 

def swicat_sc_create():
    sc = SWICAT_SC()
    sc.sr = vplob_open()
    sc.sq = None
    sc.status = 0
    sc.swverid_uuid = 0
    return sc

def swicat_sc_delete(swicat_sc):
    vplob_delete_store(swicat_sc.sr, swicat_sr_delete)
    del swicat_sc

def swicat_sc_add_sr(swicat_sc, swicat_sr):
    vplob_add(swicat_sc.sr, swicat_sr)


def swicat_sc_set_sq(swicat_sc, swicat_sq):
    swicat_sc.sq = swicat_sq


def swicat_sl_create():
    sl = SWICAT_SL()
    sl.sc = vplob_open()
    return sl


def swicat_sl_delete(swicat_sl):
    vplob_delete_store(swicat_sl.sc, swicat_sc_delete)
    del swicat_sl


def swicat_sl_add_sc(swicat_sl, swicat_sc):
    vplob_add(swicat_sl.sc, swicat_sc)


def swicat_sr_form_swspec(sr, buf):
    path = '\0'
    tmp = strob_open(48)
    strob_strcpy(buf, "")
    path = swicat_sr_form_catalog_path(sr, "", tmp)
    if path is None:
        return None
    temp_ref_path = (path)
    path = swicat_sr_form_swspec_from_catalog_path(temp_ref_path, buf)
    path = temp_ref_path
    strob_close(tmp)
    return path


def swicat_sr_form_catalog_path(sr, installed_software_catalog, buf):

    if buf is None:
        sb = sr.catalog_entry_path
    else:
        sb = buf
    strob_strcpy(sb, "")

    if sr.matches_bundle == 0 and sr.matches_product == 0:
        return strob_str(sb)

    strob_strcpy(sb, installed_software_catalog)
    s = sr.bundle

    swlib_unix_dircat(sb, s)
    s = sr.se.tag
    swlib_unix_dircat(sb, s)
    s = sr.se.revision
    swlib_unix_dircat(sb, s)
    s = sr.se.sequence
    swlib_unix_dircat(sb, s)
    if swlib_check_clean_path(strob_str(sb)):
        sys.stderr.write("error: path tainted: %s" % strob_str(sb))
    return strob_str(sb)


def swicat_sr_form_swspec_from_catalog_path(path, swspec):
    #	 path has the form:
    #		<isc_path>/foo/foo/1.1/0
    #
    #	Using this path, make a string with form
    #		bundle.product,r=revision,i=instance_id
    #	and pass it to swverid_open()
    #	

    n = 0
    s = '\0'
    tmp = None
    dir_components = None

    strob_strcpy(swspec, "")
    if path is None or len(path) == 0:
        return None

    tmp = strob_open(100)
    dir_components = strar_open()

    s = strob_strtok(tmp, path, "/")
    while s != '\0':
        strar_add(dir_components, s)
        s = strob_strtok(tmp, None, "/")

    n = strar_num_elements(dir_components)
    if n < 4:
        return None

    n -= 1
    strob_sprintf(swspec, 0, "%s.%s,r==%s,i=%s", strar_get(dir_components, n - 3), strar_get(dir_components, n - 2), strar_get(dir_components, n - 1), strar_get(dir_components, n - 0))

    return strob_str(swspec)

