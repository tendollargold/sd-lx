
# phases.swinstall.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals


from sd.cli.demand import bool


"""
    4.0 Execution Phase Systems Management: Distributed Software Administration
    swinstall utility Page 90 - 103

    Check for extended options
    Extended Options
    The swinstall utility supports the following extended options. The description in 
    Chapter 3 applies.

"""
swinstalloptions={
    'allow_downdate': bool(False),
    'allow_incompatible': bool(False),
    'ask': bool(False),
    'autoreboot': bool(False),
    'autorecover': bool(False),
    'autorecover_product': "",
    'autoselect_dependencies': "as_needed",
    'autoselect_patches': bool(True),
    'defer_configure': bool(False),
    'defer_deleting_files': bool(False),
    'distribution_source_directory': "/SD_CDROM",
    'enforce_dependencies': bool(True),
    'enforce_locatable': bool(True),
    'enforce_scripts': bool(True),
    'enforce_dsa': bool(True),
    'installed_software_catalog': "/var/adm/sw/products",
    'logfile': "/var/adm/sw/swinstall.log",
    'loglevel': "1",
    'match_target': bool(False),
    'patch_save_files': bool(True),
    'patch_filter': "*",
    'patch_match_target': bool(False),
    'reinstall': bool(False),
    'reinstall_files': bool(False),
    'reinstall_files_use_cksum': bool(True),
    'save_modified_files': bool(False),
    'saved_files_directory': "/var/adm/sw/save",
    'select_local': bool(True),
    'software': "",
    'targets': "",
    'verbose': "1"
}

def swinstall_options():
    """Collection of options for swinstall utility. :api"""
    swinstalloptions.update(globals())



# There are three key phases in the swinstall utility:
#   1. Selection phase
#   2. Analysis phase
#   3. Execution phase

#
# Selection Phase
#

# Multiple versions of a software product can exist from the source, distinguished by 
# their respective ‘‘version distinguishing attributes’’ ( revision, architecture, and
# vendor_tag). If the method described in Section 3.4.1 on page 38 results in an 
# ambiguous selection, the following method is used to identify a single version:

#    If allow_incompatible=false , the target uname attributes are used to filter the 
#    available products to only those that are compatible with the target systems, 
#    then the version with the highest possible product revision is chosen from this 
#    filtered list. If this filtering and selection of a highest revision does not 
#    result in a unique version, then no version is selected. If 
#    allow_incompatible=true , then only the highest revision is used to try to 
#    determine a unique version. In either case, if there is still an ambiguous 
#    selection, no version is selected. See Section 3.4.1.2 on page 42.

# If a software selection has dependency specifications on other software, and the 
# option autoselect_dependencies=true, the dependency software is attempted to be 
# automatically selected using the same method to determine a single version. This 
# automatically selected software is then installed along with the rest of the 
# selected software. If autoselect_dependencies=as_needed, then dependency software 
# is attempted to be automatically selected and installed only if the dependency is 
# not already met on the target.

# If a fileset has an exrequisite on another software object, and that other software 
# object is part of the specified software selection, either explicitly or as part of 
# another selection, then the fileset is excluded. If two filesets have exrequisites 
# on each other, then the behavior is implementation defined.

# If more than one ancestor is defined, then this new fileset is included in the 
# selection list during update if either or both ancestors are currently installed and
# the match_target option is set to true.

# Unless ancestor is explicitly defined, all filesets have the default ancestor of any
# revision less than itself, that is,
#     product_tag.fileset_tag,r<revision,a=architecture,v=vendor_tag

# For swinstall, each selection added to the selected software list must satisfy the 
# following validation checks. If any of these checks result in an event with a status
# of [SW_ERROR], the selection is not added to the list and the implementation defined
# handling procedure can be invoked.

#  · If the selection is not available from the source, generate an event.
#    [SW_ERROR: SW_SELECTION_NOT_FOUND]

#  · If a unique version can not be identified, generate an event.
#    [SW_ERROR: SW_SELECTION_NOT_FOUND_AMBIG]

#  · If an attempt is made to select more than one version of a given product targeted
#    for the same location, generate an event.
#    [SW_ERROR: SW_EXISTING_MULTIPLE_VERSION]

#  · If 2allow_incompatible=true", for each target where the software selected is 
#    incompatible with that target (see Section 3.4.1.2 on page 42, generate an event.
#    [SW_WARNING: SW_NOT_COMPATIBLE]

#    If allow_incompatible=false, for each target where the software selected is 
#    incompatible with that target (see Section 3.4.1.2 on page 42, generate an event.
#    The implementation defined error handling procedure is then invoked.
#    [SW_ERROR: SW_NOT_COMPATIBLE]

#  · Check if a non-default product location has been specified. 
#    If enforce_locatable=true , generate an event.
#    [SW_ERROR: SW_NOT_LOCATABLE]

#    If enforce_locatable=false , generate an event.
#    [SW_WARNING: SW_NOT_LOCATABLE]

#  · If the software is excluded, generate an event.
#    [SW_NOTE: SW_EXREQUISITE_EXCLUDE]

# If ask=true then execute the software request scripts for the selected software as 
# described in the swask utility. See swask on page 78, extended description.

# Selection of software to update can be done using the match_target option. The 
# match_target option provides a shortcut for selecting software. By default set to 
# false. When set to true, this option causes each fileset in the distribution to be 
# included in the selection list if:

#  · A fileset with the same product and fileset tag exists in only one location on 
#    the system

#  · The fileset in the depot has an ancestor attribute that matches a single 
#    installed fileset

#  · It is the highest compatible version if more than one version in the depot 
#    matches, including accounting for superseding filesets

# If there are multiple filesets that match (that is, multiple revisions are 
# installed), it is not clear which if any should be updated, and an event is 
# generated: [SW_WARNING: SELECTION_NOT_FOUND_AMBIG].

# These selections are combined with any other selections specified in the other 
# supported means (command line operands, a software_selections file or the software 
# extended option), and then go through the standard software selection checks.

#
# Analysis Phase
#

# The target role uses the file size information and checkinstall scripts obtained 
# from the source to determine whether or not the install utility proceeds on the 
# given target. When failures occurs in the disk space analysis and checkinstall 
# scripts, it is implementation defined whether or not to proceed with a partial list 
# of software selections.

# If any target generates an event with a status of [SW_ERROR] during any of the 
# analysis operations, what software is attempted to be installed is determined by the
# implementation defined error handling procedures.

# The target role checks the following requirements:

#  · If the target installed_software object does not exist on a host, create the path
#    to the target with default attributes, and generate an event.
#    [SW_NOTE: SW_SOC_CREATED]

#  · Check that selected filesets are not the same version as already installed. If 
#    2reinstall=false", note that they will be skipped by generating an event.
#    [SW_NOTE: SW_SAME_REVISION_SKIPPED]

#    If reinstall=true, note that they will be reinstalled by generating an event.
#    [SW_NOTE: SW_SAME_REVISION_INSTALLED]

#  · Check that selected filesets are not lower versions of the fileset already 
#    installed on the host.
#    If allow_downdate=false , generate an event.
#    [SW_ERROR: SW_HIGHER_REVISION_INSTALLED]

#    If allow_downdate=true , generate an event.
#    [SW_WARNING: SW_HIGHER_REVISION_INSTALLED]

#  · Execute vendor-supplied checkinstall scripts to perform product-specific checks 
#    of the target. If the checkinstall script returns an error, and 
#    enforce_scripts=true, generate an event and invoke the implementation defined 
#    error handling procedure.
#    [SW_ERROR: SW_CHECK_SCRIPT_ERROR]

#    If the checkinstall script returns an error and enforce_scripts=false, generate 
#    an event.
#    [SW_WARNING: SW_CHECK_SCRIPT_ERROR]

#    If the checkinstall script returns a warning, generate an event.
#    [SW_WARNING: SW_CHECK_SCRIPT_WARNING]

#    If the script has a return code of 3, generate an event and unselect the fileset
#    (or all filesets in the product for a product level script).
#    [SW_NOTE: SW_CHECK_SCRIPT_EXCLUDE]

#  · Verify that the needed dependencies of the filesets are met. If 
#    enforce_dependenciesr= true, generate an event.
#    [SW_ERROR: SW_DEPENDENCY_NOT_MET]

#    If enforce_dependencies=false, generate an event.
#    [SW_WARNING: SW_DEPENDENCY_NOT_MET]

#  · Check that there is enough free disk space on the target file system to install 
#    the selected products. If there is not enough disk space and enforce_dsa=true, 
#    generate an event.
#    [SW_ERROR: SW_DSA_OVER_LIMIT]

#    If there is not enough disk space and enforce_dsa=false , generate an event.
#    [SW_WARNING: SW_DSA_OVER_LIMIT]

#  · An implementation may generate the following events if disk space analysis 
#    encountered any problems that prevented the analysis. If enforce_dsa=true, 
#    generate an event.
#    [SW_ERROR: SW_DSA_FAILED_TO_RUN]

#    If enforce_dsa=false , generate an event.
#    [SW_WARNING: SW_DSA_FAILED_TO_RUN]

# How disk space analysis is implemented is undefined. However an implementation must
# account at least for the sizes of the files and control_files being installed, the 
# additional sizes from the vendor supplied space file described in Section 5.2 on 
# page 130, and the additional space required from saving files if autorecover=true 
# and if required by the implementation defined recovery process.

# Most revision checks during operation of install are done fileset by fileset. When 
# checking for newer revisions of the fileset, the product revision is checked before 
# the fileset revision:

#  · If one fileset’s product has a revision higher than the other fileset’s product 
#    revision, it is a newer revision.

#  · If the product’s revisions are undefined or the same, then the fileset revisions 
#    are checked.

#
# Execution Phase
#

# The execution phase is the third part of the installation process, and is entered 
# once either the selections have passed the analysis phase with no events with a 
# status of [SW_ERROR] or if permitted by the implementation defined error handling 
# procedures.

# The relationship between the preinstall and postinstall scripts, fileset loading, 
# and state transitions for swinstall is shown in the following list. Products are 
# ordered by prerequisite dependencies if any. Fileset operations are also ordered by 
# any prerequisites.

#  1. Install each product:
#       a. Create the installed_software catalog information for the product and its
#          contained subproducts.

#       b. Run the preinstall script for the product.

#       c. Install each fileset in the product:

#            i. Create the installed_software catalog information for the fileset, 
#               setting the state to transient . Also update the state of any existing
#               fileset that is being updated or downdated to transient.

#           ii. Run the preinstall script for the fileset.

#          iii. Load the files for the fileset.

#           iv. Run the postinstall script for the fileset.

#            v. Update the results of the scripts. Update the state of the fileset to 
#               installed . Also set the state of any existing fileset that is being 
#               updated or downdated to removed or remove the catalog information for
#               that fileset.

#       d. Run the postinstall script for the product.

#       e. Once the catalog information for the last fileset in a particular product
#          version has been removed due to update, like swremove, the catalog 
#          information for that product version should also be removed. See swremove 
#          on page 117.

#  2. Install each bundle:

#       a. Create the installed_software catalog information for the bundle.

#  3. Configure each product (see ‘‘executing configure scripts’’ in swconfig on page 
#     81.  Configuration is done at this point by the swinstall utility only if 
#     defer_configure=false, the target directory is /, and no filesets with the 
#     is_reboot attribute equal to true have been installed.

#       a. Run the configure script for the product.

#       b. Configure each fileset in the product:

#            i. Run the configure script for the fileset.

#           ii. Update the result of the script. Update the state of the fileset to 
#               configured in the catalog for the installed_software object.

# Configuration will not be executed by swinstall if the software creates a multiple 
# version, the target directory is not /, or if the software is incompatible and 
# allow_incompatible=false (see Section 3.4.1.2 on page 42). In these cases, swconfig
# may be used.

# If events with a status of [SW_ERROR] are detected during the execution phase, the 
# swinstall utility generates the appropriate event, any log entries, and invokes the
# implementation defined error handling procedures. For each fileset that failed, the
# installed_software catalog is updated to the state corrupt.

# The swinstall utility will only remove catalog information for filesets being 
# updated if they have the same product and fileset tag in the same location by 
# default. By specifying a supersedes attribute, catalog information for filesets 
# being updated that have changed names (have a different tag attribute) or are 
# otherwise superseded by new functionality will be removed as well.

#  · File Location

#    If an alternate root directory was specified (a value for 
#    installed_software=path other than /, then the alternate root directory is used 
#    as a prefix to the file path attribute to determine the file location in the file
#     system. See Section 3.4.2 on page 42.

#    The file path will be modified if the product is locatable and a new product 
#    location is specified (using the l=location software specification). The product
#    directory part of the file path is replaced by the value product location 
#    attribute before a file is placed in the target file system.12

#    (12. If a product is locatable (has the product=is_locatable attribute set to 
#    true), all files that have the value of product directory as the initial part of 
#    their path will be installed to a new location if one has been specified. The 
#    product directory attribute is the base directory for the files that are 
#    locatable within a specific product.)

#    If a bundle location is specified (using the l=location software specification 
#    when specifying a bundle), then the bundle=location will be prepended to the 
#    location specification for each software_spec in the contents of the bundle, 
#    prior to replacement of the product directory part of the file path.

#  · Preinstall Scripts
# In this step of the execution phase, swinstall executes product and fileset p
#  reinstall scripts.

#  — If a preinstall script returns an error and enforce_scripts=true, generate an 
#    event and invoke the implementation defined error handling procedures.
#     [SW_ERROR: SW_PRE_SCRIPT_ERROR]

#  — If the preinstall script returns an error and enforce_scripts=false, generate 
#    an event.
#     [SW_WARNING: SW_PRE_SCRIPT_ERROR]

#  — If a preinstall script returns a warning, generate an event.
#    [SW_WARNING: SW_PRE_SCRIPT_WARNING]

#    Control scripts must adhere to the specifications in section Section 3.6.1 on 
#    page 56.

# · File Loading

# In this step, swinstall loads the files from the source onto the target file system 
# according to information obtained from the source distribution. All file types are 
# created using the attributes defined for those files in the source distribution. 
# Regular files (that is, those with a file type of f) are loaded using the content 
# from the source distribution.

# If the source file is a regular file or a directory and its path already exists on 
# the target file system as a symbolic link, then the symbolic link is followed and 
# the file is stored in the path defined by the symbolic link.

# If the source file is a symbolic link, then the existing path is replaced by 
# symbolic link.

#  — If there are too many levels of symbolic links,13 then the file is skipped and 
#    an event is generated. (13 It is not the intention of this Software 
#    Administration specification to define symbolic links in a manner inconsistent 
#    with POSIX.1. However, no approved POSIX standard currently contains symbolic 
#    links. This definition is a placeholder until such time as an approved standard 
#    provides the definition.)
#    [SW_WARNING: SW_FILE_WARNING]

# The file owner and group names are set to the values specified for the file owner 
# and file gid attributes for the source file. If the target host does not contain 
# those file owner and group names, the file uid and gid are set to the numeric values
# specified for these attributes for the source file. If no values are specified for 
# these attributes, the uid and gid are set to the effective uid and gid of the 
#  current process. See Section 5.2.14.3 on page 144.

#  — If the user or group of the file is not defined on the target host, or either of 
#    these attributes are not defined for the file, generate an event:
#    [SW_WARNING: SW_FILE_WARNING]

#  — If the mode attribute of the file has the set user id on execution (S_ISUID) bit 
#    set and either the user attribute of the file is not defined on the target host 
#    or the user attribute is not specified for the file, the corresponding mode bit 
#    in the file system will not be set when installing the file and an event is 
#    generated. See POSIX.1.
#    [SW_ERROR: SW_FILE_ERROR]

#  — If the mode attribute of the file has the set group id on execution (S_ISGID) bit
#    set and either the group attribute of the file is not defined on the target host 
#    or the group attribute is not specified for the file, the corresponding mode bit
#    in the file system will not be set when installing the file and an event is 
#    generated. See POSIX.1.
#    [SW_ERROR: SW_FILE_ERROR]

# The value of the file mode attribute on the file is set to the value of the file mode attribute for the source file. An exception is that directories that already exist are not modified. If no values are specified for this attribute, the mode is set to the default file creation mode for the current process.

# If there is an existing installed file that matches the values supplied in the distribution for the path, cksum, date, and size attributes, the file is not reloaded unless the user has specified that the fileset is being reinstalled.

# The reinstall option is not sufficient for controlling whether files are reinstalled during update, reinstall or downdate, since it is only controls whether any of the same fileset at a target location is attempted or not, based on the catalog information stored on that target.

# The reinstall_files option, by default set to false, allows for the administrator to check for up-to-date files independent from the reinstall option. If set to true, files are installed independently of whether they are the same. If set to false, then the actual size, mtime and cksum atttributes of the installed files, as opposed to the catalog information about these files, are checked against the catalog information in the source before installing the file.

# As it takes time to compute the cksum when the reinstall_files option is set to false, there is an option to skip that check that may be sufficient in secure or controlled environments. The reinstall_files_use_cksum option, by default set to true, can be set to false to skip the cksum check. In this case, only the size and mtime are checked when determining if the file is up to date.

# If a file load fails for any other reason such as a lost connection to the remote source or tape eject, then the fileset install fails.

# In order to aid cleanup of obsolete shared files, a new file type x is defined that directs the install execution phase to remove a file instead of installing it. Files of type x will have the paths resolved with respect to product location attributes just as normal files to determine the file to remove. If defer_deleting_files=true, then the files are not removed. Disk space analysis will account for files of this type by subtracting existing file sizes.

# The following are problems that may occur during the file load step:

#  — If a problem occurs while loading a file onto a target, an event is generated 
#    and, for events with a status of [SW_ERROR], the implementation defined error 
#    handling procedures invoked. If there are too many levels of symbolic links, 
#    generate an event.
#    [SW_ERROR: SW_FILE_ERROR]

#    Whether remote files are installed is implementation defined. If the file is on 
#    a remote file system and was loaded, generate an event.
#    [SW_NOTE: SW_FILE_IS_REMOTE]

#    If it was not loaded, also generate an event.
#    [SW_WARNING: SW_FILE_IS_REMOTE]

#  — If a file can not be updated because it is busy, or it is a directory, then move 
#    that file to implementation defined location and generate an event. How these 
#    files are eventually removed is also implementation defined.
#    [SW_WARNING: SW_FILE_NOT_REMOVABLE]

#  — If the source becomes inaccessible for any reason during the process of loading 
#    files, generate an event.
#    [SW_ERROR: SW_SOURCE_ACCESS_ERROR]

# If autorecover=true, then all files that are being updated are saved. It is 
# implementation defined where these files are saved. The saved files for filesets 
# in each product are removed in an implementation defined manner at some point after
# that product completes the execution phase.

# Before overwriting any existing file, if the save_modified_files option is set to 
# true, any file that has a size or cksum different to that in the installed software
# catalog will be saved to an implementation defined location. If the 
# save_modified_files option is set to false, then these files may be overwritten.

# After a product has been installed, files in the product of type delete file are 
# removed if the defer_deleting_files option is set to false. If the 
# defer_deleting_files option is set to true, then files of this type are not deleted.

# · Compression

# When installing files, all compressed files are uncompressed as follows as part of file loading:

# — All files that have the compression_state attribute value of compressed are
#   uncompressed, according to the value of compression_type attribute. The way in 
#   which this is done is implementation defined. If the file cannot be uncompressed, 
#   generate an event.
#   [SW_ERROR: SW_COMPRESSION_FAILURE]

#  · Postinstall Scripts

# In this step, swinstall executes the product and fileset postinstall scripts.

#  — If a postinstall script returns an error and enforce_scripts=true, generate an 
#    event and invoke the implementation defined error handling procedures.
#    [SW_ERROR: SW_POST_SCRIPT_ERROR]

#  — If the postinstall script returns an error and enforce_scripts=false, generate 
#    an event.
#    [SW_WARNING: SW_POST_SCRIPT_ERROR]

#  — If a postinstall script returns a warning, generate an event.
#    [SW_WARNING: SW_POST_SCRIPT_WARNING]

#  · Kernel Scripts Special customization and install steps are executed when 
#    processing kernel filesets. Kernel filesets are those for which the value of the #    is_kernel attribute is true, causing swinstall to modify the fileset load order 
#    and to invoke the postkernel script. Apart from this, swinstall has no special 
#    functionality for installing kernels.

# The postkernel scripts are those specified by the value of the product=postkernel 
# attribute, or by the implementation defined default. The functions invoked by this 
# postkernel script are implementation defined. Examples of use include rebuilding 
# the kernel or moving a new default kernel into place.

# The postkernel script is not interactive, and issues all informative and error 
# messages to stdout and stderr, which redirects it to the log file. In addition, 
# the postkernel script provides a standard return value indicating success (0, that 
# is, zero), error (1) or warning (2).

#  — If the postkernel script has an error, an event is generated and the 
#    implementation defined error handling procedures are invoked.
#    [SW_ERROR: SW_POSTKERNEL_ERROR]

# — If the postkernel script has an warning, an event is generated.
#   [SW_WARNING: SW_POSTKERNEL_WARNING]

# The kernel filesets are processed before the rest of the filesets. All products are
# first processed for their kernel filesets, and then all products are processed for 
# their non-kernel filesets. The ordering of products and filesets also adheres to 
# prerequisites, just as normal filesets:

#   1. Install the kernel filesets for each product:
#      a. Create the installed_software catalog information for the product.

#      b. Run the preinstall script for the product.

#      c. Install each kernel fileset in the product:

#         i. Create the installed_software catalog information for the fileset, 
#            setting the state to transient .

#        ii. Run the preinstall script for the kernel fileset.

#       iii. Load the files for the kernel fileset.

#        iv. Run the postinstall script for the kernel fileset.

#         v. Update the results of the scripts. Update the state of the fileset to
#            installed.

#      d. Run the postinstall script for the product.

#   2. Perform steps after installing kernel filesets by calling the zero or more 
#      scripts defined by the product.postkernel attributes of each product with a 
#      kernel fileset, and the implementation defined default postkernel script if a 
#      product does not define a product.postkernel attribute.

#   3. Install the rest of the filesets for each product as described in the execution phase description for swinstall, omitting the kernel filesets already installed.

#   4. After all filesets have been installed, the implementation defined reboot 
#      procedure is executed on the target host if a fileset with the is_reboot 
#      attribute #   set to true has been installed 
#      and the SW_ROOT_DIRECTORY is /, and autoreboot=true. If rebooting, the 
#      software is not configured. The products will  be configured after the reboot
#      in an implementation defined manner using the  swconfig utility.

#   5. If not rebooting, then configure each product as described in the execution 
#      phase for swinstall (including both kernel and non-kernel filesets).

# · Rebooting the System

#   If this step is required, the target role executes the implementation defined reboot procedure after all products have been installed. It is performed only when software is installed that requires a reboot as part of its installation (indicated by the is_reboot fileset attribute).

#   If the system fails to execute the reboot step, generate an event.
#   [SW_ERROR: SW_CANNOT_INITIATE_REBOOT]

#  · Recovery

# Within the execution phase of a particular product (from the product preinstall 
# step through the product postinstall step), if any preinstall script, file loading,
# or postinstall script fails for a fileset, that fileset is deemed to have failed 
# during install. The failure of a product postinstall script is considered the same 
# as if all fileset postinstall scripts had failed.

# If such a failure occurs and autorecover=false , no recovery is provided for any 
# filesets deemed to have failed during install, and the fileset state attribute of 
# those filesets is set to corrupt .  No further attempt is made to install such 
# filesets during the current invocation of swinstall.  Install can proceed on other 
# filesets which did not fail during install.  

# If an install failure occurs and autorecover=true, at least the following minimal 
# error recovery must be provided at the fileset level. Additional recovery behavior, 
# such as recovering the whole product or all products, is implementation defined. 
# Additionally, if enforce_dependencies=true, implementations should take into account
# other filesets in the product that have a dependency on that failed fileset.

# The recovery is initiated at the point of failure, recovering the affected filesets,
# then continuing from the point of failure to the remaining filesets.

# Recovery involves running unpostinstall scripts, restoring files, and running
# unpreinstall scripts. The relationship between these steps for each product is shown
# in the following list.14

#   1. Create the installed_software catalog information for the product.

#   2. Run the preinstall script for the product.
#       If the preinstall script fails, or if all filesets have failed, run the product
#       unpreinstall script, remove the catalog information for the product and go on to the
#       next product.

#   3. Install each fileset in the product

#         a. Create the installed_software catalog information for the fileset, 
#            setting the state of it and the fileset being updated to transient .

#         b. Run the preinstall script for the fileset.
#            If the preinstall script fails, run the unpreinstall script for the 
#            fileset, remove the catalog information for the fileset, restore the 
#            state of the fileset being updated, and go on to the next fileset.

#         c. Load the files for the fileset.

#            Before loading any files, save any existing files that will be 
#            overwritten by a file being loaded from the fileset, and then load the 
#            files for the fileset. If the fileset loading fails, restore the saved 
#            files for the fileset, delete all loaded files for which there is no 
#            saved file, and perform the previously described recovery step for this 
#            fileset.

#         d. Run the postinstall script for the fileset.

#            If the postinstall script fails, run the unpostinstall script for the 
#            fileset, and perform steps (3b) and (3c) for this fileset.

#         e. Update the results of the scripts. Update the state of the fileset to 
#            installed.

#   4. Run the product postinstall script.

#      If the product postinstall script fails, run the product unpostinstall script,
#      and perform each of the previously described recovery steps for each fileset.

#   5. This is the first point in the process where the saved files may be removed. 
#      Remove the catalog information for filesets that were updated, or set the 
#      state of those filesets to removed.

#      Once the catalog information for the last fileset in a particular product 
#      version has been removed due to update, like swremove, the catalog information
#      for that product version should also be removed. See swremove on page 117.

#      During update, it may be useful to have either the whole product or no part of 
#      it installed if there is a failure in any fileset or product control script of 
#      file loading. Set the autorecover_product option to true to extend the 
#      autorecover behavior. The behavior is identical except that any failure unwinds
#      the entire product back to where it was before the install. In the case of a 
#      product postinstall script failure, the behavior is the same. From an 
#      implementation standpoint, all files in the product that are being updated are 
#      saved until the product completes successfully.

# · Sparse Updates

# Updates can be packaged as sparse updates. When selecting a sparse fileset, 
# swinstall has the following behavior:

#  — Unless explicitly specified, swinstall will choose the highest compatible 
#    revision of a product and fileset whether it is sparse or not.

#  — Installing a sparse fileset follows the same rules for update, downdate and 
#    reinstall as other filesets.

#  — If the ancestor of the sparse fileset is not already installed, and it is 
#    available from the depot, it is included in the list of software to install. If 
#    the ancestor is not already installed, and is not in the depot, then the sparse 
#    fileset can not be installed and an event is generated: 
#    [SW_ERROR: SW_DEPENDENCY_NOT_MET].

#  — When installing a sparse fileset, the existing fileset’s catalog information is 
#    included as the catalog information of the sparse fileset, and then augmented 
#    with the sparse filesets file contents (adding and possibly deleting files). 
#    Then, as with normal update, the previous fileset’s catalog information is 
#    deleted.

# · Patches

#   Installation of patch products will, in general, follow the same rules as software
#   installation.  The key difference will be that a filtering mechanism is provided 
#   that allows only patches meeting specified criteria selected. The additional 
#   filtering mechanism will be provided through support of category in software 
#   specifications, and autoselect_patches, patch_match_target and patch_filter 
#   options.

#   When a patch is installed, the fileset that has been patched has the 
#   applied_patches # attribute updated to include that patch, and the contained 
#   files information updated to include the patched file attributes.

#   When a patch is installed, by default it has the patch_state of applied. When 
#   that patch is committed, or it has been installed without saving roll-back files,
#   it has the state of committed. When that patch is superseded, the patch_state is 
#   set to superseded, and the superseded_by attribute is set to the software_spec of
#   the superseding fileset.

#   If a fileset is selected for installation and patch filesets for that base fileset
#   exist in the same source depot, all applicable patches will by default be selected
#   if the autoselect_patches option is set to true. The following additional rules 
#   will apply:

#   — Patch software selections will be filtered as defined by the patch_filter 
#     option.

#   — If more than one patch to a base fileset exists, patches to that fileset will be
#     examined to determine if any patches have been superseded by later versions. The
#     superseding patch will be installed and superseded patches will not be selected.

#   Patches can also be explicitly specified.

#   Patches that are installed in a separate session after the base product has been 
#   installed, can be selected explicitly or by matching the installed software using 
#   the patch_match_target option. If patches are selected via matching, the 
#   superseding mechanism described above will be used to determine the most recent 
#   patches to a fileset.

#   Consistent with the current level of expression support in the POSIX standard 
#   software specifications, a version qualifier can be repeated (for "AND" criteria),
#   and the pipe symbol ("|") can be used within qualifiers (for "OR" criteria).

#   Patches can be explicitly installed without autoselection or matching the target 
#   by specifying one or more software_spec operands.

#   In order to accommodate patching of libraries (for example libc.a), a new file 
#   type of a is used. When loading a file of type a (archive file), swinstall 
#   temporarily installs the .o file to the target path specified, integrates it into 
#   the archive specified by the archive_path attribute of the file, and then removes 
#   the .o file. If rollback is enabled (see below), the original .o file is 
#   automatically extracted first and saved so that it can be replaced. Disk Space 
#   Analysis is performed as needed to account for these operations.

#   If patch filesets and normal filesets are being installed in the same session, 
#   then each patch fileset is considered to have an implied prerequisite on the 
#   fileset that it is patching with respect to ordering. In other words, the product 
#   containing the patch fileset will be installed (or copied into serial 
#   distributions) after the one or more products containing that patches ancestors.

#   If an ancestor fileset has the is_kernel attribute set to true then the fileset 
#   patching it must have the is_kernel attribute set to true in order for it to be 
#   installed in the kernel phase of the execution. Otherwise, the patch will be 
#   installed along with other non-kernel filesets.

#   Allowing later rollback of a patch requires saving files via the patch_save_files 
#   option. This option is set to true by default, which results in files replaced by 
#   the patch being saved for later rollback. The directory where the saved files are #   stored is specified by the saved_files_directory option. The structure of the 
#   rollback directory is undefined.

#   Installation of a new version of a base fileset will result in removal of all 
#   filesets which patch the base fileset, together with any files saved for potential
#   roll-back.
