# ------------- Public Function Definitions ------------

# uxfio.py : buffered u*ix I/O functions.
#
# Copyright (C) 1997-2004,2006 James H. Lowe, Jr. <jhlowe@acm.org>
# Copyright(c) 2023 Paul Weber, convert to python
# All Rights Reserved.
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# uxfio ---  Unix  eXtended  File  Input/Output  API

#   File Controls       Hopefully none of these match a real uxix system.

# I/O Controls  Hopefully none of these match a real uxix system.

# Default tmp file name components

import errno
import io
# -------------------------------------------------------------
# ------- UXFIO API (PUBLIC FUNCTIONS)------------------------
import os
import sys
import tempfile
from ctypes import *

import fcntl

from sd.swsuplib.atomicio import safeio, atomicio
from sd.swsuplib.misc.cstrings import strerror
from sd.swsuplib.misc.swlib import fr
from sd.swsuplib.uinfile.uinfile import memcpy
from sd.util import MemoryAllocator

ma = MemoryAllocator

global UXFIO_BUFTYPE_FILE, UXFIO_BUFTYPE_DYNAMIC_MEM, UXFIO_NULL_FD, UXFIO_FD_MIN
global last_used_fd_index

class UxfioConst:
    UXFIO_FLEN = 2000000000
    UXFIO_LEN = 1024
    UXFIO_MAX_OPEN = 128
    UXFIO_FD_MIN = 2000000000
    UXFIO_OFF = 0
    UXFIO_ON = 1
    UXFIO_BUFTYPE_NOBUF = 0
    UXFIO_BUFTYPE_MEM = 1
    UXFIO_BUFTYPE_FILE = 2
    UXFIO_BUFTYPE_DYNAMIC_MEM = 3
    UXFIO_BUFTYPE_VEOF = 4
    UXFIO_RET_EFAIL = -9999
    UXFIO_RET_EBADF = -9998
    UXFIO_RDONLY = 0
    UXFIO_WRONLY = 1
    UXFIO_RDWR = 2
    UXFIO_SEEK_VCUR = 3081
    UXFIO_SEEK_VSET = 4082
    UXFIO_F_SETBL = -11725
    UXFIO_F_SET_INITTR = -11726
    UXFIO_F_SET_DISATR = -11727
    UXFIO_F_GET_BYTETR = -11728
    UXFIO_F_SET_CANSEEK = -11729
    UXFIO_F_SET_LTRUNC = -11730
    UXFIO_F_SET_BUFACTIVE = -11731
    UXFIO_F_SET_BUFTYPE = -11732
    UXFIO_F_ATTACH_FD = -11733
    UXFIO_F_SET_VEOF = -11734
    UXFIO_F_ARM_AUTO_DISABLE = -11735
    UXFIO_F_GET_BUFTYPE = -11736
    UXFIO_F_GET_CANSEEK = -11737
    UXFIO_F_WRITE_INSERT = -11738
    UXFIO_F_SET_BUFFER_LENGTH = -11725
    UXFIO_F_GET_BUFFER_LENGTH = -11739
    UXFIO_F_GET_VBOF = -11740
    UXFIO_F_DO_MEM_REALLOC = -11741
    UXFIO_F_SET_LOCK_MEM_FATAL = -11742
    UXFIO_F_SET_OUTPUT_BLOCK_SIZE = -11743
    UXFIO_F_GET_VEOF = -11744
    UXFIO_IOCTL_SET_STATBUF = -12725
    UXFIO_IOCTL_GET_STATBUF = -12726
    UXFIO_IOCTL_SET_TMPFILE_ROOTDIR = -12727
    UXFIO_IOCTL_SET_IMEMBUF = -12728

    UXFIO_TMPFILE_ROOTDIR = "/usr/tmp/"
    UXFIO_TMPFILE_PFX = "uxfio"
    UXFIO_NULL_FD = -10001
    UXFIONEEDDEBUG = 1
    UXFIONEEDFAIL = None
    UXFIO_E_FAIL = None
    UXFIO_E_FAIL2 = None
    UXFIO_E_FAIL3 = None

    UXFIO_E_DEBUG = None
    sys.stderr.write = None
    sys.stderr.write = None

    UXFIO_REMOVE = 0
    UXFIO_ADD = 1
    UXFIO_FIND = 2
    UXFIO_FINDINDEX = 3
    UXFIO_INIT = 4
    UXFIO_QUERY_NOPEN = 5
    UXFIO_BUFFER_CMD_GET = 0
    UXFIO_BUFFER_CMD_SET = 1
    UXFIO_ALLOC_AHEAD = 20480
    UXFIO_I_NULL_FD = -10001


uc = UxfioConst

Xint = int
uxg_status_table = [] * uc.UXFIO_MAX_OPEN
uxg_desc_table = [] * uc.UXFIO_MAX_OPEN
uxg_file_table = [] * uc.UXFIO_MAX_OPEN
uxg_did_init = 0
uxg_nullfd = 0


class UXFIO:
    def __init__(self):
        self.uxfd = 0  # unix file descriptor
        self.buffertype = 0  # memory segment, disk file, dynamic (growable) memory
        self.uxfd_can_seek = 0  # 1 if seekable file
        self.pos = 0  # pointer in buf , always >=start AND <end
        self.start = 0  # beginning offset in buf of valid data, always 0
        self.end = 0  # pointer offset of first byte of invalid data
        self.len = 0  # length of buffer, memory length, or, maximum file length
        self.error = 0  # error
        self.bytesread = 0  # bytes read on virtual fd
        self.current_offset = 0  # current offset in physical/logical file.
        self.virtual_offset = 0  # current offset in physical/logical file.
        self.buffer_active = 0  # 1 if any buffer type is in use
        self.buf = ""  # stream buffer for buffer type 0
        self.buffd = 0  # file buffer for buffer type 1
        self.offset_eof_saved = 0  # saved value of offset_eof
        self.offset_eof = 0  # virtual end-of marker for the self_read function
        self.offset_bof = 0  # virtual beginning of file.
        self.auto_disable = 0  # if a subsequent read() will read from uxfd entirely, set buffer_active to 0
        self.auto_arm_delay = 0  # only disable buffer if auto_disable is set AND have previously read from buffer
        self.write_insert = 0  # true if inserting into a file. see UXFIO_F_WRITE_INSERT
        self.uxfio_mode = 0
        self.v_end = 0  # length of file
        self.use_count = 0
        self.tmpfile_rootdir = ""
        self.buffilename = "" * 128
        self.statbuf = ""
        self.did_dupe_fd = 0
        self.vir_fsync = None
        self.vir_read = None
        self.vir_tread_read = None
        self.vir_write = None
        self.vir_ftruncate = None
        self.vir_close = None
        self.vir_lseek = None
        self.uxfio_fildes = 0  # Uxfio descriptor of this structure
        self.lock_buf_fatal = 0  # If realloc shifts the address of the file image, then exit
        self.output_block_size = 0
        self.output_buffer_c = 0  # output buffer data length
        self.output_buffer = 0


def internal_uxfio_close(uxfio):
    if uxfio.use_count > 1:
        uxfio.use_count -= 1
        return -1
    if uxfio.buf:
        del uxfio.buf
    if uxfio.statbuf:
        del uxfio.statbuf
        uxfio.statbuf = None
    uxfio__delete_buffer_file(uxfio)
    if uxfio.tmpfile_rootdir:
        del uxfio.tmpfile_rootdir
    return uxfio.uxfd

def bufferstate(uxfio, left, right, unused):
    if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
        if os.lseek(uxfio.buffd, 0, os.SEEK_CUR) != uxfio.pos:
            sys.stderr.write("uxfio internal error: buffer file position.")
    uxfio.pos = left
    uxfio.end = uxfio.pos - right
    uxfio.len = uxfio.end - unused
    return 0

def uxfio__delete_buffer_file(uxfio):
    if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
        if uxfio.buffd < 0 or not uxfio.buffilename:
            return -1
        os.close(uxfio.buffd)
        uxfio__unlink_tmpfile(uxfio)
        uxfio.buffd = -1
        uxfio.buffilename = ''
        return 0
    else:
        return 0

def uxfio__init_buffer_file(uxfio, buf, length):
    default_rootdir = uc.UXFIO_TMPFILE_ROOTDIR
    print("ENTERING")
    if uxfio.buffd > 0 and uxfio.buffilename:
        print("already have tmpfile")
        return 0
    if (uxfio.buffd < 0 and uxfio.buffilename) or (uxfio.buffd > 0 and not uxfio.buffilename):
        print("invalid condition")
        return -1
    if uxfio.tmpfile_rootdir:
        rootdir = uxfio.tmpfile_rootdir
    else:
        rootdir = default_rootdir
    dirtmp = rootdir + uc.UXFIO_TMPFILE_PFX + "XXXXXX"
    if len(dirtmp) - 1 > len(uxfio.buffilename):
        uxfio.buffd = -1
        print("temp file name too long.")
        return -1
    else:
        uxfio.buffilename = dirtmp
    fd, uxfio.buffilename = tempfile.mkstemp()
    print("Making tmpfile:", uxfio.buffilename)
    if fd < 0:
        sys.stderr.write("error")
        sys.stderr.write("mkstemp failed:" % os.error)
        sys.exit()
    uxfio.buffd = fd
    uxfio.len = uc.UXFIO_FLEN
    uxfio.start = uxfio.end = uxfio.pos = 0
    if buf and length > 0:
        print("fd =", uxfio.buffd, "len =", length)
        ret = uxfio_unix_atomic_write(uxfio.buffd, buf, length)
        if ret != length:
            os.close(uxfio.buffd)
            if uxfio__unlink_tmpfile(uxfio):
                sys.stderr.write("error")
                sys.stderr.write("uxfio error: error unlinking file" + uxfio.buffilename)
            sys.stderr.write("fatal, write failed on" + uxfio.buffilename)
            exit()
        else:
            uxfio.start = 0
            uxfio.end = length
            uxfio.virtual_offset = 0
            uxfio.pos = 0
            if os.lseek(uxfio.buffd, uxfio.pos, os.SEEK_SET) < 0:
                print(strerror(os.strerror))
            print("uxfio->end =", uxfio.end)
    return 0

def enforce_veof(uxfio, nbyte, einval):
    remaining = 0
    if einval:
        einval[0] = 0
    if virtual_eof_active(uxfio):
        print("virtual eof active")
        if uxfio.offset_eof >= 0:
            remaining = uxfio.offset_eof - uxfio.virtual_offset
        elif uxfio.v_end >= 0:
            remaining = uxfio.v_end - uxfio.current_offset
    else:
        uxfio.offset_eof_saved = -1
        return nbyte
    if remaining <= nbyte:
        print("setting einval")
        einval[0] = 1
        print("enforcing veof", remaining)
        return remaining
    else:
        print("no end found. returning", nbyte)
        return nbyte

def enforce_set_position(uxfio, offset, einval):
    passed = uxfio.bytesread
    einval[0] = 0
    print("offset =", offset)
    print("passed =", passed)
    if offset >= 0:
        print("returning offset =", offset)
        return offset
    if virtual_eof_active(uxfio):
        if offset < 0:
            print()
            if not passed:
                if uxfio.offset_eof >= 0:
                    print()
                    passed = uxfio.offset_eof
                elif uxfio.v_end >= 0:
                    print()
                    passed = uxfio.v_end
                else:
                    passed = 0
                    print("error")
                    print("uxfio warning: internal exception enforce_set_position()")
            if -offset > passed:
                print()
                einval[0] = 1
                print("returning 0")
                return 0
    print("returning", offset)
    return offset

def uxfio__change_state(uxfio, u):
    if u:
        uxfio.vir_fsync = uxfio_fsync
        uxfio.vir_read = uxfio_read
        uxfio.vir_tread_read = uxfio_sfread
        uxfio.vir_write = uxfio_write
        uxfio.vir_close = uxfio_close
        uxfio.vir_lseek = uxfio_lseek
        uxfio.vir_ftruncate = uxfio_ftruncate
    else:
        uxfio.vir_fsync = os.fsync
        uxfio.vir_read = uxfio_unix_safe_read
        uxfio.vir_tread_read = uxfio_unix_safe_atomic_read
        uxfio.vir_write = uxfio_unix_safe_write
        uxfio.vir_close = os.close
        uxfio.vir_lseek = os.lseek
        uxfio.vir_ftruncate = os.ftruncate

def uxfio__get_state(uxfio):
    if uxfio.vir_fsync == uxfio_fsync:
        return 1
    else:
        return 0

def uxfio__unlink_tmpfile(uxfio):
    ret = 0
    print("unlink tmpfile:", uxfio.buffilename)
    try:
        os.unlink(uxfio.buffilename)
    except OSError as e:
        ret = -1
        if e.errno != errno.ENOENT:
            print("error")
            print("uxfio error: error unlinking file", uxfio.buffilename)
    return ret

def do_buffered_flush(uxfio):
    save_bs = uxfio.output_block_size
    if uxfio.output_block_size == 0 or uxfio.output_buffer_c == 0:
        return 0
    remains = uxfio.output_buffer_c
    uxfio.output_buffer_c = 0
    uxfio.output_block_size = remains
    if remains > save_bs:
        # print("internal flush error: do_buffered_flush2: line={}".format(__LINE__), file=sys.stderr)
        print("internal flush error: do_buffered_flush2: line={}", file=sys.stderr)
        return -1
    write_ret = do_buffered_write2(uxfio, uxfio.output_buffer, remains)
    if write_ret != remains:
        uxfio.output_block_size = save_bs
        # print("flush error: do_buffered_flush2: line={}".format(__LINE__), file=sys.stderr)
        print("flush error: do_buffered_flush2: line={}", file=sys.stderr)
        return -1
    if uxfio.output_buffer_c > 0:
        uxfio.output_block_size = save_bs
        # print("flush error: do_buffered_flush2: line={}".format(__LINE__), file=sys.stderr)
        print("flush error: do_buffered_flush2: line={}", file=sys.stderr)
        return -1
    uxfio.output_block_size = save_bs
    return 0

def virtual_eof_active(uxfio):
    return uxfio.offset_eof >= 0 or uxfio.v_end >= 0 or 0

def virtual_file_active(uxfio):
    return uxfio.offset_eof >= 0 or uxfio.offset_eof_saved >= 0 or 0

def active_virtual_file_offset(uxfio):
    if virtual_file_active(uxfio):
        return uxfio.offset_bof
    else:
        return 0

def do_buffered_write2(uxfio, userbuf, nbyte):
    buf = uxfio.output_buffer
    bs = uxfio.output_block_size
    initial_remains = uxfio.output_buffer_c
    userp = userbuf
    bufp = buf + initial_remains
    remains = initial_remains + nbyte
    user_remains = nbyte
    while remains >= bs:
        udid = bs - initial_remains
        bufp[:udid] = userp
        sub_p = buf
        sub_remains = bs
        write_ret = 0
        while sub_remains > 0:
            sub_write_ret = uxfio_write(uxfio.uxfd, sub_p, sub_remains)
            if sub_write_ret < 0:
                write_ret = -1
                break
            elif sub_write_ret == 0:
                write_ret = -1
                break
            else:
                write_ret += sub_write_ret
                sub_remains -= sub_write_ret
                sub_p += sub_write_ret
        if write_ret < 0:
            return -1
        elif write_ret != bs:
            uxfio.output_buffer_c = bs
            return -1
        remains -= write_ret
        user_remains -= udid
        userp += udid
        bufp = buf
        initial_remains = 0
    bufp[:user_remains] = userp
    uxfio.output_buffer_c = remains
    return nbyte

def uxfio_common_read(uxfio, uxfio_fildes, buf, nbyte_, pollingread):
    p = buf
    nbyte = nbyte_
    einval = 0
    left = None
    right = None
    unused = None

    sys.stderr.write("ENTERING fildes = %d  nbyte=%d" % (uxfio_fildes, nbyte))
    if uxfio is None:
        if table_find(uxfio_fildes, uxfio, 1):
            sys.stderr.write("error %d" % uxfio_fildes)
            print("uxfio error [%d]: uxfio_common_read: file desc %d not found.\n" % (fr.__LINE__, uxfio_fildes))
            sys.stderr.write("LEAVING")
            return -1
    else:
        uxfio_fildes = uxfio_fildes
    if uxfio.buffertype == UXFIO_BUFTYPE_FILE or uxfio.buffertype == UXFIO_BUFTYPE_DYNAMIC_MEM or uxfio.offset_eof >= 0 or uxfio.v_end >= 0:
        sys.stderr.write("running enforce_veof")
        nbyte, einval = enforce_veof(uxfio, nbyte, einval)
        sys.stderr.write("enforce_veof  nbyte(now) = %d" % nbyte)
    sys.stderr.write("einval = %d" % einval)
    if nbyte <= 0 and einval:
        sys.stderr.write("FOUND VEOF")
        sys.stderr.write("nbyte = %d einval = %d" % (nbyte, einval))
        if uxfio.offset_eof >= 0:
            sys.stderr.write("reseting eof")
            uxfio.offset_eof = -uxfio.offset_eof
            uxfio.current_offset = 0
            uxfio.virtual_offset = 0
        if nbyte <= 0:
            sys.stderr.write("LEAVING fildes=%d" % uxfio_fildes)
            return 0
    if uxfio.buffer_active == 0:
        sys.stderr.write("buffer_active is zero")
        if pollingread and (uxfio.uxfd_can_seek == 0 or uxfio__get_state(uxfio)):
            sys.stderr.write("fildes=%d foo_read is tread." % uxfio_fildes)
            foo_read = uxfio.vir_tread_read
        else:
            sys.stderr.write("fildes=%d foo_read is read." % uxfio_fildes)
            foo_read = uxfio.vir_read
        sys.stderr.write("fildes=%d uxfd=%d " % (uxfio_fildes, uxfio.uxfd))
        sys.stderr.write("fildes=%d nbyte=%d " % (uxfio_fildes, nbyte))
        n = foo_read(uxfio.uxfd, buf, nbyte)
        if n < 0:
            sys.stderr.write("LEAVING fildes=%d ret = %d" % (uxfio_fildes, n))
            print("uxfio: read failed at line %d uxfio_fildes=[%d] uxfd=[%d] return value=[%d]\n" % (
                fr.__LINE__, uxfio_fildes, uxfio.uxfd, n))
            if foo_read == uxfio_unix_safe_read:
                print("uxfio_common_read: fd=%d  %s\n" % (uxfio.uxfd, os.error()))
            return n
        uxfio.bytesread += n
        uxfio.current_offset += n
        uxfio.virtual_offset += n
        sys.stderr.write("LEAVING fildes=%d ret = %d" % (uxfio_fildes, n))
        return n
    sys.stderr.write("posM = %d" % uxfio.pos)
    # oldpos = uxfio.pos
    bufferstate(uxfio, left, right, unused)
    if uxfio.pos < uxfio.end:
        bufcount = right if nbyte >= right else nbyte
        if uxfio.buffertype == UXFIO_BUFTYPE_FILE:
            n = uxfio_unix_atomic_read(uxfio.buffd, p, bufcount)
            if n != bufcount:
                sys.stderr.write("error %d" % uxfio_fildes)
                print("uxfio internal error: file buffer read error\n")
                if n > 0:
                    os.lseek(uxfio.buffd, -n, os.SEEK_CUR)
                sys.stderr.write("LEAVING fildes=%d" % uxfio_fildes)
                return -1
        else:
            uxfio__memcpy(p, 0, uxfio.buf, uxfio.pos, bufcount, uxfio)
        uxfio.current_offset += bufcount
        uxfio.virtual_offset += bufcount
        if bufcount < nbyte:
            if pollingread and (uxfio.uxfd_can_seek == 0 or uxfio__get_state(uxfio)):
                sys.stderr.write("fildes=%d foo_read is tread." % uxfio_fildes)
                foo_read = uxfio.vir_tread_read
            else:
                sys.stderr.write("fildes=%d foo_read is read." % uxfio_fildes)
                foo_read = uxfio.vir_read
            if uxfio.uxfd != UXFIO_NULL_FD:
                n = foo_read(uxfio.uxfd, p + bufcount, nbyte - bufcount)
            else:
                n = 0
            if n < 0:
                print("uxfio: read failed line=%d\n" % fr.__LINE__)
                return n
            uxfio.bytesread += n
            uxfio.current_offset += n
            uxfio.virtual_offset += n
            if fix_buffer(uxfio_fildes, uxfio, bufcount + n, p, n):
                sys.stderr.write("error %d" % uxfio_fildes)
                print("uxfio internal error: 10004.0 \n")
            return bufcount + n
        else:
            if fix_buffer(uxfio_fildes, uxfio, bufcount, p, 0):
                sys.stderr.write("error %d" % uxfio_fildes)
                print("uxfio internal error: 10004.1 \n")
            sys.stderr.write("LEAVING fildes=%d" % uxfio_fildes)
            return bufcount
    else:
        if pollingread and uxfio.uxfd_can_seek == 0 or uxfio__get_state(uxfio):
            sys.stderr.write("fildes=%d foo_read is tread." % uxfio_fildes)
            foo_read = uxfio.vir_tread_read
        else:
            sys.stderr.write("fildes=%d foo_read is read." % uxfio_fildes)
            foo_read = uxfio.vir_read
        if uxfio.uxfd != UXFIO_NULL_FD:
            n = foo_read(uxfio.uxfd, p, nbyte)
        else:
            n = 0
        if n < 0:
            return n
        uxfio.bytesread += n
        uxfio.current_offset += n
        uxfio.virtual_offset += n
        if uxfio.fix_buffer(uxfio_fildes, uxfio, n, p, n):
            sys.stderr.write("error %d" % uxfio_fildes)
            print("uxfio internal error: 10004.2 \n")
        if uxfio.auto_disable:
            uxfio.auto_disable = 0
            if uxfio_fcntl(uxfio_fildes, uc.UXFIO_F_SET_BUFACTIVE, uc.UXFIO_OFF):
                sys.stderr.write("error %d" % uxfio_fildes)
                print("uxfio internal error: auto_disable error\n")
        sys.stderr.write("LEAVING fildes=%d ret=%d" % (uxfio_fildes, n))
        return n
    # This code is unreachable:
    # sys.stderr.write("error %d"% uxfio_fildes)
    # sys.stderr.write("LEAVING ret=%d"% uc.UXFIO_RET_EFAIL)
    # return UXFIO_RET_EFAIL

def uxfio_common_safe_read(uxfio, fd, buf, nbyte):
    n = 0
    nret = 1
    p = buf
    while n < nbyte and nret:
        nret = uxfio_common_read(uxfio, fd, p + n, nbyte - n, 0)
        if nret < 0:
            return nret
        n += nret
    return n

def uxfio_sfread(uxfio_fildes, buf, nbyte):
    sys.stderr.write("uxfio_filedes=%d" + uxfio_fildes)
    if uxfio_fildes < UXFIO_FD_MIN:
        return uxfio_unix_safe_read(uxfio_fildes, buf, nbyte)
    else:
        return uxfio_common_safe_read(None, uxfio_fildes, buf, nbyte)

def uxfio_read(uxfio_fildes, buf, nbyte):
    if uc.UXFIO_FD_MIN > uxfio_fildes >= 0:
        #ret = uxfio_unix_safe_read(uxfio_fildes, buf, nbyte)
        ret = os.preadv(uxfio_fildes, buf, nbyte)
        if ret < 0:
            sys.stderr.write("uxfio_read fd={}: {}".format(uxfio_fildes, os.error()))
        return ret
    elif uxfio_fildes >= uc.UXFIO_FD_MIN:
        # return uxfio_common_read(uxfio_fildes, buf, nbyte, 0)
        return os.preadv(uxfio_fildes, buf, nbyte, 0)
    else:
        return -1

def uxfio_unix_safe_read(fd, buf, nbyte):
    if uc.UXFIO_FD_MIN > fd >= 0:
        return safeio(os.read, fd, buf, nbyte)
    else:
        return safeio(uxfio_read, fd, buf, nbyte)

def uxfio_espipe(uxfio_fildes):
    uxfio = UXFIO()
    if uxfio_fildes < uc.UXFIO_FD_MIN:
        if os.lseek(uxfio_fildes, 0, os.SEEK_CUR) == -1:
            return -1 if errno != 0 else 0
        else:
            return 0
    if table_find(uxfio_fildes, uxfio, 1):
        return -1
    ret = 0 if uxfio.uxfd_can_seek else errno.ESPIPE
    if ret == 0:
        ret = uxfio_fcntl(uxfio_fildes, uc.UXFIO_F_GET_CANSEEK, 0)
        if ret:
            ret = 0
        else:
            ret = errno.ESPIPE
    return ret

def uxfio_getfd(uxfio_fildes, use_next):
    uxfio = UXFIO()
    if uxfio_fildes < uc.UXFIO_FD_MIN:
        return uxfio_fildes
    if table_find(uxfio_fildes, uxfio, 1):
        return -1
    if use_next:
        if uxfio.buffer_active and uxfio.pos < uxfio.end:
            use_next = 0
        else:
            use_next = 1
    return uxfio.uxfd, use_next

def uxfio_ftruncate(uxfio, uxfio_fildes, nbyte):
    if uc.UXFIO_FD_MIN > uxfio_fildes >= 0:
        return os.ftruncate(uxfio_fildes, nbyte)
    elif uxfio_fildes >= uc.UXFIO_FD_MIN:
        return uxfio_internal_ftruncate(uxfio, uxfio_fildes, nbyte)
    else:
        return -1

def uxfio_sfa_read(uxfio, uxfio_fildes, buf, nbyte):
    print("uxfio_filedes=%d", uxfio_fildes)
    if uxfio_fildes < UXFIO_FD_MIN:
        return uxfio_unix_atomic_read(uxfio_fildes, buf, nbyte)
    else:
        return uxfio_common_safe_read(uxfio, uxfio_fildes, buf, nbyte)

def uxfio_internal_ftruncate(uxfio, uxfio_fildes, nbyte):
    global UXFIO_BUFTYPE_FILE, UXFIO_BUFTYPE_DYNAMIC_MEM

    # uxfio = UXFIO()

    if table_find(uxfio_fildes, uxfio, 1):
        sys.stderr.write("uxfio error: file desc {} not found.".format(uxfio_fildes))
        return -1
    if uxfio.statbuf and uxfio.v_end > 0:
        if nbyte > uxfio.v_end:
            uxfio.v_end = int(nbyte)
            uxfio.statbuf.st_size = nbyte
            if uxfio.offset_eof > nbyte:
                uxfio.offset_eof = int(nbyte)
            if uxfio.bytesread > nbyte:
                uxfio.bytesread = int(nbyte)
                uxfio.current_offset = int(nbyte)
                uxfio.virtual_offset = int(nbyte)
        return 0
    elif uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
        if uxfio.pos >= uxfio.end:
            return os.ftruncate(uxfio.buffd, 0)
        else:
            return -2
    elif uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM:
        # left, right, unused = bufferstate(uxfio)
        pos = uxfio_lseek(uxfio_fildes, 0, io.SEEK_CUR)
        if pos < 0:
            return -1
        if nbyte > uxfio.end:
            # buf = ['\0'] * 512
            if pos < 0:
                return -1
            if uxfio_lseek(uxfio_fildes, 0, io.SEEK_END) < 0:
                return -1
            # buf = ['\0'] * 512
            amount = int(nbyte - uxfio.end)
            ret = zero_fill(uxfio_fildes, amount)
            if uxfio_lseek(uxfio_fildes, pos, io.SEEK_SET) < 0:
                return -1
        else:
            amount = int(uxfio.end - nbyte)
            if uxfio_lseek(uxfio_fildes, nbyte, io.SEEK_SET) < 0:
                return -1
            ret = zero_fill(uxfio_fildes, amount)
            if uxfio_lseek(uxfio_fildes, pos, io.SEEK_SET) < 0:
                return -1
            if nbyte > uxfio.pos:
                uxfio_lseek(uxfio_fildes, nbyte, io.SEEK_SET)
            uxfio.end = nbyte
        return ret
    return -1

def uxfio__read_bytes(uxfio_fildes, amount):
    print("uxfio__read_bytes is not used" % uxfio_fildes, amount)
    return 0

def uxfio__close(uxfio_fildes, closeit):
    uxfio = UXFIO()
    # fd = 0
    # ret = 0
    if uxfio_fildes == uxg_nullfd:
        return 0
    if uc.UXFIO_FD_MIN > uxfio_fildes >= 0:
        return os.close(uxfio_fildes)
    if table_find(uxfio_fildes, uxfio, 1):
        return -1
    if uxfio.did_dupe_fd >= 0:
        uxfio_decr_use_count(uxfio.did_dupe_fd)
    fd = internal_uxfio_close(uxfio)
    if fd < 0 and fd != uc.UXFIO_NULL_FD:
        print("file still in use: filedes=", uxfio_fildes, "use_count=", uxfio.use_count)
        if closeit:
            print("filedes=%d" % uxfio_fildes)
            return 0
        else:
            print("filedes=%d" % uxfio_fildes)
            return -1
    elif fd >= 0:
        if closeit:
            print("filedes=%d" % uxfio_fildes)
            #ret = do_buffered_flush(uxfio)
            ret = uxfio.vir_close(fd)
        else:
            print("filedes=%d" % uxfio_fildes)
            ret = fd
        table_mon(uc.UXFIO_REMOVE, uxfio_fildes, uxfio)
        return ret
    else:
        table_mon(uc.UXFIO_REMOVE, uxfio_fildes, uxfio)
        return 0

def uxfio_close(uxfio_fildes):
    ret = uxfio__close(uxfio_fildes, 1)
    if ret < 0:
        return -1
    else:
        return 0

def uxfio_free(uxfio_fildes):
    if uc.UXFIO_FD_MIN > uxfio_fildes >= 0:
        return 0
    if uxfio_fcntl(uxfio_fildes, uc.UXFIO_F_SET_BUFACTIVE, 0) < 0:
        sys.stderr.write("error in uxfio_free")
        return -1
    return uxfio__close(uxfio_fildes, 0)

def uxfio_opendup(uxfd, buffertype):
    uxfio = UXFIO()
    uxfio_fd = uxfio_open(uxfd, os.O_RDONLY, 0)
    if uxfio_fd < 0:
        return uxfio_fd
    r = uxfio_fcntl(uxfio_fd, uc.UXFIO_F_ATTACH_FD, uxfd)
    if r < 0:
        return -1
    if table_find(uxfio_fd, uxfio, 1):
        print("uxfio error: file desc {} not found.".format(uxfio_fd))
        return -1
    if uxfd >= uc.UXFIO_FD_MIN:
        uxfd_uxfio = uxfio_get_object_address(uxfd)
        uxfio_incr_use_count(uxfd)
        if uxfio_lseek(uxfd, 0, os.SEEK_CUR) == 0 or (uxfd_uxfio and (uxfd_uxfio.uxfd_can_seek or (
                uxfd_uxfio.buffer_active and (
                uxfd_uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE or uxfd_uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM or 0)))):
            uxfio.uxfd_can_seek = 1
        else:
            uxfio.uxfd_can_seek = 0
    else:
        if (os.lseek(uxfd, 0, os.SEEK_CUR) == -1) and (errno == errno.ESPIPE):
            uxfio.uxfd_can_seek = 0
        else:
            uxfio.uxfd_can_seek = 1
    if buffertype == uc.UXFIO_BUFTYPE_FILE:
        r = uxfio_fcntl(uxfio_fd, uc.UXFIO_F_SET_BUFTYPE, uc.UXFIO_BUFTYPE_FILE)
        if r < 0:
            uxfio_close(uxfio_fd)
            return -1
    elif buffertype == uc.UXFIO_BUFTYPE_MEM:
        r = uxfio_fcntl(uxfio_fd, uc.UXFIO_F_SET_BUFTYPE, uc.UXFIO_BUFTYPE_MEM)
        if r < 0:
            uxfio_close(uxfio_fd)
            return -1
    elif buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM:
        r = uxfio_fcntl(uxfio_fd, uc.UXFIO_F_SET_BUFTYPE, uc.UXFIO_BUFTYPE_DYNAMIC_MEM)
        if r < 0:
            uxfio_close(uxfio_fd)
            return -1
    else:
        bufon = uc.UXFIO_OFF
        r = uxfio_fcntl(uxfio_fd, uc.UXFIO_F_SET_BUFACTIVE, bufon)
    if r < 0:
        uxfio_close(uxfio_fd)
        return -1
    return uxfio_fd

def uxfio_lseek(uxfio_fildes, poffset, pwhence):
    uxfio = UXFIO()
    n = 0
    left = 0
    right = 0
    unused = 0
    einval = 0
    retval = -1
    offset = 0
    do_virtual_set = 0
    whence = pwhence

    if uc.UXFIO_FD_MIN > uxfio_fildes >= 0:
        if whence == uc.UXFIO_SEEK_VCUR:
            whence = os.SEEK_CUR
        return os.lseek(uxfio_fildes, poffset, whence)

    if table_find(uxfio_fildes, uxfio, 1):
        print("uxfio error: file desc", uxfio_fildes, "not found.")
        return -1

    if whence == os.SEEK_CUR or whence == uc.UXFIO_SEEK_VCUR:
        offset = poffset
    elif whence == os.SEEK_SET or whence == uc.UXFIO_SEEK_VSET:
        if (
                virtual_eof_active(uxfio) == 0 and
                uxfio.offset_eof_saved >= 0 and
                pwhence == uc.UXFIO_SEEK_VSET
        ):
            if (
                    uxfio.pos - uxfio.offset_eof_saved == uxfio.offset_bof and
                    uxfio.offset_eof_saved >= 0
            ):
                uxfio.pos = uxfio.offset_bof
                uxfio.virtual_offset = 0
                uxfio.offset_eof = uxfio.offset_eof_saved
            else:
                print("uxfio: unsupported case")
                return -1
        if (
                not virtual_eof_active(uxfio) and
                uxfio.offset_eof_saved > 0 and
                pwhence == uc.UXFIO_SEEK_VSET
        ):
            whence = os.SEEK_CUR
            offset = poffset
            offset -= uxfio.offset_eof_saved
            uxfio.offset_eof = uxfio.offset_eof_saved
            do_virtual_set = 1
    elif whence == os.SEEK_END:
        offset = uxfio.end - uxfio.pos + poffset
        whence = os.SEEK_CUR
        if virtual_eof_active(uxfio):
            r_i = uxfio.offset_eof if uxfio.offset_eof >= 0 else uxfio.v_end
            offset = r_i - uxfio.bytesread + offset
            r_offset = offset
            offset = enforce_veof(uxfio, r_offset, einval)
            if offset < 0 or (einval and (r_offset - offset)):
                print("uxfio internal exception: negative offset")
                return -1
        elif not uxfio.buffer_active and uxfio.uxfd_can_seek:
            print("uxfio internal exception: broken case")

    if offset:
        whence = os.SEEK_CUR

    bufferstate(uxfio, left, right, unused)

    if offset < 0 and (-offset <= left):
        uxfio.pos += offset
        uxfio.current_offset += offset
        uxfio.virtual_offset += offset
        if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
            if os.lseek(uxfio.buffd, offset, whence) != uxfio.pos:
                return -1
        retval = uxfio.pos
    elif offset > 0 and (offset <= right):
        if virtual_eof_active(uxfio):
            if pwhence == os.SEEK_SET:
                print("uxfio_lseek: new lseek error")
                return -1
            elif pwhence == uc.UXFIO_SEEK_VSET:
                uxfio.pos = uxfio.offset_bof + poffset
                uxfio.virtual_offset = poffset
                retval = 0
            elif pwhence == os.SEEK_CUR:
                uxfio.pos += poffset
                uxfio.current_offset += poffset
                uxfio.virtual_offset += poffset
                retval = uxfio.pos
        else:
            uxfio.pos += offset
            uxfio.current_offset += offset
            uxfio.virtual_offset += offset
            if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
                if os.lseek(uxfio.buffd, offset, whence) != uxfio.pos:
                    return -1
            retval = uxfio.pos
    elif offset > 0 and (offset > right):
        if uxfio.v_end >= 0:
            n = uxfio.v_end
            uxfio.pos += n
            uxfio.current_offset += n
            uxfio.virtual_offset += n
            retval = uxfio.pos
            return retval
        elif uxfio.buffertype != uc.UXFIO_BUFTYPE_FILE and not uxfio.buffer_active:
            return -1
        if (n == uxfio__read_bytes(uxfio_fildes, offset)) != offset:
            uxfio.pos += n
            uxfio.current_offset += n
            uxfio.virtual_offset += n
        if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
            if uxfio.pos != os.lseek(uxfio.buffd, 0, os.SEEK_CUR):
                print("uxfio internal error: buffer file position error")
                return -1
        retval = uxfio.pos
    elif offset == 0:
        if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
            whence = os.SEEK_CUR
            if os.lseek(uxfio.buffd, offset, whence) != uxfio.pos:
                print("uxfio internal error: buffer file position error")
                return -1
            retval = uxfio.pos
        elif whence == uc.UXFIO_SEEK_VSET and uxfio.buffertype == uc.UXFIO_BUFTYPE_MEM:
            retval = uxfio.virtual_offset
        elif whence == uc.UXFIO_SEEK_VCUR and uxfio.buffertype == uc.UXFIO_BUFTYPE_MEM:
            retval = uxfio.current_offset
        elif (
                whence == uc.UXFIO_SEEK_VCUR and
                uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM
        ):
            retval = uxfio.pos - uxfio.offset_bof
        elif (
                whence == uc.UXFIO_SEEK_VSET and
                uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM and
                virtual_eof_active(uxfio)
        ):
            retval = uxfio.offset_bof
            uxfio.pos = uxfio.offset_bof
            uxfio.virtual_offset = uxfio.pos
        else:
            retval = uxfio.pos
    else:
        return uc.UXFIO_RET_EFAIL

    if do_virtual_set:
        retval -= active_virtual_file_offset(uxfio)

    return retval

def uxfio_fsync(uxfio_fildes):
    uxfio = UXFIO()
    #    i = 0
    if uc.UXFIO_FD_MIN > uxfio_fildes >= 0:
        return os.fsync(uxfio_fildes)
    if table_find(uxfio_fildes, uxfio, 1):
        sys.stderr.write("error")
        sys.stderr.write("uxfio error [{}]: file desc {} not found.\n".format(fr.__LINE__, uxfio_fildes))
        return -1
    if uxfio.output_block_size > 0:
        i = do_buffered_flush(uxfio)
    elif uxfio.uxfd_can_seek:
        i = uxfio.vir_fsync(uxfio.uxfd)
    elif uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE and uxfio.buffd >= 0:
        i = uxfio.vir_fsync(uxfio.buffd)
    else:
        return -1
    return i

def uxfio_get_dynamic_buffer(uxfio_fildes, buffer_ptr, buffer_end, buffer_len):
    uxfio = UXFIO()
    if table_find(uxfio_fildes, uxfio, 1):
        sys.stderr.write("uxfio error: file desc" % uxfio_fildes % "not found.")
        return -1
    if uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM:
        uxfio.buf.len = '\0'
        uxfio.buf.end = '\0'
        if buffer_ptr:
            buffer_ptr = uxfio.buf
        if buffer_len:
            curpos = uxfio_lseek(uxfio_fildes, 0, io.SEEK_CUR)
            uxfio_lseek(uxfio_fildes, 0, io.SEEK_END)
            buffer_len = uxfio_lseek(uxfio_fildes, 0, io.SEEK_CUR)
            uxfio_lseek(uxfio_fildes, curpos, io.SEEK_SET)
        if buffer_end:
            buffer_end = uxfio.end
        return 0
    else:
        return -1



def uxfio_ioctl(uxfio_fd, request, arg):
    uxfio = UXFIO()
    if table_find(uxfio_fd, uxfio, 1):
        sys.stderr.write("error")
        sys.stderr.write("uxfio error [{}]: file desc {} not found.\n".format(fr.__LINE__, uxfio_fd))
        return -1
    if request == uc.UXFIO_IOCTL_SET_STATBUF:
        if uxfio.statbuf:
            del uxfio.statbuf
        uxfio.statbuf = os.stat(uxfio.statbuf)
        copy_stat_members(uxfio.statbuf, arg)
        uxfio.v_end = uxfio.statbuf.__sizeof__()
        if uxfio.statbuf is None:
            sys.stderr.write("uxfio: line {}: out of memory.\n".format(fr.__LINE__))
            sys.exit(22)
        copy_stat_members(uxfio.statbuf, arg)
        uxfio.v_end = int(uxfio.statbuf.__sizeof__())
    elif request == uc.UXFIO_IOCTL_SET_IMEMBUF:
        uxfio.buf = arg
    elif request == uc.UXFIO_IOCTL_GET_STATBUF:
        arg = uxfio.statbuf
    elif request == uc.UXFIO_IOCTL_SET_TMPFILE_ROOTDIR:
        if uxfio.tmpfile_rootdir:
            del uxfio.tmpfile_rootdir
        uxfio.tmpfile_rootdir = str(arg)
    else:
        return -1
    return 0

def uxfio_fstat(uxfio, uxfio_fd, buf):
    # uxfio = UXFIO()
    if table_find(uxfio_fd, buf, 1):
        print("error")
        sys.stderr.write("uxfio error [{}]: file desc {} not found.\n".format(fr.__LINE__, uxfio_fd))
        return -1
    if uxfio.statbuf is None:
        return -1
    uxfio.copy_stat_members(buf, uxfio.statbuf)
    return 0

def uxfio_write(uxfio_fildes, buf, nbyte):
    # global UXFIO_NULL_FD, UXFIO_FD_MIN, UXFIO_BUFTYPE_DYNAMIC_MEM, UXFIO_BUFTYPE_FILE
    uxfio = UXFIO()
    #    n = 0
    einval = 0
    left, right, unused = 0, 0, 0
    # endoffset = 0
    if uxfio_fildes == UXFIO_NULL_FD:
        return nbyte
    elif UXFIO_FD_MIN > uxfio_fildes >= 0:
        return os.write(uxfio_fildes, buf)
    else:
        if table_find(uxfio_fildes, uxfio, 1):
            sys.stderr.write("uxfio error [{}]: file desc {} not found.\n".format(fr.__LINE__, uxfio_fildes))
            return -1
        if virtual_eof_active(uxfio):
            endoffset = uxfio.bytesread + nbyte
            if enforce_veof(uxfio, endoffset, einval) != endoffset:
                return -1
        if uxfio.write_insert and uxfio.buffertype != UXFIO_BUFTYPE_DYNAMIC_MEM:
            sys.stderr.write("uxfio warning: file insert only supported for UXFIO_BUFTYPE_DYNAMIC_MEM buffer.\n")
        if uxfio.buffer_active == 0 and uxfio.output_block_size == 0:
            return uxfio.vir_write(uxfio.uxfd, buf, nbyte)
        elif uxfio.output_block_size > 0 and uxfio.buffer_active == 0:
            n = do_buffered_write2(uxfio, buf, nbyte)
            return n
        elif uxfio.buffertype == UXFIO_BUFTYPE_FILE and uxfio.uxfd_can_seek == 1:
            nbyte = min(uxfio.pos + nbyte, uxfio.end) - uxfio.pos
            n = os.write(uxfio.buffd, buf[:nbyte])
            if n < 0:
                sys.stderr.write("exiting in uxfio_write")
                sys.exit()
            else:
                uxfio.pos += n
                uxfio.bytesread += n
                uxfio.current_offset += n
                uxfio.virtual_offset += n
                return n
        elif uxfio.buffertype == UXFIO_BUFTYPE_DYNAMIC_MEM:
            bufferstate(uxfio, left, right, unused)
            if nbyte > right + unused:
                if fix_dynamic_buffer(uxfio, uxfio, nbyte, buf, nbyte - right - unused):
                    sys.stderr.write("uxfio internal error: error returned by fix_dynamic_buffer.\n")
                    return -1
                return nbyte
            else:
                if uxfio.write_insert:
                    uxfio.buf[uxfio.pos + nbyte:uxfio.pos + nbyte + right] = uxfio.buf[uxfio.pos:uxfio.pos + right]
                    if uxfio.pos + nbyte + right > uxfio.end:
                        uxfio.end = uxfio.pos + nbyte
                uxfio.buf[uxfio.pos:uxfio.pos + nbyte] = buf
                uxfio.pos += nbyte
                uxfio.bytesread += nbyte
                uxfio.current_offset += nbyte
                uxfio.virtual_offset += nbyte
                if uxfio.pos > uxfio.end:
                    uxfio.end = uxfio.pos
                return nbyte
        else:
            return -1
    # return -1 # Code unreachable

def uxfio_fcntl(uxfio_fildes, cmd, value):
    uxfio = UXFIO()
    ret = 0
    left, right, unused = 0, 0, 0

    if uc.UXFIO_FD_MIN > uxfio_fildes >= 0:
        return fcntl.fcntl(uxfio_fildes, cmd, value)
    if table_find(uxfio_fildes, uxfio, 1):
        print("error")
        sys.stderr.write("uxfio error [{}]: file desc {} not found.\n".format(fr.__LINE__, uxfio_fildes))
        return -1
    if cmd == uc.UXFIO_F_SET_BUFFER_LENGTH:
        if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
            if value > uxfio.end:
                uxfio.len = value
        else:
            bufferstate(uxfio, left, right, unused)
            if value < right or value <= 0:
                return -1
            if value >= right + left:
                buf = uxfio__memcpy(0, uxfio.buf, uxfio.start, left + right, uxfio.end, uxfio)
                uxfio.len = value
            elif value >= right:
                # buf = uxfio__memcpy(0, uxfio.buf, uxfio.pos - value + right, value, uxfio, end)
                buf = uxfio__memcpy(0, 0, 0, 0, 0, uxfio)
                uxfio.len = value
                uxfio.start = 0
                uxfio.pos = value - right
                uxfio.virtual_offset = value - right
                uxfio.end = value
            else:
                return -1
            del uxfio.buf
            buf = ""
            uxfio.buf = buf
        return 0
    elif cmd == uc.UXFIO_F_SET_LTRUNC:
        return -1
    elif cmd == uc.UXFIO_F_SET_CANSEEK:
        uxfio.uxfd_can_seek = value
        return 0
    elif cmd == uc.UXFIO_F_GET_BUFFER_LENGTH:
        return uxfio.len
    elif cmd == uc.UXFIO_F_GET_VBOF:
        if virtual_file_active(uxfio):
            return uxfio.offset_bof
        else:
            return -1
    elif cmd == uc.UXFIO_F_GET_CANSEEK:
        if uxfio.buffer_active and uxfio.buffertype != uc.UXFIO_BUFTYPE_NOBUF:
            if uxfio.buffertype == uc.UXFIO_BUFTYPE_MEM:
                if uxfio.len >= 512:
                    return uxfio.len
            return 1
        return 0
    elif cmd == uc.UXFIO_F_GET_BUFTYPE:
        return uxfio.buffertype
    elif cmd == uc.UXFIO_F_SET_BUFACTIVE:
        ret = 0
        if uxfio.buffertype == uc.UXFIO_BUFTYPE_NOBUF:
            value = 0
        elif uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
            if not value:
                if uxfio.buffer_active and uxfio.pos < uxfio.end:
                    return -1
                ret = uxfio__delete_buffer_file(uxfio)
            elif value and not uxfio.buffer_active:
                ret = uxfio__init_buffer_file(uxfio,None, 0)
            else:
                print("in default else case in SET_BUFACTIVE")
        else:
            if value == 0 and uxfio.buffer_active and uxfio.pos < uxfio.end:
                ret = -1
        uxfio.buffer_active = 1 if value else 0
        return ret
    elif cmd == uc.UXFIO_F_SET_BUFTYPE:
        if value < 0 or value > 4:
            return -1
        if value != uxfio.buffertype:
            if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE and value != uc.UXFIO_BUFTYPE_FILE and uxfio.buffer_active:
                # aa = 0
                aa = uxfio.end + 2
                print("in uc.UXFIO_SET_BUFTYPE mem")
                print("in uc.UXFIO_SET_BUFTYPE nobuf")
                if uxfio.buf:
                    del uxfio.buf
                    uxfio.buf = None
                if uxfio.buffertype != uc.UXFIO_BUFTYPE_NOBUF:
                    uxfio.buf = uxfio.buf[:aa]
                    uxfio.len = uxfio.end + 1
                    if uxfio.buf is None:
                        sys.stderr.write("uxfio: line {}: out of memory.\n".format(fr.__LINE__))
                        sys.exit(22)
                    uxfio.len = uxfio.end + 1
                    os.lseek(uxfio.buffd, 0, os.SEEK_SET)
                    aa = os.read(uxfio.buffd, int(uxfio.buf[:uxfio.end]))
                ret = uxfio__delete_buffer_file(uxfio)
            elif uxfio.buffertype == uc.UXFIO_BUFTYPE_MEM or uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM and value == uc.UXFIO_BUFTYPE_FILE and uxfio.buffer_active:
                if not value or value >= uc.UXFIO_BUFTYPE_FILE:
                    uxfio.buffertype = uc.UXFIO_BUFTYPE_FILE
                else:
                    uxfio.buffertype = uc.UXFIO_BUFTYPE_MEM
                print("in uc.UXFIO_SET_BUFTYPE file")
                ret = uxfio__init_buffer_file(uxfio, uxfio.buf, uxfio.end)
            elif value == uc.UXFIO_BUFTYPE_NOBUF:
                print("in uc.UXFIO_SET_BUFTYPE nobuf")
                if uxfio_fcntl(uxfio_fildes, uc.UXFIO_F_SET_BUFACTIVE, uc.UXFIO_OFF):
                    print("error")
                    sys.stderr.write("uxfio internal error: 002.33\n")
                ret = 0
            else:
                print(" default else in set buftype")
            uxfio.buffertype = value
        else:
            ret = 0
        return ret
    elif cmd == uc.UXFIO_F_ATTACH_FD:
        uxfio.uxfd = value
        uxfio__change_state(uxfio, value >= uc.UXFIO_FD_MIN)
        return uxfio_fildes
    elif cmd == uc.UXFIO_F_GET_VEOF:
        if virtual_file_active(uxfio):
            return uxfio.offset_eof_saved
        else:
            return -1
    elif cmd == uc.UXFIO_F_SET_VEOF:
        print("fd={}  set VEOF value = {}".format(uxfio_fildes, value))
        if value < 0:
            uxfio.current_offset = 0
            uxfio.virtual_offset = 0
            uxfio.offset_eof = -1
            uxfio.offset_bof = 0
            uxfio.offset_eof_saved = -1
        else:
            uxfio.current_offset = 0
            uxfio.virtual_offset = 0
            uxfio.offset_eof = value
            uxfio.offset_bof = uxfio.pos
            uxfio.offset_eof_saved = value
        return 0
    elif cmd == uc.UXFIO_F_ARM_AUTO_DISABLE:
        uxfio.auto_disable = 1 if value else 0
        return 0
    elif cmd == uc.UXFIO_F_WRITE_INSERT:
        uxfio.write_insert = 1 if value else 0
        return 0
    elif cmd == uc.UXFIO_F_SET_LOCK_MEM_FATAL:
        uxfio.lock_buf_fatal = value
        return 0
    elif cmd == uc.UXFIO_F_DO_MEM_REALLOC:
        if value < 0:
            return -1
        if uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM:
            newbuf = memset(uxfio.buf, uxfio.len + value + uc.UXFIO_ALLOC_AHEAD + 1, uxfio.len)
            if not newbuf:
                sys.stderr.write("out of memory\n")
                sys.exit(4)
            uxfio.buf = newbuf
            uxfio.len += value + uc.UXFIO_ALLOC_AHEAD
            # memset(uxfio.buf + uxfio.end, '\0', uxfio.len + 1 - uxfio.end)
        else:
            return -2
        return 0
    elif cmd == uc.UXFIO_F_SET_OUTPUT_BLOCK_SIZE:
        if value < 0:
            return -1
        uxfio.output_block_size = value
        if uxfio.output_buffer:
            del uxfio.output_buffer
        uxfio.output_buffer = MemoryAllocator.malloc(value + 1, 100)
        return 0
    elif uxfio.uxfd != uc.UXFIO_NULL_FD:
        if uxfio.uxfd < uc.UXFIO_FD_MIN:
            return fcntl.fcntl(uxfio.uxfd, cmd, value)
        else:
            return uxfio_fcntl(uxfio.uxfd, cmd, value)
    return -1

def copy_stat_members(buf, statbuf):
    buf.st_mode = statbuf.st_mode
    buf.st_ino = statbuf.st_ino
    buf.st_dev = statbuf.st_dev
    buf.st_rdev = statbuf.st_rdev
    buf.st_nlink = statbuf.st_nlink
    buf.st_uid = statbuf.st_uid
    buf.st_gid = statbuf.st_gid
    buf.st_size = statbuf.st_size
    buf.st_atime = statbuf.st_atime
    buf.st_mtime = statbuf.st_mtime
    buf.st_ctime = statbuf.st_ctime
    buf.st_blksize = statbuf.st_blksize
    buf.st_blocks = statbuf.st_blocks

def fix_buffer(uxfiofd, uxfio, totalbytes, p, totalnewbytes):
    #    n = 0
    left = 0
    right = 0
    unused = 0
    #    ret = 0
    if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
        if uxfio.pos + totalnewbytes > uxfio.len:
            return -1
        if totalnewbytes:
            n = os.lseek(uxfio.buffd, 0, os.SEEK_END)
            if n != uxfio.end:
                sys.stderr.write("uxfio internal error: fix_buffer uxfio->end invalid value.\n")
                return -1
            n = os.write(uxfio.buffd, p + totalbytes - totalnewbytes)
            if n < 0:
                sys.stderr.write("exiting in uxfio fix_buffer\n")
                sys.exit()
            uxfio.pos += totalbytes
            uxfio.virtual_offset += totalbytes
            uxfio.end = uxfio.pos
            uxfio.start = 0
        else:
            uxfio.pos += totalbytes
            uxfio.virtual_offset += totalbytes
            uxfio.start = 0
    else:
        uxfio.bufferstate(left, right, unused)
        if totalnewbytes:
            if totalbytes >= uxfio.len:
                if uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM:
                    ret = uxfio.fix_dynamic_buffer(uxfiofd, totalbytes, p, totalnewbytes)
                    return ret
                uxfio__memcpy(uxfio.buf, 0, p, totalbytes - uxfio.len, uxfio.len, uxfio)
                uxfio.pos = uxfio.len
                uxfio.virtual_offset = uxfio.len
                uxfio.end = uxfio.len
                uxfio.start = 0
            elif totalbytes > right + unused:
                if uxfio.buffertype == uc.UXFIO_BUFTYPE_DYNAMIC_MEM:
                    ret = uxfio.fix_dynamic_buffer(uxfio, totalbytes, p, totalnewbytes)
                    return ret
                uxfio__memmove(uxfio.buf, 0, uxfio.buf, totalbytes - unused - right, uxfio.len - totalbytes + right,
                               uxfio)
                uxfio.pos = uxfio.len
                uxfio.virtual_offset = uxfio.len
                uxfio.end = uxfio.pos
                uxfio.start = 0
                uxfio__memcpy(uxfio.buf, uxfio.len - totalbytes, p, 0, totalbytes, uxfio)
            elif totalbytes > right:
                uxfio__memcpy(uxfio.buf, uxfio.pos, p, 0, totalbytes, uxfio)
                uxfio.pos = uxfio.pos + totalbytes
                uxfio.virtual_offset += totalbytes
                uxfio.end = uxfio.pos
                uxfio.start = 0
            else:
                sys.stderr.write("uxfio internal error: 10005.3 \n")
                return -1
        else:
            uxfio.pos += totalbytes
    return 0

def fix_dynamic_buffer(uxfio, uxfiofd, totalbytes, p, totalnewbytes):
    #    left, right, unused = bufferstate(uxfio)
    if totalnewbytes > 0:
        newbuf = uxfio.buf[:uxfio.len + totalnewbytes + uc.UXFIO_ALLOC_AHEAD + 1]
        if not newbuf:
            # sys.stderr.write("uxfio: fatal: out of memory at line {}\n".format(__LINE__))
            sys.stderr.write("uxfio: fatal: out of memory at line {}\n")
            sys.exit(22)
        newbuf[uxfio.pos:] = p[:totalbytes]
        uxfio.len += (totalnewbytes + uc.UXFIO_ALLOC_AHEAD)
    else:
        sys.stderr.write("uxfio internal error: fix_dynamic_buffer.\n")
        sys.exit(1)
    uxfio.pos += totalbytes
    if uxfio.pos > uxfio.end:
        uxfio.end = uxfio.pos
    newbuf[uxfio.end:] = '\0' * (uxfio.len + 1 - uxfio.end)
    uxfio.start = 0
    if newbuf != uxfio.buf:
        if uxfio.lock_buf_fatal:
            sys.stderr.write("uxfio: Realloc moved pointer, fatal according to current configuration.\n")
            sys.stderr.write("uxfio: Fatal.. exiting with status 38.\n")
            sys.exit(38)
    uxfio.buf = newbuf
    return 0

def uxfio_get_fd_mem(uxfio, uxfio_fildes, data_len):
    ret = uxfio_get_dynamic_buffer(uxfio_fildes, uxfio.pos, uxfio.end, data_len)
    if ret < 0:
        return None
    return ret


def table_mon(cmd, uxfio_fildes, uxfio_addr):
    p_index = uxfio_fildes
    index = 0
    i = 0
    if cmd == uc.UXFIO_FIND:
        return table_find(p_index, uxfio_addr, 1)
    elif cmd == uc.UXFIO_REMOVE:
        for i in range(uc.UXFIO_MAX_OPEN):
            if uxg_desc_table[i] == p_index:
                index = i
                break
        if i < uc.UXFIO_MAX_OPEN and uxg_status_table[index] > 0:
            uxg_status_table[index] = 0
            return 0
        else:
            return -1
    elif cmd == uc.UXFIO_ADD:
        for i in range(uc.UXFIO_MAX_OPEN):
            if uxg_status_table[i] == 0:
                uxg_status_table[i] = 1
                uxg_desc_table[i] = uc.UXFIO_FD_MIN + i
                alloc_file(i)
                uxfio_addr = uxg_file_table[i]
                uxfio_fildes = uc.UXFIO_FD_MIN + i
                return 0
        sys.stderr.write("uxfio: too many open files, uc.UXFIO_MAX_OPEN=%d\n" % uc.UXFIO_MAX_OPEN)
        uxfio_addr = None
        return -1
    elif cmd == uc.UXFIO_INIT:
        for i in range(uc.UXFIO_MAX_OPEN):
            uxg_file_table[i] = 0
        # uxg_did_init = 1
        return 0
    elif cmd == uc.UXFIO_QUERY_NOPEN:
        nopen = 0
        for i in range(uc.UXFIO_MAX_OPEN):
            if uxg_status_table[i] != 0:
                nopen += 1
        return nopen
    else:
        sys.stderr.write("uxfio internal error: invalid table command.\n")
        return -2


def internal_uxfio_open(uxfio_aa):
    uxfio_fd = 0
    # uxfio = UXFIO()
    if not uxg_did_init:
        table_mon(uc.UXFIO_INIT, uxfio_fd, uxfio_aa)
    if table_mon(uc.UXFIO_ADD, uxfio_fd, uxfio_aa):
        return -1

    uxfio = uxfio_aa
    uxfio_fildes = uxfio_fd
    uxfio.buffertype = uc.UXFIO_BUFTYPE_MEM  # memory segment, disk file, dynamic (growable) memory
    uxfio.pos = 0
    uxfio.len = uc.UXFIO_LEN
    uxfio.start = 0
    uxfio.end = 0
    uxfio.error = 0
    uxfio.buffer_active = 1
    uxfio.buffd = -1
    uxfio.offset_eof = -1
    uxfio.offset_bof = 0
    uxfio.offset_eof_saved = -1
    uxfio.bytesread = 0
    uxfio.current_offset = 0
    uxfio.virtual_offset = 0
    uxfio.auto_arm_delay = 0
    uxfio.use_count = 1
    uxfio.auto_disable = 0
    uxfio.write_insert = 0
    uxfio.buffilename[0] = '\0'
    uxfio.tmpfile_rootdir = None
    uxfio.v_end = -1
    uxfio.statbuf = ""
    uxfio.did_dupe_fd = -1
    uxfio.lock_buf_fatal = 0
    uxfio.output_block_size = 0
    uxfio.output_buffer_c = 0
    uxfio.output_buffer = None
    print("malloc {} {}".format(uxfio_fd, uc.UXFIO_LEN))
    uxfio.buf = ['\0'] * (uc.UXFIO_LEN + 1)
    if uxfio.buf is None:
        table_mon(uc.UXFIO_REMOVE, uxfio_fd, uxfio)
        return -1
    uxfio__change_state(uxfio, 0)
    return uxfio_fd


def uxfio__memmove(dest, dest_offset, src, src_offset, amount, uxfio):
    if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
        return do_file_buffer_move(dest, dest_offset, src, src_offset, amount, uxfio)
    else:
        return memmove(dest + dest_offset, src + src_offset, amount)


def uxfio__memcpy(dest, dest_offset, src, src_offset, amount, uxfio):
    if uxfio.buffertype == uc.UXFIO_BUFTYPE_FILE:
        return do_file_buffer_move(dest, dest_offset, src, src_offset, amount, uxfio)
    else:
        return memcpy(dest + dest_offset, src + src_offset, amount)


def do_file_buffer_move(dest, dest_offset, src, src_offset, amount, uxfio):
    return None


def zero_fill(fd, amount):
    buf = bytearray(512)
    rem = amount
    while rem > 0:
        am = min(rem, len(buf))
        ret = os.write(fd, buf[:am])
        if ret < 0:
            return -1
        rem -= ret
    return 0


def alloc_file(i):
    if uxg_file_table[i] is "":
        uxg_file_table[i] = UXFIO()


def table_find(uxfio_fildes, uxfio_addr, v):
    last_used_fd_index = 0
    if uxg_desc_table[last_used_fd_index] == uxfio_fildes:
        if last_used_fd_index < uc.UXFIO_MAX_OPEN and uxg_status_table[last_used_fd_index] > 0:
            alloc_file(last_used_fd_index)
            uxfio_addr = uxg_file_table[last_used_fd_index]
            return 0
    if UXFIO_FD_MIN <= uxfio_fildes < uc.UXFIO_MAX_OPEN + UXFIO_FD_MIN:
        i = 0
        while i < uc.UXFIO_MAX_OPEN:
            if uxg_desc_table[i] == uxfio_fildes:
                break
            i += 1
        if i < uc.UXFIO_MAX_OPEN and uxg_status_table[i] > 0:
            alloc_file(i)
            uxfio_addr = uxg_file_table[i]
            last_used_fd_index = i
            return 0
    uxfio_addr = None
    if uxfio_fildes == -1:
        return -1
    sys.stderr.write("error %d" % uxfio_fildes)
    if v:
        print("uxfio error [%d]: descriptor %d not found.\n" % (fr.__LINE__, uxfio_fildes))
    return -1


def uxfio_show_all_open_fd(f):
    uxfio = None
    n = 0
    for i in range(uc.UXFIO_FD_MIN, uc.UXFIO_MAX_OPEN + uc.UXFIO_FD_MIN):
        if table_find(i, uxfio, 0):
            pass
        else:
            print("{} {}".format(n, i), file=f)
            n += 1

def uxfio_uxfio_get_nopen():
    uxfio = None
    n = 0
    i = -1
    while table_mon(uc.UXFIO_QUERY_NOPEN, i, uxfio):
        n += 1
    return n


def uxfio_unix_safe_write(fd, buf, nbyte):
    return safeio(os.write, fd, buf, nbyte)


def uxfio_unix_atomic_read(fd, buf, nbyte):
    return atomicio(os.read, fd, buf, nbyte)


def uxfio_unix_atomic_write(fd, buf, nbyte):
    if uc.UXFIO_FD_MIN > fd >= 0:
        return atomicio(os.write, fd, buf, nbyte)
    else:
        return atomicio(uxfio_write, fd, buf, nbyte)


def uxfio_unix_safe_atomic_read(fd, buf, nbyte):
    return atomicio(os.read, fd, buf, nbyte)


def uxfio_debug_get_object_address(uxfiofd):
    uxfio = uxfio_get_object_address(uxfiofd)
    if table_find(uxfiofd, uxfio, 1):
        print("uxfio error: file desc {} not found.".format(uxfiofd), file=sys.stderr)
        return None
    return uxfio


def uxfio_get_object_address(uxfiofd):
    return uxfio_debug_get_object_address(uxfiofd)


def uxfio_devnull_close(fd):
    return 0


def uxfio_devnull_open():
    global uxg_nullfd
    if uxg_nullfd == 0:
        uxg_nullfd = os.open("/dev/null", os.O_RDWR, 0)
    if uxg_nullfd < 0:
        print("fatal error: can't open /dev/null: {}".format(os.strerror), file=sys.stderr)
        exit(1)
    return uxg_nullfd


def uxfio_open(path, oflag, mode):
    # uxfio_fd = 0
    # u_fd = 0
    uxfio = UXFIO()
    uxfio_fd = internal_uxfio_open(uxfio)
    if uxfio_fd < 0:
        return -1
    if path:
        if len(path):
            u_fd = os.open(path, oflag, mode)
            if u_fd < 0:
                sys.stderr.write("open ( {} ) : {}" + path)
                return -1
        else:
            u_fd = uc.UXFIO_NULL_FD
        uxfio__change_state(uxfio, 0)
        uxfio.uxfd_can_seek = 1
    else:
        u_fd = -1
        uxfio.buffer_active = 0
        uxfio.uxfd_can_seek = 0
    uxfio.uxfd = u_fd
    return uxfio_fd


def uxfio_decr_use_count(fd):
    uxfio = UXFIO()
    if fd < uc.UXFIO_FD_MIN:
        return -1
    if table_find(fd, uxfio, 1):
        return -1
    uxfio.use_count -= 1
    return uxfio.use_count


def uxfio_incr_use_count(fd):
    uxfio = UXFIO()
    if table_find(fd, uxfio, 1):
        return -1
    uxfio.use_count += 1
    return uxfio.use_count
