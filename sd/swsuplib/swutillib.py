# swutillib.c -- The top-level common routines for the sw<*> utilities.  */
#   Copyright (C) 2006 Jim Lowe
#   Copywrite (c) 2023 Paul Weber Python conversion

# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import os
import sys
import time

from sd.swsuplib.misc.swextopt import swextopt_is_option_true, eOpts
from sd.swsuplib.misc.swutillib import swlibc, swlib_utilname_get, swlib_doif_writeap, swlib_do_log_is_true
from sd.swsuplib.uinfile.uinfile import uinc
from sd.swsuplib.taru.xformat import xformat_close, xformat_open_archive_by_fd
from sd.swsuplib.taru.xformat import xformat_open_archive_by_fd_and_name, xformat_open_archive_dirfile

SWUTIL_NAME_SWLIST = "swlist"
SWUTIL_NAME_SWINSTALL = "swinstall"
SWUTIL_NAME_SWCOPY = "swcopy"
SWUTIL_NAME_SWPACKAGE = "swpackage"
SWUTIL_NAME_SWVERIFY = "swverify"
SWUTIL_NAME_SWREMOVE = "swremove"


class SwLogspec:
    def __init__(self):
        self.loglevel = 0
        self.logfd = 0
        self.fail_loudly = 0


def swutil_open():
    return SwLog()


def swutil_close(swutil):
    del swutil


def printlogline(buf, sptr):
    tm = time.time()
    result = time.strftime("%b %d %H:%M:%S", time.localtime(tm))
    sys.stdout.write(buf + '0' + "%s [%d] %s" + result + os.getpid() + sptr)
    return 0


class SwLog:
    def __init__(self):
        self.swu_efd = sys.stderr.fileno()
        self.swu_logspec = None
        self.swu_verbose = 1
        self.swu_fail_loudly = 0


def swutil_set_stderr_fd(swutil, fd):
    swutil.swu_efd = fd


def doif_i_writef(verbose_level, write_at_level, logspec, fd, format_, *pap):
    buffer = ""
    newret = 0
    level = verbose_level
    if write_at_level >= 0 and level < write_at_level:
        pass
    else:
        buffer = ""
        if write_at_level >= swlibc.SWC_VERBOSE_7:
            buffer = "debug> "
            buffer += swlib_utilname_get()
            buffer += f"[{os.getpid()}]: "
        else:
            buffer = f"{swlib_utilname_get()}: "
        newret = swlib_doif_writeap(fd, buffer, format_, pap)
    if logspec and logspec.logfd > 0:
        if swlib_do_log_is_true(logspec, verbose_level, write_at_level):
            if fd == sys.stderr.fileno() or fd == sys.stdout.fileno() or fd < 0:
                logbuf = ""
                if write_at_level >= swlibc.SWC_VERBOSE_7:
                    buffer = "debug> "
                    buffer += swlib_utilname_get()
                    buffer += f"[{os.getpid()}]: "
                else:
                    buffer = f"{swlib_utilname_get()}: "
                printlogline(logbuf, buffer)
                newret = swlib_doif_writeap(logspec.logfd, logbuf, format_, pap)
        else:
            newret = 0
    return newret


def swutil_setup_xformat(swutil, xformat, source_fd0, source_path, opta, is_seekable, g_verbose, g_logspec,
                         uinfile_open_flags):
    open_error = 0
    flags = uinfile_open_flags
    if is_seekable:
        flags |= uinc.UINFILE_UXFIO_BUFTYPE_DYNAMIC_MEM
        print("using dynamic mem")
    else:
        print("not using dynamic mem")
        flags &= ~uinc.UINFILE_UXFIO_BUFTYPE_DYNAMIC_MEM
    if opta and (swextopt_is_option_true(eOpts.SW_E_swbis_allow_rpm, opta) or swextopt_is_option_true(
            eOpts.SW_E_swbis_any_format, opta)):
        print("turning on UINFILE_DETECT_UNRPM")
        flags |= uinc.UINFILE_DETECT_UNRPM
    ret = xformat
    print("source_path is [%s]" % source_path)
    if source_fd0 >= 0 and (source_path is None or uinfile_open_flags & uinc.UINFILE_DETECT_IEEE) and open_error == 0:
        print("open by fd")
        print("source_path is [%s]" % source_path)
        print("flags=[%d]" % flags)
        if xformat_open_archive_by_fd(xformat, source_fd0, flags, 0):
            print("archive open failed")
            xformat_close(xformat)
            print("open error")
            ret = None
        print("after xformat_open_archive_by_fd")
    elif source_fd0 >= 0 and source_path and open_error == 0:
        print("~~~~~~~~~~~~~~~ open by fd and name")
        if xformat_open_archive_by_fd_and_name(xformat, source_fd0, flags, 0, source_path):
            print("archive open failed")
            xformat_close(xformat)
            print("open error")
            ret = None
    elif source_fd0 < 0 and open_error == 0:
        print("open file or directory")
        if xformat_open_archive_dirfile(xformat, source_path, flags, 0):
            print("open failed on %s (by dir)" % source_path)
            xformat_close(xformat)
            ret = None
    else:
        print("open error")
        print("%s not found" % source_path)
        ret = None
    return ret


def swutil_doif_writef2(swutil, write_at_level, format_, *args):
    ret = doif_i_writef(swutil.swu_verbose, write_at_level, swutil.swu_logspec, swutil.swu_efd, format_, *args)
    return ret


def swutil_do_log_is_true(logspec, verbose_level, write_at_level):
    if (logspec.logfd == 1 and write_at_level < swlibc.SWC_VERBOSE_6) or (
            logspec.logfd == 2 and write_at_level < swlibc.SWC_VERBOSE_7) or (
            logspec.logfd == 2 and verbose_level > swlibc.SWC_VERBOSE_7):
        return 1
    else:
        return 0


def swutil_doif_writef(verbose_level, write_at_level, logspec, fd, format_, *args):
    ret = doif_i_writef(verbose_level, write_at_level, logspec, fd, format_, *args)
    return ret


def cpp_doif_i_writef(verbose_level, write_at_level, logspec, fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format_, *args):
    b = "%s:%d (%s): %s" % (cpp_FILE, cpp_LINE, cpp_FUNCTION, format_)
    ret = doif_i_writef(verbose_level, write_at_level, logspec, fd, b, *args)
    return ret


def swutil_cpp_doif_writef(verbose_level, write_at_level, logspec,
                           fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format_, *args):
    ret = cpp_doif_i_writef(verbose_level, write_at_level, logspec,
                            fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format_, *args)
    return ret
