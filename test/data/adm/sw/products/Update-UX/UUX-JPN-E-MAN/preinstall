#! /sbin/sh
##########################
# Product:  Update-UX
# Filesets: UUX-ENG-A-MAN
#           UUX-JPN-E-MAN
#           UUX-JPN-S-MAN
# preinstall
# @(#) $Revision: 11.22 $
#
# (c) Copyright Hewlett-Packard Company 2002
#
##########################

UTILS="/usr/lbin/sw/control_utils"
if [[ ! -f $UTILS ]]; then
    print "ERROR:   Cannot find $UTILS"
    exit 1
fi
. $UTILS
exitval=$SUCCESS

if [[ $SW_ROOT_DIRECTORY != "/" ]]; then
    exit $exitval
fi

#
# The same preinstall script is used for all localizations of
# the man pages.
# First determine which locality is being installed.
#

UUX_MAN_FILESET=`echo $SW_SOFTWARE_SPEC | sed -e's/,.*//'  \
                                     | sed -e's/.*\.//'`

if [[ "${UUX_MAN_FILESET}" = "UUX-ENG-A-MAN" ]]
then
    MAN_DIR="/usr/share/man"
elif  [[ "${UUX_MAN_FILESET}" = "UUX-JPN-E-MAN" ]]
then
    MAN_DIR="/usr/share/man/ja_JP.eucJP"
elif  [[ "${UUX_MAN_FILESET}" = "UUX-JPN-S-MAN" ]]
then
    MAN_DIR="/usr/share/man/ja_JP.SJIS"
else
    #
    # could not identify fileset
    # Apparently this script is running
    # outside of its owning fileset.
    #
    exit $SUCCESS 
fi

# Remove formatted compressed files created by command man
#  and any obsoleted unformatted compressed files
for file in ${MAN_DIR}/cat1m.Z/update-ux.1m \
    
do
    if [[ -f $file ]]; then
        rm -f $file
        retval=$?
        if [[ $retval -ne $SUCCESS ]]; then    
            msg WARNING "Could not remove \"$file\""
            exitval=$WARNING
        fi
    fi
done

############Remove an obsolete product.fileset or obsolete manpage
typeset inst_rmlist="/var/tmp/rmlist$$"
typeset hidden_dir="/.sw/obsolete/"
mkdir -p ${hidden_dir} 

#Swmodify log is hidden, removed at reboot
typeset swmodify_log="${hidden_dir}swmodify.log"
print " ======= BEGIN $0 for OBSOLESCENCE" >> $swmodify_log


#####################

SD_FILESET=`echo $UUX_FILESET | sed -e's/UUX/SD/'`

# Verify if manpages from SW-DIST.SD currently installed on system.
rm ${inst_rmlist}  > /dev/null 2>&1   

for sw in  ${MAN_DIR}/man1m.Z/update-ux.1m
do 
    swlist -l file -a software_spec SW-DIST.${SD_FILESET} 2>/dev/null |&
    while read -p line
    do 
        set -- $line
        [[ ${1} = $sw ]] && print ${1} >> ${inst_rmlist}
    done 
done    
 
#If no manpages to be swmodify deleted are found on this system, exit here
# Direct the commands output to /dev/null after all debugging is complete.
if [[ -e $inst_rmlist ]]; then
    typeset x_opts=" -x loglevel=2 -x logfile=${swmodify_log}"
    chmod 666 ${inst_rmlist}
    swmodify -u -P $inst_rmlist $x_opts SW-DIST.${SD_FILESET} \
        >>${swmodify_log} 2>&1 
    retval=$?
    if [[ $retval -ne 0 ]]; then
        msg WARNING " Swmodify  $inst_rmlist SW-DIST.${SD_FILESET} "
        [[ $exitval -ne $FAILURE && $retval -ne 0 ]] && exitval=$WARNING
    fi
fi
        
print " ======= END $0 for OBSOLESCENCE" >> $swmodify_log
print $swmodify_log >> /var/adm/sw/cleanupfile

##Cleanup
rm ${inst_rmlist}  > /dev/null 2>&1   
exit $exitval
