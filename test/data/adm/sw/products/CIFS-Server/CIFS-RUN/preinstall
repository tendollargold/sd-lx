#! /sbin/sh
########
#  Product: CIFS-Server
#  Fileset: CIFS-RUN
#  preinstall
#  @(#) $Revision: 1.7.10.1 $
########
#
# (c) Copyright Hewlett-Packard Company 2000
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
########

    UTILS="/usr/lbin/sw/control_utils"
    if [[ ! -f $UTILS ]]
    then
        echo "ERROR:   Cannot find $UTILS"
        exit 1
    fi
    . $UTILS

    exitval=$SUCCESS              # Anticipate success

################################################################################
# A cold install have no way to check if a preinstall is TP or normal version.
# During a cold install, swlist, what and ps not yet available on the system.
# The variable SW_INITIAL_INSTALL is used to distinguish if it's a cold install.
# If it's a cold install, nothing to do.

    if [[ ${SW_INITIAL_INSTALL} = "1" ]]
    then
        exit $exitval
    fi

# For non cold installations

# Don't do the kills if in a DRD environment.

if [ "$(get_os_rev)" = "11 11" ] || [[ ${SW_SESSION_IS_DRD} -ne 1 ]]
then
	kill_named_procs smbd
	if (( $? != 0 ))
	then
	    kill_named_procs smbd KILL
	fi
	kill_named_procs nmbd
	if (( $? != 0 ))
	then
	    kill_named_procs nmbd KILL
	fi
	kill_named_procs winbindd
	if (( $? != 0 ))
	then
	    kill_named_procs winbindd KILL
	fi
fi

# Preserve customers files: smb.conf and samba
for afile in /etc/opt/samba/smb.conf /etc/rc.config.d/samba; do
    newconfig_prep $afile
    retval=$?
    if (( $retval != $SUCCESS ))
    then
        exit $retval
    else
        echo "NOTE:    Preserve $afile."
    fi
done

# When upgrading from 2.2 to 3.0, remove /opt/samba/bin & /var/opt/samba/locks/*.tdb 
# and /opt/samba/script

    CIFSUTILS=${SW_CONTROL_DIRECTORY}/cifs_utils
    if [[ ! -f $CIFSUTILS ]]; then
        echo "ERROR:   Cannot find $CIFSUTILS."
        exit $FAILURE
    fi
    . $CIFSUTILS
    is_upgrading 2.* 3.*
    if [[ $? = $SUCCESS ]]; then
        echo "NOTE:    Remove previous installed /opt/samba/bin,/var/opt/samba/locks/*.*,"
        echo "         except for ntprinters.tdb, ntdrivers.tdb and ntforms.tdb."
        rm -rf /opt/samba/bin
        rm -rf /opt/samba/script
        rm -f /var/opt/samba/locks/*.tdb.lck
        rm -f /var/opt/samba/locks/browse.dat
        for afile in `ls /var/opt/samba/locks`; do
           if [[ $afile != ntprinters.tdb && $afile != ntdrivers.tdb && $afile != ntforms.tdb && $afile != share_info.tdb ]]; then
               rm -f /var/opt/samba/locks/$afile
           fi
        done
    fi

exit $exitval
