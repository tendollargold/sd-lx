# Copyright 2005 Duke University
# Copyright (C) 2012-2016 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Handle actual output from the cli."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals


import itertools
import logging
import operator
import re


logger = logging.getLogger('sd')

def _spread_in_columns(cols_count, label, lst):
    left = itertools.chain((label,), itertools.repeat(''))
    lst_length = len(lst)
    right_count = cols_count - 1
    missing_items = -lst_length % right_count
    if not lst_length:
        lst = itertools.repeat('', right_count)
    elif missing_items:
        lst.extend(('',) * missing_items)
    lst_iter = iter(lst)
    return list(zip(left, *[lst_iter] * right_count))
class Output(object):
    """Main output class for the yum command line."""

    GRP_PACKAGE_INDENT = ' ' * 3
    FILE_PROVIDE_RE = re.compile(r'^\*{0,2}/')

    def __init__(self, base, conf):
        self.conf = conf
        self.base = base
        self.term = sd.cli.term.Term()
        self.progress = None

    def _banner(self, col_data, row):
        term_width = self.term.columns
        rule = '%s' % '=' * term_width
        header = self.fmtColumns(zip(row, col_data), ' ')
        return rule, header, rule

    def _col_widths(self, rows):
        col_data = [dict() for _ in rows[0]]
        for row in rows:
            for (i, val) in enumerate(row):
                col_dct = col_data[i]
                length = len(val)
                col_dct[length] = col_dct.get(length, 0) + 1
        cols = self.calcColumns(col_data, None, indent='  ')
        # align to the left
        return list(map(operator.neg, cols))

    def _highlight(self, highlight):
        hibeg = ''
        hiend = ''
        if not highlight:
            pass
        elif not isinstance(highlight, basestring) or highlight == 'bold':
            hibeg = self.term.MODE['bold']
        elif highlight == 'normal':
            pass # Minor opt.
        else:
            # Turn a string into a specific output: colour, bold, etc.
            for high in highlight.replace(',', ' ').split():
                if high == 'normal':
                    hibeg = ''
                elif high in self.term.MODE:
                    hibeg += self.term.MODE[high]
                elif high in self.term.FG_COLOR:
                    hibeg += self.term.FG_COLOR[high]
                elif (high.startswith('fg:') and
                      high[3:] in self.term.FG_COLOR):
                    hibeg += self.term.FG_COLOR[high[3:]]
                elif (high.startswith('bg:') and
                      high[3:] in self.term.BG_COLOR):
                    hibeg += self.term.BG_COLOR[high[3:]]

        if hibeg:
            hiend = self.term.MODE['normal']
        return (hibeg, hiend)
    def _sub_highlight(self, haystack, highlight, needles, **kwds):
        hibeg, hiend = self._highlight(highlight)
        return self.term.sub(haystack, hibeg, hiend, needles, **kwds)

    @staticmethod
    def _calc_columns_spaces_helps(current, data_tups, left):
        """ Spaces left on the current field will help how many pkgs? """
        ret = 0
        for tup in data_tups:
            if left < (tup[0] - current):
                break
            ret += tup[1]
        return ret
