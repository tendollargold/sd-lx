# sd.defaults.py  - Set defaults precedent for options and operands.
# Systems Management: Distributed Software Administration
# Copyright © 1997 The Open Group

# Copyright (C) 2021- 2024 Paul Weber

# 3.5.3.1  Precedence for Option Specification

# SD Defaults for the command line and GUI
# Precedence for Option Specification

# Multiple option or operand specifications have a precedence that defines which
# specifications are used.

# Only the option specifications with the highest level of precedence are used
# for each option and operand. The precedence is in increasing order:
#   1. System defaults file
#   2. User defaults file
#   3. Options file
#   4. Command line options and operands

# If there are multiple instances of options at any particular level, then the
# following rules apply:
#  · If both keyword and command.keyword exist in the set of defaults or options
#    files for this level, the command uses the latter, more specific, definition.
#  · All values for software and targets options from all levels are all included
#    in the resulting software_selections and target_selections for the command.
#  · For options besides software and targets, the behavior when multiple or 
#    conflicting specifications are made is undefined. This rule applies to 
#    options such as s source where implementations may choose to assign a 
#    logical interpretation to multiple

import configparser
import os.path

from sd.const import SwGlobals as g

def validate_opts(option, cmd, config_file):
    # Validate a swdefaults option is valid for the 'sd' command
    # Only swdefault options are validated when using the command line -X, -x,
    # the system defaults file /etc/sw/sw.conf or /var/adm/sw/defaults
    # and $USER/.sw/sw.conf
    # First set validopts to internal default options.  These keys are
    # the only keys that are valid.
    validopts = setdefaults(cmd)
    #     print("need to add  check for valid arguments to certain cli options")
    # We could loop here, but we don't. We loop outside and call the 
    # function for every option within a file, or command line (-x)
    # Error if the key is not found within validopts
    print(option)
    print(cmd)
    print(config_file)
    print("")
    if option not in validopts.keys():
        print("")
        print(
            "SW_ILLEGAL_OPTION: An illegal or unrecognized option was found: " + option + " for " + cmd + " within the " + config_file)
        print("")
        raise SystemExit(1)


#        return False

def config_section_map(config_file, section):
    #   print("In config_section_map")
    
    config = configparser.ConfigParser()
    config.read(config_file)
    options = config.options(section)
    swdefaults = {}
    for option in options:
        validate_opts(option, section, config_file)
        try:
            swdefaults[option] = config.get(section, option)
            if swdefaults[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            swdefaults[option] = None
    return swdefaults


def override_defaults(cmd, swdefaults, cli_options):
    print("Entered override_defaults function")
    if os.path.isfile(g.user_swdefaults_file):
        filedefaults = config_section_map(g.user_swdefaults_file, cmd)
        swdefaults = swdefaults | filedefaults
    else:
        print("NOTE: " + g.user_swdefaults_file + " does not exist")
        print(os.path.isfile(g.user_swdefaults_file))
        pass

    print("Printing cli_options 2:")
    print(cli_options)
    if cli_options is not None:
        if type(cli_options) == type(str()):
            if os.path.isfile(cli_options):
                filedefaults = config_section_map(cli_options, cmd)
                swdefaults = swdefaults | filedefaults
            else:
                print("ERROR: " + cli_options + " is not a file")

        if type(cli_options) == type(dict()):
            for option in cli_options:
                validate_opts(option, cmd, "command line")
            swdefaults = swdefaults | cli_options
            print("CliOption have been added")
            print(swdefaults)

    return swdefaults


#
# Set the internal defaults for each operand and option per sw utility
# setting operand and options in a dictionary for each utility allows
# the ability to validate and option is valid or modify an option for the utility
# The easiest methond is to use a dictionary
def setdefaults(cmd):
    sw_defaults = {
        # we aren't using dced/rpc or swagent
        # So add remote shell option (ssh, rsh) for remote targets
        'admin_directory': g.ADMIN_DIRECTORY,
        'loglevel': "1",
        'log_msgid': '0',
        'logdetail': bool(False),
        'remote_shell': "ssh",
        'shell_cmd': "sh",
        'software': None,
        'verbose': '1'
    }

    # swacl is from HP SD
    # run_as_superuser is from HP SD
    #  sw_defaults()
    if cmd == "swacl":
        swdefaults = sw_defaults | {
            'distribution_target_directory': "/var/spool/sw",
            'installed_software_catalog': g.admin_directory + "/products",
            'level': None,
            'run_as_superuser': bool(True),
            'select_local': bool(True),
            'targets': None
        }

        return swdefaults
    # swreg is from HP SD
    elif cmd == "swreg":
        swdefaults = sw_defaults | {
            'distribution_target_directory': "/var/spool/sw",
            'level': None,
            'logfile': g.admin_directory + "/swacl.log",
            'objects_to_register': None,
            'select_local': bool(True)
        }


    elif cmd == "swask":
        swdefaults = sw_defaults | {
            'ask': bool(True),
            'autoselect_dependencies': bool(True),
            'autoselect_patches': bool(True),
            'distribution_source_directory': "/var/spool/sw",
            'distribution_source_serial': "/dev/rmt/0m",
            'enforce_scripts': bool(True),
            'installed_software_catalog': g.admin_directory + "/products",
            'logfile': g.admin_directory + "/swask.log",
            'patch_filter': "*.*"
        }

    elif cmd == "swconfig":
        swdefaults = sw_defaults | {
            'allow_incompatible': bool(False),
            'allow_multiple_versions': bool(False),
            'ask': bool(False),
            'autoremove_job': bool(False),
            'autoselect_dependencies': bool(True),
            'autoselect_dependents': bool(True),
            'compress_index': bool(False),
            'controller_source': None,
            'enforce_dependencies': bool(True),
            'enforce_scripts': bool(True),
            'installed_software_catalog': g.admin_directory + "/products",
            'job_title': None,
            'logfile': g.admin_directory + "/swconfig.log",
            'mount_all_filesystems': bool(True),
            'preview': bool(False),
            'reconfigure': bool(False),
            'reuse_short_job_numbers': bool(True),
            'run_as_superuser': bool(True),
            'select_local': bool(True),
            'targets': None,
            'write_remote_files': bool(False)
        }

    elif cmd == "swcopy":
        swdefaults = sw_defaults | {
            'allow_split_patches': bool(False),
            'autoremove_job': bool(False),
            'autoselect_dependencies': bool(True),
            'autoselect_patches': bool(True),
            'autoselect_reference_bundles': bool(True),
            'compress_files': bool(False),
            'compress_index': bool(False),
            'controller_source': None,
            'compression_type': "gzip",
            'distribution_source_directory': "/var/spool/sw",
            'distribution_target_directory': "/var/spool/sw",
            'enforce_dependencies': bool(True),
            'enforce_dsa': bool(True),
            'job_title': None,
            'logfile': g.admin_directory + "/swcopy.log",
            'mount_all_filesystems': bool(True),
            'patch_filter': None,
            'preview': bool(False),
            'recopy': bool(False),
            'register_new_depot': bool(True),
            'reinstall_files': bool(True),
            'reinstall_files_use_cksum': bool(True),
            'remove_obsolete_filesets': bool(False),
            'run_as_superuser': bool(False),
            'select_local': bool(True),
            'software_view': "all_bundles",
            'source': None,
            'source_cdrom': "/SD_CDROM",
            'source_tape': "/dev/rmt/0m",
            'source_type': "directory",
            'targets': None,
            'uncompress_files': bool(False),
            'use_alternate_source': bool(False),
            'write_remote_files': bool(True)
        }

    elif cmd == "swinstall":
        swdefaults = sw_defaults | {
            'allow_downdate': bool(False),
            'allow_incompatible': bool(False),
            'allow_multiple_versions': bool(False),
            'allow_split_patches': bool(False),
            'ask': bool(False),
            'autoreboot': bool(False),
            'autorecover': bool(False),
            'autorecover_product': bool(False),
            'autoremove_job': bool(False),
            'autoselect_dependencies': bool(False),
            'autoselect_patches': bool(True),
            'autoselect_reference_bundles': bool(True),
            'compress_index': bool(False),
            'defer_configure': bool(False),
            'defer_deleting_files': bool(False),
            'distribution_source_directory': "/var/spool/sw",
            'enforce_dependencies': bool(True),
            'enforce_dsa': bool(False),
            'enforce_kernbld_failure': bool(False),
            'enforce_locatable': bool(True),
            'enforce_scripts': bool(True),
            'installed_software_catalog': g.admin_directory + "/products",
            'job_title': None,
            'logfile': g.admin_directory + "/swinstall.log",
            'match_target': bool(False),
            'patch_save_files': bool(True),
            'patch_filter': "*",
            'patch_match_target': bool(False),
            'reinstall': bool(False),
            'reinstall_files': bool(False),
            'reinstall_files_use_cksum': bool(True),
            'save_modified_files': bool(False),
            'saved_files_directory': g.admin_directory + "/save",
            'select_local': bool(True),
            'software_view': "all_bundles",
            'source': None,
            'source_cdrom': "/SD_CDROM",
            'source_tape': "/dev/rmt/0m",
            'source_type': "directory",
            'targets': None
        }

    elif cmd == "swlist":
        # The swlist utility supports the following extended options.
        #   distribution_target_directory=implementation_defined_value
        #   installed_software_catalog=implementation_de fined_value
        #   one_liner=implementation_defined_value
        #   select_local=true
        #   software=
        #   targets=

        # The swlist utility from HP/HPE supports the following extended options.
        #  run_as_superuser=true
        #  show_superseded_patches=false
        #  software_view=all_bundles

        swdefaults = sw_defaults | {
            'distribution_target_directory': "/var/spool/sw",
            'installed_software_catalog': g.admin_directory + "/products",
            'one_liner': "revision title",
            'patch_one_liner': "title patch_state",
            'run_as_superuser': bool(True),
            'select_local': bool(True),
            'show_superseded_patches': bool(False),
            'software_view': "all_bundles",
            'targets': None
        }

    elif cmd == "swmodify":
        swdefaults = sw_defaults | {
            'compress_index': bool(False),
            'control_files': None,
            'distribution_target_directory': "/var/spool/sw",
            'installed_software_catalog': g.admin_directory + "/products",
            'files': None,
            'logfile': g.admin_directory + "/swmodify.log",
            'patch_commit': bool(False),
            'select_local': bool(True),
            'source_file': None,
            'targets': None
        }

    elif cmd == "swpackage":
        swdefaults = sw_defaults | {
            'allow_partial_bundles': bool(True),
            'compress_command': "/usr/bin/gzip",
            'compress_files': bool(False),
            'compress_index': bool(False),
            'compression_type': "gzip",
            'create_target_acls': bool(True),
            'distribution_target_directory': "/var/spool/sw",
            'distribution_target_serial': "/dev/rmt/0m",
            'enforce_dsa': bool(True),
            'follow_symlinks': bool(False),
            'include_file_revisions': bool(False),
            'logfile': g.admin_directory + "/swpackage.log",
            'media_capacity': 0,
            'media_type': "directory",
            'reinstall_files': bool(False),
            'reinstall_files_use_cksum': bool(True),
            'run_as_superuser': bool(True),
            'source_files': "psf",
            'targets': None,
            'unscompress_command': "/usr/bin/gunzip",
            'write_remote_files': bool(False),
        }


    elif cmd == "swremove":
        swdefaults = sw_defaults | {
            'auto_kernel_build': bool(True),
            'autoremove_job': bool(False),
            'autoselect_dependents': bool(False),
            'autoselect_reference_bundles': bool(True),
            'compress_index': bool(False),
            'controller_source': None,
            'distribution_target_directory': "/var/spool/sw",
            'enforce_dependencies': bool(True),
            'enforce_scripts': bool(True),
            'force_single_target': bool(False),
            'installed_software_catalog': g.admin_directory + "/products",
            'job_title': None,
            'logfile': g.admin_directory + "/swremove.log",
            'loglevel': "1",
            'mount_all_filesystems': bool(True),
            'preview': bool(False),
            'remove_empty_depot': bool(True),
            'reuse_short_job_numbers': bool(True),
            'run_as_superuser': bool(True),
            'select_local': bool(True),
            'software_view': "products",
            'targets': None,
            'write_remote_files': bool(False)
        }


    elif cmd == "swverify":
        swdefaults = sw_defaults | {
            'allow_incompatible': bool(False),
            'allow_multiple_versions': bool(False),
            'autoremove_job': bool(False),
            'autoselect_dependencies': bool(True),
            'check_contents': bool(True),
            'check_contents_uncompressed': bool(False),
            'check_contents_use_cksum': bool(True),
            'check_permissions': bool(True),
            'check_requisites': bool(True),
            'check_scripts': bool(True),
            'check_volatile': bool(False),
            'distribution_target_directory': "/var/spool/sw",
            'enforce_dependencies': bool(True),
            'enforce_locatable': bool(True),
            'installed_software_catalog': g.admin_directory + "/products",
            'job_title': None,
            'logfile': g.admin_directory + "/swverify.log",
            'mount_all_filesystems': bool(True),
            'reuse_short_job_numbers': bool(True),
            'run_as_superuser': bool(True),
            'select_local': bool(True),
            'verify_signatures': bool(False),
            'verify_signatures_only': bool(False),
            'public_key': "",
            'target': None
        }
    return
