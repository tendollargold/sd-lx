
# control.utils.py
# SD control script execution
#
# Copyright (C) 2012-2016 Pdub Software, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#


def swlist():
    print("swlist utility function")
    return None

def swconfig():
    print("swconfig utility function")
    # There are three key phases in the swconfig utility:
    # 1. Selection phase
    # 2. Analysis phase
    # 3. Execution phase
    return None

def swremove():
    print("swremove utility function")
    return None

def swcopy():
    print("swcopy utility function")
    return None

