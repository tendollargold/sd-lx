# to_oct.c : derived from create.c to isolate the to_oct routines.
# GNU tar version tar-1.13.17(.tar.gz)
#
#   Copyright 1985, 92, 93, 94, 96, 97, 99, 2000 Free Software Foundation, Inc.
#   Written by John Gilmore, on 1985-08-25.
#
#   Modified by Jim Lowe
#   Conversion to Python, Paul Weber
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 3, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#   Public License for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# Import libraries
import ctypes
import pwd
import stat
import sys

import grp


class GnutarArchiveFormat:
    DEFAULT_FORMAT = 0
    V7_FORMAT = 1
    OLDGNU_FORMAT = 2
    POSIX_FORMAT = 3
    GNU_FORMAT = 4

gaf = GnutarArchiveFormat()
uintmax_t = 2 ** 64 - 1  # unsigned long int


# Define constants


class ToOct:
    CHKBLANKS = "        "
    CHAR_BIT = 8
    CHAR_MAX = sys.maxsize
    GNAME_FIELD_SIZE = 32
    LG_8 = 3
    LG_64 = 6
    LG_256 = 8
    LONG_MAX = sys.maxsize
    NAME_FIELD_SIZE = 100
    PREFIX_FIELD_SIZE = 155
    #    STRINGIFY_BIGINT = lambda i, b: str(i)
    TAREXIT_SUCCESS = 0
    TAREXIT_DIFFERS = 1
    TAREXIT_FAILURE = 2
    TTY_NAME = "/dev/tty"
    UNAME_FIELD_SIZE = 32
    TSUID = 0o4000
    TSGID = 0o2000
    TSVTX = 0o1000


tooct = ToOct()


def GID_TO_CHARS(val, where, termch):
    gid_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def MAJOR_TO_CHARS(val, where, termch):
    major_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def MINOR_TO_CHARS(val, where, termch):
    minor_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def MODE_TO_CHARS(val, where, termch):
    mode_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def MODE_TO_OLDGNU_CHARS(val, where, termch):
    mode_to_chars(val, where, len(where), gaf.OLDGNU_FORMAT, termch)

def OFF_TO_CHARS(val, where, termch):
    off_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def SIZE_TO_CHARS(val, where, termch):
    size_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def TIME_TO_CHARS(val, where, termch):
    time_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def UID_TO_CHARS(val, where, termch):
    uid_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def UINTMAX_TO_CHARS(val, where, termch):
    uintmax_to_chars(val, where, len(where), gaf.POSIX_FORMAT, termch)

def type_signed(t):
    return not (t(0) < t(-1))


def x_int_strlen_bound(t):
    char_bit = tooct.CHAR_BIT  # Assuming 8 bits in a char, as in C
    return (ctypes.sizeof(t) * char_bit - ctypes.c_int(t)) * 302 // 1000 + 1 + ctypes.c_int(t)


def uintmax_strsize_bound():
    # In Python, there's no direct equivalent of uintmax_t, but we can use the maximum size of an integer.
    # However, Python integers are unbounded, so this concept doesn't directly apply.
    # We can use the maximum value of an integer in Python to get a string length bound.
    # This is a theoretical bound since Python integers can be arbitrarily large.
    return x_int_strlen_bound(uintmax_t) + 1


def signed_char(x):
    if sys.byteorder == 'little':
        # Assuming a little-endian platform (common case)
        return (x & 0xFF).to_bytes(1, 'little').decode('latin1')
    else:
        # For a big-endian platform
        return (x >> (8 * (sys.getsizeof(int) - 1))).to_bytes(1, 'big').decode('latin1')


def stringify_uintmax_t_backwards(o, buf):
    buf.append('\0')
    while o != 0:
        buf.append(chr(ord('0') + (o % 10)))
        o //= 10
    return buf[::-1]


def max_val_with_digits(digits, bits_per_digit):
    if digits * bits_per_digit < sys.maxsize.bit_length():
        return (1 << (digits * bits_per_digit)) - 1
    else:
        return sys.maxsize


def to_octal(value, where, size):
    v = value
    i = size
    while i > 0:
        i -= 1
        where[i] = chr(ord('0') + (v & ((1 << 3) - 1)))
        v >>= 3


def to_base256(negative, value, where, size):
    v = value
    propagated_sign_bits = ((-negative) << (8 * size - 8))
    i = size
    while i > 0:
        i -= 1
        where[i] = v & ((1 << 8) - 1)
        v = propagated_sign_bits | (v >> 8)


def to_chars(negative, value, valsize, substitute, where, size, type_, archive_format, termch):
    base256_allowed = (
            archive_format == GnutarArchiveFormat.GNU_FORMAT or archive_format == GnutarArchiveFormat.OLDGNU_FORMAT)

    if not negative and value <= max_val_with_digits(size - 1, 3):
        where[size - 1] = termch
        to_octal(value, where, size - 1)

    elif type_ == "uid_t" or type_ == "gid_t":
        value = 8 ^ (size - 1)
        to_chars(negative, value, valsize, substitute, where, size, "id_again", archive_format, termch)

    elif (negative and -1 - value <= max_val_with_digits(size - 1, 8)) and base256_allowed:
        print("to_chars: making old gnu field", file=sys.stderr)
        where[0] = -1 if negative else 1 << (8 - 1)
        to_base256(negative, value, where + 1, size - 1)

    elif negative and valsize * 8 <= (size - 1) * 3:
        warned_once = False
        print("to_chars: making non portable field", file=sys.stderr)
        if not warned_once:
            print("Generating negative octal headers", file=sys.stderr)
        where[size - 1] = '\0'
        to_octal(value & max_val_with_digits(valsize * 8, 1), where, size - 1)

    else:
        maxval = max_val_with_digits(size - 1, 8) if base256_allowed else max_val_with_digits(size - 1, 3)
        maxval_string = str(maxval)
        value_string = str(value)
        print("to_chars: making substitute value.", file=sys.stderr)
        if base256_allowed:
            m = maxval + 1 if maxval + 1 else maxval // 2 + 1
            p = str(m)
            minval_string = "-" + p
        else:
            minval_string = "0"
        if negative:
            p = str(-value)
            value_string = "-" + p
        if substitute:
            negsub = 0
            sub = substitute(negsub) & maxval
            s = -sub if negsub and archive_format == GnutarArchiveFormat.GNU_FORMAT else sub
            sub_string = str(s)
            if negsub:
                sub_string = "-" + sub_string
            print("value", value_string, "out of", type_, "range", minval_string, "..", maxval_string, "; substituting",
                  sub_string)
            to_chars(negsub, s, valsize, None, where, size, type_, archive_format, termch)
        else:
            print("value", value_string, "out of", type_, "range", minval_string, "..", maxval_string, file=sys.stderr)


def gid_substitute(negative):
    r = grp.getgrnam("nobody").gr_gid
    negative[0] = False
    return r


def gid_to_chars(v, p, s, archive_format, termch):
    to_chars(v < 0, v, 4, gid_substitute, p, s, "gid_t", archive_format, termch)


def major_to_chars(v, p, s, archive_format, termch):
    to_chars(v < 0, v, 4, None, p, s, "major_t", archive_format, termch)


def minor_to_chars(v, p, s, archive_format, termch):
    to_chars(v < 0, v, 4, None, p, s, "minor_t", archive_format, termch)


def mode_to_chars(v, p, s, archive_format, termch):
    if (stat.S_ISUID == tooct.TSUID and stat.S_ISGID == tooct.TSGID and stat.S_ISVTX == tooct.TSVTX
            and stat.S_IRUSR == 0o400 and stat.S_IWUSR == 0o200 and stat.S_IXUSR == 0o100
            and stat.S_IRGRP == 0o040 and stat.S_IWGRP == 0o020 and stat.S_IXGRP == 0o010
            and stat.S_IROTH == 0o004 and stat.S_IWOTH == 0o002 and stat.S_IXOTH == 0o001
            and archive_format != GnutarArchiveFormat.POSIX_FORMAT
            and archive_format != GnutarArchiveFormat.GNU_FORMAT):
        negative = v < 0
        u = v
    else:
        negative = False
        u = ((v & stat.S_ISUID and tooct.TSUID or 0)
             | (v & stat.S_ISGID and tooct.TSGID or 0)
             | (v & stat.S_ISVTX and tooct.TSVTX or 0)
             | (v & stat.S_IRUSR and 0o400 or 0)
             | (v & stat.S_IWUSR and 0o200 or 0)
             | (v & stat.S_IXUSR and 0o100 or 0)
             | (v & stat.S_IRGRP and 0o040 or 0)
             | (v & stat.S_IWGRP and 0o020 or 0)
             | (v & stat.S_IXGRP and 0o010 or 0)
             | (v & stat.S_IROTH and 0o004 or 0)
             | (v & stat.S_IWOTH and 0o002 or 0)
             | (v & stat.S_IXOTH and 0o001 or 0))
    to_chars(negative, u, 4, None, p, s, "mode_t", archive_format, termch)


def off_to_chars(v, p, s, archive_format, termch):
    to_chars(v < 0, v, 8, None, p, s, "off_t", archive_format, termch)


def size_to_chars(v, p, s, archive_format, termch):
    to_chars(False, v, 8, None, p, s, "size_t", archive_format, termch)


def time_to_chars(v, p, s, archive_format, termch):
    to_chars(v < 0, v, 8, None, p, s, "time_t", archive_format, termch)


def uid_substitute(negative):
    r = pwd.getpwnam("nobody").pw_uid
    negative[0] = False
    return r


def uid_to_chars(v, p, s, archive_format, termch):
    to_chars(v < 0, v, 4, uid_substitute, p, s, "uid_t", archive_format, termch)


def uintmax_to_chars(v, p, s, archive_format, termch):
    to_chars(False, v, 8, None, p, s, "uintmax_t", archive_format, termch)
