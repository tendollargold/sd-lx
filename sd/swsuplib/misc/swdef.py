# swdef.py: write attribute/value pairs in metadata file.
#
#  Copyright (C) 1998  James H. Lowe, Jr.  <jhlowe@acm.org>
#  Copyright (C) 2023 Paul Weber convert to python
#  All Rights Reserved.
#
#  COPYING TERMS AND CONDITIONS
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
from sd.swsuplib.uxfio import uxfio_write
from sd.swsuplib.misc.swlib import swlib_expand_escapes
from sd.swsuplib.swparse.swparse import swpc

def swdef_write_attribute_to_buffer(buf, keyword, value, level, attribute_type):
    ret = swdef_write_keyword_to_buffer(buf, keyword, level, attribute_type)
    if attribute_type != swpc.SWPARSE_MD_TYPE_OBJ:
        iret = swdef_write_value_to_buffer(buf, value)
        if iret < 0:
            return -1
        ret += iret
    return ret


def swdef_write_attribute(keyword, value, level, xlen, attribute_type, uxfio_fd):
    ret = swdef_write_keyword(keyword, level, attribute_type, uxfio_fd)
    if attribute_type != swpc.SWPARSE_MD_TYPE_OBJ:
        iret = swdef_write_value(value, uxfio_fd, xlen, 0)
        if iret < 0:
            return -1
        ret += iret
    return ret


def swdef_write_keyword_to_buffer(ob, keyword, level, attribute_type):
    sp = "        "
    if level > len(sp):
        level = 1
    if level:
        ob += sp[:level]
    ob += keyword
    if attribute_type == swpc.SWPARSE_MD_TYPE_OBJ:
        ob += "\n"
    else:
        ob += " "
    return len(ob)


def swdef_write_keyword(keyword, level, attribute_type, uxfio_fd):
    ob = ""
    ret = swdef_write_keyword_to_buffer(ob, keyword, level, attribute_type)
    iret = uxfio_write(uxfio_fd, ob, ret)
    return iret


def swdef_write_value_to_buffer(ob, value):
    newbuf = None
    newlen = 0

    if "\n" in value:
        value = value.replace("\n", "")
    newbuf, newlen = swlib_expand_escapes(newbuf, newlen, value, None)
    if newlen == 0:
        ob += "\"\""
        ret = 2
    elif "\\n" in value or "\n" in value or "#" in value or "\"" in value or "\\" in value:
        ob += "\""
        ret = 1
        ob += newbuf
        ret += newlen
        ob += "\""
        ret += 1
    else:
        ob += value
        ret = len(value)
    ob += "\n"
    ret += 1
    if newbuf:
        del newbuf
    return ret


def swdef_write_value(value, uxfio_ofd, value_length, quote_off):
    newbuf = None
    newlen = 0
    ret = 0
    if value_length > 0:
        ret += uxfio_write(uxfio_ofd, value, value_length)
    elif not value:
        buf0 = "\"\""
        ret = uxfio_write(uxfio_ofd, buf0, len(buf0))
    else:
        if value_length < 0:
            if "\n" in value:
                value = value.replace("\n", "")
        newbuf, newlen = swlib_expand_escapes(newbuf, newlen, value, None)
        if not newlen:
            buf0 = "\"\""
            ret = uxfio_write(uxfio_ofd, buf0, len(buf0))
        elif "\\n" in value or "\n" in value or "<" in value or ">" in value or "#" in value or "\"" in value or "\\" in value:
            buf0 = "\""
            if quote_off == 0:
                ret = uxfio_write(uxfio_ofd, buf0, len(buf0))
            ret += uxfio_write(uxfio_ofd, newbuf, newlen)
            if quote_off == 0:
                ret += uxfio_write(uxfio_ofd, buf0, len(buf0))
        else:
            ret = uxfio_write(uxfio_ofd, value, len(value))
    buf0 = "\n"
    ret += uxfio_write(uxfio_ofd, buf0, len(buf0))
    if newbuf:
        del newbuf
    return ret


