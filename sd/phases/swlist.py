# phases.swlist.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals
from sd.cli.demand import bool

# Extended Options
"""
    The swlist utility supports the following extended options. The description in 
    Chapter 3 applies. see sd.defaults
"""
swlistoptions={
    'distribution_target_directory': '/var/spool/sw',
    'installed_software_catalog': '/var/adm/sw/products',
    'one_liner': 'implementation_defined_value',
    'select_local': bool(True),
    'software': "",
    'targets': ""
}
def swlist_options():
    """Collection of options for swlist utility. api"""
    swlistoptions.update(globals())


# 4.0 Execution Phase Systems Management: Distributed Software Administration
# swlist utility Page 105 - 107

# Check for extended options?

# There are two phases in the swlist utility:
#    1. Selection phase
#    2. Execution phase

#
# Selection Phase
#

# If there are no software selections specified, then all software from the 
# catalog is processed.  Otherwise, each selection added to the selected software
# list must satisfy the following validation check.

#  · If the selection is not available from the catalog file, generate an event.
#    [SW_ERROR: SW_SELECTION_NOT_FOUND]

# Unlike all other commands, swlist includes all software that matches a 
# specification, even if the specification is ambiguous.

# Implementations that support the removed state need to address swlist just as
# they do for source depots with swinstall. These implementations should at least
# document the behavior for swlist and other operations on removed filesets. In
# particular, for swlist, if attempting to list a fileset from the source that
# has a removed state, the behavior should be the same as if it was not there,
# generating an event.
# (SW_ERROR: SW_SELECTION_NOT_FOUND)

# When listing a sparse fileset in a depot or in installed software, there are no differences when filesets is sparse or normal.

#
# Execution Phase
#

# The attributes for the selections determined from the previous phase are listed in 
# the formats defined by the options.

