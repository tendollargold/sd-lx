vendor
tag HP
title "Hewlett-Packard Company"
description "Software Products from Hewlett-Packard"
end
bundle
tag OnlineDiag
data_model_revision 2.40
instance_id 1
control_directory OnlineDiag
revision B.11.23.10.05
title "HPUX 11.23 Support Tools Bundle, December 2007"
description "Vendor Name                     Hewlett-Packard

Product Name                    Support Tools Manager (Series 700/800)

The Support Tool Manager provides a simple interface to support tools.  
It has several features: three user interfaces, a wide range of support tools,
complete on-line help, and more.

Three intuitive, easy to use interfaces are available: a graphical interface 
(xstm), a menu interface (mstm), and a command line interface (cstm):  

   XSTM (graphical user interface) - This user interface is Motif-based, 
      and is designed for use with X terminals.  It provides a map of the
      system using icons, built-in help, the ability to invoke all available 
      support tools for each hardware device, access to log files created 
      by tools as well as STM, and many other functions.  Operation states 
      are indicated by color changes to the icons.  Additional \"pull-down\" 
      menus are available to view log files and abort pending operations.

   MSTM (menu interface) - Features the same map, help, and test capabilities 
      as xstm, but on a character based terminal. Mstm is designed to provide
      close to the same level of intuitiveness as xstm for users who do not
      have access to a graphical display terminal. This interface is soft-key
      driven and can run on any character based terminal.
   
   CSTM (command line interface) - Features the same map, help, and test
      capabilities as xstm and mstm, but in a strict command line environment
      that is suitable for being driven by scripting tools. This interface
      will run on any character based terminal and uses no special display 
      enhancements.
   
To use the support tools, the user first invokes one of the user interface 
modules (CSTM, XSTM, or MSTM), and enters appropriate commands to specify 
the type of tool to be performed. 

The following types of tool are supported:

Information Provide complete information about the device. Typical items that
Tool        are reported by these tools include product numbers, device type,
	    firmware revision numbers, on-board log information, etc.

Verifier    Perform a simple test of component function, providing a 
	    \"pass/fail\" indication of device condition; typically, this is 
	    the first level test of a device's condition.

Diagnostic  Perform as complete a test as possible on the device and isolate
            any errors to the faulty hardware on that device. 

Exerciser   Continuously stresses the device or subsystem. These tools are  
	    useful in providing very high confidence verification, and in 
	    detecting intermittent errors.  

Expert      Designed to facilitate low-level trouble-shooting of hardware.
Tool        These tools typically require the user to have expert knowledge
	    of the device and usually require a support class/node license 
	    to run. These tools are fully interactive.

Firmware    Designed to download firmware to devices which provide this
Update      capability. These tools are fully interactive.
Tool

As tools are run, their progress can be monitored by looking at the hardware
map provided by the user interface. Once a tool has completed its testing, 
the results can be viewed by looking at the activity log and/or failure log
that the tool may have created.

Support Tools Manager requires an HP 9000 Series 700/800 system executing 
HP-UX version 10.xx

The Support Tools Manager (Series 700/800) product is structured as follows:

    Subproduct: Runtime
    Filesets:   STM-SHLIBS, STM-UI-RUN, STM-UUT-RUN, STM-CATALOGS

    Subproduct: Manuals
    Filesets:   STM-MAN


"
mod_time 1554359518
create_time 1554347570
install_date 201904040231.58
architecture HP-UX_B.11.23_IA/PA
machine_type ia64*|9000/*
os_name HP-UX
os_release B.11.[23]*
os_version *
install_source localhost:/tmp/ign_configure/SD_CDROM
category_tag HPUXAdditions
vendor_tag HP
directory /
is_locatable true
location /
contents Contrib-Tools.CONTRIB,r=B.11.23.10.05,a=HP-UX_B.11.23_IA,v=HP
contents Sup-Tool-Mgr.STM-REL-NOTES,r=B.11.23.10.05,a=HP-UX_B.11.23_IA,v=HP
contents Sup-Tool-Mgr.STM-CATALOGS,r=B.11.23.10.05,a=HP-UX_B.11.23_IA,v=HP
contents Sup-Tool-Mgr.STM-MAN,r=B.11.23.10.05,a=HP-UX_B.11.23_IA,v=HP
contents Sup-Tool-Mgr.STM-SHLIBS,r=B.11.23.10.05,a=HP-UX_B.11.23_IA,v=HP
contents Sup-Tool-Mgr.STM-UI-RUN,r=B.11.23.10.05,a=HP-UX_B.11.23_IA,v=HP
contents Sup-Tool-Mgr.STM-UUT-RUN,r=B.11.23.10.05,a=HP-UX_B.11.23_IA,v=HP
contents Contrib-Tools.CONTRIB,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Contrib-Tools.PDCINFO,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Contrib-Tools.PIMTOOL,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Sup-Tool-Mgr.STM-REL-NOTES,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Sup-Tool-Mgr.STM-CATALOGS,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Sup-Tool-Mgr.STM-MAN,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Sup-Tool-Mgr.STM-SHLIBS,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Sup-Tool-Mgr.STM-UI-RUN,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents Sup-Tool-Mgr.STM-UUT-RUN,r=B.11.23.10.05,a=HP-UX_B.11.23_PA,v=HP
contents EMS-Config.EMS-GUI,r=A.04.20.23,a=HP-UX_B.11.23_IA/PA,v=HP
contents EMS-Config.EMS-GUI-COM,r=A.04.20.23,a=HP-UX_B.11.23_IA/PA,v=HP
contents EMS-Core.EMS-CORE,r=A.04.20.23,a=HP-UX_B.11.23_IA/PA,v=HP
contents EMS-Core.EMS-CORE-COM,r=A.04.20.23,a=HP-UX_B.11.23_IA/PA,v=HP
contents EMS-Core.EMS-MX,r=A.04.20.23,a=HP-UX_B.11.23_IA/PA,v=HP
is_reference true
hp_ii "factory_integrate=TRUE;title=OnlineDiag_ii;desktop=FALSE;always_load=TRUE"
hp_srdo "swtype=I;user=B;bundle_type=C"
