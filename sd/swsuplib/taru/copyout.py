# copyout.py:
#
#   Copyright (C) 1998, 1999, 2014 Jim Lowe
#   Copyright (C) 2024 Paul Weber Conversion to Python

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import sys
from ctypes import c_uint

import math

from sd.debug import E_DEBUG, E_DEBUG2
from sd.swsuplib.cplob import cplob_val
from sd.swsuplib.misc.cstrings import memcpy
from sd.swsuplib.atomicio import atomicio
from sd.swsuplib.misc.swlib import FrameRecord, swlib_fatal, SWLIB_ASSERT, swlib_get_pax_header_pid, swlib_basename
from sd.swsuplib.misc.swlib import swlib_utilname_get, swlib_imaxtostr, swlib_umaxtostr, swlib_pump_amount8, SWLIB_FATAL
from sd.swsuplib.misc.swvarfs import swvarfs_u_readlink
from sd.swsuplib.strob import strob_chr_index, strob_sprintf_at, strob_str, strob_memcpy_at, strob_strcpy, strob_strlen, \
    strob_sprintf, STROB_DO_APPEND, strob_close, STROB
from sd.swsuplib.taru.ahs import ahsstatic_strip_name_leading_slash, ahsstaticgettarusername, ahsstaticdeletefilehdr, \
    ahsstaticgettargroupname, ahsstaticsettarfilename, ahsstaticsetpaxlinkname
from sd.swsuplib.taru.ahs import ahsstaticsettarlinknamelength, ahsstaticsettarfilenamelength, ahsstaticgettarlinkname, ahsstaticgettarfilename, ahsstaticcreatefilehdr
from sd.swsuplib.taru.cpiohdr import chc, EXATT, OldCpioHeader
from sd.swsuplib.taru.defer import defer_set_taru, defer_is_last_link, defer_writeout_zero_length_defers, defer_add_link_defer
from sd.swsuplib.taru.filetypes import ft
from sd.swsuplib.taru.hllist import hllist_close, hllist_add_record, hllist_find_file_entry, hllist_open, hllist_disable_add, hllist_disable_find
from sd.swsuplib.taru.etar import Etar
from sd.swsuplib.taru.mtar import (taru_is_tar_linkname_too_long, taru_filehdr2filehdr, taru_statbuf2filehdr,
    taru_is_tar_filename_too_long, taru_write_long_link_member)
from sd.swsuplib.taru.otar import taru_tape_skip_padding, taru_set_filehdr_sysdb_nameid_by_policy, taru_write_out_tar_header2
from sd.swsuplib.taru.porinode import porinode_make_next_inode
from sd.swsuplib.taru.tar import tarc
from sd.swsuplib.taru.taru import tc, af,taru_digs_delete, taru_read_in_tar_header2, taru_print_tar_ls_list, taru_digs_init, taru_fill_exthdr_needs_mask, taru_write_preview_line
from sd.swsuplib.uxfio import uxfio_lseek, uxfio_sfread, uxfio_write, UXFIO_FD_MIN


def taru_write_archive_member_data_(taru, file_hdr0, out_file_des, pad_out_file_des, input_fd, fout, archive_format_out,
                                    adjuct_ofd, digs):
    fd = -1
    ret = 0
    tmpp = ahsstaticgettarlinkname(file_hdr0)

    sys.stderr.write("")

    if ((file_hdr0.c_mode & ft.CP_IFMT) == ft.CP_IFREG) and (tmpp == str(None) or len(tmpp) == 0):
        # sys.stderr.write("")
        if input_fd < 0:
            sys.stderr.write("")
            fd = os.open(ahsstaticgettarfilename(file_hdr0), os.O_RDONLY, 0)
            if fd < 0:
                sys.stderr.write("open %s: %s\n" + ahsstaticgettarfilename(file_hdr0) + os.strerror(1))
                return -1
            input_fd = fd

        if fout is None:
            E_DEBUG("")
            ret = swlib_pump_amount8(out_file_des, input_fd, int(file_hdr0.c_filesize), adjuct_ofd, digs)
            # sys.stderr.write("ret=%s", swlib_imaxtostr(ret, NULL))
            print("{}: error in taru_write_archive_member_data: ret={}".format(swlib_utilname_get(),
                                                                               swlib_imaxtostr(ret, None)))
            if ret < 0:
                sys.stderr.write("")
                if fd >= 0:
                    os.close(fd)
                sys.stderr.write(
                    "%s: error in taru_write_archive_member_data: ret=%s\n" + swlib_utilname_get() + swlib_imaxtostr(
                        ret, None))
                return -1
            else:
                E_DEBUG("")
                if ret != int(file_hdr0.c_filesize):
                    sys.stderr.write("Error: ret=%s" + swlib_imaxtostr(ret, None))
                    sys.stderr.write("Error: filesize=%s" + swlib_umaxtostr(file_hdr0.c_filesize, None))
                    sys.stderr.write(
                        "%s: taru_write_archive_member_data_ internal error. ret=%s\n" + swlib_utilname_get() + swlib_imaxtostr(
                            ret, None))
                    return -1
                else:
                    # sys.stderr.write("")
                    #
                    # sys.stderr.write( "%s loaded OK\n", ahsstaticgettarfilename(file_hdr0))
                    #
                    pass
                E_DEBUG("")
                if (ret + taru_tape_pad_output(pad_out_file_des, int(file_hdr0.c_filesize), archive_format_out)) < 0:
                    E_DEBUG("")
                    if fd >= 0:
                        os.close(fd)
                    return -1
                E_DEBUG("")
                if adjuct_ofd >= 0:
                    if taru_tape_pad_output(adjuct_ofd, int(file_hdr0.c_filesize), archive_format_out) < 0:
                        if fd >= 0:
                            os.close(fd)
                        return -1
            E_DEBUG("")
        else:
            E_DEBUG("")
            ret = fout.invoke(out_file_des)
            E_DEBUG2("ret={}".format(ret), "blah")
        if fd >= 0:
            os.close(fd)
    E_DEBUG2("ret={}".format(swlib_imaxtostr(ret, None)), None)
    return ret


def tarui_tape_buffered_write(in_buf, out_des, num_bytes, header_buffer):
    if header_buffer:
        # C++ TO PYTHON CONVERTER TASK: The memory management function 'memcpy' has no equivalent in Python:
        memcpy(header_buffer, in_buf, num_bytes)
        return 0
    else:
        return taru_safewrite(out_des, in_buf, num_bytes)


def tape_clear_rest_of_block(out_file_des, zeros_512, io_block_size, output_size):
    rem = int(math.fmod(output_size, int(io_block_size)))
    n = rem
    ret = 0
    retval = 0
    while n < io_block_size and ret >= 0:
        if (io_block_size - n) > 512:
            ret = tarui_tape_buffered_write(zeros_512, out_file_des, 512, str(None))
        else:
            ret = tarui_tape_buffered_write(zeros_512, out_file_des, io_block_size - n, str(None))
        if ret > 0:
            n += ret
            retval += ret
        else:
            sys.stderr.write("taru: tape_clear_rest_of_block: error writing trailer block.\n")
    return retval


def taru_read_for_crc_checksum(in_file_des, file_size, file_name):
    buf = str(['\0' for _ in range(tc.TARU_BUFSIZ)])
    bytes_read = 0
    crc = 0
    for bytes_left in range(file_size, 0, -bytes_read):
        bytes_read = uxfio_sfread(in_file_des, buf, tc.TARU_BUFSIZ)
        if bytes_read < 0:
            sys.stderr.write("cannot read checksum for %s" + file_name)
        if bytes_read == 0:
            break
        for i in range(0, bytes_read):
            crc += int(buf[i]) & 0xff
    return crc


def tarui_write_archive_member_(taru, file_stat, file_hdr0, linkrecord, defer, porinode, out_file_des, input_fd,
                                archive_format_out, tarheaderflags):
    ret1 = 0
    file_hdr = ahsstaticcreatefilehdr()

    file_hdr0.c_chksum = 0
    if archive_format_out == af.arf_crcascii and input_fd >= 0:
        if file_stat:
            sz = file_stat.st_size
        else:
            sz = file_hdr0.c_filesize
        old_pos = (os.lseek(input_fd, 0, os.SEEK_CUR))
        sum_ = taru_read_for_crc_checksum(input_fd, sz, "")
        uxfio_lseek(input_fd, old_pos, os.SEEK_SET)
        file_hdr0.c_chksum = sum_
    ret = taru_write_archive_member_header(taru, file_stat, file_hdr0, linkrecord, defer, porinode, out_file_des,
                                           archive_format_out, file_hdr, tarheaderflags)

    if ret < 0:
        sys.stderr.write(
            "error in taru_write_archive_member_header() called from" + " tarui_write_archive_member_().\n")
        ahsstaticdeletefilehdr(file_hdr)
        return ret
    if ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFREG) and (
            ahsstaticgettarlinkname(file_hdr) == str(None) or len(ahsstaticgettarlinkname(file_hdr)) == 0) and ret > 0:
        ret1 = taru_write_archive_member_data(taru, file_hdr, out_file_des, input_fd, int, archive_format_out, -1,
                                              file_hdr0.digs)
        if ret1 < 0:
            ahsstaticdeletefilehdr(file_hdr)
            return ret1
    else:
        if file_hdr0.digs:
            taru_digs_init(file_hdr0.digs, chc.DIGS_ENABLE_CLEAR, 0)
    ahsstaticdeletefilehdr(file_hdr)
    return ret + ret1


def get_ilen(rlen):
    i = 0
    j = 0
    r = rlen

    while math.trunc(r / float(10)) >= 1:
        i += 1
        r = math.trunc(r / float(10))
    i += 1

    # i is now the approximate guessed length of the record
    # Now it must be determined iteratively if this it is correct
    # when the length of the leading decimal length string is included.
    #

    r = rlen + i

    # r is the first guess at the total length

    while i < 99:
        j = 0
        while math.trunc(r / float(10)) >= 1:
            j += 1
            r = math.trunc(r / float(10))
        j += 1
        if i == j:
            return i + rlen
        i += 1
        r = rlen + i
    # should never get here
    return -1


def construct_exthdr_record(buf, initial_offset, keyword, value, length):
    #
    #   "%d %s=%s\n", length, keyword, value
    #

    rlen = 1 + len(keyword) + length + 1 + 1

    # rlen is the length not including the rendering of the decimal length

    ret = get_ilen(rlen)
    if ret < 0:
        # This should really never happen
        sys.stderr.write("internal error in construct_exthdr_record loc=1\n")
        sys.exit(120)
    rlen = ret

    # rlen is now the record length
    strob_sprintf_at(buf, initial_offset, "%d %s=", rlen, keyword)

    ret = len(strob_str(buf) + initial_offset)
    strob_memcpy_at(buf, initial_offset + ret, value, int(length))

    # sanity check
    if (ret + length + 1) != rlen:
        # This should really never happen
        sys.stderr.write("internal error in construct_exthdr_record loc=2\n")
        sys.exit(121)

    # Now append the '\n' newline

    strob_memcpy_at(buf, initial_offset + rlen - 1, "\n", 1)

    return rlen


def write_exthdrs_xattr_records(taru, file_hdr, data_len):
    p = EXATT()
    buf = taru.exthdr_data
    # strob_strcpy(buf, "")
    extlist = file_hdr.extattlist

    index = 0
    # FIXME
    # while extlist and (p == EXATT(cplob_val(extlist, index))) and p.is_in_use:
    while extlist and (p == cplob_val(extlist, index)) and p.is_in_use:
        #
        # construct the dot delimited attribute name e.g. "SCHILLY.selinux.access"
        #
        strob_sprintf(taru.localbuf, 0, "%s%s%s%s%s", p.vendor, "." if len(p.vendor) != 0 else "", p.namespace,
                      "." if len(p.namespace) != 0 else "", strob_str(p.attrname))
        value = strob_str(p.attrvalue)
        valuelen = p.len
        temp_ref_value = value
        ret = construct_exthdr_record(buf, data_len, strob_str(taru.localbuf), temp_ref_value, valuelen)

        if ret < 0:
            return -1
        data_len += ret
        # p->is_in_use == 0;
        index += 1
    return data_len


def write_exthdrs_ustar_records(taru, file_hdr):
    buf = taru.exthdr_data
    # strob_strcpy(buf, ""

    data_len = 0
    if (file_hdr.extHeader_needs_mask & (1 << 6)) != 0:
        temp_ref_pax_kw_linkpath = tc.PAX_KW_LINKPATH
        ret = construct_exthdr_record(buf, data_len, temp_ref_pax_kw_linkpath, ahsstaticgettarlinkname(file_hdr),
                                      len(ahsstaticgettarlinkname(file_hdr)))
        if ret < 0:
            return -1

        sys.stderr.write("LINKPATH: [%s]" + ahsstaticgettarlinkname(file_hdr))
        #
        # Now truncate the linkname to TARNAMESIZE,  this seems to be what GNU tar does
        #
        strob_chr_index(file_hdr.c_tar_linkname, tarc.TARNAMESIZE, '\0')
        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 8)) != 0:
        temp_ref_pax_kw_path = tc.PAX_KW_PATH
        ret = construct_exthdr_record(buf, data_len, temp_ref_pax_kw_path, ahsstaticgettarfilename(file_hdr),
                                      len(ahsstaticgettarfilename(file_hdr)))
        if ret < 0:
            return -2

        sys.stderr.write("PATH: [%s]" + ahsstaticgettarfilename(file_hdr))
        #
        # Now truncate the name to TARNAMESIZE,  this seems to be what GNU tar does
        #
        strob_chr_index(file_hdr.c_name, tarc.TARNAMESIZE, '\0')
        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 11)) != 0:
        strob_sprintf(taru.localbuf, 0, "%lu", file_hdr.c_filesize)
        temp_ref_pax_kw_size = tarc.PAX_KW_SIZE
        ret = construct_exthdr_record(buf, data_len, temp_ref_pax_kw_size, strob_str(taru.localbuf),
                                      strob_strlen(taru.localbuf))
        if ret < 0:
            return -3

        #
        # Now set the filesize to a arbitrary number that fits
        #

        file_hdr.c_filesize = tarc.TAR_MAX_SIZE

        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 12)) != 0:
        strob_sprintf(taru.localbuf, 0, "%lu", file_hdr.c_uid)
        temp_ref_pax_kw_uid = tarc.PAX_KW_UID
        ret = construct_exthdr_record(buf, data_len, temp_ref_pax_kw_uid, strob_str(taru.localbuf),
                                      strob_strlen(taru.localbuf))
        if ret < 0:
            return -4
        #
        #        * Now set the value to a number that will fit
        #
        file_hdr.c_uid = c_uint(tarc.TAR_MAX_7OCTAL)
        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 4)) != 0:
        strob_sprintf(taru.localbuf, 0, "%lu", file_hdr.c_gid)
        temp_ref_pax_kw_gid = tarc.PAX_KW_GID
        ret = construct_exthdr_record(buf, data_len, temp_ref_pax_kw_gid, strob_str(taru.localbuf),
                                      strob_strlen(taru.localbuf))
        if ret < 0:
            return -5
        #
        #        * Now set the value to a number that will fit
        #
        file_hdr.c_uid = c_uint(tarc.TAR_MAX_7OCTAL)
        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 13)) != 0:
        temp_ref_pax_kw_uname = tarc.PAX_KW_UNAME
        ret = construct_exthdr_record(buf, data_len, temp_ref_pax_kw_uname, ahsstaticgettarusername(file_hdr),
                                      len(ahsstaticgettarusername(file_hdr)))
        if ret < 0:
            return -6
        #
        #        * Now set the string that will fit in a ustar header by truncation
        #
        strob_chr_index(file_hdr.c_username, tarc.THB_FL_UNAME - 1, '\0')

        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 5)) != 0:
        temp_ref_pax_kw_gname = tarc.PAX_KW_GNAME
        ret = construct_exthdr_record(buf, data_len, temp_ref_pax_kw_gname, ahsstaticgettargroupname(file_hdr),
                                      len(ahsstaticgettargroupname(file_hdr)))
        if ret < 0:
            return -7
        #
        # Now set the string that will fit in a ustar header by truncation
        #
        strob_chr_index(file_hdr.c_groupname, tarc.THB_FL_gname - 1, '\0')
        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 15)) != 0:
        strob_sprintf(taru.localbuf, 0, "%lu", file_hdr.c_dev_maj)
        ret = construct_exthdr_record(buf, data_len, tc.SW_VENDOR + "." + tarc.PAX_KW_DEVMAJOR,
                                      strob_str(taru.localbuf), strob_strlen(taru.localbuf))
        if ret < 0:
            return -8
        #
        # Now set the value to a number that will fit
        #
        file_hdr.c_dev_maj = c_uint(tarc.TAR_MAX_7OCTAL)
        data_len += ret

    if (file_hdr.extHeader_needs_mask & (1 << 16)) != 0:
        strob_sprintf(taru.localbuf, 0, "%lu", file_hdr.c_dev_min)
        ret = construct_exthdr_record(buf, data_len, tc.SW_VENDOR + "." + tarc.PAX_KW_DEVMINOR,
                                      strob_str(taru.localbuf), strob_strlen(taru.localbuf))
        if ret < 0:
            return -9
        #
        # Now set the value to a number that will fit
        #
        file_hdr.c_dev_min = c_uint(tarc.TAR_MAX_7OCTAL)
        data_len += ret

    return data_len


def taru_safewrite(fd, vbuf, amount):
    rc = 0
    buf = vbuf

    while amount != 0 and ((rc := uxfio_write(fd, buf, amount)) < int(amount)) and rc > 0:
        buf += rc
        amount -= rc

    if rc < 0:
        return -(int(amount))
    return int(amount)


def taru_write_archive_trailer(taru, archive_format_out, fd, io_block_size, bytes_written, flags):
    ret = 0
    retval = 0
    file_hdr = ahsstaticcreatefilehdr()
    # Output header information.
    zeros_512b = bytearray(512)
    gnu_tar_block_size = 10240
    zeros_512 = bytes(zeros_512b)

    if io_block_size != 0:
        gnu_tar_block_size = io_block_size

    if archive_format_out != af.arf_tar and archive_format_out != af.arf_ustar:
        io_block_size = 512
        file_hdr.c_ino = 0
        file_hdr.c_mode = 0
        file_hdr.c_uid = 0
        file_hdr.c_gid = 0
        file_hdr.c_nlink = 1  # Must be 1 for crc format.
        file_hdr.c_dev_maj = 0
        file_hdr.c_dev_min = 0
        file_hdr.c_rdev_maj = 0
        file_hdr.c_rdev_min = 0
        file_hdr.c_mtime = 0
        file_hdr.c_chksum = 0
        file_hdr.c_filesize = 0
        file_hdr.c_namesize = 11
        # temp_ref_CPIO_INBAND_EOA_FILENAME = CPIO_INBAND_EOA_FILENAME
        ahsstaticsettarfilename(file_hdr, tc.CPIO_INBAND_EOA_FILENAME)
        ret = taru_write_out_header(taru, file_hdr, fd, archive_format_out, str(None), 0)
        if bytes_written != 0:
            if ret > 0:
                bytes_written += ret
            ret += tape_clear_rest_of_block(fd, zeros_512, io_block_size, bytes_written)
    else:
        if bytes_written % 512:
            tmpbuf = str(bytes_written)
            print("internal error, bytes written not multiple of 512, bytes=" + tmpbuf, file=sys.stderr)
            flags = 0
            retval = 1
        if flags & tc.TARU_TAR_GNU_BLOCKSIZE_B1:
            nblock = 2
            if flags & tc.TARU_TAR_BE_LIKE_STAR:
                nblock = 3
        elif flags & tc.TARU_TAR_GNU_OLDGNUTAR:
            n_tar_blocks = gnu_tar_block_size / 512
            nblock = (bytes_written % gnu_tar_block_size) / 512
            nblock = n_tar_blocks - nblock
            if nblock < 2:
                nblock += n_tar_blocks
        else:
            nblock = 2
        while nblock > 0:
            ret += tarui_tape_buffered_write(zeros_512, fd, 512, None)
            nblock -= 1
    ahsstaticdeletefilehdr(file_hdr)
    if retval:
        return -1
    return ret

def taru_write_archive_member(taru, filename, statbuf, file_hdr0, linkrec, defer, porinode, ofd, in_fd, format, tarheaderflags):
    linkname = [None] * 101
    file_hdr = ahsstaticcreatefilehdr()
    nullfd = -1
    vlen = 1024
    E_DEBUG("ENTERING")
    E_DEBUG2("filename=[%s]", filename)
    linkname.value = b''
    if statbuf:
        E_DEBUG("statbuf")
        taru_statbuf2filehdr(file_hdr, statbuf, None, filename, linkname)
    elif file_hdr0:
        E_DEBUG("file_hdr0")
        taru_filehdr2filehdr(file_hdr, file_hdr0)
    else:
        E_DEBUG("else")
        #if os.lstat(filename, st) == 0:
        try:
            st = os.lstat(filename)
            statbuf = st
            E_DEBUG("lstat")
            taru_statbuf2filehdr(file_hdr, statbuf, None, filename, linkname)
        except OSError as e:
            print(f"swbis: {filename} {os.error(e.errno)}")
            ahsstaticdeletefilehdr(file_hdr)
            return -1
    fd = in_fd
    if fd < 0:
        if file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFDIR:
            if os.access(filename, os.R_OK | os.X_OK) != 0:
                print(f"swbis: {os.error()} {filename}")
                ahsstaticdeletefilehdr(file_hdr)
                return 0
        elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFREG:
            if file_hdr.c_filesize > 0:
                fd = os.open(filename, os.O_RDONLY, 0)
            else:
                fd = os.open("/dev/null", os.O_RDWR, 0)
                nullfd = fd
            if fd < 0:
                print(f"swbis: {os.error()} {filename}")
                ahsstaticdeletefilehdr(file_hdr)
                return 0
        elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFLNK:
            ahsstaticsettarlinknamelength(file_hdr, vlen)
            p = ahsstaticgettarlinkname(file_hdr)
            p[vlen - 1] = '\0'
            ret = swvarfs_u_readlink(None, filename, p, vlen - 1)
            if ret < 0:
                print(f"swbis: {os.error()} {filename}")
    ret = tarui_write_archive_member_(taru, statbuf, file_hdr, linkrec, defer, porinode, ofd, fd, format, tarheaderflags)
    if 0 <= fd != nullfd:
        os.close(fd)
    else:
        os.close(fd)
    ahsstaticdeletefilehdr(file_hdr)
    return ret


def taru_write_archive_member_header(taru, file_stat, file_hdr0, linkrecord, defer, porinode, out_file_des, archive_format_out, file_hdr_return, tarheaderflags):
    do_gnu_long_link = 0
    do_gnu_long_name = 0
    do_gnu_long_link_name_bytes = 0
    do_gnu_long_link_link_bytes = 0
    nfound = 0
    retval = 0
    v_dev = 0
    v_ino = 0
    do_strip_leading_slash = 0

    tar_iflags_numeric_uids = tarheaderflags & tarc.TARU_TAR_NUMERIC_UIDS

    file_hdr = ahsstaticcreatefilehdr()
    linkrecord_active = linkrecord is not None and taru.linkrecord_disableM == 0
    if taru:
        do_strip_leading_slash = tarheaderflags & tarc.TARU_TAR_DO_STRIP_LEADING_SLASH
    taru_filehdr2filehdr(file_hdr, file_hdr0)
    SWLIB_ASSERT(file_stat is None)
    if porinode and not file_stat and (archive_format_out == af.arf_oldascii or archive_format_out == af.arf_newascii or archive_format_out == af.arf_crcascii):
        mylink = file_hdr.c_nlink
        if file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFLNK or file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFDIR:
            mylink = 1
        porinode_make_next_inode(porinode, mylink, os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min), file_hdr.c_ino, v_dev, v_ino)
        file_hdr.c_dev_maj = os.major(v_dev)
        file_hdr.c_dev_min = os.minor(v_dev)
        file_hdr.c_ino = v_ino
    if do_strip_leading_slash:
        ahsstatic_strip_name_leading_slash(file_hdr)
    # is_dir = file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFDIR
    c_type = file_hdr.c_mode & ft.CP_IFMT
    file_hdr.c_namesize = len(ahsstaticgettarfilename(file_hdr)) + 1
    if archive_format_out == af.arf_tar or archive_format_out == af.arf_ustar:
        is_too_long = taru_is_tar_filename_too_long(ahsstaticgettarfilename(file_hdr), tarheaderflags, do_gnu_long_name, 0)
        if is_too_long:
            print(f"{swlib_utilname_get()}: file name too long, not dumped: {ahsstaticgettarfilename(file_hdr)}", file=sys.stderr)
            ahsstaticdeletefilehdr(file_hdr)
            return -2
        is_too_long = taru_is_tar_linkname_too_long(ahsstaticgettarlinkname(file_hdr), tarheaderflags, do_gnu_long_link)
        if is_too_long:
            print(f"{swlib_utilname_get()}: link name too long, not dumped: {ahsstaticgettarfilename(file_hdr)}", file=sys.stderr)
            return -3
        is_pax_header_allowed = tarheaderflags & tarc.TARU_TAR_PAXEXTHDR or tarheaderflags & tarc.TARU_TAR_PAXEXTHDR_MD
        needy_tar_header = taru_fill_exthdr_needs_mask(file_hdr, tarheaderflags, is_pax_header_allowed, None)
        if needy_tar_header and is_pax_header_allowed == 0 and tarheaderflags & tarc.TARU_TAR_NO_OVERFLOW:
            return -14
        file_hdr.extHeader_needs_maskM = needy_tar_header
        if tarheaderflags & tarc.TARU_TAR_PAXEXTHDR or tarheaderflags & tarc.TARU_TAR_PAXEXTHDR_MD:
            if tar_iflags_numeric_uids == 0:
                taru_set_filehdr_sysdb_nameid_by_policy("G", file_hdr, '\0', tar_iflags_numeric_uids)
                taru_set_filehdr_sysdb_nameid_by_policy("U", file_hdr, '\0', tar_iflags_numeric_uids)
            do_gnu_long_name = 0
            do_gnu_long_link = 0
        if do_gnu_long_name | do_gnu_long_link:
            if do_gnu_long_link_name_bytes := taru_write_long_link_member(taru, out_file_des, ahsstaticgettarfilename(file_hdr), tarc.GNUTYPE_LONGNAME, c_type, tarheaderflags) != 1024:
                print(f"internal error writing GNU extension LongLink: {ahsstaticgettarfilename(file_hdr)}", file=sys.stderr)
                return -3
            if tarheaderflags & tarc.TARU_TAR_GNU_OLDGNUTAR or tarheaderflags & tarc.TARU_TAR_NAMESIZE_99 or 0:
                ahsstaticsettarfilenamelength(file_hdr, tarc.OLDGNU_TARNAMESIZE + 1)
                ahsstaticgettarfilename(file_hdr)[tarc.OLDGNU_TARNAMESIZE] = '\0'
            else:
                ahsstaticsettarfilenamelength(file_hdr, tarc.TARNAMESIZE + 1)
                ahsstaticgettarfilename(file_hdr)[tarc.TARNAMESIZE] = '\0'
        elif tarheaderflags & tarc.TARU_TAR_PAXEXTHDR or tarheaderflags & tarc.TARU_TAR_PAXEXTHDR_MD and (file_hdr.extHeader_needs_maskM or file_hdr.extattlistM):
            taru_set_filehdr_sysdb_nameid_by_policy("G", file_hdr, '\0', tar_iflags_numeric_uids)
            taru_set_filehdr_sysdb_nameid_by_policy("U", file_hdr, '\0', tar_iflags_numeric_uids)
            strob_strcpy(taru.exthdr_data, "")
            do_exthdr_bytes = write_exthdrs_ustar_records(taru, file_hdr)
            if do_exthdr_bytes < 0:
                print(f"internal error writing pax extended header POSIX attributes: {ahsstaticgettarfilename(file_hdr)}: retval={do_exthdr_bytes}", file=sys.stderr)
                return -3
            do_exthdr_bytes = write_exthdrs_xattr_records(taru, file_hdr, do_exthdr_bytes)
            if do_exthdr_bytes < 0:
                print(f"internal error writing pax extended header xattr attributes: {ahsstaticgettarfilename(file_hdr)}: retval={do_exthdr_bytes}", file=sys.stderr)
                return -4
            if do_exthdr_bytes == 0:
                pass
            etar = taru.etar
            etar.etar_tarheaderflags = taru.taru_tarheaderflagsM
            Etar.etar_init_hdr(etar)
            strob_strcpy(taru.u_name_buffer, "")
            strob_sprintf(taru.u_name_buffer, STROB_DO_APPEND, "./PaxHeaders.%d/", swlib_get_pax_header_pid())
            sbase = swlib_basename(None, ahsstaticgettarfilename(file_hdr))
            strob_sprintf(taru.u_name_buffer, STROB_DO_APPEND, "%s", sbase)
            Etar.etar_set_uid(etar, 0)
            Etar.etar_set_gid(etar, 0)
            Etar.etar_set_uname(etar, "")
            Etar.etar_set_gname(etar, "")
            Etar.etar_set_mode_ul(etar, 0o644)
            if Etar.etar_set_pathname(etar, strob_str(taru.u_name_bufferM)):
                SWLIB_FATAL("name too long")
            Etar.etar_set_typeflag(etar, tarc.XHDTYPE)
            Etar.etar_set_size(etar, do_exthdr_bytes)
            Etar.etar_set_chksum(etar)
            if out_file_des >= UXFIO_FD_MIN:
                ret = atomicio(uxfio_write, out_file_des, Etar.etar_get_hdr(etar), tarc.TARRECORDSIZE)
            else:
                ret = atomicio('write', out_file_des, Etar.etar_get_hdr(etar), tarc.TARRECORDSIZE)
            if ret != tarc.TARRECORDSIZE:
                print(f"{swlib_utilname_get()}: error writing extended header, out_file_des={out_file_des}", file=sys.stderr)
                print(f"{swlib_utilname_get()}: error writing extended header, ret={ret}", file=sys.stderr)
                return -11
            if out_file_des >= UXFIO_FD_MIN:
                ret = atomicio(uxfio_write, out_file_des, strob_str(taru.exthdr_dataM), do_exthdr_bytes)
            else:
                ret = atomicio('write', out_file_des, strob_str(taru.exthdr_dataM), do_exthdr_bytes)
            if ret != do_exthdr_bytes:
                print(f"{swlib_utilname_get()}: error writing extended data", file=sys.stderr)
                return -12
            ret = taru_tape_pad_output(out_file_des, ret, af.arf_ustar)
            if ret < 0:
                print(f"{swlib_utilname_get()}: error writing extended data padding", file=sys.stderr)
                return -13

            pass
        else:
            pass
    if file_hdr.c_is_tar_lnktype == 1:
        pass
    if file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFREG:
        if (archive_format_out == af.arf_oldascii or archive_format_out == af.arf_newascii or archive_format_out == af.arf_crcascii) and file_hdr.c_nlink > 1:
            if defer:
                defer_set_taru(defer, taru)
                if defer_is_last_link(defer, file_hdr):
                    defer_writeout_zero_length_defers(defer, file_hdr, out_file_des)
                else:
                    defer_add_link_defer(defer, file_hdr)
                    # break # Outside the loop - Paul Weber
            else:
                print("taru internal error: cpio hard link handling is disabled (loc=2).", file=sys.stderr)
        elif archive_format_out == af.arf_ustar or archive_format_out == af.arf_tar:
            if linkrecord_active:
                nfound = 0
                if file_hdr.c_nlink > 1:
                    link_record_buf = hllist_find_file_entry(linkrecord, os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min), file_hdr.c_ino, 1, nfound)
                    if nfound or file_hdr.c_is_tar_lnktype == 1:
                        if nfound:
                            ahsstaticsetpaxlinkname(file_hdr, link_record_buf.path_)
                        else:
                            pass
                        # retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
                        # break # outside the loop! - Paul Weber
                    else:
                        pass
            else:
                if not linkrecord and 0:
                    print("taru internal error: tar hard link handling is disabled (loc=3).", file=sys.stderr)
            tmpp = ahsstaticgettarlinkname(file_hdr)
            if tmpp and len(tmpp):
                if do_gnu_long_link:
                    if do_gnu_long_link_link_bytes := taru_write_long_link_member(taru, out_file_des, ahsstaticgettarlinkname(file_hdr), tarc.GNUTYPE_LONGLINK, c_type, tarheaderflags) != 1024:
                        print(f"internal error writing GNU extension LongLink for linkname: {ahsstaticgettarlinkname(file_hdr)}", file=sys.stderr)
                        return -6
                    tmpp = ahsstaticgettarlinkname(file_hdr)
                    tmpp[tarc.TARLINKNAMESIZE - 1] = '\0'
                else:
                    pass
        else:
            pass
        retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
        if archive_format_out == af.arf_ustar or archive_format_out == af.arf_tar:
            if linkrecord_active:
                if file_hdr.c_nlink > 1:
                    hllist_add_record(linkrecord, ahsstaticgettarfilename(file_hdr), os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min), file_hdr.c_ino)
            else:
                if not linkrecord and 0:
                    print("taru internal error: tar hard link handling is disabled (loc=4).", file=sys.stderr)
    elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFDIR:
        file_hdr.c_filesize = 0
        retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
    elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFSOCK:
        print(f"{swlib_utilname_get()}: {ahsstaticgettarfilename(file_hdr)}: socket ignored", file=sys.stderr)
        retval = 0
    elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFIFO or file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFCHR or file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFBLK:
        if archive_format_out == af.arf_tar:
            print(f"{ahsstaticgettarfilename(file_hdr)} not dumped: not a regular file", file=sys.stderr)
            ahsstaticdeletefilehdr(file_hdr)
            return 0
        elif archive_format_out == af.arf_ustar:
            if linkrecord_active:
                link_record_buf = hllist_find_file_entry(linkrecord, os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min), file_hdr.c_ino, 1, nfound)
                if nfound:
                    file_hdr.c_mode |= ft.CP_IFREG
                    ahsstaticsetpaxlinkname(file_hdr, link_record_buf.path_)
                    # retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
                    # break # Also outside the loop - Paul Weber
                else:
                    pass
            else:
                pass
        file_hdr.c_filesize = 0
        retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
        if archive_format_out == af.arf_ustar or archive_format_out == af.arf_tar:
            if linkrecord_active:
                if file_hdr.c_nlink > 1:
                    hllist_add_record(linkrecord, ahsstaticgettarfilename(file_hdr), os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min), file_hdr.c_ino)
            else:
                pass
    elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFLNK:
        link_size = len(ahsstaticgettarlinkname(file_hdr))
        file_hdr.c_filesize = link_size
        if archive_format_out == af.arf_tar or archive_format_out == af.arf_ustar:
            if do_gnu_long_link == 0:
                if link_size + 1 > tarc.TARNAMESIZE:
                    print(f"{ahsstaticgettarfilename(file_hdr)}: symbolic link too long", file=sys.stderr)
                    # link_name[TARNAMESIZE] = '\0'
                    return tarc.TARU_HE_LINKNAME_4
                else:
                    retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
            else:
                if do_gnu_long_link_link_bytes := taru_write_long_link_member(taru, out_file_des, ahsstaticgettarlinkname(file_hdr), tarc.GNUTYPE_LONGLINK, c_type, tarheaderflags) != 1024:
                    print(f"internal error writing GNU extension LongLink: {ahsstaticgettarlinkname(file_hdr)}", file=sys.stderr)
                    return -6
                ahsstaticgettarlinkname(file_hdr)[99] = '\0'
                file_hdr.c_filesize = 0
                retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
                if retval < 0:
                    return -7
        else:
            retval = taru_write_out_header(taru, file_hdr, out_file_des, archive_format_out, None, tarheaderflags)
            retval += tarui_tape_buffered_write(ahsstaticgettarlinkname(file_hdr), out_file_des, link_size, None)
            retval += taru_tape_pad_output(out_file_des, link_size, archive_format_out)
    else:
        print(f"{swlib_utilname_get()}: unknown file type in archive header: mode={file_hdr.c_mode:o}: FMT={file_hdr.c_mode & ft.CP_IFMT:o}: name={ahsstaticgettarfilename(file_hdr)}", file=sys.stderr)
    if file_hdr_return:
        taru_filehdr2filehdr(file_hdr_return, file_hdr)
    ahsstaticdeletefilehdr(file_hdr)
    return retval + do_gnu_long_link_name_bytes + do_gnu_long_link_link_bytes



def taru_write_archive_member_data(taru, file_hdr0, out_file_des, input_fd, fout, archive_format_out, adjuct_ofd, digs):
    ret = taru_write_archive_member_data_(taru, file_hdr0, out_file_des, out_file_des, input_fd, fout,
                                          archive_format_out, adjuct_ofd, digs)
    return ret


class foutDelegate:
    def invoke(self, UnnamedParameter):
        pass


global __FILE__, __LINE__, __FUNCTION__


def taru_write_archive_file_data(taru, file_hdr0, out_file_des, input_fd, fout, archive_format_out, adjuct_ofd):
    if taru is None:
        nullfd = os.open("/dev/null", os.O_RDWR, 0)
    else:
        nullfd = taru.nullfd

    # adjunct_ofd is not used and should be removed
    if adjuct_ofd >= 0:
        FrameRecord()
        swlib_fatal("adjuct_ofd", __FILE__, __LINE__, __FUNCTION__)
    ret = taru_write_archive_member_data_(taru, file_hdr0, out_file_des, nullfd, input_fd, fout, archive_format_out, -1,
                                          file_hdr0.digs)
    if taru is None:
        os.close(nullfd)
    return ret


def taru_tape_pad_output(out_file_des, offset, archive_format):
    zeros_512 = str(['\0' for _ in range(512)])

    if archive_format == af.arf_newascii or archive_format == af.arf_crcascii:
        pad = math.fmod((4 - (math.fmod(offset, 4))), 4)
    elif archive_format == af.arf_tar or archive_format == af.arf_ustar:
        pad = math.fmod((512 - (math.fmod(offset, 512))), 512)
    elif archive_format != af.arf_oldascii and archive_format != af.arf_hpoldascii:
        pad = math.fmod((2 - (math.fmod(offset, 2))), 2)
    else:
        pad = 0

    if out_file_des < 0:
        return 0
        # return pad;

    if pad != 0:
        temp_ref_zeros_512 = zeros_512
        temp_var = tarui_tape_buffered_write(temp_ref_zeros_512, out_file_des, pad, str(None))
        return temp_var
    else:
        return 0


def taru_write_out_header(taru, file_hdr, out_des, archive_format_out, header_buffer, tarheaderflags):
    ret = 0
    do_strip_leading_slash = 0

    sys.stderr.write("BEGIN")
    if taru:
        do_strip_leading_slash = tarheaderflags & (1 << 7)

    if do_strip_leading_slash != 0:
        ahsstatic_strip_name_leading_slash(file_hdr)

    if taru is not None and (taru.preview_level > tarc.TARU_PV_0):
        taru_write_preview_line(taru, file_hdr)

    if archive_format_out == af.arf_newascii or archive_format_out == af.arf_crcascii:
        # ascii_header = str(['\0' for _ in range(112)])

        if archive_format_out == af.arf_crcascii:
            magic_string = "070702"
        else:
            magic_string = "070701"
            file_hdr.c_chksum = 0
        ascii_header = "{0:>6}{1:0>8x}{2:0>8x}{3:0>8x}{4:0>8x}{5:0>8x}{6:0>8x}{7:0>8x}{8:0>8x}{9:0>8x}{10:0>8x}{11:0>8x}{12:0>8x}{13:0>8x}".format(
            magic_string, file_hdr.c_ino, file_hdr.c_mode, file_hdr.c_uid, file_hdr.c_gid, file_hdr.c_nlink,
            file_hdr.c_mtime, file_hdr.c_filesize, file_hdr.c_dev_maj, file_hdr.c_dev_min, file_hdr.c_rdev_maj,
            file_hdr.c_rdev_min, file_hdr.c_namesize, file_hdr.c_chksum)
        temp_ref_ascii_header = ascii_header
        ret += tarui_tape_buffered_write(temp_ref_ascii_header, out_des, 110, header_buffer)

        if ret < 0:
            return ret
        # Write file name to output.
        ret += tarui_tape_buffered_write(ahsstaticgettarfilename(file_hdr), out_des, int(file_hdr.c_namesize),
                                         header_buffer)
        if ret < 0:
            return ret
        ret += taru_tape_pad_output(out_des, int((file_hdr.c_namesize + 110)), archive_format_out)
        return ret
    elif archive_format_out == af.arf_oldascii or archive_format_out == af.arf_hpoldascii:
        ascii_header = ""
        if archive_format_out == af.arf_oldascii:
            magic_string = "070707"

            dev = os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min)
            rdev = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
        else:
            """
              HP/UX cpio creates archives that look just
              like ordinary archives, but for devices it
              sets major = 0, minor = 1, and puts the
              actual major/minor number in the filesize field.
            """
            magic_string = "070707"  # FIXME. is this correct?

            if (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFCHR) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFBLK) or (
                    file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFSOCK) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFIFO):
                file_hdr.c_filesize = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
                rdev = 1
            else:
                dev = os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min)
                rdev = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
        if (file_hdr.c_ino >> 16) != 0:
            sys.stderr.write("%s: truncating inode number"+ ahsstaticgettarfilename(file_hdr))

        ascii_header = ascii_header + magic_string
        ascii_header = ascii_header + format(file_hdr.c_dev_maj & 0xFFFF, "06o")
        ascii_header = ascii_header + format(file_hdr.c_ino & 0xFFFF, "06lo")
        ascii_header = ascii_header + format(file_hdr.c_mode & 0xFFFF, "06lo")
        ascii_header = ascii_header + format(file_hdr.c_uid & 0xFFFF, "06lo")
        ascii_header = ascii_header + format(file_hdr.c_gid & 0xFFFF, "06lo")
        ascii_header = ascii_header + format(file_hdr.c_nlink & 0xFFFF, "06lo")
        ascii_header = ascii_header + format(rdev & 0xFFFF, "06o")
        ascii_header = ascii_header + format(file_hdr.c_mtime, "011lo")
        ascii_header = ascii_header + format(file_hdr.c_namesize & 0xFFFF, "06lo")
        ascii_header = ascii_header + format(file_hdr.c_filesize, "011lo")
        temp_ref_ascii_header2 = ascii_header
        ret += tarui_tape_buffered_write(temp_ref_ascii_header2, out_des, 76, header_buffer)
        # ascii_header = temp_ref_ascii_header2
        if ret < 0:
            return ret
        ret += tarui_tape_buffered_write(ahsstaticgettarfilename(file_hdr), out_des, int(file_hdr.c_namesize),
                                         header_buffer)
        return ret
        # TAR
    elif archive_format_out == af.arf_tar or archive_format_out == af.arf_ustar:
        sys.stderr.write("calling taru_write_out_tar_header2")
        ret = taru_write_out_tar_header2(taru, file_hdr, out_des, str(None), ahsstaticgettarusername(file_hdr),
                                         ahsstaticgettargroupname(file_hdr), tarheaderflags)
        return ret
    else:
        short_hdr = OldCpioHeader()

        short_hdr.c_magic = 0o70707
        short_hdr.c_dev = os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min)

        if (file_hdr.c_ino >> 16) != 0:
            sys.stderr.write("%s: truncating inode number"+ ahsstaticgettarfilename(file_hdr))

        short_hdr.c_ino = file_hdr.c_ino & 0xFFFF
        short_hdr.c_mode = file_hdr.c_mode & 0xFFFF
        short_hdr.c_uid = file_hdr.c_uid & 0xFFFF
        short_hdr.c_gid = file_hdr.c_gid & 0xFFFF
        short_hdr.c_nlink = file_hdr.c_nlink & 0xFFFF
        if archive_format_out != af.arf_hpbinary:
            short_hdr.c_rdev = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
        else:
            # HP/UX cpio creates archives that
            # look just like ordinary archives, but
            # for devices it sets major = 0, minor = 1,
            # and puts the actual major/minor number
            # in the filesize field.

            if (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFCHR) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFBLK) or (
                    file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFSOCK) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFIFO):

                file_hdr.c_filesize = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
                short_hdr.c_rdev = os.makedev(0, 1)
            else:
                short_hdr.c_rdev = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
                short_hdr.c_mtimes[0] = file_hdr.c_mtime >> 16
                short_hdr.c_mtimes[1] = file_hdr.c_mtime & 0xFFFF

                short_hdr.c_namesize = file_hdr.c_namesize & 0xFFFF

                short_hdr.c_filesizes[0] = file_hdr.c_filesize >> 16
                short_hdr.c_filesizes[1] = file_hdr.c_filesize & 0xFFFF

                # Output the file header.
                ret += tarui_tape_buffered_write(str(short_hdr), out_des, 26, header_buffer)

                # Write file name to output.
                ret += tarui_tape_buffered_write(ahsstaticgettarfilename(file_hdr), out_des, int(file_hdr.c_namesize),
                                                 header_buffer)
                if ret < 0:
                    return ret
                ret += taru_tape_pad_output(out_des, int((file_hdr.c_namesize + 26)), archive_format_out)
                return ret
        return -254


def taru_process_copy_out(taru, input_fd, out_file_des, defer, porinode, archive_format_out, ls_fd, ls_verbose_level,
                          nb, digs):
    ls_tmp = None

    file_hdr0 = ahsstaticcreatefilehdr()
    zeros_512 = [0] * 512
    hllist = hllist_open()
    hllist_disable_add(hllist)
    hllist_disable_find(hllist)
    retvalx = 0

    taru_digs_delete(file_hdr0.digsM)
    file_hdr0.digs = digs

    if ls_fd >= 0:
        # ls_tmp = STROB([0] * 120)
        ls_tmp = STROB()

    eoa = 0
    read_ret = 0
    while eoa == 0:
        read_ret = taru_read_in_tar_header2(taru, file_hdr0, input_fd, None, eoa, 0, -1)
        if read_ret < 0:
            break
        if not eoa:
            retx = tarui_write_archive_member_(taru, None, file_hdr0, hllist, defer, porinode, out_file_des, input_fd,
                                               archive_format_out, taru.taru_tarheaderflagsM)
            if retx < 0:
                return -9
            retvalx += retx
            if retx == taru_tape_skip_padding(input_fd, file_hdr0.c_filesize, af.arf_ustar) < 0:
                return -8
            if ls_fd >= 0:
                taru_print_tar_ls_list(ls_tmp, file_hdr0, ls_verbose_level)
                uxfio_write(ls_fd, strob_str(ls_tmp), strob_strlen(ls_tmp))
        eoa = 0

    if read_ret < 0:
        return -11

    ret = taru_write_archive_trailer(taru, archive_format_out, out_file_des, 512, 0, 0)
    if ret <= 0:
        return -10
    retvalx += ret
    if nb:
        nb = retvalx
    if ls_tmp:
        strob_close(ls_tmp)
    file_hdr0.digsM = None
    ahsstaticdeletefilehdr(file_hdr0)
    hllist_close(hllist)
    return 0