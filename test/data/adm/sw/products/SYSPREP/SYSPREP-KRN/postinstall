#!/sbin/sh
########
#
#  Product: SYSPREP
#  Fileset: SYSPREP-KRN
#  postinstall
#  @(#)B.11.23.0712_IC069
#
########
#
# (C) Copyright 2000-2007 Hewlett-Packard Development Company, L.P.
#
########
#
# The purpose of the postinstall script is to perform any actions
# after the fileset's files have been installed.  The postinstall
# script runs immediately after the loading of the files, but
# before installing another fileset.
#
########

#
# Source control_utils.
#

UTILS="/usr/lbin/sw/control_utils"

if [[ ! -f ${UTILS} ]]
then
    echo "ERROR:   Cannot find ${UTILS}"
    exit 1
fi

. ${UTILS}

#
# Preset the exit value to assume the script will execute successfully.
#

exitval=${SUCCESS}

########
# AddDriverEntry
#
# Purpose:
#
#   Add a driver to the system file. Set the $exitval value based on
#   the return value from mod_systemfile(). If multiple additions,
#   ensure that a FAILURE value of $exitval is not reduced by a later
#   SUCCESS return.
#
AddDriverEntry()
{
    driver=$1

    mod_systemfile ${SW_SYSTEM_FILE_PATH} -a ${driver}

    retval=$?

    if [[ ${retval} -ne $SUCCESS ]]
    then
        msg ERROR "Could not add ${driver} into ${SW_SYSTEM_FILE_PATH}."
        exitval=${FAILURE}
    fi

} # END AddDriverEntry

legacy_drv1="hcd"
legacy_drv2="hub"
legacy_drv3="hid"
isu_driver="UsbOhci"

# Check if legacy USB driver is currently in the system file.
grep -q -e ${legacy_drv1} ${SW_SYSTEM_FILE_PATH}

status1=$?

# Check if base USB ISU driver is currently in the system file.
grep -q -e ${isu_driver} ${SW_SYSTEM_FILE_PATH}

status2=$?

#  If the new ISU is installed and the legacy is not, put the 
#  legacy drivers in the system file.
if [[ ${status1} -ne $SUCCESS && ${status2} -eq $SUCCESS ]]
then

    #
    # Legacy USB driver not in system file.
    # USB ISU driver in system file.
    # Add legacy USB drivers so they are not pulled in by dependency.
    #

    AddDriverEntry ${legacy_drv1}
    AddDriverEntry ${legacy_drv2}
    AddDriverEntry ${legacy_drv3}

fi

exit ${exitval}
