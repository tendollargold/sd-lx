#  sd.swcuplib.taru.otarc.py - read and write tar headers

#   Copyright (C) 1985, 1992, 1993, 1994, 1996, 1997, 1999, 2000, 2001,
#   2003, 2004 Free Software Foundation, Inc.
#   Copyright (C) 2003,2004,2005,2010,2014 Jim Lowe
#
#   Portions of this code are derived from code
#   copyrighted by the Free Software Foundation.  Retention of their
#   copyright ownership is required by the GNU GPL and does *NOT* signify
#   their support or endorsement of this work.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
import os
import pwd
import stat
import sys
import grp
import pycksum
import math
import tarfile

from sd.swsuplib.misc.cstrings import strncasecmp
from sd.swsuplib.taru.copyout import taru_safewrite
from sd.swsuplib.misc.swlib import swlib_strncpy
from sd.swsuplib.strob import strob_strcpy, strob_sprintf, strob_str, strob_open, strob_strcat, strob_strcmp
from sd.swsuplib.taru.ahs import ahsc, ahsstaticgettargroupname, ahsstaticgettarusername, ahsstaticgettarfilename, ahsstaticgettarlinkname
from sd.swsuplib.taru.filetypes import ft, NOTDUMPEDTYPE
from sd.swsuplib.taru.readinheader import taru_read_amount
from sd.swsuplib.taru.taru import tc, af
from sd.swsuplib.taru.tar import tarc, TarRecord, TarHeader
from sd.swsuplib.taru.tooct import gaf, MODE_TO_OLDGNU_CHARS, MODE_TO_CHARS, UID_TO_CHARS, GID_TO_CHARS, OFF_TO_CHARS, TIME_TO_CHARS, mode_to_chars, uintmax_to_chars, MINOR_TO_CHARS, MAJOR_TO_CHARS, SIZE_TO_CHARS
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.const import SwGlobals

g = SwGlobals()

dbpair_user_nset = 0
dbpair_group_nset = 0
g_pwent_msg = ["user", "group"]


class SYSDBPAIR:
    def __init__(self):
        self.id = 0
        self.sname = None
        self.in_sys = -1


class OtarConstants:
    TARRECORDSIZE = 512
    CHKBLANKS = "        "  # 8 blanks, no null
    CACHE_LEN = 10
    NOBODY_ID = 0o7777777
    FALSE = 0
    TRUE = 1
    TMAGIC = "ustar"
    TMAGLEN = 6
    TVERSION = "00"
    TVERSLEN = 2

otarc = OtarConstants

userCache = [SYSDBPAIR() for _ in range(otarc.CACHE_LEN)]
groupCache = [SYSDBPAIR() for _ in range(otarc.CACHE_LEN)]

# Return nonzero if all the bytes in BLOCK are NUL.
# SIZE is the number of bytes to check in BLOCK; it must be a
# multiple of sizeof (long).  

# #define SNAME_LEN		32  Now in taru.h 

def split_long_name(name, length):
    i = length - 1
    if length > tc.PREFIX_FIELD_SIZE:
        length = tc.PREFIX_FIELD_SIZE + 1
    while i > 0:
        if name[i] == '/':
            break
        i -= 1
    return i


def error_msg_control(name, id1, dbname):
    tmp = None
    store = None
    if tmp is None:
        tmp = strob_open(80)
    if store is None:
        store = strob_open(80)
    if name:
        strob_sprintf(tmp, 0, "%s: %s name [%s] not found in system db\n", swlib_utilname_get(), dbname, name)
    else:
        strob_sprintf(tmp, 0, "%s: %s id1 [%d] not found in system db\n", swlib_utilname_get(), dbname, id1)
    if strob_str(store).find(strob_str(tmp)) != -1:
        pass
    else:
        strob_strcat(store, strob_str(tmp))
        print(strob_str(tmp), end="")


def null_block(block, size):
    p = block
    i = size // 8
    while i > 0:
        if p[0] != 0:
            return 0
        p += 1
        i -= 1
    return 1

def dbcache_set(dbpair_plen, cache, name, id1, in_sys):
    cell = 0
    if dbpair_plen >= otarc.CACHE_LEN:
        dbpair_plen = 1
    else:
        cell = dbpair_plen
        dbpair_plen += 1
    cache[cell].id = id1
    if cache[cell].sname is None:
        cache[cell].sname = strob_open(32)
    strob_strcpy(cache[cell].sname, name)
    cache[cell].in_sys = in_sys


def dbcache_lookup(dbpair_len, cache, name, id1):
    for i in range(dbpair_len):
        pair = cache[i]
        if name:
            if pair.sname is None:
                pair.sname = strob_open(32)
            if strob_strcmp(pair.sname, name) == 0:
                return pair
        if 0 <= id1 == pair.id:
            return pair
    return None


def return_name_by_cache(dbpair_len, cache, id1, is_in_sysdb):
    pair = dbcache_lookup(dbpair_len, cache, None, id1)
    if not pair:
        return None
    # is_in_sysdb = pair.in_sys
    return strob_str(pair.sname)


def return_id_by_cache(dbpair_len, cache, name, is_in_sysdb):
    pair = dbcache_lookup(dbpair_len, cache, name, -1)
    if not pair:
        return -1
    # is_in_sysdb = pair.in_sys
    return pair.id


def taru_get_pax_user_by_uid(uid, buf):
    ret = pwd.getpwuid(uid)
    return ret


def l_tar_sysdata_getuser(uid, tarbuf, paxbuf):
    pwent = pwd.getpwuid(uid)
    if not pwent:
        if tarbuf:
            tarbuf = '\0' * tc.SNAME_LEN
        if paxbuf:
            paxbuf = ''
        return None
    if tarbuf:
        swlib_strncpy(tarbuf, pwent.pw_name, tc.SNAME_LEN)
        # tarbuf = pwent.pw_name[:tc.SNAME_LEN]
    if paxbuf:
        strob_strcpy(paxbuf, pwent.pw_name)
        # paxbuf = pwent.pw_name
    return pwent.pw_name


def l_cache_tar_getuidbyname(user, puid):
    id1 = 0
    ret = taru_get_uid_by_name(user, id1)
    puid = id1
    return ret

def taru_from_oct(digs, where):
    value = 0
    while where.isspace():  # skip spaces
        where += 1
        digs -= 1
        if digs <= 0:
            return -1  # All blank field
    while digs > 0 and where.isdigit():
        # Scan til nonoctal.
        value = (value << 3) | (ord(where) - ord('0'))
        where += 1
        digs -= 1
    if digs > 0 and where and not where.isspace():
        return -1
    return value


def taru_to_oct(value, digits, where):
    digits -= 1  # Leave the trailing NUL slot alone
    where[digits] = ' '  # Put in the space, though

    while digits > 0 and value != 0:
        digits -= 1
        where[digits] = chr(ord('0') + (value & 7))
        value >>= 3

    while digits > 0:  # Add leading spaces, if necessary.
        digits -= 1
        where[digits] = ' '

def taru_get_pax_group_by_gid(gid, buf):
    ret = 0
    print("E_DEBUG(\"\")")
    try:
        group = grp.getgrgid(gid)
        buf.append(group.gr_name)
        ret = 1
    except KeyError:
        ret = -1
    return ret


def taru_get_gid_by_name(groupname, guid):
    ret = -1
    try:
        ret = pwd.getpwnam(groupname).pw_gid
    except KeyError:
        pass
    return ret


def taru_get_uid_by_name(username, puid):
    pid = os.getpid()
    print("")
    try:
        ret = get_pwent(0, 0, username, None, None,
                        pid,
                        l_getuidbyname,
                        l_tar_sysdata_getuser,
                        dbpair_user_nset,
                        userCache
                        )
        puid = pid
    except KeyError:
        ret = -1
    return ret


def l_getuidbyname(user, puid):
    pwent = pwd.getpwnam(user)
    if not pwent:
        return -1
    puid[0] = pwent.pw_uid
    return 0


def l_tar_sysdata_getgroup(gid, tarbuf, paxbuf):
    pwent = grp.getgrgid(gid)
    if not pwent:
        if tarbuf:
            tarbuf = '\0' * tc.SNAME_LEN
        if paxbuf:
            paxbuf = ''
        return None
    if tarbuf:
        swlib_strncpy(tarbuf, pwent.gr_name, tc.SNAME_LEN)
    if paxbuf:
        paxbuf = pwent.gr_name
    return pwent.gr_name


def l_cache_tar_getgidbyname(user, pgid):
    id = []
    ret = taru_get_gid_by_name(user, id)
    return ret


def l_getgidbyname(group, pgid):
    pwent = grp.getgrnam(group)
    if not pwent:
        return -1
    pgid = pwent.gr_gid
    return 0


def get_pwent(idx, id1, userkey, tarbuf, paxbuf, ppid, v_get_id, v_get_name, dbpair_len, cache):
    is_in_sysdb = 0
    if userkey:
        if len(userkey) == 0:
            return -1
        n = return_id_by_cache(dbpair_len, cache, userkey, is_in_sysdb)
        if n >= 0:
            ppid = n
            return is_in_sysdb
        n = v_get_id(userkey, ppid)
        if n == 0:
            dbcache_set(dbpair_len, cache, userkey, ppid, 0)
        else:
            ppid = ahsc.AHS_UID_NOBODY
            error_msg_control(userkey, -1, g_pwent_msg[idx])
            dbcache_set(dbpair_len, cache, userkey, ppid, -1)
            return -1
        return n
    else:
        cname = return_name_by_cache(dbpair_len, cache, id1, is_in_sysdb)
        if cname:
            if len(cname) > tc.TARU_SYSDBNAME_LEN - 1:
                print("%s: user name too long for ustar headers: %s" % (swlib_utilname_get(), cname), end="")
            retval = is_in_sysdb
        else:
            cname = v_get_name(id1, None, None)
            if cname:
                dbcache_set(dbpair_len, cache, cname, id1, 0)
                retval = 0
            else:
                cname = ahsc.AHS_USERNAME_NOBODY
                dbcache_set(dbpair_len, cache, ahsc.AHS_USERNAME_NOBODY, id1, -1)
                error_msg_control(None, id1, g_pwent_msg[idx])
                retval = -1
        if cname and tarbuf:
            tarbuf = cname[: tc.TARU_SYSDBNAME_LEN - 1]
        if cname and paxbuf:
            paxbuf = cname
        return retval


def taru_set_filehdr_sysdb_nameid_by_policy(vselect, file_hdr, termch, tar_iflags_numeric_uids):
    if vselect[0:1].lower() == 'g':
        G = 1
        c_id = file_hdr.c_gid
        c_c = file_hdr.c_cg
        sysusername = ahsstaticgettargroupname(file_hdr.c_sysusername)
    else:
        G = 0
        c_id = file_hdr.c_uid
        c_c = file_hdr.c_cu
        sysusername = ahsstaticgettarusername(file_hdr.c_sysusername)
    if (sysusername is None or len(sysusername) == 0) and c_c != tc.TARU_C_BY_UNONE:
        if G:
            l_tar_sysdata_getgroup(c_id, None, file_hdr.c_groupname)
        else:
            l_tar_sysdata_getuser(c_id, None, file_hdr.c_username)
    else:
        if c_c == tc.TARU_C_BY_USYS or c_c == tc.TARU_C_BY_UNAME:
            if G:
                try:
                    pgid = pwd.getpwnam(sysusername).pw_gid
                except KeyError:
                    pgid = otarc.NOBODY_ID
                    sys.stderr.write("user name [%s] not found setting uid to %d" + sysusername + pgid)
                psid = pgid
            else:
                try:
                    puid = pwd.getpwnam(sysusername).pw_uid
                except KeyError:
                    puid = otarc.NOBODY_ID
                    sys.stderr.write("user name [%s] not found setting uid to %d" + sysusername + puid)
                psid = puid
        elif c_c == tc.TARU_C_BY_UID:
            psid = c_id
            if tar_iflags_numeric_uids == 0:
                if G:
                    l_tar_sysdata_getgroup(c_id, None, file_hdr.c_groupname)
                else:
                    l_tar_sysdata_getuser(c_id, None, file_hdr.c_username)
            else:
                pass
        else:
            psid = c_id
        if G:
            pass
        else:
            pass


def taru_get_tar_group_by_gid(gid, tarbuf):
    try:
        group = grp.getgrgid(gid)
        tarbuf = group.gr_name
        ret = grp.getgrgid(gid)
    except KeyError:
        ret = -1
    return ret


def taru_set_sysdb_nameid_by_policy(tar_hdr, vselect, sysusername, tarbuf, paxbuf, file_hdr, termch,
                                    tar_iflags_numeric_uids):
    pgid = 0
    puid = 0
    if strncasecmp("G", vselect, 1) == 0:
        G = 1
        tar_hdr_dest = tar_hdr.gname if tar_hdr else None
        tar_hdr_id = tar_hdr.gid if tar_hdr else None
        c_id = file_hdr.c_gid
        c_c = file_hdr.c_cg
    else:
        G = 0
        tar_hdr_dest = tar_hdr.uname if tar_hdr else None
        tar_hdr_id = tar_hdr.uid if tar_hdr else None
        c_id = file_hdr.c_uid
        c_c = file_hdr.c_cu
    if (sysusername is None or len(sysusername) == 0) and c_c != tc.TARU_C_BY_UNONE:
        l_tar_sysdata_getgroup(c_id, tarbuf, paxbuf) if G else l_tar_sysdata_getuser(c_id, tarbuf, paxbuf)
        if tar_hdr:
            swlib_strncpy(tar_hdr_dest, tarbuf, tc.SNAME_LEN)
    else:
        if c_c == tc.TARU_C_BY_USYS or c_c == tc.TARU_C_BY_UNAME:
            if G:
                if taru_get_gid_by_name(sysusername, pgid):
                    pgid = otarc.NOBODY_ID
                    print("user name [%s] not found setting uid to %d" % (sysusername, pgid), end="")
                psid = pgid
            else:
                if taru_get_uid_by_name(sysusername, puid):
                    puid = otarc.NOBODY_ID
                    print("user name [%s] not found setting uid to %d" % (sysusername, puid), end="")
                psid = puid
            if tar_hdr:
                swlib_strncpy(tar_hdr_dest, sysusername, tc.SNAME_LEN)
        elif c_c == tc.TARU_C_BY_UID:
            psid = c_id
            if tar_iflags_numeric_uids == 0:
                if G:
                    taru_get_tar_group_by_gid(psid, tarbuf)
                    taru_get_pax_group_by_gid(psid, paxbuf) if paxbuf else None
                else:
                    taru_get_tar_user_by_uid(psid, tarbuf)
                    taru_get_pax_user_by_uid(psid, paxbuf) if paxbuf else None
                if tar_hdr:
                    swlib_strncpy(tar_hdr_dest, tarbuf, tc.SNAME_LEN)
            else:
                if tar_hdr:
                    tar_hdr_dest = '\0' * tc.SNAME_LEN
        else:
            psid = c_id
            if tar_hdr:
                swlib_strncpy(tar_hdr_dest, sysusername, tc.SNAME_LEN)
        if G:
            if tar_hdr:
                tar_hdr_id = str(psid)
        else:
            if tar_hdr:
                tar_hdr_id = str(psid)

def taru_split_name_ustar(tar_hdr, name, tar_iflags):
    length = len(name)
    i = split_long_name(name, length)
    if length - i - 1 > tc.NAME_FIELD_SIZE:
        print("%s: name too long (cannot be split): (i=%d) [%s]" % (swlib_utilname_get(), i, name), end="")
        return -1
    if i == 0:
        return 0
    tar_hdr.name = name[i + 1:length]
    tar_hdr.prefix = name[0:i]
    return 0


def taru_write_out_tar_header2(taru, file_hdr, out_des, header_buffer, username, groupname, tar_iflags):
    tar_rec = TarRecord()
    tar_hdr = TarHeader()
    if not taru:
        print("fatal internal error: taru is NULL, this needs to be fixed")
        return -1
    if taru[0] != ord('A'):
        print("fatal error: taru is uninitialized, this needs to be fixed")
        return -1
    print("ENTERING fd={}, name=[{}]".format(out_des, ahsstaticgettarfilename(file_hdr)))
    print("username=[{}] groupname=[{}]".format(username, groupname))
    print("file_hdr: uid=[{}] gid=[{}]".format(file_hdr.c_uid, file_hdr.c_gid))
    tar_rec[:512] = b'\0'
    tar_iflags_like_star = tar_iflags & tc.TARU_TAR_BE_LIKE_STAR
    tar_iflags_like_pax = tar_iflags & tc.TARU_TAR_BE_LIKE_PAX
    tar_iflags_numeric_uids = tar_iflags & tc.TARU_TAR_NUMERIC_UIDS
    tar_iflags_like_gnu = tar_iflags & tc.TARU_TAR_GNU_GNUTAR
    tar_iflags_like_oldgnu = tar_iflags & tc.TARU_TAR_GNU_OLDGNUTAR
    tar_iflags_like_oldgnuposix = tar_iflags & tc.TARU_TAR_GNU_OLDGNUPOSIX
    print("tar_iflags_numeric_uids is {}".format(tar_iflags_numeric_uids))
    do_record_header = taru.do_record_header
    if tar_iflags_like_star:
        termch = ord(' ')
    else:
        termch = 0
    taru.u_name_buffer = ahsstaticgettarfilename(file_hdr).encode()
    if len(taru.u_name_buffer) == 0:
        print("internal error: taru->u_name_buffer zero length")
        return -1
    if file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFDIR:
        if taru.u_name_buffer[-1] != ord('/'):
            if len(taru.u_name_buffer) >= tarc.TARNAMESIZE - 1 and (
                    tar_iflags_like_oldgnu or tar_iflags_like_oldgnuposix):
                pass
            else:
                taru.u_name_buffer += b'/'
    u_name_buffer = taru.u_name_buffer
    u_ent_buffer = taru.u_ent_buffer
    if taru.taru_set_new_name(tar_hdr, -1, u_name_buffer, taru.taru_tarheaderflagsM):
        print("{} not dumped".format(u_name_buffer))
        return -13
    if tar_iflags_like_oldgnu:
        MODE_TO_OLDGNU_CHARS(file_hdr.c_mode, tar_hdr.mode, termch)
    else:
        MODE_TO_CHARS(file_hdr.c_mode, tar_hdr.mode, termch)
    UID_TO_CHARS(file_hdr.c_uid, tar_hdr.uid, termch)
    GID_TO_CHARS(file_hdr.c_gid, tar_hdr.gid, termch)
    OFF_TO_CHARS(file_hdr.c_filesize, tar_hdr.size, termch)
    TIME_TO_CHARS(file_hdr.c_mtime, tar_hdr.mtime, termch)
    tar_hdr.version[0] = ord('0')
    tar_hdr.version[1] = ord('0')
    if file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFREG:
        tmpp = ahsstaticgettarlinkname(file_hdr)
        if tmpp and len(tmpp):
            if len(tmpp) > tarc.TARLINKNAMESIZE:
                print("link name [{}] too long for tar (returning -4).".format(tmpp))
                print("LEAVING")
                return -4
            tar_hdr.linkname[:len(tmpp)] = tmpp.encode()
            SIZE_TO_CHARS(0, tar_hdr.size, termch)
            tar_hdr.typeflag = tarc.LNKTYPE
        else:
            tar_hdr.typeflag = tarc.REGTYPE
    elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFDIR:
        tar_hdr.typeflag = tarc.DIRTYPE
    elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFCHR:
        tar_hdr.typeflag = tarc.CHRTYPE
    elif file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFBLK:
        tar_hdr.typeflag = tarc.BLKTYPE
    elif file_hdr.c_mode == 0 and ahsstaticgettarfilename(file_hdr) == tc.GNU_LONG_LINK:
        if len(ahsstaticgettarlinkname(file_hdr)) == 0:
            tar_hdr.typeflag = tc.GNUTYPE_LONGNAME
        else:
            tar_hdr.typeflag = tc.GNUTYPE_LONGLINK
    else:
        print("bad CP_?? filetype in taru_write_out_tar_header2: mode = {}".format(file_hdr.c_mode))
    if tar_iflags_like_gnu == 0 and tar_iflags_like_oldgnu == 0:
        tar_hdr.magic[:otarc.TMAGLEN] = otarc.TMAGIC.encode()
        tar_hdr.magic[otarc.TMAGLEN:otarc.TMAGLEN + otarc.TVERSLEN] = otarc.TVERSION.encode()
    else:
        tar_hdr.magic[:6] = b'ustar '
        tar_hdr.magic[6:8] = b' '
    if (username is None or len(username) == 0) and file_hdr.c_cu != tc.TARU_C_BY_UNONE:
        print("username nil")
        l_tar_sysdata_getuser(file_hdr.c_uid, u_ent_buffer, None)
        tar_hdr.uname[:tc.SNAME_LEN] = u_ent_buffer
    else:
        puid = 0
        if file_hdr.c_cu == tc.TARU_C_BY_USYS or file_hdr.c_cu == tc.TARU_C_BY_UNAME:
            print("finding uid for username = {}".format(username))
            if taru_get_uid_by_name(username, puid):
                puid = otarc.NOBODY_ID
                print("user name [{}] not found setting uid to {}".format(username, puid))
            tar_hdr.uname[:tc.SNAME_LEN] = username.encode()
        elif file_hdr.c_cu == tc.TARU_C_BY_UID:
            puid = file_hdr.c_uid
            print("finding user for uid  {}".format(puid))
            if tar_iflags_numeric_uids == 0:
                taru_get_tar_user_by_uid(puid, u_ent_buffer)
                tar_hdr.uname[:tc.SNAME_LEN] = u_ent_buffer
            else:
                tar_hdr.uname[:tc.SNAME_LEN] = b''
        else:
            print("Using user and uid with no lookups")
            puid = file_hdr.c_uid
            tar_hdr.uname[:tc.SNAME_LEN] = username.encode()
        UID_TO_CHARS(puid, tar_hdr.uid, termch)
    if (groupname is None or len(groupname) == 0) and file_hdr.c_cg != tc.TARU_C_BY_GNONE:
        print("groupname nil")
        l_tar_sysdata_getgroup(file_hdr.c_gid, u_ent_buffer, None)
        tar_hdr.gname[:tc.SNAME_LEN] = u_ent_buffer
    else:
        pgid = 0
        if file_hdr.c_cg == tc.TARU_C_BY_GSYS or file_hdr.c_cg == tc.TARU_C_BY_GNAME:
            print("finding gid for groupname={}".format(groupname))
            tar_hdr.gname[:tc.SNAME_LEN] = groupname.encode()
            if taru_get_gid_by_name(groupname, pgid):
                pgid = otarc.NOBODY_ID
                print("group name [{}] not found setting uid to {}".format(groupname, pgid))
        elif file_hdr.c_cg == tc.TARU_C_BY_GID:
            pgid = file_hdr.c_gid
            print("finding gname for gid  {}".format(pgid))
            if tar_iflags_numeric_uids == 0:
                taru_get_tar_group_by_gid(pgid, u_ent_buffer)
                tar_hdr.gname[:tc.SNAME_LEN] = u_ent_buffer
            else:
                tar_hdr.gname[:tc.SNAME_LEN] = b''
        else:
            print("Using user and gid with no lookups")
            pgid = file_hdr.c_gid
            tar_hdr.gname[:tc.SNAME_LEN] = groupname.encode()
        GID_TO_CHARS(pgid, tar_hdr.gid, termch)
    if tar_iflags_numeric_uids:
        tar_hdr.uname[:tc.SNAME_LEN] = b''
        tar_hdr.gname[:tc.SNAME_LEN] = b''
    if tar_hdr.typeflag == tarc.CHRTYPE or tar_hdr.typeflag == tarc.BLKTYPE:
        MAJOR_TO_CHARS(file_hdr.c_rdev_maj, tar_hdr.devmajor, termch)
        MINOR_TO_CHARS(file_hdr.c_rdev_min, tar_hdr.devminor, termch)
    else:
        if tar_iflags_like_pax:
            tar_hdr.devmajor[:8] = b'0000000'
            tar_hdr.devminor[:8] = b'0000000'
        elif tar_iflags_like_star:
            tar_hdr.devmajor[:8] = b'0000000 '
            tar_hdr.devminor[:8] = b'0000000 '
        else:
            tar_hdr.devmajor[:8] = b'0000000'
            tar_hdr.devminor[:8] = b'0000000'
    tar_hdr.chksum[:8] = otarc.CHKBLANKS.encode()
    sum = taru_tar_checksum(tar_hdr)
    if tar_iflags_like_pax or tar_iflags_like_star:
        uintmax_to_chars(sum, tar_hdr.chksum, 8, tarc.POSIX_FORMAT, termch)
    else:
        uintmax_to_chars(sum, tar_hdr.chksum, 7, tarc.POSIX_FORMAT, termch)
    if header_buffer:
        if do_record_header:
            taru.header = tar_rec[:tarc.TARRECORDSIZE]
            taru.header_length = tarc.TARRECORDSIZE
        header_buffer[:512] = tar_rec
        print("LEAVING")
        return 512
    else:
        if do_record_header:
            taru.header = tar_rec[:tarc.TARRECORDSIZE]
            taru.header_length = tarc.TARRECORDSIZE
        ret = taru_safewrite(out_des, tar_rec, tarc.TARRECORDSIZE)
        print("LEAVING")
        return ret
# Read a tar header, including the file name, from file descriptor IN_DES into FILE_HDR.
# taru_read_in_tar_header2 and i_taru_read_in_tar_header2 moved to TARU class in taru.py
def taru_read_in_tar_header2():
    sys.stderr.write("taru_read_in_tar_header2 moved to TARU class")
    return 1


def i_taru_read_in_tar_header2():
    sys.stderr.write("i_taru_read_in_tar_header2 moved to TARU class")
    return 1


def taru_exattlist_init(file_hdr):
    pass

def taru_exatt_override_ustar(file_hdr, p):
    pass

def taru_set_filetype_from_tartype(ch, mode, filename):
    mode[0] &= stat.S_IFMT
    if ch == tarfile.REGTYPE or ch == tarfile.CONTTYPE:
        mode[0] |= ft.CP_IFREG
    elif ch == tarfile.DIRTYPE:
        mode[0] |= ft.CP_IFDIR
    elif ch == tarfile.CHRTYPE:
        mode[0] |= ft.CP_IFCHR
    elif ch == tarfile.BLKTYPE:
        mode[0] |= ft.CP_IFBLK
    elif ch == tarfile.FIFOTYPE:
        mode[0] |= ft.CP_IFIFO
    elif ch == tarfile.SYMTYPE:
        mode[0] |= ft.CP_IFLNK
    elif ch == tarfile.LNKTYPE:
        mode[0] |= ft.CP_IFREG
    elif ch == tarfile.AREGTYPE:
        if filename is None:
            print("taru_set_filetype_from_tartype(): warning: AREGTYPE: filename not given, assuming REGTYPE.")
            mode[0] |= ft.CP_IFREG
        else:
            if filename[-1] == '/':
                mode[0] |= ft.CP_IFDIR
            else:
                mode[0] |= ft.CP_IFREG
    elif ch == NOTDUMPEDTYPE:
        mode[0] |= ft.CP_IFSOCK
    else:
        mode[0] |= ft.CP_IFREG
        print("%s: warning:  typeflag [%c] not supported, ignoring file" % (swlib_utilname_get(), ch))
        return 1
    return 0

def set_filetype_from_tartype(file_type, mode, filename):
    mode &= stat.S_IFMT
    switcher = {
        'REGTYPE': stat.S_IFREG,
        'CONTTYPE': stat.S_IFREG,
        'DIRTYPE': stat.S_IFDIR,
        'CHRTYPE': stat.S_IFCHR,
        'BLKTYPE': stat.S_IFBLK,
        'FIFOTYPE': stat.S_IFIFO,
        'SYMTYPE': stat.S_IFLNK,
        'LNKTYPE': stat.S_IFREG,
        'AREGTYPE': None,  # Handle this case separately
        'NOTDUMPEDTYPE': stat.S_IFSOCK
    }

    if file_type in switcher:
        if file_type == 'AREGTYPE':
            if filename is None:
                print("set_filetype_from_tartype(): warning: AREGTYPE: filename not given, assuming REGTYPE.")
                mode |= stat.S_IFREG
            else:
                if filename[-1] == '/':
                    mode |= stat.S_IFDIR
                else:
                    mode |= stat.S_IFREG
        else:
            mode |= switcher[file_type]
    else:
        mode |= stat.S_IFREG
        print(f"{swlib_utilname_get()}: warning: typeflag [{file_type}] not supported, ignoring file")
        return 1

    return 0

# this function is very redundant
def tarui_get_filetypes(mode, cpio_mode, tarflag):
    if os.path.isdir(mode):
        tarflag = tarc.DIRTYPE
        cpio_mode = ft.CP_IFDIR
    elif os.path.isfile(mode):
        tarflag = tarc.REGTYPE
        cpio_mode = ft.CP_IFREG
    elif stat.S_ISLNK(mode):
        tarflag = tarc.SYMTYPE
        cpio_mode = ft.CP_IFLNK
    elif stat.S_ISBLK(mode):
        tarflag = tarc.BLKTYPE
        cpio_mode = ft.CP_IFBLK
    elif stat.S_ISCHR(mode):
        tarflag = tarc.CHRTYPE
        cpio_mode = ft.CP_IFCHR
    elif stat.S_ISFIFO(mode):
        tarflag = tarc.FIFOTYPE
        cpio_mode = ft.CP_IFIFO
    elif stat.S_ISSOCK(mode):
        tarflag = NOTDUMPEDTYPE
        cpio_mode = ft.CP_IFSOCK
    else:
        print(f"{swlib_utilname_get()}: unrecognized type in mode: {mode}")
        tarflag = chr(-1)
        cpio_mode = -1
    return tarflag, cpio_mode


def taru_get_user_by_uid(uid):
    try:
        pwent = pwd.getpwuid(uid)
        return pwent.pw_name
    except KeyError:
        return None


def taru_get_group_by_gid(gid):
    try:
        pwent = grp.getgrgid(gid)
        return pwent.gr_name
    except KeyError:
        return None


def taru_tar_checksum(hdr):
    tar_hdr = hdr
    sum = 0
    p = tar_hdr
    q = p + tarc.TARRECORDSIZE
    while p < tar_hdr.chksum:
        sum += p[0] & 0xff
        p += 1
    for _ in range(8):
        sum += ord(' ')
        p += 1
    while p < q:
        sum += p[0] & 0xff
        p += 1
    return sum


def taru_mode_to_chars(v, p, s, termch):
    mode_to_chars(v, p, s, gaf.POSIX_FORMAT, termch)


def taru_tape_skip_padding(in_file_des, offset, archive_format_in):
    pad = 0
    if archive_format_in == af.arf_crcascii or archive_format_in == af.arf_newascii:
        pad = (4 - (offset % 4)) % 4
    elif archive_format_in == af.arf_binary or archive_format_in == af.arf_hpbinary:
        pad = (2 - (offset % 2)) % 2
    elif archive_format_in == af.arf_tar or archive_format_in == af.arf_ustar:
        pad = (512 - (offset % 512)) % 512
    if pad != 0:
        if in_file_des < 0:
            return pad
        else:
            return taru_read_amount(in_file_des, pad)
    return 0


# Octal Acsii to unsigned long
def taru_otoul(s):
    val = 0
    s = s.strip()
    while s[0] == ' ':
        s = s[1:]
    while '0' <= s[0] <= '7':
        val = 8 * val + ord(s[0]) - ord('0')
        s = s[1:]
    while s[0] == ' ':
        s = s[1:]
    return val, s == ''


def taru_otoumax(s):
    val = 0
    s = s.strip()
    while s[0] == ' ':
        s = s[1:]
    while '0' <= s[0] <= '7':
        val = 8 * val + ord(s[0]) - ord('0')
        s = s[1:]
    while s[0] == ' ':
        s = s[1:]
    return val, s == ''

#
# Decimal ascii to unsigned long
# return 0 if conversion OK
# return 1 if failed to convert string to end.
# return 2 if overflow.
#
def taru_datoul(s, n):
    oldval = 0
    val = 0
    while s[0] == ' ':
        s = s[1:]

    while '0' <= s[0] <= '9' and val >= oldval:
        # sys.stderr.write("%lu %lu\n", oldval, val);
        oldval = val
        val = 10 * val + ord(s[0]) - ord('0')
        s = s[1:]
    if oldval != 0 and ((math.trunc(val / float(oldval))) < 10):
        sys.stderr.write("%s, taru_datoul : conversion overflow\n" + swlib_utilname_get())
        return 2
    while s[0] == ' ':
        s = s[1:]
    n = val
    return 1 if (not (s[0] == '\0')) else 0


def taru_get_tar_user_by_uid(uid, buf):
    ret = pwd.getpwuid(uid)
    return ret


def taru_tarheader_check(buffer):
    sumis = int(buffer[148:156], 8)
    sum = pycksum.cksum(buffer)
    if sum != sumis:
        return -1
    return 0


def taru_get_cpio_filetype(mode):
    c = ''
    cm = 0
    tarui_get_filetypes(mode, cm, c)
    return cm

def taru_get_tar_filetype(mode):
    c = ''
    cm = 0
    tarui_get_filetypes(mode, cm, c)
    return int(c)




