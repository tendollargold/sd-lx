# shlib.py: Decompress the shell routines in shell_lib.c (shell_lib.sh)
# Copyright (C) 2006 James H. Lowe, Jr.  <jhlowe@acm.org>
# Copyright (C) 2024 Paul Weber converstion to Python

# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import lzo
import sys
from  ctypes import c_char_p

shell_lib_function_pallet = '\0'
class shell_lib_function:
    def __init__(self):
        self.name = c_char_p  # Name of the function */
        self.control = c_char_p # A Control header, holds the compressed size of textM */
        self.text = c_char_p # The (compressed, or not) POSIX Shell compatible function
        self.function = c_char_p # not used, always NULL

def uncompress_func(plain_text, f):
    new_out_len = 0

    # control looks like this "0000000331:0000000890:COMPRESSION=1:"

    in_len = int(f.control)
    s = f.control[f.control.find(':') + 1:]
    out_len = int(s)
    plain_text.set_length(int(out_len) + 100)
    inbuf = bytearray(f.text)
    outbuf = bytearray(plain_text.str())

    try:
        ret = lzo.decompress(inbuf, in_len, outbuf)

        if ret == 0:
            return 0
    except IOError as e:
        print("decompression failed for %s %d %d" % (f.name, new_out_len, out_len))
        return -1

def unescape_text(text, buf, length):
    s = text
    buf.str('')
    while s:
        if s.startswith("\\x"):
            s = s[2:]
            m = int(s[:2], 16)
            s = s[2:]
            if s != '':
                print("unescape_text: error, loc=2")
                return -2
            buf.chr(m)
        else:
            print("unescape_text: error, loc=1")
            return -1
    return 0

def decode_text(f, buf):
    ret = 0
    if f.function is not None:
        return 0
    if "COMPRESSION=1" in f.control:
        ret = uncompress_func(buf, f)
    elif f.text.startswith('\\'):
        length = int(f.control)
        ret = unescape_text(f.text, buf, length)
    else:
        buf.str(f.text)
    if f.function is None:
        f.function = buf.str()
    return ret

def shlib_get_function_array():
    return shell_lib_function()

def shlib_get_function_struct(function_name):
    f = shell_lib_function()
    while f.text:
        if f.name == function_name:
            return f
        f += 1
    return None

def shlib_get_function_text(f, buf):
    ret = decode_text(f, buf)
    if ret:
        return None
    return f.function

def shlib_get_function_text_by_name(function_name, buf, pret):
    f = shlib_get_function_struct(function_name)
    if f is None:
        buf.strcpy("")
    ret = decode_text(f, buf)
    if ret:
        ret = -1
        buf.str('')
    else:
        ret = 0
    if pret:
        pret = ret
    if ret:
        return None
    else:
        return f.function

"""
  Generate a C source file that contain the shell routines in
  shell_lib.sh as static constant strings */

   Copyright (C) 2004  James H. Lowe, Jr.
   Copyright (C) 2024 Paul Weber conversion to Python
   All Rights Reserved.

"""

def conv_clump(convline, line, length):
    s = line
    count = 0
    linecount = 0
    while count < length:
        if linecount == 0:
            convline += "\""
        linecount += 1
        convline += "\\x%02x" % ord(s)
        s += 1
        count += 1
        if linecount > 15:
            linecount = 0
            convline += "\"\n"
    if linecount:
        convline += "\"\n"
    return

def decompress_func(dst, z, compressed_len):
    dst_len = 0
    dst = dst.ljust(compressed_len*40)
    ret = lzo.decompress(z, compressed_len, dst, dst_len)
    return ret

def compress_func(convline, plain_text, cloc, clen):
    out_len = 0
    IN_LEN = len(plain_text) + 100
    OUT_LEN = (IN_LEN + IN_LEN // 64 + 16 + 3)
    inbuf = bytearray(IN_LEN)
    outbuf = bytearray(OUT_LEN)

    if lzo.error != 0:
        print("lzo_init() failed !!!")
        return -1

    in_len = len(plain_text) + 1
    inbuf[:in_len] = plain_text.encode()

    r = lzo.compress(inbuf, in_len, outbuf)
    if r == lzo.error == 0:
        pass
    else:
        print("internal error - compression failed: %d" % r)
        return -2

    if cloc and clen:
        cloc = bytearray(out_len + 1)
        cloc[:out_len] = outbuf

    conv_clump(convline, outbuf, out_len)
    return out_len

def write_function_data_header(funcheader, compression, uncompressed_size, size):
    if funcheader is None:
        funcheader = "\t\"%010d:%010d:COMPRESSION=%d:\",\n" % (size, uncompressed_size, compression)
    return funcheader
def write_file_header(compression_on):
    # tm = time.time()
    sys.stdout.write("\n")
    sys.stdout.write("\n\n")
    sys.stdout.write("\n\n")
    sys.stdout.write(
        "#include \"shlib.h\"\n"
        "\n"
        "\n"
        "struct shell_lib_function shell_lib_function_pallet[]={\n"
    )

def convert_line(convline, line, compress_on):
    s = line
    if compress_on:
        convline += line
        return
    convline = "\""
    while s:
        convline += "\\x%02x" % ord(s)
        s += 1
    convline += "\"\n"
    return

def getnextpath(fp):
    buf = []
    ret = fp.readline()
    if not ret:
        return None
    if len(ret) >= (len(buf) - 1):
        sys.exit(9)
    buf[len(buf) - 1] = '\0'
    return ret

def get_next_line(flp):
    path = getnextpath(flp)
    return path

def parse_name(function_name, line):
    s = line
    # function_name = line
    while s.isalnum() or s == '_':
        s += 1
    s = '\0'
    return

def det_strlen(buf):
    s = buf
    nq = 0
    length = len(buf)
    while s:
        if s == '"' or s == '\n':
            nq += 1
        s += 1
    return (length - nq) // 4
