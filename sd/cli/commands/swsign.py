#!/usr/bin/python3

# swsign.py - swsign - supports signed software signature verification

# Copyright (C) 2023 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
import getpass
import socket
from sd.cli.cli import version_info

cmd = os.path.split(sys.argv[0])[1]
username = getpass.getuser()
hostname = socket.gethostname()

swsign_parser = argparse.ArgumentParser(
	usage="""%(prog)s -v -s source_depot [-l level] 
	       [-k public_key] [-n num_threads] [-g]
	       [-x option=value]""",
	formatter_class=argparse.RawTextHelpFormatter,
	epilog='''
 Software selections are specified as:
 
     product[.subproduct][.fileset][,version] ...

 where version is either
 
     [r=revision][,a=arch][,v=vendor][,l=location]
     instance_id
 
 Target selections are specified as:
 
     -T [host][:][path] ...
 
''')

swsign_parser.add_argument("-l",metavar='',
	choices=['depot', 'bundle', 'product', 'fileset', 'all'],
    default='all',
	help="levels of objects to verify signature, one of: 'depot'\n 'bundle', 'product', or 'fileset'")
swsign_parser.add_argument("-k", "--public_key",metavar='public_key',
			  help="path specified by public_key for the public key file \nto be used for signature verification")
swsign_parser.add_argument("-s", "--source",metavar='source_depot',
			  help="source depot for which signature verification is needed")
swsign_parser.add_argument("-x", nargs="+",action='append',metavar='option=value',
			  help="-x option=value, set the option to value")
# -g --gnu option added for GNU Copyleft
swsign_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")

swsign_parser.parse_args()
args = swsign_parser.parse_args()

print("DEBUG: Args in: ", cmd,  args)
print(args)
