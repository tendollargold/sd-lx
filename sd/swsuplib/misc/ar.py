# Header describing `ar' archive file format.
#   Copyright (C) 1996-2021 Free Software Foundation, Inc.
#   This file is part of the GNU C Library.
#   Paul Weber conversion to python
#
#   The GNU C Library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   The GNU C Library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with the GNU C Library; if not, see
#   <https://www.gnu.org/licenses/>.

# Archive files start with the ARMAG identifying string.  Then follows a
#   `struct ar_hdr', and as many bytes of member file data as its `ar_size'
#   member indicates, for each member file.

class ArConst:
    ARMAG = "!<arch>\n"  # String that begins an archive file.
    SARMAG = 8  # Size of that string.
    ARFMAG = "`\n"  # String in ar_fmag at end of each header.
    SARHDR = 60  #  not in /usr/include/ar.h

class ArHdr:
    def __init__(self):
        self.ar_name = [0] * 16  # Member file name, sometimes / terminated.
        self.ar_date = [0] * 12  # File date, decimal seconds since Epoch.
        self.ar_uid = [0] * 6    # User IDs, in ASCII decimal.
        self.ar_gid = [0] * 6    # Group IDs, in ASCII decimal
        self.ar_mode = [0] * 8   # File mode, in ASCII octal.
        self.ar_size = [0] * 10  # File size, in ASCII decimal.
        self.ar_fmag = [0] * 2   # Always contains ARFMAG.
