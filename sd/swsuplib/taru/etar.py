# etar.py - A simpler interface to the tar writing routines
#
#   Copyright (C) 2005 Jim Lowe
#   Copyright (C) 2024 Paul Weber Conversion to Python

#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# Change Log
# ------------------------------------------------------------------
# 20240106 Paul Weber - etar.py created, no errors.

import os
import sys
import time

from sd.swsuplib.misc.swlib import delay_sleep_pattern2, io_req
from sd.swsuplib.misc.swutilname import swlib_utilname_get

HEADER_ALLOC = 2048
ETAR_UNSET_FILENAME = "ETAR_UNSET_FILENAME"


class Etar:
    def __init__(self, flags):
        self.etar_tarheaderflags = flags
        self.tar_hdr = bytearray(HEADER_ALLOC)
        self.time = time.time()

    def etar_close(self):
        del self.tar_hdr

    def etar_get_hdr(self):
        return self.tar_hdr


    def etar_init_hdr(self):
        self.tar_hdr = bytearray(HEADER_ALLOC)
        self.tar_hdr[0] = ord('0')
        self.tar_hdr[1] = ord('0')
        self.tar_hdr[257:263] = b'ustar\x0000'
        self.etar_set_pathname(ETAR_UNSET_FILENAME)
        self.etar_set_mode_ul(0o0550)
        self.etar_set_uid(0)
        self.etar_set_gid(0)
        self.etar_set_size(0)
        self.etar_set_time(self.time)
        self.etar_set_typeflag(5)
        self.etar_set_linkname("")
        self.etar_set_linkname("")
        self.etar_set_uname("")
        self.etar_set_gname("")
        self.etar_set_devmajor(0)
        self.etar_set_devminor(0)
        self.etar_set_chksum()

    def etar_emit_header(self, fd):
        ret = os.write(fd, self.tar_hdr)
        if ret != len(self.tar_hdr):
            return -1
        return len(self.tar_hdr)

    def etar_set_size_from_buffer(self, buf, bufsize):
        if bufsize < 0:
            length = len(buf)
        else:
            length = bufsize
        self.etar_set_size(length)
        return length

    def etar_set_size_from_fd(self, fd, newfd):
        if newfd is not None:
            newfd[0] = -1
        if os.isatty(fd):
            tmp_fd = os.open('/dev/null', os.O_WRONLY)
            swlib_pipe_pump(tmp_fd, fd)
            vfd = tmp_fd
            if newfd is None:
                sys.stderr.write("usage error in etar_set_size_from_fd")
            if newfd is not None:
                newfd[0] = tmp_fd
        else:
            vfd = fd
        size = os.lseek(vfd, 0, os.SEEK_END)
        if size < 0:
            return -1
        self.etar_set_size(size)
        os.lseek(vfd, 0, os.SEEK_SET)
        return size

    def etar_set_pathname(self, pathname):
        self.tar_hdr[345:500] = pathname.encode('utf-8')
        return 0

    def etar_set_linkname(self, name):
        self.tar_hdr[157:257] = name.encode('utf-8')
        return 0 if len(name) <= 100 else 1

    def etar_set_uname(self, name):
        self.tar_hdr[265:297] = name.encode('utf-8')
        return 0 if len(name) <= 32 else -1

    def etar_set_gname(self, name):
        self.tar_hdr[297:329] = name.encode('utf-8')
        return 0 if len(name) <= 32 else -1

    def etar_set_chksum(self):
        chksum = sum(self.tar_hdr[:148]) + sum(self.tar_hdr[156:512])
        self.tar_hdr[148:156] = str(oct(chksum)).encode('utf-8').rjust(8, b'0')

    def etar_set_mode_ul(self, mode_i):
        self.tar_hdr[100:108] = str(oct(mode_i)).encode('utf-8').rjust(8, b'0')

    def etar_set_uid(self, val):
        self.tar_hdr[108:116] = str(oct(val)).encode('utf-8').rjust(8, b'0')

    def etar_set_gid(self, val):
        self.tar_hdr[116:124] = str(oct(val)).encode('utf-8').rjust(8, b'0')

    def etar_set_size(self, val):
        self.tar_hdr[124:136] = str(oct(val)).encode('utf-8').rjust(12, b'0')

    def etar_set_time(self, val):
        self.tar_hdr[136:148] = str(oct(int(val))).encode('utf-8').rjust(12, b'0')

    def etar_set_typeflag(self, tar_type):
        self.tar_hdr[156] = tar_type

    def etar_set_devmajor(self, devno):
        self.tar_hdr[329:337] = str(oct(devno)).encode('utf-8').rjust(8, b'0')

    def etar_set_devminor(self, devno):
        self.tar_hdr[337:345] = str(oct(devno)).encode('utf-8').rjust(8, b'0')


def etar_open(flags):
    etar = Etar(flags)
    etar.etar_tarheaderflags = flags
    etar.time = time.time()
    etar.tar_hdr = b'\0' * HEADER_ALLOC
    return etar
def etar_emit_data_from_fd(ofd, ifd):
    ret = swlib_pipe_pump(ofd, ifd)
    if ret < 0:
        sys.stderr.write("%s: etar_emit_data_from_fd(): loc=1: ret = %d\n" + swlib_utilname_get() + ret)
        return -1
    remains = ret % 512
    if ret and remains > 0:
        remains = 512 - remains
        ret1 = os.write(ofd, b'\0' * remains)
        if ret1 < 0:
            sys.stderr.write("%s: etar_emit_data_from_fd(): loc=2: ret = %d\n" + swlib_utilname_get() + ret)
            return -2
    else:
        remains = 0
    return ret + remains


def etar_emit_data_from_buffer(ofd, buf, bufsize):
    if bufsize < 0:
        length = len(buf)
    else:
        length = bufsize
    ret = swlib_pipe_pump(ofd, buf[:length])
    if ret < 0:
        return ret
    if ret != length:
        return -2
    remains = ret % 512
    if ret and remains > 0:
        remains = 512 - remains
        ret1 = os.write(ofd, b'\0' * remains)
        if ret1 < 0:
            return -2
    else:
        remains = 0
    return ret + remains


def etar_write_trailer_blocks(ofd, nblocks):
    nullblock = b'\0' * 512
    retval = 0
    for _ in range(nblocks):
        ret = os.write(ofd, nullblock)
        if ret < 0:
            return -1
        retval += 512
    if retval != (512 * nblocks):
        return -1
    return retval

# These belong in swlib but are here 20240106 to ensure this module
# correctly executes
def swlib_pipe_pump(ofd, ifd):
    return swlib_pump_amount(ofd, ifd, -1)


def swlib_pump_amount(discharge_fd, suction_fd, amount):
    i = amount
    if swlib_i_pipe_pump(suction_fd, discharge_fd, i, -1, None, os.read):
        return -1
    return i


def swlib_i_pipe_pump(suction_fd, discharge_fd, amount, adjunct_ofd, obuf, thisfpread):
    commandfailed = 0
    pumpdead = 0
    byteswritten = 0
    remains = 512
    sleepbytes = 0
    if obuf is not None:
        obuf.clear()
    while not pumpdead:
        buf = thisfpread(suction_fd, remains)
        if buf == b'':
            pumpdead = 1
        else:
            if discharge_fd >= 0:
                ibytes = os.write(discharge_fd, buf)
                if adjunct_ofd >= 0:
                    ibytes = os.write(adjunct_ofd, buf)
            else:
                ibytes = len(buf)
            if obuf is not None:
                obuf.extend(buf)
            if ibytes != len(buf):
                commandfailed = 2
                pumpdead = 1
            else:
                byteswritten += len(buf)
                if (amount - byteswritten) < remains:
                    if remains > 512:
                        remains = 512
        if sleepbytes:
            sleepbytes += len(buf)
            delay_sleep_pattern2(io_req, sleepbytes, amount, byteswritten)
    if obuf is not None:
        obuf.append(b'\0')
    return commandfailed

