# swssh.;y  --  routines to run a command via multilple ssh hops.
#

#
#   Copyright (C) 2003,2004,2006 James H. Lowe, Jr. <jhlowe@acm.org>
#   Copyright (C) 2023 Paul Weber, convert to Python
#   All Rights Reserved.
#
#   COPYING TERMS AND CONDITIONS
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

import sys
from typing import List
from sd.swsuplib.misc.swlib import swlibc, swlib_is_sh_tainted_string, swlib_basename
from sd.const import lc
from sd.swsuplib.misc.strar import strar_get, STRAR, STROB
from sd.swsuplib.strob import strob_open, strob_strcpy, strob_str, strob_close, strob_strstrtok
from sd.swsuplib.misc.shcmd import SHCMD, shcmd_add_arg

class SshConstants:
    SWSSH_POSIX_SHELL_COMMAND = "PATH=`getconf PATH` sh -s"
    SWSSH_SYSTEM_SHELL_COMMAND = "/bin/sh -s"
    SWSSH_SH_SHELL_COMMAND = "/bin/sh -s"
    SWSSH_BASH_SHELL_COMMAND = "/bin/bash -s"
    SWSSH_KSH_SHELL_COMMAND = "/bin/ksh -s"
    SWSSH_MKSH_SHELL_COMMAND = "/bin/mksh -s"
    SWSSH_ASH_SHELL_COMMAND = "/bin/ash -s"
    SWSSH_DASH_SHELL_COMMAND = "/bin/dash -s"
    SWSSH_DETECT_POISON_DEFAULT = "exit 1"
    TARGET_DELIM = "@@"
    SWC_KILL_PID_MARK = "_"
    SWSSH_TRACK_PID = "$PPID"
    g_active_flag = 1
    g_did_get_command = 0

sshc = SshConstants
def is_posix_shell_command(s):
    if "getconf" in s and "PATH" in s:
        sp = s[:]
        p = sp[:]
        # disable the test below, it is currently broken
        # return 1
        while p and p:
            if p == "\\":
                p = p[1:] + p[2:]
            else:
                p += 1
        if sp == sshc.SWSSH_POSIX_SHELL_COMMAND:
            # It really is the SWSSH_POSIX_SHELL_COMMAND
            # shell meta characters are allowed in this case.
            if sshc.g_did_get_command:
                # Sanity error, Only allow finding this once.
                # Maybe the user is up to something bad.
                sys.stderr.write("internal error in swssh\n")
                sys.exit(2)
            g_did_get_command = sshc.g_active_flag
            retval = 1
        else:
            # Zero is safe
            retval = 0
        sp = None
    else:
        retval = 0
    return retval

def bail_on_taint(s):
    if swlib_is_sh_tainted_string(s):
        print("arg contains shell meta characters [{}]".format(s), file=sys.stderr)
        sys.exit(2)
    return 0

def safe_shcmd_add_arg(cmd, s):
    if not cmd:
        return None
    if is_posix_shell_command(s) == 0:
        bail_on_taint(s)
    return shcmd_add_arg(cmd, s)

def clean_leading_comm_at_sign(ptar):
    if ptar is None:
        return
    if ptar[0][0] == '@':
        ptar[0] = ptar[0][1:]
    while ptar[0][0] == '\x20':
        ptar[0] = ptar[0][1:]

def cat_escapes(command, nhops):
    if nhops == 0:
        nbackslashes = 0
    elif nhops == 1:
        nbackslashes = 0
    elif nhops == 2:
        nbackslashes = 1
    elif nhops == 3:
        nbackslashes = 3
    elif nhops == 4:
        nbackslashes = 7
    else:
        return -1
    for i in range(nbackslashes):
        command += "\\"
    return 0

def form_intermediate_msg(command, host, nhops):
    command = "echo 10{} Intermediate Host:{}:{};".format(nhops, host, sshc.SWSSH_TRACK_PID)
    swssh_protect_shell_metacharacters(command, nhops, swlibc.SW_TAINTED_CHARS)
    return 0

def add_host_kill(doimsg, cmd, host, cmdcount):
    s = None
    tok = None
    t = strob_open(10)
    tmp = strob_open(10)
    if not doimsg:
        return
    form_intermediate_msg(tmp, host, cmdcount)
    t = strob_strcpy(t, strob_str(tmp))
    s = strob_str(t)
    while tok == strob_strstrtok(t, s, " "):
        s = None
        shcmd_add_arg(cmd, tok)
    strob_close(tmp)
    strob_close(t)

def add_host_message(doimsg, cmd, host, cmdcount):
    s = None
    tok = None
    t = strob_open(10)
    tmp = strob_open(10)
    if not doimsg:
        return
    form_intermediate_msg(tmp, host, cmdcount)
    t = strob_strcpy(t, strob_str(tmp))
    s = strob_str(t)
    while tok == strob_strstrtok(t, s, " "):
        s = None
        shcmd_add_arg(cmd, tok)
    strob_close(tmp)
    strob_close(t)

def parse_host_port(host, cmd, kmd):
    if "_" in host:
        u = host.index("_")
        s = u + 1
        status = 0
        try:
            status = int(s)
        except ValueError:
            pass
        if status == 0:
            host[u] = '\0'
            safe_shcmd_add_arg(kmd, "-p")
            safe_shcmd_add_arg(cmd, "-p")
            safe_shcmd_add_arg(kmd, s)
            safe_shcmd_add_arg(cmd, s)
            safe_shcmd_add_arg(kmd, host)
            safe_shcmd_add_arg(cmd, host)
            host[u] = '_'
        else:
            safe_shcmd_add_arg(kmd, host)
            safe_shcmd_add_arg(cmd, host)
    else:
        safe_shcmd_add_arg(kmd, host)
        safe_shcmd_add_arg(cmd, host)

def swssh_deactivate_sanity_check():
    g_active_flag = 0

def swssh_reset_module():
    g_did_get_command = 0

def swssh_landing_command(shellname, opt_no_getconf):
    if shellname:
        if shellname == "bash":
            return sshc.SWSSH_BASH_SHELL_COMMAND
        elif shellname == lc.SH_A_sh:
            return sshc.SWSSH_SH_SHELL_COMMAND
        elif shellname == lc.SH_A_ash:
            return sshc.SWSSH_ASH_SHELL_COMMAND
        elif shellname == lc.SH_A_ksh:
            return sshc.SWSSH_KSH_SHELL_COMMAND
        elif shellname == lc.SH_A_mksh:
            return sshc.SWSSH_MKSH_SHELL_COMMAND
        elif shellname == lc.SH_A_dash:
            return sshc.SWSSH_DASH_SHELL_COMMAND
        elif shellname == "posix":
            return sshc.SWSSH_POSIX_SHELL_COMMAND
        elif shellname == "detect":
            return sshc.SWSSH_SYSTEM_SHELL_COMMAND
        else:
            return sshc.SWSSH_SYSTEM_SHELL_COMMAND
    else:
        if opt_no_getconf == 0:
            return sshc.SWSSH_POSIX_SHELL_COMMAND
        else:
            return sshc.SWSSH_SYSTEM_SHELL_COMMAND

def swssh_protect_shell_metacharacters(command, nhops, taints):
    tmp = strob_open(1)
    ns = [None, None]
    s = strob_strcpy(tmp, strob_str(command))
    command = ""
    ns[1] = '\0'
    for c in s:
        if c in taints:
            cat_escapes(command, nhops)
        ns[0] = c
        command += ns[0]
    strob_close(tmp)
    return 0

def swssh_determine_target_path(target, path):
    ds = None
    p = None
    if target[0] == ':' or target[0] == '/' or target[0] == '.' or target[0] == '-':
        path = target
        target = target + len(path)
    else:
        ds = target
        p = ds.rindex(':')
        if p:
            path = p + 1
        elif not p:
            if ds and (ds[0] == '/' or ds[0] == '.' or ds[0] == '-'):
                path = ds
        else:
            return -1
        if path:
            path = target + len(target)
            if sshc.TARGET_DELIM in path:
                print("target path not at terminal host", file=sys.stderr)
                return -1
            else:
                return -1
    return 0

def swssh_parse_target(cmd: SHCMD, kmd: SHCMD, target: [], ssh_command: str, fp_remote_ssh_command: str, path: List[str], terminal_host: List[str], tty_option: str, do_imsg: int, sshoption: str, do_forward_auth_agent: int) -> int:
    cmdcount: int = 0
    icmdcount: int = 0
    nhops: int = 0
    tmp = STROB()
    tmp2 = STROB()
    parent_host: str = ""
    tok: []
    clean_leading_comm_at_sign(target)
    path = ""
    if ssh_command is None:
        ssh_command = "ssh"
    is_ssh = ssh_command.find("ssh")
    strob_strcpy(tmp, fp_remote_ssh_command)
    s = strob_str(tmp)
    if 0 and s == strob_str(tmp):
        pass
    else:
        swlib_basename(tmp, fp_remote_ssh_command)
    remote_ssh_command = strob_str(tmp)
    ds = target
    if target[0] == ':' or target[0] == '/' or target[0] == '.' or target[0] == '-':
        path = target
        target = target + len(path)
    else:
        p = target.rfind(':')
        if p:
            path = target[p+1:]
        elif not p:
            if ds and (ds[0] == '/' or ds[0] == '.' or ds[0] == '-'):
                path = ds
        else:
            pass
        if path:
            if sshc.TARGET_DELIM in path:
                print("target path not at terminal host")
                return -1
    strob_strcpy(tmp, target)
    s = strob_str(tmp)
    while tok == strob_strstrtok(tmp, s, sshc.TARGET_DELIM):
        nhops += 1
        s = None
    if nhops > 1:
        is_multihop = 1
    strob_strcpy(tmp, target)
    s = strob_str(tmp)
    while tok == strob_strstrtok(tmp, s, sshc.TARGET_DELIM):
        user = None
        host = None
        terminal_host = None
        s = None
        commat = tok.find('@')
        if commat:
            user = tok
            host = commat + 1
            tok[commat] = '\0'
        else:
            host = tok
        if host.find(':'):
            host[host.find(':')] = '\0'
        if len(host):
            terminal_host = host
            if cmdcount == 0:
                safe_shcmd_add_arg(kmd, ssh_command)
                safe_shcmd_add_arg(cmd, ssh_command)
            else:
                add_host_message(do_imsg, cmd, parent_host, cmdcount)
                add_host_kill(do_imsg, kmd, parent_host, cmdcount)
                safe_shcmd_add_arg(kmd, remote_ssh_command)
                safe_shcmd_add_arg(cmd, remote_ssh_command)
            parent_host = host
            if is_ssh:
                if tty_option:
                    safe_shcmd_add_arg(cmd, tty_option)
                    safe_shcmd_add_arg(cmd, tty_option)
                    safe_shcmd_add_arg(kmd, tty_option)
                    safe_shcmd_add_arg(kmd, tty_option)
            cmdcount += 1
        elif len(host) == 0 and icmdcount == 0:
            safe_shcmd_add_arg(cmd, "/bin/sh")
        else:
            return -1
        if user:
            safe_shcmd_add_arg(cmd, "-l")
            safe_shcmd_add_arg(cmd, user)
            safe_shcmd_add_arg(kmd, "-l")
            safe_shcmd_add_arg(kmd, user)
        if len(host):
            parse_host_port(host, cmd, kmd)
        elif len(host) == 0 and icmdcount == 0:
            safe_shcmd_add_arg(cmd, "-c")
        icmdcount += 1
    if icmdcount == 0:
        safe_shcmd_add_arg(cmd, "/bin/sh")
        safe_shcmd_add_arg(cmd, "-c")
    del remote_ssh_command
    strob_close(tmp)
    strob_close(tmp2)
    return cmdcount

def swssh_assemble_ssh_cmd(shcmd: SHCMD, cmdlist: STRAR, delimlist: STRAR, nhops: int) -> int:
    cmd1 = str()
    i = 0
    g_did_get_command = 0
    while cmd1 == strar_get(cmdlist, i):
        safe_shcmd_add_arg(shcmd, cmd1)
        i += 1
    return 0
