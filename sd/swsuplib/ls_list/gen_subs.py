# sd.swsuplib.ls_list.gen_subs.py
# == This file has been MODIFIED by jhlowe@acm.org essentially beyond
#    recognition with the original (2008-03-13).
#
# Copyright (c) 2001 Thorsten Kukuk.
#
# Copyright (c) 1992 Keith Muller.
# Copyright (c) 1992, 1993
# The Regents of the University of California.  All rights reserved.
#
# This code is derived from software contributed to Berkeley by
# Keith Muller of the University of California, San Diego.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
# This product includes software developed by the University of
# California, Berkeley and its contributors.
# 4. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

import os
import stat
import time
from sd.swsuplib.misc.swlib import swlib_toggle_trailing_slash
from sd.swsuplib.ls_list.vis import VisConst
from sd.swsuplib.ls_list.ls_list import lslistc


class GenSubsConst:
    MODELEN = 20
    DATELEN = 64
    DAYSPERNYEAR = 365
    SECSPERDAY = 60 * 60 * 24
    SIXMONTHS = (DAYSPERNYEAR / 2) * SECSPERDAY
    CURFRMT = "%b %e %H:%M"
    OLDFRMT = "%b %e  %Y"
    NAME_WIDTH = 8
    PAX_DIR = stat.S_IFDIR
    PAX_CHR = stat.S_IFCHR
    PAX_BLK = stat.S_IFBLK
    PAX_REG = stat.S_IFREG
    PAX_SLK = stat.S_IFLNK
    PAX_SCK = stat.S_IFSOCK
    PAX_FIF = stat.S_IFIFO
    PAX_HLK = stat.S_IFLNK
    PAX_HRG = stat.S_IFLNK
    PAX_CTG = stat.S_IFREG
    UGSWIDTH = 18
    ugswidth = UGSWIDTH
    encoding_flag = 0
    did_drop = 0

GenSubs = GenSubsConst
def MAJOR(x):
    return os.major(x)

def MINOR(x):
    return os.minor(x)

def TODEV(x, y):
    return os.makedev(x, y)

def ls_list_set_encoding_flag(flag):
    global encoding_flag
    encoding_flag = flag

def ls_list_get_encoding_flag():
    if encoding_flag == 0:
        return VisConst.VIS_NONE
    return encoding_flag

def ls_list_set_encoding_by_lang():
    flag = VisConst.VIS_OCTAL if os.getenv("LANG") == "C" else VisConst.VIS_NONE
    ls_list_set_encoding_flag(flag)
    return flag

def prepend_dotslash(s1, fp):
    if s1 == ".":
        return "./"
    elif s1.startswith("./"):
        return s1
    elif s1.startswith("/"):
        fp.append(".")
    else:
        fp.append("./")
    return s1

def tartime(t):
    return time.strftime("%b %d %H:%M", time.localtime(t))

def strip_slash(pname, vflag):
    name = pname
    if vflag & lslistc.LS_LIST_VERBOSE_STRIPSLASH:
        while name and name[0] == '.' and name[1] == '/':
            name = name[1:]
        while name and name[0] == '/' and name[1] != '':
            name = name[1:]
    return name
def print_date(fp, vflag, type1, mtime):
    if vflag & lslistc.LS_LIST_VERBOSE_WITH_ALL_DATES:
        fp.append(tartime(mtime))
    elif vflag & lslistc.LS_LIST_VERBOSE_WITH_REG_DATES:
        if type1 == GenSubs.PAX_REG:
            fp.append(tartime(mtime))
    else:
        pass

def safe_print(s1, fp):
    if os.isatty(fp.fileno()):
        for c in s1:
            fp.write(c.encode('unicode_escape').decode())
    else:
        fp.write(s1)

def ls_list_safe_print_to_strob(fp_str, fp, do_prepend):
    if do_prepend:
        fp_str = prepend_dotslash(fp_str, fp)
    if 1 or 0:
        for c in fp_str:
            fp.write(c.encode('unicode_escape').decode())
    else:
        fp.write(fp_str)

def print_size_rdev(fp, file_hdr, type1):
    if type1 == GenSubs.PAX_CHR or type1 == GenSubs.PAX_BLK:
        fp.append("{:4},{:4}".format(file_hdr.c_rdev_maj, file_hdr.st_rdev_min))
    else:
        if type1 == GenSubs.PAX_SLK or type1 == GenSubs.PAX_HLK:
            sizeme = 0
        else:
            sizeme = file_hdr.c_filesize
        fp.append(str(sizeme))

def print_mode_owners(fp, file_hdr, type1, vflag, fp_uname, fp_gname):
    f_mode = ""
    if vflag & lslistc.LS_LIST_VERBOSE_WITHOUT_PERMISSIONS:
        f_mode = ""
    if vflag & lslistc.LS_LIST_VERBOSE_WITHOUT_OWNERS:
        uname = ""
        gname = ""
        l_uid = 0
        l_gid = 0
    else:
        uname = fp_uname
        gname = fp_gname
        l_uid = file_hdr.c_uid
        l_gid = file_hdr.c_gid
    if vflag & lslistc.LS_LIST_VERBOSE_WITH_NAMES or (vflag & lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_IDS and vflag & lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_NAMES):
        if lslistc.LS_LIST_VERBOSE_ALTER_FORM & vflag:
            f_mode = f_mode.rstrip()
            if vflag & lslistc.LS_LIST_VERBOSE_WITHOUT_OWNERS:
                fp.append("[{}] [len=0 []]".format(f_mode))
            else:
                tmpstrob = "{}({})/{}({})".format(l_uid, uname, l_gid, gname)
                fp.append("[{}] [len={} [{}]]".format(f_mode, len(tmpstrob), tmpstrob))
        else:
            pad = len(uname) + 2 + len(gname) + 2 + 9 + 1 + 1 + 11 + 11 + 0
            if pad > GenSubs.ugswidth:
                GenSubs.ugswidth = pad
            fp.append("{}[{}]({})/[{}]({}){:>{}}".format(f_mode, l_uid, uname, l_gid, gname, "", GenSubs.ugswidth - pad))
    elif vflag & lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_IDS:
        numid = "{}/{}".format(l_uid, l_gid)
        pad = len(numid) + 8 + 1
        if pad > GenSubs.ugswidth:
            GenSubs.ugswidth = pad
        if lslistc.LS_LIST_VERBOSE_ALTER_FORM & vflag:
            fp.append("[{}] [len={} [{}]]".format(f_mode, len(numid), numid))
        else:
            fp.append("{}{}{:>{}}".format(f_mode, numid, "", GenSubs.ugswidth - pad))
    elif vflag & lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_NAMES and len(uname) and len(gname):
        pad = len(uname) + len(gname) + 9 + 1
        if pad > GenSubs.ugswidth:
            GenSubs.ugswidth = pad
        if lslistc.LS_LIST_VERBOSE_ALTER_FORM & vflag:
            tmpstrob = "{}/{}".format(uname, gname)
            fp.append("[{}] [len={} [{}]]".format(f_mode, len(tmpstrob), tmpstrob))
        else:
            fp.append("{}{}{:{}}".format(f_mode, uname, gname, GenSubs.ugswidth - pad))
    else:
        if len(uname) and len(gname):
            pad = len(uname) + len(gname) + 9 + 1
            if pad > GenSubs.ugswidth:
                GenSubs.ugswidth = pad
            if lslistc.LS_LIST_VERBOSE_ALTER_FORM & vflag:
                tmpstrob = "{}/{}".format(uname, gname)
                fp.append("[{}] [len={} [{}]]".format(f_mode, len(tmpstrob), tmpstrob))
            else:
                fp.append("{}{}{:{}}".format(f_mode, uname, gname, GenSubs.ugswidth - pad))
        else:
            numid = "{}/{}".format(l_uid, l_gid)
            pad = len(numid) + 8 + 1
            if pad > GenSubs.ugswidth:
                GenSubs.ugswidth = pad
            if lslistc.LS_LIST_VERBOSE_ALTER_FORM & vflag:
                fp.append("[{}] [len={} [{}]]".format(f_mode, len(numid), numid))
            else:
                fp.append("{}{}{:{}}".format(f_mode, numid, "", GenSubs.ugswidth - pad))

def print_linkname(fp, type1, ln_name, do_prepend):
    if type1 == GenSubs.PAX_HLK or type1 == GenSubs.PAX_HRG:
        fp.append(" link to ")
        ls_list_safe_print_to_strob(ln_name, fp, do_prepend)
    else:
        if type1 == GenSubs.PAX_SLK:
            fp.append(" -> ")
            ls_list_safe_print_to_strob(ln_name, fp, do_prepend)
        else:
            fp.append("")
            ls_list_safe_print_to_strob(ln_name, fp, do_prepend)

def print_dig(fp, file_hdr, do_dig, key, val):
    if file_hdr.digsM.do_poisonM and len(val) == 0:
        fp.append("<not available>")
    else:
        fp.append(val)

def print_sha512(fp, file_hdr):
    print_dig(fp, file_hdr, file_hdr.digsM.do_sha512, "SHA512=", file_hdr.digsM.sha512)

def print_sha1(fp, file_hdr):
    print_dig(fp, file_hdr, file_hdr.digsM.do_sha1, "SHA1=", file_hdr.digsM.sha1)

def print_md5(fp, file_hdr):
    print_dig(fp, file_hdr, file_hdr.digsM.do_md5, "MD5=", file_hdr.digsM.md5)

def alt_ls_list_to_string(fp_name, ln_name, file_hdr, now, fp, uname, gname, type1, vflag):
    name = strip_slash(fp_name, vflag)
    if type1 == GenSubs.PAX_DIR:
        swlib_toggle_trailing_slash("drop", fp_name, GenSubs.did_drop)
    mtime = file_hdr.c_mtime
    fp.clear()
    ls_list_safe_print_to_strob(name, fp, vflag & lslistc.LS_LIST_VERBOSE_PREPEND_DOTSLASH)
    name_len = len(fp)
    fp.clear()
    print_linkname(fp, 0 if lslistc.LS_LIST_VERBOSE_LINKNAME_PLAIN & vflag else type1, ln_name, vflag & lslistc.LS_LIST_VERBOSE_PREPEND_DOTSLASH)
    linkname_len = len(fp)
    fp.append(lslistc.LS_LIST_NAME_LENGTH + "={:d} ".format(name_len))
    ls_list_safe_print_to_strob(name, fp, vflag & lslistc.LS_LIST_VERBOSE_PREPEND_DOTSLASH)
    fp.append(" [" + lslistc.LS_LIST_LINKNAME_LENGTH + "={:d} " + lslistc.LS_LIST_LINKNAME_MARK)
    print_linkname(fp, 0 if lslistc.LS_LIST_VERBOSE_LINKNAME_PLAIN & vflag else type1, ln_name, vflag & lslistc.LS_LIST_VERBOSE_PREPEND_DOTSLASH)
    fp.append("]]")
    fp.append(" [")
    if vflag & lslistc.LS_LIST_VERBOSE_WITH_SIZE:
        print_size_rdev(fp, file_hdr, type1)
    fp.append("]")
    fp.append(" ")
    print_mode_owners(fp, file_hdr, type1, vflag, uname, gname)
    fp.append(" [")
    print_date(fp, vflag, type1, mtime)
    fp.append("]")
    fp.append(" [")
    if type1 == GenSubs.PAX_REG:
        if vflag & lslistc.LS_LIST_VERBOSE_WITH_MD5:
            print_md5(fp, file_hdr)
    fp.append("]")
    fp.append(" [")
    if type1 == GenSubs.PAX_REG:
        if vflag & lslistc.LS_LIST_VERBOSE_WITH_SHA1:
            print_sha1(fp, file_hdr)
    fp.append("]")
    fp.append(" [")
    if type1 == GenSubs.PAX_REG:
        if vflag & lslistc.LS_LIST_VERBOSE_WITH_SHA512:
            print_sha512(fp, file_hdr)
    fp.append("]")
    if type1 == GenSubs.PAX_DIR:
        swlib_toggle_trailing_slash("restore", fp_name, GenSubs.did_drop)
    return

def ls_list(name, ln_name, sbp, now, fp, uname, gname, type1, vflag):
    buf = []
    ls_list_to_string(name, ln_name, sbp, now, buf, uname, gname, type1, vflag)
    fp.write("{}\n".format("".join(buf)))
    fp.flush()

def ls_list_to_string(fp_name, ln_name, file_hdr, now, fp, uname, gname, type, vflag):
    name = strip_slash(fp_name, vflag)
    if vflag & lslistc.LS_LIST_VERBOSE_OFF:
        fp.append(name)
        return
    elif lslistc.LS_LIST_VERBOSE_ALTER_FORM & vflag:
        alt_ls_list_to_string(fp_name, ln_name, file_hdr, now, fp, uname, gname, type, vflag)
        return
    else:
        pass
    mtime = file_hdr.c_mtime
    fp.clear()
    print_mode_owners(fp, file_hdr, type, vflag, uname, gname)
    fp.append(" ")
    print_size_rdev(fp, file_hdr, type)
    fp.append(" ")
    print_date(fp, vflag, type, mtime)
    fp.append(" ")
    ls_list_safe_print_to_strob(name, fp, vflag & lslistc.LS_LIST_VERBOSE_PREPEND_DOTSLASH)
    print_linkname(fp, 0 if lslistc.LS_LIST_VERBOSE_LINKNAME_PLAIN & vflag else type, ln_name, vflag & lslistc.LS_LIST_VERBOSE_PREPEND_DOTSLASH)
    return