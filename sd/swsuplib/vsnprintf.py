# vsnprintf.py - Mimic various print functions: snprintf, vsnprint


import sys
from ctypes import *
def snprintf(size, format_str, *args):
    formatted_str = format_str.format(*args)
    return formatted_str[:size] if size < len(formatted_str) else formatted_str

def sprintf(str, fmt, *args):
    ret = sys.stdout.write(fmt % args)
    return ret
def vsnprintf(str1, size, format_str, *args):
    ret = 0
    at = 0
    left = size
    fmt = format_str.decode('utf-8')
    arg_index = 0

    while fmt:
        if fmt[at] != '%':
            if left > 1:
                str1[at] = fmt[at]
                at += 1
                left -= 1
            ret += 1
            fmt = fmt[1:]
        else:
            fmt = fmt[1:]
            if fmt[at] == '%':
                if left > 1:
                    str1[at] = '%'
                    at += 1
                    left -= 1
                ret += 1
                fmt = fmt[1:]
            else:
                if fmt[at] in 'diouxXeEfFgGcs':
                    conv = fmt[at]
                    fmt = fmt[1:]
                    if conv == 's':
                        arg = args[arg_index].encode('utf-8')
                        arg_index += 1
                        strlen = len(arg)
                        if left > strlen:
                            str1[at:at+strlen] = arg
                            at += strlen
                            left -= strlen
                        ret += strlen
                    elif conv in 'di':
                        arg = args[arg_index]
                        arg_index += 1
                        numstr = str1(arg).encode('utf-8')
                        numlen = len(numstr)
                        if left > numlen:
                            str1[at:at+numlen] = numstr
                            at += numlen
                            left -= numlen
                        ret += numlen
                    elif conv in 'ouxX':
                        arg = args[arg_index]
                        arg_index += 1
                        numstr = None
                        if conv == 'o':
                            numstr = oct(arg).encode('utf-8')
                        elif conv == 'u':
                            numstr = str1(arg).encode('utf-8')
                        elif conv == 'x':
                            numstr = hex(arg).encode('utf-8')
                        elif conv == 'X':
                            numstr = hex(arg).upper().encode('utf-8')
                        numlen = len(numstr)
                        if left > numlen:
                            str1[at:at+numlen] = numstr
                            at += numlen
                            left -= numlen
                        ret += numlen
                    elif conv in 'eEfFgG':
                        arg = args[arg_index]
                        arg_index += 1
                        numstr = ('{0:.6' + conv + '}').format_str(arg).encode('utf-8')
                        numlen = len(numstr)
                        if left > numlen:
                            str1[at:at+numlen] = numstr
                            at += numlen
                            left -= numlen
                        ret += numlen
                    elif conv == 'c':
                        arg = args[arg_index]
                        arg_index += 1
                        if left > 1:
                            str1[at] = arg
                            at += 1
                            left -= 1
                        ret += 1
                else:
                    break

    if left > 0:
        str1[at] = 0
    return ret


# The fprintf function is used for formatted output to a stream.
# In Python, we can use the print function with file=sys.stderr or file=sys.stdout for stderr and stdout respectively.
# The va_list and related functionality is not needed in Python due to *args and **kwargs.

def fprintf(fp, fmt, *args):
    formatted_string = fmt % args
    fp.write(formatted_string)
    fp.flush()

# Added fhe following functions to provide vsnprintf function


def vsprintf(result, format_, args):
    return int_vasprintf(result, format_, args)
def int_vasprintf(result, format_, args):
    p = format_
    total_width = len(format_) + 1
    ap = c_void_p()
    if sys.version_info >= (3, 0):
        memmove(addressof(ap), addressof(args), sizeof(c_void_p))
    else:
        memmove(addressof(ap), addressof(args), sizeof(ap))
    while p != '\0':
        if p == '%':
            p += 1
            while p in "-+ #0":
                p += 1
            if p == '*':
                p += 1
                total_width += abs(ap.va_arg(int))
            else:
                total_width += int(p)
                p = int(p)
            if p == '.':
                p += 1
                if p == '*':
                    p += 1
                    total_width += abs(ap.va_arg(int))
                else:
                    total_width += int(p)
                    p = int(p)
            while p in ["h", "l", "L"]:
                p += 1
            total_width += 30
            if p in ["d", "i", "o", "u", "x", "X", "c"]:
                ap.va_arg(int)
            elif p in ["f", "e", "E", "g", "G"]:
                ap.va_arg(float)
                total_width += 307
            elif p == "s":
                total_width += len(ap.va_arg(str))
            elif p in ["p", "n"]:
                ap.va_arg(str)
            p += 1
    result = create_string_buffer(total_width)
    if result is not None:
        return vsprintf(result, format_, args)
    else:
        return -1

# vfprint
import ctypes

from io import StringIO


# Constants
BUF = 68
ALT = 0x001
HEXPREFIX = 0x002
LADJUST = 0x004
LONGDBL = 0x008
LONGINT = 0x010
QUADINT = 0x020
SHORTINT = 0x040
ZEROPAD = 0x080
FPT = 0x100
STATIC_ARG_TBL_SIZE = 8

def __sprint(fp, uio):
    if uio['uio_resid'] == 0:
        uio['uio_iovcnt'] = 0
        return 0
    err = fp.write(uio['uio_iov'])
    uio['uio_resid'] = 0
    uio['uio_iovcnt'] = 0
    return err

def __sbprintf(fp, fmt, *args):
    fake_file = StringIO()
    ret = fake_file.write(fmt % args)
    if ret >= 0:
        fake_file.flush()
    if fake_file.getvalue():
        fp.write(fake_file.getvalue())
    return ret

def to_digit(c):
    return ord(c) - ord('0')

def is_digit(c):
    return 0 <= to_digit(c) <= 9

def to_char(n):
    return chr(n + ord('0'))

def __ultoa(val, base, octzero, xdigs):
    cp = []
    if base == 10:
        if val < 10:
            cp.insert(0, to_char(val))
            return ''.join(cp)
        if val > ctypes.c_long.from_buffer(ctypes.c_int()).value:
            cp.insert(0, to_char(val % 10))
            sval = val // 10
        else:
            sval = val
        while sval != 0:
            cp.insert(0, to_char(sval % 10))
            sval //= 10
    elif base == 8:
        while val:
            cp.insert(0, to_char(val & 7))
            val >>= 3
        if octzero and cp[0] != '0':
            cp.insert(0, '0')
    elif base == 16:
        while val:
            cp.insert(0, xdigs[val & 15])
            val >>= 4
    else:
        raise ValueError("Invalid base for conversion.")
    return ''.join(cp)

def __uqtoa(val, base, octzero, xdigs):
    cp = []
    if val <= sys.maxsize:
        return __ultoa(val, base, octzero, xdigs)
    if base == 10:
        if val < 10:
            cp.insert(0, to_char(val % 10))
            return ''.join(cp)
        sval = val
        while sval != 0:
            cp.insert(0, to_char(sval % 10))
            sval //= 10
    elif base == 8:
        while val:
            cp.insert(0, to_char(val & 7))
            val >>= 3
        if octzero and cp[0] != '0':
            cp.insert(0, '0')
    elif base == 16:
        while val:
            cp.insert(0, xdigs[val & 15])
            val >>= 4
    else:
        raise ValueError("Invalid base for __uqtoa")
    return ''.join(cp)