#!/sbin/sh
# Product: UserResources
# Fileset: EMS-CORE
# preinstall
#
# (c) Copyright Hewlett-Packard Company 1996
#
###############################################################
# Revision history and tags
# tag        date     patch      description
# -----      -------- ---------- ---------------------------------------------
# JAGae50712 02/12/16 For release Location of temporary and backup files 
#                                 for 'inittab', 'services' and 'inetd.conf' 
#                                 changed from /var/tmp to /etc.
#                                 Here we also make sure that, those temporary 
#                                 and backup files to have appropriate 
#                                 permissions.
# JAGae71330 03/04/30 For release Script modified to get EMS installed on a 
#                                 system with OS 10.20/11.00 while the OS 
#                                 is getting upgraded to 11.11 or with OS 
#                                 11.22 while the OS is getting upgraded 
#                                 to 11.23. A flag file 
#                                 /etc/opt/resmon/monitors/ems_sys_update
#                                 is touched in the checkinstall script, 
#                                 indicating this as an upgrade condition.
#                                 During upgrade condition, the p_client is
#                                 killed using this script. This ensures 
#                                 p_client does not bringup any monitor 
#                                 during this period making sure that monitor
#                                 does not dump core during upgrade.
# JAGae73477 03/05/20 For release Script modified to check if log directories
#				  "/etc/opt/resmon/log" and
#                                 "/var/opt/resmon/log" exists. If the said 
#                                 directories are present, then change there
#                                 mode to 755.
# JAGaf72087 05/08/11 For release The toggle_switch command throws unformatted
#			         errors.As a fix we redirect output and prepend 
#                                them with the "NOTE"

# JAGaf78827 05/11/19 For Release For DRD safe program the "stop" should be 
#                                 called only if the SW_SESSION_IS_DRD is set
#                                 to 0.
###############################################################################
UTILS="/usr/lbin/sw/control_utils"
if [[ ! -f $UTILS ]]
then
   print "ERROR:   Cannot find $UTILS"
   exit 1
fi

. $UTILS

exitval=$SUCCESS            # Anticipate success

########################################################################
#### JAGaf72087
TMPFILE="/tmp/ems_tmp.$$"
#------------------------------------------------------------------------------
#  Shutdown Sentinel event monitors, but save the monitoring state if any.
#  Actually, shut down unless this is an intial install or a an OS Update.
#  (added for 11i)
#------------------------------------------------------------------------------

     if [ -r /var/stm/data/tools/monitor/HWE_SHUTDOWN ]
     then
        if [ ! -f /var/stm/data/tools/monitor/HWE_SHUTDOWN.preinst ]
        then
           cp /var/stm/data/tools/monitor/HWE_SHUTDOWN \
              /var/stm/data/tools/monitor/HWE_SHUTDOWN.preinst
#          echo "NOTE:    Preserving current asynchronous monitoring requests."
        fi
     fi
# JAGaf78827 the "toggle_switch stop" should be called only if the 
# SW_SESSION_IS_DRD  is set to 0 
     if [[ -z "${SW_SESSION_IS_UPDATE}" && -z "${SW_INITIAL_INSTALL}"
 
           && "$SW_SESSION_IS_DRD" -ne "1"  ]]
     then
        if [ -x /etc/opt/resmon/lbin/toggle_switch ]
        then
	    if [ -x /etc/opt/resmon/lib/libresmon.1 ]
	    then  ####JAGaf72087 start
		/etc/opt/resmon/lbin/toggle_switch stop 1>$TMPFILE 2>&1
             
	        if [[ -n "$TMPFILE" ]]
                then
                    cat $TMPFILE |  while read line
                    do
                      echo "NOTE:  $line"
                    done
                rm -f $TMPFILE 
                fi 
	     ####JAGaf72087 end 
 
	    else
		echo "WARNING: Could not stop any asynchronous monitors"
		echo "         that might be currently running."
		exitval=$WARNING
	    fi
        fi
     fi


########################################################################
#JAGae73477 start

# File permission of /etc/opt/resmon/log and /var/opt/resmon/log
# changed from 777 to 755 if these two directories exists.

  if [ -d /etc/opt/resmon/log ]
  then
     chmod 755 /etc/opt/resmon/log
  fi

  if [ -d /var/opt/resmon/log ]
  then
     chmod 755 /var/opt/resmon/log
  fi

#JAGae73477 end 
########################################################################

# Clean out any EMS entries that might be in inittab. They will be restored
# from within the configure/EMS-CORE script. They can cause things to happen
# too soon if left around while an OS update is taking place.

#------------------------------------------------------------------------------
# Definitions for the inittab files.
#------------------------------------------------------------------------------
#JAGae50712: 
#Changed the path of backup and temporary files to /etc/ from /var/tmp
INITTAB=/etc/inittab
NEW_INITTAB=/etc/ems_inittab.new

#------------------------------------------------------------------------------
# Remove the EMS inittab entries from /etc/inittab.
# This should be done when doing an OS update so that the p-client will
# not get relaunched. /etc/init q is explicitly NOT called since it is 
# illegal from preinstall. Init will re-read inittab automatically when
# one its children dies.
#------------------------------------------------------------------------------

if [ -n "${SW_SESSION_IS_UPDATE}" ]
then
  if [[ -f $INITTAB ]] then
    #JAGae50712: Begin
    #remove the old if exists, otherwise 'umask' can't change the 
    #permissions of the file.
    rm -f $NEW_INITTAB
    #Get the current umask value, set it to readonly,
    #and restore umask to previous value after the file creation.
    UmaskValue=`umask`
    umask u=r,g=r,o=r
    #JAGae50712:End

    grep -v ^ems[1-4] $INITTAB >$NEW_INITTAB

    #JAGae50712
    umask $UmaskValue

    if [[ -r $NEW_INITTAB ]]; then
      chmod 644 $INITTAB
      cp $NEW_INITTAB $INITTAB
      chmod 444 $INITTAB
      rm -f $NEW_INITTAB

      #JAGae71330 start
      if [ -a /etc/opt/resmon/monitors/ems_sys_update ]
        then

        #Kill p_client Also. During update we do not want to restart any monitor
        # even if dies.
         kill_named_procs p_client SIGKILL
      fi
      #JAGae71330 end
    else
      echo "WARNING: Could not remove EMS entries from $INITTAB"
      exitval=$WARNING
    fi
  fi
fi

###############################################################################

#------------------------------------------------------------------------------
# Prepare user-configurable files for migration
#------------------------------------------------------------------------------
for FILE in \
    /etc/opt/resmon/dictionary/DEFAULT              \

do
    newconfig_prep $FILE
done


#------------------------------------------------------------------------------
# Stop previous subagent if running
#------------------------------------------------------------------------------
   
if [[ $SW_ROOT_DIRECTORY = "/" ]]
then
    kill_named_procs emsagent SIGKILL 
    retval=$?
    if [[ $retval -ne 0 ]] 
    then
	    echo "WARNING: Could not kill subagent \"emsagent\" process"
	    exitval=$WARNING 
    fi
fi

return $exitval
