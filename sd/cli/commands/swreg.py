#!/usr/bin/python3

# swreg.py - swreg 

# Copyright (C) 2023 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys

cmd = os.path.split(sys.argv[0])[1]

swreg_parser = argparse.ArgumentParser(prog='swreg',
		   usage="""%(prog)s -l level [-u] [-v] [-g] [-C session_file] 
		   [-f object_file] [-S session_file] [-t target_file] 
		   [-x option=value] [-X option_file] [objects_to_register] 
		   [-T target_selections]""",
        formatter_class=argparse.RawTextHelpFormatter,
        epilog='''
  Objects_to_(un)register are specified as:  path ...

  Target selections are specified as:  -T[host] ...
 
''')

swreg_parser.add_argument("level", metavar='',
	choices=['root', 'shroot', 'prroot', 'depot'],
	help="levels of objects to register, one of: 'root', 'shroot', \n'prroot', 'depot'")
swreg_parser.add_argument("-i --import", metavar='', 
			help="import GPG key")
swreg_parser.add_argument("-u --unregister", metavar='', 
			help="unregister the objects")
swreg_parser.add_argument("-v --verbose", action='store_true', 
			help="write verbose output to stdout")
# -g --gnu option added for GNU Copyleft
swreg_parser.add_argument("-g", "--gnu", action='store_true',
			 help="print GNU Copyleft")
swreg_parser.add_argument("-C", metavar='session_file',
		    help="save the session to session_file")
swreg_parser.add_argument("-f", metavar='software_file',
			help="read software selections from this file")
swreg_parser.add_argument("-S", metavar='session_file',
	        help="read options, software selections, and target \nselections from \nthis file")
swreg_parser.add_argument("-t", metavar='target_file',
			help="read target selections from this file")
swreg_parser.add_argument("-x option=value", metavar='',
			help="set the option to value")
swreg_parser.add_argument("-X option_file", metavar='', 
			help="read option definitions from this file")
swreg_parser.add_argument("objects_to_register", nargs='*',
 		  help="objects to register ...")
# B.3.4 page 188 Using -T vs @ for target selection as it is easier in argparse. :)
swreg_parser.add_argument("-T", nargs="+",metavar="[host][:][path]",
			  help="Target selections specified as: [host][:][path] \n default is localhost:/")


swreg_parser.parse_args()
args = swreg_parser.parse_args()
print("DEBUG: Args in: ", cmd,".py ",  args)

# no arguments to swreg
#ERROR:   A valid registration level is needed for the successful
#         execution of "swreg"(). The valid levels are: "depot", "root",
#         "shroot" or "prroot".
#ERROR:   Command line parsing failed.

