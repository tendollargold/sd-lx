# sd.swsuplib.misc.swgp.py  --  General purpose routines.
#
#   Copyright (C) 2003-2004 James H. Lowe, Jr. <jhlowe@acm.org>
#   Copyright (C) 2023 Paul Weber convert to python
#   All Rights Reserved.
#
#   COPYING TERMS AND CONDITIONS
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# Header translation below this line
import errno
import os
import signal
import sys
import pty
import pysigset
import select

from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import SwlibConstants, fr
from sd.swsuplib.uxfio import uxfio_unix_safe_read

DO_APPEND = 1
DO_NOT_APPEND = 0
Gverbose = 0
gbe_silent = 0
SWGP_USEC = 20


def swgp_sig_handler_exit(signo):
    sys.exit(signo)

def swgp_signal_unblock(signo, oset):
    newmask = signal.pthread_sigmask(signo, oset)
    pysigset.sigemptyset(newmask)
    pysigset.sigaddset(newmask, signo)
    ret = pysigset.sigprocmask(signal.SIG_UNBLOCK, newmask, oset)
    if ret < 0:
        sys.stderr.write(f"swgp_signal_block error, signal={signo}: {os.strerror(1)}\n")
    return ret


def swgp_signal(signo, func):
    act = signal.signal(signo, func)
    return act


def swgp_signal_block(signo, oset):
    newmask = signal.pthread_sigmask(signo, oset)
    pysigset.sigemptyset(newmask)
    pysigset.sigaddset(newmask, signo)
    ret = pysigset.sigprocmask(signal.SIG_BLOCK, newmask, oset)
    if ret < 0:
        sys.stderr.write(f"swgp_signal_block error, signal={signo}: {os.strerror(1)}\n")
    return ret



def swgp_write_as_echo_line(fd, buf):
    return os.write(fd, buf.encode())


def swgp_read_line(fd, buf, do_append):
    if do_append == DO_APPEND:
        buf += os.read(fd, 1).decode()
    else:
        buf = os.read(fd, 1).decode()
    return len(buf)



def fdgo(select_ret, read_ret, location, gbe_silent):
    if select_ret > 0 >= read_ret:
        if read_ret < 0:
            if gbe_silent < 1:
                sys.stderr.write(f"{location}: fdgo(): read error, loc={location}, errno={errno}: {os.strerror(1)}\n")
            return -1
        else:
            return 0
    elif select_ret < 0:
        if gbe_silent < 1:
            sys.stderr.write(f"{location}: fdgo(): select error, loc={location}, errno={errno}: {os.strerror(1)}\n")
        return -1
    elif select_ret > 0 and read_ret > 0:
        return 1
    elif select_ret == 0 and read_ret == 0:
        return 1
    else:
        return 1


def read_line(fd, buf, length):
    return os.read(fd, length)


def read_pipe(ofd, ifd, buf, readreturn, location, statbytes, wf, rf):
    ret = rf(ifd, buf, SwlibConstants.SWLIB_PIPE_BUF)
    if readreturn:
        readreturn[0] = ret
    if ret < 0:
        if not gbe_silent:
            sys.stderr.write(f"read error ret = {ret} location=[{location}]\n")
    elif ret == 0:
        if Gverbose:
            sys.stderr.write(f"GOT EOF read returned [{ret}] location=[{location}] fd=[{ifd}]\n")
    elif ret > 0:
        if ofd >= 0:
            wret = wf(ofd, buf, ret)
            if wret != ret:
                if not gbe_silent:
                    sys.stderr.write(f"write error ret={wret} at location {location}\n")
                if readreturn:
                    readreturn[0] = wret
            else:
                if statbytes:
                    statbytes[0] += wret


def sumpPump(buf, ofd, ifd, location, sec, usec, loopcount, statbytes):
    count = 0
    selret, retval = 1, 1
    rret = []

    while loopcount == 0 or count < loopcount:
        selret = doPumpCycle(buf, ofd, ifd, location, sec, usec, rret, statbytes)
        count += 1
        retval = fdgo(selret, rret, location, gbe_silent)

        if Gverbose:
            sys.stderr.write(f"swssh: sumpPump: fdgo({selret}, {rret}, {location}) returned {retval}")

        if retval < 0:
            if gbe_silent < 1:
                sys.stderr.write("sumpPump internal error: loc=1")
            break
        elif retval == 0:
            break
        elif loopcount == 0 and retval > 0 and rret == 0:
            retval = 0
            break
        else:
            pass

    """
    0 means finished.
    1 means not finished.
    <0 means error.
    """
    return retval


def swgp_stdioPump(stdoutfd, outputpipefd, inputpipefd, stdinfd, opt_verbose, opt_be_silent, pid, waitret, statusp,
                   cancel, statbytes):
    retval = 0
    haveclose0 = 0
    haveclose1 = 0
    have_checked_for_epipe = 0
    buf = str(['\0' for _ in range(SwlibConstants.SWLIB_PIPE_BUF)])

    # Gverbose = opt_verbose
    gbe_silent = opt_be_silent

    if stdoutfd >= 0:
        temp_ref_buf = buf
        ret1 = sumpPump(temp_ref_buf, stdoutfd, outputpipefd, "ret1", 0, SWGP_USEC, 1, statbytes)
        buf = temp_ref_buf
    else:
        ret1 = 0
    if ret1 < 0:
        retval -= 1

    temp_ref_buf2 = buf
    ret0 = sumpPump(temp_ref_buf2, inputpipefd, stdinfd, "ret0", 0, SWGP_USEC, 1, statbytes)
    buf = temp_ref_buf2
    while (ret0 > 0 or ret1 > 0) and (cancel is None or cancel[0] == 0):
        if ret1 > 0:
            #			 
            # Inbound (relative to locahost) bytes.
            #			
            temp_ref_buf3 = buf
            ret1 = sumpPump(temp_ref_buf3, stdoutfd, outputpipefd, "ret1", 0, SWGP_USEC, 1, statbytes)
            buf = temp_ref_buf3
        else:
            if ret1 < 0:
                retval -= 1
            if haveclose1 == 0:
                if opt_verbose != 0:
                    sys.stderr.write(f"closing (ret1) outputpipefd fd={outputpipefd}\n")
                os.close(outputpipefd)
                haveclose1 = 1
            if opt_verbose != 0:
                sys.stderr.write(f"fd2={inputpipefd} fd3={stdinfd} ret (ret0)finishvalue={ret0} loc=e\n")
        if ret0 > 0:
            #			 
            #  Outbound bytes
            #			
            temp_ref_buf4 = buf
            ret0 = sumpPump(temp_ref_buf4, inputpipefd, stdinfd, "ret0", 0, SWGP_USEC, 1, statbytes)
            buf = temp_ref_buf4
        else:
            if ret0 < 0:
                retval -= 1
            if haveclose0 == 0:
                if opt_verbose != 0:
                    sys.stderr.write(f"closing (ret0) inputpipefd fd={inputpipefd}\n")
                os.close(inputpipefd)
                haveclose0 = 1
            if opt_verbose != 0:
                sys.stderr.write(f"fd2={inputpipefd} fd3={stdinfd} ret (ret0)finishvalue={ret0} loc=e\n")

        #
        # If the incoming (relative to localhost) stream is 
        # finished, then check the outbound descriptor
        # for closure, but check this only once.
        #		
        if ret1 <= 0 < ret0 and have_checked_for_epipe == 0:
            have_checked_for_epipe = 1

            if stdinfd != pty.STDIN_FILENO:
                os.close(stdinfd)

            if True:
                if pid > 0 and waitret and statusp:
                    if opt_verbose != 0:
                        sys.stderr.write(f"CHECKING pid {pid} in swgp_stdioPump\n")
                    waitret[0] = os.waitpid(pid, os.WNOHANG)
                    if waitret[0] > 0:
                        #						
                        # Good stop.
                        #						
                        if opt_verbose != 0:
                            sys.stderr.write("Good stop.\n")
                        ret0 = 0
                    elif waitret[0] == 0:
                        ret0 = 0
                    else:
                        sys.stderr.write(f"swbis: expected a child status: {os.strerror(1)}\n")
                        sys.stderr.write(f"swbis: may require SIGINT or" + " a EOF to terminate.\n")
                        ret0 = -1
                else:
                    ret0 = -1
            # else: # Code from here is unreachable
                # ret0 = 1  # continue.

    if cancel is not None and cancel[0] != 0:
        return 0
    if ret0 < 0:
        if opt_be_silent == 0:
            sys.stderr.write("swssh: stdioPump ERROR on ret0\n")
    if ret1 < 0:
        if gbe_silent < 1:
            sys.stderr.write("swssh: stdioPump ERROR on ret1\n")
    if opt_verbose:
        sys.stderr.write(f"fd0={stdoutfd} fd1={outputpipefd} ret1 finishvalue={ret1} loc=f\n")
        sys.stderr.write(f"fd2={inputpipefd} fd3={stdinfd} ret0 finishvalue={ret0} loc=g\n")
    if haveclose0 == 0:
        if opt_verbose:
            sys.stderr.write(f"closing2 (ret0) inputpipefd fd={inputpipefd}\n")
        os.close(inputpipefd)
        # haveclose0 = 1
    if haveclose1 == 0:
        if opt_verbose:
            sys.stderr.write(f"closing2 (ret1) outputpipefd fd={outputpipefd}\n")
        os.close(outputpipefd)
        # haveclose1 = 1
    if stdoutfd != sys.stdout.fileno() and stdoutfd >= 0:
        os.close(stdoutfd)
    if retval:
        sys.stderr.write("got error darnit in stdioPump\n")
    return retval


def doExhaustFd_i(buf, fd_from, location, sec, usec, readreturn, f_read):
    rfds = set()
    efds = set()
    tv = (sec, usec)
    tv.sec = sec
    tv.usec = usec
    rfds.add(fd_from)
    efds.add(fd_from)
    readreturn[0] = 0
    retval = select.select([fd_from], [], [fd_from], tv.sec)
    if retval[0]:
        if fd_from in retval[2]:
            readreturn[0] = -1
            return -1
        if fd_from in retval[0]:
            if fd_from > 0:
                read_pipe(-1, fd_from, buf, readreturn, location, None, None, f_read)
    elif retval[0] < [] and errno.EINTR:
        if Gverbose > 6:
            sys.stderr.write(f"{swlib_utilname_get()}: select: (errno=EINTR): {os.strerror(1)}\n")
            sys.stderr.write(
                f"{swlib_utilname_get()}: {__file__}:{fr.__line__} select retval = {retval[0]} fd_from={fd_from} location=[{fr.__line__}] : {os.strerror(1)}\n")
        retval = list(retval)
        retval[0] = 0
        retval[0] = tuple(retval)
    elif len(retval[0]) < 0 and errno != errno.EINTR:
        sys.stderr.write(f"{swlib_utilname_get()}: select error (errno={errno}): {os.strerror(1)}\n")
        sys.stderr.write(
            f"{swlib_utilname_get()}: {__file__}:{fr.__line__} select retval = {retval[0]} fd_from={fd_from} location=[{fr.__line__}] : {os.strerror(1)}\n")
    else:
        retval = list(retval)
        retval[0] = 0
        retval[0] = tuple(retval)
    return retval[0]


def doPumpCycle(buf, ofd, ifd, location, sec, usec, readreturn, statbytes):
    rfds = [ifd]
    tv = (sec, usec)
    tv.sec = sec
    tv.usec = usec
    readreturn[0] = 0
    retval = select.select([ifd], [], [], tv.sec)
    try:
        rfds, _, _ = select.select(rfds, [], [], tv.sec)
    except select.error as e:
        print(
            "select retval = {} ofd={} ifd={} location=[{}] : {}".format(retval, ofd, ifd, fr.__LINE__, os.strerror(e.err)))
    if ifd in rfds:
        read_pipe(ofd, ifd, buf, readreturn, location, statbytes, None, None)
    return 1


def swgp_find_lowest_fd(start):
    ret = os.dup(0)
    if ret < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: {os.strerror(1)}\n")
        sys.exit(1)
    os.close(ret)
    return ret


def swgp_check_fds():
    open_max = swgp_close_all_fd(-1)
    tmpfd = os.dup(sys.stdin.fileno())
    if tmpfd < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: {os.strerror(1)}\n")
        sys.stderr.write(f"{swlib_utilname_get()}: available file descriptors look suspicious, exiting now\n")
        sys.exit(1)
    max_fd = os.dup2(tmpfd, open_max - 1)
    os.close(tmpfd)
    if max_fd < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: {os.strerror(1)}\n")
        sys.stderr.write(f"{swlib_utilname_get()}: available file descriptors look suspicious, exiting now\n")
        sys.exit(1)
    os.close(max_fd)
    if max_fd - 3 < SwlibConstants.SW_MIN_FD_AVAIL:
        sys.stderr.write(f"{swlib_utilname_get()}: max fd available is {max_fd}\n")
        sys.stderr.write(f"{swlib_utilname_get()}: available file descriptors look suspicious, exiting now\n")
        sys.exit(1)


def swgp_close_all_fd(start):
    open_max = os.sysconf(os.sysconf_names['SC_OPEN_MAX'])
    if open_max < 0:
        if errno != 0:
            sys.stderr.write(f"{swlib_utilname_get()}: sysconf: open_max not determined.\n")
        open_max = SwlibConstants.SW_SC_OPEN_MAX
    else:
        pass
    if open_max < 16:
        sys.stderr.write(f"{swlib_utilname_get()}: the maximum number of open file descriptors\n")
        sys.stderr.write(f"{swlib_utilname_get()}: was determined to be {open_max}.\n")
        sys.stderr.write(f"{swlib_utilname_get()}: swbis requires more than this for its checks\n")
        sys.stderr.write(f"{swlib_utilname_get()}: against file descriptor leaks, and its normal\n")
        sys.stderr.write(f"{swlib_utilname_get()}: operations.\n")
        sys.exit(1)
    if start >= (open_max - 3):
        sys.stderr.write(f"{swlib_utilname_get()}: Oops, something is using all the file descriptors\n")
        sys.stderr.write(f"{swlib_utilname_get()}: swbis chooses not to continue\n")
        sys.exit(1)
    if start >= 0:
        for i in range(start, open_max):
            os.close(i)
    else:
        pass
    return open_max


def swgpReadFdNonblock(pipe_buf, fd_from, length):
    length[0] = 0
    try:
        ret = doExhaustFd_i(pipe_buf, fd_from, "ret2", 0, 1000, length, uxfio_unix_safe_read)
        return ret
    except OSError:
        return -1


def swgpReadLine(buf, fd_from, readreturn):
    try:
        ret = doExhaustFd_i(buf, fd_from, "ReadLine", 0, 40000, readreturn, read_line)
        return ret
    except OSError:
        ret = -1
        return ret
