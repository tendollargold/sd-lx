#!/sbin/sh -p
##########
# Product: DSAU
# Fileset: DSAU-CLOG-RUN
# configure
# @(#) $Revision: 1.13 $
##########
#
# (C) Copyright 2005-2007 Hewlett-Packard Development Company, L.P.
#
##########

typeset util=/usr/lbin/sw/control_utils
if [[ -r $util ]] ; then
    . $util
else
    echo "ERROR:   Cannot find the file $util." 1>&2
    exit 1
fi

function ConfigureHPSIM
{
    typeset mxadd=/opt/mx/lbin/add_tools
    typeset mxremove=/opt/mx/lbin/remove_tools
    typeset xmlsrc=/opt/dsau/config/hpsim/clog.xml
    typeset xmldir=/var/opt/mx/tools
    typeset xmldst=$xmldir/clog.xml

    # Load the clog definitions for HP SIM.
    if [[ ! -d $xmldir ]] ; then
        mkdir -p -m 0555 $xmldir
    fi

    # We renamed the System Log Viewer in the new clog.xml file. 
    # We need to delete the current tool before installing the new tool
    # So this remove is using the OLD install clog.xml, not the new one
    if [[ -x $mxremove ]]
    then
        $mxremove $xmldst > /dev/null 2>&1
    fi

    cp $xmlsrc $xmldst		# Now install the new one.

    # Call the add_tools script in case HP SIM is already configured.
    if [[ -x $mxadd ]] ; then
        $mxadd $xmldst
        if [[ $? -ne 0 ]] ; then
            msg WARNING "Cannot add $xmldst for Systems Insight Manager"
            exitval=$WARNING
        fi
    fi
}

function ConfigureHPSMH
{
    # Copy files from /opt/dsau to /opt/hpsmh
    typeset csrc=/opt/dsau/config/hpsmh/clog.cgis
    typeset jsrc=/opt/dsau/config/hpsmh/clog.js
    typeset xsrc=/opt/dsau/config/hpsmh/clog.xml
    typeset xlsrc=/opt/dsau/config/hpsmh/dsau-smh-ssa-tools.xml # X-launch apps
    typeset dstdir_cgibin=/opt/hpsmh/data/cgi-bin/clog
    typeset dstdir_htdocs=/opt/hpsmh/data/htdocs/clog
    typeset dstdir_webapp=/opt/hpsmh/webapp
    typeset dstdir_xlaunch=/opt/hpsmh/data/htdocs/xlaunch/xml
    typeset cdst=$dstdir_cgibin/clog.cgis
    typeset jdst=$dstdir_htdocs/clog.js
    typeset xdst=$dstdir_webapp/clog.xml
    typeset xldst=$dstdir_xlaunch/dsau-smh-ssa-tools.xml

    # Load the clog cgi and definitions for clog HP SMH.
    [[ ! -d $dstdir_cgibin  ]] && mkdir -p -m 0555 $dstdir_cgibin
    [[ ! -d $dstdir_htdocs  ]] && mkdir -p -m 0555 $dstdir_htdocs
    [[ ! -d $dstdir_webapp  ]] && mkdir -p -m 0555 $dstdir_webapp
    [[ ! -d $dstdir_xlaunch ]] && mkdir -p -m 0555 $dstdir_xlaunch

    cp -p $csrc $cdst && cp -p $jsrc $jdst && cp -p $xsrc $xdst && cp -p $xlsrc $xldst
    if [[ $? -ne 0 ]] ; then
        msg WARNING "Cannot add clog GUI for Systems Management Homepage"
        exitval=$WARNING
    fi

    # Create a symlink to smhrun.
    ln -sf ${SW_ROOT_DIRECTORY}opt/hpsmh/lbin/smhrun $dstdir_cgibin/clog.cgi
}

# Main
typeset -i10 exitval=$SUCCESS
typeset layout_dir=/var/opt/dsau/layouts
typeset gui_help_dir=/opt/hpsmh/data/help/clog/en

ConfigureHPSMH
ConfigureHPSIM

# Create a pipe
if [[ ! -p ${SW_ROOT_DIRECTORY}dev/log_consolidation_fifo ]]
then
    typeset old_umask=$(umask)
    umask 0077
    ${SW_ROOT_DIRECTORY}sbin/mknod \
	${SW_ROOT_DIRECTORY}dev/log_consolidation_fifo p
    umask $old_umask 2> /dev/null

    if [[ $? -ne 0 ]]
    then
	msg ERROR \
         "Cannot create ${SW_ROOT_DIRECTORY}dev/log_consolidation_fifo"
	exitval=$FAILURE 
    fi
fi 

# Make sure there's a layout dir 
if [[ ! -d $layout_dir ]] ; then
    mkdir -p -m 0555 $layout_dir
fi

# Copy syslog_layout
cp ${SW_ROOT_DIRECTORY}opt/dsau/config/layouts/syslog_layout \
   ${SW_ROOT_DIRECTORY}var/opt/dsau/layouts/syslog_layout

# Copy single_column_layout
cp ${SW_ROOT_DIRECTORY}opt/dsau/config/layouts/single_column_layout \
   ${SW_ROOT_DIRECTORY}var/opt/dsau/layouts/single_column_layout


# Make sure there's a gui_help_dir 
if [[ ! -d $gui_help_dir ]] ; then
    mkdir -p -m 0555 $gui_help_dir
fi


# Update /etc/PATH file
mod_pathfile -a P "/opt/dsau/bin"
if [[ $? -ne 0 ]] ; then
    msg WARNING "Cannot add /opt/dsau/bin to /etc/PATH"
    [[ $exitval -ne $FAILURE ]] && exitval=$WARNING
fi
mod_pathfile -a P "/opt/dsau/sbin"
if [[ $? -ne 0 ]] ; then
    msg WARNING "Cannot add /opt/dsau/sbin to /etc/PATH"
    [[ $exitval -ne $FAILURE ]] && exitval=$WARNING
fi

exit $exitval
