# sd.suplib.misc.topsf.py  --  Open (almost) any format and translate to a PSF and archive.
#
#   Copyright (C) 1999,2007,2014  Jim Lowe
#   Copyright (C) 2023-2024 Paul Weber conversion to Python
#   All rights reserved.
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import stat
import sys
import time

import rpm

from sd.swsuplib.cplob import cplob_open, cplob_additem, cplob_get_nused, cplob_val
from sd.swsuplib.misc.cstrings import strcmp, strstr, strrchr, strpbrk
from sd.swsuplib.misc.debpsf import debpsf_create, debpsf_open, debpsf_write_psf, debpsf_close
from sd.swsuplib.rpm.rpmfd import rpmfd_get_fd_fd, rpmfd_getfd
from sd.swsuplib.rpm.rpmheader import headerFree
from sd.swsuplib.rpm.rpmpsf import (rpmpsf_get_index_by_name, rpmpsf_get_rpmtagvalue, rpmpsf_make_package_prefix,
                                    rpmpsf_write_beautify_psf, rpmpsf_write_beautify_info, rpmpsf_write_info,
                                    rpmpsf_get_psf_size, rpmpsf_write_out_tag, rpmpsf_get_rpmtag_length,
                                    rpmpsf_write_psf,
                                    rpmpsf_get_rpm_filenames_tagvalue)
from sd.swsuplib.rpm.rpmtag import RpmTage
from sd.swsuplib.misc.shcmd import SHCMD, shcmd_open
from sd.swsuplib.misc.strar import strar_open, strar_close, strar_get, strar_add
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.swafcl import SWACFL, SwafclConstants, swacfl_find_entry, swacfl_add, swacfl_close
from sd.swsuplib.misc.swfork import swfork
from sd.swsuplib.misc.swgp import swgp_read_line, DO_NOT_APPEND
from sd.swsuplib.misc.swlib import SwlibConstants
from sd.swsuplib.misc.swlib import (swlib_strdup, swlib_ascii_text_fd_to_buf, swlib_unix_dircat,
                                    swlib_unexpand_escapes, swlib_process_hex_escapes, swlib_add_trailing_slash,
                                    swlib_squash_leading_slash, swlib_dir_compare, swlib_exec_filter,
                                    swlib_squash_illegal_tag_chars,
                                    swlib_open_memfd, swlib_expand_escapes, swlib_compare_8859)
from sd.swsuplib.misc.swvarfs import varfs, taru_read_header, taru_write_archive_member_data
from sd.swsuplib.strob import strob_chr_index
from sd.swsuplib.strob import strob_set_fill_char, strob_open, strob_close, strob_strcpy, STROB_DO_APPEND, \
    STROB_NO_APPEND
from sd.swsuplib.strob import strob_sprintf, strob_str, strob_strcat, strob_strlen, STROB
from sd.swsuplib.taru.ahs import ahs_get_perms, ahsstaticcreatefilehdr, ahsstaticgettarfilename, ahsstaticsetpaxlinkname
from sd.swsuplib.taru.ahs import ahsstaticgettarusername, ahsstaticgettargroupname, ahsstaticdeletefilehdr
from sd.swsuplib.taru.ahs import ahsstaticgettarlinkname, ahsstaticsettarfilename
from sd.swsuplib.taru.cpiohdr import CpioHdrConst
from sd.swsuplib.taru.filetypes import Filetypes
from sd.swsuplib.taru.mtar import taru_statbuf2filehdr
from sd.swsuplib.taru.otar import taru_tape_skip_padding, swlib_utilname_get
from sd.swsuplib.taru.tar import Tar
from sd.swsuplib.taru.taru import ArchiveFormat, tc
from sd.swsuplib.uinfile.uinfile import UinfileConstants, uinfile_close, uinfile_get_type
from sd.swsuplib.uinfile.uinfile import uinfile_decode_buftype, uinfile_open, uinfile_set_type, uinfile_open_with_name
from sd.swsuplib.uxfio import UxfioConst
from sd.swsuplib.uxfio import uxfio_unix_safe_write, uxfio_write, uxfio_close, uxfio_lseek, uxfio_opendup, uxfio_sfread
from sd.swsuplib.taru.xformat import (xformat_close)
from sd.swsuplib.taru.xformat import xformat_get_next_dirent, xformat_open, xformat_u_open_file, xformat_u_close_file, \
    xformat_write_by_fd
from sd.swsuplib.taru.xformat import xformat_open_archive_by_fd
from sd.swsuplib.taru.xformat import xformat_write_trailer, xformat_is_end_of_archive, \
    xformat_ahs_object, xformat_get_tar_typeflag

uic = UinfileConstants
shcmd = SHCMD
swacflc = SwafclConstants
r = rpm
rte = RpmTage
cpio = CpioHdrConst
ft = Filetypes()
swlibc = SwlibConstants
af = ArchiveFormat
uxfio = UxfioConst()
tar = Tar


class TopsfConstants:
    TOPSF_OPEN_NO_AUDIT = (1 << 17)
    TOPSF_PSF_FORM2 = 2
    TOPSF_PSF_FORM3 = 3
    TOPSF_FILE_STATUS_UNSET = 48  # ascii 0
    TOPSF_FILE_STATUS_IN_HEADER = 49  # ascii 1
    TOPSF_FILE_STATUS_IN_ARCHIVE = 50  # ascii 2


tsc = TopsfConstants


class TOPSF:
    def __init__(self):
        self.prefix_ = None
        self.cwd_prefix_ = None
        self.swacfl_ = None
        self.hl_node_names_ = None
        self.hl_linkto_names_ = None
        self.h_ = None
        self.format_desc_ = None
        self.user_addr = None
        self.rpmfd_ = None
        self.fd_ = 0
        self.use_recursive_ = 0
        self.reverse_links_ = 0
        self.archive_names = None
        self.file_status_array = None
        self.header_names = None
        self.debug_link_ = 0
        self.single_fileset_ = 0
        self.info_only_ = 0
        self.smart_path_ = 0
        self.form_ = 0
        self.taru = None
        self.control_directory = None
        self.mtime = 0
        self.debpsf = None
        self.rpmpsf = None
        self.pkgfilename = None
        self.checkdigestname = None
        self.owner = None
        self.group = None
        self.exclude_list = None
        self.rpm_construct_missing_files = 0
        self.rpmtag_default_prefix = None
        self.usebuf1 = None
        self.verbose = 0

#
# Include machine specific syscall numbers
#
def is_blank_line(s):
    while s and s != ':' and s != '\n':
        if s.isspace():
            pass
        else:
            return 0
        s += 1
    return 1


def is_name(s):
    while s and s != ':' and s != '\n':
        if s.isalpha() or s.isdigit() or s == '_' or s == '-':
            pass
        else:
            return 0
        s += 1
    if s != ':':
        return 0
    return 1

def topsf_add_excludes(topsf, buf):
    i = 0
    if topsf.exclude_list is None:
        return
    while (s := strar_get(topsf.exclude_list, i)) is not None:
        strob_sprintf(buf, STROB_DO_APPEND, swc.SW_A_exclude + " %s\n" % s)


def parse_slack_desc(desc, title, src):
    n = 0
    # Assume the existence of strob_open(), swlib_open_memfd(), uxfio_write(), uxfio_lseek(), strob_str(),
    # is_blank_line(), is_name(), strob_strcpy(), strob_strcat(), strob_strlen(), swgp_read_line(),
    # swlib_expand_escapes(), strob_close(), and uxfio_close() functions
    tmp = strob_open(32)
    fd = swlib_open_memfd()
    uxfio_write(fd, src, len(src))
    uxfio_lseek(fd, 0, os.SEEK_SET)

    strob_strcpy(title, "")
    strob_strcpy(desc, "")
    while n == swgp_read_line(fd, tmp, DO_NOT_APPEND) > 0:
        line = strob_str(tmp)
        s = line

        if s[0] == '#':
            pass
        elif is_blank_line(s):
            pass
        elif is_name(s):
            t = s[s.find(':') + 1:]
            if not t:
                return -1
            if t[0] == ' ':
                t = t[1:]
            if strob_strlen(title) == 0:
                strob_strcpy(title, t)
            elif is_blank_line(t):
                pass
            else:
                strob_strcat(desc, t)
        else:
            pass

    strob_strcpy(tmp, strob_str(desc))
    swlib_expand_escapes(None, None, strob_str(tmp), desc)

    strob_close(tmp)
    uxfio_close(fd)
    return 0


def topsf_parse_slack_pkg_name(pkgfilename, st):
    ret = 0
    tmp = strob_open(32)
    strob_strcpy(tmp, pkgfilename)

    n = strob_str(tmp)
    s = strrchr(n, b'.')
    if not s:
        return -1
    s[0] = b'\0'  # squash the ``.tgz''

    s = strrchr(n, b':')
    if s:
        s = s + 1
        n = s

    s = strrchr(n, b'/')
    if s:
        s = s + 1
        n = s

    s = strrchr(n, b'-')
    if not s:
        return -1
    s[0] = b'\0'
    s = s + 1
    build_num = s

    s = strrchr(n, b'-')
    if not s:
        return -1
    s[0] = b'\0'
    s = s + 1
    arch = s

    s = strrchr(n, b'-')
    if not s:
        return -1
    s[0] = b'\0'
    s = s + 1
    version = s

    name = n

    if strpbrk(name, f"/:"):
        ret = -2
    strar_add(st, name)
    strar_add(st, version)
    strar_add(st, arch)
    strar_add(st, build_num)

    strob_close(tmp)
    return ret


def parse_src_pkg_name(pkgfilename, st):
    # example: foo-devel-1.3.12.tar.gz
    tmp = strob_open(32)  # Undefined function strob_open
    strob_strcpy(tmp, pkgfilename)  # Undefined function strob_strcpy

    n = strob_str(tmp)  # Undefined function strob_str
    s = None
    if not s:
        s = n.find(".tgz")
    if not s:
        s = n.find(".tar")
    if not s:
        s = n.find(".tbz")

    if not s:
        pass  # return -1;
    else:
        n = n[:s]  # squash the ``.tgz''

    s = n.rfind('-')
    if not s:
        return -1
    else:
        s += 1
        version = n[s:]
        name = n[:s]
        strar_add(st, name)  # Undefined function strar_add
        strar_add(st, version)  # Undefined function strar_add
        strob_close(tmp)  # Undefined function strob_close
    return 0


def plain_source_tarball_write_psf_buf(topsf, psf):
    dir_mtime = 0
    name_fields = strar_open()
    xtmp = strob_open(32)
    tmp = strob_open(32)
    desc = strob_open(32)
    title = strob_open(32)

    if topsf.format_desc_.pathname_prefix is None:
        return -1

    ret = parse_src_pkg_name(topsf.format_desc_.pathname_prefix, name_fields)
    if ret < 0:
        print("%s: error parsing name, should be name-version{.tar|.tgz|*}: %s\n" % (
            swlib_utilname_get(), topsf.format_desc_.pathname_prefix))
        return -2

    if topsf.format_desc_.slackheader:
        tar_hdr = topsf.format_desc_.slackheader
        prefix_mode = tar_hdr.mode
        ul_dir_mtime = tar_hdr.mtime
        dir_mtime = ul_dir_mtime
    else:
        prefix_mode = 0

    psf = ""
    psf += swc.SW_A_distribution + "\n"
    psf += swc.SW_A_layout_version + " 1.0\n"
    psf += "dfiles dfiles\n"
    psf += "pfiles pfiles\n"
    psf += swc.SW_A_control_directory + " " + topsf.format_desc_.pathname_prefix + "\n"

    if topsf.checkdigestname:
        psf += "checkdigest <catalog/checkdigest\n"

    if prefix_mode:
        psf += swc.SW_A_mode + " %04o\n" % (prefix_mode & (
                stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO | stat.S_ISUID | stat.S_ISGID | stat.S_ISVTX))

    if dir_mtime:
        psf += swc.SW_A_mtime + " %lu\n" % dir_mtime

    psf += swc.SW_A_owner + " 0\n"
    psf += swc.SW_A_group + " 0\n"
    psf += "\n"

    xformat = xformat_open(-1, -1, "arf_ustar")
    ret = uxfio_lseek(topsf.fd_, 0, os.SEEK_SET)
    if ret < 0:
        sys.stderr.write("error")
        return -2

    ret = xformat_open_archive_by_fd(xformat, topsf.fd_,
                                     uic.UINFILE_DETECT_OTARFORCE | uic.UINFILE_DETECT_DEB_CONTROL,
                                     0)
    if ret < 0:
        sys.stderr.write("error")
        return -3

    # ret = xformat_read_header(xformat)
    if xformat_is_end_of_archive(xformat):
        ret = -5
        sys.stderr.write("%s: empty data archive [%d]\n" % (swlib_utilname_get(), ret))
        sys.stderr.write("error")
        return ret

    if xformat_get_tar_typeflag(xformat) != tar.DIRTYPE:
        ret = -6
        sys.stderr.write("%s: first file not a directory  [%d]\n" % (swlib_utilname_get(), ret))
        return ret

    ahs = xformat_ahs_object(xformat)
    name = ahsstaticgettarfilename(ahs.file_hdr)
    if swlib_dir_compare(name, topsf.format_desc_.pathname_prefix, 0):
        ret = -7
        sys.stderr.write(
            "%s: first file not %s %s\n" % (swlib_utilname_get(), topsf.format_desc_.pathname_prefix, name))
        return ret

    psf += "\n"
    psf += swc.SW_A_product + "\n"
    psf += swc.SW_A_tag + " %s\n" % strar_get(name_fields, 0)
    psf += swc.SW_A_revision + " %s\n" % strar_get(name_fields, 1)
    psf += swc.SW_A_control_directory + " \"\"\n"
    psf += "\n"
    psf += swc.SW_A_fileset + "\n"
    psf += swc.SW_A_tag + " bin\n"
    psf += swc.SW_A_control_directory + " \"\"\n"
    psf += swc.SW_A_directory + " . #/\n"
    psf += "file_permissions -o 0 -g 0\n"
    psf += swc.SW_A_file + " *\n"
    psf += swc.SW_A_exclude + " PSF\n"
    psf += swc.SW_A_exclude + " catalog\n"
    psf += swc.SW_A_exclude + " catalog/*\n"
    topsf.topsf_add_excludes(psf)

    strar_close(name_fields)
    strob_close(xtmp)
    strob_close(tmp)
    strob_close(desc)
    strob_close(title)


def slack_write_psf_buf(topsf, psf):
    st = os.stat(topsf.pkgfilename)
    slack_desc = None
    doinst = None
    name_fields = strar_open()
    xtmp = strob_open(32)
    tmp = strob_open(32)
    desc = strob_open(32)
    title = strob_open(32)
    vola = strob_open(32)

    if topsf.pkgfilename is None:
        sys.stderr.write("%s: must specify a slackware package as a regular file arg\n" +
                         swlib_utilname_get())
        sys.stderr.write("error")
        return -1

    temp_ref_pkgfilename = topsf.pkgfilename
    ret = topsf_parse_slack_pkg_name(temp_ref_pkgfilename, name_fields)
    topsf.pkgfilename = temp_ref_pkgfilename
    if ret < 0:
        sys.stderr.write("error")
        return -2

    #
    # sys.stderr.write("SLACK: [%s][%s][%s][%s]\n",
    # strar_get(name_fields, 0),
    # strar_get(name_fields, 1),
    # strar_get(name_fields, 2),
    # strar_get(name_fields, 3))
    #

    strob_sprintf(psf, STROB_NO_APPEND, "")
    strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_distribution + "\n")
    strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_layout_version + "1.0\n")
    strob_sprintf(psf, STROB_DO_APPEND, "dfiles dfiles\n")
    strob_sprintf(psf, STROB_DO_APPEND, "pfiles pfiles\n")
    if topsf.owner:
        strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_owner +
                      " %s\n", topsf.owner)
        if topsf.group:
            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_group +
                          " %s\n", topsf.group)

            strob_sprintf(psf, STROB_DO_APPEND, "\n")
            strob_sprintf(psf, STROB_DO_APPEND, "vendor\n")
            strob_sprintf(psf, STROB_DO_APPEND, "the_term_vendor_is_misleading True\n")
            swlib_squash_illegal_tag_chars(strar_get(name_fields, 3))
            strob_sprintf(psf, STROB_DO_APPEND, "tag \"%s\"\n", strar_get(name_fields, 3))
            strob_sprintf(psf, STROB_DO_APPEND, "title The Slackware Linux Project\n")
            strob_sprintf(psf, STROB_DO_APPEND, "url \"https://www.slackware.com\"\n")

            xformat = xformat_open(-1, -1, "arf_ustar")
            ret = uxfio_lseek(topsf.fd_, 0, os.SEEK_SET)
            if ret < 0:
                sys.stderr.write("error")
                return -2

            ret = xformat_open_archive_by_fd(xformat, topsf.fd_,
                                             uic.UINFILE_DETECT_OTARFORCE | uic.UINFILE_DETECT_DEB_CONTROL, 0)
            if ret < 0:
                sys.stderr.write("error")
                return -3

            # ret = xformat_read_header(xformat)
            if xformat_is_end_of_archive(xformat):
                ret = -5
                sys.stderr.write("%s: empty data archive [%d]\n" + swlib_utilname_get() + ret)
                sys.stderr.write("error")
                return ret

            if xformat_get_tar_typeflag(xformat) != tar.DIRTYPE:
                ret = -6
                sys.stderr.write("%s: first file not a directory  [%d]\n" + swlib_utilname_get() + ret)
                return ret

            ahs = xformat_ahs_object(xformat)
            name = ahsstaticgettarfilename(ahs.file_hdr)

            # we expect name to be "./"

            if strcmp(name, "./"):
                ret = -7
                sys.stderr.write("%s: first file not ./\n" + swlib_utilname_get())
                return ret

            # Make a temporary copy of the explicit file definition
            strob_sprintf(xtmp, STROB_NO_APPEND, swc.SW_A_file +
                          " -t d -o %s -g %s -m %o /\n", ahsstaticgettarusername(ahs.file_hdr),
                          ahsstaticgettargroupname(
                              ahs.file_hdr), (ahs_get_perms(ahs)))

            # Now audit the ./install/ directory contents

            name = xformat_get_next_dirent(xformat, st)
            while name != '\0':
                if strstr(name, "./install/") == name or strstr(name, "install/") == name:
                    if strstr(name, "./install/") == name:
                        name += 2
                    sys.stderr.write("NAME=%s" + name)
                    if strcmp(name, "install/doinst.sh") == 0:
                        temp_ref_name = name
                        topsf_h_write_to_buf(xformat, temp_ref_name, doinst)
                    elif strcmp(name, "install/") == 0:
                        pass
                    elif strcmp(name, "install/slack-desc") == 0:
                        temp_ref_name2 = name
                        topsf_h_write_to_buf(xformat, temp_ref_name2, slack_desc)
                    else:
                        temp_ref_name3 = name
                        topsf_h_write_to_buf(xformat, temp_ref_name3, None)
                        sys.stderr.write("%s: unrecognized skackware control file: %s\n" +
                                         swlib_utilname_get() + name)
                elif (sb := strstr(name, ".new")) is not None and (sb[4] == '\0'):
                    strob_sprintf(vola, 1, "file\n")
                    strob_sprintf(vola, 1, "source %s\n", name)
                    strob_sprintf(vola, 1, "path /%s\n", name)
                    strob_sprintf(vola, 1, "is_volatile true\n")
                name = xformat_get_next_dirent(xformat, st)

            if slack_desc:
                # sys.stderr.write("%s", strob_str(slack_desc));
                parse_slack_desc(desc, title, str(slack_desc))
                if desc.rfind('\n') != -1:
                    desc = desc[:desc.rfind('\n')]
                if desc.rfind('\r') != -1:
                    desc = desc[:desc.rfind('\r')]
                if title.rfind('\n') != -1:
                    title = title[:title.rfind('\n')]
                if title.rfind('\r') != -1:
                    title = title[:title.rfind('\r')]

            sys.stderr.write("title: [%s]" + strob_str(title))
            sys.stderr.write("description: [%s]" + strob_str(desc))

            sys.stderr.write("")
            swlib_squash_illegal_tag_chars(strar_get(name_fields, 0))
            strob_sprintf(psf, STROB_DO_APPEND, "\n")
            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_product + "\n")
            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_tag + " %s\n", strar_get(name_fields, 0))
            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_vendor_tag + " %s\n", strar_get(name_fields, 3))
            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_title + " \"%s\"\n", strob_str(title))
            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_description + " \"%s\"\n", strob_str(desc))

            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_revision + " %s\n", strar_get(name_fields, 1))
            strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_control_directory + " \"\"\n")
            if doinst:
                strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_postinstall +
                              " install/doinst.sh\n")

                strob_sprintf(psf, STROB_DO_APPEND, "\n")
                strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_fileset + "\n")
                strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_tag + " bin\n")
                strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_control_directory + " \"\"\n")
                strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_directory + " . /\n")

                # Use an explicit definition for the "./" archive member
                strob_sprintf(psf, STROB_DO_APPEND, "%s", strob_str(xtmp))

                # Recursive spec after the leading ./

                strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_file +
                              " *\n")

                strob_sprintf(psf, STROB_DO_APPEND, swc.SW_A_exclude + " PSF\n")
                topsf.topsf_add_excludes(topsf, psf)
                strob_sprintf(psf, STROB_DO_APPEND, "#", swc.SW_A_exclude + " control\n")
                strob_sprintf(psf, STROB_DO_APPEND, "#" + swc.SW_A_exclude + " control/*\n")

                strob_sprintf(psf, STROB_DO_APPEND, "%s\n", strob_str(vola))

                strar_close(name_fields)
                strob_close(xtmp)
                strob_close(tmp)
                strob_close(desc)
                strob_close(title)
                return 0


def slack_write_psf(topsf, fd_out):
    psf = strob_open(32)
    ret = topsf.slack_write_psf_buf(topsf, psf)
    if ret < 0:
        return -1
    ret = uxfio_write(fd_out, strob_str(psf), strob_strlen(psf))
    if ret != int(strob_strlen(psf)):
        ret = -1
    strob_close(psf)
    return ret


def plain_source_tarball_psf(topsf, fd_out):
    psf = strob_open(32)
    # ret = slack_write_psf_buf(topsf, psf);
    ret = topsf.plain_source_tarball_write_psf_buf(psf)
    if ret < 0:
        return -1
    ret = uxfio_write(fd_out, strob_str(psf), strob_strlen(psf))
    if ret != int(strob_strlen(psf)):
        ret = -1
    strob_close(psf)
    return ret


def find_link(topsf, keyname):
    node_names = topsf.hl_node_names_
    linkto_names = topsf.hl_linkto_names_

    if strstr(keyname, "./") == keyname:
        keyname += 2
    i = 0
    while name := cplob_val(node_names, i):
        linkname = cplob_val(linkto_names, i)
        #
        # if (strstr(name, "./") == name) name+=2
        # if (strstr(linkname, "./") == linkname) linkname+=2
        #
        if swlib_compare_8859(keyname, name) == 0 or swlib_compare_8859(keyname, linkname) == 0:
            return 1
        i += 1
    return 0

def topsf_set_taru(topsf, taru):
    topsf.taru = taru

def topsf_open(topsf, filename, oflags, package_name):
    topsf = TOPSF()

    # oflags |= UINFILE_DETECT_NATIVE;
    oflags |= uic.UINFILE_DETECT_FORCEUNIXFD

    sys.stderr.write("")
    topsf.swacfl_ = SWACFL()
    sys.stderr.write("")

    if strcmp(filename, "-") == 0 and package_name is not None:
        # sys.stderr.write("JL you're a SLACKER: %s:%s at line %d\n", __FILE__, __FUNCTION__, __LINE__);
        topsf.fd_ = uinfile_open_with_name(filename, 0, topsf.format_desc_, oflags, package_name)
    else:
        topsf.fd_ = uinfile_open(filename, 0, topsf.format_desc_, oflags)
    sys.stderr.write("")

    if topsf.fd_ < 0:
        sys.stderr.write("%s: error opening package.\n" + swlib_utilname_get())
        swacfl_close(topsf.swacfl_)
        del topsf
        sys.stderr.write("")
        return None
    sys.stderr.write("")
    topsf.cwd_prefix_ = str(None)
    topsf.prefix_ = str(None)
    topsf.hl_node_names_ = cplob_open(1)
    topsf.hl_linkto_names_ = cplob_open(1)
    # topsf->archive_names_ = cplob_open(1);
    topsf.archive_names = strar_open()

    topsf.file_status_array = strob_open(1)
    topsf.header_names = strar_open()
    strob_set_fill_char(topsf.file_status_array, ord('0'))
    cplob_additem(topsf.hl_node_names_, 0, None)
    cplob_additem(topsf.hl_linkto_names_, 0, None)
    # cplob_additem(topsf->archive_names_, 0, NULL);
    topsf.user_addr = None
    topsf.use_recursive_ = 0
    topsf.reverse_links_ = 0
    topsf.debug_link_ = 0
    topsf.single_fileset_ = 0
    topsf.info_only_ = 0
    topsf.smart_path_ = 0
    topsf.form_ = 0
    topsf.control_directory = strob_open(10)
    topsf.taru = tc.taru_create()
    topsf.mtime = time.time()
    topsf.debpsf = None
    topsf.rpmpsf = None
    if strcmp(filename, "-") == 0:
        topsf.pkgfilename = None
    else:
        topsf.pkgfilename = filename
    topsf.checkdigestname = None
    topsf.owner = None
    topsf.group = None
    topsf.exclude_list = None
    topsf.rpmtag_default_prefix = None
    topsf.verbose = 0
    topsf.usebuf1 = strob_open(40)
    return topsf_i_package_open(topsf, oflags)

def topsf_i_bail(topsf):
    swacfl_close(topsf.swacfl_)
    uinfile_close(topsf.format_desc_)
    uxfio_close(topsf.fd_)
    del topsf

def topsf_close(topsf):
    if uinfile_get_type(topsf.format_desc_) == uic.PLAIN_TARBALL_SRC_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.SLACK_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.DEB_FILEFORMAT:
        debpsf_close(topsf.debpsf)
    sys.stderr.write("")
    swacfl_close(topsf.swacfl_)
    sys.stderr.write("")
    topsf.topsf_i_package_close()
    sys.stderr.write("")
    uinfile_close(topsf.format_desc_)
    sys.stderr.write("")
    uxfio_close(topsf.fd_)
    sys.stderr.write("")
    if topsf.prefix_:
        del topsf.prefix_
    sys.stderr.write("")
    del topsf

def topsf_i_package_open(topsf, oflags):
    tagval = strob_open(12)
    sys.stderr.write("FUNCTION ENTER")
    if uinfile_get_type(topsf.format_desc_) == uic.RPMRHS_FILEFORMAT:

        # topsf.rpmfd_ = rpmfd_open(None, topsf.fd_)
        # rc = rpmReadPackageHeader(topsf_rpmFD(topsf), (topsf.h_), isSource, None, None)
        ts = rpm.TransactionSet()
        topsf.rpmfd_ = os.open(topsf.fd_, os.O_RDONLY)
        rc = ts.hdrFromFdno(topsf.rpmfd_)

        if rc != 0 or not topsf.h_:
            sys.stderr.write("lxpsf: error reading package in rpmReadPackageHeader().\n")
            topsf.topsf_i_bail()
            sys.stderr.write("ABNORMAL FUNCTION EXIT")
            strob_close(tagval)
            return None

        # if RPMPSF_RPM_HAS_PAYLOADCOMPRESSOR = 1:
        tagret = rpmpsf_get_rpmtagvalue(topsf.h_, rpm.RPMTAG_PAYLOADCOMPRESSOR, 0, None, tagval)

        if tagret != 0:
            strob_strcpy(tagval, "gzip")
        uncompressor = strob_str(tagval)

        tagret = rpmpsf_get_rpmtagvalue(topsf.h_, rte.RPMTAG_DEFAULTPREFIX, 0, None, tagval)
        if tagret == 0:
            sys.stderr.write("%s: Warning: RPM file uses deprecated RPMTAG_DEFAULTPREFIX(1056): %s\n" +
                             swlib_utilname_get() + strob_str(tagval))
            topsf.rpmtag_default_prefix = strob_str(tagval)

        if (oflags & tsc.TOPSF_OPEN_NO_AUDIT) == 0:
            if topsf.topsf_rpm_do_audit_hard_links(topsf, topsf.fd_, oflags, uncompressor) != 0:
                sys.stderr.write("ABNORMAL FUNCTION EXIT")
                strob_close(tagval)
                return None
    elif uinfile_get_type(topsf.format_desc_) == uic.PLAIN_TARBALL_SRC_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.SLACK_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.DEB_FILEFORMAT:
        d = debpsf_create()
        ret = debpsf_open(d, topsf)
        if ret < 0:
            sys.stderr.write("error decoding deb format, retval=%d\n" + ret)
        topsf.debpsf = d
    else:
        sys.stderr.write("lxpsf: unrecognized package format.\n")
        topsf.topsf_i_bail()
        sys.stderr.write("ABNORMAL FUNCTION EXIT")
        strob_close(tagval)
        return None
    sys.stderr.write("FUNCTION EXIT")
    strob_close(tagval)
    return topsf

def topsf_i_package_close(topsf):
    sys.stderr.write("FUNCTION ENTER")
    if uinfile_get_type(topsf.format_desc_) == uic.RPMRHS_FILEFORMAT:
        headerFree(topsf.h_)
    elif uinfile_get_type(topsf.format_desc_) == uic.DEB_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.PLAIN_TARBALL_SRC_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.SLACK_FILEFORMAT:
        pass
    else:
        sys.stderr.write("unrecognized format in topsf_close().\n")
    sys.stderr.write("FUNCTION EXIT")


def topsf_set_fd(topsf, fd):
    topsf.fd_ = fd

def topsf_get_format_desc(topsf):
    return topsf.format_desc_

def topsf_get_rpmheader(topsf):
    return topsf.h_

def topsf_get_archive_filelist(topsf):
    return topsf.swacfl_

def topsf_add_fl_entry(topsf, to_name, from_name, source_code):
    sys.stderr.write("FUNCTION ENTER")
    #
    # from_name is the name in the converted archive.
    # to_name is the name the original rpm archive.
    #
    # sys.stderr.write("to_name=[%s], from_name=[%s]\n", to_name, from_name);
    sys.stderr.write("BEGINNING: to_name=[%s] from_name=[%s]" % to_name % from_name)
    if topsf.cwd_prefix_:
        if not strstr(from_name, topsf.cwd_prefix_):
            sys.stderr.write("exception")
        else:
            # int i = strlen(topsf->cwd_prefix_);
            from_name += len(topsf.cwd_prefix_)
            # if (*from_name == '/') from_name++;
            from_name -= 1  # backup one char to get off the '/'
            # Now back up over the name-version-release

            # FIXME
            # the name-version-release is not present in all psf-forms
            # supported by the lxpsf program
            #
            #
            # while (*from_name != '/' && i) {
            # from_name--; i--;
            # sys.stderr.write("FROM NAME+ [%s]\n", from_name)
            # }
            # from_name++;
            # move forward off the '/'
    sys.stderr.write("ADDDING ENTRY: to_name=[%s] from_name=[%s]" % to_name % from_name)
    swacfl_add(topsf.swacfl_, to_name, from_name, source_code)
    sys.stderr.write("FUNCTION EXIT")


def topsf_get_cwd_prefix(topsf):
    return topsf.cwd_prefix_

def topsf_get_psf_prefix(topsf):
    return topsf.prefix_

def topsf_set_cwd_prefix(topsf, p):
    topsf.cwd_prefix_ = swlib_strdup(p)

def topsf_set_psf_prefix(topsf, p):
    topsf.prefix_ = swlib_strdup(p)


def topsf_rpm_audit_hard_links(topsf, fd):
    tmp = STROB()
    file_hdr = ahsstaticcreatefilehdr()
    format_ = uinfile_get_type(topsf.format_desc_)
    while True:
        if taru_read_header(topsf.taru, file_hdr, fd, format_, None, 0) < 0:
            sys.stderr.write("header error %s\n" % ahsstaticgettarfilename(file_hdr))
            return -1

        if ahsstaticgettarfilename(file_hdr) == "TRAILER!!!":
            break

        strar_add(topsf.archive_names, ahsstaticgettarfilename(file_hdr))

        if (file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFREG:
            if taru_write_archive_member_data(topsf.taru, file_hdr, -1, fd, None, format_, -1, None) < 0:
                sys.stderr.write("data error %s\n" % ahsstaticgettarfilename(file_hdr))
                return -2
            taru_tape_skip_padding(fd, file_hdr.c_filesize, format_)

        if file_hdr.c_nlink > 1 and (((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFREG) or
                                     ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFBLK) or
                                     ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFCHR) or
                                     ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFSOCK)):
            if topsf.debug_link_:
                sys.stderr.write("Found First : name = %s linkname=[%s]\n" %
                                 (ahsstaticgettarfilename(file_hdr), ahsstaticgettarlinkname(file_hdr)))
            topsf.add_link_pair(ahsstaticgettarfilename(file_hdr), ahsstaticgettarfilename(file_hdr))
            strob_strcpy(tmp, ahsstaticgettarfilename(file_hdr))
            topsf.topsf_rpm_find_linkname(strob_str(tmp), fd, file_hdr, format_)
        elif file_hdr.c_nlink > 1:
            pass

    ahsstaticdeletefilehdr(file_hdr)
    strob_close(tmp)
    return 0

def topsf_rpmFD(topsf):
    return rpmfd_getfd(topsf.rpmfd_)

def print_links(topsf):
    i = 0
    node_names = topsf.hl_node_names_
    linkto_names = topsf.hl_linkto_names_

    while name := cplob_val(node_names, i):
        linkname = cplob_val(linkto_names, i)
        sys.stderr.write("PRINT: name = [%s], linkname = [%s]\n" + name + linkname if linkname != '\0' else "<nil>")
        i += 1
    return 0

def topsf_rpm_find_linkname(topsf, base_link, fd, base_file_hdr, format_):
    lastmatch = None
    file_hdr1 = ahsstaticcreatefilehdr()
    oldoffset = uxfio_lseek(fd, 0, os.SEEK_CUR)
    sys.stderr.write("FUNCTION ENTER")
    while True:
        if taru_read_header(topsf.taru, file_hdr1, fd, format_, None, 0) < 0:
            sys.stderr.write("header error %s\n" + ahsstaticgettarfilename(file_hdr1))
            exit(1)
        if (file_hdr1.c_mode & ft.CP_IFMT) == ft.CP_IFREG:
            if taru_write_archive_member_data(topsf.taru, file_hdr1, -1, fd, None, format_, -1, None) < 0:
                sys.stderr.write("data error %s\n" + ahsstaticgettarfilename(file_hdr1))
                exit(1)
            taru_tape_skip_padding(fd, file_hdr1.c_filesize, format_)
        if file_hdr1.c_ino == base_file_hdr.c_ino:
            if lastmatch:
                del lastmatch
            if topsf.debug_link_:
                sys.stderr.write("*Found linked files %s %s\n" + base_link +
                                 ahsstaticgettarfilename(file_hdr1))
            topsf.add_link_pair(base_link, ahsstaticgettarfilename(file_hdr1))
            lastmatch = swlib_strdup(ahsstaticgettarfilename(file_hdr1))
        if not strcmp(ahsstaticgettarfilename(file_hdr1), "TRAILER!!!"):
            break
    ahsstaticsetpaxlinkname(base_file_hdr, lastmatch)
    ahsstaticdeletefilehdr(file_hdr1)
    sys.stderr.write("FUNCTION EXIT")
    if uxfio_lseek(fd, oldoffset, os.SEEK_SET) != oldoffset:
        sys.stderr.write("error seeking back in topsf_rpm_find_linkname\n")
        exit(2)
    return 0


def topsf_copypass_swacfl_write_missing_files(topsf, xux, swvarfs, ofd):
    missing_array = topsf.file_status_array.str()
    tm = time.time()
    h = topsf.h_
    fname = strob_open(32)
    strb = strob_open(32)
    linkname = strob_open(32)
    ret = 0

    print("START")
    for headerindex, c in enumerate(missing_array):
        if c == 'TOPSF_FILE_STATUS_IN_HEADER' and topsf.rpm_construct_missing_files == 0:
            rpmpsf_get_rpm_filenames_tagvalue(h, headerindex, None, fname)
            print(f"{swlib_utilname_get()}: RPM file entry missing from archive: {fname.str()}")
        elif c == 'TOPSF_FILE_STATUS_IN_HEADER' and topsf.rpm_construct_missing_files:
            print(f"found missing at index={headerindex}")
            rpmpsf_get_rpm_filenames_tagvalue(h, headerindex, None, fname)
            print(f"missing filename={fname.str()}")
            ret += rpmpsf_get_rpmtagvalue(h, 'RPMTAG_FILEMODES', headerindex, None, strb)
            mode = int(strb.str())

            if os.path.isfile(mode) or os.path.isdir(mode) or os.path.islink(mode):
                ahs = xux.ahs_object()
                ahs.init_header()
                if os.path.islink(mode):
                    fnameindex = rpmpsf_get_index_by_name(topsf, fname.str())
                    if fnameindex >= 0:
                        hret = rpmpsf_get_rpmtagvalue(h, 'RPMTAG_FILELINKTOS', fnameindex, None, linkname)
                        if hret:
                            ret = -1
                            continue
                        xux.set_linkname(linkname.str())
                        xux.set_mode('C_ISLNK|0777')
                        if topsf.verbose > 1:
                            print(
                                f"{swlib_utilname_get()}: setting perms 0777 for symlink in archive: {fname.str()}")
                    else:
                        ret = -1
                        continue
                else:
                    xux.set_mode(mode)
                swlib_squash_leading_slash(fname.str())
                xux.set_name(fname.str())
                ret += rpmpsf_get_rpmtagvalue(h, 'RPMTAG_FILEUSERNAME', headerindex, None, strb)
                xux.set_username(strb.str())
                xux.set_uid(99)
                xux.set_user_systempair(strb.str())
                ret += rpmpsf_get_rpmtagvalue(h, 'RPMTAG_FILEGROUPNAME', headerindex, None, strb)
                xux.set_groupname(strb.str())
                xux.set_gid(99)
                xux.set_group_systempair(strb.str())
                xux.set_mtime(tm)
                xux.set_filesize(0)
                nullfd = os.open("/dev/null", os.O_RDWR)
                ret = xux.write_by_fd(nullfd, ahs.vfile_hdr())
                os.close(nullfd)
                if ret < 0:
                    print(f"{swlib_utilname_get()}: error constructing missing file: {fname.str()}")
                else:
                    if topsf.verbose > 1:
                        print(f"{swlib_utilname_get()}: "
                              f"file constructed for RPM Header entry with missing file: {fname.str()}")
                print(f"writing missing file: xformat_write_by_fd returned [{ret}]")
            else:
                print(f"{swlib_utilname_get()}: not creating missing file (filetype not supported): {fname.str()}")
        elif c == 'TOPSF_FILE_STATUS_IN_ARCHIVE':
            pass
        else:
            print("fatal error in topsf_copypass_swacfl_write_missing_files")
            exit(2)

    linkname.close()
    fname.close()
    strb.close()
    return 0

def topsf_copypass_swacfl_header_list(topsf, xxformat, swvarfs, ofd):
    done = 0
    en_index = 0
    #    pid = os.fork()
    file_hdr = ahsstaticcreatefilehdr()
    swacfl = topsf.swacfl_

    while not done and (en := swacfl.entry_array_[en_index]):
        savep = swlib_strdup(strob_str(en.archiveName))
        p = savep
        if p[0] == '/':
            p = p[1:]
        if en.source_code_ == swacflc.SWACFL_SRCCODE_ARCHIVE_STREAM:
            done = 1
        elif en.source_code_ == swacflc.SWACFL_SRCCODE_RPMHEADER or en.source_code_ == swacflc.SWACFL_SRCCODE_PSF:
            sys.stderr.write("in SWACFL_SRCCODE_PSF")
            file_hdr.c_magic = tc.CPIO_NEWASCII_MAGIC
            file_hdr.c_ino = 1
            file_hdr.c_mode = 0o0644
            file_hdr.c_mode |= ft.CP_IFREG
            file_hdr.c_uid = os.getuid()
            file_hdr.c_gid = os.getgid()
            file_hdr.c_nlink = 1
            ahsstaticsetpaxlinkname(file_hdr, None)
            file_hdr.c_mtime = topsf.mtime
            file_hdr.c_filesize = 0
            file_hdr.c_namesize = len(en.from_name_) + 1
            file_hdr.c_chksum = 0
            ahsstaticsettarfilename(file_hdr, en.from_name_)
            ahsstaticsetpaxlinkname(file_hdr, None)
            if en.source_code_ == swacflc.SWACFL_SRCCODE_RPMHEADER:
                sys.stderr.write("in SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_RPMHEADER")
                topsf.user_addr = p
                file_hdr.c_filesize = rpmpsf_get_rpmtag_length(topsf, p) + 1
                do_write_file = rpmpsf_write_out_tag
                sys.stderr.write("leaving SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_RPMHEADER")
            elif en.source_code_ == swacflc.SWACFL_SRCCODE_PSF:
                sys.stderr.write("in SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_PSF")
                file_hdr.c_filesize = rpmpsf_get_psf_size(topsf)
                do_write_file = rpmpsf_write_beautify_psf
                sys.stderr.write("leaving SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_PSF")
            else:
                sys.stderr.write("fatal")
                exit(1)
            pheader = os.pipe()
            pid = os.fork()
            if pid < 0:
                sys.stderr.write("fatal, fork error")
                sys.exit(1)
            elif pid == 0:
                os.close(pheader[0])
                do_write_file(topsf, pheader[1])
                if en.source_code_ == swacflc.SWACFL_SRCCODE_RPMHEADER:
                    uxfio_unix_safe_write(pheader[1], "\n", 1)
                sys.exit(0)
            os.close(pheader[1])
            xformat_write_by_fd(xxformat, pheader[0], file_hdr)
            sys.stderr.write("OK %s %s" % (p, en.from_name_))
            sys.stderr.write("leaving SWACFL_SRCCODE_PSF")
        elif en.source_code_ == swacflc.SWACFL_SRCCODE_FILESYSTEM:
            sys.stderr.write("bad case error")
            break
    ahsstaticdeletefilehdr(file_hdr)
    sys.stderr.write("FUNCTION EXIT")
    return 0

def topsf_copypass_swacfl_archive_list(topsf, xxformat, swvarfs, ofd):
    savep = None

    file_hdr = ahsstaticcreatefilehdr()

    swacfl = topsf.swacfl_
    st = os.stat_result([])
    do_write_file = do_write_fileDelegate()
    archive_name_index = 0
    archive_name = ""
    tmp = STROB()
    tmp.str_ = ""
    tmp.tok_ = ""
    tmp.length_ = 0
    tmp.extra_ = 0
    tmp.reserve_ = 0
    tmp.in_use_ = ""
    tmp.fill_char = 0

    while archive_name == topsf.archive_namesM.names[archive_name_index]:
        print("archive_name=[%s]" % (archive_name if archive_name else "<NULL>"))
        if topsf.rpmtag_default_prefix:
            tmp.str_ = topsf.rpmtag_default_prefix
            swlib_add_trailing_slash(tmp)
            tmp.str_ += archive_name
            print("will FIND ENTY prefix: tmp=[%s]" % tmp.str_)
            en = swacfl_find_entry(swacfl, tmp.str_)
        else:
            print("will FIND ENTY no prefix: archive_name=[%s]" % archive_name)
            en = swacfl_find_entry(swacfl, archive_name)
        if not en:
            print("lxpsf: Internal fatal error, swacfl entry not found for [%s]" % archive_name)
            exit(1)
        if savep:
            del savep
        if topsf.rpmtag_default_prefix:
            savep = swlib_strdup(archive_name)
        else:
            savep = swlib_strdup(en.archiveName.str_)
        print("WILL OPEN: savep = [%s]" % savep)
        print("en->from_name_ = [%s]" % en.from_name_)
        p = savep
        swlib_process_hex_escapes(p)
        swlib_unexpand_escapes(None, p)
        if p[0] == '/':
            p = p[1:]
        if en.source_code_ == swacflc.SWACFL_SRCCODE_ARCHIVE_STREAM:
            print("in SWACFL_SRCCODE_ARCHIVE_STREAM")
            print("SWACFL_SRCCODE_ARCHIVE_STREAM #1a")
            u_fd = varfs.swvarfs_u_open(swvarfs, p)
            print("SWACFL_SRCCODE_ARCHIVE_STREAM #1b")
            if u_fd < 0:
                print("SWACFL_SRCCODE_ARCHIVE_STREAM #1c")
                print("swvarfs_u_open error: file %s not found" % p)
                break
            print("SWACFL_SRCCODE_ARCHIVE_STREAM #1")
            varfs.swvarfs_u_fstat(swvarfs, u_fd, st)
            print("SWACFL_SRCCODE_ARCHIVE_STREAM #2")
            if topsf.rpmtag_default_prefix:
                taru_statbuf2filehdr(file_hdr, st, None, en.from_name_,
                                     swvarfs.swvarfs_u_get_linkname(swvarfs, u_fd))
            else:
                taru_statbuf2filehdr(file_hdr, st, None, p, varfs.swvarfs_u_get_linkname(swvarfs, u_fd))
            print("SWACFL_SRCCODE_ARCHIVE_STREAM #3")
            file_hdr.c_uid = os.getuid()
            file_hdr.c_gid = os.getgid()
            xformat_write_by_fd(xxformat, u_fd, file_hdr)
            print("OK %s %s" % (p, en.from_name_))
            ahsstaticsettarfilename(file_hdr, None)
            ahsstaticsetpaxlinkname(file_hdr, None)
            varfs.swvarfs_u_close(swvarfs, u_fd)
            if topsf.topsf_check_header_list(topsf.header_names, archive_name):
                print("%s: topsf_check_header_list() indicates not found: [%s]" % (
                    swlib_utilname_get(), archive_name))
            print("leaving SWACFL_SRCCODE_ARCHIVE_STREAM")
        elif en.source_code_ == swacflc.SWACFL_SRCCODE_RPMHEADER or en.source_code_ == swacflc.SWACFL_SRCCODE_PSF:
            print("in SWACFL_SRCCODE_PSF")
            file_hdr.c_magic = tc.CPIO_NEWASCII_MAGIC
            file_hdr.c_ino = 1
            file_hdr.c_mode = 0o0644
            file_hdr.c_mode |= ft.CP_IFREG
            file_hdr.c_uid = os.getuid()
            file_hdr.c_gid = os.getgid()
            file_hdr.c_nlink = 1
            ahsstaticsetpaxlinkname(file_hdr, None)
            file_hdr.c_mtime = topsf.mtime
            file_hdr.c_filesize = 0
            file_hdr.c_namesize = len(en.from_name_) + 1
            file_hdr.c_chksum = 0
            ahsstaticsettarfilename(file_hdr, en.from_name_)
            ahsstaticsetpaxlinkname(file_hdr, None)
            if en.source_code_ == swacflc.SWACFL_SRCCODE_RPMHEADER:
                print("in SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_RPMHEADER")
                topsf.user_addr = p
                file_hdr.c_filesize = rpmpsf_get_rpmtag_length(topsf, p)
                do_write_file = rpmpsf_write_out_tag
                print("leaving SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_RPMHEADER")
            elif en.source_code_ == swacflc.SWACFL_SRCCODE_PSF:
                print("in SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_PSF")
                file_hdr.c_filesize = rpmpsf_get_psf_size(topsf)
                do_write_file = rpmpsf_write_beautify_psf
                print("leaving SWACFL_SRCCODE_PSF: SWACFL_SRCCODE_PSF")
            pheader = os.pipe()
            pid = swfork(None)
            if pid < 0:
                print("fatal, fork error")
                exit(1)
            elif pid == 0:
                os.close(pheader[0])
                do_write_file(topsf, pheader[1])
                sys.exit(0)
            os.close(pheader[1])
            xformat_write_by_fd(xxformat, pheader[0], file_hdr)
            print("OK %s %s" % (p, en.from_name_))
            print("leaving SWACFL_SRCCODE_PSF")
        elif en.source_code_ == swacflc.SWACFL_SRCCODE_FILESYSTEM:
            print("bad case error")
            exit(1)
    print("status_array [%s]" % topsf.file_status_array.str_)
    if savep:
        del savep
    ahsstaticdeletefilehdr(file_hdr)
    print("FUNCTION EXIT")
    return 0

def topsf_copypass_swacfl_list(topsf, ofd):
    xxformat = xformat_open(-1, ofd, int(af.arf_newascii))
    swvarfs = varfs.swvarfs_opendup(topsf.fd_,
                                    uic.UINFILE_DETECT_FORCEUXFIOFD | uic.UINFILE_UXFIO_BUFTYPE_DYNAMIC_MEM,
                                    None)
    if swvarfs is None:
        return -1
    sys.stderr.write("running topsf_copypass_swacfl_header_list")
    ret = topsf.topsf_copypass_swacfl_header_list(xxformat, swvarfs, ofd)
    if ret != 0:
        sys.stderr.write("topsf_copypass_swacfl_list: error writing header files\n")
        return ret
    sys.stderr.write("running topsf_copypass_swacfl_archive_list")
    ret = topsf.topsf_copypass_swacfl_archive_list(xxformat, swvarfs, ofd)

    #
    # Now write the files that are in the header but not in the cpio archive
    #

    if topsf.rpmtag_default_prefix is not None:
        # with these rare packages that use this deprecated tag, do
        # not construct missing file
        #
        pass
    else:
        topsf.topsf_copypass_swacfl_write_missing_files(xxformat, swvarfs, ofd)

    varfs.swvarfs_close(swvarfs)
    xformat_write_trailer(xxformat)
    xformat_close(xxformat)
    return ret

def topsf_rpm_do_audit_hard_links(topsf, fd, oflags, decomp):
    buf = bytearray(6)
    opipe = [0, 0]
    cmd = [SHCMD()] * 100

    sys.stderr.write("FUNCTION ENTER")

    buffertype = uinfile_decode_buftype(oflags, uxfio.UXFIO_BUFTYPE_DYNAMIC_MEM)

    cmd[0] = shcmd_open()
    shcmd.shcmd_add_arg(cmd[0], decomp)
    shcmd.shcmd_add_arg(cmd[0], "-d")
    shcmd.shcmd_set_exec_function(cmd[0], "execvp")
    # cmd[1] = shcmd_open()

    uinformat = topsf.topsf_get_format_desc()
    os.pipe()

    pid = swfork(None)

    if not pid:
        os.close(opipe[0])
        shcmd.shcmd_set_srcfd(cmd[0], fd)
        shcmd.shcmd_set_dstfd(cmd[0], opipe[1])
        swlib_exec_filter(cmd, -1, None)
        uxfio_close(fd)
        sys.exit(0)

    os.close(opipe[1])
    sys.stderr.write("just before uxfio_opendup")
    uxfio_fd = uxfio_opendup(opipe[0], buffertype)
    sys.stderr.write("just after uxfio_opendup")

    if uxfio_sfread(uxfio_fd, buf, 6) != 6:
        return -1

    if not buf[:6] != "070701":
        uinfile_set_type(uinformat, uic.CPIO_NEWC_FILEFORMAT)
    elif not buf[:6] != "070702":
        uinfile_set_type(uinformat, uic.CPIO_CRC_FILEFORMAT)
    else:
        sys.stderr.write("error")
        return -1

    if uxfio_lseek(uxfio_fd, (-6), os.SEEK_CUR) < 0:
        sys.stderr.write("uxfio_lseek error")
        return -1

    if topsf.topsf_rpm_audit_hard_links(topsf, uxfio_fd):
        sys.stderr.write("error in topsf_audit_hard_links, exiting...")
        sys.exit(1)

    if uxfio_lseek(uxfio_fd, 0, os.SEEK_SET) < 0:
        sys.stderr.write("error from uxfio_lseek")
        return -1
    sys.stderr.write("closing fd=[%d]" % fd)
    uxfio_close(fd)
    sys.stderr.write("setting topsf fd to [%d]" % uxfio_fd)
    topsf.set_fd(uxfio_fd)
    uinfile_set_type(uinformat, uic.RPMRHS_FILEFORMAT)
    if topsf.debug_link:
        topsf.print_links()
    return 0

def add_link_pair(topsf, base_link, link):
    if topsf.find_link(link) != 0:
        return
    # sys.stderr.write("ADD:  Adding [%s] ==  [%s]\n", base_link, link);
    if topsf.debug_link_:
        sys.stderr.write("ADD:  Adding [%s] ==  [%s]\n" + base_link + link)
    cplob_additem(topsf.hl_node_names_, cplob_get_nused(topsf.hl_node_names_) - 1,
                  base_link)
    cplob_additem(topsf.hl_node_names_, cplob_get_nused(topsf.hl_node_names_), None)
    cplob_additem(topsf.hl_linkto_names_, cplob_get_nused(topsf.hl_linkto_names_) - 1,
                  link)
    cplob_additem(topsf.hl_linkto_names_, cplob_get_nused(topsf.hl_linkto_names_), None)

def slack_write_psf_buf2(topsf, psf):
    st = stat
    slack_desc = None
    doinst = None
    name_fields = []
    desc = STROB()
    title = STROB()
    vola = STROB()
    if topsf.pkgfilename is None:
        print("%s: must specify a slackware package as a regular file arg" % swlib_utilname_get())
        return -1
    ret = topsf_parse_slack_pkg_name(topsf.pkgfilenameM, name_fields)
    if ret < 0:
        return -2
    # psf = ""
    psf += swc.SW_A_distribution + "\n"
    psf += swc.SW_A_layout_version + " 1.0\n"
    psf += "dfiles dfiles\n"
    psf += "pfiles pfiles\n"
    if topsf.owner:
        psf += swc.SW_A_owner + " %s\n" % topsf.ownerM
    if topsf.groupM:
        psf += swc.SW_A_group + " %s\n" % topsf.groupM
    psf += "\n"
    psf += "vendor\n"
    psf += "the_term_vendor_is_misleading True\n"
    psf += "tag \"%s\"\n" % name_fields[3]
    psf += "title The Slackware Linux Project\n"
    psf += "url \"http:\n"
    xformat = xformat_open(-1, -1, "arf_ustar")
    ret = uxfio_lseek(topsf.fd_, 0, os.SEEK_SET)
    if ret < 0:
        return -2
    ret = xformat_open_archive_by_fd(xformat, topsf.fd_,
                                     uic.UINFILE_DETECT_OTARFORCE | uic.UINFILE_DETECT_DEB_CONTROL,
                                     0)
    if ret < 0:
        return -3
    # ret = xformat_read_header(xformat)
    if xformat_is_end_of_archive(xformat):
        ret = -5
        print("%s: empty data archive [%d]" % (swlib_utilname_get(), ret))
        return ret
    if xformat_get_tar_typeflag(xformat) != tar.DIRTYPE:
        ret = -6
        print("%s: first file not a directory  [%d]" % (swlib_utilname_get(), ret))
        return ret
    ahs = xformat_ahs_object(xformat)
    name = ahsstaticgettarfilename(ahs.file_hdr)
    if name != "./":
        ret = -7
        print("%s: first file not ./\n" % swlib_utilname_get())
        return ret
    xtmp = (swc.SW_A_file + " -t d -o %s -g %s -m %o /\n" %
            (ahsstaticgettarusername(ahs.file_hdr), ahsstaticgettargroupname(ahs.file_hdr),
             ahs_get_perms(ahs)))
    name = xformat_get_next_dirent(xformat, st)
    while name:
        if name.startswith("./install/") or name.startswith("install/"):
            if name.startswith("./install/"):
                name = name[2:]
            print("NAME=%s" % name)
            if name == "install/doinst.sh":
                topsf.topsf_h_write_to_buf(xformat, name, doinst)
            elif name == "install/":
                pass
            elif name == "install/slack-desc":
                topsf.topsf_h_write_to_buf(xformat, name, slack_desc)
            else:
                topsf.topsf_h_write_to_buf(xformat, name, None)
                print("%s: unrecognized skackware control file: %s" % (swlib_utilname_get(), name))
        elif name.endswith(".new") and len(name) == 5:
            vola = "file\n"
            vola += "source %s\n" % name
            vola += "path /%s\n" % name
            vola += "is_volatile true\n"
        name = xformat_get_next_dirent(xformat, st)
    if slack_desc:
        parse_slack_desc(desc, title, strob_str(slack_desc))
        if "\n" in strob_str(desc):
            strob_str(desc).replace("\n", "")
        if "\r" in strob_str(desc):
            strob_str(desc).replace("\r", "")
        if "\n" in strob_str(title):
            strob_str(title).replace("\n", "")
        if "\r" in strob_str(title):
            strob_str(title).replace("\r", "")
    print("title: [%s]" % strob_str(title))
    print("description: [%s]" % strob_str(desc))
    print("")
    swlib_squash_illegal_tag_chars(strar_get(name_fields, 0))
    psf += "\n"
    psf += swc.SW_A_product + "\n"
    psf += swc.SW_A_tag + " %s\n" % strar_get(name_fields, 0)
    psf += swc.SW_A_vendor_tag + " %s\n" % strar_get(name_fields, 3)
    psf += swc.SW_A_title + " \"%s\"\n" % strob_str(title)
    psf += swc.SW_A_description + " \"%s\"\n" % strob_str(desc)
    psf += swc.SW_A_revision + " %s\n" % strar_get(name_fields, 1)
    psf += swc.SW_A_control_directory + " \"\"\n"
    if doinst:
        psf += swc.SW_A_postinstall + " install/doinst.sh\n"
    psf += "\n"
    psf += swc.SW_A_fileset + "\n"
    psf += swc.SW_A_tag + " bin\n"
    psf += swc.SW_A_control_directory + " \"\"\n"
    psf += swc.SW_A_directory + " . /\n"
    psf += xtmp
    psf += swc.SW_A_file + " *\n"
    psf += swc.SW_A_exclude + " PSF\n"
    topsf.topsf_add_excludes(topsf, psf)
    psf += "#" + swc.SW_A_exclude + " control\n"
    psf += "#" + swc.SW_A_exclude + " control/*\n"
    psf += "%s\n" % strob_str(vola)
    return 0


def topsf_get_fd(topsf):
    return topsf.fd_

def topsf_set_mtime(topsf, tm):
    topsf.mtime = tm


def topsf_get_fd_fd(topsf):
    sys.stderr.write("FUNCTION ENTER")
    if uinfile_get_type(topsf.format_desc_) == uic.RPMRHS_FILEFORMAT:
        ret = rpmfd_get_fd_fd(topsf.rpmfd_)
        sys.stderr.write("FUNCTION EXIT")
        return ret
    else:
        sys.stderr.write("FUNCTION EXIT")
        return topsf.fd_

def topsf_make_package_prefix(topsf, cwdir):
    sys.stderr.write("FUNCTION ENTER")
    if uinfile_get_type(topsf.format_desc_) == uic.RPMRHS_FILEFORMAT:
        ret = rpmpsf_make_package_prefix(topsf, cwdir)
        sys.stderr.write("FUNCTION EXIT")
        return ret
    elif uinfile_get_type(topsf.format_desc_) == uic.PLAIN_TARBALL_SRC_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.SLACK_FILEFORMAT:
        pass
    elif uinfile_get_type(topsf.format_desc_) == uic.DEB_FILEFORMAT:
        ret = ""
        return ret
    else:
        sys.stderr.write("unrecognized package type.\n")
    sys.stderr.write("FUNCTION EXIT")
    return str(None)

def topsf_write_psf(topsf, fd_out, do_indent):
    sys.stderr.write("FUNCTION ENTER")
    if uinfile_get_type(topsf.format_desc_) == uic.RPMRHS_FILEFORMAT:
        sys.stderr.write("")
        if do_indent != 0:
            ret = rpmpsf_write_beautify_psf(topsf, fd_out)
            sys.stderr.write("FUNCTION EXIT")
            return ret
        else:
            ret = rpmpsf_write_psf(topsf, fd_out)
            sys.stderr.write("FUNCTION EXIT")
            return ret
    elif uinfile_get_type(topsf.format_desc_) == uic.DEB_FILEFORMAT:
        sys.stderr.write("")
        ret = debpsf_write_psf(topsf, fd_out)
        sys.stderr.write("")
        return ret
    elif uinfile_get_type(topsf.format_desc_) == uic.SLACK_FILEFORMAT:
        sys.stderr.write("")
        ret = topsf.slack_write_psf(fd_out)
        return ret
    elif uinfile_get_type(topsf.format_desc_) == uic.PLAIN_TARBALL_SRC_FILEFORMAT:
        ret = topsf.plain_source_tarball_psf(fd_out)
        return ret
    else:
        sys.stderr.write("")
        sys.stderr.write("unrecognized package type.\n")
    sys.stderr.write("FUNCTION EXIT")
    return -1

def topsf_search_list(topsf, list, archive_name):
    i = 0
    while i < len(list):
        s1 = list[i]
        # E_DEBUG3 is a placeholder for the debug function used in the original code
        print(f"comparing [{s1}] [{archive_name}]")
        if (archive_name == "/" and s1 == "./") or (archive_name == "./" and s1 == "/"):
            print(f"found special case [{s1}] [{archive_name}]")
            return i
        else:
            # swlib_dir_compare is a placeholder for the comparison function used in the original code
            # ret = swlib_dir_compare(s1, archive_name, SWC_FC_NOAB)
            ret = os.path.commonpath([s1, archive_name]) == s1
            if ret:
                return i
        i += 1
    return -1

def topsf_check_header_list(topsf, _list, archive_name):
    # Find archive_name in _list
    # and check the topsf->file_status_array
    #

    if topsf.rpmtag_default_prefix:
        #
        # Must prepend the prefix to archive_name
        #
        sys.stderr.write("archive name [%s]" % archive_name)
        strob_strcpy(topsf.usebuf1, topsf.rpmtag_default_prefix)
        swlib_unix_dircat(topsf.usebuf1, archive_name)
        sys.stderr.write("HAS prefix, searching for [%s]" % strob_str(topsf.usebuf1))
        ret = topsf.topsf_search_list(_list, strob_str(topsf.usebuf1))
    else:
        sys.stderr.write("searching for [%s]" % archive_name)
        ret = topsf.topsf_search_list(_list, archive_name)
    if ret >= 0:
        strob_chr_index(topsf.file_status_array, ret, tsc.TOPSF_FILE_STATUS_IN_ARCHIVE)
        return 0
    else:
        return 1

def topsf_write_info(topsf, fd_out, do_indent):
    sys.stderr.write("FUNCTION ENTER")
    if uinfile_get_type(topsf.format_desc_) == uic.RPMRHS_FILEFORMAT:
        if do_indent != 0:
            ret = rpmpsf_write_beautify_info(topsf, fd_out)
            sys.stderr.write("FUNCTION EXIT")
            return ret
        else:
            ret = rpmpsf_write_info(topsf, fd_out)
            sys.stderr.write("FUNCTION EXIT")
            return ret
    else:
        sys.stderr.write("unrecognized package type.\n")
    sys.stderr.write("FUNCTION EXIT")
    return -1


def topsf_h_write_to_buf(xformat, name, buf):
    if buf:
        buf[0] = strob_open(32)

    ufd = xformat_u_open_file(xformat, name)
    if ufd < 0:
        return -1
    if buf:
        ret = swlib_ascii_text_fd_to_buf(buf[0], ufd)
    else:
        # throw this file away
        tmp = strob_open(32)
        #        ret = swlib_ascii_text_fd_to_buf(tmp, ufd)
        strob_close(tmp)
        ret = 0
    xformat_u_close_file(xformat, ufd)
    return ret





class do_write_fileDelegate:
    def invoke(self, o_fd):
        pass
