# sdcommon.py
# Basic sd utils.
#
# Copyright (C) 2003,2004,2005,2006,2007  James H. Lowe, Jr.
# Copyright (C) 2023 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the


import sys
import os
import errno
import signal
import re
import string
import stat
import pty
import time
import pysigset

from sd.debug import E_DEBUG, E_DEBUG2, E_DEBUG3
from sd.defaults import setdefaults
from sd.swprogs.swproglib import swpl_bashin_detect, swpl_bashin_posixsh, swpl_bashin_testsh
from sd.swsuplib.atomicio import atomicio
from sd.swsuplib.vsnprintf import snprintf
from sd.swsuplib.misc.cstrings import strcmp, strncmp, strrchr, strdup
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swevents import swevent_is_error, swevents_get_struct_by_message, swevent_get_events_array, swevent_write_rpsh_event
from sd.swsuplib.misc.swlib import (swlibc, swlib_squash_all_dot_slash, swlib_squash_double_slash,
                                    swlib_unix_dircat, swlib_squash_trailing_slash, swlib_get_umask,
                                    swlib_doif_writef, swlib_is_sh_tainted_string, swlib_test_verbose,
                                    swlib_squash_trailing_char, swlib_tee_to_file, swlib_is_option_true)
from sd.swsuplib.misc.swfork import  swlib_fork, swfc, swndfork
from sd.swsuplib.versionid import swverid_open
from sd.swsuplib.misc.swutillib import swutil_writelogline, swutil_set_stderr_fd
from sd.swsuplib.cplob import vplob_add
from sd.swsuplib.strob import (STROB, strob_sprintf, strob_close, strob_open, strob_strtok, strob_strcpy, strob_str,
                               strob_strcat, strob_strlen, STROB_DO_APPEND, strob_charcat)
from sd.swsuplib.misc.strar import STRAR, strar_add, strar_get, strar_close, strar_qsort, strar_copy_construct, strar_qsort_neg_strcmp
from sd.swsuplib.misc.shcmd import (shcmd_add_arg, shcmd_get_argvector, shcmd_debug_show_command, shcmd_cmdvec_exec,
                                    shcmd_cmdvec_wait, shcmd_get_exitval, shcmd_unix_execve)
from sd.swsuplib.misc.swssh import  swssh_landing_command, swssh_protect_shell_metacharacters, swssh_assemble_ssh_cmd
from sd.swsuplib.misc.swgp import swgp_close_all_fd, swgp_read_line, swgp_signal, DO_NOT_APPEND, swgpReadFdNonblock, \
    swgp_signal_block
from sd.swsuplib.misc.swextopt import get_opta

class SwCommonConstants(object):
    SWC_SHELL_KEY_POSIX = "posix"
    SWC_SHELL_KEY_DETECT = "detect"
    DEFAULT_PAX_W = "pax"
    DEFAULT_PAX_R = "pax"
    DEFAULT_PAX_REM = "tar"
    MAX_CONTROL_MESG_LEN = 2000
    SWINSTALL_VOLATILE_SUFFIX = ".swbisnew"
    SWREMOVE_VOLATILE_SUFFIX = ".swbisold"
    PAX_READ_COMMANDS_LEN = 10
    PAX_WRITE_COMMANDS_LEN = 10
    SW_UTILNAME = "swutility"
    REPORT_BUGS = "<bug-swbis@gnu.org>"
    SWC_PID_ARRAY_LEN = 30
    SWC_TARGET_FD_ARRAY_LEN = 10
    SWC_SCRIPT_SLEEP_DELAY = 0 # Seconds to sleep a the end of the script
    CMD_TAINTED_CHARS = "'|\"*?;&<>`$"
    SW_FAILED_ALL_TARGETS = 1
    SW_FAILED_SOME_TARGETS = 2

    SWBIS_TARGET_CTL_MSG_125 = "125 target script started"
    SWBIS_TARGET_CTL_MSG_128 = "128 catalog path determined"
    SWBIS_TARGET_CTL_MSG_129 = "129 current directory"
    SWBIS_TARGET_CTL_MSG_508 = "508 target script error"
    SWBIS_SOURCE_CTL_ARCHIVE = "130 source is from archive"
    SWBIS_SOURCE_CTL_DIRECTORY = "140 source is from directory"
    SWBIS_SOURCE_CTL_CLEANSH = "150 source is from cleansh"
    SWBIS_TS_LOAD_SIGNATURES = "Load signatures"
    SWBIS_TS_LOAD_FILESET = "load fileset"
    SWBIS_TS_LOAD_CATALOG = "Load catalog and attributes"
    SWBIS_TS_LOAD_INSTALLED = "Load INSTALLED file"
    SWBIS_TS_LOAD_SESSION_OPTIONS = "load session options"
    SWBIS_TS_GET_CATALOG_SELECTIONS = "Get catalog selections"
    SWBIS_TS_ABORT = "Programmed Abort" # Do nothing unsucessfully
    SWBIS_TS_DO_NOTHING = "Do nothing" # Do nothing sucessfully
    SWBIS_TS_UTS = "get utsnames"
    SWBIS_TS_GET_ISCS_LISTING = "Get catalog selections listing"
    SWBIS_TS_GET_ISCS_ENTRY = "Get catalog entry"
    SWBIS_TS_LOAD_INDEX_FILE_90 = "Load INDEX file loc90"
    SWBIS_TS_CATALOG_DIR_REMOVE = "Catalog directory removal"
    SWBIS_TS_CATALOG_UNPACK = "unpack catalog.tar"
    SWBIS_TS_ANALYSIS_002 = "Run analysis phase 002"
    SWBIS_TS_ANALYSIS_001 = "Run analysis phase 001"
    SWBIS_TS_MAKE_CATALOG_DIR = "Make catalog entry directory"
    SWBIS_TS_PREVIEW_TASK = "Preview Task"
    SWBIS_TS_MAKE_LIVE_INSTALLED = "Make INSTALLED status live"
    SWBIS_TS_MAKE_LOCKED_SESSION = "Lock Session"
    SWBIS_TS_CHECK_LOOP = "Check Loop"
    SWBIS_TS_CONTINUE = "Programmed Continue"
    SWBIS_TS_REPORT_STATUS = "Report Status"
    SWBIS_TS_REMOVE_CATALOG_ENTRY = "Remove catalog entry"
    SWBIS_TS_RESTORE_CATALOG_ENTRY = "Restore catalog entry"
    SWBIS_TS_REMOVE_FILES = "Remove files"
    SWBIS_TS_RETRIEVE_FILES_ARCHIVE = "Retrieve files archive"
    SWBIS_TS_POST_VERIFY = "Run post Verify"
    SWBIS_TS_GET_CATALOG_PERMS = "Get catalog permissions"
    SWBIS_TS_RUN_CONFIGURE = "Run configure script"
    SWBIS_TS_LOAD_MANAGEMENT_HOST_STATUS = "Load manangement host status"
    SWBIS_TS_CHECK_OVERWRITE = "Check overwrites"
    SWBIS_TS_CHECK_STATUS = "Check Status"
    SWBIS_TS_GET_PRELINK_FILELIST = "Get prelink file list" # swverify

swcc = SwCommonConstants()

global sw_defaults

"""
def GFP_GET(K):
    return gb_fparams_get(G, K)

def GFP_ADD(K, V):
    return gb_fparams_add(G, K, V)
"""
def G_is_in_control_script(G):
    return G.e_in_control_script != 0

def G_set_is_in_control_script(G, a):
    G.e_in_control_script = a
class PaxWriteCommand:
    def __init__(self, id, command):
        self.id = id
        self.command = command

class PaxReadCommand:
    def __init__(self, id, command, verbose_command, keep_command):
        self.id = id
        self.command = command
        self.verbose_command = verbose_command
        self.keep_command = keep_command

class PaxRemoveCommand:
    def __init__(self, id, command, verbose_command):
        self.id = id
        self.command = command
        self.verbose_command = verbose_command

class ErrorCode:
    def __init__(self, code):
        self.code = code
g_pax_write_commands = [
    {
        "command": "pax",
        "description": "pax -x ustar -w"
    },
    {
        "command": "star",
        "description": "star cbf 1 - -H ustar"
    },
    {
        "command": "tar",
        "description": "tar cbf 1 -"
    },
    {
        "command": "gtar",
        "description": "gtar cbf 1 - --format=pax"
    },
    {
        "command": "swbistar",
        "description": "/usr/bin/swtar"
    },
    {
        "command": "detect",
        "description": "false"
    },
    {
        "command": None,
        "description": None
    }
]

g_pax_read_commands = [
    {
        "command": "pax",
        "description1": "pax -pe -r",
        "description2": "pax -pe -r -v",
        "description3": "pax -pe -r -k"
    },
    {
        "command": "star",
        "description1": "star xpf - -U",
        "description2": "star xpvf - -U 1>&2",
        "description3": "star xpf - --keep-old-files 1>&2"
    },
    {
        "command": "tar",
        "description1": "tar xpf -",
        "description2": "tar xpvf - 1>&2",
        "description3": "tar xpkf - 1>&2"
    },
    {
        "command": "gtar",
        "description1": "gtar xpf - --overwrite",
        "description2": "gtar xpvf - --overwrite 1>&2",
        "description3": "gtar xpkf - 1>&2"
    },
    {
        "command": None,
        "description1": None,
        "description2": None,
        "description3": None
    }
]

g_pax_remove_commands = [
    {
        "command": "pax",
        "description1": None,
        "description2": None
    },
    {
        "command": "star",
        "description1": None,
        "description2": None
    },
    {
        "command": "tar",
        "description1": "tar zcf - --no-recursion --remove-files  --files-from=-",
        "description2": "tar zvcf - --no-recursion --remove-files --files-from=- 1>&2"
    },
    {
        "command": "gtar",
        "description1": "gtar zcf - --no-recursion --remove-files --files-from=-",
        "description2": "gtar zvcf - --no-recursion --remove-files --files-from=- 1>&2"
    },
    {
        "command": None,
        "description1": None,
        "description2": None
    }
]
global g_logger_sigterm

def try_nibble(fd, buf, timeout):
    return os.read(fd, 1)


def logger_sig_handler(signum):
    global g_logger_sigterm
    if signum in [signal.SIGTERM, signal.SIGUSR2, signal.SIGABRT]:
        g_logger_sigterm = signum


def detect_special_verbose_status(G, sptr):
    if "SW_CONTROL_SCRIPT_BEGINS" in sptr:
        G_set_is_in_control_script(G, 1)
    elif "SW_CONTROL_SCRIPT_ENDS" in sptr:
        G_set_is_in_control_script(G, 0)


def is_ssh_sys_error(sptr):
    if (
            "DOING SOMETHING NASTY!" in sptr or
            "man-in-the-middle attack!" in sptr or
            "Offending key in" in sptr or
            "Offending key for" in sptr or
            "Connection refused" in sptr or
            " Name or service not known" in sptr
    ):
        return 1
    else:
        return 0


def is_unix_sys_error(sptr):
    return (
            "bnormal termination" in sptr or
            "bnormal Termination" in sptr or
            "SIGABRT" in sptr or
            "SIGINT" in sptr or
            "ermission denied" in sptr or
            "ot permitted" in sptr or
            " input/output" in sptr or
            " Input/Output" in sptr or
            " i/o" in sptr or
            " I/O" in sptr or
            "i/o " in sptr or
            "I/O " in sptr or
            " error" in sptr or
            " Error" in sptr or
            "error " in sptr or
            "Error " in sptr or
            "roken pipe" in sptr or
            "ead-only" in sptr or
            "ead only" in sptr or
            "o such file" in sptr or
            "o such device" in sptr or
            "o space" in sptr or
            "ccess denied" in sptr or
            "out of memory" in sptr or
            "ot enough space" in sptr or
            "format error" in sptr or
            "Bad " in sptr or
            "bad file" in sptr or
            "Too many " in sptr or
            "too many " in sptr or
            "oo large " in sptr or
            "nvalid arg" in sptr or
            "Unexpected EOF in archive" in sptr or
            "nexpected eof in archive" in sptr or
            "NUL blocks at start" in sptr or
            0
    )


def writeline(do_stderr, do_log, logfd, efd, msg_prefix, p_sptr, length):
    sptr = p_sptr
    ret = 0
    eret = 0
    lret = 0

    E_DEBUG("")
    E_DEBUG3("do_log=%d  logfd=%d", do_log, logfd)
    E_DEBUG3("do_stderr=%d  efd=%d", do_stderr, efd)

    if msg_prefix and len(msg_prefix) > 0 and msg_prefix not in sptr:
        tmp = strob_open(40)
        strob_sprintf(tmp, 0, "%s%s", msg_prefix, p_sptr)
        sptr = strob_str(tmp)
        length = len(sptr)

    else:
        tmp = None
        sptr = p_sptr

    if do_stderr != 0:
        eret = atomicio('write', efd, sptr, length)

        if do_log and logfd > 0:
            lret = swutil_writelogline(logfd, sptr)
    ret = eret
    ret = lret if lret < 0 else ret
    E_DEBUG2("return = %d", ret)
    if tmp:
        strob_close(tmp)
    return ret


def is_event(line, status, is_swbis_event, is_swi_eventp, is_posix_eventp):
    status = 0
    if ": SW_" in line or ": SWI_" in line or ": SWBIS_" in line:
        if swevent_is_error(line, status) and status == 0:
            status = -1
        if ": SWBIS_" in line:
            is_swbis_event = 1
        if ": SWI_" in line:
            is_swi_eventp = 1
        if ": SW_" in line:
            is_posix_eventp = 1
        return 1
    else:
        return 0


def write_log(G, linebuf, logspec, vofd, vefd, bufa, len, verbose_level, swi_event_fd):
    s = strob_str(linebuf)
    nl = re.search("\n", s)
    tot = 0
    linebuf += bufa
    while nl:
        s1 = s[nl.start() + 1]
        s = s[:nl.start() + 1]
        ret = write_error_line(G, logspec, vofd, vefd, linebuf, len(s), verbose_level, swi_event_fd)
        if ret < 0:
            return -1
        if ret > 0:
            tot += ret
        s += s1
        s = s[nl.start() + 1:]
        nl = re.search("\n", s)
    return tot


#
# Need to figure out shcmd_cmdvec_exec and shcmd_cmdvec_wait functions
#
def do_run_cmd(cmd, verbose_level):
    ret = 0
    cmdvec = [cmd, None]
    if cmd is None:
        return 0
    print("sending SIGTERM to remote script.", file=sys.stderr)
    if verbose_level >= swlibc.SWC_VERBOSE_6:
        shcmd_debug_show_command(cmdvec[0], sys.stderr)
    shcmd_cmdvec_exec(cmdvec)
    shcmd_cmdvec_wait(cmdvec)
    ret = shcmd_get_exitval(cmdvec[0])
    if ret != 0:
        print(f"{swlib_utilname_get()}: Error: client script shutdown failed.", file=sys.stderr)
        print(f"{swlib_utilname_get()}: Stranded processes may be left on target and intermediate hosts.",
              file=sys.stderr)
        print(f"{swlib_utilname_get()}: The failed command was: ", file=sys.stderr)
        shcmd_debug_show_command(cmd, sys.stderr)
    print("remote kill returned", ret, file=sys.stderr)
    return ret


def swc_checkbasename(s):
    # We can do better
    #
    # Make sure the basename of the pathname
    # is a non-zero length filename (i.e. has no '/')
    #

    if len(s) > 1:
        if (
                not s or
                s[0] == '\0' or
                s[0] == '/' or
                '/' in s or
                s[len(s) - 1] == '/' or
                0
        ):
            return 1
    return 0


# @staticmethod
def write_error_line(G, logspec, vofd, vefd, linebuf, len, verbose_level, swi_event_fd):
    ret = 0
    event_status = 0
    is_swi_event = 0
    is_posix_event = 0
    is_swbis_event = 0
    do_log = 0
    ev = None

    E_DEBUG("")
    E_DEBUG2("verbose_level=%d", verbose_level)
    E_DEBUG3("vefd=%d  vofd=%d", vefd, vofd)

    sptr = strob_str(linebuf)
    detect_special_verbose_status(G, sptr)
    E_DEBUG("")

    if is_event(sptr, event_status, is_swbis_event, is_swi_event, is_posix_event):
        E_DEBUG("")
        if swi_event_fd >= 0:
            #
            # swi_event_fd is the pipe to the swi_<*> routine
            # which is gated by these events in an expect/send
            # type of arrangement.
            #
            E_DEBUG("")
            news = sptr
            E_DEBUG2("sptr=[%s]", news)
            ev = swevents_get_struct_by_message(sptr, swevent_get_events_array())
            # if (ev=swevents_get_struct_by_message(sptr, swevent_get_events_array())) == None:
            if ev is None:
                sys.stderr.write("internal error: EVENT NOT Found for [%s]\n" % sptr)
            else:
                pass

            #
            # The next function is very important, it reads the human readable
            # messages and forms machine readable ascii text consisting only of
            # CODE:STATUS (e.g. 262:0) separated by newlines. This form is written
            # into the event_fd and read by the main process in the G->g_swi_event_fd
            #  descriptor.
            #

            evret = swevent_write_rpsh_event(swi_event_fd, news, len)

            E_DEBUG("")
            if evret < 0:
                sys.stderr.write("error writing event pipe in write_error_line: %d\n" % evret)

        if is_swi_event == 0:
            E_DEBUG("")
            do_log = 1
        else:
            E_DEBUG("")
            do_log = 0

        do_stderr = swlib_test_verbose(ev, verbose_level, is_swbis_event, is_swi_event, event_status, is_posix_event)
        if do_log != 0 or do_stderr != 0:
            E_DEBUG("")
            if event_status != 0:
                #
                # the status indicates an error ocurred.
                #
                E_DEBUG("calling writeline")
                ret = writeline(do_stderr, do_log, logspec.logfd, vefd, "", sptr, len)
            else:
                E_DEBUG("")
                if vofd >= 0:
                    E_DEBUG("calling writeline")
                    ret = writeline(do_stderr, do_log, logspec.logfd, vofd, "", sptr, len)
        else:
            E_DEBUG("nothing")
            pass
    else:
        if logspec.loglevelM > 0:
            E_DEBUG("")
            do_log = 1
        if verbose_level > 0:
            is_ssh = 0
            E_DEBUG("")
            # if is_unix_sys_error(sptr) or (is_ssh:=is_ssh_sys_error(sptr)) or verbose_level >= swlibc.SWC_VERBOSE_6:
            if is_unix_sys_error(sptr) or is_ssh_sys_error(sptr) or verbose_level >= swlibc.SWC_VERBOSE_6:
                #
                #                                * Error, send to stderr.
                #
                E_DEBUG("calling writeline")

                do_stderr = 1
                if is_ssh != 0:
                    ret = writeline(do_stderr, do_log, logspec.logfd, vefd, "ssh: ", sptr, len)
                else:
                    ret = writeline(do_stderr, do_log, logspec.logfd, vefd, "", sptr, len)
            else:
                #
                #                                * Info, send to stdout.
                #
                E_DEBUG("")
                if logspec.loglevelM >= 2 or G_is_in_control_script(G) or False:
                    do_log = 1
                else:
                    do_log = 0
                if verbose_level >= G.g_verbose_threshold or G_is_in_control_script(G) or False:
                    do_stderr = 1
                else:
                    do_stderr = 0
                E_DEBUG("calling writeline")
                ret = writeline(do_stderr, do_log, logspec.logfd, vofd, "", sptr, len)
    return ret


def write_log(G, linebuf, logspec, vofd, vefd, bufa, len, verbose_level, swi_event_fd):
    s = ""
    s1 = ""
    nl = ""
    ret = 0
    tot = 0
    linebuf += bufa[:len]
    s = linebuf
    nl = s.find("\n")
    while nl != -1:
        s1 = s[nl + 1]
        s = s[:nl + 1]
        ret = write_error_line(G, logspec, vofd, vefd, linebuf, len(s), verbose_level, swi_event_fd)
        if ret < 0:
            return -1
        if ret > 0:
            tot += ret
        s = s[:nl + 1] + s1 + s[nl + 2:]
        s = s[nl + 1:]
        nl = s.find("\n")
    return tot


def do_run_cmd(cmd, verbose_level):
    ret = 0
    cmdvec = [cmd, None]
    if cmd is None:
        return 0
    print("sending SIGTERM to remote script.", file=sys.stderr)
    if verbose_level >= swlibc.SWC_VERBOSE_6:
        shcmd_debug_show_command(cmdvec[0], sys.stderr)
    shcmd_cmdvec_exec(cmdvec)
    shcmd_cmdvec_wait(cmdvec)
    ret = shcmd_get_exitval(cmdvec[0])
    if ret != 0:
        print(f"{swlib_utilname_get()}: Error: client script shutdown failed.", file=sys.stderr)
        print(f"{swlib_utilname_get()}: Stranded processes may be left on target and intermediate hosts.",
              file=sys.stderr)
        print(f"{swlib_utilname_get()}: The failed command was: ", file=sys.stderr)
        shcmd_debug_show_command(cmd, sys.stderr)
    print(f"remote kill returned {ret}", file=sys.stderr)
    return ret


def process_targetfile_line(tmp, current_arg):
    end = None
    line = None
    tmp.set_length(tmp.length() + 2)
    line = tmp.str()
    if len(line) == 0:
        current_arg[0] = line
        return 0
    end = line.find("\n\r#")
    if end != -1:
        line = line[:end]
        end -= 1
    else:
        end = len(line) - 1
    #
    # Squash whitespace at the end of the line.
    #
    while end > 0 and line[end].isspace():
        line = line[:end]
        end -= 1
    #
    # Squash whitespace at the beginning of the line.
    #
    while line[0].isspace():
        line = line[1:]
    #
    # Add a leading '@'
    #
    # for sd-lx we use -T option vs @ for simlicity
    #
    if len(line) > 0 and line[0] != '@':
        line = '@' + line
    current_arg[0] = line
    return len(line)


@staticmethod
def get_dir_component(ret, news, nf):
    tmp = None
    s = '\0'
    i = 0

    tmp = strob_open(20)
    i = 0
    s = strob_strtok(tmp, news, "/")
    while s != '\0' and i < nf:
        s = strob_strtok(tmp, None, "/")
        i += 1
    if s != '\0':
        if strob_strtok(tmp, None, "/") is None:
            strob_strcpy(ret, "")
        else:
            strob_strcpy(ret, s)
    else:
        strob_strcpy(ret, "")
    strob_close(tmp)


def get_dir_component(ret, news, nf):
    tmp = []
    s = ""
    i = 0
    tmp = ["" for _ in range(20)]
    i = 0
    s = news.split("/")
    while s and i < nf:
        s = news.split("/", 1)[1:]
        i += 1
    if s:
        if news.split("/", 1)[1:] == []:
            ret = ""
        else:
            ret = s
    else:
        ret = ""


@staticmethod
def determine_longest_common_dir(p_sourcepath_list):
    s = '\0'
    dirname = '\0'
    is_same = 0
    nf = 0
    n = 0
    nrel = 0
    news = '\0'
    tmp = None
    ret = None
    current_dir = '\0'
    sourcepath_list = None

    E_DEBUG("")
    if p_sourcepath_list is None:
        return None

    sourcepath_list = strar_copy_construct(p_sourcepath_list)
    if sourcepath_list is None:
        return None

    ret = strob_open(24)
    tmp = strob_open(24)
    dirname = None

    E_DEBUG("")
    #         Check that the path are all relative or all
    #           absolute
    n = 0
    nrel = 0
    # while (s:=strar_get(sourcepath_list, n)):
    while strar_get(sourcepath_list, n):
        news = s
        swlib_squash_all_dot_slash(news)
        swlib_squash_double_slash(news)
        swlib_squash_trailing_slash(news)
        if news[0] != '/':
            nrel += 1
        n += 1
    E_DEBUG("")

    if nrel != 0 and nrel != (n - 1):
        # commonality not supported
        # mixed relative and absolute paths
        strob_close(ret)
        strar_close(sourcepath_list)
        return None

    # sort the list, this algorithm requires it
    strar_qsort(sourcepath_list, strar_qsort_neg_strcmp)

    if nrel == 0:
        strob_strcpy(ret, "/")

    E_DEBUG("")
    current_dir = None
    nf = 0
    is_same = 1
    while is_same != 0:
        E_DEBUG("")
        nf += 1
        n = 0
        # while (s:=strar_get(sourcepath_list, n)):
        while strar_get(sourcepath_list, n):
            E_DEBUG2("s=[%s]", s)
            news = s
            swlib_squash_all_dot_slash(news)
            swlib_squash_trailing_slash(news)
            swlib_squash_double_slash(news)
            get_dir_component(tmp, news, nf - 1)
            E_DEBUG2("component is [%s]", strob_str(tmp))
            if current_dir is None:
                if strob_strlen(tmp) != 0:
                    current_dir = strob_str(tmp)
                else:
                    is_same = 0
                E_DEBUG2("setting current_dir=[%s]", current_dir)
            else:
                E_DEBUG2("current_dir is already [%s]", current_dir)
                if strcmp(strob_str(tmp), current_dir) != 0:
                    #                                         a component is different, return the
                    #                                           last component
                    is_same = 0
                    E_DEBUG3("found difference [%s] [%s]", current_dir, strob_str(tmp))
                    current_dir = None
                    break
                else:
                    pass
            n += 1
        E_DEBUG("out of while")
        if current_dir != '\0':
            E_DEBUG2("dir catting [%s]", current_dir)
            swlib_unix_dircat(ret, current_dir)
            current_dir = None
        if s is not None:
            E_DEBUG("s == NULL")
            break
    E_DEBUG("")
    strob_close(tmp)
    E_DEBUG2("returning [%s]", strob_str(ret))
    strar_close(sourcepath_list)
    return ret


def swc_make_multiplepathlist(sourcepath_listM, rootdir, firstpath):
    s = ""
    b = ""
    dir2 = ""
    base = ""
    n = 0

    lpath = determine_longest_common_dir(sourcepath_listM)
    if lpath is None:
        E_DEBUG("return NULL")
        return None

    # Report the longest common directory to the caller
    if rootdir:
        rootdir = str(lpath)
        E_DEBUG2("rootdir is [%s]", rootdir)

    base = str(lpath)
    E_DEBUG2("base is [%s]", base)

    #         Now make a list of paths that are relative to the
    #           longest common directory

    strb = ""
    tmp = ""
    n = 0

    # while (s:=sourcepath_listM[n]):
    while sourcepath_listM[n]:
        E_DEBUG2("s=[%s]", s)
        swlib_squash_trailing_slash(s)
        b = s.find(base)
        if b == None or b != s:
            E_DEBUG("return NULL")
            return None
        else:
            dir2 = s[len(base):]
            if base != "/":
                if dir2[0] != "/":
                    E_DEBUG("return NULL")
                    return None
                dir2 = dir2[1:]
            else:
                pass
            E_DEBUG2("dir2=[%s]", dir2)
            strb = "\"%s\" " % ("." if len(dir2) == 0 else dir2)
            if firstpath:
                firstpath = dir2
        E_DEBUG("")
        n += 1

    E_DEBUG2("list is [%s]", strob_str(strb))
    return strb


def swc_print_umask(buf, len):
    ret = 0
    mode = swlib_get_umask()
    ret = snprintf(buf, len - 1, "%03o", mode)
    buf[ret] = '\0'
    return buf


def createErrorCode():
    ec = ErrorCode
    ec.code = 0
    return ec


def swc_print_umask(buf, length):
    mode = swlib_get_umask()
    ret = snprintf(buf, length - 1, "%03o", mode)
    buf[ret] = '\0'
    return buf

def createErrorCode():
    ec = ErrorCode
    ec.code = 0
    return ec


def destroyErrorCode(EC):
    del EC



def swc_setErrorCode(EC, code, msg):
    EC.contents.codeM = code


def swc_getErrorCode(EC):
    return EC.contents.codeM


def swc_copyright_info(fp):
    fp.write(
        "Copyright (C) 2004,2005,2006,2007,2008,2009,2010 Jim Lowe\n"
        "Copyright (C) 2023 Paul Weber - Translation to Python\n"
        "Portions are copyright 1985-2000 Free Software Foundation, Inc.\n"
        "This software is distributed under the terms of the GNU General Public License\n"
        "and comes with NO WARRANTY to the extent permitted by law.\n"
        "See the file named COPYING for details.\n"
    )


def swc_parse_soc_spec(arg, selections, target):
    p = '\0'
    target[0] = None
    selections[0] = None

    if not arg:
        selections[0] = None
        target[0] = None
        return 0

    if strncmp(arg, "@:", 2) == 0:
        #
        # Implementation Extention:
        # Support relative path
        # using '':path'' syntax
        #
        if len(arg) > 2:
            target[0] = arg[1:]
        else:
            #
            # colon not allowed by itself, per spec.
            #
            return 1
        return 0

    if arg[0] != '@':
        #
        # this happens for ''selections @ targets'' syntax
        #
        selections[0] = arg
        # if (p := strrchr(selections[0], ord('@'))) is not None:
        if strrchr(selections[0], ord('@')) is not None:
            p[0] = '\0'
            target[0] = p[1:]
    elif arg[0] == '@':
        target[0] = arg[1:]
    else:
        pass  # dead branch

    if target[0] is not None and target[0] == ':':
        target[0] = target[0][1:]
    return 0


def swc_construct_newkill_vector(kmd, nhops, tramp_list, script_pid, verbose_level):
    #
    # Need shcmd_get_argvector()
    #
    i = 0
    if not script_pid or len(script_pid) == 0:
        return -1
    swlib_doif_writef(verbose_level, 7, 0, pty.STDERR_FILENO, "In swc_construct_newkill_vector: looping thru argvec\n")
    argvec = shcmd_get_argvector(kmd)
    while argvec:
        if argvec[0] == swlibc.SWC_KILL_PID_MARK:
            clientmsg = strar_get(tramp_list, i)
            if not clientmsg:
                return -1
            if len(clientmsg) == 0:
                return -2
            pid = clientmsg.rsplit(':', 1)[-1]
            if not pid:
                return -3
            if len(pid) == 0:
                return -4
            pid = pid[1:]
            s = pid
            while s:
                if not s.isdigit():
                    return -5
                s += 1
            argvec[0] = strdup(pid)
        argvec += 1
    #
    # Sanity Check
    #
    if i != nhops - 1:
        return -9
    clientmsg = strar_get(tramp_list, i)
    if clientmsg:
        #
        # Sanity Check
        #
        if not clientmsg.startswith(swcc.SWBIS_TARGET_CTL_MSG_125):
            swlib_doif_writef(verbose_level, -1, 0, pty.STDERR_FILENO,
                              "construct_newkill_vector: unexpected tramp in the list: %s]\n", strar_get(tramp_list, i))
            return -12
    else:
        #
        # Sanity Check
        #
        return -14
    argvec = shcmd_get_argvector(kmd)
    while argvec:
        if not argvec[1]:
            shcmd_add_arg(kmd, "kill")
            shcmd_add_arg(kmd, script_pid)
            break
        argvec += 1
    return 0


def swc_form_command_args(G, context, targetpath, target_cmdlist, opt_preview, nhops, opt_no_getconf, landing_shell):
    tmpcommand = strob_open(10)
    if strcmp(targetpath, "-") == 0:
        strar_add(target_cmdlist, "/bin/cat")
    else:
        # strob_strcpy(tmpcommand, SWSSH_POSIX_SHELL_COMMAND);
        strob_strcpy(tmpcommand, swssh_landing_command(landing_shell, opt_no_getconf))

        if G.g_do_cleanshM == 0:
            strob_sprintf(tmpcommand, STROB_DO_APPEND, " _swbis _%s", swlib_utilname_get())
        else:
            pass

        swssh_protect_shell_metacharacters(tmpcommand, nhops, swcc.CMD_TAINTED_CHARS)
        strar_add(target_cmdlist, strob_str(tmpcommand))
    strob_close(tmpcommand)
    return 0


def swc_make_absolute_path(target_current_dir, target_path):
    ret = '\0'
    tmp = None

    if target_path[0] == '/':
        return target_path

    tmp = strob_open(32)
    strob_strcpy(tmp, target_current_dir)
    strob_strcat(tmp, "/")
    strob_strcat(tmp, target_path)

    ret = strob_str(tmp)
    strob_close(tmp)
    return ret


def swc_validate_targetpath(nhops, targetpath, default_target, cwd, context):
    if nhops < 0:
        print(f"{swlib_utilname_get()}: {context} error", file=sys.stderr)
        return None

    if targetpath is None:
        # apply the default target
        targetpath = default_target

    E_DEBUG("")
    if nhops == 0 and targetpath[0] == ':':
        #
        #  This path support implem. extension
        #  relative path targets.
        #
        if targetpath[1] != '/' and targetpath[1] != '-':
            tmp = cwd + targetpath[1:]
            targetpath = tmp
        else:
            #
            # Skip over the leading ':'
            #
            targetpath = targetpath[1:]

    E_DEBUG("")
    if nhops == 0 and targetpath[:2] == "\\:":
        #
        # handle escaped colon ':'
        #
        targetpath = targetpath[1:]

    E_DEBUG("")
    if nhops > 0 and default_target == "-":
        #
        # This combination, stdout on a remote host makes
        # little sense.  Change the default target to "."
        # This will have the effect of supporting the
        # target syntax of a plain host with no path
        # spec.  e.g.    @host1
        #
        default_target = "."

    E_DEBUG("")
    if targetpath is None or len(targetpath) == 0:
        targetpath = default_target
        if nhops >= 1 and targetpath == "":
            #
            # If the target path is empty
            #
            targetpath = "/"

    E_DEBUG("")
    if swlib_is_sh_tainted_string(targetpath):
        E_DEBUG2("string tainted with shell meta-characters: %s", targetpath)
        swlib_doif_writef(1, -1, 0, pty.STDERR_FILENO, "%s directory name is tainted with shell meta-characters\n", context)
        return None

    E_DEBUG("")
    if targetpath != "-":
        if targetpath == ".":
            #
            # allow a "." as a targetpath.
            # This is an implementation extension.
            #
            pass
        elif nhops < 1 and targetpath[0] != '/':
            #
            # allow relative path for remote targets
            #
            swlib_doif_writef(1, -1, 0, pty.STDERR_FILENO, "target path [%s] not absolute.\n", targetpath)
            return None
    E_DEBUG("")
    return targetpath


def swc_do_preview_cmd(G, prefix, fver, targetpath, sshcmd, kmd, cl_target, cmdlist, nhops, do_first, opt_no_getconf,
                       landing_shell):
    ret = 0
    argvec = []
    args = None
    p = []

    if do_first != 0:
        ret = swc_form_command_args(G, "target", targetpath, cmdlist, 1, nhops, opt_no_getconf, landing_shell)
    if ret != 0:
        return -1
    # swssh_reset_module();
    ret = swssh_assemble_ssh_cmd(sshcmd, cmdlist, None, nhops)

    if ret != 0:
        return -2

    args = shcmd_get_argvector(sshcmd)
    p = args
    while p != '\0' and p[0] != '\0':
        fver.write("[%s]" % p)
        p += 1
    fver.write("\n")

    argvec = shcmd_get_argvector(kmd)
    while argvec:
        if argvec + 1 == None:
            shcmd_add_arg(kmd, "kill")
            shcmd_add_arg(kmd, "$PPID")
            break
        argvec += 1

    fver.write("%s", prefix)
    args = shcmd_get_argvector(kmd)
    p = args
    while p != '\0' and p[0] != '\0':
        fver.write("[%s]", p[0])
        p += 1
    fver.write("\n")

    return 0


def swc_initialize_logspec(logspec, logfile, log_level):
    fd = 0
    logspec.loglevelM = 0
    logspec.logfd = -1

    if log_level > 0:
        fd = swc_open_logfile(logfile)
        if fd >= 0:
            logspec.logfd = fd
            logspec.loglevelM = log_level
        else:
            print(f"{swlib_utilname_get()}: error opening logfile : {logfile}", file=sys.stderr)


def swc_open_logfile(logfile):
    ret = 0
    ret = os.open(logfile, os.O_WRONLY | os.O_CREAT | os.O_APPEND, 0o644)
    return ret


def swc_open_filename(sourcefilename, open_errorp):
    st = 0

    if strcmp(sourcefilename, "-") == 0:
        open_errorp = None
        return pty.STDIN_FILENO

    if os.stat(sourcefilename):
        open_errorp = 1
        return -1

    if stat.S_ISDIR(st.st_mode):
        open_errorp = None
        return -1

    if stat.S_ISDIR(st.st_mode):
        open_errorp = 0
        return -1
    if stat.S_ISCHR(st.st_mode) or stat.S_ISBLK(st.st_mode):
        flag = os.O_RDONLY
    else:
        flag = os.O_RDONLY
    try:
        fd = os.open(sourcefilename, flag)
    except OSError as e:
        print("open error : ({}) : {}".format(sourcefilename, os.strerror(e.errno)))
        open_errorp = 1
        return -1
    open_errorp = 0
    return fd


def swc_run_ssh_command(G, sshcmd, cmdlist, path, opt_preview, nhops, o_fdar, i_fdar, login_orig_termiosP, login_sizeP,
                        pump_pid, fork_type, make_master_raw, ignoremask, is_no_fork_ok, verbose_level,
                        source_file_size, opt_no_getconf, is_local_fd, landing_shell, error_fd, logspec):
    did_open = 0
    i_fdm = [0 for _ in range(3)]
    fdm = [0 for _ in range(3)]
    pump_pid[0] = 0

    E_DEBUG("")
    E_DEBUG2("path=[%s]", path)
    E_DEBUG2("nhops=[%d]", nhops)
    E_DEBUG2("is_no_fork_ok=[%d]", is_no_fork_ok)

    if source_file_size:
        source_file_size[0] = 0

    if is_local_fd:
        is_local_fd[0] = 0
    if is_no_fork_ok > 0 and nhops == 0 and path[0] == '/' and True:
        #
        # Optimization for Local source file.
        #
        st = os.stat(path)

        E_DEBUG("Local File")
        if not os.path.exists(path):
            #
            # Source Access error
            #
            sys.stderr.write(
                "SW_SOURCE_ACCESS_ERROR while accessing local file {}: {}\n".format(path, os.strerror(1)))
            return -2
        E_DEBUG("")
        if stat.S_ISREG(st.st_mode) and G.g_do_create == 0 and True:

            if source_file_size:
                source_file_size = int(st.st_size)
            fd = os.open(path, os.O_RDONLY, 0)
            if fd < 0:
                sys.stderr.write("open error {}: {}\n".format(path, os.strerror(fd.err)))

            if stat.S_ISREG(st.st_mode):
                if is_local_fd:
                    is_local_fd = 1
            i_fdm[0] = fd
            i_fdm[1] = os.open("/dev/null", os.O_RDWR, 0)
            i_fdm[2] = i_fdar[2]
            did_open = 1
            fork_type = swlibc.SWFORK_NO_FORK
            if fd < 0:
                return -1
            if verbose_level > swlibc.SWC_VERBOSE_8:
                sys.stderr.write("Local file opened : {}\n".format(path))

    E_DEBUG2("did_open=%d", did_open)
    if did_open == 0:
        i_fdm[0] = i_fdar[0]
        i_fdm[1] = i_fdar[1]
        i_fdm[2] = i_fdar[2]

        E_DEBUG("")
        ret = swc_form_command_args(G, "target", path, cmdlist, opt_preview, nhops, opt_no_getconf, landing_shell)
        swlibc.SWLIB_ASSERT(ret == 0)

        # if (opt_preview) swssh_reset_module();
        ret = swssh_assemble_ssh_cmd(sshcmd[0], cmdlist, STRAR(), nhops)

        swlibc.SWLIB_ASSERT(ret == 0)

    if nhops == 0:
        E_DEBUG("Set sigdelset(ignoremask, SIGINT);")
        signal.sigdelset(ignoremask, signal.SIGINT)

    ts_pid = swlib_fork(fork_type, fdm, i_fdm[1], i_fdm[0], i_fdm[2], login_orig_termiosP, login_sizeP, pump_pid,
                        make_master_raw, ignoremask)

    if strcmp(fork_type, swfc.SWFORK_NO_FORK) != 0:
        #
        # Fork happened.
        #
        E_DEBUG("Fork Happened")
        if ts_pid == 0:
            #
            # Execute the ssh pipeline.
            #
            if verbose_level >= swlibc.SWC_VERBOSE_4:
                swlib_doif_writef(verbose_level, swlibc.SWC_VERBOSE_4, 0, pty.STDERR_FILENO, "Executing : ")
                shcmd_debug_show_command(sshcmd[0], pty.STDERR_FILENO)
            # setup the fd closes
            swgp_close_all_fd(3)

            # Set the SIG_IGN in the specified signals
            if nhops >= 1:
                swc_set_sig_ign_by_mask(ignoremask)
            shcmd_unix_execve(sshcmd[0])
            sys.stderr.write("exec failed\n")
            sys.exit(1)

        if ts_pid < 0:
            sys.stderr.write("fork error : {}\n".format(os.strerror(ts_pid.err)))
            sys.exit(1)
    else:
        pass
        E_DEBUG("No Fork Happened")
        #
        #  No fork required.
        #

    o_fdar[0] = fdm[0]
    o_fdar[1] = fdm[1]
    o_fdar[2] = fdm[2]
    return ts_pid


def swc_analyze_status_array(pid, num, status, debug):
    ret = 0
    for i in range(num):
        if pid[i] < 0 and status[i]:
            if os.WIFEXITED(status[i]):
                x = os.WEXITSTATUS(status[i])
                if x:
                    ret += 1
                if debug >= swlibc.SWC_VERBOSE_IDB:
                    print("status[pid={}] exit val = {}".format(pid[i], x), file=sys.stderr)
            else:
                print("{}: process[pid={}] failed to exit normally".format(swlib_utilname_get(), pid[i]),
                      file=sys.stderr)
                ret += 1
    return ret


def swc_do_run_kill_cmd(killcmd, target_kmd, source_kmd, verbose_level):
    ret = 0
    ret = do_run_cmd(killcmd, verbose_level)
    ret = do_run_cmd(target_kmd, verbose_level)
    ret = do_run_cmd(source_kmd, verbose_level)
    return ret


def swc_checkbasename(s):
    #
    # Make sure the basename of the pathname
    # is a non-zero length filename (i.e. has no '/')
    #
    if len(s) > 1:
        if not s or s == '\0' or s == '/' or '/' in s or s[-1] == '/' or 0:
            return 1
    return 0


def swc_record_pid(pid, p_pid_array, p_pid_array_len, verbose_level):
    if p_pid_array_len < swlibc.SWC_PID_ARRAY_LEN and pid > 0:
        swlib_doif_writef(verbose_level, swlibc.SWC_VERBOSE_IDB, 0, sys.stderr,
                          "{} : record_pid [{}]".format(swlibc.SW_UTILNAME, pid))
        p_pid_array[p_pid_array_len] = pid
        p_pid_array[p_pid_array_len + swlibc.SWC_PID_ARRAY_LEN] = pid
        p_pid_array_len += 1


def swc_set_sig_ign_by_mask(ignoremask):
    if signal.SIGTERM in ignoremask:
        signal.signal(signal.SIGTERM, signal.SIG_IGN)
    if signal.SIGALRM in ignoremask:
        signal.signal(signal.SIGALRM, signal.SIG_IGN)
    if signal.SIGINT in ignoremask:
        signal.signal(signal.SIGINT, signal.SIG_IGN)


def swc_process_selection_files(G, swspec_list):
    retval = 0
    tmp = []
    for i in range(swlibc.SWC_TARGET_FD_ARRAY_LEN):
        fd = G.g_selectfd_array[i]
        if fd >= 0:
            while swgp_read_line(fd, tmp, DO_NOT_APPEND) > 0:
                s = tmp
                while swlib_squash_trailing_char(s, '\n') == 0:
                    pass
                if len(s) == 0:
                    continue
                swverid = swverid_open(None, s)
                if swverid is None:
                    sys.stderr.write(
                        swlib_utilname_get() + ": error processing software selection from file: [" + s + "]\n")
                    retval = -1
                    strob_close(tmp)
                    return retval
                else:
                    vplob_add(swspec_list, swverid)
            if fd > pty.STDERR_FILENO:
                os.close(fd)
            G.g_selectfd_array[i] = -1
        else:
            pass
    return retval


def swc_process_selection_args(swspec_list, argvector, uargc, poptind):
    count = 0
    is_last = 0
    error_count = 0
    if not swspec_list:
        return -1
    while poptind < uargc and is_last == 0:
        s = argvector[poptind]
        if s and s[0] == '@':
            break
        if len(s) > 0 and s[-1] == '@':
            is_last = 1
        poptind += 1
        count += 1
        swverid = swverid_open(None, s)
        if swverid is None:
            print("%s: error processing software selection: %s" % (swlib_utilname_get(), s), file=sys.stderr)
            error_count += 1
        else:
            vplob_add(swspec_list, swverid)
    return error_count


def swc_read_selections_file():
    return 0


def swc_get_next_target(argvector, uargc, poptind, targetsfd_array, default_target, pnum_remains):
    did_one = 0
    targetfd_index = 0
    current_arg = None
    pnum_remains[0] = 0
    tmp = strob_open(10)

    if poptind[0] == uargc and targetsfd_array[targetfd_index] < 0 and did_one == 0:
        #
        # No args, use the default.
        #
        did_one = 1
        pnum_remains[0] = 0
        strob_strcpy(tmp, "@")
        tmpch = default_target
        strob_strcat(tmp, tmpch)
        current_arg = strob_str(tmp)
        poptind[0] += 1
        strob_close(tmp)
        return current_arg
    did_one = 1

    if poptind[0] < uargc:
        #
        # Return the next arg.
        #
        current_arg = argvector[poptind[0] + 1]
        #       poptind[0] += 1
        pnum_remains[0] = uargc - poptind[0]
        if strcmp(current_arg, "@") == 0:
            #
            # Handle the case of  `` @<space>target_spec ''
            #
            current_arg = argvector[poptind[0] + 1]
            if not current_arg:
                sys.stderr.write("invalid target.\n")
                return None
            strob_strcpy(tmp, "@")
            strob_strcat(tmp, current_arg)
            current_arg = strdup(strob_str(tmp))
        elif current_arg[0] != '@':
            strob_strcpy(tmp, "@")
            strob_strcat(tmp, current_arg)
            current_arg = strdup(strob_str(tmp))
        else:
            current_arg = strdup(current_arg)
        strob_close(tmp)
        return current_arg

    fd = targetsfd_array[targetfd_index]
    while fd >= 0:
        #
        # read next line in the targets file.
        #
        pnum_remains[0] = 1  # assume more that one target
        n = swgp_read_line(fd, tmp, DO_NOT_APPEND)
        if n > 0:
            if process_targetfile_line(tmp, current_arg) >= 1:
                current_arg = strdup(current_arg)
                strob_close(tmp)
                return current_arg
            else:
                #
                # A comment or empty line.
                #
                pass
        elif n == 0:
            targetfd_index += 1
        else:
            targetfd_index += 1
        fd = targetsfd_array[targetfd_index]
    strob_close(tmp)
    return None


def swc_flush_logger(G, slinebuf, tlinebuf, efd, ofd, logspec, verboselevel):
    write_log(G, slinebuf, logspec, ofd, efd, "", 0, verboselevel, -1)
    write_log(G, tlinebuf, logspec, ofd, efd, "", 0, verboselevel, -1)
    if logspec and logspec.logfd > 0:
        os.close(logspec.logfd)


def swc_fork_logger(G, x_slinebuf, x_tlinebuf, efd, ofd, logspec, s_efd, t_efd, vlevel, swi_event_fd_p):
    pid = os.fork()
    if pid == 0:
        spipe = [0, 0]
        tpipe = [0, 0]
        ppipe = [0, 0]
        #       fork_defaultmask = signal.sigset_t()
        #       fork_blockmask = signal.sigset_t()
        fork_defaultmask = signal.SIG_SETMASK()
        fork_blockmask = signal.SIG_SETMASK()
        slinebuf = ""
        tlinebuf = ""

        if s_efd:
            spipe = os.pipe()
            if spipe[0] < 0:
                return -1
        else:
            spipe[0] = -1
            spipe[1] = -1

        tpipe = os.pipe()
        if tpipe[0] < 0:
            return -1

        if swi_event_fd_p:
            ppipe = os.pipe()
            if ppipe[0] < 0:
                return -1
        else:
            ppipe[0] = -1
            ppipe[1] = -1

        signal.sigemptyset(fork_blockmask)
        signal.sigaddset(fork_blockmask, signal.SIGINT)
        signal.sigaddset(fork_blockmask, signal.SIGTERM)
        signal.sigaddset(fork_blockmask, signal.SIGALRM)
        signal.sigemptyset(fork_defaultmask)
        signal.sigaddset(fork_defaultmask, signal.SIGINT)
        signal.sigaddset(fork_defaultmask, signal.SIGPIPE)
        signal.sigaddset(fork_defaultmask, signal.SIGTERM)

        pid = swndfork(fork_blockmask, fork_defaultmask)

        if pid == 0:
            status = 0
            rra = 0
            sra = 0
            done_s = 0
            rrb = 0
            srb = 0
            done_t = 0
            bufa = ""
            bufb = ""
            tmp = ""

            swgp_signal(signal.SIGABRT, logger_sig_handler)
            swgp_signal(signal.SIGTERM, logger_sig_handler)
            bufa = " " * (swlibc.SWLIB_PIPE_BUF + swlibc.SWLIB_PIPE_BUF)
            bufb = " " * (swlibc.SWLIB_PIPE_BUF + swlibc.SWLIB_PIPE_BUF)
            tmp = ""

            if s_efd:
                os.close(spipe[1])
            os.close(tpipe[1])
            if ppipe[0] >= 0:
                os.close(ppipe[0])

            bufa = bufa[:swlibc.SWLIB_PIPE_BUF + swlibc.SWLIB_PIPE_BUF]
            bufb = bufb[:swlibc.SWLIB_PIPE_BUF + swlibc.SWLIB_PIPE_BUF]

            # Force all file descriptors to 6 or below and
            # close all descriptors greater than 6.

            if (s_efd and spipe[0] <= 6) or tpipe[0] <= 6 or (ppipe[1] >= 0 and ppipe[1] <= 6):
                os.write(2, "{}: internal error in fork_logger\n".format(swlib_utilname_get()))
                return -1

            if logspec.logfd >= 0 and logspec.logfd <= 6:
                while logspec.logfd <= 6 and logspec.logfd != -1:
                    logspec.logfd = os.dup(logspec.logfd)

            if s_efd:
                if os.dup2(spipe[0], 3) < 0:
                    os.write(2, "dup2 error (loc=a)\n")
            else:
                os.close(3)

            if os.dup2(tpipe[0], 4) < 0:
                os.write(2, "dup2 error (loc=b)\n")

            if ppipe[1] >= 0:
                if os.dup2(ppipe[1], 5) < 0:
                    os.write(2, "dup2 error (loc=c)\n")
                ppipe[1] = 5
            else:
                os.close(5)

            if efd != 2:
                if os.dup2(efd, 2) < 0:
                    os.write(2, "dup2 error (loc=c)\n")

            if logspec.logfd >= 0:
                if os.dup2(logspec.logfd, 6) < 0:
                    os.write(2, "dup2 error (loc=e)\n")
                logspec.logfd = 6
            else:
                os.close(6)
            # close all fd's 7 and above
            swgp_close_all_fd(7)
            done_s = 0
            done_t = 0

            if not s_efd:
                done_s += 1

            while (not done_s or not done_t) and g_logger_sigterm == 0:
                rra = 0
                rrb = 0

                if not done_s:
                    sra = swgpReadFdNonblock(bufa, 3, rra)
                    if sra < 0 or (sra and not rra):
                        done_s += 1

                if rra > 0:
                    write_log(G, slinebuf, logspec, ofd, efd, bufa, rra, vlevel, ppipe[1])

                if not done_t:
                    srb = swgpReadFdNonblock(bufb, 4, rrb)
                    if srb < 0 or (srb and not rrb):
                        done_t += 1

                if rrb > 0:
                    write_log(G, tlinebuf, logspec, ofd, efd, bufb, rrb, vlevel, ppipe[1])

            status = 0

            if g_logger_sigterm:
                if g_logger_sigterm == signal.SIGTERM or g_logger_sigterm == signal.SIGUSR2:
                    tmp = "logger process terminated normally on SIGUSR2.\n"
                elif g_logger_sigterm == signal.SIGABRT:
                    status = 1
                    tmp = "logger process: abnormal termination on SIGABRT.\n"
                else:
                    status = 2
                    tmp = "logger process: abnormal termination: signum={}\n".format(g_logger_sigterm)

                swlib_doif_writef(vlevel, 1 if status else swlibc.SWC_VERBOSE_6, logspec, efd, "{}".format(tmp))
            else:
                swlib_doif_writef(vlevel, swlibc.SWC_VERBOSE_8, logspec, efd, "logger unexpected exit.\n")

            swc_flush_logger(G, slinebuf, tlinebuf, efd, ofd, logspec, vlevel)
            os.exit(status)

    strob_close(slinebuf)
    strob_close(tlinebuf)

    if s_efd:
        os.close(spipe[0])

    os.close(tpipe[0])

    if ppipe[1] >= 0:
        os.close(ppipe[1])

    if swi_event_fd_p:
        swi_event_fd_p = ppipe[0]

    if s_efd:
        s_efd = spipe[1]

    t_efd = tpipe[1]

    return pid


def swc_get_pax_read_command(g_pax_read_commands, keyid, verbose_level, keep, paxdefault):
    i = 0
    id = None
    ret = None
    readcmd = None
    print("BEGIN struct=%p" % g_pax_read_commands)
    print("BEGIN keyid=%s" % keyid)
    print("BEGIN verbose_level=%d" % verbose_level)
    print("BEGIN keep=%d" % keep)
    print("BEGIN paxdefault=%s" % paxdefault)
    if not keyid:
        return None
    if keyid == None or len(keyid) == 0:
        print("strlen(keyid) == 0")
        keyid = swcc.DEFAULT_PAX_R
    i = 0
    readcmd = g_pax_read_commands[i]
    id = readcmd.idM
    while id:
        print("while..")
        if keyid == id:
            print("strcmp(keyid, id) == 0")
            ret = None
            if keep:
                print("")
                ret = readcmd.keep_commandM
            if ret == None and verbose_level <= 0:
                print("")
                ret = readcmd.commandM
            if ret == None:
                print("")
                ret = readcmd.verbose_commandM
            print("return pax command [%s]" % ret)
            return ret
        i += 1
        readcmd = g_pax_read_commands[i]
        id = readcmd.idM
    return swc_get_pax_read_command(g_pax_read_commands, paxdefault, verbose_level, keep, swcc.DEFAULT_PAX_R)


def swc_get_pax_write_command(g_pax_write_commands, keyid, verbose_level, paxdefault):
    i = 0
    id = None
    writecmd = None
    if not keyid:
        return None
    if len(keyid) == 0:
        return swc_get_pax_write_command(g_pax_write_commands, swcc.DEFAULT_PAX_W, verbose_level, swcc.DEFAULT_PAX_W)
    writecmd = g_pax_write_commands[i]
    id = writecmd.idM
    while id:
        if keyid == id:
            return writecmd.commandM
        i += 1
        writecmd = g_pax_write_commands[i]
        id = writecmd.idM
    return swc_get_pax_write_command(g_pax_write_commands, paxdefault, verbose_level, swcc.DEFAULT_PAX_W)


def swc_get_pax_remove_command(g_pax_remove_commands, keyid, verbose_level, paxdefault):
    i = 0
    id = None
    removecmd = None
    if not keyid:
        return None
    if len(keyid) == 0:
        return swc_get_pax_remove_command(g_pax_remove_commands, swcc.DEFAULT_PAX_REM, verbose_level, swcc.DEFAULT_PAX_REM)
    removecmd = g_pax_remove_commands[i]
    id = removecmd.idM
    while id:
        if keyid == id:
            return removecmd.commandM
        i += 1
        removecmd = g_pax_remove_commands[i]
        id = removecmd.idM
    return swc_get_pax_remove_command(g_pax_remove_commands, paxdefault, verbose_level, swcc.DEFAULT_PAX_REM)


def swc_process_swoperand_file(swutil, type_string, filename, p_stdin_in_use, p_array_index, fd_array):
    # We are using a dictionary for the valid operands
    return None


def swc_set_boolean_x_option(opta, nopt, arg, optionp):
    # We use a dictionary for options
    return None


def swc_write_source_copy_script(G, ofd, sourcepath, do_get_file_type, verbose_threshold, delaytime, nhops,
                                 pax_write_command_key, hostname, blocksize):
    vlv = G.g_verboseG

    if ".." in sourcepath and sourcepath.index("..") == 0:
        return 1
    if "../" in sourcepath:
        return 1
    if swlib_is_sh_tainted_string(sourcepath):
        return 1

    sourcepath = sourcepath.replace("//", "/")
    if len(sourcepath) > 1:
        if sourcepath[-1] == '/':
            sourcepath = sourcepath[:-1]

    to_devnull = []
    opt1 = []
    opt2 = []
    set_vx = []
    rootdir = []
    firstpath = []
    shell_lib_buf = []
    if vlv >= swlibc.SWC_VERBOSE_SWIDB:
        set_vx = "set -vx\n"
    if vlv <= verbose_threshold:
        to_devnull = "2>/dev/null"
    tmp = os.path.dirname(sourcepath)
    dirname = tmp
    tmp = os.path.basename(sourcepath)
    spath = tmp
    if do_get_file_type:
        if '\n' in spath or len(spath) > swcc.MAX_CONTROL_MESG_LEN - len(swcc.SWBIS_SWINSTALL_SOURCE_CTL_ARCHIVE + ":"):
            return 1
        opt1 = "echo \"" + swcc.SWBIS_SOURCE_CTL_ARCHIVE + ": " + spath + "\""
        opt2 = "echo \"" + swcc.SWBIS_SOURCE_CTL_DIRECTORY + ": " + spath + "\""
    pax_write_command = swc_get_pax_write_command(G.g_pax_write_commands, pax_write_command_key, G.g_verboseG,
                                                  swcc.DEFAULT_PAX_W)
    if swlib_is_sh_tainted_string(spath):
        return 1
    if swlib_is_sh_tainted_string(dirname):
        return 1
    if spath == "/" and dirname == "/":
        spath = "."
    if G.g_sourcepath_list:
        n_source_specs = len(G.g_sourcepath_list)
    else:
        n_source_specs = 1
    if n_source_specs > 1:
        E_DEBUG("n_source_specs > 1")
        mpath = swc_make_multiplepathlist(G.g_sourcepath_list, rootdir, firstpath)
        if mpath:
            dirname = strob_str(rootdir)
            spath = strob_str(firstpath)
    else:
        E_DEBUG("n_source_specs not > 1")
        mpath = None
    if n_source_specs > 1:
        E_DEBUG("")
        if mpath is None:
            E_DEBUG("")
            n_source_specs = 1
    buffer_new = ""
    #   if get_opta(G.opta, SW_E_swbis_shell_command) == "detect":
    setdefaults(None)
    if get_opta(G.opta, sw_defaults['shell_cmd']) == "detect":
        swpl_bashin_detect(buffer_new)
    #   elif get_opta(G.opta, SW_E_swbis_shell_command) == "posix":
    elif get_opta(G.opta, sw_defaults['shell_cmd']) == "posix":
        swpl_bashin_posixsh(buffer_new)
    else:
        #       swpl_bashin_testsh(buffer_new, get_opta(G.opta, SW_E_swbis_shell_command))
        swpl_bashin_testsh(buffer_new, get_opta(G.opta, '/usr/bin/sh'))

    buffer_new += "echo " + swcc.SWBIS_TARGET_CTL_MSG_125 + ": " + os.P_PID() + "\n"
    buffer_new += swlibc.CSHID
    buffer_new += set_vx + "\n"
   # buffer_new += shlib_get_function_text_by_name("shls_check_for_gnu_tar", shell_lib_buf, None) + "\n"
    buffer_new += "do_create=\"" + ("x" if G.g_do_createM != 0 else "") + "\"\n"
    buffer_new += "multiple_source=\"" + ("x" if n_source_specs > 1 else "") + "\"\n"
    buffer_new += "blocksize=\"" + blocksize + "\"\n"
    buffer_new += "dirname=\"" + dirname + "\"\n"
    buffer_new += "path=\"" + spath + "\"\n"
    buffer_new += "sourcepath=\"" + sourcepath + "\"\n"
    buffer_new += "sw_retval=0\n"
    buffer_new += "sb__delaytime=" + str(delaytime) + "\n"
    buffer_new += "pax_write_command_key=\"" + pax_write_command_key + "\"\n"
    buffer_new += swlibc.CSHID
    buffer_new += "if test -d \"$dirname\"; then\n"
    buffer_new += "       cd \"$dirname\"\n"
    buffer_new += "       sw_retval=$?\n"
    buffer_new += "       case $sw_retval in\n"
    buffer_new += "       0)\n"
    buffer_new += "       if test \"$multiple_source\" -o ( -d \"$path\" -a -r \"$path\" ) -o ( \"$do_create\" -a -r \"$path\" ); then\n"
    buffer_new += "               \t\t" + opt2 + "\n"
    buffer_new += "               \t\t" + opt1 + "\n"
    buffer_new += "               \t\t" + opt2 + "\n"
    buffer_new += "       # disable test for gnu tar when the selected tar command\n"
    buffer_new += "       # is tar, this preserves ability to only use generic tar options\n"
    buffer_new += "       # and avoid addition of the --format=pax string\n"
    buffer_new += "               case \"${pax_write_command_key}\" in\n"
    buffer_new += "               tar)\n"
    buffer_new += "               ( exit 1 ) # false\n"
    buffer_new += "               ;;\n"
    buffer_new += "               *)\n"
    buffer_new += "               shls_check_for_gnu_tar \n"
    buffer_new += "               ;;\n"
    buffer_new += "               esac\n"
    buffer_new += "               has_gnu_tar=$?\n"
    buffer_new += "               case \"$multiple_source\" in\n"
    buffer_new += "                       \"\")\n"
    buffer_new += "                               case \"$has_gnu_tar\" in \n"
    buffer_new += "                               0)  # Yes, has gnu tar as tar\n"
    buffer_new += "                               tar cbf 1 - --format=pax \"$path\"\n"
    buffer_new += "                               sw_retval=$?\n"
    buffer_new += "                               ;;\n"
    buffer_new += "                               *)\n"
    buffer_new += "                               " + pax_write_command + " \"$path\"\n"
    buffer_new += "                               sw_retval=$?\n"
    buffer_new += "                               ;;\n"
    buffer_new += "                               esac\n"
    buffer_new += "                       ;;\n"
    buffer_new += "                       x)\n"
    buffer_new += "                               case \"$has_gnu_tar\" in \n"
    buffer_new += "                               0)  # Yes, has gnu tar as tar\n"
    buffer_new += "                               tar cbf 1 - --format=pax " + (
        strob_str(mpath) if mpath else ".") + "\n"
    buffer_new += "                               sw_retval=$?\n"
    buffer_new += "                               ;;\n"
    buffer_new += "                               *)\n"
    buffer_new += "                               " + pax_write_command + " " + (
        strob_str(mpath) if mpath else ".") + "\n"
    buffer_new += "                               sw_retval=$?\n"
    buffer_new += "                               ;;\n"
    buffer_new += "                               esac\n"
    buffer_new += "                       ;;\n"
    buffer_new += "               esac\n"
    buffer_new += "\t\t" + opt1 + "\n"
    buffer_new += "       elif test -e \"$path\" -a -r \"$path\"; then\n"
    buffer_new += "\t\t" + opt2 + "\n"
    buffer_new += "\t\t" + opt1 + "\n"
    buffer_new += "\t\t" + opt2 + "\n"
    buffer_new += "               dd ibs=\"$blocksize\" obs=512 if=\"$path\" " + to_devnull + ";\n"
    buffer_new += "               sw_retval=$?\n"
    buffer_new += "\t\t" + opt1 + "\n"
    buffer_new += "       else\n"
    buffer_new += "\t\t" + opt2 + "\n"
    buffer_new += "\t\t" + opt1 + "\n"
    buffer_new += "               sw_retval=1\n"
    buffer_new += "       fi\n"
    buffer_new += "       ;;\n"
    buffer_new += "       *)\n"
    buffer_new += "\t\t" + opt2 + "\n"
    buffer_new += "\t\t" + opt1 + "\n"
    buffer_new += "       ;;\n"
    buffer_new += "       esac\n"
    buffer_new += swlibc.CSHID
    buffer_new += "elif test -e \"$sourcepath\" -a -r \"$sourcepath\"; then\n"
    buffer_new += "\t" + opt2 + "\n"
    buffer_new += "\t" + opt1 + "\n"
    buffer_new += "\t" + opt2 + "\n"
    buffer_new += "       dd ibs=\"$blocksize\" obs=512 if=\"$sourcepath\" " + to_devnull + ";\n"
    buffer_new += "       sw_retval=$?\n"
    buffer_new += "\t" + opt1 + "\n"
    buffer_new += swlibc.CSHID
    buffer_new += "else\n"
    buffer_new += "\t" + opt2 + "\n"
    buffer_new += "\t" + opt1 + "\n"
    buffer_new += "       sw_retval=1\n"
    buffer_new += "fi\n"
    buffer_new += "if test \"$sw_retval\" != \"0\"; then\n"
    buffer_new += "        sb__delaytime=0;\n"
    buffer_new += "fi\n"
    buffer_new += "sleep \"$sb__delaytime\"\n"
    buffer_new += opt1 + "\n"
    buffer_new += opt2 + "\n"

    xx = buffer_new
    ret = os.write(ofd, xx)
    if ret != len(xx):
        return 1
    if G.g_source_script_name:
        E_DEBUG2("writing source script: %s", G.g_source_script_name)
        swlib_tee_to_file(G.g_source_script_name, -1, xx, -1, 0)
    del spath, dirname, tmp, buffer_new, opt1, opt2, set_vx, to_devnull, rootdir, shell_lib_buf
    if mpath:
        del mpath

    return not (ret > 0)


def swc_read_target_message_129(G, fd, cdir, vlevel, loc):
    tag = swlibc.SWBIS_TARGET_CTL_MSG_129 + ": "
    control_message = ""

    ret = swc_read_target_ctl_message(G, fd, control_message, vlevel, loc)
    if ret < 0:
        return -1
    if tag not in control_message:
        return -1
    d = control_message[len(tag):]
    cdir = d.rstrip('\n')
    return cdir


def swc_read_target_ctl_message(G, fd, control_message, vlevel, loc):
    n = 0
    c = ['', '']
    count = 0
    E_DEBUG2("BEGIN loc=%s", loc)
    control_message = ""
    c[1] = '\0'
    n = try_nibble(fd, (c, 3))
    if (n != 1):
        E_DEBUG("")
        return -1
    else:
        E_DEBUG("")
        count += 1
    E_DEBUG("")
    while n == 1 and c[0] != '\n' and count < swlibc.MAX_CONTROL_MESG_LEN:
        E_DEBUG("")
        control_message += c
        n = os.read(fd, 1)
        E_DEBUG("")
        if n == 1:
            count += 1
    E_DEBUG("out of loop")
    swlib_doif_writef(vlevel, swlibc.SWC_VERBOSE_IDB, G.g_logspec, G.g_stderr_fd,
                      "%s control message: %s\n",
                      loc, control_message)
    E_DEBUG("after msg")
    if n == 1 and count == 1:
        E_DEBUG("return 0")
        return 0
    elif n == 1 and count < swlibc.MAX_CONTROL_MESG_LEN:
        E_DEBUG2("return count=%d", count)
        return count
    else:
        E_DEBUG("return -1")
        return -1


def swc_get_target_script_pid(G, control_message):
    s = control_message.decode('utf-8')
    p = s.find(swlibc.SWBIS_TARGET_CTL_MSG_125)
    if p == -1:
        return None
    if p != 0:
        return None
    p += len(swlibc.SWBIS_TARGET_CTL_MSG_125)
    if s[p] != ':':
        return None
    p += 1
    if s[p] != ' ':
        return None
    p += 1
    ret = s[p:]
    if '\n' in ret:
        ret = ret[:ret.index('\n')]
    if '\r' in ret:
        ret = ret[:ret.index('\r')]
    return ret


def swc_read_start_ctl_message(G, fd, control_message, tramp_list, vlevel, pscript_pid, loc):
    ret = 0
    print("START loc={}".format(loc))
    control_message = ""
    while ret >= 0 and control_message.find(swlibc.SWBIS_TARGET_CTL_MSG_125) == -1:
        print("")
        ret = swc_read_target_ctl_message(G, fd, control_message, vlevel, loc)
        print("ret={}".format(ret))
        if ret >= 0 and len(control_message) > 0:
            print("")
            tramp_list.append(control_message)
    if ret < 0:
        print("error")
        return ret
    pscript_pid[0] = swc_get_target_script_pid(G, control_message)
    if pscript_pid[0] == None:
        # allow this to be OK, A NetBSD PATH=`getconf PATH` sh
        # did not support $PPID. Use of the PPID is only needed for
        # multiple ssh-hops, and maybe it shouldn't be needed at all.
        ret = 0
    else:
        swlib_doif_writef(vlevel, swlibc.SWC_VERBOSE_IDB, G.g_logspec, G.g_stderr_fd,
                          "remote script pid: {}\n".format(pscript_pid[0]))
    print("returning {}".format(ret))
    return ret


def swc_tee_to_file(filename, buf):
    ret = 0
    fd = 0
    res = 0
    if filename.isdigit():
        fd = int(filename)
        if res:
            return -1
    else:
        fd = open(filename, 'w')
    if fd < 0:
        return fd
    ret = len(buf)
    fd.write(buf)
    fd.close()
    return ret


def swc_gf_xformat_close(G, xformat):
    if xformat:
        xformat.close()
    G.g_xformat = None


def swc_get_stderr_fd(G):
    return G.g_stderr_fd

def swc_shutdown_logger(G, signum):
    if signum == 0:
        signum = signal.SIGTERM

    if G.g_logger_pid > 0:
        try:
            os.kill(G.g_logger_pid, signum)
        except OSError as e:
            if e.errno != errno.ESRCH:
                print(f"{swlib_utilname_get()}: kill(2): {e.strerror}")

    if G.g_swevent_fd >= 0:
        os.close(G.g_swevent_fd)

    swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB, G.g_logspec, swc_get_stderr_fd(G), "waitpid() on logger_pid [%d]" % G.g_logger_pid)

    ret = os.waitpid(G.g_logger_pid, os.WNOHANG)
    i = 0
    time_to_wait = 10  # seconds
    while ret[0] == 0 and i < time_to_wait:
        time.sleep(0.05)
        ret = os.waitpid(G.g_logger_pid, os.WNOHANG)
        print(f"waitpid returned {ret[0]} status={ret[1]}")
        if ret[0] < 0:
            return ret[0]
        i += 1

    if ret[0] == 0 and signum != signal.SIGKILL:
        signum = signal.SIGKILL
        try:
            os.kill(G.g_logger_pid, signum)
        except OSError as e:
            print(f"{swlib_utilname_get()}: kill(2): {e.strerror}")

    if signum != signal.SIGKILL:
        if os.WIFEXITED(ret[1]):
            retval = os.WEXITSTATUS(ret[1])
        else:
            retval = -1
    else:
        retval = -2

    swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB, G.g_logspec, swc_get_stderr_fd(G), "finished waitpid() on logger_pid [%d]" % G.g_logger_pid)

    os.close(G.g_t_efd)
    os.close(G.g_s_efd)

    return retval


def swc_set_stderr_fd(G, fd):
    G.g_stderr_fd = fd
    swutil_set_stderr_fd(G.g_swlog, fd)


def swc_stderr_fd2_set(G, fd):
    G.g_save_stderr_fdM = swc_get_stderr_fd(G)
    swc_set_stderr_fd(G, fd)


def swc_stderr_fd2_restore(G):
    swc_set_stderr_fd(G, G.g_save_stderr_fdM)


def swc_lc_raise(G, file, line, signo):
    swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB, G.g_logspec, swc_get_stderr_fd(G), "raising signal %d at %s:%d\n",
                      signo, file, line)
    G.g_signal_flag = signo
    raise (signo)


def swc_lc_exit(G, file, line, status):
    swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB, G.g_logspec, swc_get_stderr_fd(G),
                      "exiting at %s:%d with value %d\n", file, line, status)
    exit(status)


def swc_is_option_true(s):
    return swlib_is_option_true(s)


def sw_exitval(G, count, suc):
    if G.g_master_alarm == 0 and suc and count == suc:
        return 0
    if suc and count:
        return 2
    if suc == 0:
        return 1
    swlib_doif_writef(G.g_verboseG, G.g_fail_loudly, G.g_logspec, swc_get_stderr_fd(G), "abnormal exit\n")
    return 255


def swc_check_for_current_signals(G, lineno, no_remote_kill):
    #   pending_set = sigset_t()
    pending_set = signal.sigpending()
    signum = 0
    ret = 0

    # check for pending signals

    ret = signal.sigpending()
    if ret == 0:
        signum = 0
        if pysigset.sigismember(pending_set, signal.SIGINT) == 1:
            E_DEBUG("got pending SIGINT")
            signum = signal.SIGINT
        elif pysigset.sigismember(pending_set, signal.SIGTERM) == 1:
            E_DEBUG("got pending SIGTERM")
            signum = signal.SIGTERM

        if signum != 0:
            G.g_safe_sig_handler.invoke(signum)

    if G.g_signal_flag == signal.SIGUSR1:
        if G.g_running_signalusr1:
            return
        G.g_running_signalusr1 = 1
        G.g_main_sig_handler.invoke(G.g_signal_flag)
    elif (G.g_signal_flag == signal.SIGTERM) or (G.g_signal_flag == signal.SIGPIPE) or (G.g_signal_flag == signal.SIGINT):
        if G.g_running_signalsetM:
            return
        E_DEBUG("")
        swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB, G.g_logspec, swc_get_stderr_fd(G),
                          "swc_check_for_current_signals: line=%d Got signal %d.\n", lineno, G.g_signal_flag)
        E_DEBUG("")
        swgp_signal_block(signal.SIGTERM, None)
        swgp_signal_block(signal.SIGINT, None)
        swgp_signal_block(signal.SIGPIPE, None)

        E_DEBUG("")

        #
        # Call the main handler here
        #

        G.g_running_signalsetM = 1
        (G.g_main_sig_handler).invoke(G.g_signal_flag)

        E_DEBUG("")


# class fp_scriptDelegate:
#    def invoke(self, G, ofd, sourcepath, do_get_file_type, vlv, delaytime, nhops, pax_write_command_key, hostname, blocksize):
#        pass

def swc_run_source_script(G, swutil_x_mode, local_stdin, source_pipe, source_path, nhops, pax_write_command_key,
                          fork_blockmask, fork_defaultmask, hostname, blocksize, verbose_threshold, fp_script):

    if swutil_x_mode == 0 or local_stdin != 0:
        #
        # fork not required.
        #
        return 0

    write_pid = swndfork(fork_blockmask, fork_defaultmask)
    if write_pid == 0:
        ret_source = 0
        #
        # Write the source scripts.
        #
        if swutil_x_mode != 0 and local_stdin == 0:
            #
            # If source is remote.
            #
            swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB2, G.g_logspec, swc_get_stderr_fd(G),
                              "running source script.\n")
            # ret_source = swc_write_source_copy_script(G,
            ret_source = fp_script.invoke(G, source_pipe, source_path, swlibc.SWINSTALL_DO_SOURCE_CTL_MESSAGE,
                                          verbose_threshold, swlibc.SWC_SCRIPT_SLEEP_DELAY, nhops, pax_write_command_key,
                                          hostname, blocksize)
            swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB2, G.g_logspec, swc_get_stderr_fd(G),
                              "Source script finished return value=%d.\n", ret_source)

        else:
            swlib_doif_writef(G.g_verboseG, G.g_fail_loudly, G.g_logspec, swc_get_stderr_fd(G),
                              "Unexpected Dead code excpeption\n")
            swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_IDB2, G.g_logspec, swc_get_stderr_fd(G),
                              "NOT running source script in child, exiting with %d.\n", ret_source)
            sys.exit(1)
        #
        # 0 is OK
        # !0 is error
        #
        sys.exit(ret_source)
    elif write_pid < 0:
        swlib_doif_writef(G.g_verboseG, G.g_fail_loudly, G.g_logspec, swc_get_stderr_fd(G), "fork error : %s\n",
                          os.error(1))
    return write_pid


def swc_helptext_target_syntax(fp):
    print("Help text is in argparse for" + fp)


def swc_wait_on_script(G, write_pid, location):
    #
    # wait on the write_script process and check for errors.
    #
    ret = os.waitpid(write_pid, 0)
    if ret < 0:
        swlib_doif_writef(G.g_verboseG, G.g_fail_loudly, G.g_logspec, swc_get_stderr_fd(G),
                          "{} scripts waitpid : {}\n".format(location, os.strerror(errno)))
        return -1
    status = ret[1]
    if os.WIFEXITED(status):
        return os.WEXITSTATUS(status)
    else:
        swlib_doif_writef(G.g_verboseG, G.g_fail_loudly, G.g_logspec, swc_get_stderr_fd(G),
                          "{} script had abnormal exit.\n".format(location))
        return -2


def swc_set_shell_dash_s_command(G, wopt_shell_command):
    tmp = string.Template()
    sh_dash_s = swssh_landing_command(wopt_shell_command, 0)
    if G.g_do_cleanshM == 0:
        tmp = tmp.substitute(sh_dash_s=sh_dash_s, swlib_utilname_get=swlib_utilname_get)
        tmp = tmp.substitute(sh_dash_s=sh_dash_s, swlib_utilname_get=swlib_utilname_get)
    else:
        tmp = tmp.substitute(sh_dash_s=sh_dash_s)
    G.g_sh_dash_s = tmp


def swc_write_swi_debug(swi, filename):
    if filename.isdigit():
        fd, res = int(filename), 0
    else:
        fd = os.open(filename, os.O_RDWR | os.O_CREAT | os.O_TRUNC, 0o644)
        if fd < 0:
            return -1
    image = swi_dump_string_s(swi, "(SWI*)")
    if not image:
        return -1
    res = os.write(fd, image.encode())
    os.close(fd)
    return res

def swi_dump_string_s(xx, prefix):
    buf9 = STROB()
    buf = buf9
    tmp_s = swi_package_dump_string_s(xx.swi_pkg, prefix)
    return buf.str_

def swi_package_dump_string_s(xx: "SWI_PACKAGE", prefix: str) -> str:
    buf = STROB(b"", "", 0, 0, 0, 0, 0)
    tmp = STROB(b"", "", 0, 0, 0, 0, 0)
    tmpprefix = STROB(b"", "", 0, 0, 0, 0, 0)

    strob_sprintf(buf, 0, f"{prefix}{xx!r} (SWI_PACKAGE*)\n")
    strob_sprintf(buf, 1, f"{prefix}{xx.base!r}->base = [{xx.base!r}]\n")
    strob_sprintf(tmp, 0, f"{prefix}{xx.base!r}->base:")
    strob_sprintf(buf, 1, f"{swi_base_dump_string_s(xx.base, tmp.str_.decode())}")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->swi_sc = [{xx.swi_sc!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->exlist = [{xx.exlist!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->dfiles = [{xx.dfiles!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->current_xfile = [{xx.current_xfile!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->prev_swpath_ex = [{xx.prev_swpath_ex!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->dfiles_attribute = [{xx.dfiles_attribute!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->pfiles_attribute = [{xx.pfiles_attribute!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->dfiles = [{xx.dfiles!r}] (BEGIN DFILES)\n")
    strob_sprintf(buf, 1, f"{swi_xfile_dump_string_s(xx.dfiles, prefix)}")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->dfiles = [{xx.dfiles!r}] (END DFILES)\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->catalog_start_offset = [{xx.catalog_start_offset}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->catalog_end_offset = [{xx.catalog_end_offset}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->catalog_length = [{xx.catalog_length}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->target_path = [{xx.target_path!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->target_host = [{xx.target_host!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->catalog_entry = [{xx.catalog_entry!r}]\n")
    strob_sprintf(buf, 1, f"{prefix}{xx!r}->installed_software_catalog = [{xx.installed_software_catalog!r}]\n")
    strob_sprintf(tmpprefix, 0, f"{prefix}")
    strob_sprintf(buf, 1, f"{swi_script_list_dump_string_s(xx.swi_sc, tmpprefix.str_.decode())}")

    for i in range(swlibc.SWI_MAX_OBJ):
        prod = xx.swi_coM[i]
        if prod:
            strob_sprintf(buf, 1, f"{prefix}{xx!r}->swi_co[{i}] = {prod!r} (BEGIN PRODUCT[{i}])\n")
            strob_sprintf(buf, 1, f"{swi_product_dump_string_s(prod, prefix)}")
            strob_sprintf(buf, 1, f"{prefix}{xx!r}->swi_co[{i}] = {prod!r} (END PRODUCT[{i}])\n")

    return buf.str_.decode()

def swi_product_dump_string_s(xx, prefix):
    buf = STROB()
    tmp = STROB()

    buf.str_ = f"{prefix}{xx} (SWI_PRODUCT*)\n"

    buf.str_ += f"{prefix}{xx.p_base} ->p_base = [{xx.p_base}]\n"
    tmp.str_ = f"{prefix}{xx.p_base} ->base:"
    buf.str_ += swi_base_dump_string_s(xx.p_base, tmp.str_)

    buf.str_ += f"{prefix}{xx} ->package_path = [{xx.package_path}]\n"
    buf.str_ += f"{prefix}{xx} ->control_dir = [{xx.control_dir}]\n"
    buf.str_ += f"{prefix}{xx} ->xfile = [{xx.xfile}]\n"

    buf.str_ += f"{prefix}{xx} ->xfile = [{xx.xfile}] (BEGIN PFILES)\n"
    tmp_s = swi_xfile_dump_string_s(xx.xfile, prefix)
    buf.str_ += tmp_s
    buf.str_ += f"{prefix}{xx} ->xfile = [{xx.xfile}] (END PFILES)\n"

    buf.str_ += f"{prefix}{xx} ->filesets = [{xx.filesets}]\n"
    if xx.filesets:
        buf.str_ += strar_dump_string_s(xx.filesets, prefix)

    i = 0
    while i < swlibc.SWI_MAX_OBJ:
        buf.str_ += f"{prefix}{xx} ->swi_co[{i}] = [{xx.swi_co[i]}]\n"
        i += 1

    i = 0
    while i < swlibc.SWI_MAX_OBJ and xx.swi_co[i]:
        buf.str_ += f"{prefix}{xx} ->swi_coM[{i}] = {xx.swi_co[i]} (BEGIN FILESET[{i}])\n"
        tmp_s = swi_xfile_dump_string_s(xx.swi_co[i], prefix)
        buf.str_ += tmp_s
        buf.str_ += f"{prefix}{xx} ->swi_co[{i}] = {xx.swi_co[i]} (END FILESET[{i}])\n"
        i += 1

    return buf.str_

def swc_convert_multi_host_target_syntax(G, target_spec):

    first = target_spec.find(':')
    second = target_spec.find(':', first + 1)

    length = 0


    retval = target_spec[:]
    if first != -1 and second != -1:
        # Two or more ':', this is a multi hop spec using the
        # Ssh scp-like syntax
        if first == 0 and second == len(target_spec) - 1:
            # Its  :HOST:
            swlib_doif_writef(G.g_verboseG, G.g_fail_loudly, G.g_logspec, swc_get_stderr_fd(G),
                              "warning: assuming %s is a host\n", target_spec)
            retval = target_spec[first + 1:]
            return retval
        else:
            # Its  HOST:HOST:  ...
            # a trailing ':' means it's a host

            # Scan the string backwards
            length = len(target_spec)
            is_a_host = 0
            is_a_file = 0
            cnt = length
            tmp_target_spec = retval
            s = tmp_target_spec + (length - 1)
            while cnt > 0:
                s = tmp_target_spec + (cnt - 1)
                if cnt == length:
                    # Check last char
                    if s == ':':
                        is_a_host = 1
                        s = '\0'
                    else:
                        is_a_file = 1
                    # squash this ':' while we have a chance
                elif s == ':':
                    if is_a_host != 0:
                        s = '\r'  # placeholder for @@
                    else:
                        # Leave the ':' it indicates a file,
                        # all subsequent matches are hosts
                        is_a_host = 1
                cnt -= 1
                # Now, convert the '\r' to "@@"
                tmp = strob_open(14)
                s = tmp_target_spec
                while s != '\0':
                    if s == '\r':
                        strob_strcat(tmp, "@@")
                    else:
                        strob_charcat(tmp, ord(s))
                    s += 1
                retval = strob_str(tmp)
                strob_close(tmp)
                return retval
    else:
        # Only one ':', normal usage
        return retval
    return retval  # this should never happen


def sw_e_msg(G, format, *args):
    ret = doif_i_writef(G.g_verboseG, G.g_fail_loudly, G.g_logspec, swc_get_stderr_fd(G), format, *args)
    return ret


def sw_l_msg(G, at_level, format, *args):
    ret = doif_i_writef(G.g_verboseG, at_level, G.g_logspec, swc_get_stderr_fd(G), format, *args)
    return ret


def sw_d_msg(G, format, *args):
    dl = G.g_verboseG
    if G.devel_verbose:
        dl = swlibc.SWC_VERBOSE_IDB2
    ret = doif_i_writef(dl, swlibc.SWC_VERBOSE_IDB2, G.g_logspec, swc_get_stderr_fd(G), format, *args)
    return ret


def swc_check_shell_command_key(G, cmd):
    wopt_shell_command = cmd
    if (
            wopt_shell_command != swlibc.SWC_SHELL_KEY_BASH and
            wopt_shell_command != swlibc.SWC_SHELL_KEY_SH and
            wopt_shell_command != swlibc.SWC_SHELL_KEY_KSH and
            wopt_shell_command != swlibc.SWC_SHELL_KEY_MKSH and
            wopt_shell_command != swlibc.SWC_SHELL_KEY_DETECT and
            wopt_shell_command != swlibc.SWC_SHELL_KEY_POSIX
    ):
        sw_e_msg(G, "illegal shell command: %s \n", wopt_shell_command)
        exit(1)


#
# Ugh.. just use pax/opax for Red Hat LInux
#
def swc_check_all_pax_commands(G):
    type = "write"
    command = get_opta(G.opta, SW_E_swbis_local_pax_write_command)
    xcmd = swc_get_pax_write_command(G.g_pax_write_commands, command, 1, None)
    if not xcmd:
        sw_e_msg(G, "illegal pax %s command: %s \n", type, command)
        LCEXIT(1)
    command = get_opta(G.opta, SW_E_swbis_remote_pax_write_command)
    xcmd = swc_get_pax_write_command(G.g_pax_write_commands, command, 1, None)
    if not xcmd:
        sw_e_msg(G, "illegal pax %s command: %s \n", type, command)
        LCEXIT(1)
    type = "read"
    command = get_opta(G.opta, SW_E_swbis_remote_pax_read_command)
    xcmd = swc_get_pax_write_command(G.g_pax_write_commands, command, 1, None)
    if not xcmd:
        sw_e_msg(G, "illegal pax %s command: %s \n", type, command)
        LCEXIT(1)
    command = get_opta(G.opta, SW_E_swbis_local_pax_read_command)
    xcmd = swc_get_pax_write_command(G.g_pax_write_commands, command, 1, None)
    if not xcmd:
        sw_e_msg(G, "illegal pax %s command: %s \n", type, command)
        LCEXIT(1)


def swc_process_pax_command(G, command, type, sw_e_num):
    xcmd = None
    if type == "write":
        xcmd = swc_get_pax_write_command(
            G.g_pax_write_commands,
            command,
            G.g_verboseG, None)
    elif type == "read":
        xcmd = swc_get_pax_read_command(
            G.g_pax_read_commands,
            command,
            0, 0, None)
    elif type == "remove":
        xcmd = swc_get_pax_remove_command(
            G.g_pax_remove_commands,
            command, 0, None)
    else:
        xcmd = None
        sw_e_msg(G, "internal error\n")
        LCEXIT(1)
    if xcmd == None:
        sw_e_msg(G, "illegal pax %s command: %s \n",
                 type, command)
        LCEXIT(1)
    set_opta(G.opta, sw_e_num, command)


def swc_get_default_sh_dash_s(G):
    if get_opta(G.opta, SW_E_swbis_shell_command) == swlibc.SWC_SHELL_KEY_DETECT:
        return SWSSH_DETECT_POISON_DEFAULT
    else:
        return G.g_sh_dash_s
