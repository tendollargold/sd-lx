# swfork.py  --  specialized routines for fork'ing
#
#   Copyright (C) 2003-2004 James H. Lowe, Jr. <jhlowe@acm.org>
#   All Rights Reserved.
#   Copyright (C) 2023 Paul Weber convert to python
#
#   COPYING TERMS AND CONDITIONS
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
import sys
import os
import termios
import fcntl
import grp
import pwd
import tty
import select

import struct
import signal
import pysigset
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlibc
class SwForkConst:
    SWFORK_PTY = "pty"
    SWFORK_PTY2 = "pty2"
    SWFORK_NO_PTY = "no-pty"
    SWFORK_FORK = "fork"
    SWFORK_NO_FORK = "no-fork"

swfc = SwForkConst()

def swfork(arg):
    swndfork(arg, None)

def swlib_fork(ctype, ptrfdm, ofd, ifd, efd, slave_termios, slave_winsize, pcpid, make_raw, sigmask):
    if ctype == "pty":
        return -1
    elif ctype == "pty2":
        return swlib_pty_fork2(ptrfdm, ofd, ifd, efd, slave_termios, slave_winsize, pcpid, make_raw, sigmask)
    elif ctype == "no-pty":
        return swlib_no_pty_fork(ptrfdm, ofd, ifd, efd, sigmask)
    elif type == "fork":
        pid = swndfork(sigmask, None)
        if pid < 0:
            return -1
        elif pid == 0:
            return 0
        else:
            ptrfdm[0] = ifd
            ptrfdm[1] = ofd
            ptrfdm[2] = efd
            return pid
    elif type == "no-fork":
        ptrfdm[0] = ifd
        ptrfdm[1] = ofd
        ptrfdm[2] = efd
        return 0
    return -1

def swlib_pty_fork2(ptrfdm, ofd, ifd, efd, slave_termios, slave_winsize, pc_pid, make_raw, sigmask):
    fdm = os.openpty()[0]
    fds = os.open(os.ttyname(fdm), os.O_RDWR)
    if os.setsid():
        print("setsid error")
    if slave_termios:
        termios.tcsetattr(fds, termios.TCSANOW, slave_termios)
    if slave_winsize:
        fcntl.ioctl(fds, termios.TIOCSWINSZ, struct.pack("HHHH", slave_winsize.ws_row, slave_winsize.ws_col, slave_winsize.ws_xpixel, slave_winsize.ws_ypixel))
    c_pid = os.fork()
    if c_pid < 0:
        print("fork error")
        sys.exit(1)
    elif c_pid > 0:
        os.close(fdm)
        if os.dup2(fds, 0) != 0:
            print("dup2 error to ifd =", ifd)
        if os.dup2(fds, 1) != 1:
            print("dup2 error to ofd =", ofd)
        if os.dup2(efd, 2) != 2:
            print("dup2 error to efd =", efd)
        os.close(efd)
        return 0
    else:
        ret = swgp_stdioPump(fdm, fdm, 0, 1, 0, None, None, None)
        sys.exit(ret)


def swlib_no_pty_fork(ptrfdm, ofd, ifd, efd, sigmask):
    opipe = os.pipe()
    ipipe = os.pipe()
    pid = swndfork(sigmask, None)
    if pid < 0:
        return -1
    elif pid == 0:
        os.close(opipe[0])
        os.close(ipipe[1])
        if os.dup2(ipipe[0], 0) != 0:
            sys.stderr.write("dup2 error to ifd = %d" % ifd)
        if os.dup2(opipe[1], 1) != 1:
            sys.stderr.write("dup2 error to ofd = %d" % ofd)
        if os.dup2(efd, 2) != 2:
            sys.stderr.write("dup2 error to efd = %d" % efd)
        os.close(efd)
        os.close(opipe[1])
        os.close(ipipe[0])
        swgp_close_all_fd(3)
        return 0
    else:
        os.close(opipe[1])
        os.close(ipipe[0])
        ptrfdm[0] = opipe[0]
        ptrfdm[1] = ipipe[1]
        ptrfdm[2] = efd
        return pid


def swlib_tty_cbreak(fd):
    try:
        tty.setcbreak(fd)
        termios.tcsetattr(fd, termios.TCSAFLUSH, termios.tcgetattr(fd))
        return 0
    except termios.error:
        return -1

def swlib_tty_termios():
    return termios.tcgetattr(sys.stdin)


def swndfork(mask_to_block, mask_to_set_dfl):
    try:
        pid = os.fork()
        if pid > 0:
            return pid
        else:
            if mask_to_block is not None:
                pysigset.sigprocmask(signal.SIG_BLOCK, mask_to_block)
            if mask_to_set_dfl is not None:
                signal.signal(signal.SIGINT, signal.SIG_DFL)
            return pid
    except OSError:
        return -1
#
# Headers Above
#

def swlib_ptym_open(pts_name, start):
    ptr2char = "0123456789abcdef"
    ptr2start = ptr2char[start]
    if start < 0 or start >= len(ptr2char):
        return -1
    #pts_name = "/dev/ptyXY"
    for ptr1 in "pqrstuvwxyzPQRST":
        #pts_name[8] = ptr1
        #pts_name = ptr1
        for ptr2 in ptr2start:
            if swlib_get_verbose_level() >= swlibc.SWC_VERBOSE_7:
                sys.stderr.write("%s: ptr2start is [%s]\n" % (swlib_utilname_get(), ptr2start))
           # pts_name[9] = ptr2
            pts_name = ptr2
            try:
                fdm = os.open(pts_name, os.O_RDWR)
            except FileNotFoundError:
                return -1
            except:
                start += 1
                if swlib_get_verbose_level() >= swlibc.SWC_VERBOSE_7:
                    sys.stderr.write("%s: open attempt failed on pty (master) : %s\n" % (swlib_utilname_get(), pts_name))
                continue
            if swlib_get_verbose_level() >= swlibc.SWC_VERBOSE_7:
                sys.stderr.write("%s: Using pty (master) : %s\n" % (swlib_utilname_get(), pts_name))
            #pts_name[5] = 't'
            #pts_name = 't'
            return fdm
    return -1

def swlib_ptys_open(pts_name):
    pw = pwd.getpwuid(os.getuid())
    group = grp.getgrnam("tty")
    mode = 0
    uid = pw.pw_uid
    gid = group.gr_gid if group else pw.pw_gid
    try:
        fds = os.open(pts_name, os.O_RDWR)
        if os.chown(pts_name, uid, gid):
            if uid == 0:
                print("chown failed for slave pty")
                return -1
            else:
                print("warning: using pty %s with insecure ownership" % pts_name)
        if os.chmod(pts_name, mode):
            if uid == 0:
                print("chmod failed for slave pty")
                return -1
            else:
                print("warning: using pty %s with insecure permissions" % pts_name)
        return fds
    except FileNotFoundError:
        print("swlib_ptys_open: open failed on %s : %s" % (pts_name, os.error.errno))
        return -1

def my_openpty(p_fdm, p_fds):
    try:
        return os.openpty()
    except OSError:
        return -1

def release_tty_name(slave_pty_name, be_verbose):
    try:
        os.chown(slave_pty_name, 0, 0)
    except OSError:
        if be_verbose:
            print("%s: chown %.100s 0 0 failed: %.100s" % (swlib_utilname_get(), slave_pty_name, os.error.errno))
    try:
        os.chmod(slave_pty_name, 0o666)
    except OSError:
        if be_verbose:
            print("%s: chmod %.100s 0666 failed: %.100s" % (swlib_utilname_get(), slave_pty_name, os.error.errno))

def swlib_tty_raw(fd):
    attr = termios.tcgetattr(fd)
    attr[3] = attr[3] & ~termios.ICANON & ~termios.ECHO & ~termios.ISIG
    attr[6][termios.VMIN] = 1
    attr[6][termios.VTIME] = 0
    termios.tcsetattr(fd, termios.TCSANOW, attr)
    return 0

def swlib_tty_reset(fd):
    try:
        termios.tcsetattr(fd, termios.TCSAFLUSH, termios.tcgetattr(fd))
        return 0
    except termios.error:
        return -1

def swlib_tty_atexit():
    pass

def swlib_get_verbose_level():
    return 0

def swgp_close_all_fd(n):
    for fd in range(n, 1024):
        try:
            os.close(fd)
        except OSError:
            pass

def swgp_stdioPump(op, fdm, fds, ip, ifd, ofd, efd, sb):
    fds = [op, fdm, fds, ip]
    while True:
        r, w, x = select.select(fds, [], [])
        if op in r:
            data = os.read(op, 4096)
            if not data:
                fds.remove(op)
            else:
                os.write(ofd, data)
        if fdm in r:
            data = os.read(fdm, 4096)
            if not data:
                fds.remove(fdm)
            else:
                os.write(ip, data)
        if not fds:
            break
    return 0


