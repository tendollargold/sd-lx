# swintl.c  --  Internationalization info

# Copyright (C) 1999  Jim Lowe
# Copyright (c) 2024 Paul Weber, conversion to python
# All rights reserved.

# COPYING TERMS AND CONDITIONS:
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  */


# Language mapping table
swintl_lang_table = [
    {"C": ""},
    {"da": "Danish"},
    {"de": "German"},
    {"cs": "Czech"},
    {"en": "English"},
    {"es": "Spanish"},
    {"fi": "Finnish"},
    {"fr": "French"},
    {"hy": "Armenian"},
    {"hr": "Croatian"},
    {"hu": "Hungarian"},
    {"in": "Indonesian"},
    {"is": "Icelandic"},
    {"it": "Italian"},
    {"iw": "Hebrew"},
    {"ja": "Japanese"},
    {"ji": "Yiddish"},
    {"jw": "Javanese"},
    {"ko": "Korean"},
    {"no": "Norwegian"},
    {"pl": "Polish"},
    {"pt": "Portuguese"},
    {"pt_BR": "pt_BR"},
    {"ru": "Russian"},
    {"sk": "Slovak"},
    {"sv": "Swedish"},
    {"tr": "Turkish"},
    {"zh": "Chinese"},
    {None: None}
    # please add more
]

def get_lang_code(lang_name):
    i = 0
    while swintl_lang_table[i]["language_code"] is not None:
        if lang_name == swintl_lang_table[i]["language_name"]:
            return swintl_lang_table[i]["language_code"]
        i += 1
    return None

def swintl_get_lang_name(lang_code):
    i = 0
    while swintl_lang_table[i]["language_code"] is not None:
        if lang_code == swintl_lang_table[i]["language_code"]:
            return swintl_lang_table[i]["language_name"]
        i += 1
    return None