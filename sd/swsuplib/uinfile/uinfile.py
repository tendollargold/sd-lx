# sd.swsuplib.uinfile.uinfile.py: Open a package.
#
# Copyright (C) 1998  James H. Lowe, Jr.  <jhlowe@acm.org>
# Copyright (C) 2023 Paul Weber Convert to python

# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#


import fnmatch
import os
import errno
import signal
import sys
import time
import pysigset
import ctypes

from sd.const import LegacyConstants, SwGlobals
from sd.swsuplib.cplob import vplob_get_nstore, vplob_get_list, vplob_open, vplob_add
from sd.swsuplib.ls_list.ls_list import LsListConst
from sd.swsuplib.misc.shcmd import SHCMD, SHCMD_UNSET_EXITVAL
from sd.swsuplib.misc.cstrings import strcmp, strncmp, strstr, strerror
from sd.swsuplib.misc.ar import ArConst
from sd.swsuplib.misc.swvarfs import swvarfs_opendup, swvarfs_get_next_dirent, swvarfs_close
from sd.swsuplib.misc.swlib import TaruConstants, swlib_exec_filter, swlib_fork_to_make_unixfd, swlib_open_memfd, swlib_unix_dirtrunc
from sd.swsuplib.misc.swpath import swpath_open, swpath_close, swpath_parse_path
from sd.swsuplib.strob import strob_open, strob_strlen, strob_close, strob_strcpy, strob_str
from sd.swsuplib.taru.ahs import ahsstaticdeletefilehdr, ahsstaticgettarfilename, ahsstaticsettarfilename, \
    swlib_utilname_get, ahsstaticsetpaxlinkname, ahsstaticcreatefilehdr
from sd.swsuplib.taru.filetypes import Filetypes
from sd.swsuplib.taru.readinheader import taru_pump_amount2, taru_tape_buffered_read
from sd.swsuplib.taru.otar import taru_read_in_tar_header2
from sd.swsuplib.taru.taru import (taru_otoul, taru_tar_checksum, taru_create, taru_delete, taru_make_header,
                                   taru_free_header, taru_read_header, taru_print_tar_ls_list)
from sd.swsuplib.taru.taruib import taruib_unread, taruib_set_datalen
from sd.swsuplib.uxfio import UxfioConst, uxfio_fcntl, uxfio_close, uxfio_read
from sd.swsuplib.uxfio import uxfio_lseek, uxfio_opendup, uxfio_espipe, uxfio_write
from sd.swsuplib.misc.swlib import FrameRecord
from sd.swsuplib.misc.swfork import swndfork
from sd.swsuplib.misc.swgp import swgp_signal

g = SwGlobals()
uxfioc = UxfioConst()
tc = TaruConstants()
ft = Filetypes()
fr = FrameRecord()
ls = LsListConst()


class UinfileConstants:
    UINFILE_COMPRESSED_NA = -1
    UINFILE_COMPRESSED_NOT = 0
    UINFILE_COMPRESSED_Z = 1
    UINFILE_COMPRESSED_GZ = 2
    UINFILE_COMPRESSED_BZ = 3
    UINFILE_COMPRESSED_BZ2 = 4
    UINFILE_COMPRESSED_RPM = 5
    UINFILE_COMPRESSED_CPIO = 6
    UINFILE_COMPRESSED_DEB = 7
    UINFILE_COMPRESSED_LZMA = 8
    UINFILE_COMPRESSED_GPG = 9
    UINFILE_COMPRESSED_XZ = 10
    UINFILE_COMPRESSED_SLACK_WITH_NAME = 11
    UINFILE_I_INIT_READ_SIZE = 124
    UINFILE_IEEE_MAX_LEADING_DIR = 3
    UINFILE_NOSAVE = 0
    UINFILE_SAVE = 1
    UINFILE_MAGIC_gz = b"\x1f\x8b\x08\x00"
    UINFILE_MAGIC_Z = b"\x1f\x9d\x90"
    UINFILE_MAGIC_rpm = b"\xed\xab\xee\xdb"
    UINFILE_MAGIC_bz2 = b"\x42\x5a\x68"
    UINFILE_MAGIC_lzma = b"\x5d\x00\x00\x80"
    UINFILE_MAGIC_xz = b"\xfd\x37\x7a\x58"
    UINFILE_MAGIC_ar = b"!<arch>\n"
    UINFILE_MAGIC_deb = UINFILE_MAGIC_ar + b"debian-binary"
    UINFILE_MAGIC_binary_tarball = b"./"
    UINFILE_MAGIC_slack = b"install"
    UINFILE_MAGIC_slack_desc = UINFILE_MAGIC_slack + b"slack-desc"
    UINFILE_MAGIC_gpg_armor = b"-----BEGIN PGP"
    UINFILE_MAGIC_gpg_sym = b"\x8c\x0d"
    UINFILE_MAGIC_gpg_enc1 = b"\x85\x01"
    UINFILE_MAGIC_gpg_enc2 = b"\x85\x02"
    UINFILE_SWBIS_PATH0 = "catalog/swbis-0.2/"
    UINFILE_SW_MAGIC = "catalog/INDEX"
    UINFILE_SW_MAGIC1 = "catalog/INDEX"
    UINFILE_SW_MAGIC2 = "*/catalog/INDEX"
    UINFILE_SW_MAGIC_SRC = "*/catalog/INDEX"
    UINFILE_DEB_CONTROL_OFFSET = 132
    UINFILE_DETECT_2TARRPM = (1 << 0)
    UINFILE_DETECT_2TAR = (1 << 1)
    UINFILE_DETECT_2ARCHIVE = (1 << 2)
    UINFILE_DETECT_OTARALLOW = (1 << 3)
    UINFILE_DETECT_OTARFORCE = (1 << 4)
    UINFILE_DETECT_2ARCHIVERAW = (1 << 5)
    UINFILE_DETECT_FORCEUXFIOFD = (1 << 6)
    UINFILE_DETECT_FORCEUNIXFD = (1 << 7)
    UINFILE_DETECT_RETURNFILE = (1 << 8)
    UINFILE_DETECT_NATIVE = (1 << 9)
    UINFILE_DETECT_FORCE_SEEK = (1 << 10)
    UINFILE_DETECT_IEEE = (1 << 11)
    UINFILE_DETECT_ARBITRARY_DATA = (1 << 12)
    UINFILE_UXFIO_BUFTYPE_MEM = (1 << 13)
    UINFILE_UXFIO_BUFTYPE_DYNAMIC_MEM = (1 << 14)
    UINFILE_UXFIO_BUFTYPE_FILE = (1 << 15)
    UINFILE_DETECT_UNRPM = (1 << 16)
    UINFILE_DETECT_UNCPIO = (1 << 17)
    UINFILE_DETECT_DEB_DATA = (1 << 18)
    UINFILE_DETECT_DEB_CONTROL = (1 << 19)
    UINFILE_DETECT_DEB_CONTEXT = (1 << 20)
    UINFILE_DETECT_SLACK = (1 << 21)
    UINFILE_DETECT_RECOMPRESS = (1 << 22)
    LENGTH_OF_MAGIC = 3

    # this order is the same as the arf_format enumeration in GNU cpio
    UNKNOWN_FILEFORMAT = 0  # arf_unknown
    BINARY_FILEFORMAT = 1  # arf_binary
    CPIO_POSIX_FILEFORMAT = 2  # arf_oldascii
    CPIO_NEWC_FILEFORMAT = 3  # arf_newascii
    CPIO_CRC_FILEFORMAT = 4  # arf_crcascii
    TAR_FILEFORMAT = 5  # arf_tar
    USTAR_FILEFORMAT = 6  # 'arf_ustar'
    HP_OLDASCII_FILEFORMAT = 7  # arf_hpoldascii
    HP_BINARY_FILEFORMAT = 8  # arf_hpbinary
    RPMRHS_FILEFORMAT = 9  # RedHat Format
    UINFILE_FILESYSTEM = 10  # the file is a directory in the file system.
    DEB_FILEFORMAT = 11  # debian package format
    SLACK_FILEFORMAT = 12  # slackware package format
    PLAIN_TARBALL_SRC_FILEFORMAT = 13  # common free software source tarball
    UINFILE_FILELAYOUT_NA = 100  # not applicable
    UINFILE_FILELAYOUT_IEEE = 101  # IEEE 1387.2 layout
    UINFILE_FILELAYOUT_UNKNOWN = 102  # Generic Tar or cpio archive
    UINFILE_NEEDDEBUG = False
    MIN_PEEK_LEN = 3

uinc = UinfileConstants


class UINFORMAT:
    def __init__(self):
        self.fd = 0
        self.underlying_fd = 0
        self.type = 0
        self.type_revision = [0] * 12
        self.ztype = 0
        self.layout_type = 0
        self.did_dupe = 0
        self.file_hdr = None
        self.pidlist = [0] * 20
        self.verbose = 0
        self.has_leading_slash = 0
        self.swpath = None
        self.taru = None
        #       self.blockmask_ = signal.sigset_t()
        #       self.defaultmask_ = signal.sigset_t()
        self.blockmask_ = signal.sigpending()
        self.defaultmask_ = signal.sigpending()
        self.current_pos_ = 0
        self.deb_file_fd_ = 0
        self.deb_peeked_bytes = [0] * 12
        self.n_deb_peeked_bytes = 0
        self.slackheader = None
        self.pathname_prefix = ""
        self.slack_name = ""
        self.recompress_commands = None

def memcmp(s1, s2, num):
    libc = ctypes.CDLL("libc.so.6")
    return libc.memcmp(s1, s2, num)

def memcpy(dest, source, size):
    libc = ctypes.CDLL("libc.so.6")
    return libc.memcpy(dest, source, size)

def uinfile_i_open(filename, oflag, mode, uinformat, oflags, xdupfd, uxfio_buffer_type, slack_name):
    pipe_fd = [0 for _ in range(2)]
    zpipe = [0 for _ in range(2)]
    tar_hdr_sum = 0
    xbuffer = str(['\0' for _ in range(1025)])
    gzmagic = UinC.UINFILE_MAGIC_gz
    Zmagic = UinC.UINFILE_MAGIC_Z
    rpmmagic = UinC.UINFILE_MAGIC_rpm
    bz2magic = UinC.UINFILE_MAGIC_bz2
    lzmamagic = UinC.UINFILE_MAGIC_lzma
    xzmamagic = UinC.UINFILE_MAGIC_xz
    debmagic = UinC.UINFILE_MAGIC_ar #"debian-binary"
    zret = 0
    deb_gz_size = 0
    deb_gz_offset = 0
    val1 = 0
    val2 = 0
    #
    #	if (slack_name)
    #		*-*sys.stderr.write("JL you're a SLACKER: %s:%s at line %d\n", fr.__FILE__, __FUNCTION__, fr.__LINE__)
    #	
    print("BEGIN ****************************** ")
    print("xdupfd = %d", xdupfd)
    print("slack_name = %s", slack_name.arg_value)

    dodebcontrol = oflags & (1 << 19)
    dodebcontext = oflags & (1 << 20)
    dodebdata = oflags & (1 << 18)
    do_recompress = oflags & (1 << 22)

    print("dodebdata=%d", dodebdata)
    print("dodebcontext=%d", dodebcontext)
    print("dodebcontrol=%d", dodebcontrol)

    forcetar = oflags & (1 << 4)
    doieee = oflags & (1 << 11)
    doarb = oflags & (1 << 12)
    dounrpminstall = oflags & (1 << 16)
    douncpio = oflags & (1 << 17)
    donative = oflags & (1 << 9)

    print("forcetar=%d", forcetar)
    print("donative=%d", donative)
    print("dounrpminstall=%d", dounrpminstall)

    if dodebdata != 0:
        pass
        # Special case, the uinfile struct is already created 
    else:
        uinformat[0] = UINFORMAT()
        if not uinformat[0]:
            return -1
        uinformat.underlying_fd = -1
        uinformat.current_pos_ = 0
        uinformat.file_hdr=ahsstaticcreatefilehdr()
        uinformat.verbose = 0
        uinformat.has_leading_slash = 0
        uinformat.ztype = UinC.UINFILE_COMPRESSED_NA
        uinformat.type = UinC.UNKNOWN_FILEFORMAT
        ctypes.cdll.memset(uinformat.type_revision, '\0', sys.getsizeof(uinformat.type_revision))
        uinformat.swpath = swpath_open("")
        uinformat.taru = taru_create()
        uinformat.slackheader = None
        print("setting deb_file_fd = -1")
        uinformat.deb_file_fd_ = -1
        uinformat.n_deb_peeked_bytes = 0
        uinformat.pathname_prefix = None
        uinformat.recompress_commands = vplob_open()
        if slack_name.arg_value:
            uinformat.slack_name = slack_name.arg_value
        else:
            uinformat.slack_name = None
        pysigset.sigemptyset(uinformat.blockmask_)
        pysigset.sigemptyset(uinformat.defaultmask_)
        pysigset.sigaddset(uinformat.blockmask_, signal.SIGALRM)
        pysigset.sigaddset(uinformat.defaultmask_, signal.SIGTERM)
        pysigset.sigaddset(uinformat.defaultmask_, signal.SIGINT)
        pysigset.sigaddset(uinformat.defaultmask_, signal.SIGPIPE)
        i = 0
        while i < int((len(uinformat.pidlist))):
            uinformat.pidlist[i] = 0
            i += 1

    # ------------------------------------------------------------- 
    #            Open file or pipe                                  
    # ------------------------------------------------------------- 

    if filename.arg_value == str(None) or (not strcmp(filename.arg_value, "-")):
        print("HERE PIPE")

        if filename.arg_value == str((None)):
            print("HERE")
            dupfd = xdupfd
        else:
            print("HERE")
            dupfd = sys.stdin.fileno()
        print("HERE dupfd = %d", dupfd)

        val1 = 1 if dupfd < uxfioc.UXFIO_FD_MIN else 0
        if dupfd < uxfioc.UXFIO_FD_MIN:
            print("HERE:  lseek\'ing on dupfd")
            val2 = os.lseek(dupfd, 0, os.SEEK_CUR)
        else:
            val2 = -1

        if val1 != 0 and val2 == -1:
            print("HERE %d %s" + os.strerror(val2))
            if os.strerror(val2) == errno.ESPIPE:
                #				
                # unseekable pipe
                #				 
                #				
                # print("HERE\n%s", uxfio_dump_string(fd))
                #				
                fd = uxfio_opendup(dupfd, uxfio_buffer_type)
                #				
                # print("HERE\n%s", uxfio_dump_string(fd))
                #				
                if fd < 0:
                    sys.stderr.write("uinfile.c: uxfio_opendup failed\n")
                    return -1
            else:
                sys.stderr.write("uinfile: fatal error")
                return -1
        else:
            print("HERE")
            ret = val2 #lseek(dupfd, 0L, SEEK_CUR);
            if ret >= 0:
                print("HERE current_pos_=%d", int((uinformat.current_pos_)))
                uinformat.current_pos_ = ret
                #				
                #				if ((*uinformat)->current_pos_) {
                #					sys.stderr.write("uinfile.c: warning: current_pos_ not equal to zero\n")
                #				}
                #
            else:
                #
                #				sys.stderr.write("uinfile.c: warning: lseek failed\n")
                #
                pass
            fd = dupfd
        uinformat.did_dupe=1
    else:
        print("HERE REG FILE")
        fd = os.open(filename.arg_value, os.O_RDONLY, 0)
        if fd < 0:
            print("HERE error.\n")
            return fd
        dupfd = fd
        uinformat.did_dupe=0
    refd = fd
    uinformat.underlying_fd = refd
    def label_reread():
        uinformat.fd = refd
        if (uinformat[0]).typeM == UinC.DEB_FILEFORMAT and dodebdata == 0:
            # FIXME, should go through handle_return()
            return refd

    #
    # Initial read on the serial access archive.
    # Fixme:
    #
    if dodebdata != 0:
        print("HERE")
        peeklen = ArConst.SARHDR + 1 + len(UinC.UINFILE_MAGIC_gz) # for gz magic
    elif doarb == 0:
        print("HERE")
        peeklen = UinC.UINFILE_I_INIT_READ_SIZE
    else:
        #
        # this is used if DETECT_ABRITRARY_DATA is set
        #
        print("HERE")
        peeklen = UinC.MIN_PEEK_LEN

    ctypes.cdll.memset(xbuffer, '\0', ctypes.sizeof(xbuffer))

    print("HERE refd=%d  peeklen=%d", refd, peeklen)
    if doarb != 0:
        # arbitray data, make no requirement that it be longer than MIN_PEEK_LEN
        if (ret:=uxfio_read(refd, xbuffer, peeklen)) < 0:
            print("ERROR HERE")
            sys.stderr.write("%s: error in read of arbitrary data: request=%d return=%d\n" +swlib_utilname_get()+ peeklen+ ret)
            return -1
    else:
        if (ret:=taru_tape_buffered_read(refd, xbuffer, peeklen)) != peeklen:
            print("ERROR HERE")
            if ret <= 0:
                sys.stderr.write("%s: error in initial read. request=%d return=%d\n"+ swlib_utilname_get()+ peeklen+ ret)
            return -1
    print("HERE")

    #
    # Identify the file type.
    #
    if doarb == 0:
        # Peeked Length is  UINFILE_I_INIT_READ_SIZE
        #if False:
        #    pass
        if dodebdata != 0:
            # Peeked length is (ArConst.SARHDR + 1 + strlen(UINFILE_MAGIC_gz))
            print("in dodebdata")
            # the ar header of data.tar.gz should be in xbuffer
            print("type: DEB (data)")
            ret = ar_get_size(ord(xbuffer), val1)
            if ret != 0:
                #				 this may be caused by alignment byte padding in the ar format,
                #				   so try it again, if it fails again only then your screwed.
                ret = ar_get_size(ord(xbuffer)+1, val1)
                if ret != 0:
                    sys.stderr.write("%s: ar archive error %s: at line %d\n"+ swlib_utilname_get()+ fr.__FILE__+ fr.__LINE__)
                    return -1
                print("in dodebdata with 1 byte of padding")
                deb_gz_offset = ArConst.SARHDR + 1
                #				 for example "\n\037\213\b"
                #				use offset of deb_gz_offset - ArConst.SARHDR with write size of 3

                uinformat.n_deb_peeked_bytes = len(UinC.UINFILE_MAGIC_gz)

            else:
                print("in dodebdata with no padding")
                deb_gz_offset = ArConst.SARHDR
                #				 for example "\037\213\b\xNN"
                #				use offset of deb_gz_offset - ArConst.SARHDR with write size of 4

                uinformat.n_deb_peeked_bytes = len(UinC.UINFILE_MAGIC_gz) + 1
                pass
            ctypes.cdll.memcpy((uinformat.deb_peeked_bytes), xbuffer + ArConst.SARHDR, peeklen - ArConst.SARHDR)

            deb_gz_size = val1
            print("deb_gz_size is %d", val1)
            ret = uxfio_lseek(refd, (0), os.SEEK_CUR)
            if ret < 0:
                sys.stderr.write("%s: ar archive error %s: at line %d\n"+ swlib_utilname_get()+ fr.__FILE__+ fr.__LINE__)
                return -1
            uinformat.current_pos_ = ret
            print("current_pos_ is %d", ret)

            # End of dodebdata
        elif (not memcmp(xbuffer, rpmmagic, 4)) and forcetar == 0:
            print("type: RPM")
            if dounrpminstall == 0 and doieee == 0:
                #
                # This is the old historic response to
                # an RPM file.
                #
                uinformat.type=UinC.RPMRHS_FILEFORMAT
                ret = uinfile_handle_return(uinformat[0], oflags, uxfio_buffer_type)
                #
                #				print("HERE\n%s", uxfio_dump_string(ret))
                #
                return ret
            elif dounrpminstall != 0:
                #
                # Model the RPM as a compression type since this
                #  fits with the code below, e.g. instead of calling
                # ''gzip -d'', call ''swpackage --unrpm''
                #
                uinformat.ztype = UinC.UINFILE_COMPRESSED_RPM
            elif doieee != 0 and dounrpminstall == 0:
                sys.stderr.write("%s: try using the --allow-rpm or --any-format options\n"+ swlib_utilname_get())
                return -1
            else:
                print("")
                return -1
        elif douncpio != 0 and ((strncmp(xbuffer, "0707", 4) == 0) and (1) != 0):
            #
            # Model the cpio format as a compression type since this
            # fits with the code below, e.g. instead of calling
            # ''gzip -d'', call ''arf2arf -H ustar''
            #
            print("type: 0707")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_CPIO
        elif (not strncmp(xbuffer, "0707", 4)) and (1) != 0 and forcetar == 0:
            if not strncmp(xbuffer, "070702", 6):
                print("type: 070702")
                uinformat.type = UinC.CPIO_CRC_FILEFORMAT
            elif not strncmp(xbuffer, "070701", 6):
                print("type: 070701")
                uinformat.type = UinC.CPIO_NEWC_FILEFORMAT
            elif not strncmp(xbuffer, "070707", 6):
                print("type: 070707")
                uinformat.type = UinC.CPIO_POSIX_FILEFORMAT
            else:
                print("")
                return -1
            uinformat.has_leading_slash = determine_if_has_leading_slash(uinformat[0], xbuffer)
            ret = uinfile_handle_return(uinformat[0], oflags, uxfio_buffer_type)
            if ret > 0 and doieee != 0:
                ret = uinfile_detect_ieee(uinformat[0], oflags)
            return ret
        elif not memcmp(xbuffer, gzmagic, 2):
            # gzipped compression
            print("type: COMPRESSED_GZ")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_GZ
        elif not memcmp(xbuffer, bz2magic,UinC.LENGTH_OF_MAGIC):
            # bz2 compression
            print("type: COMPRESSED_BZ2")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_BZ2
        elif not memcmp(xbuffer, lzmamagic,UinC.LENGTH_OF_MAGIC):
            # LZMA compression
            print("type: COMPRESSED_LZMA")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_LZMA
        elif not memcmp(xbuffer, xzmamagic,UinC.LENGTH_OF_MAGIC):
            # XZ compression
            print("type: COMPRESSED_XZ")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_XZ
        elif not memcmp(xbuffer, Zmagic, 2):
            # Unix compress
            print("type: COMPRESSED_Z")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_Z
        elif is_gpg_data(xbuffer):
            print("type: GPG Encrypted")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_GPG
        elif (memcmp(xbuffer, UinC.UINFILE_MAGIC_gpg_armor, len(UinC.UINFILE_MAGIC_gpg_armor)) == 0
              or is_gpg_packet(str(xbuffer)) or False):
            detect_text = None
            detect_text = run_gpg_list_packets_command(ord(xbuffer), peeklen)
            if detect_text:
                strob_close(detect_text)
            print("type: GPG Encrypted")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_GPG
        #elif donative == 0 and dounrpminstall == 0 and memcmp(xbuffer, debmagic, len(UinC.UINFILE_MAGIC_ar "debian-binary")) == 0:
        elif donative == 0 and dounrpminstall == 0 and memcmp(xbuffer, debmagic, len(UinC.UINFILE_MAGIC_ar)) == 0:

            print("type: DEB")
            # Great, its a deb package

            uinformat.ztype = UinC.UINFILE_COMPRESSED_DEB
            uinformat.type = UinC.DEB_FILEFORMAT
            print("setting deb_file_fd = %d"+ dupfd)
            uinformat.deb_file_fd_ = dupfd

            #			 Store the 2.0\n which is the data of the first
            #			   archive member

            uinformat.type_revision = str((str(xbuffer)))[68:72]
           # uinformat.type_revision[4] = '\0'

            #			 Now read some more bytes to include the gzip magic of
            #			   the control.tar.gz file

            ret = ar_get_size(ord(xbuffer)+8, val1)
            if ret != 0 or val1 != 4:
                sys.stderr.write("%s: deb format bad size value\n"+ swlib_utilname_get())
                return -1

            # Now we know for certain where the control.tar.gz file begins,
            # make sure we have peek'ed that far into the file and confirm
            # the gz magic is there

            deb_gz_offset = ArConst.SARMAG + ArConst.SARHDR + val1 + ArConst.SARHDR # val1 must be 4
            if peeklen < deb_gz_offset + UinC.LENGTH_OF_MAGIC:

                # read further into the file
                ret = taru_tape_buffered_read(refd, xbuffer[peeklen:], deb_gz_offset+UinC.LENGTH_OF_MAGIC - peeklen)
                if ret != deb_gz_offset+UinC.LENGTH_OF_MAGIC - peeklen:
                    sys.stderr.write("%s: error in second initial read. request=%d return=%d\n"+ swlib_utilname_get() +peeklen+ ret)
                    return -1
                peeklen += ((deb_gz_offset+UinC.LENGTH_OF_MAGIC) - peeklen)

            # look for gzmagic

            if memcmp((ord(xbuffer))+deb_gz_offset, gzmagic,UinC.LENGTH_OF_MAGIC) != 0 and memcmp((ord(xbuffer))+deb_gz_offset, xzmamagic,UinC.LENGTH_OF_MAGIC) != 0:
                sys.stderr.write("%s: neither gz or xz magic found for control.tar.gz\n"+ swlib_utilname_get())
                return -1

            # Now get the size of the compressed control.tar.gz file

            ret = ar_get_size(ord(xbuffer) + ArConst.SARMAG + ArConst.SARHDR + val1, val2)
            if ret != 0:
                sys.stderr.write("%s: bad size conversion\n"+ swlib_utilname_get())
                return -1

            if uxfio_lseek(refd, deb_gz_offset, os.SEEK_SET) != deb_gz_offset:
                sys.stderr.write("%s: %s: uxfio_lseek error. 001.3 pos=%d\n"+ swlib_utilname_get()+ fr.__FILE__+ (uinformat.current_pos_))
                return -1
            # this is important
            deb_gz_size = val2

            uinformat.current_pos_ = deb_gz_offset

            # Now continue as a compressed file
            pass
        elif doieee == 0 and donative == 0 and dodebcontext == 0 and dodebcontrol == 0 and dounrpminstall == 0 and dodebdata == 0 and does_have_name_version(xbuffer):
           # st = stat() # stat() is not callable
            st = None
            xeoa = 0

            print("type: plain source tarball")
            print("")
            tmp = strob_open(32)

            # read further into the file
            ret = taru_tape_buffered_read(refd, xbuffer[peeklen:], sys.getsizeof(xbuffer) - peeklen)
            if ret != int(sys.getsizeof(xbuffer)) - int(peeklen):
                return -1
            peeklen += (sys.getsizeof(xbuffer) - peeklen)

            print("ret=%d", ret)

            ret = taru_read_in_tar_header2(uinformat.taru, uinformat.file_hdr, -1, xbuffer, xeoa, 0, tc.TARRECORDSIZE)
            if ret != tc.TARRECORDSIZE:
                return -1

            #			 xbuffer now contains what should be the leading path prefix of the
            #			   plain tarball

            taru_print_tar_ls_list(tmp, uinformat.file_hdr, ls.LS_LIST_VERBOSE_L2)

            uinformat.slackheader = bytearray(tc.TARRECORDSIZE)
            if (uinformat[0]).slackheader is None:
                return -1
            memcpy(uinformat.slackheader, xbuffer, tc.TARRECORDSIZE)

            path_prefix = strob_open(32)
            print("")

            # read the entire package into the buffered file descriptor
            while (ret:=uxfio_read(refd, xbuffer, tc.TARRECORDSIZE)) > 0:
                pass
            if ret < 0:
                print("read error")
                return -1

            # now seek back to beginning
            print("")

            ret = uxfio_lseek(refd, 0, os.SEEK_SET)
            if ret != 0:
                print("")
                return -1

            print("")
            # Now read the package to determine the path prefix
            swvarfs = swvarfs_opendup(refd, (1 << 9), 0)
            print("")
            if swvarfs is None:
                print("")
                print("")
                return -1
            print("")
            found = 0
            while (name:=swvarfs_get_next_dirent(swvarfs, st)) is not None and len(name) != 0:
                #				 While we are reading the files, store the leading path prefix
                #				   and determine if it is the same for every file.
                if path_prefix is not None and strob_strlen(path_prefix) < 4:
                    strob_strcpy(path_prefix, name)
                    swlib_unix_dirtrunc(path_prefix)
                if path_prefix is not None and strob_strlen(path_prefix) >= 4 and strstr(name, strob_str(path_prefix)) != name:
                    strob_close(path_prefix)
                    path_prefix = None
                print("||||||| [%s]", name)

            print("")
            swvarfs_close(swvarfs)
            ret = uxfio_lseek(refd, 0, os.SEEK_SET)
            if ret != 0:
                print("")
                return -1
            uinformat.ztype = UinC.UINFILE_COMPRESSED_NOT
            uinformat.type = UinC.PLAIN_TARBALL_SRC_FILEFORMAT
            ret = uinfile_handle_return(uinformat[0], (1 << 6), uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
            print("returning fd=%d", ret)
            if path_prefix is not None and strob_strlen(path_prefix) < 4:
                strob_close(path_prefix)
                path_prefix = None
            if path_prefix:
                # sys.stderr.write("path prefix = [%s]\n", strob_str(path_prefix));
                uinformat.pathname_prefix = strob_str(path_prefix)
                strob_close(path_prefix)
            else:
                sys.stderr.write("%s: no constant leading pathname prefix in tarball\n"+ swlib_utilname_get())
                return -1
            print("returning ret=%d", ret)
            strob_close(tmp)
            return ret

        elif slack_name.arg_value and doieee == 0 and donative == 0 and dodebcontext == 0 and dodebcontrol == 0 and dodebdata == 0 and dounrpminstall == 0 and memcmp(xbuffer, UinC.UINFILE_MAGIC_binary_tarball, len(UinC.UINFILE_MAGIC_binary_tarball)) == 0:
            print("type: binary_tarball, probably slackware: slack_name=%s"+ slack_name.arg_value)

            #			 Here we know it could be slackware binary package
            #			   this code path supports the --slackware-pkg-name=NAME option of swpackage
            #			   and lxpsf and the swinstall case of ''swinstall --slackware -s:slackage-binary-tarball''
            #			   No other cases take this code path, If we are here we punt to swpackage instead of
            #			   returning in this process

            uinformat.ztype = UinC.UINFILE_COMPRESSED_SLACK_WITH_NAME
            sys.stderr.write("JL you're a SLACKER: %s:%s at line %d\n"+ fr.__FILE__+ fr.__FUNCTION__+ fr.__LINE__)

        elif doieee == 0 and donative == 0 and dodebcontext == 0 and dodebcontrol == 0 and dodebdata == 0 and dounrpminstall == 0 and memcmp(xbuffer, UinC.UINFILE_MAGIC_binary_tarball, len(UinC.UINFILE_MAGIC_binary_tarball)) == 0:
            xeoa = 0
            print("type: binary_tarball")
            print("")

            # read further into the file
            ret = taru_tape_buffered_read(refd, xbuffer[peeklen:], sys.getsizeof(xbuffer) - peeklen)
            if ret != int(sys.getsizeof(xbuffer)) - int(peeklen):
                return -1
            print("ret=%d", ret)
            peeklen += (sys.getsizeof(xbuffer) - peeklen)


            #			 This will save the first archive member which is "./", we need the
            #			   permissions on this to create in the sw POSIX archive
            ret = taru_read_in_tar_header2(uinformat.taru, uinformat.file_hdr, -1, xbuffer, xeoa, 0, tc.TARRECORDSIZE)
            if ret != tc.TARRECORDSIZE:
                return -1

            if UinC.UINFILE_MAGIC_slack:

                # This was too easy,
                # the "install/" directory was at the front of the package

                print("")
                uinformat.ztype = UinC.UINFILE_COMPRESSED_NOT
                uinformat.type = UinC.SLACK_FILEFORMAT

                ret = uinfile_handle_return(uinformat[0], (1 << 6), uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
                return ret
            else:
                # st = stat() # stat() is not callable
                st = refd
                print("Looking for install directory")
                # The only binary tarball recognized is slackware

                #				 This code path handles the ./install directory placed anywhere
                #				   in the archive.

                # read the entire package into the buffered file descriptor
                while (ret:=uxfio_read(refd, xbuffer, tc.TARRECORDSIZE)) > 0:
                    pass
                if ret < 0:
                    print("read error")
                    return -1

                # now seek back to beginning

                ret = uxfio_lseek(refd, 0, os.SEEK_SET)
                if ret != 0:
                    print("")
                    return -1

                #				 Now read the package and look for the install directory which
                #				   may be all the way at the end
                swvarfs = swvarfs_opendup(refd, (1 << 9), 0)
                print("")
                if swvarfs is None:
                    print("")
                    return -1
                print("")
                found = 0
                while (name:=swvarfs_get_next_dirent(swvarfs, st)) is not None and len(name) != 0:
                    # While we are reading the files, store the leading path prefix
                    # and determine if it is the same for every file.
                    print("||||||| [%s]", name)
                    # Look for the slackware control directory "install/", if we find
                    # one assume it is slackware.
                    # Also, need to look for ./install/ since some packagers
                    # prepend a ./
                    #
                    if name == UinC.UINFILE_MAGIC_slack+"/" or name == "./"+UinC.UINFILE_MAGIC_slack+"/":
                        print("found install directory")
                        found = 1
                        break

                print("")

                ret = uxfio_lseek(refd, 0, os.SEEK_SET)
                if ret != 0:
                    print("")
                    return -1

                swvarfs_close(swvarfs)
                if found != 0:
                    print("FOUND SLACK")
                    uinformat.ztype = UinC.UINFILE_COMPRESSED_NOT
                    uinformat.type = UinC.SLACK_FILEFORMAT
                    ret = uinfile_handle_return(uinformat[0], (1 << 6), uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
                else:
                    print("error")
                    ret = -1
                print("returning fd=%d", ret)
                return ret
        else:
            tar_hdr = None
            print("type: arbitrary tar")
            if (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_NA:
                uinformat.ztype = UinC.UINFILE_COMPRESSED_NOT

            # Read further into the file
            if taru_tape_buffered_read(refd, xbuffer[peeklen:], 512 - peeklen) != (512 - peeklen):
                return -1
            peeklen += (512 - peeklen)

            tar_hdr = xbuffer
            taru_otoul(tar_hdr.chksum, tar_hdr_sum)

            if strncmp(xbuffer[257:], "ustar", 5) == 0 or tar_hdr_sum == taru_tar_checksum(xbuffer):
                # uncompressed tar
                #
                # This could be a deb format package
                #
                if (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_DEB:
                    print("Found Compressed DEB")
                    uinformat.type=UinC.DEB_FILEFORMAT
                    uinformat.has_leading_slash = 0 # NO
                    ret = uinfile_handle_return(uinformat[0], oflags, uxfio_buffer_type)
                else:
                    print("Found USTAR_FILEFORMAT")
                    uinformat.type=UinC.USTAR_FILEFORMAT
                    uinformat.has_leading_slash = determine_if_has_leading_slash(uinformat[0], xbuffer)
                    ret = uinfile_handle_return(uinformat[0], oflags, uxfio_buffer_type)
                    if ret > 0 and doieee != 0:
                        ret2 = 0
                        ret2 = uinfile_detect_ieee(uinformat[0], oflags)
                        if ret2 < 0 and dounrpminstall != 0:
                            uinformat.ztype = UinC.UINFILE_COMPRESSED_RPM
                        elif ret2 < 0:
                            ret = ret2
                        else:
                            ret = ret2
                #
                #				print("HERE\n%s", uxfio_dump_string(ret))
                #
                return ret
            else:
                print("TAR")
                if dounrpminstall != 0:
                    # Try to let swpackage decode it
                    print("Setting ztype to UINFILE_COMPRESSED_RPM")
                    uinformat.ztype = UinC.UINFILE_COMPRESSED_RPM
                else:
                    uxfio_close(refd)
                    del uinformat
                    print("Unrecognized tar layout")
                    sys.stderr.write("%s: error: uinfile: unrecognized tar variant format\n", swlib_utilname_get())
                    return -1
            # }
            # arbitrary tar
    else:
        # Peeked Length is  MIN_PEEK_LEN
        print("")
        if not memcmp(xbuffer, gzmagic, 2):
            # gzipped compression
            print("")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_GZ
        elif not memcmp(xbuffer, bz2magic,UinC.LENGTH_OF_MAGIC):
            # bz2 compression
            print("")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_BZ2
        elif not memcmp(xbuffer, lzmamagic,UinC.LENGTH_OF_MAGIC):
            # LZMA compression
            print("")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_LZMA
        elif not memcmp(xbuffer, xzmamagic,UinC.LENGTH_OF_MAGIC):
            # XZ compression
            print("")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_XZ
        elif not memcmp(xbuffer, Zmagic, 2):
            # Unix compress
            print("")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_Z
        elif is_gpg_data(xbuffer):
            print("type: GPG Encrypted")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_GPG
        elif memcmp(xbuffer, UinC.UINFILE_MAGIC_gpg_armor, len(UinC.UINFILE_MAGIC_gpg_armor)) == 0 or is_gpg_packet(ord(xbuffer)) or False:
            print("type: GPG Encrypted")
            detect_text = None
            detect_text = run_gpg_list_packets_command(ord(xbuffer), peeklen)
            if detect_text:
                strob_close(detect_text)
            print("type: GPG Encrypted")
            uinformat.ztype = UinC.UINFILE_COMPRESSED_GPG
        else:
            print("")
            ret = uinfile_handle_return(uinformat[0], oflags, uxfio_buffer_type)
            print("ret=%d", ret)
            return ret

    # -------------------------------------------------------------
    #     If we're here then it must be a compressed file.
    #     -or- its an RPM and UINFILE_DETECT_RPM is ON.
    # -------------------------------------------------------------

    print("HERE its compressed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    unzip = [None for _ in range(2)]

    unzip[0] = None
    unzip[1] = None

    print("HERE current_pos_=%d", int((uinformat.current_pos_)))
    if dodebdata == 0:
        if uxfio_lseek(refd, (uinformat.current_pos_), os.SEEK_SET) != (uinformat.current_pos_):
            sys.stderr.write("%s: %s: uxfio_lseek error. 001.1 pos=%d\n", swlib_utilname_get(), fr.__FILE__, (uinformat.current_pos_))
            return -1
    if (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_Z:
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], "compress")
        SHCMD.shcmd_add_arg(unzip[0], "-cd")
        if do_recompress != 0:
            print("recompress: compress")
            rezip = SHCMD.shcmd_open()
            print("recompress: xz")
            SHCMD.shcmd_add_arg(rezip, "compress")
            SHCMD.shcmd_add_arg(rezip, "-c")
            vplob_add(uinformat.recompress_commandsM, rezip)
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_DEB:

        # Do not record the recompress_commands for DEB packages

        print("in deb code")
        if memcmp((str((xbuffer))) + deb_gz_offset, gzmagic,UinC.LENGTH_OF_MAGIC) == 0:
            unzip[0] = SHCMD.shcmd_open()
            SHCMD.shcmd_add_arg(unzip[0], "gzip")
            SHCMD.shcmd_add_arg(unzip[0], "-d")
        elif memcmp((str((xbuffer))) + deb_gz_offset, bz2magic,UinC.LENGTH_OF_MAGIC) == 0:
            #
            #			 * preemptive support for bzip2'ed debian packages
            #
            unzip[0] = SHCMD.shcmd_open()
            SHCMD.shcmd_add_arg(unzip[0], "bzip2")
            SHCMD.shcmd_add_arg(unzip[0], "-d")
        elif memcmp((str((xbuffer))) + deb_gz_offset, lzmamagic,UinC.LENGTH_OF_MAGIC) == 0:
            unzip[0] = SHCMD.shcmd_open()
            SHCMD.shcmd_add_arg(unzip[0], "lzma")
            SHCMD.shcmd_add_arg(unzip[0], "-d")
        elif memcmp((str((xbuffer))) + deb_gz_offset, xzmamagic,UinC.LENGTH_OF_MAGIC) == 0:
            unzip[0] = SHCMD.shcmd_open()
            SHCMD.shcmd_add_arg(unzip[0], "xz")
            SHCMD.shcmd_add_arg(unzip[0], "-d")
        else:
            #
            #			 * error, the compressed magic not found at the expected offset
            #
            sys.stderr.write("%s: unsupported compression in a .deb file %s: at line %d\n", swlib_utilname_get(), fr.__FILE__, fr.__LINE__)
            return -1
        #
        #		 * The file is now positioned at the control.tar.gz file
        #
        print("leaving deb code")
        pass # continue
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_GZ:
        print("in gzip code")
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], "gzip")
        SHCMD.shcmd_add_arg(unzip[0], "-d")
        if do_recompress != 0:
            print("recompress: gzip")
            rezip = SHCMD.shcmd_open()
            SHCMD.shcmd_set_exec_function(rezip, "execvp")
            SHCMD.shcmd_add_arg(rezip, "gzip")
            SHCMD.shcmd_add_arg(rezip, "-c")
            SHCMD.shcmd_add_arg(rezip, "-9")
            vplob_add(uinformat.recompress_commandsM, rezip)
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_BZ2:
        print("in bzip code")
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], "bzip2")
        SHCMD.shcmd_add_arg(unzip[0], "-d")

        if do_recompress != 0:
            print("recompress: bzip2")
            rezip = SHCMD.shcmd_open()
            SHCMD.shcmd_set_exec_function(rezip, "execvp")
            SHCMD.shcmd_add_arg(rezip, "bzip2")
            SHCMD.shcmd_add_arg(rezip, "-c")
            vplob_add(uinformat.recompress_commandsM, rezip)
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_LZMA:
        print("in lzma code")
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], "lzma")
        SHCMD.shcmd_add_arg(unzip[0], "-d")

        if do_recompress != 0:
            print("recompress: lzma")
            rezip = SHCMD.shcmd_open()
            SHCMD.shcmd_set_exec_function(rezip, "execvp")
            SHCMD.shcmd_add_arg(rezip, "lzma")
            SHCMD.shcmd_add_arg(rezip, "-z")
            SHCMD.shcmd_add_arg(rezip, "-c")
            vplob_add(uinformat.recompress_commandsM, rezip)
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_XZ:
        print("in xz code")
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], "xz")
        SHCMD.shcmd_add_arg(unzip[0], "-d")

        if do_recompress != 0:
            rezip = SHCMD.shcmd_open()
            print("recompress: xz")
            SHCMD.shcmd_set_exec_function(rezip, "execvp")
            SHCMD.shcmd_add_arg(rezip, "xz")
            SHCMD.shcmd_add_arg(rezip, "-z")
            SHCMD.shcmd_add_arg(rezip, "-c")
            vplob_add(uinformat.recompress_commandsM, rezip)

    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_GPG:
        print("in gpg code")
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], g.SW_GPG_BIN)
        SHCMD.shcmd_add_arg(unzip[0], "--decrypt")
        SHCMD.shcmd_add_arg(unzip[0], "--use-agent")
        SHCMD.shcmd_add_arg(unzip[0], "--no-verbose")
        SHCMD.shcmd_add_arg(unzip[0], "-o")
        SHCMD.shcmd_add_arg(unzip[0], "-")
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_CPIO:
        print("in unrpm code")
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], g.SW_BIN_DIR, "/arf2arf")
        SHCMD.shcmd_add_arg(unzip[0], "-H")
        SHCMD.shcmd_add_arg(unzip[0], "ustar")
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_SLACK_WITH_NAME:
        print("in ", UinC.UINFILE_COMPRESSED_SLACK_WITH_NAME)
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], g.SW_BIN_DIR, "/swpackage")
        SHCMD.shcmd_add_arg(unzip[0], "--to-swbis")
        SHCMD.shcmd_add_arg(unzip[0], "-s")
        SHCMD.shcmd_add_arg(unzip[0], "-")
        SHCMD.shcmd_add_arg(unzip[0], "--slackware-pkg-name")
        SHCMD.shcmd_add_arg(unzip[0], slack_name.arg_value)
        SHCMD.shcmd_add_arg(unzip[0], "--catalog-owner=0")
        SHCMD.shcmd_add_arg(unzip[0], "--catalog-group=0")
        SHCMD.shcmd_add_arg(unzip[0], "@-")
    elif (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_RPM:
        print("in unrpm code")
        unzip[0] = SHCMD.shcmd_open()
        SHCMD.shcmd_add_arg(unzip[0], g.SW_BIN_DIR, "/swpackage")
        SHCMD.shcmd_add_arg(unzip[0], "--to-swbis")
        SHCMD.shcmd_add_arg(unzip[0], "--catalog-owner=0")
        SHCMD.shcmd_add_arg(unzip[0], "--catalog-group=0")
        SHCMD.shcmd_add_arg(unzip[0], "@-")
    else:
        sys.stderr.write("compression format not supported by uinfile_open.\n")
        return -1

    # --------------------------------------------------------
    #     Uncompress the file
    # --------------------------------------------------------

    if os.pipe() < 0:
        sys.stderr.write("%s"+ os.strerror(refd))
        uxfio_close(refd)
        return -1
    if (pid := swndfork((uinformat.blockmask_), (uinformat.defaultmask_))) != 0:
        # read compressed file
        #
        # Parent
        #
        if pid < 0:
            sys.stderr.write("fork failed.\n")
            uxfio_close(refd)
            return -1
        uinfile_add_pid(uinformat[0], int(pid))
        os.close(pipe_fd[1])
        refd = uxfio_opendup(pipe_fd[0], uxfio_buffer_type)
        #
        #		print("HERE\n%s", uxfio_dump_string(refd))
        #
        #
        #		 * Re-read the alledgedly uncompressed file.
        #
        if dodebdata != 0:
            return refd
        dounrpminstall = 0
        print("GOTO'ING !!!!!!!!!!!!!!!!!!!!!!!!!! ")
        label_reread()
    else:
        #
        #		 * Child
        #
        jlxx = 0
        b_closefd = -1
        argvector = None

        os.close(pipe_fd[0])
        #
        #		 * This is the master child. clear all the pids.
        #
        i = 0
        while i < int((len(uinformat.pidlist))):
            uinformat.pidlist[i] = 0
            i += 1

        if unzip[0] is not None:
            #
            #			* Use the command to uncompress the stream.
            #
            print("HERE ")
            SHCMD.shcmd_set_dstfd(unzip[0], pipe_fd[1])
            zpipe[0] = -1
            zpipe[1] = -1

            #
            #			 * If its a uxfio descriptor then we must fork.
            #
            if True and (refd >= uxfioc.UXFIO_FD_MIN or uxfioc.uinformat.ztype == UinC.UINFILE_COMPRESSED_DEB):
                print("HERE ")
                if (uinformat[0]).ztype == UinC.UINFILE_COMPRESSED_DEB:
                    #
                    # pump the exact size of the control.tar.gz file
                    #
                    print("HERE")
                    pump_amount = int(deb_gz_size)
                else:
                    #
                    #  pump until the end
                    #
                    print("HERE")
                    pump_amount = -1

                os.pipe(zpipe)
                a_pid = swndfork((uinformat.blockmask_), (uinformat.defaultmask_))
                if a_pid > 0:
                    print("HERE ")
                    uxfio_close(refd)
                    os.close(zpipe[1])
                    uinfile_add_pid(uinformat[0], int(a_pid))
                elif a_pid == 0:
                    print("HERE ")
                    os.close(zpipe[0])
                    if (True or uinformat.ztype != UinC.UINFILE_COMPRESSED_DEB) and dodebdata == 0:
                        print("HERE at UXFIO_F_ARM_AUTO_DISABLE")
                        uxfio_fcntl(refd, uxfioc.UXFIO_F_ARM_AUTO_DISABLE, 1)
                        print("HERE current_pos_=%d", int((uinformat.current_pos_)))
                        if uxfio_lseek(refd, (uinformat.current_pos_), os.SEEK_SET) < 0:
                            sys.stderr.write("uxfio_lseek error in child 002.\n")

                    if dodebdata != 0 and uinformat.n_deb_peeked_bytes > 0:
                        # Here we are opening the data.tar.gz file and we must
                        # write the peek'ed bytes to reconstitute the stream
                        if uxfio_write(zpipe[1], (int((uinformat.deb_peeked_bytes)) + deb_gz_offset - ArConst.SARHDR), uinformat.n_deb_peeked_bytes) != uinformat.n_deb_peeked_bytes:
                            sys.stderr.write("%s: ar archive error %s: at line %d\n"+ swlib_utilname_get()+ fr.__FILE__+ fr.__LINE__)
                            sys.exit(1)
                    taru_pump_amount2(zpipe[1], refd, pump_amount - (uinformat.n_deb_peeked_bytes), -1)
                    os.close(zpipe[1])
                    uxfio_close(refd)
                    sys.exit(0)
                else:
                    print("HERE ")
                    sys.stderr.write("%s\n", strerror(errno))
                    sys.exit(29)
                SHCMD.shcmd_set_srcfd(unzip[0], zpipe[0])
                b_closefd = zpipe[0]
                print("HERE ")
            else:
                print("HERE ")
                SHCMD.shcmd_set_srcfd(unzip[0], refd)
                b_closefd = refd
            print("HERE ")
            SHCMD.shcmd_set_exec_function(unzip[0], "execvp")
            swgp_signal(signal.SIGPIPE, signal.SIG_DFL)
            SHCMD.shcmd_cmdvec_exec(unzip)
            jlxx = SHCMD.shcmd_cmdvec_wait(unzip)
            print("HERE ")
            zret = SHCMD.shcmd_get_exitval(unzip[0])
            print("HERE ")
            argvector = SHCMD.shcmd_get_argvector(unzip[0])
            if argvector and argvector[0] != '\0' and strcmp(argvector[0], "gzip") == 0:
                if zret == 2:
                    sys.stderr.write("%s: Warning: gzip exited with a warning.\n", swlib_utilname_get())
                    zret = 63
                elif zret == SHCMD.SHCMD_UNSET_EXITVAL:
                    #
                    #					* This happens when the whole package
                    #					* is not read. such as when writing
                    #					* out only the catalog section.
                    #
                    zret = 0
                elif zret != 0:
                    sys.stderr.write("%s: Error: gzip exited with an error: pid=%d exit value=%d.\n", swlib_utilname_get(), int((unzip[0].pid_)), int(zret))
            else:
                if zret != 0 and zret != SHCMD_UNSET_EXITVAL:
                    sys.stderr.write("%s: decompression process exiting with value %d.\n", swlib_utilname_get(), zret)
        else:
            sys.stderr.write(" uinfile: internal error 84.001\n")

        #
        #		 * Now wait on all the pids.
        #
        if b_closefd >= 0:
            uxfio_close(b_closefd)
        os.close(pipe_fd[1])
        while uinfile_wait_on_all_pid(uinformat[0], os.WNOHANG) < 0:
            time.sleep(1)
        sys.exit(zret) # Child

    if unzip[0]:
        SHCMD.shcmd_close(unzip[0])
# End --  Compressed file.

    #
    #	* The child returns -1, but it never should get here.
    #
    sys.stderr.write(" uinfile: internal error 85.002\n")
    return -1



def uinfile_get_layout_type(uinformat):
    return uinformat.layout_type


def uinfile_open(filename, mode, uinformat, oflags):
    buftype = uinfile_decode_buftype(oflags, uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
    ret = uinfile_i_open(filename, -1, mode, uinformat, oflags, -1, buftype, None)
    return ret
def uinfile_open_with_name(filename, mode, uinformat, oflags, name):
    sys.stderr.write("")
    buftype = uinfile_decode_buftype(oflags, UXFIO_BUFTYPE_DYNAMIC_MEM)
    ret = uinfile_i_open(filename, -1, mode, uinformat, oflags, -1, buftype, name)
    return ret

def uinfile_opendup_with_name(xdupfd, mode, uinformat, oflags, name):
    if name == None:
        return uinfile_opendup(xdupfd, mode, uinformat, oflags)
    buftype = uinfile_decode_buftype(oflags, uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
    ret = uinfile_i_open(None, -1, mode, uinformat, oflags, xdupfd, buftype, name)
    return ret


def uinfile_opendup(xdupfd, mode, uinformat, oflags):
    buftype = uinfile_decode_buftype(oflags, uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
    ret = uinfile_i_open(None, -1, mode, uinformat, oflags, xdupfd, buftype, None)
    return ret


def uinfile_close(uinformat):
    ahsstaticdeletefilehdr(uinformat.file_hdr)
    ret = uinfile_wait_on_all_pid(uinformat, 0)
    if uinformat.verbose:
        print("Leaving uinfile_close with status", ret, file=sys.stderr)
    if uinformat.taru:
        taru_delete(uinformat.taru)
    if uinformat.swpathM:
        swpath_close(uinformat.swpath)
    if uinformat.slack_nameM:
        del uinformat.slack_name
    del uinformat
    return ret


def uinfile_get_ztype(uinformat):
    return uinformat.ztype


def uinfile_get_type(uinformat):
    return uinformat.type


def uinfile_get_swpath(uinformat):
    return uinformat.swpath


def uinfile_get_has_leading_slash(uinformat):
    return uinformat.has_leading_slash


def uinfile_set_type(uinformat, type):
    uinformat.type = type


def uinfile_decode_buftype(oflags, v):
    if oflags & UinC.UINFILE_UXFIO_BUFTYPE_DYNAMIC_MEM:
        v = uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM
    elif oflags & UinC.UINFILE_UXFIO_BUFTYPE_FILE:
        v = uxfioc.UXFIO_BUFTYPE_FILE
    elif oflags & UinC.UINFILE_UXFIO_BUFTYPE_MEM:
        v = uxfioc.UXFIO_BUFTYPE_MEM
    return v


def uinfile_wait_on_pid(uinformat, pid, flag, status):
    ret = 0
    if uinformat.verbose:
        sys.stderr.write("Entering uinfile_wait_on_pid: pid=%d options=%d.\n" % (pid, flag))
    ret = os.waitpid(pid, status, flag)
    if uinformat.verbose:
        sys.stderr.write("uinformat: waitpid(pid=%d, options=%d) returned %d, status=%d\n" % (pid, flag, ret, status))
    if ret > 0:
        if uinformat.verbose:
            sys.stderr.write("uinformat: clearing pid %d\n" % pid)
        uinfile_del_pid(uinformat, pid)
    return ret


def uinfile_wait_on_all_pid(uinformat, flag):
    retval = 0
    ret = 0
    i = 0
    m = 0
    status = 0
    gotgzwarn = 0
    if uinformat.verbose:
        sys.stderr.write("Entering uinfile_wait_on_all_pid\n")
    while i < len(uinformat.pidlist):
        if uinformat.pidlist[i] > 0:
            if uinformat.verbose:
                sys.stderr.write("Processing pid %d.\n" % uinformat.pidlist[i])
            m = uinformat.pidlist[i]
            ret = uinfile_wait_on_pid(uinformat, uinformat.pidlist[i], flag, status)
            if uinformat.verbose:
                sys.stderr.write("uinformat: uinfile_wait_on_pid [%d] returned %d\n" % (m, ret))
            if ret < 0 and flag == os.WNOHANG:
                return -1
            if ret > 0:
                if os.WIFEXITED(status):
                    if os.WEXITSTATUS(status):
                        if os.WEXITSTATUS(status) == 63:
                            gotgzwarn = 63
                        else:
                            retval += 1
                else:
                    retval += 1
        i += 1
    if uinformat.verbose:
        sys.stderr.write("uinformat: uinfile_wait_on_all_pid returning 0.\n")
    return gotgzwarn + retval


def uinfile_get_recompress_vector(uinformat):
    vplob = uinformat.recompress_commands
    if vplob_get_nstore(vplob) == 0:
        return None
    else:
        ret = vplob_get_list(vplob)
        return ret


def run_gpg_list_packets_command(xbuffer, buffer_length):
    detect_gpg = [None, None]
    detect_text = strob_open(32)
    detect_command = SHCMD.shcmd_common_open()
    # Run this command
    # gpg --list-only --list-packets  --status-fd 1

    SHCMD.shcmd_add_arg(detect_command, "gpg")
    SHCMD.shcmd_add_arg(detect_command, "--list-only")
    SHCMD.shcmd_add_arg(detect_command, "--list-packets")
    SHCMD.shcmd_add_arg(detect_command, "--status-fd")
    SHCMD.shcmd_add_arg(detect_command, "1")
    SHCMD.shcmd_set_errfile(detect_command, "/dev/null")
    detect_gpg[0] = detect_command
    detect_gpg[1] = None
    fd = swlib_open_memfd()
    if fd < 0:
        return None
    ret = uxfio_write(fd, xbuffer, buffer_length)
    if ret <= 0:
        return None
    swlib_exec_filter(detect_gpg, fd, detect_text)
    if strob_strlen(detect_text) < 2:
        return None
    uxfio_close(fd)
    SHCMD.shcmd_close(detect_command)
    return detect_text


def is_gpg_data(gp):
    if gp[:len(UinC.UINFILE_MAGIC_gpg_sym)] == UinC.UINFILE_MAGIC_gpg_sym:
        return 1
    if gp[:len(UinC.UINFILE_MAGIC_gpg_enc1)] == UinC.UINFILE_MAGIC_gpg_enc1:
        return 2
    if gp[:len(UinC.UINFILE_MAGIC_gpg_enc2)] == UinC.UINFILE_MAGIC_gpg_enc2:
        return 3
    return 0

def is_gpg_packet(gp):
    # These tests are based on RFC4880
    b1 = gp[0]
    b2 = gp[1]

    if not (b1 & (1 << 7)):
        return 0  # Bit 7 is not set

    if b1 & (1 << 6):
        # New Packet
        packet_tag = \
            (b1 & (1 << 5)) * 32 + \
            (b1 & (1 << 4)) * 16 + \
            (b1 & (1 << 3)) * 8 + \
            (b1 & (1 << 2)) * 4 + \
            (b1 & (1 << 1)) * 2 + \
            (b1 & (1 << 0)) * 1
    else:
        # Old Packet
        packet_tag = \
            (b1 & (1 << 5)) * 8 + \
            (b1 & (1 << 4)) * 4 + \
            (b1 & (1 << 3)) * 2 + \
            (b1 & (1 << 2)) * 1

    if packet_tag == 0:
        return 0  # Must not be a OpenPGP packet

    if packet_tag == 1 or packet_tag == 3 or 0:
        return 1
    else:
        # Nothing else is supported
        pass
    return 0


def does_have_name_version(buf):
    s = buf.find('-')
    if s == -1:
        return 0
    s += 1
    if buf[s].isdigit() and not buf[0].isdigit():
        return 1
    else:
        return 0


def ar_get_size(buf, value):
    a = buf[:60]
    if a[58:60] != ArConst.ARFMAG:
        return -1
    a = a[:58]
    *value, = map(int, a.split())
    return 0


def determine_if_has_leading_slash(uinformat, buffer):
    ret = 0
    eoa = 0
    if uinformat['type'] == UinC.USTAR_FILEFORMAT:
        file_hdr = taru_make_header()
        taru_read_in_tar_header2(uinformat['taru'], file_hdr, -1, buffer, eoa, 0, tc.TARRECORDSIZE)
        if ahsstaticgettarfilename(file_hdr)[0] == '/':
            ret = 1
        taru_free_header(file_hdr)
    elif uinformat['type'] == UinC.CPIO_POSIX_FILEFORMAT:
        if buffer[70] == '/':
            ret = 1
    else:
        if buffer[104] == '/':
            ret = 1
    return ret


def uinfile_del_pid(uinformat, pid):
    i = 0
    while i < len(uinformat['pidlist']):
        if uinformat['pidlist'][i] == pid:
            uinformat['pidlist'][i] = 0
            return 0
        i += 1
    return -1


def uinfile_add_pid(uinformat, pid):
    i = 0
    while i < len(uinformat['pidlist']):
        if uinformat['pidlist'][i] == 0:
            uinformat['pidlist'][i] = pid
            return 0
        i += 1
    return -1


def uinfile_i_get_name(uinformat, file_hdr, fd, format, retval):
    retval[0] = taru_read_header(uinformat['taru'], file_hdr, fd, format, None, 0)
    if retval[0] < 0:
        return None
    return ahsstaticgettarfilename(file_hdr).copy()


def uinfile_check_ieee_fd(fd):
    current_buftype = uxfio_fcntl(fd, uxfioc.UXFIO_F_GET_BUFTYPE, 0)
    if current_buftype != uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM and current_buftype != uxfioc.UXFIO_BUFTYPE_MEM and current_buftype != uxfioc.UXFIO_BUFTYPE_FILE:
        print("internal error, incorrect usage of uinfile_check_ieee", file=sys.stderr)
        return -1
    if current_buftype == uxfioc.UXFIO_BUFTYPE_MEM:
        if uxfio_fcntl(fd, uxfioc.UXFIO_F_SET_BUFFER_LENGTH, 3072) != 0:
            return -1
    return fd


def uinfile_detect_ieee(uinformat, oflags):
    ret = 0
    nameretval = 0
    name = None
    mm = 0
    lead_bytes = 0
    allow_generic_tar = oflags & UinC.UINFILE_DETECT_OTARALLOW
    file_hdr = uinformat['file_hdr']
    if uinformat['swpath']:
        swpath_close(uinformat['swpath'])
    uinformat['swpath'] = swpath_open("")
    if uinfile_check_ieee_fd(uinformat['fd']) < 0:
        sys.stderr.write("uinfile: Incorrect uxfio fd settings.")
        return -1
    while mm < UinC.UINFILE_IEEE_MAX_LEADING_DIR and name == uinfile_i_get_name(uinformat, file_hdr, uinformat.fd,
                                                                                uinformat.type, nameretval) is not None:
        if nameretval <= 0:
            uinformat['layout_type'] = UinC.UINFILE_FILELAYOUT_UNKNOWN
            print(f"{swlib_utilname_get()}: error: package format read error.", file=sys.stderr)
            uxfio_close(uinformat['fd'])
            uinformat['fd'] = -1
            return -1
        lead_bytes += nameretval
        if swpath_parse_path(uinformat['swpath'], name) < 0:
            if allow_generic_tar == 0:
                sys.stderr.write("uinfile: swpath_parse_path: error parsing: {name}")
                uxfio_close(uinformat['fd'])
                uinformat['fd'] = -1
                return -1
            else:
                uinformat['layout_type'] = UinC.UINFILE_FILELAYOUT_UNKNOWN
                break
        if not fnmatch.fnmatch("*/catalog/INDEX", name) or not fnmatch.fnmatch("catalog/INDEX", name):
            uinformat['layout_type'] = UinC.UINFILE_FILELAYOUT_IEEE
            break
        if (file_hdr['c_mode'] & ft.CP_IFMT) != ft.CP_IFDIR:
            if allow_generic_tar == 0:
                uinformat['layout_type'] = UinC.UINFILE_FILELAYOUT_UNKNOWN
                print(f"{swlib_utilname_get()}: Package layout_version 1.0 not found", file=sys.stderr)
                uxfio_close(uinformat['fd'])
                uinformat['fd'] = -1
                return -1
            else:
                uinformat['layout_type'] = UinC.UINFILE_FILELAYOUT_UNKNOWN
                break
        mm += 1
        del name
    if mm >= UinC.UINFILE_IEEE_MAX_LEADING_DIR and (allow_generic_tar == 0) or name == None:
        uinformat['layout_type'] = UinC.UINFILE_FILELAYOUT_UNKNOWN
        sys.stderr.write("{swlib_utilname_get()}: Package layout_version 1.0 not found.")
        uxfio_close(uinformat['fd'])
        uinformat['fd'] = -1
        return -1
    if ret == uxfio_lseek(uinformat['fd'], -lead_bytes, uxfioc.UXFIO_SEEK_VCUR) and ret != 0:
        sys.stderr.write("uxfio_lseek error in uinfile_handle_return 0015. off={lead_bytes} ret={ret}")
        return -1
    taruib_unread(lead_bytes)
    return uinformat['fd']


def uinfile_handle_return(uinformat, oflags, uxfio_buffer_type):
    file_hdr = uinformat['file_hdr']
    ahsstaticsettarfilename(file_hdr, None)
    ahsstaticsetpaxlinkname(file_hdr, None)
    if uxfio_lseek(uinformat['fd'], 0, os.SEEK_SET) < 0:
        print(f"uxfio_lseek error in uinfile_handle_return 0001. fd={uinformat['fd']}", file=sys.stderr)
        return -1
    taruib_set_datalen(0)
    if oflags & UinC.UINFILE_DETECT_FORCEUNIXFD and uinformat['fd'] >= uxfioc.UXFIO_FD_MIN:
        print("HERE: forking to make unix fd")
        uxfio_fcntl(uinformat['fd'], uxfioc.UXFIO_F_ARM_AUTO_DISABLE, 1)
        uinformat['fd'] = swlib_fork_to_make_unixfd(uinformat['fd'], uinformat['blockmask_'], uinformat['defaultmask_'],
                                                    None)
    elif oflags & UinC.UINFILE_DETECT_FORCEUXFIOFD and uinformat['fd'] < uxfioc.UXFIO_FD_MIN:
        print("HERE: opendup with buffer type")
        uinformat['fd'] = uxfio_opendup(uinformat['fd'], uxfio_buffer_type)
        uxfio_fcntl(uinformat['fd'], uxfioc.UXFIO_F_SET_CANSEEK, 0)
    elif oflags & UinC.UINFILE_DETECT_FORCE_SEEK and uxfio_espipe(uinformat['fd']) and uinformat[
        'fd'] < uxfioc.UXFIO_FD_MIN:
        print("HERE: 2:opendup with buffer type")
        uinformat['fd'] = uxfio_opendup(uinformat['fd'], uxfio_buffer_type)
    elif oflags & UinC.UINFILE_DETECT_FORCE_SEEK and uxfio_espipe(uinformat['fd']) and uinformat[
        'fd'] >= uxfioc.UXFIO_FD_MIN:
        print("HERE: setting to buf type mem")
        uxfio_fcntl(uinformat['fd'], uxfioc.UXFIO_F_SET_BUFTYPE, uxfioc.UXFIO_BUFTYPE_MEM)
    else:
        print("HERE: non of the above")
    if uinformat['fd'] >= uxfioc.UXFIO_FD_MIN:
        print(f"setting uxfio_buffer_type {uxfio_buffer_type}")
        uxfio_fcntl(uinformat['fd'], uxfioc.UXFIO_F_SET_BUFTYPE, uxfio_buffer_type)
    print(f"returning fd={uinformat['fd']}")
    return uinformat['fd']
