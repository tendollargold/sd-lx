# self.py  --  File I/O calls on an archive stream or directory file.
#
#   Copyright (C) 2003  Jim Lowe
#   Copyright (c) 2023-2024 Paul Weber conversion to Python
#   All Rights Reserved.
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import sys
import ctypes
import stat
import errno
import fcntl
import tarfile

from sd.const import LegacyConstants, SwGlobals
from sd.debug import FrameRecord, SW_E_FAIL3, SW_E_FAIL, SWVARFS_E_DEBUG2, SWVARFS_E_DEBUG
from sd.swsuplib.misc.cstrings import strrchr
from sd.swsuplib.strob import strob_strtok, strob_strcat, strob_open, strob_close
from sd.swsuplib.uxfio import uxfio_ioctl, uxfio_lseek, uxfio_opendup, uxfio_close
from sd.swsuplib.misc.strar import strar_add, strar_open, strar_get, strar_num_elements, strar_close
from sd.swsuplib.strob import strob_str, strob_strcpy, strob_memset
from sd.swsuplib.misc.swlib import swlib_unix_dircat, swlib_resolve_path, swlib_strdup, swlib_slashclean
from sd.swsuplib.misc.swlib import swlib_squash_all_dot_slash, swlib_utilname_get, swlib_assertion_fatal
from sd.swsuplib.misc.swlib import swlib_process_hex_escapes, swlib_imaxtostr
from sd.swsuplib.taru.hllist import hllist_close, hllist_open
from sd.swsuplib.taru.ahs import ahs_open
from sd.swsuplib.taru.taru import TARU, taru_create, taru_delete, taru_set_tarheader_flag
from sd.swsuplib.taru.tar import tarc
from sd.swsuplib.taru.copyout import taru_write_archive_member_data
from sd.swsuplib.taru.readinheader import taru_pump_amount2, taru_read_header
from sd.swsuplib.taru.filetypes import Filetypes
from sd.swsuplib.taru.mtar import taru_statbuf2filehdr, taru_filehdr2statbuf
from sd.swsuplib.taru.otar import taru_tape_skip_padding
from sd.swsuplib.taru.ahs import ahsstaticgettarfilename, ahsstaticsettarfilename, ahs_vfile_hdr, ahsstaticgettarlinkname
from sd.swsuplib.taru.ahs import ahs_get_tar_typeflag, ahsstaticsetpaxlinkname, ahs_close
from sd.swsuplib.uinfile.uinfile import uinc, uinfile_get_type, uinfile_get_has_leading_slash
from sd.swsuplib.uinfile.uinfile import uinfile_close, uinfile_opendup, uinfile_opendup_with_name
from sd.swsuplib.uxfio import UxfioConst, uxfio_open
from sd.swsuplib.misc.usrstat import swlib_write_stats

g = SwGlobals
uxfioc = UxfioConst

ft = Filetypes()
lc = LegacyConstants
taru = TARU()

SWVARFS_OPEN = 8
SWVARFS_DIR_WALK_LENGTH = 100
SWVARFS_S_IFDIR = 0o040000
SWVARFS_S_IFREG = 0o100000
SWVARFS_VSTAT_LSTAT = "lstat"
SWVARFS_VSTAT_STAT = "stat"


def readdir(path):
    libc = ctypes.CDLL("libc.so.6")
    return libc.readdir(path)


def opendir(path):
    libc = ctypes.CDLL("libc.so.6")
    return libc.opendir(path)


class FileChain:
    def __init__(self):
        self.nameFC = None
        self.header_offsetFC = 0
        self.data_offsetFC = 0
        self.prevFC = None
        self.nextFC = None
        self.uxfio_u_fdFC = 0

    def make_link(self, header_offset, data_offset):
        # self = FileChain()
        self.nameFC = ahsstaticgettarfilename(self)
        self.nameFC = self.nameFC.strip('/')
        self.nameFC = self.nameFC.replace('\\\\', '\\')
        print("link made name=[%s] header_offset = [%s]" % (self.nameFC, str(header_offset)))
        print("link made name=[%s]   data_offset = [%s]" % (self.nameFC, str(data_offset)))
        self.header_offsetFC = header_offset
        self.data_offsetFC = data_offset
        self.prevFC = None
        self.nextFC = None
        self.uxfio_u_fdFC = -1
        return self

    def delete_link(self):
        del self.nameFC
        del self


fc = FileChain()


class SWVARFS_U_FD_DESC:
    def __init__(self):
        self.u_current_name_ = ""  # u_<*> are attributes for the opening of
        self.u_linkname_ = ""  # individual files
        self.u_fd = 0  # file descriptor, maybe a uxfio_fd


class SWVARFS:
    def __init__(self):
        self.u_current_name_ = ""
        self.u_linkname_ = ""
        self.u_fd = 0
        self.format_desc = None  # The Format descriptor, see uinfile.c
        self.uinformat_close_on_delete = 0  # close format_desc  on close
        self.format = 0
        self.openpath = None  # The opening path
        self.opencwdpath = None  # The current working directory.
        self.u_name = None  # Temporary name of current user file
        self.dirscope = None  # The current scope, trversals are limited to this directory.
        self.tmp = None
        self.vcwd = None
        self.ahs = None
        self.do_close_ahs = 0
        self.fd = 0
        self.did_dup = 0
        self.u_fd_set = [SWVARFS_U_FD_DESC() for _ in range(4)]
        self.u_current_name_ = ""
        self.g_linkname_ = ""
        self.u_fd = 0
        self.did_u_open = 0
        self.head = None
        self.tail = None
        self.f_do_stop_ = None
        self.direntpath = None
        self.stackix = 0
        self.stack = None
        self.derr = 0
        self.have_read_files = 0
        self.current_filechain = None
        self.uxfio_buftype = 0
        self.current_header_offset = 0
        self.current_data_offset = 0
        self.is_unix_pipe = 0
        self.eoa = 0
        self.makefilechain = 0
        self.has_leading_slash = 0
        self.f_stat = None
        self.link_record = None
        self.taru = None
        self.n_loop_symlinks = 0


def swvarfs_i_u_open_fs(swvarfs, name):
    ret = 0
    st = os.stat_result(name)
    SWVARFS_E_DEBUG2("ENTERING [%s]", name)
    if not name:
        if ret == swvarfs_dirent_err(swvarfs):
            SWVARFS_E_DEBUG("Internal error status returned from swvarfs_get_next_dirent")
            return -2
        name = swvarfs_get_next_dirent(swvarfs, None)
        if not name and not swvarfs_dirent_err(swvarfs):
            return 0
        if swvarfs_dirent_err(swvarfs):
            SWVARFS_E_DEBUG("Internal error status returned from swvarfs_get_next_dirent")
            return -2

    if name[0] != '/':
        if swvarfs.stackix == 0 and is_name_a_path_prefix(strob_str(swvarfs.direntpath),
                                                       strob_str(swvarfs.openpath)) == 0:
            swvarfs.u_name = strob_str(swvarfs.openpath)
        else:
            swvarfs.u_name = ""
        swlib_unix_dircat(swvarfs.u_name, name)
    else:
        swvarfs.u_name = name

    fd = uxfio_open(strob_str(swvarfs.u_name), os.O_RDONLY, 0)
    if swvarfs.u_current_name_:
        del swvarfs.u_current_name_
    swvarfs.u_current_name_ = swlib_strdup(strob_str(swvarfs.u_name))
    if fd < 0:
        ahsstaticsettarfilename(ahs_vfile_hdr(swvarfs.ahs), None)
        return -1
    SWLIB_ASSERT(ud_set_fd(swvarfs, fd) >= 0)
    u_index = ud_find_fd(swvarfs, fd)
    SWLIB_ASSERT(u_index >= 0)
    swvarfs_uxfio_fcntl(swvarfs, uxfioc.UXFIO_F_SET_BUFTYPE, uxfioc.UXFIO_BUFTYPE_NOBUF)
    if swvarfs.f_stat(strob_str(swvarfs.u_name), st):
        return -2
    ahsstaticsettarfilename(ahs_vfile_hdr(swvarfs.ahs), None)
    if swvarfs.u_fd_set[u_index].u_current_name_:
        del swvarfs.u_fd_set[u_index].u_current_name_
    swvarfs.u_fd_set[u_index].u_current_name_ = swlib_strdup(strob_str(swvarfs.u_name))
    if stat.S_ISLNK(st.st_mode):
        set_linkname(swvarfs, fd, strob_str(swvarfs.u_name))
    taru_statbuf2filehdr(ahs_vfile_hdr(swvarfs.ahs), st, None, strob_str(swvarfs.u_name), None)
    uxfio_ioctl(fd, uxfioc.UXFIO_IOCTL_SET_STATBUF, st)
    swvarfs.u_fd = fd
    return fd

def swvarfs_i_u_lstat_fs(swvarfs, path, st):
    print("ENTERING")
    if path[0] != '/':
        swvarfs.u_name = ""  # ??? FIXME What should the policy be.
        # swvarfs.u_name += path
    else:
        swvarfs.u_name = path
    ret = swvarfs.f_stat(swvarfs.u_name, st)
    print("LEAVING")
    return ret

def swvarfs_i_u_lstat_archive(swvarfs, path, st):
    if fd := swvarfs_u_open(swvarfs, path) < 0:
        return -3
    if swvarfs_file_has_data(swvarfs):
        taru_pump_amount2(-1, fd, -1, -1)
    if st and swvarfs_u_fstat(swvarfs, fd, st):
        return -2
    swvarfs_u_close(swvarfs, fd)
    return 0

def swvarfs_get_uinformat(swvarfs):
    return swvarfs.format_desc

def swvarfs_get_next_dirent_archive_NEW(swvarfs, st, peoa):
    if swvarfs.did_u_open == 0 and swvarfs.u_current_name_:
        """
        Open the file.
        """
        swvarfs_i_u_lstat_archive(swvarfs, swvarfs.u_current_name_, None)

    SWVARFS_E_DEBUG("ENTERING")
    SWVARFS_E_DEBUG2("OFFSET = %d", int(uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)))
    old_pos = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
    swvarfs.current_header_offset = uxfio_lseek(swvarfs.fd, 0, os.SEEK_CUR)

    if taru_read_header(swvarfs.taru, ahs_vfile_hdr(swvarfs.ahs), swvarfs.fd, swvarfs.format, peoa, swvarfs.taru.taru_tarheaderflags) < 0:
        SW_E_FAIL("header read error")
        swvarfs.derr = -3
        return None
    if peoa:
        return None
    swvarfs.current_data_offset = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)

    """
    seek back to the start of the header because
    the <>_u_open routine reads the header again.
    """

    new_pos = swvarfs.current_data_offset
    if uxfio_lseek(swvarfs.fd, old_pos - new_pos, uxfioc.UXFIO_SEEK_VCUR) < 0:
        exit(88)
        # return None # Can't return if previously exited, duh
    if st:
        taru_filehdr2statbuf(st, ahs_vfile_hdr(swvarfs.ahs))
    name = ahsstaticgettarfilename(ahs_vfile_hdr(swvarfs.ahs))
    if swvarfs.u_current_name_:
        del swvarfs.u_current_name_
        swvarfs.u_current_name_ = None
    """
    Check for out of scope.
    """
    if  swvarfs.current_data_offset == uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR):
        """
        Out of scope.
        """
        SWVARFS_E_DEBUG2("SCOPE REJECT HERE name = %s", name)
        u_reject(swvarfs)
    if swvarfs.u_current_name_:
        del swvarfs.u_current_name_
    swvarfs.u_current_name_ = swlib_strdup(name)
    swvarfs.did_u_open = 0
    return name

def swvarfs_i_readin_filechain(name):
    last = swvarfs.tail
    if last:
        uxfio_lseek(swvarfs.fd, last.header_offsetFC, os.SEEK_SET)
        header_offset = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
        if taru_read_header(swvarfs.taru, ahs_vfile_hdr(swvarfs.ahs), swvarfs.fd, swvarfs.format, None,
                            swvarfs.taru.taru_tarheaderflags) < 0:
            print("header read error", file=sys.stderr)
            return None
        swvarfs.current_data_offset = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
        data_offset = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
        if (ahs_vfile_hdr(swvarfs.ahs).c_mode & ft.CP_IFMT) == ft.CP_IFREG:
            if taru_write_archive_member_data(swvarfs.taru, ahs_vfile_hdr(swvarfs.ahs), -1, swvarfs.fd, None,
                                              uinfile_get_type(swvarfs.format_desc), -1, None) < 0:
                print("data error %s\n" % ahsstaticgettarfilename(swvarfs), file=sys.stderr)
                return None
            taru_tape_skip_padding(swvarfs.fd, ahs_vfile_hdr(swvarfs.ahs).c_filesize,
                                   uinfile_get_type(swvarfs.format_desc))
        else:
            data_offset = -1
    else:
        uxfio_lseek(swvarfs.fd, 0, os.SEEK_SET)
    while True:
        header_offset = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
        if taru_read_header(swvarfs.taru, ahs_vfile_hdr(swvarfs.ahs), swvarfs.fd, swvarfs.format, None,
                            swvarfs.taru.taru_tarheaderflags) < 0:
            new_link = None
            break
        swvarfs.current_data_offset = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
        if ahsstaticgettarfilename(swvarfs) == "TRAILER!!!":
            new_link = None
            break
        data_offset = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
        if (ahs_vfile_hdr(swvarfs.ahs).c_mode & ft.CP_IFMT) == ft.CP_IFREG:
            if taru_write_archive_member_data(swvarfs.taru, ahs_vfile_hdr(swvarfs.ahs), -1, swvarfs.fd, None,
                                              uinfile_get_type(swvarfs.format_desc), -1, None) < 0:
                print("data error %s\n" % ahsstaticgettarfilename(swvarfs), file=sys.stderr)
                return None
            taru_tape_skip_padding(swvarfs.fd, ahs_vfile_hdr(swvarfs.ahs).c_filesize,
                                   uinfile_get_type(swvarfs.format_desc))
        else:
            FileChain.data_offset = -1
        new_link = FileChain.make_link(ahs_vfile_hdr(swvarfs.ahs), header_offset, data_offset)
        swvarfs_i_attach_filechain(new_link)
        if do_stop_reading(ahsstaticgettarfilename(swvarfs), None, name) != 0:
            break
    return new_link

def raise_dir_context(swvarfs, dirx):
    ds = DirContext()
    get_dir_context(swvarfs, ds)
    if ds.dp:
        ds.dp.close()
    swvarfs.stackix -= 1
    if not swvarfs.stackix:
        dirx.dp = None
    else:
        get_dir_context(swvarfs, dirx)

def get_dir_context(swarfs, dirx):
    i = swvarfs.stackix
    s = swvarfs.stack
    ds = s[i]
    dirx.statbuf = ds.statbuf
    dirx.dirp = ds.dirp
    dirx.dp = ds.dp
    dirx.ptr = ds.ptr

def show_nopen():
    ret = os.open("/dev/null", os.O_RDWR)
    if ret < 0:
        sys.stderr.write("fcntl error: {}" + os.strerror(ret))
    os.close(ret)
    return ret

def ud_init(swvarfs):
    for i in range(u_fd_array_len(swvarfs)):
        swvarfs.u_fd_set[i].u_fd = -1
        swvarfs.u_fd_set[i].u_current_name_ = None
        swvarfs.u_fd_set[i].u_linkname_ = None

def u_fd_array_len(a):
    return len(a.u_fd_set) // sys.getsizeof(SWVARFS_U_FD_DESC)

def ud_set_fd(swvarfs, fd):
    for i in range(len(swvarfs.u_fd_set)):
        if swvarfs.u_fd_set[i].u_fd < 0:
            swvarfs.u_fd_set[i].u_fd = fd
            return fd
    return -1

def ud_unset_fd(fd):
    for i in range(u_fd_array_len(swvarfs)):
        if swvarfs.u_fd_set[i].u_fd == fd:
            swvarfs.u_fd_set[i].u_fd = -1
            return fd
    return -1

def ud_find_fd(swvarfs, fd):
    for i in range(len(swvarfs.u_fd_set)):
        if swvarfs.u_fd_set[i].u_fd == fd:
            return i
    return -1

def u_reject(swvarfs):
    if swvarfs.u_current_name_:
        del swvarfs.u_current_name_
    swvarfs.u_current_name_ = None
    swvarfs.current_filechain = None

def swvarfs_get_next_dirent_fs(swvarfs, st):
    sto = os.stat_result(st)
    fullpath = swvarfs.direntpath
    dirx = DirContext()

    if st is None:
        statbuf = sto
    else:
        statbuf = st

    if swvarfs.stackix:
        SWVARFS_E_DEBUG("")
        get_dir_context(swvarfs, dirx)
        dp = dirx.dp
        dirp = dirx.dirp

        while dp is None:
            delete_last_component(swvarfs, fullpath)
            raise_dir_context(swvarfs, dirx)
            dirp = dirx.dirp
            dp = dirx.dp
        delete_last_component(swvarfs, fullpath)
    else:
        SWVARFS_E_DEBUG("")
        swvarfs_setdir(swvarfs, fullpath)
        get_dir_context(swvarfs, dirx)
        dp = dirx.dp
        stat_node(swvarfs, fullpath, statbuf)
        return fullpath

    while True:
        while os.scandir(dirp) is not None:
            if dirp.d_name == "." or dirp.d_name == "..":
                continue
            strob_strcat(swvarfs.direntpath, "/")
            strob_strcat(swvarfs.direntpath, dirp.d_name)
            fullpath = strob_str(swvarfs.direntpath)
            stat_node(swvarfs, fullpath, statbuf)
            SWVARFS_E_DEBUG("")
            if stat.S_ISDIR(statbuf.st_mode):
                olddp = dp
                dp = os.scandir(fullpath)
                if dp is None:
                    if errno == errno.EACCES:
                        sys.stderr.write(f"Permission denied: {fullpath}")
                    else:
                        SWVARFS_E_DEBUG2("internal error -3: [%s]", fullpath)
                        swvarfs.derr = -3
                        return None
                dirx.dp = dp
                add_dir_context(swvarfs, dirx)
                strob_strcat(swvarfs.direntpath, "/")
                fullpath = strob_str(swvarfs.direntpath)
            swvarfs.derr = 0
            return fullpath
            # break
        raise_dir_context(swvarfs, dirx)
        delete_last_component(swvarfs, fullpath)
        dp = dirx.dp
        if dirp is None:
            break
    swvarfs.derr = 0
    return None

def handle_leading_slash(swvarfs, mode, name, pflag):
    if swvarfs.format != uinc.UINFILE_FILESYSTEM and swvarfs.has_leading_slash == 0:
        if mode == "drop":
            if name[0].startswith("/"):
                name = name[1:]
                pflag = 1
            else:
                pflag = 0
        elif mode == "restore":
            if pflag:
                name = '/' + name
                pflag = 0
    else:
        pflag = 0
    return pflag, name

def check_seek_violation(swvarfs):
    uxfio_buffer_type = swvarfs_uxfio_fcntl(swvarfs, uxfioc.UXFIO_F_GET_BUFTYPE, 0)
    if (
        (swvarfs.is_unix_pipe and swvarfs.fd < uxfioc.UXFIO_FD_MIN) or
        (
            swvarfs.is_unix_pipe and
            swvarfs.fd >= uxfioc.UXFIO_FD_MIN and
            (uxfio_buffer_type != uxfioc.UXFIO_BUFTYPE_FILE and uxfio_buffer_type != uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
        )
    ):
        sys.stderr.write("\nThe pending operation is invalid for use with a pipe.\n")
        sys.stderr.write("Re-try with a disk file or stdin redirected from a disk file\n")
        sys.stderr.write("or, file/dynamic-mem buffering, or, in-order traversal only.\n")
        return 10
    return 0

def construct_new_path_from_pathspec(swvarfs, newvcwd, pathspec):
    pathcomps = []
    cmdcomps = []
    i = 0
    newvcwd = ""
    if strob_str(swvarfs.vcwd)[0] == '/':
        sx = "/"
    else:
        sx = ""
    sx = sx + strob_str(swvarfs.vcwd)
    process_vcwd(pathcomps, sx)
    swlib_squash_all_dot_slash(pathspec)
    process_vcwd(cmdcomps, pathspec)
    ncomp = strar_num_elements(pathcomps)
    j = 0
    cx = strar_get(cmdcomps, 0)
    while cx:
        if cx == "..":
            j = j + 1
        cx = strar_get(cmdcomps, i)
    ncomp = ncomp - j
    if ncomp <= 0:
        ret = -1
    else:
        strob_strcpy(newvcwd, "")
        i = 0
        while i < ncomp:
            if i > 0:
                strob_strcat(newvcwd, "/")
            strob_strcat(newvcwd, strar_get(pathcomps, i))
            i = i + 1
        strob_strcat(newvcwd, "/")
        i = j
        while i < strar_num_elements(cmdcomps):
            if i > j:
                strob_strcat(newvcwd, "/")
            strob_strcat(newvcwd, strar_get(cmdcomps, i))
            i = i + 1
        ret = 0
    strar_close(pathcomps)
    strar_close(cmdcomps)
    return ret

def set_linkname(swvarfs, ufd, path):
    retval = 0
    file_hdr = swvarfs.ahs.vfile_hdr()
    vlen = 1024
    if ufd < 0:
        vlinkp = swvarfs.g_linkname_
    else:
        u_index = ud_find_fd(swvarfs, ufd)
        if u_index >= 0:
            vlinkp = swvarfs.u_fd_set[u_index].u_linkname_
        else:
            sys.stderr.write("internal error, %d desc not found in swvarfs:set_linkname()\n" % ufd)
            return -1
    file_hdr.set_tar_linkname_length(vlen)
    s = file_hdr.get_tar_linkname()
    s[vlen-1] = '\0'
    # c = os.readlink(path, s, vlen-1)
    c = os.readlink(path)
    if c < 0:
        sys.stderr.write("error: readlink on [%s] returned %d.\n" % (path, c))
        return -1
    if c >= vlen-1:
        c = vlen - 1
        sys.stderr.write("Warning, linkname [%s] truncated.\n" % s)
    s[c] = '\0'
    if len(s) > tarc.TARNAMESIZE:
        sys.stderr.write("Warning, linkname [%s] is too long for tar archives\n" % s)
    if vlinkp:
        del vlinkp
        vlinkp = None
    vlinkp = s
    return retval

def delete_filechain(swvarfs):
    last = swvarfs.tail
    while last:
        newlast = last.prevFC
        FileChain.delete_link(last)
        last = newlast
    swvarfs.head = None
    swvarfs.tail = None

def delete_last_component(swvarfs, path):
    index_of_slash = len(swvarfs.dirscope) - 2
    swvarfs.dirscope = swvarfs.dirscope[:index_of_slash] + '\0'
    s = path.find(swvarfs.dirscope)
    swvarfs.dirscope = swvarfs.dirscope[:index_of_slash] + '/'
    if s is None:
        SW_E_FAIL3("delete_last_component path=[%s] dirscope=[%s]", path, swvarfs.dirscope)
        return
    s += len(swvarfs.dirscope) - 2
    if s == "/":
        return
    s = path.rfind('/', 0, s)
    if s:
        path = path[:s]
    return path

def add_dir_context(swvarfs, dirx):
    i = swvarfs.stackix + 1
    s = swvarfs.stack
    ds = dirx
    s[i] = ds
    swvarfs.stackix = i

def do_stop_reading(xx, member_name, name):
    ret = 0
    did_squash = 0
    did_squash1 = 0
    did_squash2 = 0
    swvarfs = xx
    if member_name[:2] == "./":
        member_name = member_name[2:]
    if not name:
        return 0
    if name[:2] == "./":
        name = name[2:]
    handle_trailing_slash("drop", name, did_squash)
    handle_trailing_slash("drop", member_name, did_squash1)
    swvarfs.handle_leading_slash("drop", name, did_squash2)
    ret = 1 if name != member_name else 0
    swvarfs.handle_leading_slash("restore", name, did_squash2)
    handle_trailing_slash("restore", name, did_squash)
    handle_trailing_slash("restore", member_name, did_squash1)
    return not ret

def swvarfs_i_init(swvarfs):
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    swvarfs.uinformat_close_on_delete = 1
    swvarfs.opencwdpath = strob_open(32)
    swvarfs.openpath = strob_open(32)
    swvarfs.dirscope = strob_open(32)
    swvarfs.tmp = strob_open(128)
    swvarfs.vcwd = strob_open(128)
    strob_strcpy(swvarfs.opencwdpath, "")
    strob_strcpy(swvarfs.openpath, "")
    swvarfs.u_name = strob_open(32)
    swvarfs.ahs = ahs_open()
    swvarfs.do_close_ahs = 1
    swvarfs.head = None
    swvarfs.tail = None
    swvarfs.g_linkname_ = None
    swvarfs.u_current_name_ = None
    swvarfs.u_fd = -1
    swvarfs.format = 0
    swvarfs.f_do_stop_ = do_stop_reading
    swvarfs.stack = strob_open(16)
    swvarfs.direntpath = strob_open(16)
    swvarfs.stackix = 0
    swvarfs.derr = 0
    swvarfs.have_read_files = 0
    swvarfs.current_filechain = None
    swvarfs.uxfio_buftype = uxfioc.UXFIO_BUFTYPE_FILE
    swvarfs.is_unix_pipe = 0
    swvarfs.current_data_offset = 0
    swvarfs.current_header_offset = -1
    swvarfs.makefilechain = 0
    swvarfs.eoa = 0
    swvarfs.has_leading_slash = 0
    swvarfs.did_u_open = -1
    ud_init(swvarfs)  # Interesting
    swvarfs.f_stat = os.lstat
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    swvarfs.link_record = hllist_open()
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    swvarfs.taru = taru_create()
    swvarfs.taru.taru_tarheaderflags = 0
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    swvarfs.n_loop_symlinks = 20

def swvarfs_i_search_filechain(name_p, scope_rejection):
    did_drop = 0
    did_omit_lslash = 0
    name = name_p
    last = swvarfs.head
    if not name:
        return None
    name = name
    swlib_process_hex_escapes(name)
    scope_rejection = 0
    while last:
        p = last.nameFC
        if p.startswith("./"):
            p = p[2:]
        if name == "./":
            name = name[:1]
        elif name.startswith("./"):
            name = name[2:]
        handle_trailing_slash("drop", name, did_drop)
        handle_leading_slash(swvarfs, "drop", name, did_omit_lslash)
        if name == p:
            if is_name_in_scope(strob_str(swvarfs.dirscope), name, None):
                handle_trailing_slash("restore", name, did_drop)
                handle_leading_slash(swvarfs,"restore", name, did_omit_lslash)
                return last
            else:
                scope_rejection = 1
                handle_trailing_slash("restore", name, did_drop)
                handle_leading_slash(swvarfs,"restore", name, did_omit_lslash)
                return None
        handle_trailing_slash("restore", name, did_drop)
        handle_leading_slash(swvarfs,"restore", name, did_omit_lslash)
        last = last.nextFC
    return None

def swvarfs_i_u_open_archive_seek_to_name(swvarfs, name):
    header_offset = 0
    peoa = swvarfs.eoa
    existing = ""
    current_name = swvarfs.u_current_name_
    if current_name is None:
        scope_rejection = 0
        existing = swvarfs_i_search_filechain(name, scope_rejection)
        if existing is None:
            if scope_rejection:
                swvarfs.u_reject()
                return -1
            current_name = swvarfs_get_next_dirent_archive_NEW(swvarfs, None, peoa)
        else:
            swvarfs.current_filechain = existing
            current_name = existing.nameFC

    if current_name and not filename_does_match(name, current_name) and existing is None:
        if is_name_in_scope(strob_str(swvarfs.dirscope), current_name, None) == 0:
            swvarfs.u_reject()
            return -1
        else:
            swvarfs.current_filechain = existing
    else:
        if swvarfs.check_seek_violation():
            return -3
        if existing is None:
            existing = swvarfs_i_readin_filechain(name)
            if existing is None:
                swvarfs.u_reject()
                return -1
            else:
                if is_name_in_scope(strob_str(swvarfs.dirscope), name, None) == 0:
                    swvarfs.u_reject()
                    return -1
        swvarfs.current_filechain = existing
        tret = uxfio_lseek(swvarfs.fd, existing.header_offsetFC, os.SEEK_SET)
        if tret != existing.header_offsetFC:
            swvarfs.u_reject()
            return -2

    if swvarfs.makefilechain and swvarfs.current_filechain is None:
        header_offset = uxfio_lseek(swvarfs.fd, 0, os.SEEK_CUR)

    if taru_read_header(swvarfs.taru, ahs_vfile_hdr(swvarfs.ahs), swvarfs.fd, swvarfs.format, peoa,
                        swvarfs.taru.taru_tarheaderflags) < 0:
        if swvarfs.u_current_name_:
            del swvarfs.u_current_name_
        swvarfs.u_current_name_ = None
        return -3

    swvarfs.current_data_offset = uxfio_lseek(swvarfs.fd, 0, os.SEEK_CUR)

    if swvarfs.makefilechain and swvarfs.current_filechain is None:
        FileChain.data_offset = uxfio_lseek(swvarfs.fd, 0, os.SEEK_CUR)
        new_link = FileChain.make_link(ahs_vfile_hdr(swvarfs.ahs), header_offset, FileChain.data_offset)
        swvarfs_i_attach_filechain(new_link)
        swvarfs.current_filechain = new_link

    return 0

def swvarfs_i_u_open_archive_bh(swvarfs, file_hdr):
    st = os.stat(file_hdr)
    if swvarfs.ud_find_fd(-1) < 0:
        sys.stderr.write("too many open swvarfs:u_fd descriptors.")
        return -4

    fd = uxfio_opendup(swvarfs.fd, uxfioc.UXFIO_BUFTYPE_NOBUF)
    if fd < 0:
        return -5
    SWLIB_ASSERT(swvarfs.ud_set_fd(fd) >= 0)
    u_index = swvarfs.ud_find_fd(fd)
    SWLIB_ASSERT(u_index >= 0)
    taru_filehdr2statbuf(st, file_hdr)
    if len(ahsstaticgettarfilename(swvarfs)) > 0:
        if swvarfs.u_current_name_:
            del swvarfs.u_current_name_
        swvarfs.u_current_name_ = swlib_strdup(ahsstaticgettarfilename(swvarfs))

        if swvarfs.u_fd_set[u_index].u_current_name_:
            del swvarfs.u_fd_set[u_index].u_current_name_
        swvarfs.u_fd_set[u_index].u_current_name_ = swlib_strdup(ahsstaticgettarfilename(swvarfs))
    else:
        if swvarfs.u_current_name_:
            del swvarfs.u_current_name_
        swvarfs.u_current_name_ = None

        if swvarfs.u_fd_set[u_index].u_current_name_:
            del swvarfs.u_fd_set[u_index].u_current_name_
        swvarfs.u_fd_set[u_index].u_current_name_ = None

    if swvarfs.g_linkname_:
        del swvarfs.g_linkname_
        swvarfs.g_linkname_ = None

    if len(ahsstaticgettarlinkname(swvarfs)) > 0:
        swvarfs.g_linkname_ = swlib_strdup(ahsstaticgettarlinkname(swvarfs))
        swvarfs.u_fd_set[u_index].u_linkname_ = swlib_strdup(ahsstaticgettarlinkname(swvarfs))

    uxfio_ioctl(fd, uxfioc.UXFIO_IOCTL_SET_STATBUF, st)
    swvarfs.u_fd = fd
    swvarfs.u_fd_set[u_index].u_fd = fd

    return fd

def swvarfs_i_u_open_archive(swvarfs, name):
    ret = swvarfs_i_u_open_archive_seek_to_name(swvarfs, name)
    if ret:
        return ret

    if swvarfs.eoa:
        return -1

    fd = swvarfs_i_u_open_archive_bh(swvarfs, ahs_vfile_hdr(swvarfs.ahs))
    return fd

def swvarfs_i_iopen(swvarfs, oflags):
    if swvarfs.fd >= uxfioc.UXFIO_FD_MIN:
        if os.lseek(swvarfs.fd, 0, os.SEEK_SET) != 0:
            swvarfs.fd = -1
            return -1
        if os.fstat(swvarfs.fd).st_mode & stat.S_IFIFO:
            current_buftype = fcntl.fcntl(swvarfs.fd, uxfioc.UXFIO_F_GET_BUFTYPE, 0)
            if (
                    current_buftype != uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM and
                    current_buftype != uxfioc.UXFIO_BUFTYPE_MEM and
                    current_buftype != uxfioc.UXFIO_BUFTYPE_FILE
            ):
                swvarfs.is_unix_pipe = 1
                fcntl.fcntl(swvarfs.fd, uxfioc.UXFIO_F_SET_BUFTYPE, uxfioc.UXFIO_BUFTYPE_DYNAMIC_MEM)
                fcntl.fcntl(swvarfs.fd, uxfioc.UXFIO_F_SET_BUFACTIVE, uxfioc.UXFIO_ON)
        else:
            swvarfs.is_unix_pipe = 0
    return 0

def swvarfs_i_u_close_archive(swvarfs, fd):
    u_index = swvarfs.ud_find_fd(fd)
    if u_index < 0:
        print(f"close error, desc {fd} not found.")
        return -1
    if fd != swvarfs.u_fd and fd != swvarfs.u_fd_set[u_index].u_fd:
        print(f"close error, desc {fd} not found.")
        return -1
    if (ahs_vfile_hdr(swvarfs.ahs).c_mode & Filetypes.CP_IFMT) != Filetypes.CP_IFLNK:
        if swvarfs.uxfio_buftype != uxfioc.UXFIO_BUFTYPE_NOBUF:
            ret = os.lseek(fd, 0, os.SEEK_END)
            if ret < 0:
                print("swvarfs_i_u_close_archive: os.lseek error")
                return -1
    if (ahs_vfile_hdr(swvarfs.ahs).c_mode & Filetypes.CP_IFMT) == Filetypes.CP_IFREG:
        taru_tape_skip_padding(swvarfs.fd, ahs_vfile_hdr(swvarfs.ahs).c_filesize, swvarfs.format)
    else:
        current_pos = os.lseek(swvarfs.fd, 0, os.SEEK_CUR)
        if current_pos < 0:
            print("uxfio_lseek failed-1")
        if os.lseek(swvarfs.fd, swvarfs.current_data_offset - current_pos, os.SEEK_CUR) < 0:
            print("uxfio_lseek failed-2")
    swvarfs.u_fd = -1
    if swvarfs.g_linkname_:
        del swvarfs.g_linkname_
        swvarfs.g_linkname_ = None
    if swvarfs.u_current_name_:
        del swvarfs.u_current_name_
        swvarfs.u_current_name_ = None
    if swvarfs.u_fd_set[u_index].u_current_name_:
        del swvarfs.u_fd_set[u_index].u_current_name_
        swvarfs.u_fd_set[u_index].u_current_name_ = None
    swvarfs.ud_unset_fd(fd)
    os.close(fd)
    return 0

def clear_all_dir_context(swvarfs):
    dirx = DirContext()
    while swvarfs.stackix > 0:
        swvarfs.raise_dir_context(dirx)

def set_dirscope(path):
    swvarfs.dirscope = path
    swvarfs.dirscope = swvarfs.dirscope.rstrip("/")
    swvarfs.dirscope = swvarfs.dirscope.lstrip("./")
    if "/." in swvarfs.dirscope:
        swvarfs.dirscope += "/*"
    elif swvarfs.dirscope == "/":
        swvarfs.dirscope += "*"

def open_archive_file(name, oflags):
    swvarfs = SWVARFS()  # is this a needed at this function
    # SWVARFS  # Should it be this?
    ret = open_serial_access_file_internal(name, oflags)
    if ret:
        swvarfs.openpath = name
        set_vcwd(swvarfs.vcwd, name)
    return ret

def open_serial_access_file_internal(name, oflags):
    swvarfs.opencwdpath = ""
    swvarfs.dirscope = ""
    swvarfs.fd = os.open(name, os.O_RDONLY)
    swvarfs.did_dup = 1
    if swvarfs.fd < 0:
        print("error (open_serial_access_file_internal) loc=1")
        del swvarfs
        return None
    if swvarfs_i_iopen(swvarfs, oflags):
        print("error (open_serial_access_file_internal) loc=2")
        del swvarfs
        return None
    swvarfs.format = uinfile_get_type(swvarfs.format_desc)
    swvarfs.has_leading_slash = uinfile_get_has_leading_slash(swvarfs.format_desc)
    return swvarfs

def swvarfs_open_directory(name):
    # swvarfs = SWVARFS() # is this needed at this function?
    ret = swvarfs_open_directory_internal(name)
    if ret:
        swvarfs.openpath = name
        set_vcwd(swvarfs.vcwd, name)
    swvarfs.format_desc = None
    return ret

def swvarfs_open(name, oflags, p_mode):
    statrc = 0
    mode = 0
    if p_mode == 0:
        if name and name != "-":
            try:
                stbuf = os.stat(name)
                mode = stbuf.st_mode
            except:
                statrc = -1
    else:
        mode = p_mode
    if name and name != "-" and statrc == 0 and (stat.S_ISDIR(mode) or mode == SWVARFS_S_IFDIR):
        rval = swvarfs_open_directory(name)
    else:
        rval = open_archive_file(name, oflags)
    return rval

def swvarfs_open_directory_internal(name):
    swvarfs.format = uinc.UINFILE_FILESYSTEM
    swvarfs.opencwdpath = [0] * 256
    if os.getcwd() is None:
        sys.stderr.write("swvarfs_open: cwd name too long.\n")
        del swvarfs
        return None
    swvarfs.opencwdpath = os.getcwd()
    swvarfs.direntpath = name
    set_dirscope(name)
    swlib_slashclean(swvarfs.direntpath)
    swlib_slashclean(swvarfs.opencwdpath)
    swvarfs.did_dup = 0
    swvarfs.fd = -1
    return swvarfs

def swvarfs_opendup_with_name(ifd, oflags, mode, name):
    # swvarfs = SWVARFS() M
    if name is None:
        return swvarfs_opendup(ifd, oflags, mode)
    # swvarfs.fd = uinfile_opendup_with_name(ifd, 0, swvarfs.format_desc, oflags, name) # Original
    swvarfs.fd = uinfile_opendup_with_name(ifd, mode, swvarfs.format_desc, oflags, name)
    if swvarfs.fd < 0:
        del swvarfs
        return None
    elif swvarfs.fd == ifd:
        swvarfs.did_dup = 0
    else:
        swvarfs.did_dup = 1
    swvarfs_i_init(swvarfs)
    if swvarfs_i_iopen(swvarfs, oflags):
        return None
    swvarfs.format = uinfile_get_type(swvarfs.format_desc)
    return swvarfs

def swvarfs_opendup(ifd, oflags, mode):
    swvarfs = SWVARFS()
    SWVARFS_E_DEBUG2("ENTERING mode=%d", oflags)
    if swvarfs is None:
        return None
    SWVARFS_E_DEBUG("")
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    # swvarfs.fd = uinfile_opendup(ifd, 0, swvarfs.format_desc, oflags) # Original - Paul Weber
    swvarfs.fd = uinfile_opendup(ifd, mode, swvarfs.format_desc, oflags)
    SWVARFS_E_DEBUG("")
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    if swvarfs.fd < 0:
        SWVARFS_E_DEBUG("")
        del swvarfs
        return None
    elif swvarfs.fd == ifd:
        SWVARFS_E_DEBUG("did_dup = 0")
        swvarfs.did_dup = 0
    else:
        SWVARFS_E_DEBUG("did_dup = 1")
        swvarfs.did_dup = 1
    SWVARFS_E_DEBUG("")
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    swvarfs_i_init(swvarfs)
    SWVARFS_E_DEBUG("")
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    if swvarfs_i_iopen(swvarfs, oflags):
        return None
    SWVARFS_E_DEBUG2("LOWEST FD=%d", show_nopen())
    swvarfs.format = uinfile_get_type(swvarfs.format_desc)
    return swvarfs

def swvarfs_u_readlink(swarfs, path, buf, bufsize):
    st = os.stat(path)
    if swvarfs and swvarfs.format != uinc.UINFILE_FILESYSTEM:
        fd = swvarfs_u_open(swvarfs, path)
        if fd < 0:
            return -4
        if swvarfs.format != uinc.UINFILE_FILESYSTEM:
            if swvarfs_file_has_data(swvarfs):
                taru_pump_amount2(-1, fd, -1, -1)
        if swvarfs_u_fstat(swvarfs, fd, st):
            swvarfs_u_close(swvarfs, fd)
            return -3
        if stat.S_ISLNK(st.st_mode):
            s = ahsstaticgettarlinkname(ahs_vfile_hdr(swvarfs.ahs))
        else:
            swvarfs_u_close(swvarfs, fd)
            return -2
        buf[:bufsize-1] = s
        buf[bufsize-1] = '\0'
        if len(s) >= bufsize - 1:
            ret = -len(s)
        else:
            ret = len(s)
        swvarfs_u_close(swvarfs, fd)
    else:
        try:
            # ret = os.readlink(path, buf, bufsize)
            ret = os.readlink(path)
            if ret >= bufsize:
                print("readlink (%s) : linkname too long" % path)
                buf[bufsize - 1] = '\0'
                ret = -ret
            elif ret == 0:
                print("readlink (%s) : has empty linkname" % path)
                ret = -1
                buf[0] = '\0'
            else:
                print("readlink (%s) : %s" % (path, os.strerror(ret)))
                buf[ret] = '\0'
        except OSError as e:
            print("readlink ({}) : {}".format(path, os.strerror(e.errno)))
            ret = -1
    return ret

def swvarfs_vchdir(swvarfs, path):
    ret = 0
    newvcwd = os.getcwd()
    ret = construct_new_path_from_pathspec(swvarfs, newvcwd, path)
    if ret == 0:
        os.chdir(newvcwd)
        ret = swvarfs_setdir(swvarfs, newvcwd)
    return ret

def swvarfs_setdir(swvarfs, path):
    if (path is None or len(path) == 0) and swvarfs.format != uinc.UINFILE_FILESYSTEM:
        swvarfs.dirscope = ""
        return 0
    depth = 0
    swlib_resolve_path(path, depth, None)
    if depth < 0:
        return -1
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        if is_name_in_scope(strob_str(swvarfs.openpath), path, None) == 0:
            print("directory [{}] not in current scope of {} openpath={}".format(path, strob_str(swvarfs.dirscope),
                                                                                 strob_str(swvarfs.openpath)))
            return -2
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        dirx = DirContext()
        stbuf = 0
        dirx.statbuf = None
        dirx.dirp = None
        dirx.dp = None
        dirx.ptr = None
        swvarfs.clear_all_dir_context()
        statrc = os.stat(path)
        if statrc or not stat.S_ISDIR(stbuf.st_mode):
            sys.stderr.write("swvarfs_setdir : {} : {}".format(path, os.error(errno)))
            ret = -1
        else:
            swvarfs.stackix = 0
            swvarfs.direntpath = path
            ret = 0
        try:
            dp = os.scandir(path)

        except OSError as e:
            print("{} : {}".format(path, os.strerror(e.errno)))
            swvarfs.derr = -1
            return -3
        dirx.dp = dp
        swvarfs.add_dir_context(dirx)
        ret = 0
    else:
        st = None
        espipe = 0
        swvarfs.makefilechain = 1
        swvarfs.current_filechain = 0
        if espipe:
            pass
        else:
            if uxfio_lseek(swvarfs.fd, 0, os.SEEK_SET):
                sys.stderr.write("uxfio_lseek error")
        if path == "." or path == "./":
            if espipe:
                return -4
            else:
                return 0
        if swvarfs.u_lstat(path, st):
            return -5
        existing = swvarfs.current_filechain
        if existing is None:
            sys.stderr.write("(struct fileChain*)existing is null. fatal.")
            return -6
        old_pos = uxfio_lseek(swvarfs.fd, 0, uxfioc.UXFIO_SEEK_VCUR)
        if old_pos < 0:
            sys.stderr.write("uxfio_lseek error ret = {}".format(old_pos))
            return -7
        header_offset = existing.header_offsetFC
        if header_offset - old_pos >= 0:
            SW_E_FAIL3("internal error old_pos=%s  header_offset=%s" % (
            swlib_imaxtostr(old_pos, None), swlib_imaxtostr(header_offset, None)), None, None)
            return -8
        if uxfio_lseek(swvarfs.fd, header_offset - old_pos, uxfioc.UXFIO_SEEK_VCUR) < 0:
            SW_E_FAIL("uxfio_lseek error")
            return -9
        clear_filechain(swvarfs)
        ret = 0
    set_vcwd(swvarfs.vcwd, path)
    set_dirscope(path)
    return ret

def u_lstat(swvarfs, path, st):
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        return swvarfs_i_u_lstat_fs(swvarfs, path, st)
    else:
        return swvarfs_i_u_lstat_archive(swvarfs, path, st)

def swvarfs_u_lstat(swvarfs, path, st):
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        return swvarfs_i_u_lstat_fs(swvarfs, path, st)
    else:
        return swvarfs_i_u_lstat_archive(swvarfs, path, st)

def swvarfs_close(swvarfs):
    ret = 0
    if swvarfs.did_dup:
        os.close(swvarfs.fd)
    strob_close(swvarfs.opencwdpath)
    strob_close(swvarfs.openpath)
    strob_close(swvarfs.u_name)
    strob_close(swvarfs.tmp)
    strob_close(swvarfs.dirscope)
    if swvarfs.stack is not None:
        strob_close(swvarfs.stack)
    if swvarfs.direntpath is not None:
        strob_close(swvarfs.direntpath)
    if swvarfs.do_close_ahs:
        ahs_close(swvarfs.ahs)
    if swvarfs.uinformat_close_on_delete:
        if swvarfs.format_desc:
            ret = uinfile_close(swvarfs.format_desc)
    else:
        ret = 0
    swvarfs.delete_filechain()
    hllist_close(swvarfs)
    taru_delete(swvarfs)
    return ret

def clear_filechain(swvarfs):
    delete_filechain(swvarfs)
    swvarfs.head = None
    swvarfs.tail = None

def swvarfs_i_attach_filechain(new):
    new.prevFC = swvarfs.tail
    new.nextFC = None
    if swvarfs.tail:
        swvarfs.tail.nextFC = new
    swvarfs.tail = new
    if not swvarfs.head:
        swvarfs.head = swvarfs.tail

def stat_node(swvarfs, fp_path, statbuf):
    path = fp_path
    i = 0
    bufsize = 300
    linkbuf = None
    totallinkbuf = None
    while i < swvarfs.n_loop_symlinks:
        if swvarfs.f_stat < 0:
            print("{}: getnext_dirent: {}() failed: {}" + swlib_utilname_get() + swvarfs_get_stat_syscall(swvarfs))
            swvarfs.derr = -2
            return -1
        if stat.S_ISLNK(statbuf.st_mode):
            break
        else:
            pass
        if stat.S_ISLNK(statbuf.st_mode):
            if linkbuf is None:
                linkbuf = strob_open(bufsize + 2)
            if totallinkbuf is None:
                totallinkbuf = strob_open(bufsize)
                strob_strcpy(totallinkbuf, path)
            strob_memset(linkbuf, '\0', bufsize + 2)
            # ret = os.readlink(strob_str(totallinkbuf), strob_str(linkbuf), bufsize)
            ret = os.readlink(strob_str(totallinkbuf))
            if ret < 0 or ret >= bufsize - 1:
                if ret < 0:
                    sys.stderr.write(
                        "{}: readlink failed: {} on {}".format(swlib_utilname_get(), strob_str(totallinkbuf),
                                                               os.error()))
                else:
                    sys.stderr.write("{}: readlink failed: program buffer: {}".format(swlib_utilname_get(),
                                                                                      strob_str(totallinkbuf)))
                return -1
            if strob_str(linkbuf)[0] == '/':
                strob_strcpy(totallinkbuf, strob_str(linkbuf))
            else:
                strob_strcat(totallinkbuf, "/")
                strob_strcat(totallinkbuf, strob_str(linkbuf))
            path = strob_str(totallinkbuf)
        else:
            break
    if linkbuf:
        strob_close(linkbuf)
    if totallinkbuf:
        strob_close(totallinkbuf)
    if i >= swvarfs.n_loop_symlinks:
        sys.stderr.write("{}: getnext_dirent: recursive {} failed on {}".format(swlib_utilname_get(),
                                                                                swvarfs_get_stat_syscall(swvarfs),
                                                                                fp_path))
        swvarfs.derr = -2
        return -2
    taru_statbuf2filehdr(ahs_vfile_hdr(swvarfs.ahs), statbuf, None, None, None)
    ahsstaticsettarfilename(ahs_vfile_hdr(swvarfs.ahs), None)
    if stat.S_ISLNK(statbuf.st_mode):
        swvarfs.set_linkname(-1, fp_path)
    else:
        ahsstaticsetpaxlinkname(ahs_vfile_hdr(swvarfs.ahs), None)
    return 0

def swvarfs_uxfio_fcntl(swvarfs, cmd, value):
    ret = 0
    if swvarfs.fd >= uxfioc.UXFIO_FD_MIN:
        ret = fcntl.fcntl(swvarfs.fd, cmd, value)
        if ret == 0:
            if cmd == uxfioc.UXFIO_F_SET_BUFTYPE:
                fcntl.fcntl(swvarfs.fd, uxfioc.UXFIO_F_SET_BUFACTIVE, uxfioc.UXFIO_ON)
                swvarfs.uxfio_buftype = value
            elif cmd == uxfioc.UXFIO_F_ARM_AUTO_DISABLE:
                if value:
                    swvarfs.uxfio_buftype = uxfioc.UXFIO_BUFTYPE_NOBUF
        return ret
    else:
        return 0

def swvarfs_get_ahs(swvarfs):
    return swvarfs.ahs

def swvarfs_set_ahs(swvarfs, ahs):
    if swvarfs.do_close_ahs:
        ahs_close(swvarfs.ahs)
    swvarfs.ahs = ahs
    swvarfs.do_close_ahs = 0

def swvarfs_fd(swvarfs):
    return swvarfs.fd

def swvarfs_header(swvarfs):
    return swvarfs.vfile_hdr

def swvarfs_u_open(swvarfs, name):
    ret = 0
    if ud_find_fd(swvarfs, -1) < 0:
        sys.stderr.write("too many open swvarfs:u_fd descriptors.\n")
        return -1
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        ret = swvarfs_i_u_open_fs(swvarfs, name)
    else:
        ret = swvarfs_i_u_open_archive(swvarfs, name)
    return ret

def swvarfs_u_close(swvarfs, fd):
    u_index = 0
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        # if (u_index = ud_find_fd(fd)) < 0:
        if u_index == swvarfs.ud_find_fd(fd):
            sys.stderr.write("close error, expecting desc=%d. given=%d\n" % (swvarfs.u_fd, fd))
            return -1
        if fd != swvarfs.u_fd and fd != swvarfs.u_fd_set[u_index].u_fd:
            sys.stderr.write("close error, expecting desc=%d. given=%d\n" % (swvarfs.u_fd, fd))
            return -1
        if swvarfs.u_fd_set[u_index].u_linkname_:
            del swvarfs.u_fd_set[u_index].u_linkname_
            swvarfs.u_fd_set[u_index].u_linkname_ = None
        if swvarfs.g_linkname_:
            del swvarfs.g_linkname_
            swvarfs.g_linkname_ = None
        if swvarfs.u_current_name_:
            del swvarfs.u_current_name_
            swvarfs.u_current_name_ = None
        if swvarfs.u_fd_set[u_index].u_current_name_:
            del swvarfs.u_fd_set[u_index].u_current_name_
            swvarfs.u_fd_set[u_index].u_current_name_ = None
        swvarfs.ud_unset_fd(fd)
        swvarfs.u_fd = -1
        ret = uxfio_close(fd)
        return ret
    else:
        ret = swvarfs_i_u_close_archive(swvarfs, fd)
        return ret

def swvarfs_u_usr_stat(swvarfs, fd, ofd):
    if os.fstat(fd):
        return 1
    name = swvarfs_u_get_name(swvarfs, fd)
    linkname = swvarfs_u_get_linkname(swvarfs, fd)
    st = os.fstat(fd)
    swlib_write_stats(name, linkname, st, 0, "", ofd, None)
    return 0

def swvarfs_u_get_name(swvarfs, fd):
    u_index = ud_find_fd(swvarfs, fd)
    if u_index < 0:
        print("swvarfs invalid file descriptor {}.".format(fd))
        return None
    return swvarfs.u_fd_set[u_index].u_current_name_

def swvarfs_u_get_linkname(swvarfs, fd):
    if fd != swvarfs.u_fd:
        sys.stderr.write("error, expecting %d.\n" % swvarfs.u_fd)
        return None
    return swvarfs.g_linkname_

def swvarfs_u_fstat(swvarfs, fd, st):
    print("swarvfs"+swvarfs+" and st"+st+"are not used in this function")
    ret = os.fstat(fd)
    return ret

def swvarfs_stop_function(swvarfs, fc):
    swvarfs.f_do_stop_ = fc

def swvarfs_dirent_reset(swvarfs):
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        pass
    else:
        os.lseek(swvarfs.fd, 0, os.SEEK_SET)
        swvarfs.eoa = 0
    return 0

def swvarfs_get_next_dirent(swarfs, st):
    s = None
    if swvarfs.format == uinc.UINFILE_FILESYSTEM:
        name = swvarfs_get_next_dirent_fs(swvarfs, st)
        is_at_end = 0
    else:
        peoa = swvarfs.eoa
        name = swvarfs_get_next_dirent_archive_NEW(swvarfs, st, peoa)
        is_at_end = peoa
    if is_at_end == 0:
        tartype = ahs_get_tar_typeflag(swvarfs.ahs)
        if tartype < 0:
            sys.stderr.write("%s: unrecognized file type [%d] for file: %s\n" % (
                swlib_utilname_get(), tartype, name if name else "<null>"))
            set_vcwd(swvarfs.vcwd, name if name else "")
        elif tartype == tarfile.DIRTYPE:
            set_vcwd(swvarfs.vcwd, name if name else "")
        else:
            set_vcwd(swvarfs.vcwd, name if name else "")
            if s == strrchr(strob_str(swvarfs.vcwd), '/'):
                s = '\0'
    else:
        name = None
    return name

def swvarfs_dirent_err(swvarfs):
    return swvarfs.derr

def swvarfs_get_hllist(swvarfs):
    return swvarfs.link_record

def swvarfs_get_format(swvarfs):
    return swvarfs.format

def swvarfs_file_has_data(swvarfs):
    type = ahs_get_tar_typeflag(swvarfs.ahs)
    return type == tarfile.REGTYPE

def swvarfs_get_tarheader_flags(swvarfs):
    return swvarfs.taru.taru_tarheaderflags

def swvarfs_set_tarheader_flags(swvarfs, flags):
    swvarfs.taru.taru_tarheaderflags = flags

def swvarfs_set_tarheader_flag(flag, n):
    taru_set_tarheader_flag(swvarfs.taru, flag, n)

def swvarfs_set_stat_syscall(s):
    if s[0] == 'l':
        swvarfs.f_stat = os.lstat
    elif s[0] == 's':
        swvarfs.f_stat = os.stat

def swvarfs_get_stat_syscall(swvarfs):
    if swvarfs.f_stat == os.lstat:
        return SWVARFS_VSTAT_LSTAT
    elif swvarfs.f_stat == os.stat:
        return SWVARFS_VSTAT_STAT
    else:
        sys.stderr.write("swvarfs: fatal error in swvarfs_get_stat_syscall\n")
        sys.exit(1)


varfs = SWVARFS()
swvarfs = SWVARFS()


def set_vcwd(cw, newvcwd):
    strob_strcpy(cw, newvcwd)
    if strob_str(cw) == "./":
        strob_str(cw)[1] = '\0'


def is_name_a_path_prefix(npath, nprefix):
    ret = 0
    trl = None
    if nprefix == "." or nprefix == "./":
        return 1
    if not nprefix or nprefix == "":
        return 0
    if nprefix[len(nprefix) - 1] == '/':
        trl = nprefix[:len(nprefix) - 1]
    if npath[:2] == "./":
        path = npath[2:]
    else:
        path = npath
    if nprefix[:2] == "./":
        prefix = nprefix[2:]
    else:
        prefix = nprefix

    if len(prefix) == 0 or len(path) == 0 or path == prefix:
        pass
    else:
        s = path.find(prefix)
        if s != -1 and s == 0 and (len(path) == len(prefix) or path[len(prefix)] == '/'):
            ret = 1
    if trl:
        trl = '/'
    return ret


def handle_trailing_slash(mode, name, pflag):
    if mode == "drop":
        if name[len(name) - 1] == '/':
            name = name[:len(name) - 1]
            pflag = 1
        else:
            pflag = 0
    else:
        if pflag:
            name = name + '/'
    return pflag, name


def is_name_in_scope(dirscope, name, namest):
    pathcomps = strar_open()
    if len(dirscope) == 0 or dirscope == "/":
        return 1
    s = name.find("./")
    if s != -1 and s == 0 and len(name) > 2:
        name1 = name[2:]
    else:
        name1 = name
    if len(name1) + 4 < len(dirscope):
        return 0
    s = dirscope.find(name1)
    if s != -1 and len(name1) >= len(dirscope) - 2:
        if namest is not None and not stat.S_ISDIR(namest.st_mode):
            return 0
        else:
            return 1
    tmp = strob_open(10)
    tmp1 = strob_open(10)
    strob_strcpy(tmp, dirscope)
    strob_strcpy(tmp1, name1)
    if not strob_str(tmp).find(strob_str(tmp1)):
        return
    pc = strob_strtok(tmp, s, "/")
    while pc:
        strar_add(pathcomps, pc)
        pc = strob_strtok(tmp, None, "/")
    strob_close(tmp)
    return


class DirContext:  # Stack for directory-tree walking routine

    def __init__(self):
        self.statbuf = None
        self.dirp = None
        self.dp = None
        self.ptr = None

global __FILE__, __LINE__, __FUNCTION__
def SWLIB_ASSERT(arg):
    global __FILE__, __LINE__, __FUNCTION__
    FrameRecord()
    return swlib_assertion_fatal(arg, "", __FILE__, __LINE__, __FUNCTION__)

def SWLIB_ALLOC_ASSERT(arg):
    FrameRecord()
    return swlib_assertion_fatal(arg, "out of memory", __FILE__, __LINE__, __FUNCTION__)

def filename_does_match(name, candidate):
    return name == candidate


def process_vcwd(pathcomps, source):
    tmp = []

    if source.startswith("\\"):
        pathcomps.append(source)
        source = source[1:]
        s = source[2:]
    else:
        s = source

    pc = s.split("/")
    for p in pc:
        pathcomps.append(p)

    return
