

# sd.sw_def


# Not sure how to incorporate this definition

#
# Copyright (C) 2021 
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

# Page 139
# 5.2 Software Definition File Format
# The software definition files contain the software structure and the detailed 
# attributes for distributions, installed_software, bundles, products, 
# subproducts, filesets, files, and control_files.  While information on 
# installed software is represented in this form as input to or output from the 
# various software administration utilities, the actual storage of this 
# metadata for installed software is undefined. This section describes the 
# format of the software definition files:

#  · The INDEX file contains the definition of distribution or 
#    installed_software objects as well as the software objects contained 
#    within those software_collections. The information in this file is 
#     primarily used in selection phases of the utilities.

#  · The INFO file contains the definition of the software files and 
#    control_files for a product or fileset within a distribution or 
#    installed_software object. The information in this file is primarily used
#    in analysis and execution phases of the utilities.

#  · The PSF (product specification file) also contains the definition of 
#    distribution attributes, software objects, and the software files and 
#    control_files for the product and fileset software objects. This file is 
#    created by the software vendor and used by the packaging tool to create the
#    distribution, represented by the INDEX and INFO files, in the software 
#    packaging layout.

#     The PSF supports the same syntax as the INDEX and INFO files. Additional 
#     syntactic constructs are supported for specifying files and control_files.
#     This file is used in selection, analysis and packaging Phases of the 
#     swpackage command.

# Additionally, there is a space file that is created by the software vendor 
# for additional disk space needed for a product or fileset. This file is used 
# in analysis phase of the swinstall command to account for additional disk 
#  space required.

# 5.2.1 Software Definition File Syntax

# The INDEX and INFO files have essentially the same syntax and semantics as the
# PSF. One key difference is that the INDEX file does not contain control_file 
# and file definitions, the INFO file contains only control_file and file 
# definitions, and the PSF file contains all definitions. In a distribution, 
# each product and fileset has a separate INFO file.

# The software specification file syntax is as follows.

# %token FILENAME_CHARACTER_STRING /* as defined in Glossary */
# %token NEWLINE_STRING /* as defined in Glossary */
# %token PATHNAME_CHARACTER_STRING /* as defined in Glossary */
# %token SHELL_TOKEN_STRING /* as defined in Glossary */
# %token WHITE_SPACE_STRING /* as defined in Glossary */

# %start software_definition_file
# %%

# software_definition_file : INDEX
#                          | INFO
#                          | PSF
#                          ;
software_definition_file="INDEX"
# INDEX                    : soc_definition
#                            soc_contents
#                          ;

#INDEX=soc_definition|soc_contents

# INFO                     : info_contents
#                          ;
#INFO=info_contents

# PSF                      : distribution_definition
#                            soc_contents
#                          ;

#PSF=distribution_devinition|soc_contents

# Media imported from clases.py
# media                    : /* empty */
#                          | media_definition
#                          ;

# vendors                  : /* empty */
#                          | vendors NEWLINE_STRING vendor_definition
#                          | vendor_definition
#                          ;

# bundles                  : /* empty */
#                          | bundles NEWLINE_STRING bundle_definition
#                          | bundle_definition
#                          ;


# products                 : /* empty */
#                          | products NEWLINE_STRING product_specification
#                          | product_specification
#                          ;

# product_specification    : product_definition
#                           product_contents
#                          ;

# subproducts              : /* empty */
#                          | subproducts NEWLINE_STRING subproduct_definition
#                          | subproduct_definition
#                          ;

# filesets                 : filesets NEWLINE_STRING fileset_specification
#                          | fileset_specification
#                          ;

# fileset_specification    : fileset_definition
#                            fileset_contents
#                            /* fileset contents not valid in INDEX files */
#                          ;

# control_files            : /* empty */
#                          | control_files NEWLINE_STRING control_file_definition
#                          | control_file_definition
#                          ;

# files                    : /* empty */
#                          | files NEWLINE_STRING file_definition
#                          | file_definition
#                          ;

# fileset_contents         : fileset_contents NEWLINE_STRING fileset_content_items
#                          | fileset_content_items
#                          ;

# fileset_content_items    : control_files
#                          | files
#                          ;

# info_contents            : info_contents NEWLINE_STRING info_content_items
#                          | info_content_items
#                          ;

# info_content_items       : control_files
#                          | files
#                          ;

# product_contents         : product_contents NEWLINE_STRING product_content_items
#                          | product_content_items
#                          ;

# product_content_items    : control_files
#                            /* control_files not valid in INDEX files */
#                          | subproducts
#                          | filesets
#                          ;

# soc_contents             : soc_contents NEWLINE_STRING soc_content_items
#                          | soc_content_items
#                          ;
# soc_contents

# soc_content_items        : vendors
#                          | bundles
#                          | products
#                          ;

# soc_definition           : distribution_definition
#                          | installed_software_definition
#                          ;

# distribution_definition  : software_definition
#                             media
#                          ;

# media_definition         : software_definition
#                          ;

# installed_software_definition : software_definition
#                          ;

# vendor_definition        : software_definition
#                          ;

# bundle_definition        : software_definition
#                          ;

# product_definition       : software_definition
#                          ;

# subproduct_definition    : software_definition
#                          ;

# fileset_definition       : software_definition
#                          ;

# control_file_definition  : software_definition
#                          | extended_definition
#                            /* extended_definition only valid in PSF files */
#                          ;

# file_definition          : software_definition
#                          | extended_definition
#                            /* extended_definition only valid in PSF files */
#                          ;

# software_definition      : object_keyword NEWLINE_STRING
# attribute_value_list
#                          ;

# attribute_value_list     : /* empty */
#                          | attribute_value_list attribute_definition NEWLINE_STRING
#                          | attribute_definition NEWLINE_STRING
#                          ;

# attribute_definition     : attribute_keyword WHITE_SPACE_STRING attribute_value
#                          ;

# object_keyword           : FILENAME_CHARACTER_STRING
#                          ;

# attribute_keyword        : FILENAME_CHARACTER_STRING
#                          ;

# extended_definition      : extended_keyword WHITE_SPACE_STRING attribute_value
#                          ;

# extended_keyword         : FILENAME_CHARACTER_STRING
#                          ;

# attribute_value          : attribute_value WHITE_SPACE_STRING single_value
#                          | single_value
#                          | ’<’ WHITE_SPACE_STRING PATHNAME_CHARACTER_STRING
#                          | ’<’ PATHNAME_CHARACTER_STRING
#                          ;

# single_value             : SHELL_TOKEN_STRING
#                          ;

# The following syntax rules are applicable to software definition files:

#   1. All keywords and values are represented as character strings.

#   2. Each keyword is located on a separate line. Keywords can be preceded by 
#      white space (tab, space). White space separates the keyword from the 
#      value.

#   3. Comments can be placed on a line by themselves or after the 
#      keyword-value syntax. They are designated by preceding them with the # 
#      (pound) character. The way in which comments are used in INDEX and INFO 
#      is undefined

#   4. All object keywords have no values. All attribute keywords have one or 
#      more values.

#   5. An attribute value ends on the same line as the keyword with one 
#      exception. Attribute values can span lines if and only if the value is 
#      prefixed and suffixed with the " (double quote) character.

#   6. When an attribute value begins with < (less than), the remainder of the 
#      string value is interpreted as a filename whose contents will be used as
#      a quoted string value for the attribute. For INDEX files, the filename 
#      is a path relative to the control directory for that distribution, 
#      product, or fileset. For PSF files, the filename is a path to a file on 
#      the host that contains the file.

#   7. The use of " (double quote) is not required when defining a single line 
#      string value that contains embedded white space. Trailing white space is 
#      removed; embedded white space is used. The quotes can be used.

#   8. The " (double quote), # (pound), and \ (backslash) characters can be 
#      included in multiline string values by ‘‘escaping’’ them with \ 
#      (backslash).

#   9. The order of attributes is not significant, except that the 
#      layout_version is the first attribute defined in an INDEX file for a 
#      distribution or installed_software object.

