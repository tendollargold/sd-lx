
# phases.swask.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals


"""
    4.0 Execution Phase Systems Management: Distributed Software Administration
    swask utility Page 79 and 80

    Extended Options
    The swask utility supports the following extended options. The description in 
    Chapter 3 applies.
"""

swaskoptions = {
    'autoselect_dependencies': bool(True),
    'distribution_source_directory': "/SD_CDROM",
    'distribution_source_serial': "/dev/rmt0",
    'logfile': "/var/adm/sw/swask.log",
    'loglevel': "1",
    'ask': 'bool(True)',
    'software': "",
    'verbose': "1"
}
def swask_options():
    """Collection of extended options for swask utility. api"""
    # implementation_defined_value is set via the setting in swdefaults?
    swaskoptions.update(globals())

# There are two phases in the swask utility:
#    1. Selection phase
#    2. Execution phase

#
# Selection Phase
#

# The software selections apply to the software available from the distribution 
# source if the -s option was specified. Otherwise, the software selections apply to 
# software that has been already installed on the targets. Each specified selection is
# added to the selection list after it passes the following checks:

#    · If the -s option is specified and the selection is not available from the 
#      source, generate an event. If the -s option 
#      is not specified and the selection is not available from the target, generate 
#      an event.
#      (SW_ERROR: SW_SELECTION_NOT_FOUND)

#    · If a unique version can not be identified, generate an event.
#      (SW_ERROR: SW_SELECTION_NOT_FOUND_AMBIG)

# Add any dependencies to the selection list if autoselect_dependencies=true.

#
# Execution Phase
#

# For each selected software that has a request control script:

#    · If it does not already exist, build the necessary control directories of the 
#      exported catalog structure to hold the response file for that software object.

#    · Set the value of the SW_CONTROL_DIRECTORY environment variable to the directory
#      below which the request script writes the response file. It may be set to the 
#      control directory where the response file will eventually be held, or it may 
#      be set to another, temporary, directory.

#      If a response file can be found from one of the following sources, searched 
#      in the order specified, then the implementation ensures that the response file 
#      is contained within the directory pointed to by SW_CONTROL_DIRECTORY. The means
#      for doing this (e.g., copy, link, symlink) is undefined.

#        1. If -c catalog was specified, any response file already existing below 
#           that catalog
#        2. If -s was not specified, any response file from the catalog of the target #           or targets specified
#        3. If -s was specified, any response file already existing in the source 
#           catalog.

#    · If ask=true, execute the request script. If ask=as_needed, execute the request 
#      script only if a response file does not already exist in the control directory.

#    · The request script writes a single response file in the control directory11 
#      defined by the supplied environment variable SW_CONTROL_DIRECTORY.

#    · If a request script returns an error and enforce_scripts=true, generate an 
#      event and invoke the implementation defined error handling procedures.
# (SW_ERROR: SW_REQUEST_ERROR)

#      If a request script returns an error and enforce_scripts=false, generate an 
#      event.
#      (SW_WARNING: SW_REQUEST_ERROR)

#      If a request script returns a warning, generate an event.
#      (SW_WARNING: SW_ASK_SCRIPT_WARNING)


