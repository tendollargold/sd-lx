# Copyright (C) 2016  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Various utility functions, and a utility class."""

from __future__ import absolute_import
from __future__ import unicode_literals
# from sd.cli.format import format_number
from sd.i18n import _
import sd.util
# import logging
import os
import time
from datetime import datetime,timezone

_USER_HZ = os.sysconf(os.sysconf_names['SC_CLK_TCK'])
#logger = logging.getLogger('sd')

def jiffies_to_seconds(jiffies):
    """Convert a number of jiffies to seconds. How many jiffies are in a second
    is system-dependent, e.g. 100 jiffies = 1 second is common.

    :param jiffies: a number of jiffies
    :return: the equivalent number of seconds
    """
    return int(jiffies) / _USER_HZ

def seconds_to_ui_time(seconds):
    """Return a human-readable string representation of the length of
    a time interval given in seconds.

    :param seconds: the length of the time interval in seconds
    :return: a human-readable string representation of the length of
      the time interval
    """
    if seconds >= 60 * 60 * 24:
        return "%d day(s) %d:%02d:%02d" % (seconds // (60 * 60 * 24),
                                           (seconds // (60 * 60)) % 24,
                                           (seconds // 60) % 60,
                                           seconds % 60)
    if seconds >= 60 * 60:
        return "%d:%02d:%02d" % (seconds // (60 * 60), (seconds // 60) % 60,
                                 (seconds % 60))
    return "%02d:%02d" % ((seconds // 60), seconds % 60)


def get_process_info(pid):
    """Return info dict about a process."""

    pid = int(pid)

    # Maybe true if /proc isn't mounted, or not Linux ... or something.
    if (not os.path.exists("/proc/%d/status" % pid) or
        not os.path.exists("/proc/stat") or
        not os.path.exists("/proc/%d/stat" % pid)):
        return

    ps = {}
    with open("/proc/%d/status" % pid) as status_file:
        for line in status_file:
            if line[-1] != '\n':
                continue
            data = line[:-1].split(':\t', 1)
            if len(data) < 2:
                continue
            data[1] = sd.util.rtrim(data[1], ' kB')
            ps[data[0].strip().lower()] = data[1].strip()
    if 'vmrss' not in ps:
        return
    if 'vmsize' not in ps:
        return

    boot_time = None
    with open("/proc/stat") as stat_file:
        for line in stat_file:
            if line.startswith("btime "):
                boot_time = int(line[len("btime "):-1])
                break
    if boot_time is None:
        return

    with open('/proc/%d/stat' % pid) as stat_file:
        ps_stat = stat_file.read().split()
        ps['start_time'] = boot_time + jiffies_to_seconds(ps_stat[21])
        ps['state'] = {'R' : _('Running'),
                       'S' : _('Sleeping'),
                       'D' : _('Uninterruptible'),
                       'Z' : _('Zombie'),
                       'T' : _('Traced/Stopped')
                       }.get(ps_stat[2], _('Unknown'))

    return ps

def show_lock_owner(pid):
    """Output information about process holding a lock."""

    ps = get_process_info(pid)
    if not ps:
        msg = _('Unable to find information about the locking process (PID %d)')
        #logger.critical(msg, pid)
        return

    msg = _('  The application with PID %d is: %s') % (pid, ps['name'])

    # logger.critical("%s", msg)
    # logger.critical(_("    Memory : %5s RSS (%5sB VSZ)"),
    #                format_number(int(ps['vmrss']) * 1024),
    #               format_number(int(ps['vmsize']) * 1024))

    ago = seconds_to_ui_time(int(time.time()) - ps['start_time'])
    #logger.critical(_('    Started: %s - %s ago'),
     #               sd.util.normalize_time(ps['start_time']), ago)
    #logger.critical(_('    State  : %s'), ps['state'])

    return

def create_date():
  # If this value is specified, swlist returns a sequence of characters
  # representing the date associated with the create_time attribute.

  # The format of this sequence of characters in the POSIX Locale is
  # equivalent to the default date format described in POSIX.2.
  #      date "+%a %b %e %H:%M:%S %Z %Y"
  # Create the time using UTC,then the conversion can be local time via
  # sd.util.normalize_time()
  cdate = datetime.now(timezone.utc)
  cdate = time.mktime(cdate.timetuple())
  cdate = int(cdate)
  return cdate

def install_date():
  # This is the date and time the software was installed
  # it is added to bundles, products, and filesets at the completion
  # of install

  idate = datetime.today().strftime('%Y%m%d%H%M.%S')
  return idate
def mod_date():
  # mod_time is set to the time a bundle, product, fileset was last modified

  # The format of this sequence of characters in the POSIX Locale is
  # equivalent to the default date format described in POSIX.2.
  #    date "+%a %b %e %H:%M:%S %Z %Y"
  mdate = create_date()
  return mdate

def mod_time():
  # A value which is set by the implementation to be the time that the catalog information for this object was last written.
  # Time is represented as seconds since the Epoch, as defined in POSIX.1.
  mtime = int(time.time())
  return mtime