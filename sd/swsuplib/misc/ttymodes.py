# ttymodes.py

#   Copyright (C) 1999,2007,2014  Jim Lowe
#   Copyright (C) 2023-2024 Paul Weber conversion to Python
#   All rights reserved.
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import termios
import enum

global save_termios, ttysavefd, ttystate
save_termios = None
ttysavefd = -1
ttystate = "RESET"

class TtySet(enum):
    RESET = 0
    RAW = 1
    CBREAK = 2

def swlib_tty_cbreak(fd): # put terminal into a cbreak mode
    global save_termios, ttysavefd, ttystate
    try:
        save_termios = termios.tcgetattr(fd)
    except:
        return -1
    buf = termios.tcgetattr(fd)
    buf[3] = buf[3] & ~(termios.ECHO | termios.ICANON)
    buf[6][termios.VMIN] = 1
    buf[6][termios.VTIME] = 0

    try:
        termios.tcsetattr(fd, termios.TCSAFLUSH, buf)
    except:
        return -1

    ttystate = "CBREAK"
    ttysavefd = fd
    return 0


def swlib_tty_raw(fd): # put terminal into a raw mode
    global ttysavefd, ttystate, save_termios
    buf = termios.tcgetattr(fd)
    try:
        buf = termios.tcgetattr(fd)
        save_termios = buf
        # c_lflag
        # echo off, canonical mode off, extended input
        # processing off, signal chars off
        buf[3] = buf[3] & ~(termios.ECHO | termios.ICANON | termios.IEXTEN | termios.ISIG)
        # c_iflag
        # no SIGINT on BREAK, CR-to-NL off, input parity
        # check off, don't strip 8th bit on input,
        # output flow control off
        buf[0] = buf[0] & ~(termios.BRKINT | termios.ICRNL | termios.INPCK | termios.ISTRIP | termios.IXON)
        # c_cflag
        # clear size bits, parity checking off
        buf[2] = buf[2] & ~(termios.CSIZE | termios.PARENB)
        buf[2] = buf[2] | termios.CS8
        # set 8 bits/char
        # c_oflag
        buf[1] = buf[1] & ~(termios.OPOST)
        # c_cc
        buf[6][termios.VMIN] = 1
        buf[6][termios.VTIME] = 0
        termios.tcsetattr(fd, termios.TCSAFLUSH, buf)
        ttystate = 'RAW'
        ttysavefd = fd
        return 0
    except:
        return -1


def swlib_tty_reset(fd): # restore terminal's mode
    global save_termios, ttystate
    if ttystate not in ['CBREAK', 'RAW']:
        return 0
    try:
        termios.tcsetattr(fd, termios.TCSAFLUSH, save_termios)
    except termios.error:
        return -1

    ttystate = 'RESET'
    return 0


def swlib_tty_atexit(): # can be set up by atexit(tty_atexit)
    global ttysavefd
    if ttysavefd >= 0:
        swlib_tty_reset(ttysavefd)


def swlib_tty_termios(): # let caller see original tty state
    global save_termios
    return save_termios

