# swi.py -- POSIX package decoding.
#   Copyright (C) 2004,2005,2006,2007 Jim Lowe
# swi_xfile -- Fileset/Dfiles/Pfiles Object
#    Copyright (C) 2005 Jim Lowe
#  Copyright (C) 2023 Paul Weber convert to python
#  All Rights Reserved.
#
#  COPYING TERMS AND CONDITIONS:
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA

import os
import sys
import time


from sd.debug import E_DEBUG, E_DEBUG2, SWSWINEEDDEBUG
from sd.cli.utils import mod_time, create_date
from sd.swsuplib.ls_list.gen_subs import ls_list
from sd.swsuplib.ls_list.ls_list import lslistc
from sd.swsuplib.versionid import swverid_open, swverid_set_namespace
from sd.swsuplib.swutillib import swlib_utilname_get, swutil_setup_xformat
from sd.swsuplib.cplob import (cplob_val, cplob_release, cplob_get_nused, cplob_open, cplob_get_list, cplob_add_nta)
from sd.swsuplib.misc.cstrings import strdup, strcmp
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.swextopt import set_opta, get_opta, swextopt_is_option_set, eOpts
from sd.swsuplib.swparse.swparse import sw_yyparse
from sd.swsuplib.misc.glbindex import glbindex_find_by_swpath_ex
from sd.swsuplib.misc.swheader import (SWHEADER_STATE, swheader_get_attribute, swheader_get_next_object, swheaderline_get_value,
                                       swheader_reset, swheader_get_current_offset, SWHEADER_PEEK_NEXT, SWHEADER_GET_NEXT, swheader_get_current_offset_p,
                                       swheader_store_state, swheader_set_current_offset_p, swheader_set_current_offset,
                                       swheader_restore_state, swheader_set_current_offset_p_value, swheader_get_single_attribute_value,
                                       swheader_getTarTypeFromTypeAttribute, swheader_open, swheader_set_image_head)
from sd.swsuplib.strob import strob_open, strob_str, strob_close, strob_strcpy
from sd.swsuplib.misc.strar import strar_close, strar_open, strar_get, strar_add
from sd.swsuplib.misc.swvarfs import swvarfs_close, swvarfs_uxfio_fcntl, swvarfs_get_uinformat
from sd.swsuplib.misc.swlib import (swlibc, SWLIB_ERROR, SWLIB_WARN, SW_IMPL_ERROR_DIE, SWLIB_ASSERT, swlib_doif_writef, swlib_basename_compare, fr, 
                                    swlib_pipe_pump, swlib_check_clean_path, swlib_is_sh_tainted_string, swlib_attribute_check_default,swlib_apply_location,
                                    swlib_return_relative_path, swlib_is_option_true, swlib_basename, swlib_open_memfd, swlib_unix_dircat)
from sd.swsuplib.misc.swicol import swicol_delete, swicol_create
from sd.swsuplib.misc.swpath import (swpath_get_pfiles, swpath_get_product_control_dir, swpath_get_basename, swpath_reset,
                                     swpath_parse_path, swpath_get_is_catalog, swpath_create_export, SWPATH_CTYPE_STORE,
                                     swpath_get_prepath,swpath_delete_export, swpath_ex_print, swpath_get_is_minimal_layout,
                                     swpath_set_dfiles, swpath_set_pfiles)
from sd.swsuplib.misc.swuts import swuts_create, swuts_compare, swuts_delete, swuts_add_attribute
from sd.swsuplib.swparse.swparse import swpc
from sd.swsuplib.misc.swheader import (swheader_close, swheaderline_write_debug, swheader_get_object_by_tag, swheader_get_image_head,
                                       swheader_get_next_attribute, swheader_goto_next_line, swheader_get_current_line,
                                       swheaderline_get_type, swheader_generate_swverid, swheaderline_get_keyword)

from sd.swsuplib.uxfio import uc, uxfio_close, uxfio_lseek, uxfio_get_dynamic_buffer, uxfio_write
from sd.swsuplib.uinfile.uinfile import uinfile_close, uinc, uinfile_get_swpath, uinfile_get_ztype
from sd.swsuplib.taru.ahs import ahsstaticgettarfilename, ahsstaticgettarlinkname
from sd.swsuplib.taru.taru import af, tc, taru_get_tar_filetype
from sd.swsuplib.taru.xformat import (xformat_close, xformat_open, xformat_set_ofd, xformat_get_swvarfs,
                                      xformat_get_ifd, xformat_set_tarheader_flag, xformat_u_open_file, xformat_u_close_file,
                                      xformat_is_end_of_archive, xformat_get_next_dirent, xformat_file_has_data)


UCHAR_MAX = 255

class SWI_Constants:
    SWI_FL_TRUE = 'T'
    SWI_FL_FALSE = 'F'
    SWI_FL_OFFSET_TARTYPE = 0
    SWI_FL_OFFSET_IS_VOLATILE = 1
    SWI_FL_OFFSET_USER_FLAG = 2
    SWI_FL_OFFSET_KEEP_FLAG = 3
    SWI_FL_OFFSET_PATH = 10
    SWI_FL_HEADER_LEN = 10
    SWI_BASE_ID_BEGIN = 85  # Sanity id
    SWI_BASE_ID_END = 170  # Sanity i
    SWI_I_TYPE_PACKAGE = 'K'
    SWI_I_TYPE_BUNDLE = 'B'
    SWI_I_TYPE_PROD = 'P'
    SWI_I_TYPE_XFILE = 'C'
    SWI_I_TYPE_AFILE = 'E'
    SWI_I_TYPE_SCRIPT = 'D'
    SWI_MAX_OBJ = 10
    SWI_SECTION_BOTH = 2
    SWI_SECTION_CATALOG = 1
    SWI_SECTION_STORAGE = 0
    SWI_RESULT_UNDEFINED = 'SW_NONE'
    SWI_RESULT_SUCCESS = 'SW_SUCCESS'
    SWI_RESULT_WARNING = 'SW_WARNING'
    SWI_RESULT_FAILURE = 'SW_ERROR'
    SWINSTALL_CATALOG_TAR = 'catalog.tar'
    SWI_XFILE_TYPE_FILESET = 0
    SWI_XFILE_TYPE_DFILES = 1
    SWI_XFILE_TYPE_PFILES = 2


swic = SWI_Constants

class SWI_FILELIST:
    def __init__(self):
        self.ar = []

class SWI_BUNDLE:
    def __init__(self):
        #instance fields found by C++ to Python Converter:
        self.type_id = ''
        self.is_active = 0
        self.tag = ''
        self.create_time = create_date()
        self.mod_time = mod_time()


class SWI_PRODUCT:

    def __init__(self):
        #instance fields found by C++ to Python Converter:
        self.p_base = SWI_BASE()
        self.xfile = None
        self.swi_co = [None for _ in range(swic.SWI_MAX_OBJ)]
        self.package_path = '\0'
        self.control_dir = '\0'
        self.filesets = None
        self.is_selected = 0
        self.is_compatible = 0
        self.INDEX_ordinal = 0


class SWI_PACKAGE:

    def __init__(self):
        #instance fields found by C++ to Python Converter:
        self.base = SWI_BASE()
        self.swi_co = [None for _ in range(swic.SWI_MAX_OBJ)]
        self.swi_sc = None
        self.exlist = None
        self.swdef_fd = 0
        self.current_product = None
        self.dfiles = None
        self.current_xfile = None
        self.did_parse_def_file = 0
        self.prev_swpath_ex = None
        self.dfiles_attribute = '\0'
        self.pfiles_attribute = '\0'
        self.catalog_start_offset = 0
        self.catalog_end_offset = 0
        self.catalog_length = 0
        self.target_path = '\0'
        self.target_host = '\0'
        self.catalog_entry = '\0'
        self.installed_software_catalog = '\0'
        self.is_minimal_layout = 0
        self.location = '\0'
        self.qualifier = '\0'
        self.installer_sig = '\0'
        self.installed_catalog_owner = '\0'
        self.installed_catalog_group = '\0'
        self.installed_catalog_mode = '\0'


    # relative to  target_path

class SWI_DISTDATA:

    def __init__(self):
        #instance fields found by C++ to Python Converter:
        self.did_part1 = 0
        self.dist_tag = '\0'
        self.dist_revision = '\0'
        self.bundle_tags = None
        self.product_tags = None
        self.product_revisions = None
        self.vendor_tags = None
        self.catalog_bundle_dir1 = '\0'


class SWI:

    def __init__(self):
        #instance fields found by C++ to Python Converter:
        self.xformat = None
        self.swvarfs = None
        self.uinformat = None
        self.swpath = None
        self.swi_pkg = None
        self.nullfd = 0
        self.verbose = 0
        self.tarbuf = str(['\0' for _ in range(1024)])
        self.distdata = None
        self.opt_alt_catalog_root = 0
        self.exported_catalog_prefix = '\0'
        self.excluded_file_conflicts_fd = 0
        self.replaced_file_conflicts_fd = 0
        self.pending_file_conflicts_fd = 0
        self.swicol = None
        self.swevent_list = None
        self.opta = None
        self.swc_id = 0
        self.debug_events = 0
        self.target_version_id = None
        self.does_have_payload = 0
        self.xformat_close_on_delete = 0
        self.swvarfs_close_on_delete = 0
        self.uinformat_close_on_delete = 0

class SWI_XFILE:
    def __init__(self):
        self.base = SWI_BASE()
        self.xfile = None
        self.package_path = None
        self.control_dir = None
        self.type = 0
        self.state = [0] * 12
        self.swi_sc = None
        self.info_header = None
        self.INFO_header_index = 0
        self.archive_files = None
        self.swdef_fd = 0
        self.did_parse_def_file = 0
        self.is_selected = 0
        self.INDEX_ordinal = 0

class SWI_FILE_MEMBER:
    def __init__(self):
        self.base = SWI_BASE()
        self.pathname = None
        self.len = None
        self.data = None
        self.data_start_offset = None
        self.data_end_offset = None
        self.header_start_offset = None
        self.refcount = None

class SWI_SCRIPTS:
    def __init__(self):
        self.swi_co = [None] * swic.SWI_MAX_OBJ

class SWI_CONTROL_SCRIPT:
    def __init__(self):
        self.base = SWI_BASE()
        self.sid = None
        self.result = None
        self.afile = None
        self.swi_xfile = None
        self.INFO_offset = None

        
def swi_fl_create():
    return SWI_FILELIST()


def swi_make_file_list(swi, swuts, location, disallow_incompatible):
    E_DEBUG("")

    if swi is None:
        SWLIB_ERROR("")
        return None
    E_DEBUG("")
    product_number = swi_find_compatible_product(swi, swuts, disallow_incompatible)
    if product_number < 0:
        SWLIB_WARN("no compatible product found in entry")
        return None
    E_DEBUG("")
    flist = swi_make_fileset_file_list(swi, product_number, swlibc.SWI_FILESET_0, location)
    return flist

def swi_make_fileset_file_list(swi, product_num, fileset_num, location):
    state = SWHEADER_STATE()

    E_DEBUG("")
    flist = swi_fl_create()
    relocated_path = strob_open(12)

    if swi is None:
        SWLIB_ERROR("")
        return None

    E_DEBUG("")

    # Get the product 
    product = swi_package_get_product(swi.swi_pkg, product_num)
    if product is None:
        SWLIB_ERROR("")
        return None

    E_DEBUG("")

    # Get the INFO file for the fileset 
    infoheader = swi_get_fileset_info_header(swi, product_num, fileset_num)
    if infoheader is None:
        SWLIB_ERROR("")
        return None

    global_index = swi_get_global_index_header(swi)
    swheader_set_current_offset(global_index, product.p_base.header_index)
    directory = swheader_get_single_attribute_value(global_index, swc.SW_A_directory)
    directory = swlib_attribute_check_default(swc.SW_A_product, swc.SW_A_directory, directory)

    E_DEBUG("")
    swheader_store_state(infoheader, state)
    swheader_reset(infoheader)

    E_DEBUG("")
    while (object_line := swheader_get_next_object(infoheader, int(UCHAR_MAX), int(UCHAR_MAX))) is not None:
        E_DEBUG("---------------------------------------------------------------")
        if swheaderline_get_type(object_line) != swpc.SWPARSE_MD_TYPE_OBJ:
            SWLIB_ERROR("")
            return None
        object_keyword = swheaderline_get_keyword(object_line)
        if strcmp(object_keyword, swc.SW_A_control_file) == 0:
            continue
        path = swheader_get_single_attribute_value(infoheader, swc.SW_A_path)
        if path is None or len(path) == 0:
            SWLIB_ERROR("unexpected null path")
        # need to check if it's a volatile file
        E_DEBUG2("location=[%s]", location)
        E_DEBUG2("path=[%s]", path)
        E_DEBUG2("directory=[%s]", directory)

        # apply relocation 
        if strcmp(location, "/") != 0:
            E_DEBUG2("APPLY LOCATION:  path=[%s]", path)
            swlib_apply_location(relocated_path, path, location, directory)
            path = strob_str(relocated_path)
            E_DEBUG2("AFTER path=[%s]", path)
        # SWLIB_ERROR2("path=[%s]", path); 

        #		
        # Impose the every pathname begins with ''./''
        #		 
        if path[0] == '.' and path[1] == '/':
            prefix = ""
        else:
            E_DEBUG2("path=[%s]", path)
            path = swlib_return_relative_path(path)
            E_DEBUG2("path=[%s]", path)
            prefix = "./"
        E_DEBUG2("path=[%s]", path)
        store = swi_fl_add_store(flist, len(path) + len(prefix))
        temp_ref_store = store
        temp_ref_prefix = prefix
        temp_ref_path = path
        swi_fl_set_path(flist, temp_ref_store, temp_ref_prefix, temp_ref_path)
        path = temp_ref_path

        store = temp_ref_store
        E_DEBUG2("path=[%s]", path)

        # Set the file type 
        filetype = swheader_get_single_attribute_value(infoheader, swc.SW_A_type)
        if filetype is None or len(filetype) == 0:
            filetype = "0" # REGTYPE
            SWLIB_ERROR("unexpected error, type attribute not present")
        tar_filetype = swheader_getTarTypeFromTypeAttribute(filetype[0])
        temp_ref_store2 = store
        swi_fl_set_type(flist, temp_ref_store2, tar_filetype)
        store = temp_ref_store2

        # Set the volatile flag 
        is_volatile = swheader_get_single_attribute_value(infoheader, swc.SW_A_is_volatile)
        ret = swlib_is_option_true(is_volatile)
        temp_ref_store3 = store
        swi_fl_set_is_volatile(flist, temp_ref_store3, -1, ret)
    E_DEBUG("")
    #def out():
    #    swheader_restore_state(infoheader, state)
    #    strob_close(tmp)
    #    strob_close(relocated_path)
    return flist

def swi_get_fileset_info_header(swi, product_index, fileset_index):
    ret = swi.swi_pkg.swi_co[product_index].swi_co[fileset_index].info_headerM
    return ret

def swi_find_compatible_product(swi, target_uts, disallow_incompat):
    product = SWI_PRODUCT()
    n_products = 0
    n_matches = swi_audit_all_compatible_products(swi, target_uts, n_products, None)
    if disallow_incompat == 0 and n_matches == 0 and n_products == 1:
        return 0
    if n_matches == 1 and n_products == 1:
        return 0
    product_number = 0
    while product == swi_package_get_product(swi.swi_pkg, product_number):
        if product.is_compatible == swlibc.SW_TRUE:
            # check_found = 1
            break
        product_number += 1
    if product_number >= n_products:
        return -1
    return product_number


def swi_fl_delete(fl):
    del fl

def swi_fl_add_store(fl, length):
    return fl.ar.add_store(length)

def swi_fl_set_path(fl, hdr, prefix, path):
    fl.ar[hdr + swic.SWI_FL_OFFSET_PATH] = path

def swi_fl_set_is_volatile(fl, hdr, index, is_volatile):
    fl.ar[hdr + swic.SWI_FL_OFFSET_IS_VOLATILE + index] = is_volatile

def swi_fl_set_type(fl, hdr, index):
    fl.ar[hdr + swic.SWI_FL_OFFSET_TARTYPE] = index

def swi_fl_set_user_flag(fl, hdr, index):
    fl.ar[hdr + swic.SWI_FL_OFFSET_USER_FLAG] = index

def swi_fl_get_user_flag(fl, index):
    return fl.ar[index + swic.SWI_FL_OFFSET_USER_FLAG]

def swi_fl_get_path(fl, index):
    return fl.ar[index + swic.SWI_FL_OFFSET_PATH]

def swi_fl_is_volatile(fl, index):
    return fl.ar[index + swic.SWI_FL_OFFSET_IS_VOLATILE]

def swi_fl_get_type(fl, index):
    return fl.ar[index + swic.SWI_FL_OFFSET_TARTYPE]

def swi_fl_compare(f1, f2):
    return f1 - f2

def swi_fl_compare_reverse(f1, f2):
    return f2 - f1

def swi_fl_qsort_reverse(fl):
    fl.ar.sort(reverse=True)

def swi_fl_qsort_forward(fl):
    fl.ar.sort()

def swi_delete(s):
    print("Entering swi_delete LOWEST FD=", show_nopen())
    os.close(s.nullfd)
    swi_package_delete(s.swi_pkg)
    swicol_delete(s.swicol)
    print("LOWEST FD=", show_nopen())
    if s.distdata:
        swi_distdata_delete(s.distdata)
    print("LOWEST FD=", show_nopen())
    if s.xformat_close_on_delete:
        print("calling xformat_close")
        xformat_close(s.xformat)
    print("LOWEST FD=", show_nopen())
    if s.swvarfs_close_on_delete:
        print("calling swvarfs_close")
        swvarfs_close(s.swvarfs)
    print("LOWEST FD=", show_nopen())
    if s.uinformat_close_on_delete:
        print("calling uinfile_close")
        uinfile_close(s.uinformat)
    print("LEAVING swi_delete: LOWEST FD=", show_nopen())
    uxfio_close(s.excluded_file_conflicts_fd)
    uxfio_close(s.replaced_file_conflicts_fd)
    uxfio_close(s.pending_file_conflicts_fd)
    del s


def find_swdef_in_INFO(xfile, tag, name):
    ret = -1
    p = swheader_get_object_by_tag(xfile.info_header, swc.SW_A_control_file, tag)
    E_DEBUG("tag==[{}] name=[{}]".format(tag, name))
    E_DEBUG("p=[{}]".format(p))
    if p:
        ret = p - swheader_get_image_head(xfile.info_header)
        E_DEBUG("ret={}".format(ret))

        if ret < 0:
            sys.stderr.write(
                "{}: error: invalid value calculated in find_swdef_in_INFO: {}\n".format(swlib_utilname_get(), ret))
            ret = -1
        if ret > 987654321:
            sys.stderr.write(
                "{}: error: possible invalid value calculated in find_swdef_in_INFO: {}\n".format(swlib_utilname_get(),
                                                                                                  ret))
            ret = -1
    return ret

def swi_package_delete(s):
    index = 0
    expath = cplob_val(s.exlist, index)
    while expath:
        swpath_delete_export(expath)
        index += 1
        expath = cplob_val(s.exlist, index)
    cplob_release(s.exlist)
    if s.catalog_entry:
        del s.catalog_entry
    if s.target_path:
        del s.target_path
    if s.target_host:
        del s.target_host
    if s.installed_software_catalog:
        del s.installed_software_catalog
    if s.dfiles:
        swi_xfile_delete(s.dfiles)
    if s.base.global_header:
        swheader_close(s.base.global_header)
    if s.swdef_fd > 0:
        uxfio_close(s.swdef_fd)
    if s.installer_sig:
        del s.installer_sig
    s.swdef_fd = -1
    swi_scripts_delete(s.swi_sc)
    for i in range(swic.SWI_MAX_OBJ):
        if s.swi_co[i]:
            swi_product_delete(s.swi_co[i])
    del s
    return

def show_nopen():
    ret = os.open("/dev/null", os.O_RDWR)
    if ret < 0:
        print("fcntl error: " + os.strerror(ret))
    os.close(ret)
    return ret

def swi_get_last_swpath_ex(swi):
    list_ = swi.swi_pkg.exlist
    index = cplob_get_nused(list_) - 2
    ret = list_[index]
    return ret

def find_object_by_swsel(parent, swsel, number, p_index):
    package = parent
    if p_index:
        p_index = -1
    for i in range(swic.SWI_MAX_OBJ):
        object_ = package.swi_co[i].p_base
        if object:
            verid_delim = swsel.find(',')
            if not verid_delim:
                verid_delim = len(swsel)
            if swsel[:verid_delim] == object_.b_tag:
                if number and object_.number:
                    if number != object_.number:
                        continue
                if p_index:
                    p_index = i
                return object_
        else:
            return None
    return None

def need_new_pfiles(swi):
    swpath = swi.swpath


    pfiles_dir = swpath_get_pfiles(swpath)
    prod_dir = swpath_get_product_control_dir(swpath)
    pfiles_attribute = swi.swi_pkg.pfiles_attribute

    if (len(pfiles_dir) != 0 and strcmp(pfiles_attribute, pfiles_dir)) or (len(prod_dir) == 0):
        #
        # Sanity check.
        # Error
        #
        return -1

    if len(pfiles_dir) != 0 and strcmp(pfiles_attribute, pfiles_dir) == 0 and len(swpath_get_basename(swpath)) != 0:
        return 1
    return 0

def swi_audit_all_compatible_products(swi, target_uts, pn_products, p_swi_index):
    if p_swi_index:
        p_swi_index = -1
    if pn_products:
        pn_products = None
    n_matches = 0

    # Get the global INDEX access header object
    global_index = swi_get_global_index_header(swi)
    swheader_store_state(global_index, None)

    # Loop over the products
    product_number = 0
    while product := swi_package_get_product(swi.swi_pkg, product_number):
        product_number += 1

        # Now set the offset of the relevant part of the global INDEX file
        # which is the start of the product definition
        if pn_products:
            pn_products += 1
        swheader_reset(global_index)
        swheader_set_current_offset(global_index, product.p_base.header_index)

        product_uts = swuts_create()
        swi_get_uts_attributes_from_current(swi, product_uts, global_index)
        ret = swuts_compare(target_uts, product_uts, 0)
        if ret == 0:
            n_matches += 1
            product.is_compatible = swlibc.SW_TRUE
            if p_swi_index:
                p_swi_index = product_number - 1
        swuts_delete(product_uts)
    product_number += 1
    swheader_restore_state(global_index, None)
    return n_matches
def swi_get_uts_attributes_from_current(swi, uts, swheader):

    value = swheader_get_single_attribute_value(swheader, swc.SW_A_os_name)
    if value:
        swuts_add_attribute(uts, swc.SW_A_os_name, value)
    else:
        swuts_add_attribute(uts, swc.SW_A_os_name, "")

    value = swheader_get_single_attribute_value(swheader, swc.SW_A_os_version)
    if value:
        swuts_add_attribute(uts, swc.SW_A_os_version, value)
    else:
        swuts_add_attribute(uts, swc.SW_A_os_version, "")

    value = swheader_get_single_attribute_value(swheader, swc.SW_A_os_release)
    if value:
        swuts_add_attribute(uts, swc.SW_A_os_release, value)
    else:
        swuts_add_attribute(uts, swc.SW_A_os_release, "")

    value = swheader_get_single_attribute_value(swheader, swc.SW_A_machine_type)
    if value:
        swuts_add_attribute(uts, swc.SW_A_machine_type, value)
    else:
        swuts_add_attribute(uts, swc.SW_A_machine_type, "")
    return


def swi_do_decode(swi, swutil, target_fd1, source_fd0, target_path, source_path, swspecs, target_host, opta, is_seekable, do_debug_events, verboseG, g_logspec, uinfile_open_flags):
    E_DEBUG("")
    SWLIB_ASSERT(source_fd0 >= 0)
    swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_IDB, g_logspec, swutil.swu_efd, "run audit: start setup_xformat\n")

    format_ = af.arf_ustar

    # Create the xformat object
    xformat = xformat_open(-1, -1, format_)
    """
      FIXME  the open policy described by flags may should be
      forced up a level so the top level utility can
      control the policy 
    """
    E_DEBUG("")
    if uinfile_open_flags < 0:
        flags = uinc.UINFILE_DETECT_FORCEUXFIOFD | uinc.UINFILE_DETECT_UNCPIO | uinc.UINFILE_DETECT_IEEE
    else:
        flags = uinfile_open_flags

    # Now, open the package

    E_DEBUG("")
    xformat = swutil_setup_xformat(swutil, xformat, source_fd0, source_path, opta, is_seekable, verboseG, g_logspec, flags)

    if xformat is None:
        E_DEBUG("error")
        return -1
    E_DEBUG("")

    # The package is now opened and partially decoded

    swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_IDB2, g_logspec, swutil.swu_efd, "run audit: finished setup_xformat\n")

    xformat_set_ofd(xformat, target_fd1)

    swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_IDB2, g_logspec, swutil.swu_efd, "run audit: starting swlib_audit_distribution\n")

    swvarfs = xformat_get_swvarfs(xformat)
    uinformat = swvarfs_get_uinformat(swvarfs)
    swpath = uinfile_get_swpath(uinformat)
    swpath_reset(swpath)

    xformat_set_tarheader_flag(xformat, tc.TARU_TAR_FRAGILE_FORMAT, 1)
    xformat_set_tarheader_flag(xformat, tc.TARU_TAR_RETAIN_HEADER_IDS, 1)

    #
    # f translating an RPM, don't install the RPMFILE_CONFIG files
    #
    if uinfile_get_ztype(uinformat) == uinc.UINFILE_COMPRESSED_RPM:
        #
        #  we're installing an RPM, yeah!!! ;)
        #
        if opta:
            set_opta(opta, eOpts.SW_E_swbis_install_volatile, "true")
            rpm_newname = get_opta(opta, eOpts.SW_E_swbis_volatile_newname)
            if swextopt_is_option_set(eOpts.SW_E_swbis_volatile_newname, opta) == 0 and ((not rpm_newname) != '\0' or len(rpm_newname) == 0):
                set_opta(opta, eOpts.SW_E_swbis_volatile_newname, ".rpmnew")
            else:
                pass
                #
                # Use the one the user set.
                #

    swi.xformat = xformat
    swi.swvarfs = swvarfs
    swi.uinformat = uinformat
    swi.swpath = swpath
    swi.verbose = verboseG
    if target_host:
        swi.swi_pkg.target_host = target_host
    else:
        swi.swi_pkg.target_host = "localhost"
    ifd = xformat_get_ifd(xformat)
    swi.opta = opta
    swi.debug_events = do_debug_events

    #
    # Decode the catalog section.
    #
    ret = swi_decode_catalog(swi)
    E_DEBUG("")

    """
      Set the auto disable of the memory cache.
      This will turn off memory cache as soon as the
      file advances beyond the previous farthest point
    """
    E_DEBUG("")
    swvarfs_uxfio_fcntl(swvarfs, uc.UXFIO_F_ARM_AUTO_DISABLE, 1)
    E_DEBUG("")

    #
    # Now seek back to the beginning of the package.
    #
    E_DEBUG("")
    if os.lseek(ifd, 0, os.SEEK_SET) != 0:
        print("uxfio_lseek error: {} : {}".format(fr.__FILE__, fr.__LINE__))
        sys.exit(1)

    E_DEBUG("")
    swpath_reset(swpath)
    if ret != 0:
        #
        #  error, swi_decode_catalog failed
        #
        swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, g_logspec, swutil.swu_efd, "swi_decode_catalog: return code %d\n", ret)
        retval = -1
        swi = None
    else:
        retval = 0
    E_DEBUG("")
    return retval

def swi_get_global_index_header(swi):
    ret = swi.swi_pkg.base.global_header
    return ret

def swi_xfile_add_control_script(swi, xfile, name, fd, tag):
    sc = swi_control_script_create()
    s = swi_xfile_construct_file_member(xfile, name, fd, swi.xformat.swvarfs)
    sc.base.b_tag = tag
    swi_com_check_clean_relative_path(sc.base.b_tag)
    sc.afile = s
    sc.swi_xfile = xfile
    offset = find_swdef_in_INFO(xfile, tag, name)
    sc.INFO_offset = offset
    if offset < 0:
        print("warning: find_swdef_in_INFO returned", offset, file=sys.stderr)
        # should be an error in the future
    swi_add_script(xfile.swi_sc, sc)
    return


def swi_xfile_add_file_member(swi, xfile, name, fd):
    s = swi_xfile_construct_file_member(xfile, name, fd, swi.xformat.swvarfs)
    cplob_add_nta(xfile.archive_files, str(s))
    return

def swi_get_distribution_attributes(swi, swheader):
    E_DEBUG("")
    swi_com_header_manifold_reset(swheader)
    E_DEBUG("")
    obj = swheader_get_object_by_tag(swheader, swc.SW_A_distribution, "*")
    E_DEBUG("")
    if (not obj) != '\0':
        return -1
    E_DEBUG("")
    value = swheader_get_single_attribute_value(swheader, swc.SW_A_dfiles)
    if value != '\0':
        swi.swi_pkg.dfiles_attribute = value
    else:
        swi.swi_pkg.dfiles_attribute = swc.SW_A_dfiles

    E_DEBUG("")
    value = swheader_get_single_attribute_value(swheader, swc.SW_A_pfiles)
    if value != '\0':
        swi.swi_pkg.pfiles_attribute = value
    else:
        swi.swi_pkg.pfiles_attribute = swc.SW_A_pfiles

    E_DEBUG("")
    return 0

def swi_package_create():
    s = SWI_PACKAGE()
    E_DEBUG("")
    swi_com_assert_pointer(s, fr.__FILE__, fr.__LINE__)
    swiInitListOfObjects(s.swi_co)
    swi_vbase_init(s, swic.SWI_I_TYPE_PACKAGE, None, None)
    s.swi_sc = swi_scripts_create()
    s.exlist = cplob_open(1)
    s.base.global_header = None
    s.swdef_fd = -1
    s.current_product = None
    s.dfiles = None
    s.current_xfile = None
    s.did_parse_def_file = 0
    s.prev_swpath_ex = None
    s.catalog_length = 0
    s.catalog_entry = str(None)
    s.target_path = str(None)
    s.target_host = str(None)
    s.installed_software_catalog = str(None)
    s.location = str(None)
    s.qualifier = str(None)
    s.is_minimal_layout = 0
    s.installer_sig = None
    s.installed_catalog_owner = None
    s.installed_catalog_group = None
    s.installed_catalog_mode = 0
    return s

def swi_create():
    E_DEBUG("")
    swi = SWI()
    swi_com_assert_pointer(swi, fr.__FILE__, fr.__LINE__)
    swi.xformat = None
    swi.swvarfs = None
    swi.uinformat = None
    swi.swpath = None
    swi.swi_pkg = swi_package_create()
    swi.nullfd = os.open("/dev/null", os.O_RDWR, 0)
    swi.tarbuf = [0] * 1024
    swi.distdata = None
    swi.opt_alt_catalog_root = 0
    swi.swicol = swicol_create()
    swi.exported_catalog_prefix = None
    swi.verbose = 0
    swi.debug_events = 0
    swi.target_version_id = swverid_open("HOST", None)
    swi.does_have_payload = -1 # unset
    swi.xformat_close_on_delete = 0
    swi.swvarfs_close_on_delete = 0
    swi.uinformat_close_on_delete = 0

    swi.excluded_file_conflicts_fd = -1
    swi.replaced_file_conflicts_fd = -1
    swi.pending_file_conflicts_fd = -1
    return swi

def swi_product_create(global_index_header, current):
    s = SWI_PRODUCT()
    E_DEBUG("")
    swi_com_assert_pointer(s, fr.__FILE__, fr.__LINE__)

    if SWSWINEEDDEBUG:
        tmp = strob_open(100)
        sys.stderr.write(swpath_ex_print(current, tmp, "swi_product_create"))
        strob_close(tmp)

    swi_vbase_init(s, swic.SWI_I_TYPE_PROD, global_index_header, current)
    s.package_path = str(None)
    s.control_dir = str(None)
    s.filesets = None
    swiInitListOfObjects(s.swi_co)
    s.xfile = None
    s.is_selected = 1
    s.is_compatible = 0
    return s

def swi_product_add_fileset(thisis, v):
    E_DEBUG("ENTERING")
    swiAddObjectToList(thisis.swi_co, v)
    E_DEBUG("LEAVING")


def swi_package_add_product(thisis, v):
    E_DEBUG("ENTERING")
    swiAddObjectToList(thisis.swi_co, v)
    E_DEBUG("LEAVING")


def swi_store_file(swi, name, current_xfile):
    xformat = swi.xformat

    E_DEBUG2("ENTERING name=[%s]", name)
    swi_com_assert_pointer(current_xfile, fr.__FILE__, fr.__LINE__)

    fd = xformat_u_open_file(xformat, name)
    swi_com_assert_value(fd >= 0, fr.__FILE__, fr.__LINE__)

    if swi_afile_is_ieee_control_script(name):
        tag = swlib_basename(None, name)
        temp_ref_tag = tag
        swi_xfile_add_control_script(swi, current_xfile, name, fd, temp_ref_tag)
        tag = temp_ref_tag
    else:
        swi_xfile_add_file_member(swi, current_xfile, name, fd)
    xformat_u_close_file(xformat, fd)
    E_DEBUG("LEAVING")
    return 0


def swi_expand_shared_file_control_sripts(swi, xfile):
    swheader = xfile.info_headerM

    E_DEBUG("ENTERING")
    swheader_reset(swheader)
    swheader_set_current_offset_p_value(swheader, 0)

    next_line = swheader_get_next_object(swheader, int(UCHAR_MAX), int(UCHAR_MAX))
    while next_line != '\0':
        swheader_goto_next_line(swheader, swheader_get_current_offset_p(swheader), SWHEADER_PEEK_NEXT)
        path_attr = swheader_get_attribute(swheader, swc.SW_A_path, None)
        SWLIB_ASSERT(path_attr is not None)
        tag_attr = swheader_get_attribute(swheader, swc.SW_A_tag, None)
        SWLIB_ASSERT(tag_attr is not None)

        #
        # Now search the control scripts to see if this
        # tag is present.
        #
        swi_com_assert_value(tag_attr is not None, fr.__FILE__, fr.__LINE__)
        tag_value = swheaderline_get_value(tag_attr, None)
        path_value = swheaderline_get_value(path_attr, None)

        if swi_afile_is_ieee_control_script(tag_value):
            script = swi_xfile_get_control_script_by_tag(xfile, tag_value)
            if script is None:
                #
                # Not found, need to make it.
                # This happens when control_files share the
                # same script.
                #

                # fprintf(stderr, "tag=[%s] Not Found\n", tag_attr ? swheaderline_get_value(tag_attr, NULL) : "null");
                # swi_xfile_add_control_script(swi, xfile, name, int fd, tag);

                swi_com_assert_value(path_value is not None, fr.__FILE__, fr.__LINE__)
                script = swi_xfile_get_control_script_by_path(xfile, path_value)
                # BUG ME swi_com_assert_value(source_script != NULL, fr.__FILE__, fr.__LINE__);

                #
                # Now we need to make a new (SWI_CONTROL_SCRIPT *) object.
                #
                if script:
                    afile = script.afile
                else:
                    afile = swi_xfile_get_control_file_by_path(xfile, path_value)

                SWLIB_ASSERT(afile is not None)

                new_script = swi_control_script_create()
                new_script.base.b_tag = tag_value
                swi_com_check_clean_relative_path(new_script.base.b_tag)
                new_script.afile = afile
                afile.refcount += 1
                swi_add_script(xfile.swi_sc, new_script)
        else:
            pass
        next_line = swheader_get_next_object(swheader, int(UCHAR_MAX), int(UCHAR_MAX))
    return 0


def swi_update_current_context(swi, swpath_ex):
    return 0

def swi_parse_file(swi, name, swdeffile_type):
    xformat = swi.xformat
    swpath = swi.swpath
    curfd = swlib_open_memfd()
    ofd = swlib_open_memfd()
    len_ = 0

    E_DEBUG("ENTERING")
    ufd = xformat_u_open_file(xformat, name)
    if ufd < 0:
        swi_com_fatal_error(fr.__FILE__, fr.__LINE__)

    swlib_pipe_pump(curfd, ufd)
    xformat_u_close_file(xformat, ufd)

    os.lseek(curfd, 0, os.SEEK_SET)

    ret = sw_yyparse(curfd, ofd, name, 0, swpc.SWPARSE_FORM_MKUP_LEN)
    E_DEBUG("")
    if ret != 0:
        E_DEBUG("")
        print("%s: error parsing %s" % (swlib_utilname_get(), name), file=sys.stderr)

        swi_com_fatal_error(fr.__FILE__, fr.__LINE__)
        return ret
    E_DEBUG("")
    uxfio_write(ofd, "\x00", 1)
    base = swi_com_get_fd_mem(ofd, len_)
    newbase = base[:]
    swi_com_assert_pointer(newbase, fr.__FILE__, fr.__LINE__)
    swi_com_close_memfd(ofd)

    swheader = swheader_open(None, None)
    swheader_set_image_head(swheader, newbase)
    os.lseek(curfd, 0, os.SEEK_SET)

    swi_com_assert_pointer(swi.swi_pkg, fr.__FILE__, fr.__LINE__)
    if swdeffile_type == swpc.SWPARSE_SWDEF_FILETYPE_INFO:
        E_DEBUG("case SWPARSE_SWDEF_FILETYPE_INFO")
        E_DEBUG2("xfile: %p", swi.swi_pkg.current_xfile)
        swi_com_assert_pointer(swi.swi_pkg.current_xfile, fr.__FILE__, fr.__LINE__)
        swi.swi_pkg.current_xfile.info_header = swheader
        swi.swi_pkg.current_xfile.INFO_header_index = 0
        swi.swi_pkg.current_xfile.swdef_fd = curfd
        swi.swi_pkg.current_xfile.did_parse_def_file = 1

        swheader_set_current_offset_p(swheader, swi.swi_pkg.current_xfile.INFO_header_index)
        swheader_set_current_offset_p_value(swheader, 0)
        swheader_reset(swheader)
        swheader_goto_next_line(swheader, swheader_get_current_offset_p(swheader), SWHEADER_GET_NEXT)
    elif swdeffile_type == swpc.SWPARSE_SWDEF_FILETYPE_INDEX:
        E_DEBUG("SWPARSE_SWDEF_FILETYPE_INDEX")
        swi.swi_pkg.base.global_header = swheader
        swi.swi_pkg.base.header_index = 0
        swi.swi_pkg.swdef_fd = curfd
        swi.swi_pkg.did_parse_def_file = 1

        E_DEBUG("")
        swheader_set_current_offset_p(swheader, swi.swi_pkg.base.header_index)
        swheader_set_current_offset_p_value(swheader, 0)
        swheader_reset(swheader)
        E_DEBUG("")
        swheader_goto_next_line(swheader, swheader_get_current_offset_p(swheader), SWHEADER_GET_NEXT)

        E_DEBUG("")
        if swi_get_distribution_attributes(swi, swheader) != 0:
            swi_com_fatal_error(fr.__FILE__, fr.__LINE__)
        E_DEBUG("")
        swpath_set_dfiles(swpath, swi.swi_pkg.dfiles_attribute)
        E_DEBUG("")
        swpath_set_pfiles(swpath, swi.swi_pkg.pfiles_attribute)
        E_DEBUG("")
    else:
        swi_com_fatal_error(fr.__FILE__+":invalid case", fr.__LINE__)
    E_DEBUG("LEAVING")
    return ret
    
def swi_add_swpath_ex(swi):
    list_ = swi.swi_pkg.exlistM
    swpath = swi.swpath

    E_DEBUG("ENTERING")
    swi.swi_pkg.prev_swpath_ex = swi_get_last_swpath_ex(swi)

    swpath_ex = swpath_create_export(swpath)
    swi_com_assert_pointer(swpath_ex, fr.__FILE__, fr.__LINE__)

    cplob_add_nta(list_, str(swpath_ex))
    E_DEBUG("LEAVING")
    return 0

def swi_handle_control_transition(swi):
    package = swi.swi_pkg
    found_edge = 0
    E_DEBUG("ENTERING")
    previous = swi.swi_pkg.prev_swpath_exM
    current = swi_get_last_swpath_ex(swi)

    E_DEBUG("ENTERING")

    if SWSWINEEDDEBUG:
        tmp = strob_open(100)
        E_DEBUG2("Previous SWPATH_EX:\n %s\n", swpath_ex_print(previous, tmp, "previous"))
        E_DEBUG2("Current  SWPATH_EX:\n %s\n", swpath_ex_print(current, tmp, "current"))
        strob_close(tmp)

    """
     	 SWPATH_EX
     	   int is_catalog;
     	   int ctl_depth
     	   char * pkgpathname
     	   char * prepath
     	   char * dfiles
     	   char * pfiles
     	   char * product_control_dir
     	   char * fileset_control_dir
     	   char * pathname;
     	   char * basename
    """

    if previous is None:
        #
        # FIXME, Free this.
        #
        previous = swpath_create_export(None)

    detect = swi_com_field_edge_detect(current.dfiles, previous.dfiles)
    if found_edge == 0 and detect != 0:
        #
        # New dfiles.
        #
        E_DEBUG("New dfiles")
        package.dfiles = swi_xfile_create(swic.SWI_XFILE_TYPE_DFILES, swi.swi_pkg.base.global_header, current)
        package.current_xfile = package.dfilesM
        package.dfiles.package_path = current.pkgpathname
        E_DEBUG2("Setting Current XFILE (dfiles) [%p]", package.current_xfile)
        found_edge = 1

    detect = swi_com_field_edge_detect(current.product_control_dir, previous.product_control_dir)
    if found_edge == 0 and detect != 0:
        #
        # New product.
        #
        E_DEBUG("New Product")
        package.current_product = swi_product_create(swi.swi_pkg.base.global_header, current)
        swi_package_add_product(package, package.current_product)
        package.current_xfile = None
        package.current_product.package_path = current.pkgpathname
        if (tret := need_new_pfiles(swi)) > 0:
            """
            This happens in packages without directories in
            the archive.
            e.g.
            ...
            catalog/dfiles/INFO
            catalog/prod1/pfiles/INFO
            ...
            """
            pfiles = swi_xfile_create(swic.SWI_XFILE_TYPE_FILESET, swi.swi_pkg.base.global_header, current)
            pfiles.package_path = current.pkgpathname
            package.current_product.xfile = pfiles
            package.current_xfile = pfiles
        elif tret < 0:
            SWI_internal_error()
            return -1
        else:
            pass
        E_DEBUG2("Setting Current Product [%p]", package.current_product)
        E_DEBUG2("Setting Current XFILE (NULL) [%p]", package.current_xfile)
        found_edge = 1

    detect = swi_com_field_edge_detect_fileset(current, previous)
    if found_edge == 0 and detect != 0:
        #
        #		 * New fileset.
        #
        E_DEBUG("New Fileset")
        fileset = swi_xfile_create(swic.SWI_XFILE_TYPE_FILESET, swi.swi_pkg.base.global_header, current)
        swi_product_add_fileset(package.current_product, fileset)
        package.current_xfile = fileset
        package.current_xfile.package_path = current.pkgpathname
        E_DEBUG2("        Current Product [%p]", package.current_product)
        E_DEBUG2("Setting Current XFILE (fileset) [%p]", package.current_xfile)
        found_edge = 1

    detect = swi_com_field_edge_detect(current.pfiles, previous.pfiles)
    if found_edge == 0 and detect != 0:
        #
        #		 * New pfiles.
        #
        E_DEBUG("New pfiles")
        pfiles = swi_xfile_create(swic.SWI_XFILE_TYPE_PFILES, swi.swi_pkg.base.global_header, current)
        if package.current_product is None:
            #
            #			 * This happens for minimal package layout.
            #
            package.current_product = swi_product_create(swi.swi_pkg.base.global_header, current)
            swi_package_add_product(package, package.current_product)
            package.current_xfile = None
        pfiles.package_path = current.pkgpathname
        package.current_product.xfile = pfiles
        package.current_xfile = pfiles
        E_DEBUG2("   Current Product [%p]", package.current_product)
        E_DEBUG2("Setting Current XFILE (pfiles) [%p]", package.current_xfile)
        found_edge = 1

    E_DEBUG2("LEAVING return value = [%d]", found_edge)
    return found_edge

def swi_decode_catalog(swi):
    xformat = swi.xformat
    swpath = swi.swpath
    tmp = strob_open(24)
    st = ""
    copyret = 0

    swdeffile_type = 0
    offset = 0
    catalog_start_offset = -1
    catalog_end_offset = 0
    retval = 0
    nullfd = swi.nullfd
    if nullfd < 0:
        return -22

    ifd = xformat_get_ifd(xformat)
    name = xformat_get_next_dirent(xformat, st)
    if ifd >= 0:
        offset = os.lseek(ifd, 0, os.SEEK_CUR)

    while name != '\0':
        E_DEBUG2("ENTERING LOOP: name=[%s]", name)
        if xformat_is_end_of_archive(xformat):
            #
            # error
            #
            E_DEBUG("EOA found before storage section: break")
            retval = -1
            break

        #
        # Check for absolute path, and for shell metacharacters
        # characters in the pathname.
        #
        swi_com_check_clean_relative_path(name)
        E_DEBUG2("name=[%s]", name)

        swpath_ret = swpath_parse_path(swpath, name)
        if swpath_ret < 0:
            E_DEBUG("swpath_parse_path error")
            print("%s: error parsing pathname [%s]" % (swlib_utilname_get(), name), file=sys.stderr)
            retval = -2
            break

        is_catalog = swpath_get_is_catalog(swpath)
        if is_catalog != SWPATH_CTYPE_STORE:
            #
            # Either -1 leading dir, or 1 catalog.
            #
            if is_catalog == 1 and catalog_start_offset < 0:
                E_DEBUG("")
                catalog_start_offset = offset
                swi.swi_pkg.catalog_start_offset = offset
                #
                #fprintf(stderr,  "eraseme START ---OFFSET=%d %d\n", offset, xformat->swvarfs->current_header_offset);
                #

            if swi.exported_catalog_prefix is None and is_catalog == 1:
                #
                # fill in the swi->exported_catalog_prefix  member.
                # This is how the catalog directory is
                # specified when being unpacked (by tar)
                # and deleted.
                #
                E_DEBUG("")
                strob_strcpy(tmp, swpath_get_prepath(swpath))
                swlib_unix_dircat(tmp, swc.SW_A_catalog)
                swi.exported_catalog_prefix = strob_str(tmp)
                swi_com_check_clean_relative_path(strob_str(tmp))

            swi_add_swpath_ex(swi)

            handle_ret = swi_handle_control_transition(swi)
            if handle_ret < 0:
                #
                # error
                #
                E_DEBUG("")
                swi_com_assert_pointer(None, fr.__FILE__, fr.__LINE__)

            temp_ref_name = name
            temp_ref_swdeffile_type = swdeffile_type
            if swi_is_definition_file(swi, temp_ref_name, temp_ref_swdeffile_type) != 0:
                swdeffile_type = temp_ref_swdeffile_type
                name = temp_ref_name
                #
                # parse the IEEE 1387.2 software definition file
                #
                E_DEBUG("")
                temp_ref_name2 = name
                swdef_ret = swi_parse_file(swi, temp_ref_name2, swdeffile_type)
                name = temp_ref_name2
                E_DEBUG("")
                if swdef_ret:
                    print("error parsing %s" % name, file=sys.stderr)
                    retval = -3
                    break
                E_DEBUG("")
                copyret = 0
            else:
                swdeffile_type = temp_ref_swdeffile_type
                name = temp_ref_name
                if xformat_file_has_data(xformat):
                    #
                    # Store the file data.
                    #
                    E_DEBUG("")
                    temp_ref_name3 = name
                    swi_store_file(swi, temp_ref_name3, swi.swi_pkg.current_xfile)
                else:
                    #
                    # No data
                    #
                    E_DEBUG("")
                    copyret = 0
                    pass
            if copyret < 0:
                #
                # error
                #
                E_DEBUG("")
                SWI_internal_error()
                retval = -4
                break
            E_DEBUG("")
            catalog_end_offset = os.lseek(ifd, 0, os.SEEK_CUR)
            swi.swi_pkg.catalog_end_offset = catalog_end_offset
            E_DEBUG2("catalog end offset = [%d]", swi.swi_pkg.catalog_end_offset)
        else:
            #
            # Good.
            # Finished, read past catalog section
            # This is the first file of the storage section.
            #
            E_DEBUG("STORAGE SECTION found: break")
            swi.does_have_payload = 1
            break

        E_DEBUG("")
        name = xformat_get_next_dirent(xformat, st)
        offset = os.lseek(ifd, 0, os.SEEK_CUR)
        E_DEBUG2("NAME=[%s]",name if name != '\0' else "<NIL>")

    E_DEBUG("")
    if retval < 0:
        E_DEBUG2("ERROR: retval = [%d]", retval)
        return retval

    E_DEBUG("")
    if swi.does_have_payload < 0:
        # Must not have a payload such as the
        # catalog.tar file from the installed_software_catalog
        #  or a package with no files

        swi.does_have_payload = 0

        # also we must set the swi->swi_pkg->catalog_end_offset value
        # which is wrong at the present
        #

        catalog_end_offset = os.lseek(ifd, 0, os.SEEK_CUR)
        swi.swi_pkg.catalog_end_offset = catalog_end_offset
        E_DEBUG2("NO PAYLOAD: NOW catalog end offset = [%d]", swi.swi_pkg.catalog_end_offset)
        E_DEBUG2("EOA bytes = [%d]", xformat.eoa)

    E_DEBUG("")
    # sanity check
    if catalog_start_offset < 0:
        catalog_start_offset = 0
        print("internal error: %s:%d" % (fr.__FILE__, fr.__LINE__), file=sys.stderr)

    E_DEBUG("")
    swi.swi_pkg.catalog_length = catalog_end_offset - catalog_start_offset

    # Now fix-up the control scripts that share
    # a common control file.
    E_DEBUG("")

    if swi.swi_pkg is None:
        return -8


    if swi.swi_pkg is not None and swi.swi_pkg.swi_co[0] is not None:
        ret = swi_expand_shared_file_control_sripts(swi, swi.swi_pkg.swi_co[0].xfile)
    else:
        # This is an internal error and should never happen.
        #
        print("%s: internal error swi.swi_pkg && swi.swi_pkg.swi_co[0]" % swlib_utilname_get(), file=sys.stderr)
        ret = 0

    E_DEBUG("")
    swi_com_assert_value(ret == 0, fr.__FILE__, fr.__LINE__)

    E_DEBUG("")

    swi_recurse_swi_tree(swi, str(None), swi_vbase_set_verbose_level, swi.verbose)
    E_DEBUG("")
    swi_recurse_swi_tree(swi, str(None), swi_vbase_update, None)
    E_DEBUG("")
    ret = swi_recurse_swi_tree(swi, str(None), swi_vbase_generate_swverid, None)
    if ret != 0:
        if retval == 0:
            retval = -1
    E_DEBUG("")
    swi.swi_pkg.is_minimal_layout = swpath_get_is_minimal_layout(swpath)
    strob_close(tmp)
    E_DEBUG("")
    return retval

def swi_package_get_product(swi_pkg, index):
    ret = swi_pkg.swi_co[index]
    return ret


def swi_recurse_swi_tree(swi, sw_selections, payload, uptr):
    retval = 0
    package = swi.swi_pkg
    for i in range(swic.SWI_MAX_OBJ):
        product = package.swi_co[i]
        if product:
            if payload:
                ret = payload(product, uptr)
                if ret:
                    retval += 1
            for j in range(swic.SWI_MAX_OBJ):
                fileset = product.swi_co[j]
                if fileset:
                    if payload:
                        ret = payload(fileset, uptr)
                        if ret:
                            retval += 1
                else:
                    break
        else:
            break
    return retval
def swi_product_get_fileset(swi_prod, index):
    ret = swi_prod.swi_co[index]
    return ret


def swi_product_has_control_file(swi_prod, control_file_name):
    ret = swi_xfile_has_posix_control_file(swi_prod.xfile, control_file_name)
    return ret


def swi_product_get_control_script_by_tag(prod, tag):
    ret = swi_xfile_get_control_script_by_tag(prod.xfile, tag)
    return ret


def swi_get_control_script_by_swsel(swi, swsel, script_tag):
    #
    # 'swsel' *      is software selection
    #
    return 0

def swi_find_product_by_swsel(parent, swsel, number, p_index):
    base = find_object_by_swsel(parent, swsel, number, p_index)
    if not base:
        return base
    swi_base_assert(base)
    SWLIB_ASSERT(base.type_idM == swic.SWI_I_TYPE_PROD)
    return base

def swi_find_fileset_by_swsel(parent, swsel, p_index):
    base = find_object_by_swsel(parent, swsel, None, p_index)
    if not base:
        return base
    swi_base_assert(base)
    assert base.type_id == swic.SWI_I_TYPE_XFILE
    assert base.type == swic.SWI_XFILE_TYPE_FILESET or  base.type == swic.SWI_XFILE_TYPE_PFILES
    return base

def swi_set_utility_id(swi, swc_u_id):
    swi.swc_id = swc_u_id


def swi_is_definition_file(swi, name, swdeffile_type_p):
    if (s := name.find("/" + swc.SW_A_INDEX)) != -1 and name[s+6] == '\0':
        if swi_is_global_index(swi.swpath, name):
            if swi.swi_pkg.did_parse_def_file != 0:
                swi_com_fatal_error("loc=index", fr.__LINE__)
            swi.swi_pkg.did_parse_def_file = 1
            # swdeffile_type_p = swpc.SWPARSE_SWDEF_FILETYPE_INDEX
            return 1
        else:
            pass
            """
             ignore INDEX files that are not the
             global index file.
            """
    elif (s := name.find("/" + swc.SW_A_INFO)) != -1 and name[s+5] == '\0':
        if swi.swi_pkg.current_xfile.did_parse_def_file:
            swi_com_fatal_error("loc=info", fr.__LINE__)
        swi.swi_pkg.current_xfile.did_parse_def_file = 1
        # swdeffile_type_p = swpc.SWPARSE_SWDEF_FILETYPE_INFO
        return 1
    swdeffile_type_p = -1
    return 0

def swi_product_delete(s):
    if s.p_base.b_tag:
        del s.p_base.b_tag
    if s.package_path:
        del s.package_path
    if s.control_dir:
        del s.control_dir
    if s.filesets:
        strar_close(s.filesets)
    if s.xfile:
        swi_xfile_delete(s.xfile)
    for i in range(swic.SWI_MAX_OBJ):
        if s.swi_co[i]:
            swi_xfile_delete(s.swi_co[i])
    del s


def loop_and_record(INDEX, object_keyword, list_, vendor_list, revision_list):
    obj = swheader_get_object_by_tag(INDEX, object_keyword, "*")
    count = 0
    while obj:
        attrline = swheader_get_attribute(INDEX, swc.SW_A_tag, None)
        if not attrline:
            print("no tag found for", object_keyword, file=sys.stderr)
            retval = 20
            return retval
        value = swheaderline_get_value(attrline, None)
        strar_add(list_, value)
        attrline = swheader_get_attribute(INDEX, swc.SW_A_vendor_tag, None)
        if attrline:
            value = swheaderline_get_value(attrline, None)
            if vendor_list:
                strar_add(vendor_list, value)
        attrline = swheader_get_attribute(INDEX, swc.SW_A_revision, None)
        if not attrline:
            retval = 23
            return retval
        value = swheaderline_get_value(attrline, None)
        if revision_list:
            strar_add(revision_list, value)
        swheader_get_next_object(INDEX, UCHAR_MAX, UCHAR_MAX)
        obj = swheader_get_object_by_tag(INDEX, object_keyword, "*")
        count += 1
    return count

def determine_catalog_directories(part1, enforce_swinstall_policy):
    bundle_tag = strar_get(part1.bundle_tags, 0)
    product_tag = strar_get(part1.product_tags, 0)
    if enforce_swinstall_policy:
        pass
    if bundle_tag is None:
        bundle_tag = product_tag
    if product_tag is None:
        return 1
    part1.catalog_bundle_dir1 = strdup(bundle_tag)
    return 0

def swi_distdata_create():
    part1 = SWI_DISTDATA()
    swi_distdata_initialize(part1)
    return part1

def swi_distdata_initialize(part1):
    part1.did_part1 = 0
    part1.dist_tag = None
    part1.dist_revision = None
    part1.catalog_bundle_dir1 = None
    part1.bundle_tags = strar_open()
    part1.product_tags = strar_open()
    part1.product_revisions = strar_open()
    part1.vendor_tags = strar_open()

def swi_distdata_resolve(swi, part1, enforce_swinstall_policy):
    swi_distdata_initialize(part1)
    INDEX = SWI_get_index_header(swi)
    swheader_reset(INDEX)
    swheader_reset(INDEX)
    count = loop_and_record(INDEX, swc.SW_A_bundle, part1.bundle_tags, None, None)
    swheader_reset(INDEX)
    count = loop_and_record(INDEX, swc.SW_A_product, part1.product_tags, part1.vendor_tags, part1.product_revisions)
    if count == 0:
        retval = 30
        return retval
    swheader_reset(INDEX)
    obj = swheader_get_object_by_tag(INDEX, swc.SW_A_distribution, "*")
    if not obj:
        retval = 40
        return retval
    offset = swheader_get_current_offset(INDEX)
    attrline = swheader_get_attribute(INDEX, swc.SW_A_tag, None)
    if not attrline:
        value = strar_get(part1.product_tags, 0)
    else:
        value = swheaderline_get_value(attrline, None)
    part1.dist_tag = strdup(value)
    swheader_reset(INDEX)
    if determine_catalog_directories(part1, enforce_swinstall_policy):
        return 44
    retval = 0
    return retval

def swi_distdata_delete(part1):
    del part1.dist_tag
    strar_close(part1.bundle_tags)
    strar_close(part1.product_tags)
    strar_close(part1.product_revisions)
    strar_close(part1.vendor_tags)

def swi_xfile_construct_file_member(xfile, name, fd, swvarfs):
    length = 0
    s = SWI_FILE_MEMBER()
    s.base.is_active = 0
    s.header_start_offset = swvarfs.current_header_offset
    s.data_start_offset = swvarfs.current_data_offset
    mem_fd = os.memfd_create("", 0)
    assert mem_fd >= 0
    swlib_pipe_pump(mem_fd, fd)
    s.data_end_offset = os.lseek(mem_fd, 0, os.SEEK_CUR) + s.data_start_offset
    data = swi_com_get_fd_mem(mem_fd, length)
    if length != (s.data_end_offset - s.data_start_offset):
        SWI_internal_error()
    s.pathname = name
    s.len = length
    newdata = bytearray(length + 1)
    newdata[length] = 0
    newdata[:length] = data
    s.data = newdata
    os.close(mem_fd)
    return s

def swi_xfile_delete(s):
    index = 0
    file = None
    if s.base.b_tag:
        del s.base.b_tag
    if s.package_path:
        del s.package_path
    if s.control_dir:
        del s.control_dir
    if s.swi_sc:
        swi_scripts_delete(s.swi_sc)
    if s.info_header:
        swheader_close(s.info_header)
    if s.swdef_fd > 0:
        uxfio_close(s.swdef_fd)
    s.swdef_fd = -1
    file = cplob_val(s.archive_files, index)
    while file:
        swi_file_member_delete(file)
        file = cplob_val(s.archive_files, index)
    del s.archive_files
    del s

def swi_xfile_create(type_, global_index_header, current):
    s = SWI_XFILE()
    if type_ == swic.SWI_XFILE_TYPE_FILESET:
        swi_vbase_init(s, swic.SWI_I_TYPE_XFILE, global_index_header, current)
    else:
        swi_vbase_init(s, swic.SWI_I_TYPE_XFILE, global_index_header, None)
    s.xfile = s
    s.type = type_
    s.package_path = None
    s.control_dir = None
    s.swi_sc = swi_scripts_create()
    s.info_header = None
    s.archive_files = cplob_open(1)
    s.swdef_fd = -1
    s.did_parse_def_file = 0
    swi_xfile_set_state(s, swc.SW_STATE_UNSET)
    s.is_selected = 1
    return s

def swi_write_fileset(xfile, ofd):
    return 0

def swi_xfile_set_state(s, state):
    s.state = state[:12]

def swi_xfile_get_infoheader(xfile):
    return xfile.info_header

def swi_xfile_get_control_file_by_path(xfile, path):
    index = 0
    files = cplob_get_list(xfile.archive_files)
    while files and index < swic.SWI_MAX_OBJ:
        if swlib_basename_compare(path, files.pathname) == 0:
            return files
        index += 1
    return None

def swi_xfile_get_control_script_by_path(xfile, path):
    index = 0
    sci = xfile.swi_sc
    scripts = sci.swi_co
    while scripts and index < swic.SWI_MAX_OBJ:
        if swlib_basename_compare(path, scripts.afile.pathname) == 0:
            return scripts
        index += 1
        scripts += 1
    return None

def swi_xfile_get_control_script_by_id(xfile, id):
    index = 0
    sci = xfile.swi_sc
    scripts = sci.swi_co
    while scripts and index < swic.SWI_MAX_OBJ:
        if id == scripts.sid:
            return scripts
        index += 1
        scripts += 1
    return None

def swi_xfile_get_control_script_by_tag(xfile, tag):
    index = 0
    sci = xfile.swi_sc
    scripts = sci.swi_co
    while scripts and index < swic.SWI_MAX_OBJ:
        if tag == scripts.base.b_tag:
            return scripts
        index += 1
        scripts += 1
    return None

def swi_examine_signature_blocks(dfiles, sig_block_start, sig_block_end):
    index = 0
    cplob = dfiles.archive_files
    sig_block_start[0] = -1
    sig_block_end[0] = -1
    s = cplob_val(cplob, index)
    while s:
        f = s.pathname.find("/" + swc.SW_A_signature)
        if f != -1 and s.pathname[f+10] == '\0':
            if sig_block_start[0] == -1:
                sig_block_start[0] = s.header_start_offset
            sig_block_end[0] = s.data_start_offset + s.len
        index += 1
        s = cplob_val(cplob, index)
    return 0

def swi_xfile_has_posix_control_file(xfile, tag):
    ret = swi_xfile_get_control_script_by_tag(xfile, tag)
    return 1 if ret else 0

"""
swi_afile.py -- POSIX attribute and control file object.

   Copyright (C) 2005 Jim Lowe
   Copyright (C) 2024 Paul Weber conversion to Python
   All Rights Reserved.

"""
class G_CONTROL_SCRIPT:
    def __init__(self, did_it, tag):
        self.did_it = did_it
        self.tag = tag


# Define pp_g_scripts array
pp_g_scripts = [
    G_CONTROL_SCRIPT(0, "checkinstall"),
    G_CONTROL_SCRIPT(0, "preinstall"),
    G_CONTROL_SCRIPT(0, "postinstall"),
    G_CONTROL_SCRIPT(0, "verify"),
    G_CONTROL_SCRIPT(0, "fix"),
    G_CONTROL_SCRIPT(0, "checkremove"),
    G_CONTROL_SCRIPT(0, "preremove"),
    G_CONTROL_SCRIPT(0, "postremove"),
    G_CONTROL_SCRIPT(0, "configure"),
    G_CONTROL_SCRIPT(0, "unconfigure"),
    G_CONTROL_SCRIPT(0, "request"),
    G_CONTROL_SCRIPT(0, "unpreinstall"),
    G_CONTROL_SCRIPT(0, "unpostinstall"),
    G_CONTROL_SCRIPT(0, "space"),
    G_CONTROL_SCRIPT(0, None)
]
def make_case_pattern_for_unused_tags(buf):
    is_first = 1
    script_ent = pp_g_scripts
    buf += "\t\t"
    while script_ent.tag:
        if script_ent.did_it == 0:
            if not is_first:
                buf += "|"
            buf += script_ent.tag
            is_first = 0
        else:
            script_ent.did_it = 0
        script_ent += 1
    buf += ")\n"
    buf += "\t\t\techo \"$0: no ${SWBCS_SCRIPT_TAG} script for ${SWBCS_MATCH}\" 1>&2\n"
    buf += "\t\t\texit " + str(swc.SW_STATUS_COMMAND_NOT_FOUND) + "\n"
    buf += "\t\t\t;;\n"


def set_is_ieee_control_script(pathname, do_set):
    base = os.path.basename(pathname)
    script_ent = pp_g_scripts
    while script_ent.tag:
        if base == script_ent.tag:
            if do_set:
                script_ent.did_it = 1
            return 1
        script_ent += 1
    return 0

def get_attribute_from_INFO_object(control_script, attr_name):
    xfile = control_script.swi_xfile
    if not xfile:
        return None
    h = xfile.info_header
    if not h:
        return None
    state = SWHEADER_STATE()
    val = None
    swheader_store_state(h, state)
    swheader_reset(h)
    swheader_set_current_offset_p(h, xfile.INFO_header_index)
    swheader_set_current_offset_p_value(h, control_script.INFO_offset)
    val = swheader_get_single_attribute_value(h, attr_name)
    swheader_restore_state(h, state)
    return val

def swi_scripts_delete(s):
    for i in range(swic.SWI_MAX_OBJ):
        if s.swi_co[i]:
            swi_control_script_delete(s.swi_co[i])
    del s

def swi_scripts_create():
    s = SWI_SCRIPTS()
    swiInitListOfObjects(s.swi_co)
    return s

def swi_file_member_delete(s):
    if s.refcount > 1:
        s.refcount -= 1
        return
    if s.pathname:
        del s.pathname
    if s.data:
        del s.data
    del s

def swi_file_member_create():
    s = SWI_FILE_MEMBER()
    swi_vbase_init(s, swic.SWI_I_TYPE_AFILE, None, None)
    s.refcount = 1
    s.pathname = None
    s.len = -1
    s.data = None
    return s

def swi_control_script_delete(s):
    if s.base.b_tag:
        del s.base.b_tag
    if s.afile:
        swi_file_member_delete(s.afile)
    del s

def swi_control_script_create():
    global id
    s = SWI_CONTROL_SCRIPT()
    s.base = SWI_BASE()
    s.sid = id + 1
    s.afile = None
    s.swi_xfile = None
    s.INFO_offset = -1
    s.result = swic.SWI_RESULT_UNDEFINED
    return s

def swi_add_script(thisisit, v):
    swiAddObjectToList(thisisit.swi_co, v)

def swi_control_script_get_return_code(posix_result):
    if posix_result.lower() == swc.SW_RESULT_NONE.lower():
        return swic.SWI_RESULT_UNDEFINED
    elif posix_result.lower() == swc.SW_RESULT_SUCCESS.lower():
        return swic.SWI_RESULT_SUCCESS
    elif posix_result.lower() == swc.SW_RESULT_WARNING.lower():
        return swic.SWI_RESULT_WARNING
    elif posix_result.lower() == swc.SW_RESULT_FAILURE.lower():
        return swic.SWI_RESULT_FAILURE
    else:
        sys.stderr.write(f"{swlib_utilname_get()}: unrecognized POSIX result string: [{posix_result}]\n")
        return swic.SWI_RESULT_FAILURE

def swi_control_script_posix_result(s):
    if s.result == swic.SWI_RESULT_UNDEFINED or s.result < 0:
        return swc.SW_RESULT_NONE
    elif s.result == swc.SW_SUCCESS:
        return swc.SW_RESULT_SUCCESS
    elif s.result == swc.SW_ERROR:
        return swc.SW_RESULT_FAILURE
    elif s.result == swc.SW_WARNING:
        return swc.SW_RESULT_WARNING
    elif s.result == swc.SW_NOTE:
        return swc.SW_RESULT_WARNING
    else:
        sys.stderr.write(f"{swlib_utilname_get()}: internal error: bad result code: [{s.result}]\n")
        return swc.SW_RESULT_FAILURE

def swi_afile_write_script_cases(scripts, buf, installed_isc_path):
    script_ent = pp_g_scripts
    control_script = None
    vintbuf = ""
    script_ent = pp_g_scripts
    while script_ent.tag:
        script_ent.did_it = 0
        script_ent += 1
    buf += "               case \"${SWBCS_SCRIPT_TAG}\" in\n"
    SWLIB_ASSERT(installed_isc_path is not None)
    for i in range(swic.SWI_MAX_OBJ):
        script_ent = pp_g_scripts
        control_script = scripts.swi_co[i]
        if control_script:
            set_is_ieee_control_script(control_script.base.b_tag, 1)
            interpreter = get_attribute_from_INFO_object(control_script, swc.SW_A_interpreter)
            if interpreter is None:
                interpreter = ""
            if len(interpreter) and swlib_check_clean_path(interpreter):
                interpreter = ""
            if len(interpreter):
                vintbuf = "INTERPRETER=\"" + interpreter + "\"\n"
            else:
                vintbuf = ""
            lslash = control_script.afile.pathname.rfind('/')
            SWLIB_ASSERT(lslash is not None)
            name = lslash + 1
            buf += "\t\t" + script_ent.tag + ")\n"
            buf += "\t\t\texport swc.SW_CONTROL_TAG=\"" + script_ent.tag + "\"\n"
            buf += "\t\t\t" + vintbuf + "\n"
            buf += "\t\tcase \"$SW_CATALOG\" in\n"
            buf += "\t\t\t  # User specified swc.SW_CATALOG as really, really absolute\n"
            make_case_pattern_for_unused_tags(buf)
    buf += "               *)\n"
    buf += "                       echo \"$0: invalid tag\" 1>&2\n"
    buf += "                       exit 1\n"
    buf += "                       ;;\n"
    buf += "               esac\n"
    return 0

def swi_afile_is_ieee_control_script(pathname):
    ret = set_is_ieee_control_script(pathname, 0)
    return ret

"""
 swi_common -- Posix package decoding common routines.

   Copyright (C) 2004 Jim Lowe
   Copyright (C) 2024 Paul Weber conversion to Python
   All Rights Reserved.

   COPYING TERMS AND CONDITIONS:
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""


def is_clean_relative_path(name):
    if (
        swlib_is_sh_tainted_string(name) or
        "../" in name or
        "/.." in name or
        name[0] == '/'
    ):
        return False
    else:
        return True

def fatal_error(msg0, msg, msg2):
    sys.stderr.write(f"{swlib_utilname_get()} : {msg0} : {msg} line=[{msg2}]\n")
    sys.exit(99)

def swi_com_set_header_index(header, swpath_ex, ai):
    index_offset = glbindex_find_by_swpath_ex(header, swpath_ex)
    if index_offset >= 0:
        ai = index_offset
        retval = 0
    else:
        swi_com_internal_error(fr.__FILE__, fr.__LINE__)
        ai = 0
        retval = -1
    return retval

def SWI_get_index_header(A):
    return A.swi_pkg.base.global_header

def SWI_internal_error():
    return swi_com_internal_error(fr.__FILE__, fr.__LINE__)

def swi_com_internal_error(file, line):
    sys.stderr.write(f"{swlib_utilname_get()}: internal error : {file}:{line}\n")

def swi_com_internal_fatal_error(file, line):
    sys.stderr.write(f"{swlib_utilname_get()}: internal fatal error : {file}:{line}\n")
    sys.exit(1)

def swi_com_assert_pointer(p, file, lineno):
    if not p:
        fatal_error("fatal null pointer error", file, lineno)

def swi_com_assert_value(p, file, lineno):
    if not p:
        fatal_error("fatal zero value error", file, lineno)

def swi_com_fatal_error(msg, msg2):
    sys.stderr.write(f"{swlib_utilname_get()}: fatal error : {msg} [{msg2}]\n")
    os.close(sys.stderr.fileno())
    os.close(sys.stdin.fileno())
    os.close(sys.stdout.fileno())
    sys.exit(10)

def swi_com_close_memfd(fd):
    return uxfio_close(fd)

def swi_com_new_fd_mem(fd, datalen):
    length = 0
    s = swi_com_get_fd_mem(fd, length)
    ret = bytearray(length + 1)
    ret[:length] = s
    ret.append(0x00)
    if datalen:
        datalen = length
    return ret

def swi_com_get_fd_mem(fd, datalen):
    nullchar = b'\0'
    s = uxfio_get_dynamic_buffer(fd, None, None, datalen)
    ret = uxfio_lseek(fd, 0, os.SEEK_END)
    swi_com_assert_value(ret >= 0, fr.__FILE__, fr.__LINE__)
    ret = uxfio_write(fd, nullchar, 1)
    swi_com_assert_value(ret == 1, fr.__FILE__, fr.__LINE__)
    s = uxfio_get_dynamic_buffer(fd, None, None, None)
    return s

def swi_com_do_preview(taru, file_hdr, tar_header_p, header_len, now):
    ftype = taru_get_tar_filetype(os.stat(taru).st_mode)
    if ftype >= 0:
        ls_list(ahsstaticgettarfilename(file_hdr),
                ahsstaticgettarlinkname(file_hdr),
                file_hdr,
                now,
                sys.stdout,
                tar_header_p[tc.THB_BO_UNAME:tc.THB_BO_UNAME+31],
                tar_header_p[tc.THB_BO_GNAME:tc.THB_BO_GNAME+31],
                ftype,
                lslistc.LS_LIST_VERBOSE_L1)
    else:
        sys.stderr.write(f"{swlib_utilname_get()}: unrecognized file type in mode [{os.stat(taru).st_mode}] for file: {ahsstaticgettarfilename(file_hdr)}\n")

def swi_com_check_clean_relative_path(name):
    if is_clean_relative_path(name):
        return
    else:
        swi_com_assert_value(0, fr.__FILE__, fr.__LINE__)
        sys.exit(44)

def swiInitListOfObjects(pp):
    for i in range(swic.SWI_MAX_OBJ):
        pp[i] = None

def swiGetNumberOfObjects(pp):
    for i in range(swic.SWI_MAX_OBJ - 1):
        if pp[i] is None:
            return i
    sys.stderr.write("too many contained objects, fatal error\n")
    sys.exit(88)

def swiAddObjectToList(pp, p):
    for i in range(swic.SWI_MAX_OBJ - 1):
        if pp[i] is None:
            pp[i] = p
            return 0
    sys.stderr.write("too many contained objects, fatal error\n")
    sys.exit(88)

def swi_com_header_manifold_reset(swheader):
    swheader_reset(swheader)
    swheader_set_current_offset_p_value(swheader, 0)
    swheader_goto_next_line(swheader,
                            swheader_get_current_offset_p(swheader),
                            SWHEADER_GET_NEXT)

def swi_com_field_edge_detect(current, previous):
    if not previous or not current:
        swi_com_assert_pointer(None, fr.__FILE__, fr.__LINE__)
    if len(previous) == 0 and len(current):
        return 1
    if previous != current and len(current):
        return 1
    return 0

def swi_com_field_edge_detect_fileset(current, previous):
    ret = swi_com_field_edge_detect(current.fileset_control_dir,
                                    previous.fileset_control_dir)
    if ret == 0:
        if len(current.pfiles) == 0 and len(previous.pfiles):
            ret = 1
    return ret

def print_header(swheader):
    swheader_reset(swheader)
    swheader_set_current_offset_p_value(swheader, 0)
    next_line = swheader_get_next_object(swheader,
                                         UCHAR_MAX, UCHAR_MAX)
    next_attr = swheader_get_next_attribute(swheader)
    while next_line:
        swheaderline_write_debug(next_line, sys.stderr.fileno())
        swheader_goto_next_line(swheader,
                                swheader_get_current_offset_p(swheader),
                                SWHEADER_PEEK_NEXT)

        while next_attr == swheader_get_next_attribute(swheader):
            swheaderline_write_debug(next_attr, sys.stderr.fileno())
        next_line = swheader_get_next_object(swheader,
                                             UCHAR_MAX, UCHAR_MAX)

def swi_check_clean_relative_path(name):
    if is_clean_relative_path(name):
        return
    else:
        swi_com_assert_value(0, fr.__FILE__, fr.__LINE__)
        sys.exit(44)

def swi_is_global_index(swpath, name):
    if (
        (s := name.find("/INDEX")) and s+6 == len(name) and
        len(swpath.get_dfiles()) == 0 and
        len(swpath.get_pfiles()) == 0 and
        len(swpath.get_product_control_dir()) == 0
    ):
        return 1
    else:
        return 0

"""
swi_common.py -  Posix package decoding
   Copyright (C) 2005  James H. Lowe, Jr.  <jhlowe@acm.org>
   All Rights Reserved.

   COPYING TERMS AND CONDITIONS:
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
"""


"""
swi_base.py -- 

   Copyright (C) 2005 Jim Lowe
   Copyright (C) 2024 Paul Weber conversion to Python
   All Rights Reserved.
  
   COPYING TERMS AND CONDITIONS:
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""


class SWI_BASE:
    def __init__(self):
        self.id_start = 85  # Always SWI_BASE_ID_BEGIN
        self.type_id = ''
        self.is_active = 0
        self.b_tag = ''
        self.create_time = create_date()
        self.mod_time = mod_time()
        self.header_index = 0  # index in the global INDEX file to relevant part
        self.global_header = None  # Global INDEX access object
        self.swverid = None
        self.verbose = 0  # verbose level
        self.id_end = 170  # Always SWI_BASE_ID_END
        self.number = ''


class SWI_BASE_Derived_:
    def __init__(self):
        self.base = SWI_BASE()

def swi_vbase_init(derived, type_, index, current):

    base = SWI_BASE_Derived_()
    base.id_start = 85
    base.type_id = type_
    base.is_active = 0
    base.b_tag = None
    base.create_time = 0
    base.mod_time = 0
    base.global_header = index
    base.verbose = 1
    base.swverid = swverid_open(None, None)
    base.id_end = 170
    base.number = None

    if index and current:
        if type_ == swic.SWI_I_TYPE_PROD:
            swverid_set_namespace(base.swverid, swc.SW_A_product)
        else:
            swverid_set_namespace(base.swverid, swc.SW_A_fileset)

        ret = swi_com_set_header_index(index, current, base.header_index)
        if ret != 0:
            sys.stderr.write("swinstall:  Fatal: the section of the Global INDEX file belonging\n"
                             "swinstall:  Fatal: to a package file could not be determined.\n")
        assert ret == 0

        current_file_offset = swheader_get_current_offset(index)
        swheader_set_current_offset(index, base.header_index)
        tag = swheader_get_single_attribute_value(index, swc.SW_A_tag)
        swheader_set_current_offset(index, current_file_offset)

        if not tag:
            sys.stderr.write("swinstall: Fatal: tag attribute not found\n")
        assert tag is not None
        base.b_tag = tag

        number = swheader_get_single_attribute_value(index, swc.SW_A_number)
        if number:
            base.number = number
        else:
            base.number = None
    else:
        base.header_index = 0
        base.b_tag = ""


def swi_base_assert(base):
    assert base.id_start == 85
    assert base.id_end == 170


def swi_base_set_is_active(base, n):
    base.is_active = n


def swi_vbase_update(vbase, user_defined_parameter):
    base = vbase.base
    swi_base_assert(base)
    base.is_active = 1
    if base.create_time == 0:
        base.create_time = int(time.time())
    base.mod_time = int(time.time())
    return 0


def swi_vbase_generate_swverid(derived, user_defined_parameter):
    base = derived.base
    next_attr = ""

    swi_base_assert(base)

    #
    # Store the current position
    #
    current_file_offset = swheader_get_current_offset(base.global_header)

    #
    # Seek to the offset of this object
    #
    swheader_set_current_offset(base.global_header, base.header_index)

    if 1:
        #
        # This code prints the object to stderr, for debugging purposes.
        #
        sys.stderr.write("<<< New Object \n")
        obj = swheader_get_current_line(base.global_header)
        swheaderline_write_debug(obj, sys.stderr.fileno())
        while next_attr == swheader_get_next_attribute(base.global_header):
            swheaderline_write_debug(next_attr, sys.stderr.fileno())
        sys.stderr.write("<<<\n")

        #
        # swlib_doif_writef(base->verbose,  SWC_VERBOSE_SWIDB, (NULL), STDERR_FILENO, "")
        #


    obj = swheader_get_current_line(base.global_header)
    tag = swheader_get_single_attribute_value(base.global_header, swc.SW_A_tag)

    #
    # 'obj' should be an object keyword line
    # Make this sanity assertion
    #

    if swheaderline_get_type(obj) != swpc.SWPARSE_MD_TYPE_OBJ:
        #
        # Sanity check
        #
        SW_IMPL_ERROR_DIE(1)

    #
    #  Generate the version id for this object
    #
    ret = swheader_generate_swverid(base.global_header, base.swverid, obj)
    if ret < 0:
        #
        # error generating version id
        #
        swlib_doif_writef(base.verbose, swlibc.SWC_VERBOSE_1, None, sys.stderr.fileno(),
                          "error generating version id for %s [tag=%s]\n",
                          swheaderline_get_keyword(obj), tag)
        if base.verbose >= swlibc.SWC_VERBOSE_8:
            obj = swheader_get_current_line(base.global_header)
            swheaderline_write_debug(obj, sys.stderr.fileno())
            while next_attr == swheader_get_next_attribute(base.global_header):
                swheaderline_write_debug(next_attr, sys.stderr.fileno())

    #
    # restore the original position
    #
    swheader_set_current_offset(base.global_header, current_file_offset)
    return ret

def swi_vbase_set_verbose_level(derived, verbose_level):
    base = derived.base
    base.verbose = verbose_level
    return 0
