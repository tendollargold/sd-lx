# optparse.py
# CLI options parser.
#
# Copyright (C) 2021 - 2024 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any Red Hat trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License and may only be used or replicated with the express permission of
# Red Hat, Inc.
#

# This file was copied from dnf/cli/option_parser.py

from __future__ import unicode_literals

import logging
import os
import os.path
from sd.compression import depot_file_check
from sd.defaults import setdefaults,override_defaults
from sd.const import swg as g
from sd.control.tasks import swlist_task
from sd.cli.cli import GnuNotice
logger = logging.getLogger("sd")

swdefaults_file = g.DEFAULTS_FILENAME

print("")
print("Entering option_parser.py")
# Command line option override to /etc/ and ~/.sw/

def parse_var(s):
    """
    Parse a key, value pair, separated by '='
    That's the reverse of ShellArgs.

    On the command line (argparse) a declaration will typically look like:
        foo=hello
    or
        foo="hello world"
    """
    items = s.split('=')
    key = items[0].strip() # we remove blanks around keys, as is logical
    if len(items) > 1:
        # rejoin the rest:
        value = '='.join(items[1:])
        return key, value
    else:
        return key, None

def parse_targets(targets):
    """
    Parse the targets to determine type:
        local or remote host
        directory depot, serial file depot, or remote depot
    """
    print("Entered parse_targets")
    # is_file = False
    # x = 0
    for target in targets:
      print("")
      print(target)
      print("")
      if os.path.isfile(target):
         # Determine if the file is a depot file (serial) or a compressed depot
         is_serial=depot_file_check(target)

         # Validate it is a sw depot
      else:
        print(str(target))
        print("Is not a file")

def parse_vars(items):
    """
    Parse a series of key-value pairs and return a dictionary
    """
    d = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            d[key] = value
    return d

def clioption_args(cmd, args):
  print(args)
  print("" )
  print("Entered clioption_args function" )
  print("setting global dictionary swdefaults" )
  print("Setting internal swdefaults for "+cmd)
  swdefaults=setdefaults(cmd)
  print("Internal swdefaults for "+cmd+" are:")
  print(swdefaults)
  print("****** Checking for existance of "+swdefaults_file)
  if os.path.isfile(swdefaults_file):
    print(swdefaults_file+" exists!")
    swdefaults=override_defaults(cmd, swdefaults, swdefaults_file)
  else:
    if os.path.isdir(swdefaults_file):
      print("ERROR: "+swdefaults_file+" is a directory")
      raise SystemExit(1)

  # Check for -X <option file>
  if args.option_file is not None:
    clioptionfile= args.option_file
    if os.path.isfile(clioptionfile):
      swdefaults=override_defaults(cmd, swdefaults, clioptionfile)
    else:
      if os.path.isdir(clioptionfile):
        print("ERROR: "+clioptionfile+" is a directory")
        raise SystemExit(1)

  print("Before checking for -x")
  print(args.x)

  # Check for -x option=value
  if args.x is not None:
    #-x option=value has been entered on the cli
    print("")
    print("Setting -x option=value from cli for "+cmd)
    cliopts = {}
    for opt in args.x:
      # Call parse_vars to create a dictionary of the -x option=value
      # command line option
      cliopts = cliopts|parse_vars(opt)
#   override_defaults(cmd, swdefaults, cliopts)
    swdefaults=override_defaults(cmd, swdefaults, cliopts)
    print("Swdefaults from clioption_args function:")
    print(swdefaults)
  else:
    print("No -x cli options passed for "+cmd)
    
  # SWLIST: The -v option specifies that the format of the output is in the
  # INDEX file format, as defined in Section 5.2 on page 130. Which attributes
  # and objects are included is controlled by other options and operands. 
  # If the -a option is defined, then only those attributes are listed,
  # otherwise all attributes are listed. 
  # If there is no -v option, then listing format is undefined
  # (see one_liner extended option).

  if args.attribute is not None and args.v == False:
    print("")
    print("Only the -a cli option is selected and not the -v option "+cmd)
    print("Showing attributes:")
  elif args.attribute is None and args.v == True:
    print("Only -v is selected on the cli, no -a option")
    print("List all the attribute value pairs of the objects specified.")
  elif args.attribute is None and args.v == False:
    print("Neither -v nor -a options were selected on the command line")
    print("Show no specific attributes for cmd")
  else:
    print("Check what was on the command line.. no check for this")

  # Targets
  if args.gnu:
     GnuNotice(cmd)
     
  if args.T is None:
       print("")
       print("")
       print("")
       print("")
       print("")
       print("No target specified, assume local listing")
       swdefaults['targets']="localhost:/"
       print(swdefaults['targets'])
  else:
       swdefaults['targets']=args.T
       print("")
       print("")
       print(args.T)
       print("Targets specified")
       print("calling parse_targets")
       parse_targets(args.T)


  if cmd == 'swlist':
    if args.R:
      print("list Bundles, products, subproducts, and filesets")
    else:
      # The -l <level> Specifies level at which to list the objects below 
      # the specified software.
      # Option level may have values from the enumerated list:
      #     bundle
      #     product
      #     subproduct
      #     fileset
      #     control_file
      #     file
      #     category

      # If the -l level option is not included, then only the object at 
      # the level directly below the specified software or software_collection
      # is listed. See Table 4-1.

      #                  Software Selection | Level Listed
      #                  ---------------------------------
      #                  none specified     | products
      #                  bundle             | products
      #                  product            | filesets
      #                  subproduct         | filesets
      #                  fileset            | files
      #                       Table 4-1 Default Levels

      # If no level is specified for bundle and subproduct specifications, 
      # all the available or currently installed product and fileset objects, 
      # resolved recursively, are listed.

      if args.l == 'bundle':
        print("Shows only bundles @target")

      if args.l is None or args.l == 'product':
        print("Shows only products @ target")
        swlist_task(args.l, swdefaults, args.software_selections, args.attribute, args.T)

      if args.l == 'subproduct':
        print("Shows products and subproducts @ target")

      if args.l == 'fileset':
        print("Shows products, subproducts, and filesets @ target")

      if args.l == 'file':
        print("list products, subproducts, filesets, files, and numbers @ target")
        print("numbers are used in software licensing")

      if args.l == 'depot':
        print("Shows the registered depots @target")

      if args.l == 'root':
        print("Shows the root level (roots on the specified target hosts)")
        
      if args.l == 'control_file':
        print("list control_file @target")
        print("Shows the control_file for specific product")
        print("Not Implemented")

      if args.l == 'category':
        print(" Shows all categories of available patches for patches that have included category objects in their definition. @target")
        print("Not Implemented")

      if args.l == 'patch':
        print("Shows all applied patches @target")
        print("Not Implemented")

      if args.l == 'shroot':
        print("Shows the shared roots @target")
        print("Not Implemented")

      if args.l == 'prroot':
        print("Shows the private roots @target")
        print("Not Implemented")



print("Exiting option_parser.py")
print("")
