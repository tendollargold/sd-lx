# copyinold.py - readin old ascii cpio headers
#   Copyright (C) 1990, 1991, 1992 Free Software Foundation, Inc.
#
#   Portions of this code are derived from code (found in GNU cpio)
#   copyrighted by the Free Software Foundation.  Retention of their
#   copyright ownership is required by the GNU GPL and does *NOT* signify
#   their support or endorsement of this work.
#   						jhl
#
#   Converted to Python, Paul Weber
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import ctypes
import struct
import sys

from sd.swsuplib.taru.filetypes import ft
from sd.swsuplib.taru.ahs import ahsstaticsettarfilenamelength, ahsstaticgettarfilename
from sd.swsuplib.taru.readinheader import taru_tape_buffered_read

def sscanf(fp, fmt, argp, flags):
    libc = ctypes.CDLL("libc.so.6")
    return libc.sscanf(fp, fmt, argp, flags)

def taru_read_in_old_ascii2(taru, file_hdr, in_des, buf):
    ascii_header = bytearray(78)
    if buf:
        ascii_header[:70] = buf[:70]
    else:
        if os.pread(in_des, 0, 70) != 70:
            return -1
    bytesread = 70

    # sscanf(ascii_header,"%6lo%6lo%6lo%6lo%6lo%6lo%6lo%11lo%6lo%11lo", dev, file_hdr.c_ino, file_hdr.c_mode, file_hdr.c_uid, file_hdr.c_gid, file_hdr.c_nlink, rdev, file_hdr.c_mtime, file_hdr.c_namesize, xxsize)
    dev, file_hdr.c_ino, file_hdr.c_mode, file_hdr.c_uid, file_hdr.c_gid, file_hdr.c_nlink, rdev, file_hdr.c_mtime, file_hdr.c_namesize, xxsize = struct.unpack(
        "6lo6lo6lo6lo6lo11lo6lo11lo", ascii_header)
    file_hdr.c_filesize = xxsize
    file_hdr.c_dev_maj = os.major(dev)
    file_hdr.c_dev_min = os.minor(dev)
    file_hdr.c_rdev_maj = os.major(rdev)
    file_hdr.c_rdev_min = os.minor(rdev)

    # Read file name from input.

    ahsstaticsettarfilenamelength(file_hdr, file_hdr.c_namesize + 1)

    if not buf:
        if (ret := taru_tape_buffered_read(in_des, (ahsstaticgettarfilename(file_hdr)), int(file_hdr.c_namesize))) != int((file_hdr.c_namesize)):
            return -bytesread
    else:
        # Simulate a read from tape
        if file_hdr.c_namesize > 442:
            sys.stderr.write("taru_read_in_oldascii2 name too long for this implementation.\n")
            sys.exit(33)
            # return -1 # Paul W- Can't return if one has already sys.exit
        ahsstaticgettarfilename(file_hdr)[:] = buf[70:70 + file_hdr.c_namesize]
    bytesread += file_hdr.c_namesize

    # HP/UX cpio creates archives that look just like ordinary archives,
    # but for devices it sets major = 0, minor = 1, and puts the
    # actual major/minor number in the filesize field.  See if this
    # is an HP/UX cpio archive, and if so fix it.  We have to do this
    # here because process_copy_in() assumes filesize is always 0
    # for devices.

    if (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFCHR) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFBLK) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFSOCK) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFIFO):
        if file_hdr.c_filesize != 0 and file_hdr.c_rdev_maj == 0 and file_hdr.c_rdev_min == 1:
            file_hdr.c_rdev_maj = os.major(file_hdr.c_filesize)
            file_hdr.c_rdev_min = os.minor(file_hdr.c_filesize)
            file_hdr.c_filesize = 0
    return bytesread





