###############################################################################
##  This control script provides the necessary common infrastructure
##  to install control scripts for the muxplex product.
##
##  This file has dependencies on the control_utils file provided by SD
##  for several functions and variables.
##
## (c)Copyright 2001-2002 Hewlett-Packard Co.,  All Rights Reserved.
##
###############################################################################

#set -o xtrace


#
# Set platform dependent variables.
#
INIT_DIR=/sbin/init.d
CONF_PATH=/etc/rc.config.d


##
## Define some directories.
##
typeset INITTAB=/etc/inittab
typeset NEW_INITTAB=/etc/new_inittab
typeset OPT_MX=/opt/mx
typeset VAR_OPT_MX=/var${OPT_MX}
typeset ETC_OPT_MX=/etc${OPT_MX}
typeset OPT_WEB_MX=/opt/hpwebadmin

typeset MXLBIN_DIR=${OPT_MX}/lbin
typeset MXBIN_DIR=${OPT_MX}/bin
typeset MXLIB_DIR=${OPT_MX}/lib
typeset MXJ2RE_DIR=${OPT_MX}/j2re
typeset MXNEWCONFIG_DIR=${OPT_MX}/newconfig
typeset MXSETUP_DIR=${VAR_OPT_MX}/setup
typeset MXTOOLS_DIR=${VAR_OPT_MX}/tools
typeset MXLOG_DIR=${VAR_OPT_MX}/logs
typeset MXSHARE_DIR=${OPT_MX}/share
typeset MXMAN_DIR=${MXSHARE_DIR}/man
typeset MXMAN_NLS_DIR=${MXSHARE_DIR}/man/%L
typeset MXDOC_DIR=${MXSHARE_DIR}/doc
typeset MXETCCONFIG_DIR=${ETC_OPT_MX}/config

##
## Define some files.
##
typeset MXPROPFILE=${MXETCCONFIG_DIR}/mx.properties
typeset MXJAVAPOLICY_FILE=${MXETCCONFIG_DIR}/java.policy

typeset SETUP_STATUS_FILE=${MXETCCONFIG_DIR}/setup.properties


##
## Define some constants.
##
typeset NEWTOOL_FILES=$( ls ${MXNEWCONFIG_DIR}/${MXTOOLS_DIR}/*.xml 2>/dev/null)

typeset -i10 FALSE=0
typeset -i10 TRUE=1


if [ -z "$SUCCESS" ]
then
    SUCCESS=0
fi
if [ -z "$FAILURE" ]
then
    FAILURE=1
fi
if [ -z "$WARNING" ]
then
    WARNING=2
fi

#
# Variables defined by a function in this script:
#
typeset INIT_DIR
typeset CONF_PATH
typeset myPreviousVersion=


###############################################################################
## Function : mx_cond_rm.
## Purpose  : Remove listed files, e.g. obsoleted files.
## Parameter: $1.. = A space delimited list of file names to remove.
## Returns  : $SUCCESS or $FAILURE
###############################################################################
mx_cond_rm()
{
    if [ $# -lt 1 ]
    then
        msg error "Internal Error: Invalid number of parameters passed to mx_cond_rm()."
        exit 1
    fi

    typeset file
    typeset retval=$SUCCESS

    for file in "$@"
    do
        if [ -a "$file" ]
        then
            rm -rf "$file" >/dev/null 2>&1
            if [ $? -ne $SUCCESS ]
            then
                msg error "Cannot remove '$file'."
                retval=$FAILURE
            fi
        fi
    done

    return $retval
}

###############################################################################
## Function : mx_cond_rmdir.
## Purpose  : Remove listed directories.  The directories must be empty to be removed
## Parameter: $1    = -R [optional] to recursively enter subdirectories to remove.
## Parameter: $1... = A space delimited list of dir names to remove.
## Returns  : $SUCCESS
###############################################################################
mx_cond_rmdir()
{
    if [ $# -lt 1 ]
    then
        msg error "Internal Error: Invalid number of parameters passed to mx_cond_rmdir()."
        exit 1
    fi

    typeset -i10 recurse_mode=0
    typeset -i10 retval=$SUCCESS

    #
    # Use getopts to parse the command line options
    # (the ':' as the first char in the getopts string is a kludge
    #  that prevents getopts from printing an error message to stderr).
    #
    while getopts ":R" opt
    do
        case "$opt" in
            R  )
                recurse_mode=1
                ;;
            \? )
                msg error "Internal Error: Invalid parameter passed to mx_cond_rmdir()."
                exit 1
                ;;
        esac
    done
    shift $(( $OPTIND - 1 ))

    typeset dir
    for dir in "$@"
    do
        if [ -d "$dir" ]
        then
            if [ $recurse_mode -eq 1 ]
            then
                #
                # NOTE: Not all versions of `find` allow the + for -exec.
                #
                typeset rmdirs=$( find "$dir" -type d -depth 2>/dev/null )
                rmdir -f $rmdirs >/dev/null 2>&1
            else
                rmdir -f "$dir"  >/dev/null 2>&1
            fi
        fi
    done

    return $retval
}


###############################################################################
## Function: mx_Daemon_Stop_Old
## Purpose:  Remove SCM daemon entries from the /etc/inittab file.
## Returns  : $SUCCESS or $WARNING
###############################################################################
mx_Daemon_Stop_Old()
{
    if [ $# -lt 1 ]
    then
        msg error "Internal Error: Invalid number of parameters passed to mx_Daemon_Stop_Old()."
        exit 1
    fi

    ##
    ## Check to see if this is occurring during a Cold Install, when the
    ## /etc/inittab file does not yet exist.  If it is, then return, since no
    ## other actions in this function are relevant in that scenario.
    ##
    if [ "$SW_INITIAL_INSTALL" -eq 1 ]
    then
#
# JAGaf44479 SysMgmtServer - Cannot stop the daemons 'mxinventory'
# The warning error message is being printed out during a cold
# install. Since there is no mxinventory daemon during a cold
# install, a SUCCESS error code should be returned to prevent
# the printing of the error message.
#
        return $SUCCESS
    fi

    typeset retval=$SUCCESS
    typeset daemon_list="$@"
    typeset grep_string
    typeset daemon

    for daemon in $daemon_list
    do
        grep_string="$grep_string -e $daemon"
    done

    if [ -z "$grep_string" ]
    then
        msg error "Internal Error: The grep_string variable has not been set."
        exit 1
    fi

    if [ -r "$INITTAB" ]
    then
        grep -v $grep_string $INITTAB > $NEW_INITTAB
        ##
        ## Compare the new inittab with the old one.
        ##
        diff $INITTAB $NEW_INITTAB 1>/dev/null 2>&1
        isDiff=$?

        if [ -s "$NEW_INITTAB" ] && [ $isDiff -eq 1 ]
        then
	    msg note "Removing HP SIM entries from $INITTAB"
            cat $NEW_INITTAB >$INITTAB 2>/dev/null
            ##
            ## If we're in an OS update situation, don't try to run the init
            ## command.  This could result in a Bad system call problem.
            ## Otherwise, run init to keep the processes removed from
            ## restarting when they are killed below.
            ##
            if [ "$MX_INSTALLED_WITH_REBOOT" -ne "$TRUE" ]
            then

		# if in drd session, do not call init q
		if [[ $SW_SESSION_IS_DRD -ne 1 ]]
		then
                  init q
		fi
            fi
        fi

        #
        # Kill the process regardless of whether or not it was in the
	# inittab file.
        #
        for daemon in $daemon_list
        do
            kill_named_procs $daemon 1>/dev/null 2>&1
	    if [ $? -eq 1 ]
	    then
	      msg warning "Unable to shutdown daemon, $daemon"
	    fi
        done

        rm -f $NEW_INITTAB
    fi

    return $retval
}



###############################################################################
## Function: mx_swcleanup_scm
## Purpose:  Remove files that were busy during install, so they were copied to
##           '#<filename' by SD.
## Returns:  $SUCCESS.
###############################################################################
mx_swcleanup_scm()
{
    typeset SW_CLEANUP_FILE=/var/adm/sw/cleanupfile
    typeset MX_CLEANUP_FILE=${VAR_OPT_MX}/cleanupfile
    typeset retval=$SUCCESS

    grep -s "\/mx\/" $SW_CLEANUP_FILE >| $MX_CLEANUP_FILE 2>/dev/null

    if [ -s "$MX_CLEANUP_FILE" ]
    then
        mx_cond_rm $( <$MX_CLEANUP_FILE ) 2>/dev/null
    fi

    if [ $retval -eq $SUCCESS ]
    then
        grep -s -v "\/mx\/" $SW_CLEANUP_FILE >| $MX_CLEANUP_FILE 2>/dev/null
        cat $MX_CLEANUP_FILE >| $SW_CLEANUP_FILE 2>/dev/null
        mx_cond_rm $MX_CLEANUP_FILE 2>/dev/null
    fi

    return $retval
}


###############################################################################
## Function: mx_setStatus_Info
## Purpose : Store the SCM status in a file for reference after that state changes.
##           Only run this once at the start of the install process.
## return  : $SUCCESS or $FAILURE
###############################################################################
mx_setStatus_Info()
{
    #set -o xtrace
    typeset -i10 retval=$SUCCESS
    ##
    ## Determine the current status of the SCM node prior to making
    ## any status changes:
    ##   is this an update situation.
    ##   is this a reboot situation.
    ##

    # determine if any previous version is installed
    # when the 4.2 release was added to the 11.23 OE, the version info
    # was not put into setup.properties properly
    # for the cold install, the MX_INSTALL_VERSION entry is missing
    # for OS/OE upgrade, the MX_INSTALL_VERSION entry has the wrong version
    # info -- it contains the version of the previous product
    # starting with hp sim 5.0, the version info in setup.properties should
    # be correct
    typeset previousVersion=unknown

    # check for scm 2.5
    # note that checkinstall have already filter out any version before 2.5
    # scm 2.5, product tag: ServControlMgr, Version: A.02.05
    # scm 3.0, product tag: SysMgmtServer, Version: B.03
    # hp sim 4.x, product tag: SysMgmtServer, Version: C.04
    # hp sim 5.X, product tag: SysMgmtServer, Version: C.05
    #
    swlist -l product ServControlMgr > /dev/null 2>/dev/null
    if [ $? -eq 0 ]
    then
      # have scm 2.5, get the version number
      previousVersion=$( swlist -l product ServControlMgr 2>/dev/null | grep ServControlMgr | awk '{ print $2 }' )
    else
      # check to see if have any previous version installed
      swlist -l product SysMgmtServer,r\<C.05 > /dev/null 2> /dev/null
      if [ $? -eq 0 ]
      then
	# have previous version installed, get the version number
        previousVersion=$( swlist -l product SysMgmtServer,r\<C.05 2>/dev/null | grep SysMgmtServer | awk '{ print $2 }' )
      fi
    fi


    #
    # Make sure we have a setup property file.
    #
    if [ -a $SETUP_STATUS_FILE ]
    then
      # since the previous setup.properties exist, get the installed version
      # number from it to be used as previous, if we have not gotten it already
      # 
      # note that during the preinstall phase, the old and new product info
      # exist at the same time in sd, so calling mx_checkForPreviousProduct
      # may or may not return the correct info
      # 
      # mx_checkForPreviousProduct can only be used properly during the
      # checkinstall phase
      #
      if [ "$previousVersion" = "unknown" ]
      then
        previousVersion=$( cat /etc/opt/mx/config/setup.properties | grep MX_INSTALL_VERSION | awk -F= '{ print $2 }' )
      fi
    else
        typeset SETUP_STATUS_DIR=${SETUP_STATUS_FILE%/*}
        typeset SETUP_STATUS_DIR2=${SETUP_STATUS_DIR%/*}

        #mkdir -p   ${MXNEWCONFIG_DIR}/$SETUP_STATUS_DIR  2>/dev/null
        #touch      ${MXNEWCONFIG_DIR}/$SETUP_STATUS_FILE 2>/dev/null

        mkdir -p   $SETUP_STATUS_DIR  2>/dev/null
        chmod 755  $SETUP_STATUS_DIR  2>/dev/null
        chown root $SETUP_STATUS_DIR  2>/dev/null
        chgrp root $SETUP_STATUS_DIR  2>/dev/null

        chmod 555  $SETUP_STATUS_DIR2 2>/dev/null
        chown root $SETUP_STATUS_DIR2 2>/dev/null
        chgrp bin  $SETUP_STATUS_DIR2 2>/dev/null

        touch      $SETUP_STATUS_FILE 2>/dev/null
        chmod 444  $SETUP_STATUS_FILE 2>/dev/null
        chown root $SETUP_STATUS_FILE 2>/dev/null
        chgrp bin  $SETUP_STATUS_FILE 2>/dev/null

        if [ ! -a "$SETUP_STATUS_FILE" ]
        then
            msg warning "Cannot create file: $SETUP_STATUS_FILE"
            return $WARNING
        fi

    fi

    #
    # Is a reboot product being installed?
    #
    if [ "$SW_SESSION_IS_REBOOT" -eq "TRUE" ]
    then
        ch_rc -a -p MX_INSTALLED_WITH_REBOOT=1 $SETUP_STATUS_FILE 2>/dev/null
    else
        ch_rc -a -p MX_INSTALLED_WITH_REBOOT=0 $SETUP_STATUS_FILE 2>/dev/null
    fi

    #
    # record the previous version if found
    #
    if [ "$previousVersion" = "unknown" ]
    then
      ch_rc -r -p MX_PREVIOUS_VERSION $SETUP_STATUS_FILE 2>/dev/null
    else
      ch_rc -a -p "MX_PREVIOUS_VERSION=$previousVersion" $SETUP_STATUS_FILE 2>/dev/null
    fi

    #
    # Record the installing version.
    #
    typeset installVersion=$(get_version)
    installVersion=$(echo ${installVersion#*r=} | cut -d , -f 1)
    ch_rc -a -p "MX_INSTALL_VERSION=$installVersion" $SETUP_STATUS_FILE 2>/dev/null


    . $SETUP_STATUS_FILE
    return $retval
}


###############################################################################
## Function: mx_checkForPreviousProduct
## Purpose : Report version of a previously installed product.
## Set     : myPreviousVersion to the version number of any previous product.
## return  : $SUCCESS or $FAILURE
##
## note: This function works properly only during the checkinstall phase.
##       During the preinstall phase both the old and new product information
##       are on the system at the same time.
###############################################################################
mx_checkForPreviousProduct()
{
    #set -o xtrace
    typeset -i10 retval=$SUCCESS

    #
    # Report if there is a previous product is installed.
    # NOTE: assume only 1 product is installed.
    #
    typeset prodList="ServControlMgr SysMgmtServer SysMgmtAgent"
    typeset prod
    for prod in $prodList
    do
        #
        # Check if any earlier releases of the product exist.
        #
        swliststring=$(swlist -a revision -x one_liner=title -x verbose=0 $prod  2>/dev/null | grep "^#[ \t]*$prod")
        if [ $? -eq 0 ]
        then
            if [ -a /opt/mx/bin/mxversion ]
            then
                typeset version=$(/opt/mx/bin/mxversion -r 2>/dev/null | awk '{print $1 ; exit}' 2>/dev/null)

                if [ -z "$version" ]
                then
                    typeset swrevision=$(echo $swliststring | awk -vprod=$prod '{split($0,arr,prod); print arr[2]}' 2>/dev/null)
                    if [ -n "$swrevision" ]
                    then
                        typeset -L version="$swrevision"
                    else
                        version="B.03.00 or later (unknown)"
                    fi
                fi
            else
                # mxversion did not exist prior to B.03.00
                swrevision=$(echo $swliststring | awk -vprod=$prod '{split($0,arr,prod); print arr[2]}' 2>/dev/null)
                if [ -n "$swrevision" ]
                then
                    typeset -L version="$swrevision"
                else
                    version="A.02.05 or earlier (unknown)"
                fi
            fi

            myPreviousVersion=$version

            # Assume there is only 1 product installed.
            break

        fi  # end of "if product exists"
    done    # try next product

    return $retval
}


