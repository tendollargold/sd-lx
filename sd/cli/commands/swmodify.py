#!/usr/bin/python3

# swmodify.py - swmodify - modify the IPD and catalog files
# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
cmd = os.path.split(sys.argv[0])[1]

swmodify_parser = argparse.ArgumentParser(prog='swmodify',
	usage="""%(prog)s [-d] [-r] [-p] [-u] [-v [-V] [-g] [-a attribute]
       [-c catalog] [-C session_file] [-f software_file] [-P pathname_file ]
       [-s product_specification_file] [-S session_file] [-x option=value]
       [-X option_file] [software_selections] [-T target_selections]""",
        formatter_class=argparse.RawTextHelpFormatter,
        epilog='''
 Software selections are specified as:

     {product|bundle[.product]}[.subproduct][.fileset][,version] ...

 where version is

     [r=revision][,a=arch][,v=vendor][,c=category]

 The target selection is specified as:

     -T path


''')


swmodify_parser.add_argument("-r --root",metavar='root', 
			    help="target selection is an alternate root directory")
swmodify_parser.add_argument("-d --depot",metavar='depot', 
			    help="target selections are software depots")
swmodify_parser.add_argument("-p --preview",metavar='', 
			    help="preview the session without modifying anything")
swmodify_parser.add_argument("-v --verbose",action="store_true", 
			    help="write verbose output to stdout")
swmodify_parser.add_argument("-V",metavar='', 
			    help="list SD layout_versions this command supports")
swmodify_parser.add_argument("-u --unconfigure",metavar='', 
			    help="delete the software selections (or attributes)")
swmodify_parser.add_argument("-s --source",metavar='source', 
			    help="read the software selections from this source")
swmodify_parser.add_argument("-a --attribute[=value]",metavar='', 
			    help="add, modify, or delete (the value of) the attribute \nfrom the software selections")
swmodify_parser.add_argument("-P",metavar='path_file', 
			    help="read added/deleted pathnames from this file")
swmodify_parser.add_argument("-x option=value",metavar='', 
			    help="set the option to value")
swmodify_parser.add_argument("-X",metavar='option_file', 
			    help="read option definitions from this file")
swmodify_parser.add_argument("-C",metavar='session_file', 
			    help="save the session to session_file")
swmodify_parser.add_argument("-S",metavar='session_file', 
			    help="read options, software selections, and target \nselections from this file")
swmodify_parser.add_argument("-f",metavar='software_file', 
			    help="read software selections from this file")
# -g --gnu option added for GNU Copyleft
swmodify_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")
swmodify_parser.parse_args()
args = swmodify_parser.parse_args()
print("DEBUG: Args in: ", cmd,".py ",  args)

#ERROR:   You have not specified any software selections, and you have
#         not specified any file or attribute modifications.  To modify
#         an existing software object's files or attributes, you must
#         specify the software and the modifications.  See the SD user
#         manual or the swmodify(1m) manual page for more details.

