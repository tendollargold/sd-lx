# mtar.py - tar/cpio routines

#   Copyright (C) 2003 Jim Lowe
#   Copyright (c) 2023 Paul Weber convert to python
#   All Rights Reserved.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# UNIX� is a registered trademark of The Open Group.


import os
import stat
import sys
import time

from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.taru.ahs import AHS, ahsstaticdeletefilehdr, ahsstaticcreatefilehdr

from sd.swsuplib.misc.swlib import swlib_process_hex_escapes
from sd.swsuplib.taru.tar import tarc
from sd.swsuplib.taru.filetypes import ft
from sd.swsuplib.strob import strob_strcpy, strob_str, strob_close, strob_open
from sd.swsuplib.taru.otar import taru_tar_checksum, taru_split_name_ustar, taru_write_out_tar_header2
from sd.swsuplib.taru.taru import TaruConstants
from sd.swsuplib.taru.tooct import gaf

ahs = AHS()
tc = TaruConstants()
TARRECORDSIZE = 512
FALSE = 0
TRUE = 1


def taru_dup_tar_linkname(pt):
    tar_hdr = pt
    hold_tar_linkname = tar_hdr[157:157 + 100].decode('utf-8')
    return hold_tar_linkname


def stash_tar_filename(prefix, filename):
    if prefix is None or prefix == '':
        hold_tar_filename = filename[:100]
    else:
        hold_tar_filename = prefix[:155] + '/' + filename[:100]
    return hold_tar_filename


def taru_dup_tar_name(pt):
    tar_hdr = pt
    if tar_hdr[257:257 + 5].decode('utf-8') != 'ustar':
        return stash_tar_filename(__eq__, tar_hdr)
    else:
        return stash_tar_filename(tar_hdr[345:345 + 155].decode('utf-8'), tar_hdr)


def taru_is_tar_linkname_too_long(name, tarheaderflags, p_do_gnu_long_link):
    too_long = 1
    do_gnu_longlinks = tarheaderflags & tc.TARU_TAR_GNU_LONG_LINKS
    do_exthdr = tarheaderflags & tc.TARU_TAR_PAXEXTHDR
    if p_do_gnu_long_link:
        p_do_gnu_long_link[0] = 0
    if len(name) <= 100:
        too_long = 0
    elif len(name) > 100:
        if do_gnu_longlinks:
            if len(name) < TARRECORDSIZE:
                too_long = 0
                if p_do_gnu_long_link:
                    p_do_gnu_long_link[0] = 1
            else:
                pass
        elif do_exthdr:
            too_long = 0
        else:
            pass
    else:
        pass
    return too_long

def taru_is_tar_filename_too_long(name, tarheaderflags, p_do_gnu_long_link, is_dir):
    retval = 1
    whole_name_len = len(name)
    did_squash = 0
    if name[-1] == '/':
        name = name[:-1]
        did_squash = 1
    do_gnu_longlinks = tarheaderflags & tc.TARU_TAR_GNU_LONG_LINKS
    do_exthdr = tarheaderflags & tc.TARU_TAR_PAXEXTHDR
    if (do_gnu_longlinks and (tarheaderflags & tc.TARU_TAR_NAMESIZE_99)) or (
            tarheaderflags & tc.TARU_TAR_GNU_OLDGNUTAR):
        tarnamesize = 99
    else:
        tarnamesize = 100
    if is_dir:
        tarnamesize -= 1
    if p_do_gnu_long_link:
        p_do_gnu_long_link[0] = 0
    if whole_name_len < tarnamesize:
        retval = 0
    elif (whole_name_len > (tarnamesize + 155 + 1)) and (do_gnu_longlinks == 0 and do_exthdr == 0):
        retval = 1
    else:
        p = 0
        while p < whole_name_len:
            while p < whole_name_len and name[p] != '/':
                p += 1
            if p == whole_name_len:
                break
            if (p < 155) and ((len(name) - 1 <= tarnamesize) and p != 0):
                retval = 0
                break
            else:
                p += 1
        if p == whole_name_len:
            retval = 1
    if retval:
        if do_gnu_longlinks:
            if whole_name_len >= 512:
                print("tar filename too long, location = 3")
                retval = 1
            elif whole_name_len > tarnamesize:
                if p_do_gnu_long_link:
                    p_do_gnu_long_link[0] = 1
                retval = 0
            else:
                retval = 0
        elif do_exthdr:
            if p_do_gnu_long_link:
                p_do_gnu_long_link[0] = 0
            retval = 0
    if did_squash:
        name += '/'
    return retval


def taru_filehdr2statbuf(statbuf, file_hdr):
    statbuf.st_mode = file_hdr.c_mode
    statbuf.st_ino = file_hdr.c_ino
    statbuf.st_dev = os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min)
    statbuf.st_rdev = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
    statbuf.st_nlink = file_hdr.c_nlink
    statbuf.st_uid = file_hdr.c_uid
    statbuf.st_gid = file_hdr.c_gid
    statbuf.st_size = file_hdr.c_filesize
    statbuf.st_atime = time.time()
    statbuf.st_ctime = time.time()
    statbuf.st_mtime = file_hdr.c_mtime
    statbuf.st_blksize = 512
    statbuf.st_blocks = statbuf.st_size // 512 + 1
    return 0


def taru_filehdr2filehdr(file_hdr_dst, file_hdr_src):
    file_hdr_dst.c_magic = file_hdr_src.c_magic
    file_hdr_dst.c_ino = file_hdr_src.c_ino
    file_hdr_dst.c_mode = file_hdr_src.c_mode
    file_hdr_dst.c_uid = file_hdr_src.c_uid
    file_hdr_dst.c_gid = file_hdr_src.c_gid
    file_hdr_dst.c_nlink = file_hdr_src.c_nlink
    file_hdr_dst.c_mtime = file_hdr_src.c_mtime
    file_hdr_dst.c_ctime = file_hdr_src.c_ctime
    file_hdr_dst.c_ctime_nsec = file_hdr_src.c_ctime_nsec
    file_hdr_dst.c_atime = file_hdr_src.c_atime
    file_hdr_dst.c_atime_nsec = file_hdr_src.c_atime_nsec
    file_hdr_dst.c_filesize = file_hdr_src.c_filesize
    file_hdr_dst.c_dev_maj = file_hdr_src.c_dev_maj
    file_hdr_dst.c_dev_min = file_hdr_src.c_dev_min
    file_hdr_dst.c_rdev_maj = file_hdr_src.c_rdev_maj
    file_hdr_dst.c_rdev_min = file_hdr_src.c_rdev_min
    file_hdr_dst.c_namesize = file_hdr_src.c_namesize
    file_hdr_dst.c_chksum = file_hdr_src.c_chksum
    file_hdr_dst.c_cu = file_hdr_src.c_cu
    file_hdr_dst.c_cg = file_hdr_src.c_cg
    file_hdr_dst.c_is_tar_lnktype = file_hdr_src.c_is_tar_lnktype
    file_hdr_dst.linkname = file_hdr_src.linkname
    file_hdr_dst.filename = file_hdr_src.filename
    file_hdr_dst.username = file_hdr_src.username
    file_hdr_dst.groupname = file_hdr_src.groupname

def taru_statbuf2filehdr(file_hdr, statbuf, sourcefilename, filename, linkname):
    file_hdr.c_mode = statbuf.st_mode & 0o7777
    if not sourcefilename:
        sourcefilename = filename
    if stat.S_ISREG(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFREG
    elif stat.S_ISDIR(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFDIR
    elif stat.S_ISBLK(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFBLK
    elif stat.S_ISCHR(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFCHR
    elif stat.S_ISFIFO(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFIFO
    elif stat.S_ISDOOR(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFDOOR
    elif stat.S_ISLNK(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFLNK
    elif stat.S_ISSOCK(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFSOCK
    elif ft.S_ISNWK(statbuf.st_mode):
        file_hdr.c_mode |= ft.CP_IFNWK
    file_hdr.c_ino = statbuf.st_ino
    file_hdr.c_nlink = statbuf.st_nlink
    file_hdr.c_uid = statbuf.st_uid
    file_hdr.c_gid = statbuf.st_gid
    file_hdr.c_filesize = statbuf.st_size
    file_hdr.c_mtime = statbuf.st_mtime
    file_hdr.c_ctime = statbuf.st_ctime
    file_hdr.c_atime = statbuf.st_atime
    file_hdr.c_atime_nsec = 0
    file_hdr.c_ctime_nsec = 0
    file_hdr.c_dev_maj = os.major(statbuf.st_dev)
    file_hdr.c_dev_min = os.minor(statbuf.st_dev)
    file_hdr.c_rdev_maj = os.major(statbuf.st_rdev)
    file_hdr.c_rdev_min = os.minor(statbuf.st_rdev)
    if filename:
        file_hdr.filename = filename
        file_hdr.c_namesize = len(filename) + 1
    else:
        file_hdr.filename = ""
        file_hdr.c_namesize = 1
    if filename and len(filename):
        if stat.S_ISLNK(statbuf.st_mode) and linkname and not len(linkname):
            symbuf = os.readlink(sourcefilename)
            if len(symbuf) > 100:
                print("warning: linkname too long for tar format:", sourcefilename)
            file_hdr.linkname = linkname
        elif stat.S_ISLNK(statbuf.st_mode) and linkname and len(linkname):
            file_hdr.linkname = linkname
        else:
            file_hdr.linkname = linkname
    file_hdr.c_is_tar_lnktype = -1
    return 0

def taru_set_tar_header_sum(fp_tar_hdr, tar_iflags):
    tar_iflags_like_star = (tar_iflags & tc.TARU_TAR_BE_LIKE_STAR)
    tar_iflags_like_pax = (tar_iflags & tc.TARU_TAR_BE_LIKE_PAX)

    termch = '\040' if tar_iflags_like_star else 0

    if fp_tar_hdr:
        tar_hdr = fp_tar_hdr
    else:
        print(f"{swlib_utilname_get()}: internal error in taru_set_header_sum", file=sys.stderr)
        sys.exit(34)

    sum_ = taru_tar_checksum(tar_hdr)

    if tar_iflags_like_pax or tar_iflags_like_star:
        gaf.uintmax_to_chars(sum_, tar_hdr.chksum, 8, gaf.POSIX_FORMAT, termch)
    else:
        gaf.uintmax_to_chars(sum_, tar_hdr.chksum, 7, gaf.POSIX_FORMAT, termch)

    return 0


#  taru_set_new_linkname moved to taru.py! 20240125


def taru_set_new_name(fp_tar_hdr, length, fpname, tar_iflags):
    name = fpname
    tmp = None

    if "\\x" in fpname:
        tmp = strob_open(64)
        strob_strcpy(tmp, name)
        swlib_process_hex_escapes(strob_str(tmp))
        name = strob_str(tmp)

    if fp_tar_hdr:
        tar_hdr = fp_tar_hdr
    else:
        print(f"{swlib_utilname_get()}: internal error in taru_set_new_name", file=sys.stderr)
        sys.exit(33)

    if len(name) <= tarc.TARNAMESIZE:
        tar_hdr.name = name.ljust(tarc.TARNAMESIZE)
        ret = 0
    else:
        ret = taru_split_name_ustar(tar_hdr, name, tar_iflags)
        if ret:
            print(f"{swlib_utilname_get()}: error splitting name", file=sys.stderr)
            return ret

    taru_set_tar_header_sum(fp_tar_hdr, tar_iflags)
    if tmp:
        strob_close(tmp)

    return ret


def taru_write_long_link_member(taru, out_file_des, filename, gnu_long_type, c_type, tarheaderflags):
    is_dir = (c_type & 0o170000) == 0o040000
    file_hdr = ahsstaticcreatefilehdr()
    do_add_trailing_slash = 0
    if is_dir and len(filename) and filename[-1] != '/' and len(filename) < 511:
        do_add_trailing_slash = 1
    if len(filename) >= 510:
        return -1
    file_hdr.c_mode = 0
    if (c_type & 0o170000) == 0o100000 or (c_type & 0o170000) == 0o040000:
        file_hdr.filename = 'GNU_LONG_LINK'
        if gnu_long_type == 1:
            file_hdr.linkname = ''
        else:
            file_hdr.linkname = filename
    else:
        file_hdr.filename = 'GNU_LONG_LINK'
        file_hdr.linkname = filename
        file_hdr.c_mode = c_type
    file_hdr.c_namesize = len('GNU_LONG_LINK') + 1
    file_hdr.c_ino = 0
    file_hdr.c_nlink = 0
    file_hdr.c_uid = 0
    file_hdr.c_gid = 0
    file_hdr.c_filesize = len(filename) + 1 + do_add_trailing_slash
    file_hdr.c_mtime = 0
    file_hdr.c_dev_maj = 0
    file_hdr.c_dev_min = 0
    file_hdr.c_rdev_maj = 0
    ret = taru_write_out_tar_header2(taru, file_hdr, out_file_des, None, "root", "root", tarheaderflags)
    if ret == 512:
        namebuf = filename
        if do_add_trailing_slash:
            namebuf += '/'
        if os.write(out_file_des, namebuf.encode('utf-8')) != 512:
            pass
        else:
            ret += 512
    else:
        return -1
    ahsstaticdeletefilehdr(file_hdr)
    return ret

