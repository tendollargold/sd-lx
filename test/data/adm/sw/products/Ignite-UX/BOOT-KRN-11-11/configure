#! /sbin/sh
########
# Product: Ignite-UX
# Fileset: BOOT-KRN-*
# configure
# @(#) $Revision: 10.12 $
########
#
# (c) Copyright Hewlett-Packard Company 2002
#
########################################################################

##########################################################################
# This removes kernel and RAMFS files from the legacy ( pre C.6.0 )
# locations.
#
# Also the PA and IA AUTO files are updated for each target OS that
# is supported by the Ignite filesets that are installed.
#

UTILS=/usr/lbin/sw/control_utils
if [ -f $UTILS ]
then
    . $UTILS
else
    echo "ERROR:   Cannot find $UTILS"
    exit 1
fi

#
# initialize variables
#
init() {

    case $FILESET in
    BOOT-KRN-11-31)
        RELEASE=B.11.31
        ;;
    BOOT-KRN-11-23)
        RELEASE=B.11.23
        ;;
    BOOT-KRN-11-11)
        RELEASE=B.11.11
        ;;
    BOOT-KRN-11-00)
        RELEASE=B.11.00
        ;;
    *)
        msg error "Fileset ($FILESET) does not match a recognized release"
        exit 1;;
    esac

    # path variables

    INST_DIR='/opt/ignite'
    BOOT_DIR="$INST_DIR/boot"
    TMP_CFG=${BOOT_DIR}/fs_cfg.old
    DEF_CFG=${BOOT_DIR}/fs_cfg.def
    BOOT_REL_DIR="$BOOT_DIR/Rel_$RELEASE"
    DATA_DIR="$INST_DIR/data"
    BOOT_COMMON='Ignite-UX.BOOT-COMMON-PA'
    PATH=${INST_DIR}/bin:$PATH

    auto_conf_PA='auto_conf_pa'
    auto_conf_IA='auto_conf_ia'


    #
    # determine the architecture we are running on and set the location of
    # native binaries to link to /opt/ignite/bin 
    ############
    case $(uname -m) in
        ia64*) bindir=/opt/ignite/binia
               ;;
            *) bindir=/opt/ignite/binpa
               ;;
    esac

    binlink=/opt/ignite/bin

    # check for INSTCMDS archives in the Release specific
    # directories.
         
    set -A CMDS $(ls ${DATA_DIR}/R*/*INSTCMDS* 2>/dev/null)

} # end init()

# Print the max array index for the variable provided as $1
maxindex()
{
     typeset i=$(set | sed -n 's/^'$1'\[\([[:digit:]]\{1,\}\)\]=.*$/\1/p' \
         | tail -1)
     echo $i
}


# check return code, exit != success if code > 0
# if command name passed in as second arg, then print
# error message too.
#
check_return()
{
    #
    # check for non-zero exit status
    #
    if (( $1 ))
    then
        if [[ -n $2 ]]
        then
            msg error "$2 exited with a non-zero ($1) exit status"
        fi
        exit $ERROR
    fi
} #end check_return()

# Remove legacy files:
#
rm_old()
{

    # Remove old install kernels and RAM FS files, old being < C.6.0; they
    # are now in Rel_*/ subdirectories:

    rm -f $BOOT_DIR/INSTALL   $BOOT_DIR/[VW]INSTALL
    rm -f $BOOT_DIR/INSTALLFS $BOOT_DIR/[VW]INSTALLFS

    # Remove old INSTCMDS and RECCMDS, old being < C.6.0; they are now in
    # /opt/ignite/data/Rel_*/:

    rm -f $DATA_DIR/INSTCMDS $DATA_DIR/RECCMDS

    # Remove old IA-only file, if present; it's now in Rel_*/
    # subdirectories:

    rm -f $BOOT_DIR/EFI_CD_image

} # end rm_old

#
# update the AUTO file in boot_lif
#
update_auto_pa()
{

    # get basename of argument
    conf=${BOOT_REL_DIR}/${auto_conf_PA}
    old_conf=${BOOT_REL_DIR}/auto_conf
    
    if [[ ! -f ${conf} ]];then
        msg error "Missing ${conf}, fileset corrupt."
        exit $ERROR
    fi

    # remove old auto_conf file without the ia/pa suffix
    [[ -f ${old_conf} ]] && rm -f ${old_conf}
    
    # extract current AUTO file
    typeset tmpflin=$(mktemp)
    typeset tmpflout=$(mktemp)
    lifcp ${BOOT_DIR}/boot_lif:AUTO ${tmpflin}

    # check return code
    check_return $? 'lifcp'

    # kernel prompt label from conf file
    typeset label="$(awk '$1 ~ /label/ {FS="=";sub("= ","=");print $2}' ${conf})"

    # check if the auto file already has this label
    typeset dup=0
    if grep -q "${label}" ${tmpflin}
    then
       dup=1
    fi

    if (( ! dup ))
    then
        # update with auto_globals
        ${INST_DIR}/bin/auto_adm -f ${tmpflin} -A ${conf} -OISL > ${tmpflout}
        
        # check return code
        check_return $? 'auto_adm'

        # remove any blank lines
        sed '/^$/d'  ${tmpflout} > ${tmpflin}

        # put the new AUTO file back in the boot_lif
        ${INST_DIR}/bin/instl_adm -F ${BOOT_DIR}/boot_lif -l AUTO -f ${tmpflin}

        # check return code
        check_return $? 'instl_adm'

    fi
    
    # remove temp files
    rm -f ${tmpflin} ${tmpflout}

} # end update_auto_pa()

#
# update to IA AUTO file
#
update_auto_ia()
{

    # conf file and target AUTO file
    conf=${BOOT_REL_DIR}/${auto_conf_IA}
    auto_fl=${BOOT_DIR}/AUTO

    if [[ ! -f ${conf} ]];then
        msg error "Missing ${conf}, fileset corrupt."
        exit $ERROR
    fi
 
    typeset tmpflout=$(mktemp) 

    # kernel prompt label from conf file
    typeset label="$(awk '$1 ~ /label/ {FS="=";sub("= ","=");print $2}' ${conf})"

    # check if the auto file already has this label
    typeset dup=0
    if grep -q "${label}" ${auto_fl}
    then
       dup=1
    fi

    if (( ! dup ))
    then
        # update with auto_globals
        ${INST_DIR}/bin/auto_adm -pia -f ${auto_fl} -A ${conf} -OISL > ${tmpflout}

        # check return code
        check_return $? 'auto_adm'

        tmpflin=$(mktemp)

        # remove any blank lines
        sed '/^$/d'  ${tmpflout} > ${tmpflin}

        # Put the new AUTO file in place
        cp ${tmpflin} ${auto_fl}
        
        rm -f ${tmpflin} ${tmpflout}

    fi
    

} # end update_auto_ia()

#
# Check for instl_adm command in various locations
#
instl_adm_check() {

    #
    # do we have the instl_adm command? look in linked and native bin
    # directories
    # if not exit
    #
    have_instl_adm=0
    if [[ -x ${binlink}/instl_adm ]];then
      instladm=${binlink}/instl_adm
      have_instl_adm=1
    elif [[ -x ${bindir}/instl_adm ]];then
      instladm=${bindir}/instl_adm
      have_instl_adm=1
    fi

    #
    # if no instl_adm exit
    if (( !  have_instl_adm ));then
      warn="instl_adm not found on the system, configuration information not modified."

      msg warning "${warn}"
      exit ${WARNING}
    fi

} # end instl_adm_check()

#
# Update content of RAMFS files
#
RAMFS_update() {

    if [ -s $TMP_CFG ]
    then
        #
        # without the '-F INSTALLFS' option all of the RAM FS images are sync'd
        #
        ${instladm} -f $TMP_CFG
        if [ "$?" != 0 ]
        then
            warn="instl_adm failed to restore the old cold-install configuration. Old configuration can be found in: $TMP_CFG"

            msg warning "${warn}"
            exit $WARNING
        else
            rm -f $TMP_CFG
        fi
    else

        #
        # The ram-fs has no configuration info in it.  So
        # fill in as much as possible given what we know about the
        # server:
        #    - if -f $CMDS, then server=`hostname`
        #    - netmask=our-netmask
        #    - gateway=our-gateway (if gateway != our IP address)

        ia_opts=""
        #
        # Find out which interfaces are configured.  Cannot rely
        # on this script running while they are up however because it
        # can be running after a reboot in single-user state via swconfig
        # (See JAGaa86074).  Source in netconf and pick one that exits
        # in lanscan output so we don't use a bogus netconf entry.
        #
        typeset -i num_ifaces=0
        typeset -i best_iface_idx=-1
        if [ -f /etc/rc.config.d/netconf ] ; then
            . /etc/rc.config.d/netconf

            typeset -i inum=0
            typeset -i maxidx=$(maxindex INTERFACE_NAME)
            while [ $inum -le $maxidx ]
            do
                iface="${INTERFACE_NAME[$inum]}"
                if [ -n "$iface" -a -n "${IP_ADDRESS[$inum]}" ] ; then
                    # if the iface does not contain a ":", or if it does
                    # it is ":0" then it is the first IP address for that
                    # iface, ignore all else for systems that multiple
                    # IP's per iface
                    if [ "${iface%%*:*}" != "" -o "${iface%%*:0}" = "" ] ; then
                        ifaces[$num_ifaces]="${iface%*:*}"
                        netmasks[$num_ifaces]="${SUBNET_MASK[$inum]}"
                        IPs[$num_ifaces]="${IP_ADDRESS[$inum]}"
                        ((num_ifaces += 1))
                    fi
                fi
                ((inum += 1))
            done

            for iface in $(lanscan -i 2>/dev/null |  awk '{print $1}') ""
            do
                inum=0
                while [ $inum  -lt $num_ifaces ]
                do
                    if [ "${ifaces[$inum]}" = "$iface" ] ; then
                        best_iface_idx=$inum
                    fi
                    ((inum += 1))
                done
                if [ $best_iface_idx -ge 0 ] ; then
                    break;
                fi
            done
        fi

        if [ $num_ifaces = 0 ] ; then
            if [ -f /tmp/install.vars ] ; then
                # Use what is available from IUX, there is a chance that
                # this maybe temporary information, but probably small.
                . /tmp/install.vars
                if [ -n "$INST_HOST_IP" ] ; then
                    ifaces[0]="$INST_LAN_DEV"
                    netmasks[0]="$INST_NETMASK"
                    IPs[0]="$INST_HOST_IP"
                    best_iface_idx=0
                    num_ifaces=1
                fi
            fi
        fi

        if [ $num_ifaces = 0 -o $best_iface_idx -eq -1 ]
        then
            note="There are no recognized network interfaces configured at this time. The default networking parameters cannot be configured for this Ignite-UX server.  Please use the instl_adm(1M) command to set the server's IP address (-t), netmask (-m) and default gateway
(-g) as desired."

            msg note "${note}"
            exit $SUCCESS
        fi

        if [[ -n ${CMDS[*]} &&  -n "${IPs[$best_iface_idx]}" ]]
        then
            ia_opts="-t ${IPs[$best_iface_idx]} $ia_opts"
        fi

        if [ -n "${netmasks[$best_iface_idx]}" ]
        then
            ia_opts="-m ${netmasks[$best_iface_idx]} $ia_opts"
        fi

        #
        # Determine the default route.  Again, we cannot rely on
        # the current settings from netstat since we could be running
        # in single user state, so read it out of the netconf that was
        # sourced in.
        #
        # To fix FSDdt23231 we must look at all our IPs to make sure
        # that the default route does not match any of them.  Also
        # only consider routes that are for the interface we picked
        # for the IP and netmask.
        #
        route=""
        inum=0
        maxidx=$(maxindex ROUTE_DESTINATION)
        while [ $inum -le $maxidx ]
        do
            if [ "${ROUTE_DESTINATION[$inum]}" = "default" -a \
                -n "${ROUTE_GATEWAY[$inum]}" ] ; then
                is_local=false
                for ip in ${IPs[*]}
                do
                    if [ $ip = "${ROUTE_GATEWAY[$inum]}" ]
                    then
                        is_local=true
                    fi
                done
                if [ $is_local = false ]
                then
                    route="${ROUTE_GATEWAY[$inum]}"
                    break; # Keep the first non-local route
                fi
            fi
            ((inum += 1))
        done

        if [ -z "$route" -a -n "$INST_DEFAULT_ROUTE" ]
        then
            # Use the INST_DEFAULT_ROUTE that may have been sourced in
            # from /tmp/install.vars
            route="$INST_DEFAULT_ROUTE"
        fi

        if [ -n "$route" ]
        then
            ia_opts="-g $route $ia_opts"
        fi

        if [ -n "$ia_opts" ]
        then
            #
            # without the '-F INSTALLFS' option all of the RAM FS images
            # are sync'd
            #
            ${instladm}  $ia_opts && \
            ${instladm}  -d > $DEF_CFG && \
            IPD_addfile $DEF_CFG $BOOT_COMMON
            if [ $num_ifaces -gt 1 ]
            then
                note="Because this system has multiple LAN interfaces configured, you should verify that the default Ignite-UX server IP address, network route, and netmask were chosen correctly by running the \"/opt/ignite/bin/instl_adm -d\" command."
                msg note "${note}"
            fi
        fi
    fi

} # end RAMFS_update()

#############################################
#           Main
#############################################

#
# initialize variables
#
init

#
# Make sure we have instl_adm available
#
instl_adm_check

#
# remove legacy Kernel/RAMFS/*CMDS files
#
rm_old

#
# update AUTO files
#
update_auto_pa
[[ ${RELEASE} > "B.11.22" ]] && update_auto_ia

#
# Update RAM filesystem
#
RAMFS_update

exit $SUCCESS
