# debpsf.py - Routines for deb-to-PSF file translation.
#
#   Copyright (C) 2007 Jim Lowe
#   Copyright (C) 2023-2024 Paul Weber conversion to Python
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note
import ctypes
import os
import sys
from sd.swsuplib.misc.swlib import swlib_open_memfd, swlib_pipe_pump, swlib_ascii_text_fd_to_buf
from sd.swsuplib.strob import strob_strncat, strob_strncpy, strob_strcat

from sd.swsuplib.uinfile.uinfile import uinc, uinfile_get_type, uinfile_opendup
from sd.swsuplib.uxfio import uxfio_close, uxfio_write
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.topsf import topsf_add_excludes
from sd.swsuplib.strob import strob_open, strob_close, strob_sprintf, STROB
from sd.swsuplib.taru.ahs import ahs_get_perms, ahsstaticgettarusername, ahsstaticgettargroupname
from sd.swsuplib.taru.xformat import xformat_open, xformat_close, xformat_u_open_file, xformat_open_archive_by_fd, xformat_u_close_file, xformat_get_next_dirent
from sd.const import LegacyConstants

lc = LegacyConstants()

class DebPSFConstants(object):
    DEBPSF_PSF_DATA_DIR = "data"
    DEBPSF_PSF_CONTROL_DIR = "control"
    DEBPSF_DATA_PREFIX = "./"
    DEBPSF_CONTROL_PREFIX = "./"
    DEBPSF_FILE_CONTROL = "control"
    DEBPSF_FILE_MD5SUM = "md5sums"
    DEBPSF_FILE_POSTINSTALL = "postinst"
    DEBPSF_FILE_PREINSTALL = "preinst"
    DEBPSF_FILE_PREREMOVE = "prerm"
    DEBPSF_FILE_POSTREMOVE = "postrm"
    DEBPSF_FILE_CONFFILES = "conffiles"
    DEBPSF_FILE_SHLIBS = "shlibs"
    DEBPSF_ATTR_Package = "Package"
    DEBPSF_ATTR_Version = "Version"
    DEBPSF_ATTR_Architecture = "Architecture"
    DEBPSF_ATTR_Maintainer = "Maintainer"
    DEBPSF_ATTR_Description = "Description"

dpc = DebPSFConstants
class DebControlMember:
    def __init__(self):
        self.name = None
        self.hdr= None
        self.data = None
        self.next = None

class DebAttributes:
    def __init__(self):
        self.Package = None
        self.Version = None
        self.Version_epoch = None
        self.Version_revision = None
        self.Version_release = None
        self.Architecture = None
        self.Maintainer = None
        self.Description = None

class DebPSF:
    def __init__(self):
        self.head = None
        self.topsf = None
        self.header_fd = -1
        self.control_fd = -1
        self.data_fd = -1
        self.source_data_fd = -1
        self.da = None


#def normalize_leading_directory(b, name):
#    strob_strcpy(b, ".")
#    swlib_unix_dircat(b, name.arg_value)

def normalize_leading_directory(b, name):
    if not b:
        b = "."
    b = os.path.join(b, name)
    return b
def squash_illegal_tag_chars(s):
    s = s.replace('.', '_')
    s = s.replace(',', '_')
    s = s.replace(':', '_')
    return s


def parse_version(da):
    if da.Version is None:
        print("Version attribute missing", file=sys.stderr)
        return -1
    s0 = da.Version.str_
    s = s0
    e = s.find(':')
    if e != -1:
        # epoch
        da.Version_epoch = strob_open(32)
        da.Version_epoch.str_ = s[:e]
        s = s[e + 1:]
    r = s.rfind('-')
    if r != -1:
        da.Version_release = strob_open(32)
        da.Version_release.str_ = s[r + 1:]
        s = s[:r]
    da.Version_revision = strob_open(32)
    da.Version_revision.str_ = s
    return 0


def x_strncasecmp(line, s):
    libc = ctypes.CDLL("libc.so.6")
    return libc.strncasecmp(line, s, len(s))


def process_line(attr, line, do_append):
    if do_append == 0:
        s = line.find(':')
        if s == -1:
            return -1
        attr = strob_open(32)
        s += 1
        while line[s].isspace():
            s += 1

        p = line
        w = None
        while p != '\0':
            if p and p.isspace():
                w = p
            elif p:
                w = None
            p += 1

        strob_strncpy(attr, line[s:], len(s) if w else len(s) - 1)
        return 0
    else:
        s = line
        while s.isspace():
            s += 1
        p = s
        w = None
        while p != '\0':
            if p and p.isspace():
                w = p
            elif p:
                w = None
            p += 1

        if s == '\0':
            return -1
        if s == '.' and s + 1 == w:
            strob_sprintf(attr, 1, "\n")
        else:
            strob_strcat(attr, "\n")
            strob_strncat(attr, s, len(s) if w else len(s) - 1)
        return 0
   

def parse_control_file(da, filebuf):
    #tmp = STROB()
    last_attribute = None
    lines = filebuf.split("\n")
    for line in lines:
        if line and line[0].isalpha():
            if x_strncasecmp(line, dpc.DEBPSF_ATTR_Package + ":") == 0:
                process_line(da.Package, line, 0)
                last_attribute = da.Package
            elif x_strncasecmp(line, dpc.DEBPSF_ATTR_Version + ":") == 0:
                process_line(da.Version, line, 0)
                last_attribute = da.Version
            elif x_strncasecmp(line, dpc.DEBPSF_ATTR_Architecture + ":") == 0:
                process_line(da.Architecture, line, 0)
                last_attribute = da.Architecture
            elif x_strncasecmp(line, dpc.DEBPSF_ATTR_Maintainer + ":") == 0:
                process_line(da.Maintainer, line, 0)
                last_attribute = da.Maintainer
            elif x_strncasecmp(line, dpc.DEBPSF_ATTR_Description + ":") == 0:
                process_line(da.Description, line, 0)
                last_attribute = da.Description
            else:
                print(f"debpsf: attribute not supported at this time: {line}", file=sys.stderr)
        elif line.isspace():
            if last_attribute is None:
                return -1
            process_line(last_attribute, line, 1)
        else:
            pass


def write_to_buf(xformat, name, buf):
    ufd = xformat_u_open_file(xformat, name)
    if ufd < 0:
        return -1
    if buf:
        ret = swlib_ascii_text_fd_to_buf(buf, ufd)
    else:
        tmp = STROB()
        ret = swlib_ascii_text_fd_to_buf(tmp, ufd)
        del tmp
    xformat_u_close_file(xformat, ufd)
    return ret
def debpsf_create():
    debpsf = DebPSF()
    debpsf.head = None
    debpsf.topsf = None
    debpsf.header_fd = -1
    debpsf.control_fd = -1
    debpsf.data_fd = -1
    return debpsf


def debpsf_open(dp, v_topsf):
    topsf = v_topsf
    dp.topsfM = topsf

    if uinfile_get_type(topsf.format_desc_) !=   uinc.DEB_FILEFORMAT:
        return -1
    fd = swlib_open_memfd()
    if fd < 0:
        return -2

    # store the control.tar.gz archive member in fd 	
    ret = swlib_pipe_pump(fd, topsf.fd_)
    if ret < 0:
        return -3

    dp.control_fdM = fd
#    uxfio_lseek(fd, 0, os.SEEK_SET)

    # Now go after the data.tar.gz archive member 
    ret = uinfile_opendup(topsf.format_desc_.deb_file_fd_, 0, topsf.format_desc_,   uinc.UINFILE_DETECT_DEB_DATA)
    if ret < 0:
        return -4

    # the data is now in data_fdM 
    dp.data_fdM = swlib_open_memfd()
    dp.source_data_fdM = ret

    return 0

def debpsf_deb_attributes_init(da):
    da.Package = None
    da.Version = None
    da.Version_epoch = None
    da.Version_revision = None  # upstream version
    da.Version_release = None  # Debian revision
    da.Architecture = None
    da.Maintainer = None
    da.Description = None



def debpsf_close(dp):
    swlib_pipe_pump(dp.data_fdM, dp.source_data_fdM)
    uxfio_close(dp.control_fdM)
    uxfio_close(dp.data_fdM)
    dp.control_fdM = -1
    dp.data_fdM = -1
    return 0


def debpsf_write_psf(v_topsf, ofd):
    psf = strob_open(32)
    ret = debpsf_write_psf_buf(v_topsf, psf)
    if ret < 0:
        return ret
    ret = uxfio_write(ofd, psf.str_, len(str(psf.str_)))
    strob_close(psf)
    return ret


def debpsf_write_psf_buf(v_topsf, psf):
    st = os.stat(v_topsf)
    tmp = strob_open(12)
    topsf = v_topsf
    dp = topsf.debpsf
    dp.da = DebAttributes()
    topsf = v_topsf
    debpsf_deb_attributes_init(dp.da)
    control_buf = ""
    md5sums_buf = ""
    postinstall_buf = ""
    preinstall_buf = ""
    preremove_buf = ""
    postremove_buf = ""
    conffiles_buf = ""
    shlib_buf = ""
    control_fd = dp.control_fdM
    os.lseek(control_fd, 0, os.SEEK_SET)
    control_xformat = xformat_open(-1, -1, 'arf_ustar')
    ret = xformat_open_archive_by_fd(control_xformat, control_fd,   uinc.UINFILE_DETECT_OTARFORCE|  uinc.UINFILE_DETECT_DEB_CONTROL, 0)
    if ret < 0:
        return -2
    while (name:=xformat_get_next_dirent(control_xformat, st)) is not None:
        if name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_CONTROL:
            write_to_buf(control_xformat, name, control_buf)
            parse_control_file(dp.da, control_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_MD5SUM:
            write_to_buf(control_xformat, name, md5sums_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_POSTINSTALL:
            write_to_buf(control_xformat, name, postinstall_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_PREINSTALL:
            write_to_buf(control_xformat, name, preinstall_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_PREREMOVE:
            write_to_buf(control_xformat, name, preremove_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_POSTREMOVE:
            write_to_buf(control_xformat, name, postremove_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_CONFFILES:
            write_to_buf(control_xformat, name, conffiles_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX + dpc.DEBPSF_FILE_SHLIBS:
            write_to_buf(control_xformat, name, shlib_buf)
        elif name == dpc.DEBPSF_CONTROL_PREFIX:
            pass
        else:
            write_to_buf(control_xformat, name, None)
            print(f"unrecognized file in control.tar.gz: {name}", file=sys.stderr)
    xformat_close(control_xformat)
    parse_version(dp.da)
    if control_buf:
        psf.str_ += f"\n{swc.SW_A_control_file} {dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_CONTROL} control\n"
    if preinstall_buf:
        psf.str_ += f"{swc.SW_A_preinstall} {dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_PREINSTALL}\n"
    if postinstall_buf:
        psf.str_ += f"{swc.SW_A_postinstall} {dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_POSTINSTALL}\n"
    if preremove_buf:
        psf.str_ += f"{swc.SW_A_preremove} {dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_PREREMOVE}\n"
    if postremove_buf:
        psf.str_ += f"{swc.SW_A_postremove} {dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_POSTREMOVE}\n"
    if dp.da.Version_release:
        psf.str_ += "\n"
        psf.str_ += "vendor\n"
        psf.str_ += "the_term_vendor_is_misleading True\n"
        psf.str_ += f"tag \"{dp.da.Version_release.str_}\"\n"
        psf.str_ += "title Debian\n"
        psf.str_ += "url \"https://www.debian.org\"\n"
    psf.str_ += "\n"
    psf.str_ += f"{swc.SW_A_tag} \"{dp.da.Package.str_}-{dp.da.Version_revision.str_}{dp.da.Version_release.str_}\"\n"
    if md5sums_buf:
        psf.str_ += f"md5sums <{dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_MD5SUM}\n"
    if conffiles_buf:
        psf.str_ += f"conffiles <{dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_CONFFILES}\n"
    if shlib_buf:
        psf.str_ += f"shlibs <{dpc.DEBPSF_PSF_CONTROL_DIR}/{dpc.DEBPSF_FILE_SHLIBS}\n"
    if dp.da.Version_release:
        psf.str_ += "\n"
        psf.str_ += "vendor\n"
        psf.str_ += f"the_term_vendor_is_misleading True\n"
        psf.str_ += f"tag \"{dp.da.Version_release.str_}\"\n"
        psf.str_ += "title Debian\n"
        psf.str_ += "url \"https://www.debian.org\"\n"
    psf.str_ += "\n"
    psf.str_ += f"{swc.SW_A_product}\n"
    psf.str_ += f"{swc.SW_A_tag} {dp.da.Package.str_}\n"
    psf.str_ += f"{swc.SW_A_revision} {dp.da.Version_revision.str_}\n"
    psf.str_ += f"{swc.SW_A_control_directory} \"\"\n"
    if dp.da.Version_release:
        psf.str_ += f"vendor_tag \"{dp.da.Version_release.str_}\"\n"
    psf.str_ += "\n"
    psf.str_ += f"{swc.SW_A_fileset}\n"
    psf.str_ += f"{swc.SW_A_tag} bin\n"
    psf.str_ += f"{swc.SW_A_control_directory} \"\"\n"
    psf.str_ += f"{swc.SW_A_directory} .\n"
    psf.str_ += "\n"
    psf.str_ += "# Note Using file -t d -o XX -g XX ./\n"
    psf.str_ += "# instead of file -t d -o XX -g XX /\n"
    psf.str_ += "# will be a broken package, this is a bug in swinstall\n"
    psf.str_ += f" {swc.SW_A_file} -t d -o {ahsstaticgettarusername(psf)} -g {ahsstaticgettargroupname(psf)} -m {ahs_get_perms(psf)} /\n"
    psf.str_ += f"{swc.SW_A_file} *\n"
    if conffiles_buf:
        conffile_lines = conffiles_buf.split("\n")
        for line in conffile_lines:
            line = line.strip()
            if line:
                line = squash_illegal_tag_chars(line)
                normalize_leading_directory(tmp, line)
                psf.str_ += f"{swc.SW_A_file}\n"
                psf.str_ += f"{swc.SW_A_source} {tmp.str_}\n"
                psf.str_ += f"{swc.SW_A_is_volatile} true\n"
                psf.str_ += "\n"
    psf.str_ += f"{swc.SW_A_exclude} control\n"
    psf.str_ += f"{swc.SW_A_exclude} control/*\n"
    psf.str_ += f"{swc.SW_A_exclude} PSF\n"
    topsf_add_excludes(topsf, psf)
    return ret


