

# Corequisites 
# An object requires another to operate correctly, but does not imply any load order. 
# The specification in a software object that another software object must be 
# installed in conjunction with the installation of the first and configured in 
# conjunction with the configuration of the first.
