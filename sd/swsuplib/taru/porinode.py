# porinode.py - make a 32-bit virtual inode using two 16-bit fields.
#
#   Copyright (C) 2000  James H. Lowe, Jr.
#   Copyright (C) 2023-2024 Paul Weber, conversion to Python
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.



import sys

from sd.swsuplib.taru.hllist import hllist_open, hllist_close, hllist_add_vrecord, hllist_find_file_entry

PORINODE_MAX = 0xFFFF - 1

class PORINODE:
    def __init__(self):
        self.current_inode = 0
        self.current_device = 0
        self.max_inode = 0
        self.max_device = 0
        self.hllist = None

def porinode_i_get_next_vinode(porinode, p_dev, p_ino):
    if porinode.current_inode >= porinode.max_inode - 1:
        if porinode.current_device >= porinode.max_device:
            sys.stderr.write("porinode: virtual inode space overflow.\n")
        else:
            porinode.current_device += 1
            porinode.current_inode = 0
    p_ino[0] = porinode.current_inode + 1
    p_dev[0] = porinode.current_device

def porinode_open():
    porinode = PORINODE()
    porinode.hllist = hllist_open()
    porinode.current_inode = 1
    porinode.current_device = 1
    porinode.max_inode = PORINODE_MAX
    porinode.max_device = PORINODE_MAX
    return porinode

def porinode_close(porinode):
    hllist_close(porinode.hllist)
    del porinode

def porinode_add_vrecord(porinode, path, dev, ino, v_dev, v_ino):
    hllist_add_vrecord(porinode.hllist, path, dev, ino, v_dev, v_ino)

def porinode_make_next_inode(porinode, nlinks, dev, ino, p_dev, p_ino):
    link_record_buf = None
    nfound = 0
    if nlinks < 2:
        porinode_i_get_next_vinode(porinode, p_dev, p_ino)
    else:
        link_record_buf = hllist_find_file_entry(porinode.hllist, dev, ino, 1, nfound)
        if nfound == 0:
            porinode_i_get_next_vinode(porinode, p_dev, p_ino)
            porinode_add_vrecord(porinode, "", dev, ino, p_dev[0], p_ino[0])
        else:
            p_dev[0] = link_record_buf.v_dev_
            p_ino[0] = link_record_buf.v_ino_
