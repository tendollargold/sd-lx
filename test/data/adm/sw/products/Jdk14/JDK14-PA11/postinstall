#!/sbin/sh
################################################################################
#
# File:         postinstall
# Description:  Generic Java2 SD post-installation control script.
# Purpose:	Enforces directory permissions to those specified in the PSF.
#		Enforces ancestor removal.
#
# (c) Copyright 1999-2006 Hewlett-Packard Development Company, L.P.
# No part of this artifact may be reproduced without prior written permission
# from Hewlett-Packard Company.
#
################################################################################

# set environment variable DBG_SH to "set -x" to get full-script trace.
# $DBG_SH
# export DBG_SH="set -x"
# set -x

# ------------------------------------------------------------------------------
fatal_error() {
   $DBG_SH
   echo "ERROR:   $1"
   echo "         Exiting from postinstall script."
   exit 1
   }

# ------------------------------------------------------------------------------
# include standard control script functions.
UTILS="/usr/lbin/sw/control_utils"
if [ ! -f $UTILS ]; then
   fatal_error "Cannot find $UTILS"
   fi
. $UTILS
export PATH=$PATH:/usr/bin

# ------------------------------------------------------------------------------
# set directory permissions to those specified in the PSF.
if [ -n "$SW_SOFTWARE_SPEC" ]; then
   swlist -l file -a type -a mode -a uid -a gid $SW_SOFTWARE_SPEC 2>/dev/null |
   while read dir type mode uid gid; do
      if [ "$type" = "d" -a -d "$dir" -a "$dir" != "/" -a "$dir" != "/opt" ]; then
         if [ ! "$uid" -o ! "$gid" ]; then
            chmog $mode bin bin $dir
         else
            chmog $mode $uid $gid $dir
            fi
         fi
      done
   fi

# ------------------------------------------------------------------------------
# enforce ancestor removal for all (non self) ancestors
if [ -n "$SW_SOFTWARE_SPEC" ]; then
   # get the list of this fileset's ancestors
   swlist -l fileset -a ancestor $SW_SOFTWARE_SPEC 2>/dev/null |
   grep -v '^#' | grep -v '^$' |
   while read tag ancestors; do
      for ancestor in $ancestors; do
	 # skip removing ourselves
	 if [ -z "$tag" -o "$tag" = "$ancestor" ]; then
	    continue
	    fi
	 # skip removing something that does not exist
	 if [ -z "$(get_sw_spec $ancestor)" ]; then
	    continue
	    fi
	 # get this fileset's containing product
	 ancestorProduct=$(echo $ancestor | sed 's/\..*//')
	 # remove the owning product and all attached filesets from the IPD
	 swmodify -u $ancestorProduct.\* $ancestorProduct 1>/dev/null 2>&1
	 done
      done
   fi

# ------------------------------------------------------------------------------
exit 0
