#  strob.py --  unlimited length Null terminated string object
#
# Copyright (C) 1995,1996,1997,1998,2000,2001,2005,2014  James H. Lowe, Jr. <jhlowe@acm.org>
# Copyright (C) 2023-2024 Paul Weber Conversion to Python
#
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# STROB: object and methods for unlimited length string object

# Change Log
# ------------------------------------------------------------------
# 20240106 Paul Weber - strob.py updated, no errors, just warnings

import sys

from ctypes import memmove,  memset
from sd.swsuplib.vsnprintf import vsnprintf

from sd.swsuplib.misc.cstrings import *

STROB_INITLENGTH = 32
STROB_LENGTHINCR = 156
STROB_DO_APPEND = 1
STROB_NO_APPEND = 0


class STROB:
    def __init__(self):
        self.str_ = None
        self.tok_ = None  # Used by strob_strtok
        self.length_ = 0  # current length, not including the last NULL
        self.extra_ = 0  # the lookahead allocation amount
        self.reserve_ = 0  # the total length of reserved memory
        self.in_use_ = False  # Only used if S_MEM_CACHE is defined
        self.fill_char = '\0'  # character used to fill extra memory, default: '\0'

def sb_close(strb):
    del strb.str_
    return 0

def sb_open(initial_size):
    strb = STROB()
    if strb is None:
        sys.stderr.write("strob_open: out of memory.\n")
        sys.exit(22)
    if initial_size <= 0:
        initial_size = STROB_INITLENGTH
    strb.extra_ = STROB_LENGTHINCR
    strb.length_ = 0
    strb.str_ = ""
    strb.in_use_ = 1
    strb.fill_char = '\0'
    # strob_reopen(strb, initial_size + 1)
    return strb

# ===============================================================

def strob_reopen_if_fill_with(strb, reqd_length, fill_char):
    if int(reqd_length) > strb.reserve_:
        return strob_reopen_fill_with(strb, reqd_length + strb.extra_, fill_char)
    return strb

def strob_reopen_if(strb, reqd_length):
    if int(reqd_length) > strb.reserve_:
        return strob_reopen(strb, reqd_length + strb.extra_)
    return strb

def strob_set_up(strb, n):
    if strob_reopen_if(strb, n + 1) is None:
        return None

    if n > strb.length_:
        strb.length_ = n
        strb.str_[n] = '\0'
    return strb

def strob_close(strb):
    return strb.sb_close()

def strob_open(initial_size):
    return sb_open(initial_size)

def strob_release(strb):
    x = str(strb.str_)
    return x

def strob_set_reserve(strb, res):
    strb.extra_ = res

def strob_reopen_fill_with(strb, new_length, fill_char):
    if new_length <= 1:
        new_length = 2
    if strb.str_ is None:
        strb.str_ = [0]

    # tmpstr = byte(int(SW_REALLOC(strb.str_, int((new_length)), strb.reserve_)))
    tmpstr = bytearray(int(len(strb.str_) + int(new_length) + strb.reserve_))
    if not tmpstr:
        sys.stderr.write("strob_reopen(loc=1): out of memory.\n")
        sys.exit(22)

    strb.str_ = tmpstr
    strb.reserve_ = new_length
    if strb.reserve_ > strb.length_:
        strb.str_ = strb.str_[:strb.length_] + [fill_char] * (strb.reserve_ - strb.length_)
    # strb.str_[new_length - 1] = '\0'
    return strb

def strob_reopen(strb, new_length):
    return strb.strob_reopen_fill_with(new_length, ord('\0'))

def strob_get_str(strb):
    return str(strb.str_)

def strob_get_reserve(strb):
    return strb.reserve_

def strob_get_length(strb):
    return strb.length_

def strob_trunc(strb):
    return strob_reopen(strb, STROB_INITLENGTH + 1)

def strob_set_length(strb, length):
    strob_set_memlength(strb, length + 1)
    strb.str_[length] = '\0'

def strob_strcpy_at_offset(strb, offset, str1):
    # strb_ret = None
    strb_ret = strob_reopen_if(strb, len(str1) + offset + 1)
    if strb_ret is None:
        return str(None)

    strb.length_ = offset + len(str)
    memmove(strb.str_ + offset, str, len(str) + 1)
    return str((strb.str_ + offset))

def strob_chr_index(strb, index, ch):
    strb.strob_reopen_if_fill_with(index + 2, strb.fill_char)

    if index >= strb.length_:
        memset(strb.str_ + strb.length_, strb.fill_char, index - strb.length_ + 1)
        strb.str_[index + 1] = '\0'

    if index > strb.length_ - 1:
        strb.length_ = index + 1
        strb.str_[strb.length_] = '\0'
    strb.str_[index] = bytearray(int(ch))

def strob_get_char(strb, index):
    if index < 0:
        return -1
    if index >= strb.length_:
        return -1
    return int(strb.str_[index])

def strob_strcat_at_offset(strb, offset, str1):
    # strb_ret = None
    strb_ret = strb.strob_reopen_if(len(str1) + offset + 1 + len(str((strb.str_ + offset))))
    if strb_ret is None:
        return str(None)

    strb.length_ = offset + len(str1)
    memmove(strb.str_ + offset + len(str((strb.str_ + offset))), str, len(str1) + 1)
    return str((strb.str_ + offset))

# --- NULL Terminated String Interface ------------------------

def strob_chomp(strb):
    s = strob_str(strb)
    if p := strchr(s, '\n'):
        p[0] = '\0'
    if p := strchr(s, '\r'):
        p[0] = '\0'
    return s

def strob_strncat(strb, str1, length):
    ilen = int(length)
    strb_ret = strb.strob_reopen_if(len(str1(strb.str_)) + ilen + 1)
    if strb_ret is None:
        return str1(None)
    cret = strncat(str1(strb.str_), str1, ilen)
    strb.length_ = len(str1(strb.str_))
    [strb.length_] = '\0'
    return cret

def strob_strcat(strb, str1):
    strb_ret = strob_reopen_if(strb, len(str1(strb.str_)) + len(str1) + 1)
    if strb_ret is None:
        return str1(None)
    strb.length_ = len(str1(strb.str_)) + len(str1)
    return strcat(str1(strb.str_), str1)

def strob_strcpy(strb, str1):
    return strob_strcpy_at_offset(strb, 0, str1)

def strob_charcat(strb, ch):
    c = chr(ch)
    if ch:
        return strob_strcat(strb, c)
    else:
        s = strob_str(strb)
        s = s[:-1]
        return None, s

def strob_strncpy(strb, str1, n):
    strob_reopen_if(strb, n + 1)
    s = strncpy(str1(strb.str_), str1, n)
    strb.str_[n] = '\0'
    strb.length_ = len(str1(strb.str_))
    return s

def strob_strcmp(strb, str1):
    return strcmp(str1(strb.str_), str1)

def strob_strlen(strb):
    return len(str(strb.str_))

def strob_strchar(strb, index):
    strob_reopen_if(strb, index + 1)
    return str((strb.str_ + index))

def strob_strrchr(strb, c):
    return strrchr(str(strb.str_), c)

def strob_strstr(strb, str1):
    return strstr(str(strb.str_), str1)

def strob_chr(strb, ch):
    strob_chr_index(strb, len(str(strb.str_)), ch)

def strob_append_hidden_null(strb):
    strb.strob_set_up(strb.length_ + 1)
    memcpy(strb.str_ + strb.length_, "\0", 1)

def strob_set_fill_char(strb, ch):
    strb.fill_char = ch

def strob_get_fill_char(strb):
    return strb.fill_char

# --- Unrestricted binary string interface ---- NOT WELL TESTED ----------

def strob_memcpy(strb, ct, n):
    if not strb.strob_set_up(int(n)):
        return None
    return memcpy(strb.str_, ct, n)

def strob_memcpy_at(strb, offset, ct, n):
    if not strb.strob_set_up(int(n) + int(offset)):
        return None
    return memcpy(strb.str_ + offset, ct, n)

def strob_memmove(strb, ct, n):
    return strob_memmove_to(strb, 0, ct, n)

def strob_memmove_to(strb, dst_offset, ct, n):
    if not strob_set_up(strb, int((dst_offset + n))):
        return None
    return memmove(strb.str_ + dst_offset, ct, n)

def strob_memcat(strb, ct, n):
    return strb.strob_memmove_to(strb.length_, ct, n)

def strob_memset(strb, c, n):
    if not strb.strob_set_up(int(n)):
        return None
    return memset(strb.str_, c, n)

def strob_set_memlength(strb, length):
    strob_reopen_if(strb, length)
    strb.length_ = length

# -------------------- Depricated Names ------------------------------------

def strob_setlen(strb, length):
    strob_set_length(strb, length)
    return length

def strob_length(strb):
    return strob_get_length(strb)

def strob_str(strb):
    return strob_get_str(strb)

def strob_catstr(strb, str1):
    return strb.strob_strcat(str1)





# -------------------------------------------------------------

def strob_cpy(s, ct):
    strob_strcpy_at_offset(s, 0, str(ct.str_))
    return s


def strob_cat(s, ct):
    strob_catstr(s, str(ct.str_))
    return s


def strob_cmp(cs, ct):
    return strob_strcmp(cs, str(ct.str_))




# Function to tokenize a string in STROB using given delimiter and strstr function


def strob_strtok(buf, s, delim):
    if s:
        if s != buf.str_:
            strob_strcpy(buf, s)
        buf.tok_ = strob_str(buf)
    start = buf.tok_
    if not len(start):
        return None
    p = "\0"
    while True:
        if p == start.index(start,0):
            start += 1
        p = strpbrk(start, delim)
        if not p or p != start.index(start,0):
            break
        else:
            start += 1
    if p:

        end = p + 1 + len(chr(p)) + 1
        if len(start):
            retval = start
        else:
            retval = None
    else:
        # p = start + len(start)
        p = start.index(start,0) + len(start)
        end = p
        retval = start
    buf.tok_ = end
    if p < end:
        m = p + 1
        while m and delim.find(m) != -1:
            m += 1
        buf.tok_ = m
    if not len(retval):
        retval = None
    return retval

def strob_strstrtok(buf, s, delim):
    dlen = len(delim)
    if s:
        strob_strcpy(buf, s)
        buf.tok_ = strob_str(buf)
    start = buf.tok_
    if not len(start):
        return None
    p = None
    while True:
        if p == start:
            start += dlen
        p = strstr(start, delim)
        if not p or p != start:
            break
    if p:
        p[0] = '\0'
        end = p + dlen + len(p + dlen)
        if len(start):
            retval = start
        else:
            retval = None
    else:
        # p = start + len(start)
        end = p
        retval = start
    buf.tok_ = end
    if p < end:
        m = p
        m += dlen
        while m and strstr(m, delim) == m:
            m += dlen
        buf.tok_ = m
    if not len(retval):
        retval = None
    return retval


# -------------------- Deprecated Names ------------------------------------


def strob_vsprintf_at(sb, at_offset, format_, ap):
    added_amount = 0
    up_incr = 128
    oldend = []

    if at_offset > strob_get_reserve(sb):
        strob_set_memlength(sb, at_offset + up_incr)

    while True:
        if oldend:
            oldend[0] = '\0'
        strob_set_memlength(sb, strob_get_reserve(sb) + added_amount)
        start = strob_str(sb) + at_offset
        length = strob_get_reserve(sb) - at_offset
        oldend = start
        added_amount += up_incr
        aq = ap
        ret = vsnprintf(start, length, format_, aq)
        if 0 <= ret < length:
            break
    return ret


def strob_vsprintf(sb, do_append, format_, ap):
    added_amount = 0
    up_incr = 128
    oldend = []

    while True:
        if oldend:
            oldend[0] = '\0'
        strob_set_memlength(sb, strob_get_reserve(sb) + added_amount)
        if do_append:
            start = len(strob_str(sb)) + len(strob_str(sb))
            length = strob_get_reserve(sb) - len(strob_str(sb))
        else:
            start = strob_str(sb)
            length = strob_get_reserve(sb)
        oldend = start
        added_amount += up_incr
        aq = ap
        ret = vsnprintf(start, length, format_, aq)
        if 0 <= ret < length:
            break
    return ret


def strob_snprintf(sb, do_append, length, format_, *args):
    if length <= 0:
        return length

    if do_append:
        old_len = strob_strlen(sb)
    else:
        old_len = 0

    ap = args
    ret1 = strob_vsprintf(sb, do_append, format_, ap)

    strob_chr_index(sb, old_len + length - 1, 0)
    return ret1


def strob_sprintf(sb, do_append, format_, *args):
    ap = args
    ret1 = strob_vsprintf(sb, do_append, format_, ap)
    return ret1


def strob_sprintf_at(sb, at_offset, format_, *args):
    ap = args
    ret1 = strob_vsprintf_at(sb, at_offset, format_, ap)
    return ret1



