# swicol.py -- Write script and data into a shell's stdin.
#
#  Copyright (C) 2004  James H. Lowe, Jr.  <jhlowe@acm.org>
#  Copyright (C) 2023 Paul Weber convert to Python
#
#  COPYING TERMS AND CONDITIONS:
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
import os
import sys
import time

from sd.debug import E_DEBUG, E_DEBUG2
from sd.swsuplib.atomicio import atomicio
from sd.swsuplib.misc.strar import strar_num_elements, strar_add
from sd.swsuplib.misc.swevents import ec
from sd.swsuplib.misc.sw import swc

from sd.swsuplib.swutillib import swutil_doif_writef
from sd.swsuplib.strob import strob_sprintf, strob_strcpy, strob_str, strob_strlen
from sd.swsuplib.misc.swgp import swgpReadLine
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlibc, fr, swlib_atoi, swlib_tee_to_file, swlib_is_ascii_noaccept, swlib_append_synct_eof, swlib_umaxtostr

swlib_doif_writef = swutil_doif_writef


class IcolConstants:
    SWICOL_TRAILER = "CPIO_INBAND_EOA_FILENAME"
    SWICOL_ALARM_EVENT = "Intentional abort"
    SWICOL_STATUS_NOMINAL_ERROR = 129
    SWICOL_ABORT_STEP_1 = 1
    SWICOL_ABORT_STEP_2 = 2
    SWICOL_DEBUG_TASK_SCRIPT_PREFIX = "/tmp/swbis_task_"
    SWICOL_STOP_STRING = "305:"
    SWICOL_START_STRING = "304:"
    SWICOL_CTS_STRING = "318:"
    SWICOL_TL_1 = 1
    SWICOL_TL_2 = 2
    SWICOL_TL_3 = 3
    SWICOL_TL_4 = 4
    SWICOL_TL_5 = 5
    SWICOL_TL_6 = 6
    SWICOL_TL_7 = 7
    SWICOL_TL_8 = 8
    SWICOL_TL_9 = 9
    SWICOL_TL_10 = 10
    SWICOL_TL_11 = 11
    SWICOL_TL_12 = 12
    SWICOL_TL_13 = 13
    SWICOL_TL_14 = 14
    SWICOL_TL_15 = 15
    SWICOL_TL_16 = 16
    SWICOL_TL_17 = 17
    SWICOL_TL_18 = 18
    SWICOL_TL_19 = 19
    SWICOL_TL_20 = 20
    SWICOL_TL_21 = 21
    SWICOL_TL_22 = 22
    SWICOL_TL_23 = 23
    SWICOL_TL_24 = 24
    SWICOL_TL_25 = 25
    SWICOL_TL_26 = 26
    SWICOL_TL_27 = 27
    SWICOL_TL_28 = 28
    SWICOL_TL_29 = 29
    SWICOL_TL_30 = 30
    SWICOL_TL_31 = 31
    SWICOL_TL_32 = 32
    SWICOL_TL_33 = 33
    SWICOL_TL_34 = 34
    SWICOL_TL_40 = 40
    SWICOL_TL_50 = 50
    SWICOL_TL_60 = 60
    SWICOL_TL_80 = 80
    SWICOL_TL_100 = 100
    SWICOL_TL_500 = 500
    SWICOL_TL_1000 = 1000
    SWICOL_TL_3600 = 3600
    SWICOL_TL_6h = SWICOL_TL_3600 * 6
    SWICOL_TL_12h = SWICOL_TL_3600 * 12
    SWICOL_TL_1d = SWICOL_TL_3600 * 24
    SWICOL_TL_UL = SWICOL_TL_1d * 10000

icolc=IcolConstants
class SWICOL:
    def __init__(self):
        self.logspec = None
        self.tmp = None
        self.script = None
        self.verbose_level = 0
        self.event_list = None
        self.setvx = None
        self.umask = None
        self.blocksize = None
        self.delaytime = 0
        self.nhops = 0
        self.targetpath = None
        self.event_index = 0
        self.id_string = None
        self.debug_task_scripts = 0
        self.master_alarm = 0
        self.event_fd = 0
        self.magic_header = None
        self.needs_synct_eoa = 0

def subshell_marks(subsh, type_, wh, nhops, verbose_level, group_delim):
    is_subshell = (group_delim[0] == '(')
    fr()
    subsh.str_ = ""
    if wh == ord('L'):
        #		
        # Left
        #		 
        subsh.str_ += "{}\n".format(group_delim)
    elif wh == ord('R'):
        #		
        # Right
        #		 
        subsh.str_ += "exit $sw_retval;\n{}".format(group_delim[2:])
        if is_subshell and type_ == "source":
            #			
            # Source
            #			
            if nhops >= 1:
                if verbose_level == 0:
                    subsh.str_ += "0</dev/null 2>/dev/null"
                else:
                    subsh.str_ += "0</dev/null"
            else:
                if verbose_level == 0:
                    subsh.str_ += "2>/dev/null"
                else:
                    subsh.str_ += "0</dev/null"
        elif is_subshell and type_ == "target":
            #			
            # Target
            #			
            if nhops >= 1:
                if verbose_level == 0:
                    subsh.str_ += "1>/dev/null 2>/dev/null"
                else:
                    subsh.str_ += "1>/dev/null"
            else:
                if verbose_level == 0:
                    subsh.str_ += "1>/dev/null 2>/dev/null"
                else:
                    subsh.str_ += "1>/dev/null"
        elif is_subshell and type_ == "install_target":
            if nhops >= 1:
                if verbose_level == 0:
                    subsh.str_ += ""
                else:
                    subsh.str_ += ""
            else:
                if verbose_level == 0:
                    subsh.str_ += "2>/dev/null"
                else:
                    subsh.str_ += ""
        else:
            if is_subshell != 0:
                print("internal error in swicol_task_wait at line {}".format(fr.__LINE__))
                sys.exit(1)
        if is_subshell != 0:
            subsh.str_ += "; exit $?"
        else:
            subsh.str_ += ""
    else:
        # error 
        pass
    return subsh.str_


def init_magic_header(swicol):
    swicol.magic_header = "# # # # # # # # # # # # # # # # # # # # # # # # # # # ##\n" * 9
    if len(swicol.magic_header) < 513:
        exit(42)

def task_wait_for(swicol, retbuf, event_fd, timelimit, stop_string):
    retval = -1
    readReturn = 0
    buf = ""
    start = time.time()
    now = start
    req = 0
    E_DEBUG("Entering: %s" % stop_string)
    if retbuf:
        retbuf = ""
    alarm_start = 0
    alarm_monitor = 0
    cret = 0
    do_stop = 0
    E_DEBUG("")
    while (alarm_monitor == 0) and ((cret == 0) or (do_stop == 0 and (now - start) < timelimit)):
        time.sleep(req['tv_sec'] + req['tv_nsec'] / 1000000000)
        now = time.time()
        if swicol.master_alarm:
            E_DEBUG("Got Alarm")
            if alarm_start == 0:
                E_DEBUG("Set Alarm Start")
                alarm_start = time.time()
            if (now - alarm_start) > 1:
                E_DEBUG("Alarm Expired setting alarm_monitor")
                alarm_monitor = 1
        ret = swgpReadLine(buf, event_fd, readReturn)
        if cret == 0:
            cret = ret
        E_DEBUG("read buf: [%s]" % buf)
        s = buf.find(stop_string)
        if s:
            if len(str(s)) == 6 and s[5] == '\n':
                E_DEBUG("Normal stop")
                retval = 0
                do_stop = 1
            elif s.find(icolc.SWICOL_START_STRING) and s[len(str(s)) - 1] == '\n':
                E_DEBUG("Got Start")
                retval = 0
                do_stop = 1
            elif len(str(s)) > 8 and s.find(icolc.SWICOL_STOP_STRING):
                E_DEBUG("")
                retval = 1
                do_stop = 1
                sys.stderr.write("internal error in swicol_task_wait at line %d\n" % fr.__LINE__)
                sys.stderr.write("expect string [%s]\n" % s)
            else:
                E_DEBUG("partial read")
        else:
            E_DEBUG("nothing")
        if ret and retbuf:
            retbuf += buf
    if do_stop == 0 and swicol.master_alarm == 0:
        E_DEBUG("time limit exceeded")
        retval = 2
        swutil_doif_writef(swicol.verbose_level, 1, swicol.logspecM, sys.stderr.fileno(), "SW_RESOURCE_ERROR: time limit of %d seconds exceeded\n" % timelimit)
    elif alarm_monitor or swicol.master_alarm:
        E_DEBUG("ALARM")
        retval = 2
        swutil_doif_writef(swicol.verbose_level, 1, swicol.logspecM, sys.stderr.fileno(), "SW_ABORT_SIGNAL_RECEIVED: user interrupt\n")
    E_DEBUG("Leaving: %d" % retval)
    return retval
def form_debug_task_filename(buf):
    s = buf
    while s:
        if s == ' ':
            s = '_'
        if s == ':':
            s = '\0'
            break
        s += 1

def set_selected_index(swicol, first_current_event):
    if first_current_event < 0:
        return swicol.event_index
    else:
        return first_current_event

def set_buf(dst, buf):
    if dst:
        del dst
    dst = buf
    return dst

def convert_result_to_event_list(event_string, event_list):
    ret = strar_num_elements(event_list)
    s = event_string.find('\n')
    current = event_string
    while s and s:
        s = '\0'
        print("adding event to event list: %s", current)
        strar_add(event_list, current)
        s += 1
        current = s
        s = event_string.find(s, '\n')
    strar_add(event_list, "\n")
    return ret

def print_task_script(swicol, command, size, dir_, task_script, task_desc):
    ubuf = ""
    synct_eof_buf = ""
    synct_eof_buf2 = ""
    blocks = size // 512
    if size % 512:
        blocks += 1
        print("Internal error: in swicol.c:print_task_script")
    if swlib_is_ascii_noaccept(dir_, swlibc.SW_TAINTED_CHARS, "\a\b\n\r\t \v\\"):
        print("print_task_script: illegal dir:", dir_)
        return 1
    if swicol.verbose_level > swlibc.SWC_VERBOSE_8:
        verbosestring = "set -vx"
    else:
        verbosestring = "#set -vx"
    if swicol.verbose_level >= swlibc.SWC_VERBOSE_5:
        tonullstring = " "
    else:
        tonullstring = "2>/dev/null"
    if swicol.needs_synct_eoa:
        synct_eof_buf = "(\n(\n"
        swlib_append_synct_eof(synct_eof_buf2)
    enddesc = "%s: "+ec.SWEVENT_STATUS_PFX+"$sw_retval"+task_desc
    ablocks = swlib_umaxtostr(blocks, ubuf)
    command = "{\ndd bs=512 count=%s %s | (\n# trap true 1 2 13 14 15\n" % (ablocks, tonullstring)
    command += swlibc.CSHID
    command += "%s\n" % verbosestring
    command += "swxdir=\"%s\"\n" % dir_
    command += "cd \"$swxdir\"\n"
    command += "swret=$?; export swret\n"
    command += "%s\n" % verbosestring
    command += "case $swret in\n"
    command += "0)\n"
    command += ";;\n"
    command += "*)\n"
    command += "%s\n" % synct_eof_buf
    command += "dd count=%s of=/dev/null 2>/dev/null\n" % ablocks
    command += "%s\n" % synct_eof_buf2
    command += "%s\n" % synct_eof_buf2
    command += "exit \"$swret\"\n"
    command += ";;\n"
    command += "esac\n"
    command += "%s" % synct_eof_buf2
    command += "case \"$sw_retval\" in\n"
    command += "\"\") echo \"%s:\" Warning: sw_retval is not set for task script: %s 1>&2\n" % (task_desc, task_script)
    command += "sw_retval=0\n"
    command += ";;\n"
    command += "esac\n"
    command += "exit \"$sw_retval\"\n"
    command += "); sw_retval=$?\n"
    command += "%s\n" % synct_eof_buf2
    command += "exit $sw_retval\n"
    command += "}\n"
    return 0

def swicol_create():
    swicol = SWICOL()
    swicol.tmp = ""
    swicol.script = ""
    swicol.logspec = None
    swicol.verbose_level = 0
    swicol.event_list = []
    swicol.umask = None
    swicol.blocksize = None
    swicol.setvx = None
    swicol.delaytime = 0
    swicol.targetpath = None
    swicol.nhops = 1
    swicol.event_index = 1
    swicol.id_string = ""
    swicol.debug_task_scripts = 0
    swicol.needs_synct_eoa = 0
    swicol_clear_master_alarm(swicol)
    swicol_set_event_fd(swicol, -1)
    return swicol

def swicol_delete(swicol):
    del swicol

def swicol_set_event_fd(swicol, fd):
    swicol.event_fd = fd

def swicol_set_delaytime(swicol, delay):
    swicol.delaytime = delay

def swicol_set_nhops(swicol, nhops):
    swicol.nhops = nhops

def swicol_set_verbose_level(swicol, level):
    swicol.verbose_level = level

def swicol_set_umask(swicol, buf):
    swicol.umask = buf

def swicol_set_setvx(swicol, buf):
    swicol.setvx = buf

def swicol_set_targetpath(swicol, buf):
    swicol.targetpath = buf

def swicol_get_umask(swicol):
    return swicol.umask

def swicol_get_setvxk(swicol):
    return swicol.setvx

def swicol_get_blocksize(swicol):
    return swicol.blocksize

def swicol_rpsh_task_send_script2(swicol, fd, data_size, dir_, script, f_desc):
    desc = f_desc
    if swicol_get_master_alarm_status(swicol) == icolc.SWICOL_ABORT_STEP_2:
        return -1
    elif swicol_get_master_alarm_status(swicol) == icolc.SWICOL_ABORT_STEP_1:
        # do the task script but intentionally send a wrong task
        # id_string, this will cause the target main script to abort
        swicol.master_alarmM = icolc.SWICOL_ABORT_STEP_2
        desc = icolc.SWICOL_ALARM_EVENT

    tmp = swicol.script
    strob_strcpy(tmp, "")

    strob_sprintf(tmp, 1, "%s", swicol.magic_header)
    strob_sprintf(tmp, 1, "# SWI_TASK: %s\n", desc)
    strob_sprintf(tmp, 1, "\n")
    ret = os.write(fd, tmp.str())
    if ret != len(tmp):
        return -1
    E_DEBUG("")
    swicol_set_task_idstring(swicol, f_desc)
    temp_ref_desc = desc
    ret = swicol_rpsh_task_send_script(swicol, fd, data_size, dir_, script, temp_ref_desc)
    desc = temp_ref_desc
    E_DEBUG("")

    if swicol.debug_task_scripts:
        E_DEBUG("")
        swicol_write_debug_task_script(swicol, strob_str(swicol.script))

    if swicol.event_fdM >= 0:
        swicol_rpsh_wait_304(swicol, swicol.event_fd)

    E_DEBUG("")
    if swicol_get_master_alarm_status(swicol) != 0:
        if ret == 0:
            return -1
    return ret


def swicol_rpsh_task_send_script(swicol, fd, data_size, dir_, script, desc):
    #
    # swicol_rpsh_task_send_script - send script to shell stdin
    #
    # Return 0 on success, -1 on error
    #
    tmp = swicol.script
    strob_strcpy(tmp, "")

    if data_size == 0:
        sys.stderr.write("%s: Error: a non-zero length data payload is required.\n" % swlib_utilname_get())

    if print_task_script(swicol, tmp, data_size, dir_, script, desc) != 0:
        sys.stderr.write("swicol_rpsh_task_send_script: error forming task script\n")
        return -2

    E_DEBUG2("\n<AA>\n%s</AA>\n", strob_str(tmp))

    ret = atomicio('write', fd, tmp.str(), tmp.length())
    eret = strob_strlen(tmp)
    if ret == eret:
        return 0
    else:
        sys.stderr.write("rpsh_task_send_script: error writing task script to fd=%d: %s\n" % (fd, os.strerror(eret.err)))
        return -1


def swicol_rpsh_wait_cts(swicol, event_fd):
    stop_string = icolc.SWICOL_CTS_STRING
    buf = ""
    ret = task_wait_for(swicol, buf, event_fd, icolc.SWICOL_TL_20, stop_string)
    first_current_event = convert_result_to_event_list(buf, swicol.event_list)
    swicol.event_index = first_current_event
    return ret

def swicol_rpsh_wait_304(swicol, event_fd):
    stop_string = icolc.SWICOL_START_STRING
    ret = task_wait_for(swicol, None, event_fd, icolc.SWICOL_TL_9, stop_string)
    return ret

def swicol_rpsh_task_wait(swicol, retbuf, event_fd, timelimit):
    stop_string = icolc.SWICOL_STOP_STRING
    ret = task_wait_for(swicol, retbuf, event_fd, timelimit, stop_string)
    return ret

def swicol_rpsh_wait_for_event(swicol, retbuf, event_fd, event):
    buf = str(event) + ":"
    ret = task_wait_for(swicol, retbuf, event_fd, icolc.SWICOL_TL_10, buf)
    return ret

def swicol_rpsh_get_event_message(swicol, event_value, first_current_event, p_index):
    retval = None
    tmp = ""
    event_list = swicol.event_list
    ix = set_selected_index(swicol, first_current_event)
    strob_sprintf(tmp, 0, str(event_value) + ":")
    s = event_list[ix]
    while s:
        if s.find(tmp) != -1:
            s = s[s.find(":")+1:]
            retval = s
            break
        ix += 1
        s = event_list[ix]
    if p_index:
        p_index = ix - 1
    return retval

def swicol_rpsh_get_event_status(swicol, msg, event_value, first_current_event, p_index):
    atoi_ret = 0
    if not msg:
        msg = swicol_rpsh_get_event_message(swicol, event_value, first_current_event, p_index)
    if not msg:
        return -1
    if msg.isdigit():
        retval = swlib_atoi(msg, atoi_ret)
        if atoi_ret:
            retval = swc.SW_INTERNAL_ERROR_127
    elif msg == ec.SWEVENT_STATUS_PFX + ec.SWEVENT_VALUE_PREVIEW:
        retval = 0
    else:
        sys.stderr.write(swlib_utilname_get() + ": event " + str(event_value) + " did not return a valid status\n")
        retval = swc.SW_INTERNAL_ERROR_128
    return retval

def swicol_rpsh_task_expect(swicol, event_fd, timelimit):
    retval = 0
    buf = ""
    event_list = swicol.event_list
    swicol.event_index = -1
    ret = swicol_rpsh_task_wait(swicol, buf, event_fd, timelimit)
    if ret:
        retval = -4
    if "305:" not in buf:
        sys.stderr.write(swlib_utilname_get() + ": error: END event not received for task: " + swicol.id_string + "\n")
        if retval == 0:
            retval = -1
    else:
        ev = buf[buf.find("305:") + 4:]
        if not ev.isdigit():
            sys.stderr.write(swlib_utilname_get() + ": error: END event has invalid status: " + swicol.id_string + " status=" + buf + "\n")
            retval = -2
        else:
            retval = swlib_atoi(ev, None)
    first_current_event = convert_result_to_event_list(buf, event_list)
    if swicol.verbose_level >= swlibc.SWC_VERBOSE_8:
        swicol_show_events_to_fd(swicol, sys.stderr.fileno(), first_current_event)
    swicol.event_index = first_current_event
    return retval

def swicol_show_events_to_fd(swicol, fd, first_current_event):
    if first_current_event < 0:
        ix = swicol.event_index
    else:
        ix = first_current_event
    swicol_print_events(swicol, swicol.tmp, ix)
    length = len(swicol.tmp)
    ret = os.write(fd, swicol.tmp)
    return ret != length

def swicol_print_event(swicol, buf, ev):
    buf += swlib_utilname_get() + ": swicol: " + ev + "\n"

def swicol_print_events(swicol, buf, index):
    i = set_selected_index(swicol, index)
    buf += swlib_utilname_get() + ": swicol: event stack start\n"
    while True:
        ev = swicol.event_list[i]
        if ev == "\n":
            swicol_print_event(swicol, buf, "")
        else:
            swicol_print_event(swicol, buf, ev)
        if ev == "\n" and index < 0:
            break
        i += 1
    buf += swlib_utilname_get() + ": swicol: event stack end\n"

def swicol_subshell_marks(subsh, type_, wh, nhops, verbose_level):
    return subshell_marks(subsh, type_, wh, nhops, verbose_level, "(\x00)")

def swicol_brace_marks(subsh, type_, wh, nhops, verbose_level):
    return subshell_marks(subsh, type_, wh, nhops, verbose_level, "{\x00}")

def swicol_initiate_fall_thru(swicol):
    return 0

def swicol_clear_task_idstring(swicol):
    swicol_set_task_idstring(swicol, "__unset__")

def swicol_set_task_debug(swicol, do_debug):
    swicol.debug_task_scripts = do_debug

def swicol_set_task_idstring(swicol, id_):
    swicol.id_string = id_

def swicol_get_task_idstring(swicol):
    return swicol.id_string

def swicol_write_debug_task_script(swicol, script):
    name = icolc.SWICOL_DEBUG_TASK_SCRIPT_PREFIX + swicol.id_string
    form_debug_task_filename(name)
    ret = swlib_tee_to_file(name, -1, script, -1, 0)
    return ret

def swicol_set_master_alarm(swicol):
    swicol.master_alarm = icolc.SWICOL_ABORT_STEP_1

def swicol_clear_master_alarm(swicol):
    swicol.master_alarm = 0

def swicol_get_master_alarm_status(swicol):
    return swicol.master_alarm

def swicol_send_loop_trailer(swicol, fd):
    send = icolc.SWICOL_TRAILER + "\n"
    ret = os.write(fd, send.encode())
    if ret == len(send):
        E_DEBUG("OK")
        return 0
    else:
        E_DEBUG("error")
        return -1

