# vrealpath.py - Virtual Real path resolution.
#

#
# Copyright (C) 2003  James H. Lowe, Jr.
#   All rights reserved.
#
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

# SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note
# Change Log
# ------------------------------------------------------------------
# 20240106 Paul Weber - vrealpath.py created, no errors.

from sd.swsuplib.strob import strob_open, strob_strcpy, strob_strcat, strob_str
from sd.swsuplib.strob import strob_strtok, strob_strlen, strob_close
from sd.swsuplib.misc.swlib import swlib_slashclean, swlib_squash_leading_slash
from sd.swsuplib.misc.cstrings import strcmp, strrchr
def swlib_vrealpath(vpwd, ppath, depth, resolved_path):
    count = 0
    numcomponents = 0
    compcount = 0

    tmp = strob_open(10)
    bpath = strob_open(10)

    if vpwd.arg_value and len(vpwd.arg_value) != 0:
        strob_strcpy(bpath, vpwd.arg_value)
        if vpwd.arg_value[len(vpwd.arg_value) - 1] != '/':
            strob_strcat(bpath, "/")
    strob_strcat(bpath, ppath.arg_value)

    path = strob_str(bpath)

    swlib_slashclean(path)

    if resolved_path:
        strob_strcpy(resolved_path, "")

    if vpwd.arg_value and len(vpwd.arg_value) != 0:
        is_absolute_path = 1 if (vpwd.arg_value[0] == '/') else 0
    else:
        is_absolute_path = 1 if (ppath.arg_value[0] == '/') else 0

    s = strob_strtok(tmp, path, "/")
    while s != '\0':
        do_add = 0
        if strcmp(s, "..") == 0:
            do_add = -1
            count -= 1
        elif strcmp(s, ".") == 0:
            # do nothing
            pass
        else:
            do_add = 1
            count += 1

        if resolved_path:
            if do_add > 0:
                if (is_absolute_path != 0 and compcount == 0) or compcount != 0:
                    if strob_strlen(resolved_path) != 0 and                             (strob_str(resolved_path) + strob_strlen(resolved_path) - 1) != '/':
                        strob_strcat(resolved_path, "/")
                strob_strcat(resolved_path, s)
                compcount += 1
            elif do_add < 0:
                ls = strrchr(strob_str(resolved_path), '/')
                if ls != '\0' and ls != strob_str(resolved_path):
                    ls[0] = '\0'
                elif is_absolute_path != 0 and count <= 0:
                    strob_strcpy(resolved_path, "/")
                elif is_absolute_path == 0 and count <= 0:
                    strob_strcpy(resolved_path, "")
            else:
                pass
            if is_absolute_path == 0:
                swlib_squash_leading_slash(strob_str(resolved_path))

        numcomponents += 1
        s = strob_strtok(tmp, None, "/")

    if depth.arg_value:
        depth.arg_value = count
    strob_close(tmp)
    strob_close(bpath)
    return numcomponents

