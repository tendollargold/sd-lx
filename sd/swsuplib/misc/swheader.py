# swheader.py  --  Routines for searching the sw_parser output.
#
#  Copyright (C) 1998-2004  James H. Lowe, Jr.  <jhlowe@acm.org>
#  Copyright (C) 2023 Paul Weber convert to python
#  All Rights Reserved.
#
#  COPYING TERMS AND CONDITIONS
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
import ctypes
import sys
import tarfile

from sd.debug import E_DEBUG,E_DEBUG2, SWHEADER_E_DEBUG, SWHEADER_E_DEBUG2, SWERROR
from sd.swsuplib.swutillib import swlib_utilname_get
from sd.swsuplib.cplob import cplob_open, cplob_add_nta, cplob_release
from sd.swsuplib.misc.cstrings import strcmp
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.swdef import swdef_write_attribute_to_buffer, swdef_write_attribute
from sd.swsuplib.misc.swlib import swlib_strncpy, swlib_atoi, fr, SWLIB_ASSERT
from sd.swsuplib.misc.swsdflt import swsdflt_return_entry, swsdflt_get_value_type, sdvt
from sd.swsuplib.strob import strob_close
from sd.swsuplib.swparse.swparse import swpc, swparse_write_attribute_att, swparse_write_attribute_obj
from sd.swsuplib.taru.ahs import (ahsstaticsettarusername, ahsstaticsettargroupname,ahsstaticgettarusername,ahsstaticsettarfilename, 
                                  ahsstaticgettargroupname, taru_get_uid_by_name, taru_get_gid_by_name,
                                  ahsstaticsetpaxlinkname)
from sd.swsuplib.taru.cpiohdr import chc
from sd.swsuplib.taru.filetypes import NOTDUMPEDTYPE
# from sd.swsuplib.taru.taruib import taru_otoul
from sd.swsuplib.misc.strar import strar_close
from sd.swsuplib.taru.taru import taru_init_header, tc, taru_init_header_digs
from sd.swsuplib.taru.otar import taru_set_filetype_from_tartype
from sd.swsuplib.uinfile.uinfile import uinc
from sd.swsuplib.versionid import (swvidc, swverid_set_namespace, swverid_add_attribute, swverid_open, swverid_set_tag,
                                   swverid_set_comparison_code, swverid_close, swverid_verid_compare, swverid_vtagOLD_compare)

USTAR_FILEFORMAT = 1
CPIO_NEWC_FILEFORMAT = 2
CPIO_CRC_FILEFORMAT = 3
SWHEADER_IMAGE_STACK_LEN = 5
SWHEADER_GET_NEXT = 0
SWHEADER_PEEK_NEXT = 1
SW_ARCHIVE_TAR_USTAR = uinc.USTAR_FILEFORMAT
SW_ARCHIVE_CPIO_NEWC =  CPIO_NEWC_FILEFORMAT
SW_ARCHIVE_CPIO_CRC = CPIO_CRC_FILEFORMAT
UCHAR_MAX = 255
NEWLINE_LEN = 1
MAGNITUDE_ARRAY_LEN = 10

class SWHEADER_STATE:
    def __init__(self):
        self.save_current_offset_p = None
        self.save_current_offset = 0
        self.save_current_offset_v = 0

F_GOTO_NEXT = ctypes.CFUNCTYPE(ctypes.c_char_p, ctypes.c_void_p, ctypes.POINTER(ctypes.c_int), ctypes.c_int)

class SWHEADER:
    def __init__(self):  # swheader object
        self.image_head_ = None  # pointer to the first line of metadata, i.e. offset zero
        self.image_object_ = None # pointer to object that knows how to iterate the metadata.
        self.image_object_stack_ = [None] * (SWHEADER_IMAGE_STACK_LEN + 1)
        self.current_offset_p_ = None # current offset pointer
        self.current_offset_ = 0 # current offset
        self.saved_ = SWHEADER_STATE()
        self.f_goto_next_ = None

def swheaderline_get_type_pointer(output_line):
    ret = None
    if output_line.isdigit():
        if output_line[swpc.SWPARSE_MKUP_LEN_WIDTH] == ' ':
            ret = output_line[swpc.SWPARSE_MKUP_LEN_WIDTH + 1:]
        else:
            t = output_line
            while t.isdigit():
                t += 1
            if t == '\0' or t != ' ':
                print("internal error in swheaderline_get_type_pointer")
                return None
            t += 1
            if t[0] == swpc.SWPARSE_MD_TYPE_ATT or t[0] == swpc.SWPARSE_MD_TYPE_OBJ or t[0] == swpc.SWPARSE_MD_TYPE_EXT or t[0] == swpc.SWPARSE_MD_TYPE_FILEREF or 0:
                pass
            else:
                print("internal error in swheaderline_get_keyword loc=2")
                return None
            ret = t
        return ret
    else:
        return output_line

def swheaderline_write_to_buffer(buf, line):
    keyword = swheaderline_get_keyword(line)
    value = swheaderline_get_value(line, None)
    xtype = swheaderline_get_type(line)
    level = swheaderline_get_level(line)
    if keyword and len(keyword):
        return swdef_write_attribute_to_buffer(buf, keyword, value, level, xtype)
    else:
        return 0

def swheaderline_write(line, uxfio_fd):
    len = 0
    keyword = swheaderline_get_keyword(line)
    value = swheaderline_get_value(line, len)
    type = int(swheaderline_get_type(line))
    level = swheaderline_get_level(line)
    len = -1
    if keyword:
        return swdef_write_attribute(keyword, value, level, len, int(type), uxfio_fd)
    else:
        return 0
def swheaderline_get_level(outputline):
    typep = swheaderline_get_type_pointer(outputline)
    if not typep:
        print("internal error in swheaderline_get_level")
        exit(2)
    typep += 2
    return ord(typep) - 0x30

def swheaderline_get_type(output_line):
    p = swheaderline_get_type_pointer(output_line)
    if p:
        return p
    else:
        return 0

def swheaderline_get_keyword(output_line):
    if not output_line:
        return None
    if output_line.isdigit():
        t = output_line + swpc.SWPARSE_MKUP_LEN_WIDTH + 1
    else:
        t = output_line
    if t[0] == swpc.SWPARSE_MD_TYPE_ATT or t[0] == swpc.SWPARSE_MD_TYPE_OBJ or t[0] == swpc.SWPARSE_MD_TYPE_EXT or t[0] == swpc.SWPARSE_MD_TYPE_FILEREF or 0:
        pass
    else:
        t = swheaderline_get_type_pointer(output_line)
        if not t:
            return None
    t += 3
    while t and t.isspace():
        t += 1
    ret = t
    while t and not t.isspace():
        t += 1
    return ret


def swheaderline_strdup(outputline):
    value_length = 0
    value = swheaderline_get_value(outputline, value_length)
    length = int(value - outputline) + value_length + 1
    dup = outputline[:length+1]
    return dup

def swheaderline_get_value_pointer(output_line, value_len):
    return get_value(output_line, value_len, 0)

def swheaderline_get_value(output_line, value_len):
    return get_value(output_line, value_len, 1)

def swheaderline_set_flag1(output_line):
    output_line[1] = '0'

def swheaderline_clear_flag1(output_line):
    output_line[1] = ' '

def swheaderline_get_flag1(output_line):
    return 0 if output_line[1] == ' ' else 1

def get_value(output_line, value_len, do_terminate):
    if output_line is None:
        return None
    if ret := swheaderline_get_keyword(output_line) is None:
        return None

    if swheaderline_get_type(output_line) == swpc.SWPARSE_MD_TYPE_OBJ:
       # if value_len:
       #     value_len = 0
        return ret + len(ret)

    length = swheaderline_get_value_length(output_line)

    #if value_len:
    #    value_len = length

    if do_terminate:
        ret[len(ret) + 1 + length] = '\0'
    return ret + len(ret) + 1  # pointer to the value

def swheaderline_get_value_length(output_line):
    mytens = [0, 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000]
    if not output_line.isdigit():
        print("bad parser output, value length not found")
        return -1
    n = 0
    t = output_line
    p = output_line
    while t and not t.isspace():
        t += 1
    for i in range(len(t - output_line), 0, -1):
        if i >= MAGNITUDE_ARRAY_LEN:
            print("fatal error: attribute value length in swheaderline_get_value_length() too big")
            exit(33)
        if not p.isdigit():
            print("internal error in swheaderline_get_value_length [%s]" % output_line)
        n += (ord(p) - 48) * mytens[i]
        p += 1
    return n

def swheader_state_create():
    x = SWHEADER_STATE()
    return x

def swheader_state_delete(x):
    del x

def swheader_state_copy(to, from_):
    to.save_current_offset_pM = from_.save_current_offset_pM
    to.save_current_offsetM = from_.save_current_offsetM
    to.save_current_offset_vM = from_.save_current_offset_vM

def swheader_store_state(header, fp_state):
    state = fp_state if fp_state else header.saved_
    state.save_current_offset_pM = header.current_offset_p_
    state.save_current_offsetM = header.current_offset_
    if header.current_offset_p_:
        state.save_current_offset_vM = header.current_offset_p_[0]
    else:
        state.save_current_offset_vM = 0

def swheader_restore_state(header, state):
    if state:
        restore_state(header, state)
    else:
        restore_state(header, header.saved_)

def restore_state(header, state):
    header.current_offset_p_ = state.save_current_offset_p
    header.current_offset_ = state.save_current_offset
    if header.current_offset_p_:
        header.current_offset_p_[0] = state.save_current_offset_v

def swheader_open(f_goto_next, image_object):
    swheader = SWHEADER()
    for i in range(SWHEADER_IMAGE_STACK_LEN + 1):
        swheader.image_object_stack_[i] = None
    if image_object:
        swheader_set_image_object(swheader, image_object, 0)
        swheader.image_object_ = image_object
    else:
        swheader_set_image_object(swheader, swheader, 0)
        swheader.image_object_ = swheader
    swheader.f_goto_next_ = f_goto_next
    swheader.current_offset_p_ = [swheader.current_offset_]
    swheader.current_offset_ = 0
    swheader.image_head_ = None
    if not swheader.f_goto_next_:
        swheader_set_iter_function(swheader, swheader_goto_c_next_line)
    swheader_store_state(swheader, swheader.saved_)
    return swheader

def swheader__reset(swheader):
    swheader.current_offset_ = 0
    swheader.current_offset_p_ = 0
def swheader_goto_c_next_line(vheader, output_line_offset, peek_only):
    swheader = vheader
    ih = swheader.image_head_
    if not output_line_offset:
        swheader__reset(swheader)
        return None
    if output_line_offset == sys.maxsize:
        return ih
    if output_line_offset < 0:
        return ih + (-output_line_offset)
    type = swheaderline_get_type(ih + output_line_offset)
    if type == 'F':
        p = ih + output_line_offset + ih[output_line_offset:].index('\n') + NEWLINE_LEN
        if p:
            output_line_offset = p - swheader.image_head_
        return p
    keyw = swheaderline_get_keyword(ih + output_line_offset)
    if keyw == None:
        return None
    n = swheaderline_get_value_length(ih + output_line_offset)
    if n < 0:
        return None
    if type == 'A':
        n += 1
    line = keyw + (len(keyw) + n + NEWLINE_LEN)
    if line == '\0' and peek_only == 0:
        output_line_offset = -1
    if line == '\0':
        return None
    if peek_only == 0:
        output_line_offset = line - swheader.image_head_
    return line
def swheader_close(swheader):
    if swheader.image_head_:
        del swheader.image_head_
    del swheader

def swheader_reset(swheader):
    swheader__reset(swheader)
    if swheader.f_goto_next_ != swheader_goto_c_next_line:
        while swheader_f_goto_next(swheader):
            pass
    swheader__reset(swheader)
    swheader.f_goto_next_(swheader.image_object_, None, SWHEADER_GET_NEXT)

def swheader_f_goto_next(swheader):
    ret = swheader.f_goto_next_(swheader.image_object_,
                                swheader_get_current_offset_p(swheader),
                                SWHEADER_GET_NEXT)
    return ret

def swheader_goto_next_line(vheader, output_line_offset, peek_only):
    ret = ((vheader).f_goto_next_)((vheader).image_object_,
                                    output_line_offset, peek_only)
    return ret

def swheader_get_current_line(swheader):
    i = -swheader_get_current_offset(swheader)
    p = swheader.f_goto_next_(swheader.image_object_, i, SWHEADER_PEEK_NEXT)
    if not p:
        return None
    else:
        return p

def swheader_set_iter_function(swheader, fc):
    swheader.f_goto_next_ = fc

def swheader_set_image_object(swheader, image, index):
    i = 0
    if index < 0:
        while swheader.image_object_stack_[i]:
            i += 1
            if i >= SWHEADER_IMAGE_STACK_LEN:
                return -1
    else:
        i = index
    swheader.image_object_stack_[i] = image
    return i

def swheader_set_image_object_active(swheader, index):
    swheader.image_object_ = swheader.image_object_stack_[index]

def swheader_set_image_head(swheader, image):
    if swheader.image_head_:
        del swheader.image_head_
    swheader.image_head_ = image

def swheader_set_current_offset(swheader, n):
    swheader_set_current_offset_p_value(swheader, n)

def swheader_set_current_offset_p_value(swheader, n):
    swheader.current_offset_p_[0] = n

def swheader_set_current_offset_p(swheader, n):
    if not n:
        swheader.current_offset_p_ = [swheader.current_offset_]
    else:
        swheader.current_offset_p_ = n

def swheader_get_iter_function(swheader):
    return swheader.f_goto_next_

def swheader_get_current_offset_p(swheader):
    return swheader.current_offset_p_

def swheader_get_current_offset(swheader):
    return swheader.current_offset_p_

def swheader_get_image_head(swheader):
    i = sys.maxsize
    return swheader.f_goto_next_(swheader.image_object_, i, SWHEADER_GET_NEXT)

def swheader_get_next_object(swheader, relative_level_min, relative_level_max):
    next_line = None
    object_level = 0
    current_relative_level = 0
    SWHEADER_E_DEBUG("")
    next_line = swheader_goto_next_line(swheader,
                                        swheader_get_current_offset_p(swheader),
                                        SWHEADER_GET_NEXT)
    while next_line:
        SWHEADER_E_DEBUG("")
        current_relative_level = swheaderline_get_level(next_line) - object_level
        if swheaderline_get_type(next_line) == swpc.SWPARSE_MD_TYPE_OBJ:
            if relative_level_min == UCHAR_MAX and relative_level_max == UCHAR_MAX:
                SWHEADER_E_DEBUG("")
                return next_line
            elif (current_relative_level < relative_level_min or
                    current_relative_level > relative_level_max):
                SWHEADER_E_DEBUG("tested NULL")
                return None
            elif relative_level_min <= current_relative_level <= relative_level_max:
                SWHEADER_E_DEBUG("")
                return next_line
        next_line = swheader_goto_next_line(swheader,
                                            swheader_get_current_offset_p(swheader),
                                            SWHEADER_GET_NEXT)
    SWHEADER_E_DEBUG("terminating NULL")
    return None

def swheader_generate_swverid(swheader, swverid, object_line):
    length = 0
    next_attr = swheader_get_next_attribute(swheader)
    object_offset = swheader_get_current_offset(swheader)
    if object_line is None:
        return -1
    #
    # Enforce requirement that we must be at the start of an object
    #
    if swheaderline_get_type(object_line) != swpc.SWPARSE_MD_TYPE_OBJ:
        return -2
    object_keyword = swheaderline_get_keyword(object_line)
    swverid_set_namespace(swverid, object_keyword)
    while next_attr == swheader_get_next_attribute(swheader):
        value = swheaderline_get_value(next_attr, length)
        keyw = swheaderline_get_keyword(next_attr)
        if not keyw or not value:
            sys.stderr.write("error in swheader_generate_swverid.\n")
            swheader_set_current_offset_p_value(swheader, object_offset)
            return -3
        if swverid_add_attribute(swverid, object_keyword, keyw, value) < 0:
            return -4
    #
    # Restore the position in the header.
    #
    swheader_set_current_offset_p_value(swheader, object_offset)
    return 0

def swheader_get_single_attribute_value(swheader, keyword):
    state = SWHEADER_STATE()
    swheader_store_state(swheader, state)
    line = get_attribute_in_current_object(swheader, keyword, None, None)
    if line is None:
        swheader_restore_state(swheader, state)
        return None
    value = swheaderline_get_value(line, None)
    swheader_restore_state(swheader, state)
    return value

def swheader_get_attribute(swheader, attribute_keyword, is_multi):
    ret = str(None)

    SWERROR("SWHEADER DEBUG: ",
               "")  # define NEWLINE_LEN 1static char swheader_goto_c_next_line(object * vheader, int * output_line_offset, int peek_only)[0]
    list_ = swheader_get_attribute_list(swheader, attribute_keyword, is_multi)
    pp = list_
    if not list_:
        return str(None)
    # return the last item in the list. 	
    while pp[0] != '\0':
        ret = pp[0]
        pp = pp[1:]
    del list_
    return ret


def swheader_get_attribute_in_current_object(swheader, keyword, object_keyword, is_multi_value):
    return get_attribute_in_current_object(swheader, keyword, object_keyword, is_multi_value)

def get_attribute_in_current_object(swheader, keyword, object_keyword, is_multi_value):
    state = SWHEADER_STATE()

    SWERROR("SWHEADER DEBUG: ", "")#define NEWLINE_LEN 1static char swheader_goto_c_next_line(object * vheader, int * output_line_offset, int peek_only)[0]
    swheader_store_state(swheader, state)
    if object_keyword:
        sys.stderr.write("warning: get_attribute_in_current_object() is deprecated for this usage\n")

    if object_keyword:
        swd = swsdflt_return_entry(object_keyword, keyword)
        if swd is None:
            multi_line = sdvt.sdf_single_value
        else:
            multi_line = swsdflt_get_value_type(swd)
    else:
        multi_line = sdvt.sdf_single_value

    if multi_line == sdvt.sdf_single_value:
        #
        # single value, return the last one.
        #
        if is_multi_value:
            is_multi_value = None
        line = swheader_raw_get_attribute_in_object(swheader, keyword)
        prevline = line
        while line != '\0':
            line = swheader_raw_get_attribute_in_object(swheader, keyword)
            if line != '\0':
                prevline = line
        restore_state(swheader, state)
        return prevline
    else:
        #
        #  multi value attribute.
        #
        if is_multi_value:
            is_multi_value = 1
        line =  swheader_raw_get_attribute_in_object(swheader, keyword)
        restore_state(swheader, state)
        return line

def swheader_get_attribute_list(swheader, attribute_keyword, multi_line):
    line = ""
    state = SWHEADER_STATE()
    swheader_store_state(swheader, state)
    headerline = swheader_get_current_line(swheader)
    if swheaderline_get_type(headerline) != swpc.SWPARSE_MD_TYPE_OBJ:
        restore_state(swheader, state)
        return None
    #
    # keyw is the object keyword.
    #
    keyw = swheaderline_get_keyword(headerline)
    #
    # Determine if this is a multi line attribute.
    #
    if multi_line:
        swd = swsdflt_return_entry(keyw, attribute_keyword)
        if swd is None:
            multi_line = 0
        else:
            if swsdflt_get_value_type(swd) == sdvt.sdf_single_value:
                multi_line = 0
            else:
                multi_line = 1
    cplob_obj = cplob_open(2)
    # initialize the null terminated array
    cplob_add_nta(cplob_obj, None)
    while line == swheader_get_next_attribute(swheader):
        keyw = swheaderline_get_keyword(line)
        if not strcmp(attribute_keyword, keyw):
            cplob_add_nta(cplob_obj, line)
    ret = cplob_release(cplob_obj)
    restore_state(swheader, state)
    return ret

def swheader_get_next_attribute(swheader):
    att_count = 0

    SWHEADER_E_DEBUG("")
    nextline = swheader_goto_next_line(swheader, swheader_get_current_offset_p(swheader), SWHEADER_PEEK_NEXT)
    currentline = swheader_get_current_line(swheader)
    if not currentline:
        att_count = 0
        SWHEADER_E_DEBUG("returning NULL loc 0")
        return None
    elif swheaderline_get_type(currentline) == swpc.SWPARSE_MD_TYPE_OBJ and att_count:
        att_count = 0
        SWHEADER_E_DEBUG("returning NULL loc 1")
        return None
    else:
        if not nextline or swheaderline_get_type(nextline) == swvidc.SWPARSE_MD_TYPE_OBJ:
            att_count = 0
            SWHEADER_E_DEBUG("returning NULL loc 2")
            return None
        att_count += 1
        swheader_f_goto_next(swheader)
        SWHEADER_E_DEBUG2("returning next line [%s]", nextline)
        return nextline

def swheader_get_object_by_tag(swheader, object_keyword, idtag):
    swverid = swverid_open(object_keyword, None)
    swverid_set_tag(swverid, swc.SW_A_tag, idtag)
    swverid_set_comparison_code(swverid, swvidc.SWVERID_CMP_EQ)
    ret = get_object_by_swverid(swheader, swverid, swverid_vtagOLD_compare(object_keyword, idtag), None)
    swverid_close(swverid)
    return ret

def get_object_by_swverid(swheader, swverid, f_comp, nmatches):
    start_offset = swheader_get_current_offset(swheader)
    ret_object_offset = start_offset
    retval = ret_object_offset
    t_swverid = swverid_open(None, str(None))
    if f_comp is None:
        f_comp = swverid_verid_compare

    SWERROR("SWHEADER DEBUG: ", "")#define NEWLINE_LEN 1static char swheader_goto_c_next_line(object * vheader, int * output_line_offset, int peek_only)[0]

    if nmatches:
        nmatches = 0

    #	
    # step to first object
    # 	 
    next_line = swheader_get_current_line(swheader)
    E_DEBUG2("initial next_line is %p", next_line)
    if not next_line:
        return None
    while next_line and (swheaderline_get_type(next_line) != swpc.SWPARSE_MD_TYPE_OBJ):
        next_line = swheader_f_goto_next(swheader)

    #	
    # Now compare current object to the object described by 'swverid'
    #	 
    loop_condition = True
    while loop_condition:
        E_DEBUG("")
        object_offset = swheader_get_current_offset(swheader)
        ret_object_offset = object_offset
        t_swverid = swverid_open(None, str(None))
        temp_ref_next_line = next_line
        if swheader_generate_swverid(swheader, t_swverid, next_line):
            # next_line = temp_ref_next_line
            sys.stderr.write("swheader_get_object: error returned by get_object.\n")
            swheader_set_current_offset_p_value(swheader, start_offset)
            return str(None)
        else:
            next_line = temp_ref_next_line
        comparison_result = f_comp.invoke(swverid, t_swverid)
        if comparison_result is swvidc.SWVERID_CMP_EQ:
            swverid_close(t_swverid)
            t_swverid = None
            swheader_set_current_offset_p_value(swheader, object_offset)
            retval = next_line
            ret_object_offset = object_offset
            if nmatches:
                nmatches += 1
            else:
                break
            E_DEBUG("")
        if t_swverid:
            swverid_close(t_swverid)
        t_swverid = None
        E_DEBUG("")
        loop_condition = (next_line := swheader_get_next_object(swheader, int((UCHAR_MAX)), int(UCHAR_MAX)))
    if t_swverid:
        swverid_close(t_swverid)
    swheader_set_current_offset_p_value(swheader, ret_object_offset)
    return retval

def swverid_get_object_by_swverid(swheader, swverid, nmatches):
    ret = get_object_by_swverid(swheader, swverid, swverid_vtagOLD_compare(swheader, swverid), nmatches)
    return ret

def swheader_get_object_offset_by_control_directory(swheader, pairs):
    next_line = None
    object_offset = 0
    control_directory = None
    SWHEADER_E_DEBUG("")
    next_line = swheader_get_current_line(swheader)
    if not next_line:
        return -1
    while next_line and swheaderline_get_type(next_line) != swpc.SWPARSE_MD_TYPE_OBJ:
        next_line = swheader_f_goto_next(swheader)
    while True:
        control_directory = swheader_get_single_attribute_value(swheader, swc.SW_A_control_directory)
        next_line = swheader_get_next_object(swheader, UCHAR_MAX, UCHAR_MAX)
        if not next_line:
            break
    swheader_set_current_offset_p_value(swheader, object_offset)
    return -1

def swheader_raw_get_attribute_in_object(swheader, keyword):
    line = None
    keyw = None
    SWHEADER_E_DEBUG("")
    line = swheader_get_next_attribute(swheader)
    while line:
        keyw = swheaderline_get_keyword(line)
        if keyw == keyword:
            return line
        line = swheader_get_next_attribute(swheader)
    return None

def swheader_fileobject2filehdr(fileheader, file_hdr):
    info_filetype = 0
    modet = 0
    uid = 0
    gid = 0
    did_uname = 0
    did_gname = 0
    did_gid = 0
    did_uid = 0
    state = SWHEADER_STATE()
    taru_init_header(file_hdr)
    taru_init_header_digs(file_hdr)
    swheader_store_state(fileheader, state)
    while True:
        attr = swheader_get_next_attribute(fileheader)
        if not attr:
            break
        keyword = swheaderline_get_keyword(attr)
        value, length = swheaderline_get_value(attr, 0) # unknown value_len
        if keyword == swc.SW_A_mode:
            file_hdr.c_mode = 0
            # taru_otoul(value, file_hdr.c_mode)
            file_hdr.c_mode = file_hdr.c_mode & 0o07777
            file_hdr.usage_mask |= chc.TARU_UM_MODE
        elif keyword == swc.SW_A_size:
            file_hdr.c_filesize = int(value)
            if file_hdr.digs:
                swlib_strncpy(file_hdr.digs.size, value, file_hdr.digs.size)
                file_hdr.digs.do_size = chc.DIGS_ENABLE_ON
            did_size = 1
        elif keyword == swc.SW_A_path:
            ahsstaticsettarfilename(file_hdr, value)
        elif keyword == swc.SW_A_uid:
            file_hdr.c_uid = int(value)
            file_hdr.usage_mask |= chc.TARU_UM_UID
            did_uid = 1
        elif keyword == swc.SW_A_gid:
            file_hdr.c_gid = int(value)
            file_hdr.usage_mask |= chc.TARU_UM_GID
            did_gid = 1
        elif keyword == swc.SW_A_link_source:
            ahsstaticsetpaxlinkname(file_hdr, value)
            did_link_source = 1
        elif keyword == swc.SW_A_owner:
            ahsstaticsettarusername(file_hdr, value)
            did_uname = 1
            file_hdr.usage_mask |= chc.TARU_UM_OWNER
        elif keyword == swc.SW_A_group:
            ahsstaticsettargroupname(file_hdr, value)
            did_gname = 1
            file_hdr.usage_mask |= chc.TARU_UM_GROUP
        elif keyword == swc.SW_A_mtime:
            file_hdr.c_mtime = int(value)
            did_mtime = 1
            file_hdr.usage_mask |= chc.TARU_UM_MTIME
        elif keyword == swc.SW_A_major:
            file_hdr.c_rdev_maj = swlib_atoi(value, None)
            did_major = 1
        elif keyword == swc.SW_A_minor:
            file_hdr.c_rdev_min = swlib_atoi(value, None)
            did_minor = 1
        elif keyword == "md5sum":
            if file_hdr.digs:
                swlib_strncpy(file_hdr.digs.md5, value, file_hdr.digs.md5)
                file_hdr.digs.do_md5 = chc.DIGS_ENABLE_ON
        elif keyword == "sha1sum":
            if file_hdr.digs:
                swlib_strncpy(file_hdr.digs.sha1, value, file_hdr.digs.sha1)
                file_hdr.digs.do_sha1 = chc.DIGS_ENABLE_ON
        elif keyword == "sha512sum":
            if file_hdr.digs:
                swlib_strncpy(file_hdr.digs.sha512, value, file_hdr.digs.sha512)
                file_hdr.digs.do_sha512 = chc.DIGS_ENABLE_ON
        elif keyword == swc.SW_A_type:
            info_filetype = ord(value)
            did_type = 1
        elif keyword == swc.SW_A_is_volatile:
            file_hdr.usage_mask |= chc.TARU_UM_IS_VOLATILE
    tartype = swheader_getTarTypeFromTypeAttribute(info_filetype)
    if tartype == tarfile.LNKTYPE:
        file_hdr.c_is_tar_lnktype = 1
    else:
        file_hdr.c_is_tar_lnktype = 0
    taru_set_filetype_from_tartype(chr(tartype), modet, "/")
    file_hdr.c_mode = modet
    if did_uname and did_uid:
        file_hdr.c_cu = tc.TARU_C_BY_UNONE
    elif did_uid:
        ahsstaticsettarusername(file_hdr, "")
        file_hdr.c_cu = tc.TARU_C_BY_UNONE
    if did_gname and did_gid:
        file_hdr.c_cg = tc.TARU_C_BY_UNONE
    elif did_gid:
        ahsstaticsettargroupname(file_hdr, "")
        file_hdr.c_cg = tc.TARU_C_BY_UNONE
    if not did_gname and not did_gid or not did_uname and not did_uid:
        swheader_restore_state(fileheader, state)
        return 1
    if not did_gid:
        ret = taru_get_gid_by_name(ahsstaticgettargroupname(file_hdr), gid)
        if ret == 0:
            file_hdr.c_gid = gid
    if not did_uid:
        ret = taru_get_uid_by_name(ahsstaticgettarusername(file_hdr), uid)
        if ret == 0:
            file_hdr.c_uid = uid
    swheader_restore_state(fileheader, state)
    return 0

def swheader_getTarTypeFromTypeAttribute(ch):
    if ch == 'f':
        return tarfile.REGTYPE
    elif ch == 'd':
        return tarfile.DIRTYPE
    elif ch == 'c':
        return tarfile.CHRTYPE
    elif ch == 'b':
        return tarfile.BLKTYPE
    elif ch == 'p':
        return tarfile.FIFOTYPE
    elif ch == 's':
        return tarfile.SYMTYPE
    elif ch == 'h':
        return tarfile.LNKTYPE
    elif ch == swc.SW_ITYPE_y:
        return NOTDUMPEDTYPE
    else:
        sys.stderr.write("%s: invalid C701 file type [%c] not found, ignoring.\n" % (swlib_utilname_get(), ch))
        return tarfile.REGTYPE

def swheader_print_header(swheader):
    next_attr = None
    next_line = None
    state = SWHEADER_STATE()
    swheader_store_state(swheader, state)
    swheader_reset(swheader)
    swheader_set_current_offset_p_value(swheader, 0)
    next_line = swheader_get_next_object(swheader, UCHAR_MAX, UCHAR_MAX)
    while next_line:
        sys.stderr.write(next_line + sys.stderr.fileno())
        swheader_goto_next_line(swheader, swheader_get_current_offset_p(swheader), SWHEADER_PEEK_NEXT)
        while True:
            next_attr = swheader_get_next_attribute(swheader)
            if not next_attr:
                break
            sys.stderr.write(next_line + sys.stderr.fileno())
        next_line = swheader_get_next_object(swheader, UCHAR_MAX, UCHAR_MAX)
    swheader_restore_state(swheader, state)
"""
 swheaderline1.py  --  Routines to set values in the parser output.

   Copyright (C) 1998, 1999  Jim Lowe
   Copyright (C) 2024 Paul Weber conversion to Python
   All rights reserved.
   
   COPYING TERMS AND CONDITIONS:
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

"""

def determine_control_directory(swheader):
    value = swheader_get_single_attribute_value(swheader, swc.SW_A_control_directory)
    if not value:
        value = swheader_get_single_attribute_value(swheader, swc.SW_A_tag)
    return value

def search_object_for_directory(i_next_line, swheader, key_dir, object_name, level):
    control_directory = None
    next_line = i_next_line
    keyword = swheaderline_get_keyword(next_line)
    while next_line:
        if keyword == object_name:
            control_directory = determine_control_directory(swheader)
            if control_directory:
                if key_dir == control_directory:
                    return next_line
            else:
                break
        next_line = swheader_get_next_object(swheader, level, UCHAR_MAX)
    return None

def search_for_fileset(i_next_line, swheader, key_dir):
    level = swheaderline_get_level(i_next_line)
    type = swheaderline_get_type(i_next_line)
    if type == swpc.SWPARSE_MD_TYPE_OBJ:
        level += 1
    ret = search_object_for_directory(i_next_line, swheader, key_dir, swc.SW_A_fileset, level)
    return ret

def search_for_product(i_next_line, swheader, key_dir):
    level = swheaderline_get_level(i_next_line)
    ret = search_object_for_directory(i_next_line, swheader, key_dir, swc.SW_A_product, UCHAR_MAX)
    return ret

def determine_object(object_keyword, control_dir_list, swpath_ex):
    if swpath_ex.is_minimal_layout == 0 and len(swpath_ex.product_control_dir) > 0 and len(swpath_ex.fileset_control_dir) == 0:
        control_dir_list.append(swc.SW_A_product)
        control_dir_list.append(swpath_ex.product_control_dir)
        object_keyword = swc.SW_A_product
    elif swpath_ex.is_minimal_layout == 1 and len(swpath_ex.pfiles) > 0 and len(swpath_ex.fileset_control_dir) == 0:
        control_dir_list.append(swc.SW_A_product)
        control_dir_list.append(swpath_ex.product_control_dir)
        control_dir_list.append(swc.SW_A_fileset)
        control_dir_list.append(swpath_ex.fileset_control_dir)
        object_keyword = swc.SW_A_fileset
    else:
        print("swinstall: Uh-Oh WARNING IN " + fr.__FILE__ + " " + str(fr.__LINE__))
        object_keyword = swc.SW_A_distribution
    return 0

def glbindex_find_by_swpath_ex(global_index, swpath_ex):

    swheader = global_index
    swheader_store_state(swheader, None)
    swheader_set_current_offset(swheader, 0)
    control_dir_list = []
    object_keyword = ""
    determine_object(object_keyword, control_dir_list, swpath_ex)
    next_line = swheader_get_next_object(swheader, UCHAR_MAX, UCHAR_MAX)
    SWLIB_ASSERT(next_line is not None)
    if len(control_dir_list) > 3:
        next_line = search_for_product(next_line, swheader, control_dir_list[1])
        SWLIB_ASSERT(next_line is not None)
        next_line = search_for_fileset(next_line, swheader, control_dir_list[3])
        SWLIB_ASSERT(next_line is not None)
    else:
        next_line = search_for_product(next_line, swheader, control_dir_list[1])
        SWLIB_ASSERT(next_line is not None)
    ret = swheader_get_current_offset(swheader)
    strar_close(control_dir_list)
    strob_close(object_keyword)
    swheader_restore_state(swheader, None)
    return ret



def swheaderline_set_level(outputline, level):
    slev = str(level).zfill(2)
    p = swheaderline_get_type_pointer(outputline)
    if p:
        p[1:3] = slev

def swheaderline_write_debug(line, fd):
    keyw = swheaderline_get_keyword(line)
    value, i = swheaderline_get_value(line)
    level = swheaderline_get_level(line)
    if not len(value):
        ret = swparse_write_attribute_obj(fd, keyw, level, 1)
    else:
        p = value.find('\n')
        if p == -1:
            p = len(value)
        value = value[:p]
        ret = swparse_write_attribute_att(fd, keyw, value, level, 1)
    return ret