# tasks.py
# SD task control of software installation, update, removal
#
# Copyright (C) 2012-2016 Pdub Software, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any Red Hat trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License and may only be used or replicated with the express permission of
# Red Hat, Inc.
#

from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals
from datetime import datetime

import sd
import errno
import functools
import getpass
import logging
import os
import sys
import socket
import pwd
import shutil
import tempfile
import time
from sd.util import readipd
from sd.errors import SW_ERROR,SW_NOTE
print("Entering sd/control/tasks.py")

def bundleinfo(level, swselect):
    print("Entered bundleinfo function")
    findipdindexes(level, 'pfiles/INDEX', SW_CATALOG)
    
def productinfo(level, swdefaults, swselect, swattr, targets, cmd):
    print("Entered productinfo function")
    print("Use these software defaults:")
    print("Swdefaults:")
    print(swdefaults)
    print("Print these software attributes:")
    print(swattr)
    print("Print these Software Selections:")
    print(swselect)
    print(swdefaults['installed_software_catalog'])
    isc=swdefaults['installed_software_catalog']
    if swselect == None:
      print("listing all products")
    else:
      print(type(swselect))
      print(swselect)

      for swsel in swselect:
        swsel = swsel.split(', ')
        swsel = str(swsel).replace('[','').replace(']','').replace('\'','').replace('\"','')
        print(isc)
        print(type(isc))
        print(isc+"/"+swsel)
        swindex=isc+"/"+swsel+"/pfiles/INDEX"
        if os.path.isfile(swindex):
          print(swindex+" file exists")

          # Pass the software selection, attrib, index, and targets
          # to read IPD
          print("")
          print("Print args to readipd function")
          readipd(level, swsel, swattr, swindex, targets)
        else:
          print(SW_NOTE.get(62))

#   for swsel in swselect:
#       prodipd= os.path.isfile()
#       if os.path.isfile(swdefaults_file)  
#     with open(SW_CATALOG+swsel"/pfiles/INDEX", "rt") as ipd:
#       ipddict=dict()
    
def subproductinfo(level, swselect):
    print("Entered subproductinfo function")
    
def filesetinfo(level, swselect):
    print("Entered filesetinfo function")
    
# sw_ tasks definitions, act on installed software

# Install software (swinstall)
# This task takes software from a source distribution and installs it on a 
# target file system in a form suitable to be configured on this system or
# another system sharing this software. Parts of software products 
# (subproducts or filesets) can be installed or reinstalled at different times.
# In the case where the system on which the software is installed will also be 
# using the software (that is, it is acting as both a target and client role),
# configuring the software can be combined with the install software task.

def swinstall_task():
    print("swinstall_task")
    print("Install software:  swinstall")
    return None

# Reinstall software (swinstall)
# This task is simply installing the exact same software that was previously 
# already installed.

def swreinstall_task():
    print("swreinstall_task")
    print("Reinstall software: swinstall")
    return None

# Configure software (swconfig)
# This task takes place on the client role that will be using the installed 
# software. Configuration makes that software ready to use. Configured software 
# can also be reconfigured as required or can be unconfigured (to deactivate a 
# particular version or prepare it for removal).

def swconfig_task():
    print("swconfig_task")
    print("Configure software: swconfig")
    return None

# Update software (swinstall)
# This task updates the target file system by installing a newer revision of 
# software than is already installed. This is also referred to as upgrading.  
# The new revision of software can be installed in the same location as the 
# current revision. In this case, the software configure scripts executed by 
# the configure task need to handle saving or updating the necessary 
# configuration data.
# The new revision of software can alternatively be installed in a location 
# different than the current revision. In this case, the old revision may be 
# unconfigured by the unconfigure script executed as part of the unconfigure 
# task, and the new revision is configured by the configure scripts executed 
# as part of the configure task.

def swupdate_task():
    print("swupdate_task")
    print("Update software: swinstall")
    return None


# Downdate software (swinstall)
# This task ‘‘downdates’’ the target file system by installing an older 
# revision of software than is already installed. This is also referred to as
# ‘‘downgrading’’ or ‘‘reverting.’’
# The older revision of software can be installed in the same location as the 
# current revision.
# In this case, the configuration process of the older version handles the 
# necessary changes in configuration.

# The older revision of software can alternatively be installed in a location 
# different from that of the current revision. In this case, the new revision 
# can be unconfigured via the unconfigure task, and the older revision can be 
# configured either independently, or as part of install.

def swdowngrade_task():
    print("swdowngrade_task")
    print("Calling sw_downdate")
    self.swdowndate_task()
    return None

def swdowndate_task():
    print("swdowndate_task")
    print("Downdate software: swinstall")
    return None

# Recover software (swinstall)
# This task restores the previous version of software (if it exists) in the case 
# where an update, downdate, or reinstall of software fails. This Software 
# Administration specification defines the minimum required support for automatic 
# recovery process in the install task.

def swrecover_task():
    print("swrecover_task")
    print("Recover software: swinstall")
    return None

# Apply software patch (swinstall)
# This task replaces part of a software fileset with a new set of files by 
# installing a fileset with those new files in the same location as the fileset 
# being patched.  This is also referred to as fixing software.
# Updates and patches can be implemented through standard 1387.2 facilities and 
# control scripts, although these control scripts can become quite complex. This 
# Software Administration specification also proposes significant enhancements to
# the standard to facilitate these operations.

def swpatch_task():
    print("swpatch_task")
    print("Apply software patch: swinstall")
    return None

# Remove installed software (swremove)
# This task removes software from an installed_software object where it
# previously was installed. Parts of software products (subproducts or filesets)
# can be removed at different times.

# If the system where the software is installed was also using the software, 
# unconfiguring the software can be combined with the remove software task.

def swremove_task():
    print("swremove_task")
    print("Remove installed software: swremove")
    return None

# Remove software patch (swremove)
# This task removes a patch fileset. This is also referred to as rejecting 
# software.

# Filesets related through naming conventions and prerequisites can be used. 
# Restoring patch files when removing the patched can be achieved via remove 
# control scripts.

def swremove_patch_task():
    print("swremove_patch_task")
    print("Remove software patch: swremove")
    return None

# Verify the installed software (swverify)
# This task checks that software previously installed still exists and is intact.
# If operating on a system that was configured to use the software, it can also 
# check that the software is configured properly.

def swverify_task():
    print("swverify_task")
    print("Verify the installed software: swverify")
    return None

# List installed software information (swlist)
# This task provides a list of the software that has been installed on a target.
# Options are available to specify which software packages are to be listed and to# control the amount of information provided.

def swlist_task(level, swdefaults, swselect, swattr, targets):
    print("Entering sd/control/tasks swlist_task")
    print("List installed software information: swlist")
    productinfo(level, swdefaults, swselect, swattr, targets, 'swlist')
    #readipd(level, swselect, targets)
    print(level)
    return level

# Fix installed software information (swmodify, swverify)
# This task modifies information about software that has been installed on a 
# target. 

# Options are available to specify which software packages, and what information 
# about those packages, are modified.

def swfix_task():
    print("swfix_task")
    print("Fix installed software information:  swmodify, swverify")
    return None

# Package software (swpackage)
# This task takes place in the packager role and transforms developed software 
# into the software packaging layout suitable for distribution. The metadata that
# defines the software objects to be packaged is contained in the product 
# specification file (PSF).

def swpackage_task():
    print("swpackage_task")
    print("Package software:  swpackage")
    return None
# 
# sd_ tasks definitions, act on distribution software (depot)
# sd_copy, sd_remove
# Copy distribution software (swcopy)
# This task copies distribution software between a source and a target, for 
# subsequent use of that target as a source. Copying software can be used to merge# distributions, to 
# distribute products to the installation targets, and then 
# install from that local copy, or to copy part of a distribution to a removable 
# media for physical distribution (as opposed to electronic distribution).

def sdcopy_task():
    print("swcopy_task")
    print("Copy distribution software: swcopy")
    return None

# Remove distribution software (swremove)
# This task removes products from a target distribution.

def sdremove_task():
    print("sdremove_task")
    print("Remove distribution software: swremove")
    return None

# Check/verify distribution software (swverify)
# This task checks that a target distribution exists and is intact.

def sdcheck_task():
    print("sdcheck_task")
    print("Check/verify distribution software: swverify")
    return None

# List distribution information (swlist)
# This task lists source or target distribution information. Options are 
# available to specify which objects in a distribution are to be listed, and to
# control the amount of information provided.

def sdlist_task():
    print("sdlist_task")
    print("List installed software information: swlist")
    swlist_task()
    return None

# Fix distribution information (swmodify, swverify)
# This task modifies information that describes, and is contained within, target 
# distributions.  Options are available to specify which objects in a distribution# are modified.

def sdfix_task():
    print("sdfix_task")
    print("Fix distribution information:  swmodify, swverify")
    return None

# License installed software (undefined)
# How software licenses are managed is undefined within this Software 
# Administration specification.

def sw_license():
    print("sw_license is undefined")
    return None

def sd_license():
    print("sd_license is undefined")
    return None


print("Exiting sd/control/tasks.py")
