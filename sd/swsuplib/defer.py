#  defer.c - handle "defered" links in newc and crc archives
# Copyright (C) 1999 Jim Lowe
# Copyright (C) 2023 Paul Weber Port to Python
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the


from sd.swsuplib.taru.ahs import ahsstaticcreatefilehdr, ahsstaticsetpaxlinkname, ahsstaticsettarfilename, ahsstaticdeletefilehdr
from sd.swsuplib.taru.copyout import taru_write_out_header, taru_filehdr2filehdr, taru_write_archive_member_data
class deferment:
    def __init__(self):
        self.headerP = None
        self.nextP = None

class DEFER:
    def __init__(self, format=None, deferouts=None, taru=None):
        self.format = format
        self.deferouts = deferouts
        self.taru = taru

def defer_set_taru(defer, taru):
    defer.taru = taru

def defer_set_format(defer, format):
    defer.format = format

def defer_is_last_link(defer, file_hdr):
    return defer.deferouts.headerP == file_hdr

def defer_add_link_defer(defer, file_hdr):
    new_deferment = deferment()
    new_deferment.headerP = file_hdr
    new_deferment.nextP = defer.deferouts
    defer.deferouts = new_deferment

def defer_writeout_zero_length_defers(defer, file_hdr, outfd):
    if defer.deferouts is None:
        return 0
    if defer.deferouts.headerP == file_hdr:
        return 0
    return 1

def defer_writeout_final_defers(defer, out_des):
    return 0

class Deferment:
    def __init__(self, headerP=None, nextP=None):
        self.headerP = headerP
        self.nextP = nextP



def def_create_deferment(file_hdr):
    d = Deferment()
    d.headerP = ahsstaticcreatefilehdr()
    taru_filehdr2filehdr(d.headerP, file_hdr)
    ahsstaticsetpaxlinkname(d.headerP, None)
    return d

def def_free_deferment(d):
    ahsstaticsetpaxlinkname(d.headerP, None)
    ahsstaticsettarfilename(d.headerP, None)
    ahsstaticdeletefilehdr(d.headerP)
    del d

def def_count_defered_links_to_dev_ino(def_obj, file_hdr):
    d = def_obj.deferouts
    ino = file_hdr.c_ino
    maj = file_hdr.c_dev_maj
    min = file_hdr.c_dev_min
    count = 0
    while d:
        if d.headerP.c_ino == ino and d.headerP.c_dev_maj == maj and d.headerP.c_dev_min == min:
            count += 1
        d = d.nextP
    return count

def def_writeout_defers(def_obj, file_hdr, out_des, infd, is_final):
    ret = 0
    retval = 0
    d_prev = None
    d = def_obj.deferouts
    ino = file_hdr.c_ino
    maj = file_hdr.c_dev_maj
    min = file_hdr.c_dev_min
    while d:
        if d.headerP.c_ino == ino and d.headerP.c_dev_maj == maj and d.headerP.c_dev_min == min:
            remaining_links = def_count_defered_links_to_dev_ino(def_obj, d.headerP)
            if is_final == 0 or (is_final == 1 and remaining_links > 1):
                d.headerP.c_filesize = 0
                ret = taru_write_out_header(def_obj.taru, d.headerP, out_des, def_obj.format, None, 0)
                retval += ret
            elif is_final == 1 and remaining_links == 1:
                ret = def_writeout_defered_file(def_obj, d.headerP, infd, out_des, def_obj.format)
                retval += ret
            if d_prev:
                d_prev.nextP = d.nextP
            else:
                def_obj.deferouts = d.nextP
            d_free = d
            d = d.nextP
            def_free_deferment(d_free)
        else:
            d_prev = d
            d = d.nextP
    return retval

def def_writeout_defered_file(def_obj, header, infd, outfd, format):
    ret = taru_write_out_header(def_obj.taru, header, outfd, format, None, 0)
    ret += taru_write_archive_member_data(def_obj.taru, header, outfd, infd, None, format, -1, None)
    return ret



