#!      /sbin/sh
########
#  Product: SW-DIST
#  Fileset: SD-CMDS
#  postinstall
#  @(#) $Revision: 5.3 $
########
#
# (C) Copyright 2002-2007 Hewlett-Packard Development Company, L.P.
#
########

# Cleanup
#
# Cleans up files from previous release(s) which are not needed in the current
# release.
#
Cleanup()
{

    ##
    ##  Blow away some files which were shipped with 10.00 coldinstall that
    ##  are not needed in 10.01 and later releases.
    ##
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/Swinstall

    ##
    ##  Remove files from HP-UX 9.0 versions of SD-OV.
    ##
    rm -rf ${ROOT}etc/newconfig/sd/openview
    rm -f ${ROOT}var/adm/sw/ui/*.ui
    rm -f ${ROOT}var/adm/sw/ui/*.h
    
    ##
    ##  Remove obsolete files from pre-10.10 versions of SD
    ##
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_addhost.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_analyze.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_bundles.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_chgloc.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_defs.h
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_depend.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_details.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_dsa.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_init.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_install.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_menudefs.h
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_options.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_remove.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_rmsourc.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_selalt.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_seldpt.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_session.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_softinf.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_softwar.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_source.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_targets.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_task.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_jobs.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_jobopts.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/smc_sched.ui
    rm -f ${SW_ROOT_DIRECTORY}usr/lib/sw/ui/sdfal_list.ui

    ##
    ##  Remove the obsolete swdepot command if it exists.
    ##
    if [ -f ${SW_ROOT_DIRECTORY}usr/sbin/swdepot ]; then
	rm -f ${SW_ROOT_DIRECTORY}usr/sbin/swdepot
    fi

    ##
    ##  Remove files obsoleted at 11.00 if present 
    ##
    rm -f ${SW_ROOT_DIRECTORY}usr/sbin/swcluster
    rm -f ${SW_ROOT_DIRECTORY}usr/newconfig/etc/rc.config.d/swcluster
    rm -f ${SW_ROOT_DIRECTORY}usr/old/usr/newconfig/etc/rc.config.d/swcluster
    rm -f ${SW_ROOT_DIRECTORY}etc/rc.config.d/swcluster
    rm -f ${SW_ROOT_DIRECTORY}sbin/init.d/swcluster
    rm -f ${SW_ROOT_DIRECTORY}sbin/rc2.d/S880swcluster


} # Cleanup()


# dce_whatdate - use what to get the date of a library in YYYYMMDD format
# this works only for libraries created by the DCE team
#
# the relevant line of the what string looks like this:
# 	HP DCE/9000 1.8 PHSS_24261 Module: libdce.2 Date: Jul 24 2001 11:05:16
#
# $1 is the filename of the library
# $(NF-1) is the year
# $(NF-2) is the day
# $(NF-3) is the month
#
# this function is only for use by update_library_links (below)
#
# this function and update_library_links are duplicated in three areas:
# 1. the SD-CMDS postinstall script
# 2. the swconfig startup script (S120swconfig, SD-CMDS/startup)
# 3. future 11.11 patch postinstall scripts (PHCO_28848 and beyond)
#
# dce_whatdate prints a YYYYMMDD string that corresponds to the what date
# of the library specified by $1, or a blank string if $1 does not exist,
# is not a DCE-made library, or is a DCE-made library with an unrecognized
# format of what string.  This condition is checked by the caller.
#
dce_whatdate () {

if [ -e "$1" ]
then
  what $1 | awk '/HP DCE.*/ {

    MONTH = $(NF-3)

    if (MONTH ~ /[jJ]an.*/ ) monthnum="01"
    else if (MONTH ~ /[fF]eb.*/ ) monthnum="02"
    else if (MONTH ~ /[mM]ar.*/ ) monthnum="03"
    else if (MONTH ~ /[aA]pr.*/ ) monthnum="04"
    else if (MONTH ~ /[mM]ay.*/ ) monthnum="05"
    else if (MONTH ~ /[jJ]un.*/ ) monthnum="06"
    else if (MONTH ~ /[jJ]ul.*/ ) monthnum="07"
    else if (MONTH ~ /[aA]ug.*/ ) monthnum="08"
    else if (MONTH ~ /[sS]ep.*/ ) monthnum="09"
    else if (MONTH ~ /[oO]ct.*/ ) monthnum="10"
    else if (MONTH ~ /[nN]ov.*/ ) monthnum="11"
    else if (MONTH ~ /[dD]ec.*/ ) monthnum="12"
    else {
            # the what string format has changed, we cannot get the date
            exit 1
         }
    # make sure that month and day are zero-padded
    printf("%d%.2d%.2d\n", $(NF-1), monthnum, $(NF-2))
    }' # end of awk function
  fi
} #end dce_whatdate


# update_library_links - remove links to SD-delivered libraries if
#   newer ones exist on the system ; put links in if old libraries are
#   on the system.  This is the mechanism used to maintain SD-private
#   copies of libdce.2, libcma.2, and libd4r.1 that are required for
#   versions of SD after Sept. 2003 11.11 OEUR (OEUR_111110.)
#
# This function and dce_whatdate are duplicated in three areas:
# 1. SD-CMDS postinstall script**
# 2. swconfig startup script (S120swconfig, SD-CMDS/startup)
# 3. future 11.11 patch postinstall scripts (PHCO_28848 and beyond)**
#
# ** The postinstall versions of this function have additional handling
#    for rm and ln failures.
#
update_library_links () 
{
  for i in `ls /usr/lib/sw/lib/lib*.sd 2>/dev/null`
  do	
    i=${i#/usr/lib/sw/lib/%.sd}
    system_libdate=`dce_whatdate /usr/lib/$i`
    sd_libdate=`dce_whatdate /usr/lib/sw/lib/$i.sd`
    if [ -z "$system_libdate" ] || [ -z "$sd_libdate" ]
    then
      # we can't compare, the DCE what string format has changed.
      # if this happens, it is safe to assume that the system
      # library is newer than this private copy.
      # if the rm fails and the library exists, move it aside for later cleanup
      rm -f /usr/lib/sw/lib/$i  #remove SD-visible link to private library
      if [ $? -gt 0 ] && [ -e /usr/lib/sw/lib/$i ]
      then
	mv /usr/lib/sw/lib/$i /usr/lib/sw/lib/\#$i 2>/dev/null
	echo "/usr/lib/sw/lib/#$i" >> /var/adm/sw/cleanupfile
      fi
    elif [ $system_libdate -lt $sd_libdate ] || [ ! -e /usr/lib/$i ]
    then
      # our private library is newer; link to it
      # if the ln fails, move the target file aside for later cleanup and
      # try again
      ln -f /usr/lib/sw/lib/$i.sd /usr/lib/sw/lib/$i 2>/dev/null
      if [ $? -gt 0 ]
      then
	mv /usr/lib/sw/lib/$i /usr/lib/sw/lib/\#$i 2>/dev/null
	ln -f /usr/lib/sw/lib/$i.sd /usr/lib/sw/lib/$i 2>/dev/null
	echo "/usr/lib/sw/lib/#$i" >> /var/adm/sw/cleanupfile
      fi
    elif [ -e /usr/lib/sw/lib/$i ]
    then
      # our private library is older but our link exists; remove it
      # if the rm fails, move library aside for later cleanup
      rm -f /usr/lib/sw/lib/$i
      if [ $? -gt 0 ]
      then
	mv /usr/lib/sw/lib/$i /usr/lib/sw/lib/\#$i 2>/dev/null
	echo "/usr/lib/sw/lib/#$i" >> /var/adm/sw/cleanupfile
      fi
    fi
  done
} # end update_library_links

# MAIN

    UTILS=/usr/lbin/sw/control_utils
    if [ ! -f $UTILS ]
    then
        echo "ERROR:   Cannot find $UTILS"
        exit 1
    fi
    . $UTILS

###############################################################################

    exitval=$SUCCESS

##
##   Remove old files which are no longer needed.
##
    Cleanup

#  (JAGad72461) Fix permissions on the /usr/lib/sw/mx directory to avoid
#  swverify warnings.

    chmog 555 bin bin /usr/lib/sw/mx
    if [[ $? -ne 0 ]]
    then
           msg NOTE "Failed to modify attributes for /usr/lib/sw/mx" 
    fi

#
# extract revision from SW_SOFTWARE_SPEC
#
revision=`echo $SW_SOFTWARE_SPEC | sed -e's/.*r=//' | sed -e's/,.*//'`

major=`echo $revision | awk -F. '{print $2}'`
minor=`echo $revision | awk -F. '{print $3}'`

if [[  $major  -lt 11 ]] || ( [[  $major  -eq 11 ]] &&  [[ $minor -lt 23  ]] )
then
 
    #
    # Prior to 11.23, this processing is done by SD-CMDS/postinstall
    # For 11.23 and above, this processing is done by SD2-CMDS/postinstall
    #
    #  If the Update-UX versions of these commands exist on the system
    #  then restore the links to that version.  They were probably removed
    #  by the preinstall script for this fileset - i.e. this is a reinstall
    #  of SW-DIST when Update-UX is already installed.

    if [[ -f /usr/sbin/swinstall ]] && [[ -f /var/adm/sw/sbin/swinstall ]]
	then

	mv /usr/sbin/swinstall  /usr/sbin/swinstall#
	ln -s  /var/adm/sw/sbin/swinstall  /usr/sbin/swinstall

	rm /usr/sbin/swacl
	ln -s  /var/adm/sw/sbin/swacl  /usr/sbin/swacl

	rm /usr/sbin/swconfig
	ln -s  /var/adm/sw/sbin/swconfig  /usr/sbin/swconfig
	
        rm /usr/sbin/swlist
        ln -s  /var/adm/sw/sbin/swlist  /usr/sbin/swlist

	rm /usr/sbin/swreg
	ln -s  /var/adm/sw/sbin/swreg  /usr/sbin/swreg

	rm /usr/sbin/swremove
	ln -s  /var/adm/sw/sbin/swremove  /usr/sbin/swremove

	rm /usr/sbin/swverify
	ln -s  /var/adm/sw/sbin/swverify  /usr/sbin/swverify

	rm /usr/sbin/swjob
	ln -s  /var/adm/sw/sbin/swjob  /usr/sbin/swjob

	rm /usr/sbin/swcopy
	ln -s  /var/adm/sw/sbin/swcopy  /usr/sbin/swcopy

	rm /usr/sbin/sd
	ln -s  /var/adm/sw/sbin/sd  /usr/sbin/sd
    fi

    if [[ -f /usr/sbin/swpackage ]] && [[ -f /var/adm/sw/sbin/swpackage ]]
	then

	mv /usr/sbin/swpackage /usr/sbin/swpackage#
	ln -s /var/adm/sw/sbin/swpackage /usr/sbin/swpackage

	rm /usr/sbin/swmodify
	ln -s /var/adm/sw/sbin/swmodify /usr/sbin/swmodify

    fi
fi

#
# Add hard links to libraries for SD private use
# As of Sept '03 this includes libdce.2, libcma.2 and libd4r.1
#

update_library_links


##
## JAGag32093: Set up our newconfig files.
##
## JAGag36385: Either copy the new (/usr/newconfig) version of
## /etc/rc.config.d/swconfig to its active location or merge
## the change to the active file.
##

newconfig_cp /etc/rc.config.d/swconfig
retval=$?

case  ${retval} in
    0)
        # The active file was overwritten by the newly installed file.
        # No further action is necessary.
        ;;

    1)
        # An error occurred in the newconfig_cp() function.
        exitval=$FAILURE
        ;;

    101)
        # The active file did not need to be overwritten because it is
        # identical to the newly installed file under /usr/newconfig.
        # No further action is necessary.
        ;;

    100 | 102 | 103)
        # For return code 100, the original file delivered by the fileset
        # is not present, but the active file is present so it cannot be
        # determined if the active file has been changed.  The change
        # will be merged into the active file.
        #
        # For return code 102, the active file was not overwritten because
        # it has been modified by the user since its original installation
        # and the previously installed file is identical to the newly
        # installed file under /usr/newconfig.  It normally can be assumed
        # the user has made necessary changes to the active file and the
        # changes should be maintained so no further action should be
        # necessary.  However, because this file used to be incorrectly
        # delivered from this fileset's configure script, this change
        # will not be in the active file if the changed file is installed
        # in a patch at the same time a new version of SW-DIST is installed.
        #
        #     For example:
        #         - The system is installed with the 11i v2 September 2006
        #           Update Release.
        #
        #         - SW-DIST on the system is updated by installing from a
        #           depot that contains both the 11i v2 December 2006 Update
        #           Release and PHCO_36146.
        #
        # The active file will not be updated with the change.  To correct
        # this, the change delivered by this install will be merged into
        # the active file.
        #
        # For return code 103, the original file under /usr/newconfig is
        # present and differs from both the working copy and from the newly
        # installed file under /usr/newconfig.  The change will be merged
        # into the active file.
        #
        # The change to the active file will already exist if SW-DIST
        # is reinstalled.  If the customer modified the active file after
        # the first install, then we don't want to merge the change again.
        # The change will only be merged into the active file if it does
        # not already exist.

        CONFIG_FILE=/etc/rc.config.d/swconfig
        SAVED_CONFIG_FILE=${CONFIG_FILE}.before_merge
        CONFIG_FILE_SETTING="SW_ENABLE_SWAGENTD="

        # See if the change is in the file.
        grep -q ${CONFIG_FILE_SETTING} ${CONFIG_FILE}
        if [[ $? -eq 1 ]]
        then
            # The change needs to be merged.

            # Save the current file.
            cp -f ${CONFIG_FILE} ${SAVED_CONFIG_FILE} > /dev/null 2>&1

            # Merge the change
            print "" >> ${CONFIG_FILE}
            print "########" >> ${CONFIG_FILE}
            print "# SW_ENABLE_SWAGENTD" >> ${CONFIG_FILE}
            print "#" >> ${CONFIG_FILE}
            print "#	1:   	Enable the swagentd daemon." >> ${CONFIG_FILE}
            print "#" >> ${CONFIG_FILE}
            print "#	0:   	Disable the swagentd daemon." >> ${CONFIG_FILE}
            print "#" >> ${CONFIG_FILE}
            print "# Default value is 1." >> ${CONFIG_FILE}
            print "" >> ${CONFIG_FILE}
            print "SW_ENABLE_SWAGENTD=1" >> ${CONFIG_FILE}

	    msg NOTE "The change delivered by this install for the 
                \"$CONFIG_FILE\" configuration file has been 
	        merged into the file.  The contents of the file prior to
                the merge are saved in \"${SAVED_CONFIG_FILE}\"."
        fi
        ;;

    *)
        ;;
esac

    exit $exitval
