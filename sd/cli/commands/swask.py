#!/usr/bin/python3

# swask.py - swask - ask for user responses

# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
import getpass
import socket
cmd = os.path.split(sys.argv[0])[1]
username = getpass.getuser()
hostname = socket.gethostname()

swask_parser = argparse.ArgumentParser(
	usage="""%(prog)s [-v] [-g] [-c catalog] [-C session_file] 
	       [-f software_file] [-s source] [-S session_file] 
	       [-x option=value] [-X option_file] 
	       [software_selections] [-T target_selections]""",
	formatter_class=argparse.RawTextHelpFormatter,
	epilog='''
 Software selections are specified as:
 
     product[.subproduct][.fileset][,version] ...

 where version is either
 
     [r=revision][,a=arch][,v=vendor][,l=location]
     instance_id
 
 Target selections are specified as:
 
     -T [host][:][path] ...
 
''')

swask_parser.add_argument("-v --verbose",metavar='', help="write verbose output to stdout")
swask_parser.add_argument("-c --catalog",metavar='catalog',
		      help="use the exported catalog structure at this path \nas the source of the 'response' files")
swask_parser.add_argument("-C", "--session-file", metavar='session_file',
		      help="save the session to session_file")
swask_parser.add_argument("-f", "--software-file", metavar='software_file',
		      help="read software selections from this file")
swask_parser.add_argument("-s", "--source",metavar='source',
			  help="read the software selections from this source")
swask_parser.add_argument("-S", metavar='session_file',
			  help="read options, software selections, and target \nselections from this file")
# HP SD-UX swask does not support -t target file
# Future implementation for SD-LX
# swask_parser.add_argument("-t", "--target-file", metavar='target_file',
#			  help="read target selections from this file")
swask_parser.add_argument("-x", nargs="+",action='append',metavar='option=value',
			  help="-x option=value, set the option to value")
swask_parser.add_argument("-X", "--option-file",metavar='option_file',
			  help="read option definitions from this file")
swask_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")

swask_parser.parse_args()
args = swask_parser.parse_args()

print("DEBUG: Args in: ", cmd,  args)
print(args)


