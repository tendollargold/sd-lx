
# compression.py
# Basic compression utils.
#
# Copyright (C) 2023 Paul Weber Modified for python3

# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

print("Entering compression.py")
print("")

#
# Dictionary to match the magic number to the compression algorythm
#

magic_dict = {
    b'\x1f\x8b\x08': "gz",
    b'\x42\x5a\x68': "bz2",
    b'\x50\x4b\x03\x04': "zip",
    b'catalog/INDEX\x00\x00': "depot"
    }

max_len = max(len(x) for x in magic_dict)


# Determine if a file is compressed, if uncompressed does it contain
# and SD depot within it

def depot_file_check(filename):
    with open(filename, "rb") as f:
        file_start = f.read(max_len)
    for magic, filetype in magic_dict.items():
        if file_start.startswith(magic):
           if filetype == "depot":
               print(filetype)
               print("filetype is a depot!")
               return filetype

    return False


