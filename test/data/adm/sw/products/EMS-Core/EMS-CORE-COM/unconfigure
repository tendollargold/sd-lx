#!/sbin/sh
# Product: UserResources
# Fileset: EMS-CORE
# configure
#
# (c) Copyright Hewlett-Packard Company 1996
#
###############################################################
# Revision history and tags
# tag        date     patch      description
# -----      -------- ---------- ---------------------------------------------
# JAGae50712 02/12/16 For release Location of temporary and backup files 
#                                 for 'inittab', 'services' and 'inetd.conf' 
#                                 changed from /var/tmp to /etc.
#                                 Here we also make sure that, those temporary 
#                                 and backup files to have appropriate 
#                                 permissions.
# JAGaf78827 05/10/19 For DRD_SAFE program the init should  be called 
#                                only if the SW_SESSION_IS_DRD_SAFE is zero
#                                Also toggle_switch stop should be called 
#                                only if not DRD_SAFE_SESSION
###############################################################################
UTILS="/usr/lbin/sw/control_utils"
if [[ ! -f $UTILS ]]
then
   print "ERROR:   Cannot find $UTILS"
   exit 1
fi

. $UTILS

exitval=$SUCCESS            # Anticipate success

#------------------------------------------------------------------------------
# Remove Sentinel monitors if necessary.
#------------------------------------------------------------------------------

# JAGaf78827 the "toggle_switch stop" should be called only if the 
# SW_SESSION_IS_DRD  is set to 0 
if [[  "$SW_SESSION_IS_DRD" -ne "1" && -x /etc/opt/resmon/lbin/toggle_switch ]]
then
    if [ -x /etc/opt/resmon/lib/libresmon.1 ]
    then
	/etc/opt/resmon/lbin/toggle_switch stop
    else
	echo "WARNING: Could not stop any asynchronous monitors"
	echo "         that might be currently running."
	exitval=$WARNING
    fi
fi

INIT=/sbin/init
########################################################################



#------------------------------------------------------------------------------
# Definitions for the inittab files.
#------------------------------------------------------------------------------
#JAGae50712: 
#Changed the path of backup and temporary files to /etc/ from /var/tmp
INITTAB=/etc/inittab
NEW_INITTAB=/etc/ems_inittab.new
OLD_INITTAB=/etc/ems_inittab.old

#------------------------------------------------------------------------------
# inittab_entries_p checks to see if there are any EMS entries
# in /etc/inittab.  If there are, it returns "TRUE" otherwise
# it returns NULL.
#------------------------------------------------------------------------------
function inittab_entries_p
{
  typeset result
  result="$(awk '
  BEGIN { ems_present = 0; initdefault=0; FS = ":" }
  $3 == "initdefault" { if ( !initdefault ){
                           initdefault = $2  
                        }         
	              }
  /ems1[ \t]*:/       { ems_present = 1 }
  /ems2[ \t]*:/       { ems_present = 1 }
  /ems3[ \t]*:/       { ems_present = 1 }
  /ems4[ \t]*:/       { ems_present = 1 }
  END { if(ems_present == 0){
          if(!initdefault){ initdefault = 3 }
             printf("%s",initdefault);
          } 
      }' $INITTAB)"
  if [[ "$result" != "" ]]; then
    let initdefault="$result"
  else
    let initdefault=0
    echo "TRUE"
  fi
  return $initdefault
}

#------------------------------------------------------------------------------
# inittab_filter writes out the contents of /etc/inittab to STDOUT
# without the EMS entries.
#------------------------------------------------------------------------------
function inittab_filter
{
  awk '
  /^/           { ems_present = 0 }
  /ems1[ \t]*:/ { ems_present = 1 }
  /ems2[ \t]*:/ { ems_present = 1 }
  /ems3[ \t]*:/ { ems_present = 1 }
  /ems4[ \t]*:/ { ems_present = 1 }   
  ems_present == 0     { print $0 } ' $INITTAB 
}

#------------------------------------------------------------------------------
# uninstall_inittab removes the EMS inittab entries from
# /etc/inittab.
#------------------------------------------------------------------------------
function uninstall_inittab
{
  if [[ -f $INITTAB && "$(inittab_entries_p)" != "" ]]; then
    #JAGae50712: Begin
    #remove the old if exists, otherwise 'umask' can't change the 
    #permissions of the file.
    rm -f $NEW_INITTAB
    #Get the current umask value, set it to readonly,
    #and restore umask to previous value after the file creation.
    UmaskValue=`umask`
    umask u=r,g=r,o=r
    #JAGae50712: End

    inittab_filter >$NEW_INITTAB

    #JAGae50712
    umask $UmaskValue

    if [[ -r $NEW_INITTAB ]]; then
      chmod 644 $INITTAB   
#     cp $INITTAB $OLD_INITTAB                              BJH 3/10/00
      cp $NEW_INITTAB $INITTAB 
      chmod 444 $INITTAB 
      rm -f $NEW_INITTAB
      rm -f $OLD_INITTAB                                   #BJH 3/10/00
#     print "NOTE:    The previous version of $INITTAB has been saved to"
#     print "         ${OLD_INITTAB}."                      BJH 3/10/00
    else
      echo "WARNING: Could not remove EMS entries from $INITTAB"
      exitval=$WARNING
    fi
  fi
}



#------------------------------------------------------------------------------
# Definitions for the services files.
#------------------------------------------------------------------------------
#JAGae50712: 
#Changed the path of backup and temporary files to /etc/ from /var/tmp
SERVICES=/etc/services
NEW_SERVICES=/etc/services.new
OLD_SERVICES=/etc/services.old

modified=0

#------------------------------------------------------------------------------
# Make sure the registrar/tcp entry is in the file.  If it is, delete
# the registrar entry line from the file.
#------------------------------------------------------------------------------
if [[ -f $SERVICES ]]
then
#   cp $SERVICES $OLD_SERVICES                               BJH 3/10/00
    grep -E -q "^registrar[    ]+.*tcp" $SERVICES
    if [[ $? = 0 ]]
    then
        #JAGae50712: Begin 
        #remove the old if exists, otherwise 'umask' can't change the 
        #permissions of the file.
        rm -f $NEW_SERVICES 
        #Get the current umask value, set it to readonly,
        #and restore umask to previous value after the file creation.
        UmaskValue=`umask`
        umask u=r,g=r,o=r
        #JAGae50712: End

        grep -E -v "^registrar[    ]+.*tcp" $SERVICES >$NEW_SERVICES 

        #JAGae50712
        umask $UmaskValue

        if [[ $? = 0 ]]
        then
            modified=1
        fi
    fi
fi

#------------------------------------------------------------------------------
# If we haven't modified the file, there is no need to apply the changes to
# the file. 
# If we have modified the file, tell the user where the old one is kept.
#------------------------------------------------------------------------------
if [[ $modified = 1 ]]                  # BJH 3/10/00 Used to check for 0
then
#    rm -f $OLD_SERVICES                # 
#else                                   #
    chmod 644 $SERVICES
    cp $NEW_SERVICES $SERVICES
    chmod 444 $SERVICES
    rm -f $NEW_SERVICES
    rm -f $OLD_SERVICES                 # BJH 3/10/00 
#   print "NOTE:    The previous version of $SERVICES has been saved to"
#   print "         ${OLD_SERVICES}."
fi



#------------------------------------------------------------------------------
# Definitions for the /etc/inetd.conf files
#------------------------------------------------------------------------------
#JAGae50712: 
#Changed the path of backup and temporary files to /etc/ from /var/tmp
INETD_CONF=/etc/inetd.conf
NEW_INETD_CONF=/etc/inetd.conf.new
OLD_INETD_CONF=/etc/inetd.conf.old

modified=0

#------------------------------------------------------------------------------
# Make sure the registrar entry is in the file.  If it is, delete it from the
# file
#------------------------------------------------------------------------------
if [[ -f $INETD_CONF ]]
then
#   cp $INETD_CONF $OLD_INETD_CONF                        # BJH 3/10/00
    grep -E -q "^registrar.*/etc" $INETD_CONF
    if [[ $? = 0 ]]
    then
        #JAGae50712: Begin
        #remove the old if exists, otherwise 'umask' can't change the 
        #permissions of the file.
        rm -f $NEW_INETD_CONF
        #Get the current umask value, set it to readonly,
        #and restore umask to previous value after the file creation.
        UmaskValue=`umask`
        umask u=r,g=r,o=r
        #JAGae50712 : End

        grep -E -v "^registrar.*/etc" $INETD_CONF >$NEW_INETD_CONF

        #JAGae50712
        umask $UmaskValue

        if [[ $? = 0 ]]
        then
            modified=1
        fi
    fi
fi

#------------------------------------------------------------------------------
# If we haven't modified the file, there is no need to apply the changes
# to the file. 
# If we have modified the file, tell the user where the old one is kept.
# Also tell inetd to re-read its config file.
#------------------------------------------------------------------------------
if [[ $modified = 1 ]]                # BJH 3/10/00 Used to check for 0
then
#    rm -f $OLD_INETD_CONF
#else
    chmod 644 $INETD_CONF
    cp $NEW_INETD_CONF $INETD_CONF
    chmod 444 $INETD_CONF
    rm -f $NEW_INETD_CONF
    rm -f $OLD_INETD_CONF            #BJH 3/10/00
     
# JAGag15402 the "inetd c" should be called only if the SESSION_DRD_SAFE 
# is not true
    if [[ $SW_SESSION_IS_DRD -ne 1 ]]
    then
   	 inetd -c
    fi
#   print "NOTE:    The previous version of $INETD_CONF has been saved to"
#   print "         ${OLD_INETD_CONF}."
fi



#------------------------------------------------------------------------------
# Delete resmon paths into the PATH, MANPATH, and SHLIB_PATH files in /etc
#------------------------------------------------------------------------------
mod_pathfile -d P /opt/resmon/bin
mod_pathfile -d MP /opt/resmon/share/man/%L
mod_pathfile -d MP /opt/resmon/share/man
mod_pathfile -d SP /etc/opt/resmon/lib



#------------------------------------------------------------------------------
# Stop the subagent /opt/resmon/bin/emsagent
#------------------------------------------------------------------------------
if [[ $SW_ROOT_DIRECTORY = "/" ]]
then
    kill_named_procs emsagent SIGKILL
    retval=$?
    if [[ $retval -ne 0 ]]
    then
            echo "WARNING: Could not kill subagent \"emsagent\" process"
            exitval=$WARNING
    fi
fi



#------------------------------------------------------------------------------
# Stop the persistence client.  Need to do this in 2 steps.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Step 1:  Delete the entries in the inittab file first so that the persistence
#          client will not be relaunched after we terminate it in step 2.
#------------------------------------------------------------------------------
uninstall_inittab

#------------------------------------------------------------------------------
# Tell init to reread the inittab file.
#------------------------------------------------------------------------------
# JAGaf78827 the "init q" should be called only if the SESSION_DRD_SAFE 
# is not true
if [[ -z "${SW_SESSION_IS_DRD}" ]]
then
 /sbin/init q
fi

#------------------------------------------------------------------------------
# Step 2:  Terminate the p_client
#------------------------------------------------------------------------------
if [[ $SW_ROOT_DIRECTORY = "/" ]]
then
    kill_named_procs p_client SIGKILL
    retval=$?
    if [[ $retval -ne 0 ]]
    then
            echo "WARNING: Could not kill p_client process"
            exitval=$WARNING
    fi
fi

#------------------------------------------------------------------------------
# Remove the files that have been created by EMS
#------------------------------------------------------------------------------
rm -f /etc/opt/resmon/log/*
rm -f /etc/opt/resmon/lock/*
rm -f /etc/opt/resmon/persistence/*
rm -f /etc/opt/resmon/pipe/*

return $exitval
