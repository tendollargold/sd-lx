
# phases.swcopy.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

from sd.cli.demand import bool

"""
    4.0 Execution Phase Systems Management: Distributed Software Administration
    wcopy utility Page 79 and 80
    
    Extended Options

    The swcopy utility supports the following extended options. The description in 
    Chapter 3 applies.
"""
swcopyoptions={
    'autoselect_dependencies': "as_needed",
    'autoselect_patches': bool(True),
    'compress_files': bool(False),
    'compression_type': "gzip", #
    'distribution_source_directory': "/SD_CDROM",
    'distribution_target_directory': "/var/spool/sw",
    'enforce_dependencies': bool(True),
    'enforce_dsa': bool(True),
    'logfile': "/var/adm/sw/swcopy.log",
    'loglevel': "1",
    'patch_filter': "*",
    'patch_match_target': bool(False),
    'recopy': bool(False),
    'reinstall_files': bool(False),
    'reinstall_files_use_cksum': bool(True),
    'select_local': bool(True),
    'software': "",
    'targets': "",
    'uncompress_files': bool(False),
    'verbose': "1"
}
# compression type options
compression_type={
  '': "0",
  'gzip': "1",
  'bzip2': "2",
  'xz': "3"
}
def swcopy_options():
    """Collection of extended options for swinstall utility. :api"""
    # implementation_defined_value is set via the setting in swdefaults?
    swcopyoptions.update(globals())

# There are three key phases in the copy utility:
#   1. Selection phase
#   2. Analysis phase
#   3. Execution phase

#
# Selection Phase
#

# If a software selection has dependency specifications on other software and
# autoselect_dependencies=true, this software is automatically selected using the 
# method described in Section 3.4.1 on page 38. The resulting selection must be 
# unambiguous. This automatically selected software is then copied along with the 
# rest of the selected software. 

# If autoselect_dependencies=if_needed, then automatically selected software is only 
# copied if the dependency is not already met.

# The swcopy utility supports corequisite and prerequisite dependencies for 
# autoselection, but treats them equally (no ordering considerations like swinstall).
# The swcopy utility ignores exrequisites.

# For swcopy each selection added to the selected software list satisfies the 
# following validation checks. If any of these checks result in an error, the 
# selection is not added to the list.

#   · If the selection is not available from the source, generate an event.
#     (SW_ERROR: SW_SELECTION_NOT_FOUND)

#   · If a unique version can not be identified, generate an event.
#     *SW_ERROR: SW_SELECTION_NOT_FOUND_AMBIG)

# No compatibility filtering is done for swcopy.

# In general, usage of the patch_filter and patch_match_target options for copying 
# patches corresponds exactly to their use in swinstall. One difference is that 
# the ‘‘highest superseding’’ rule is not used; instead, all levels of patches are 
# included.

#
# Analysis Phase
#

# The target role uses the file size information obtained from the source to 
# determine whether or not the copy utility proceeds on the given target. If any 
# target fails any of the analysis operations, what software is attempted to be copied
# is determined by the implementation defined error handling procedures.

# The target role checks the following requirements:
#   · If the target distribution does not exist on a host, create the path to the 
#     target with default attributes and generate an event.
#     (SW_NOTE: SW_SOC_CREATED)

#   · Check that selected filesets are not the same version as already available. If 
#     recopy=false, note that these filesets will be skipped by generating an event.
#     (SW_NOTE: SW_SAME_REVISION_SKIPPED)

#     If recopy=true, note that they will be recopied by generating an event.
#     (SW_NOTE: SW_SAME_REVISION_INSTALLED)

#   · Verify that the needed dependencies of the filesets are met. If 
#     enforce_dependencies=true, generate an event.
#     (SW_ERROR: SW_DEPENDENCY_NOT_MET)

#     If enforce_dependencies=false", generate an event.
#     (SW_WARNING: SW_DEPENDENCY_NOT_MET)

#   · Check that there is enough free disk space on the target file system to copy 
#     the selected products. If there is not enough disk space and enforce_dsa=true, 
#     generate an event.
#     (SW_ERROR: SW_DSA_OVER_LIMIT)

#     If there is not enough disk space and enforce_dsa=false , generate an event.
#     (SW_WARNING: SW_DSA_OVER_LIMIT)

# How disk space analysis is implemented is undefined. However an implementation must
# account at least for the sizes of the files and control_files being copied.

#
# Execution Phase
#

# As the result of a swcopy, products and bundles, if specified, are copied to the 
# target, which is a distribution software object.

# When creating serial distributions, an implementation must support one or both of 
# the POSIX.1 extended cpio or extended tar archive formats. Whether an implementation
# supports writing both archive formats or only one, and which of the formats is 
# supported if only one, is implementation defined.

# The relationship between the fileset loading and state transitions for swcopy is 
# shown in the following list.

# Copy each product:
#   1. Create the distribution catalog information for the product and its contained
#      subproducts.

#   2. Copy each fileset in the product:
#        a. Create the distribution catalog information for the fileset, setting the 
#           state to transient.

#        b. Load the files for the fileset.

#        c. Update the state of the fileset to available .

# Copy each bundle:
#   1. Create the distribution catalog information for the bundle.

# A depot update is defined as ‘‘copying or packaging a higher revision of a fileset 
# than one that currently exists within a particular product revision in a depot’’.

# When the new revision of the fileset has completed the copy, and the state of the 
# fileset is set to available, also remove the catalog information for the existing 
# fileset (or set the state of the existing fileset to removed), and remove any files
# that were contained in the old fileset but are not contained in the new fileset. 
# See step 2c above.

# For disk space analysis during a copy update, ensure that files that will be removed
# from a depot during update are accounted for, in addition to the normal analysis for
# new files and files being replaced.

#  · File Loading
#    In this step, swcopy copies the files from the source onto the target.

#    If a file load fails for any other reason such as a lost connection to the 
#    remote source or tape eject, then the fileset load fails. The following are the 
#    errors that can occur during the file loading step:

#    — If an error or warning occurs while loading a file onto a target, an event is 
#      generated and, for errors, the implementation defined error handling procedures
#      invoked.

#      Whether remote files are loaded is implementation defined. If the file is on a 
#      remote file system generate an event if it was loaded.
#      (SW_NOTE: SW_FILE_IS_REMOTE)

#      Generate an event if it was not loaded.
#      (SW_WARNING: SW_FILE_IS_REMOTE)

#    — If the error was a source access problem, the user may attempt to correct the 
#      problem and start over with the fileset that had the failure.
#      (SW_ERROR: SW_SOURCE_ACCESS_ERROR)

# Like install, the reinstall_files option, by default set to true, allows for 
# administrator control of recopying up to date files independent of a recopy or an 
# update. If set to true, files are copied independent of whether they are the same. 
# If set to false, then they are checked first.

# Like install, the cksum check can take as long as actually transferring and 
# recopying the file.  The reinstall_files_use_cksum option, by default set to true, 
# can be set to false to skip the checksum check.

# Copying a sparse fileset into a depot has the same behavior as copying a normal file
# set into a depot. If the fileset has a different fileset revision but the same 
# product revision as a fileset that is already in the depot, it replaces that 
# fileset. If the fileset has a different product revision, then the copy task will 
# result in both product revisions, and both filesets, residing in the depot.

# · Compression
#   If compress_files=true is specified, then the files must be compressed as follows 
#   in copying to the target distribution:

#     — INDEX and INFO files must not be compressed

#     — All files that have the compression_state attribute undefined or its value 
#       set to uncompressed should be compressed. If the file cannot be compressed, 
#       generate an event:
#       (SW_ERROR: SW_COMPRESSION_FAILURE)

#     — All files that have the value of the compression_state attribute set to 
#       not_compressible must be copied without change.

#     — A source file which is already compressed, and which has the value of its 
#       compression_type attribute equal to the value of the compression_type extended
#       option, should be copied without change.

#     — If a source file is already compressed, and the value of its compression_type 
#       attribute is different to the value of the compression_type extended option, 
#       the behavior is undefined.
#       Unless the implementation can successfully uncompress the file and then 
#       compress it with the correct type, generate an event:
#       (SW_ERROR: SW_COMPRESSION_FAILURE)

# If uncompress_files=true, the files must be uncompressed as follows in copying to 
# the target distribution:

#     — All files with a compression_state attribute value of compressed must be 
#       uncompressed as part of the copy. If the file cannot be uncompressed, generate
#       an event:
#       (SW_ERROR: SW_COMPRESSION_FAILURE)

#     — All other files should be copied without change.

#  If neither compress_files=true nor 2uncompress_files=true", then the files are 
#  copied without change. Behavior when both are set to true is undefined.

# · Copying into Existing Products

#   When a fileset is copied into an existing target product, the attributes of this 
#   existing product may be affected as follows. If an attribute exists in the product
#   from which the fileset came, the value of this attribute is set in the target 
#   product. Any attributes in the target product not found in the product from which 
#   the fileset came are left unaltered. It is possible for attributes to be set 
#   multiple times as filesets from different products are copied into the target
#   product.
