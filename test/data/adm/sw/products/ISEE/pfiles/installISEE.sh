#! /sbin/sh 
##########################################################################
# installISEE.sh 
# version 1.0
##########################################################################
# Shell script that installs ISEE Client in a mass deployment mode
#
#
##########################################################################
PATH=$PATH:/usr/bin:/usr/sbin
export PATH


###########################
# Check for available space
#
##########################
checkSpace()
{
### checking disk space requirements
TMP_REQD=10240
TMP_SIZE=`bdf /tmp | grep dev | tr -s ' ' | cut -f 4 -d " "`

OPT_REQD=102400
OPT_SIZE=`bdf /opt | grep dev | tr -s ' ' | cut -f 4 -d " "`

echo "Checking disk space requirements.."
echo "Available space in /tmp: " $TMP_SIZE "bytes"

if [ $TMP_SIZE -lt $TMP_REQD ]
then
 echo "WARNING: Disk space in /tmp is less than 10 MB. Increase the free space and re-execute the script..!\n"
 echo "Exiting...."
 exit 1
else
 echo "Available space in /tmp is OK.\n\n"
fi

echo "Available space in /opt: " $OPT_SIZE "bytes"
if [ $OPT_SIZE -lt $OPT_REQD ]
then
 echo "FAIL! Disk space in /opt is less than 100 MB. Increase the free space and re-execute the script..!\n" 
 echo "Exiting....\n"
 exit 1
else
 echo "Available space in /opt is OK\n\n"
fi
}

########################################
# Check OVFN and prompt to 
# deinstall it before proceeding further
########################################
checkOVFN()
{
### checking if OVFN (HAO) is still installed
echo "Checking if OVFN (HAO) is installed"

if [ $OVFN_FOUND -ne 0 ]
then
 echo "FAIL! The OVFN Client (HAO) is still installed. Please remove OVFN CLIENT before installing ISEE!\n" 
 echo "Exiting....."
 exit 1
else
 echo "Found that OVFN (HAO) is not installed\n\n"
fi
}

######################
# Check for OnlineDiag
#
######################
checkOnlineDiag()
{

DIAG_1100="B.11.00.19.00"
DIAG_1111="B.11.11.05.00"

### checking OnlineDiag version
eval DIAG_OS=\$DIAG_${OS_SHORT}
echo "Checking OnlineDiag version"
echo "Minimum: " $DIAG_OS
echo "Found:   " $DIAG_FOUND
DIAG_FOUND_SHORT=`echo $DIAG_FOUND | cut -c 9-10`
DIAG_OS_SHORT=`echo $DIAG_OS | cut -c 9-10`
if [[ $DIAG_FOUND_SHORT < $DIAG_OS_SHORT ]]
then
 echo "FAIL! DIAG VERSION MISSING OR TOO OLD!!!\n"
 echo "Exiting....."
 exit 1
else
 echo "Found OnlineDiag and is OK\n\n"
fi
}

####################
# Check for JAVA
#
####################
checkJava()
{

# Java information
JRE_VERSIONS="1.4 1.3 1.2"
	
### checking Java version
if [ "$TYPE" = "ia64" ]
then

echo "Java Runtime version"
FOUND="0"

for JRE in $JRE_VERSIONS
do
 if [ "$FOUND" = "0" ]
 then
  FOUND=`egrep -i "jre|rte" $FILE | grep -i java | grep $JRE | wc -l`
  FOUNDXX=`egrep -i "jre|rte" $FILE | grep -i java | grep $JRE`
  JRE_FOUND=$JRE
  echo $FOUNDXX
 fi
done 

if [ "$FOUND" = "0" ]
then
 echo "FAIL! JAVA RUNTIME NOT FOUND! Install Java Runtime before installaing ISEE.\n"
 echo "Exiting..."
 exit 1
else
 echo "Found JRE version " $JRE_FOUND "\n\n"
fi

fi
}

#############################
# Check for patches
#
############################

checkPatches()
{
PRESENT=0
PATCHLIST1=""

if [ -n "$PATCHLIST" ]; then
 PATCHLIST1=`echo $PATCHLIST | sed 's/:/  /g'`
fi

### checking all patches
if [ -n "$PATCHLIST1" -a "$PATCHLIST1" != "PATCHLIST" ];then
  echo "Checking Patch level"
  for PATCH in $PATCHLIST1
   do
    echo "Verifying $PATCH"
    PRESENT=`/usr/sbin/swlist -l fileset -a supersedes | grep $PATCH | wc -l`
     if [ $PRESENT -eq 0 ]; then
             echo "$PATCH (or superseding) is NOT present"
             echo "Install $PATCH before installing ISEE Client. Exiting...\n"
             exit 1
     else
             echo "$PATCH (or superseding) is present.\n"
     fi
   done
else
    echo "No patches are specified for verification..\n"
fi

echo "Patch verification completed successfully. \n"

}


###########################
#checking connectivity
#
############################
checkConnect()
{

if [ -z "$ISEEDC_HOST" -o "$ISEEDC_HOST" = "ISEEDC_HOST" ]; then
  echo "ISEE Datacenter Host name is mandatory but found that it is not specified properly!!"
  echo "Re-execute the script by specifying the datacenter name.Exiting....\n"
  exit 1
fi

echo "Testing connection to the SPOP/ISEE Backend (may take up to 90 seconds)"


if [ -n "$PROXY_PORT" -a "$PROXY_PORT" != "PROXY_PORT" ]; then
  if [ -n "$PROXY_HOST" -a "$PROXY_HOST" != "PROXY_HOST" ]; then
   CONN=`echo "GET http://$ISEEDC_HOST/motivedocs/wait.html" | telnet $PROXY_HOST $PROXY_PORT 2>&- | grep "HP Instant" | wc -l`
  else
   echo "Web-Proxy Host name and port are rerquired to connect to ISEE Backend but found that they are not specified properly."
   echo "Re-execute the script by specified Web-Proxy Host name and port. Exiting....\n"
   exit 1
  fi
else
   CONN=`echo "GET /motivedocs/wait.html" | telnet $ISEEDC_HOST 80 | grep "HP Instant" | wc -l`
fi

if [ $CONN -ge 1 ]
then
 echo "Connection to $ISEEDC_HOST is OK\n\n"
else
 echo "WARNING: No connection to the SPOP/ISEE Backend or proxy needs authentication\n"
 echo "Enable connectivity to SPOP/ISEE Backend before installing ISEE. Exiting...\n"
 exit 1
fi
}


####################################
# Install Client from the depots
#
####################################
installClient()
{
### installation tasks

if [ "$TYPE" = "ia64" ]; then
  if [ -n "$DEPOT_IA64" -a "$DEPOT_IA64" = "DEPOT_IA64" ]; then
    echo "ISEE Client pre-configured depot location is mandatory but found that it is not specified properly!!"
    echo "Re-execute the script by specifying the location of pre-configured depot for IA64.Exiting...."
    exit 1
  fi
else
  if [ -n "$DEPOT_PARISC" -a "$DEPOT_PARISC" = "DEPOT_PARISC" ]; then
    echo "ISEE Client pre-configured depot location is mandatory but found that it is not specified!!"
    echo "Re-execute the script by specifying the location of pre-configured depot for PA-RISC.Exiting...."
    exit 1
  fi
fi

 echo "All required dependencies were found. Installing ISEE in 10 seconds. To abort, press CTRL-C now."
 sleep 10
 echo "Executing swinstall... (may take some minutes)"

 if [[ "$TYPE" = "ia64" ]]
 then
  ## IA 64 swinstall
  /usr/sbin/swinstall -x autoselect_patches=false -x autoselect_dependencies=false -x autoreboot=false -x enforce_dependencies=false -x mount_all_filesystems=false -s $DEPOT_IA64 ISEEPlatform 1> $LOGFILE 2>&1

  if [ $? -ne 0 ]
  then
   echo "\n\nERROR! swinstall failed!!! See " $LOGFILE "for details\n"
   exit 1
  fi 

 else
  ## PA RISC swinstall
  /usr/sbin/swinstall -x autoselect_patches=false -x autoselect_dependencies=false -x autoreboot=false -x enforce_dependencies=false -x mount_all_filesystems=false -s $DEPOT_PARISC ISEEPlatform 1> $LOGFILE 2>&1

 if [ $? -ne 0 ]
  then
   echo "\n\nERROR! swinstall failed!!! See " $LOGFILE "for details\n"
   exit 1
  fi 
 fi
}


#####################
#  Check New Installation incident is created or not
#
######################
checkNewInstall()
{

COUNT=0
NEWINST=0
EXIT=0
## checking if a new_installation incident is being generated (loops for max 1 hour)
 echo "Waiting for new_installation incident (may take up to 1 hour)"
 while [ $EXIT -eq 0 ]
 do
  NEWINST=`grep New_Installation /opt/hpservices/log/iseeInit.log | wc -l`
  COUNT=`expr $COUNT + 1`
  if [ $NEWINST -ge 1 ] ; then
   EXIT=1
  fi
  if [ $COUNT -ge 720 ] ; then
   EXIT=1
  fi
  sleep 5
 done

 if [ $NEWINST -ge 1 ]
 then
  echo "OK: New_installation incident found!"
 else
  echo "WARNING! No New_installation incident occurred within 1 hour!!!\n\n"
  echo "Open Client UI through browser and verify configuration options..."
 fi
}


#######################
# Send an EMS test Event
#
#######################
checkEMSEvent()
{
 ## sending EMS test event and check if an ISEE incident is being created 
 echo "\nSleeping 60 seconds to allow time to clean up..."
 sleep 60
 echo "\nSending EMS test event...\n"
 /etc/opt/resmon/lbin/send_test_event sysstat_em
 if [ $? -ne 0 ] ; then
  echo "Sending the EMS test event failed...review EMS settings!!! \n\n"
 else
  echo "Successfully sent an EMS test event..! \n\n" 
 fi

 WARNING=0
 
# echo "Verifying the EMS test incident in ISEE Client (takes few minutes)\n"
 sleep 180
 INCIDENT=`grep ISEESubmitIncident /opt/hpservices/log/submitIncident.log | grep sysstat_em | grep SUCCESS | wc -l`
 if [ $INCIDENT -ne 0 ]
 then
  echo "EMS Test incident reached ISEE successfully. ISEE is working properly on this system.\n"
 else
  echo "ERROR! EMS Test incident not reached ISEE....!\n"
  WARNING=1
 fi

 ## exiting installation routine
 if [ $WARNING -eq 0 ]
  then
   echo "Installation Phase succeeded without any warnings.\n"
  else
   echo "!!!WARNING! The installation phase had warnings. Please refer to the logfiles\n\n"
   exit 1
 fi
}

#######################
# Display usage of the script
#
#######################

displayUsage()
{
  echo ""
  echo "Usage of the script:" 
  echo "	Execute " $0 "<full-path-of-configuration-file> : to check pre-requisites only."
  echo "	Execute	" $0 "<full-path-of-configuration-file> -i : to check pre-requisites and install the Client."

}
#################################### M A I N ####################################

INSTALL=0

PATCHLIST=""
ISEEDC_HOST=""
DEPOT_PARISC=""
DEPOT_IA64=""
PROXY_HOST=""
PROXY_PORT=""
CONFIGFILE=""
TMPCONFIG=/tmp/installConfig$$

### parse command line
if [ $# -eq 0 ]
then
 displayUsage
 exit 1
elif [ $# -eq 1 ] 
then
  CONFIGFILE=$1
  if [ ! -f $CONFIGFILE ]; then
    echo "The specified configuration file does not exist!!"
    echo "Please specify proper configuration file and re-execute the script."
    exit 1
  fi
  # Below line is to handle CSV configuration file, which replace ,'s with either = or :
  # in lines that does not contanin '#'.
  if [ -f $TMPCONFIG ]; then 
   rm $TMPCONFIG > /dev/null
  fi
  cat $CONFIGFILE | sed -e '/\#/!s/,/=/' -e '/\#/!s/,/:/g' > $TMPCONFIG
  INSTALL=0
else
 if [ $2 = "-i" ]
 then
  CONFIGFILE=$1
  # Below line is to handle CSV configuration file, which replace ,'s with either = or :
  # in lines that does not contanin '#'.
  if [ -f $TMPCONFIG ]; then 
   rm $TMPCONFIG > /dev/null
  fi
  cat $CONFIGFILE | sed -e '/\#/!s/,/=/' -e '/\#/!s/,/:/g' > $TMPCONFIG
  INSTALL=1
 else
  displayUsage
  exit 1
 fi
fi

FILE=`pwd`/swlist.out
### get information from system
if [ -f $FILE ]; then
  rm $FILE > /dev/null
fi
/usr/sbin/swlist -l product > $FILE

### display generic system info

uname -a | read OSNAME HOST OS OTHER TYPE others
HOSTNAME=`hostname`
OS_SHORT=`echo $OS | cut -c 3-4,6-7`
DIAG_FOUND=`grep "Sup-Tool-Mgr" $FILE | cut -f 3 | cut -f 1 -d " " `
OVFN_FOUND=`egrep -i "OVFN-CLIENT" $FILE | wc -l`
echo "\nHostname:   " $HOSTNAME
echo "Operating System:  " $OS "\n\n"

LOGFILE=`pwd`/isee_swinstall_$HOSTNAME.out
if [ -f $LOGFILE ]; then 
  rm $LOGFILE > /dev/null
fi

if [ -f $TMPCONFIG ]; then 
  PATCHLIST=`grep PATCHLIST $TMPCONFIG | grep -v "#" | cut -d "=" -f2 | sed 's/ //g'`
  ISEEDC_HOST=`grep ISEEDC_HOST $TMPCONFIG | grep -v "#" | cut -d "=" -f2 | sed 's/ //g'`
  DEPOT_PARISC=`grep DEPOT_PARISC $TMPCONFIG | grep -v "#" | cut -d "=" -f2 | sed 's/ //g'`
  DEPOT_IA64=`grep DEPOT_IA64 $TMPCONFIG | grep -v "#" | cut -d "=" -f2 | sed 's/ //g'`
  PROXY_HOST=`grep PROXY_HOST $TMPCONFIG | grep -v "#" | cut -d "=" -f2 | sed 's/ //g'`
  PROXY_PORT=`grep PROXY_PORT $TMPCONFIG | grep -v "#" | cut -d "=" -f2 | sed 's/ //g'`
  rm $TMPCONFIG > /dev/null
fi


## perform pre-install checks

checkSpace
checkOVFN
checkOnlineDiag
checkJava
checkPatches
checkConnect

if [ $INSTALL -ne 1 ]; then
    echo "Not selected to install the Client and hence exiting the script.....\n" 
    exit 0  
 fi

echo "All required dependencies were found and ready to install ISEE Client.\n"

# install the client
installClient

## re-starting ISEE processes, in case they do not get started after installation
 echo "\nHP ISEE is installed successfully, restarting ISEE processes (takes 30 seconds)\n"
 sleep 20
 /sbin/init.d/hpservices stop
 sleep 5
 /sbin/init.d/hpservices start

# perform post install tasks
 
checkNewInstall
checkEMSEvent

