# sd.swsuplib.cplob.py: character pointer list object.
# Includes vplob  void pointer list object

# Copyright (C) 1997 James H. Lowe, Jr. <jhlowe@acm.org>
# Copyright (C) 2023-2024 Conversion to Python Paul Weber <paul@weber.net>
#
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.



CPLOB_NINITLENGTH = 16
CPLOB_NLENGTHINCR = 8

class CPLOB:
    def __init__(self):
        self.list = []
        self.nlen = 0
        self.nused = 0
        self.width = 0
        self.refcount = 0



def cplob_open(nobj):
    lob = CPLOB()
    if lob is None:
        return None
    if nobj <= 0:
        nobj = CPLOB_NINITLENGTH
    lob.nlen = nobj
    lob.list = [] * nobj
    lob.width = len(lob.list[0])
    lob.refcount = 0
    cplob_shallow_reset(lob)
    return lob

def cplob_shallow_close(lob):
    if lob is not None:
        try:
            del lob
        except NameError:
            lob = None
            return lob

def cplob_shallow_reset(lob):
    lob.nused = 0
    lob.list = []

def cplob_close(lob):
    lob.cplob_freeall()
    del lob.list
    del lob

def cplob_release(lob):
    p = lob.list
    del lob
    return p

def cplob_add_nta(lob, addr):
    if lob.nused == 0:
        lob.nused += 1
        lob.cplob_reopen(lob.nused + CPLOB_NLENGTHINCR)
        lob.list[lob.nused-1] = addr
        if not addr:
            return
    else:
        lob.cplob_reopen(lob.nused + CPLOB_NLENGTHINCR)
    lob.list[lob.nused-1] = addr
    lob.cplob_additem(lob.nused, None)

def cplob_add(lob, addr):
    lob.cplob_additem(lob.nused, addr)

def cplob_additem(lob, index, addr):
    if index + 1 > lob.nlen:
        lob.cplob_reopen(index + 1 + CPLOB_NLENGTHINCR)
    lob.list[index] = addr
    if index + 1 > lob.nused:
        lob.nused = index + 1
    return

def cplob_val(lob, index):
    if index < 0:
        return None
    if index > lob.nused:
        return None
    return lob.list[index]

def cplob_freeall(lob):
    for i in range(lob.nused):
        if lob.list[i] is not None:
            del lob.list[i]
            lob.list[i] = None

def cplob_backfill_and_nullterminate(lob):
    i = 0
    while i < lob.nused:
        while i < lob.nused and lob.list[i] is None:
            if i + 1 == lob.nused:
                break
            lob.list[i:] = lob.list[i+1:]
            lob.nused -= 1
        i += 1
    if lob.list[lob.nused] is not None:
        lob.cplob_add(None)
    return 0

def cplob_get_list(lob):
    return lob.list

def cplob_get_nused(lob):
    return lob.nused

def cplob_set_nused(lob, n):
    lob.nused = n

def cplob_remove_index(lob, i):
    if i < 0 or i >= lob.nused:
        return -1
    if lob.nused == 0 or lob.nlen == 0:
        return 0
    lob.list[i:] = lob.list[i+1:]
    lob.nused -= 1
    lob.nlen -= 1
    return 0

def cplob_reopen(lob, new_nobj):
# wAS: def cplob_reopen(new_nobj, lob):
    if new_nobj <= 1:
        new_nobj = 2
    if new_nobj > lob.nlen:
        lob.list = lob.list + [None] * (new_nobj - lob.nlen)
        lob.nlen = new_nobj
    return lob

def vplob_open():
    return VPLOB()

class VPLOB:
    def __init__(self):
        self.cplob = CPLOB()



def vplob_close(vplob):
    cplob = vplob.cplob
    cplob_freeall(cplob)
    del cplob.list
    del cplob

def vplob_shallow_close(vplob):
    cplob = vplob.cplob
    cplob.list = []
    cplob.nlen = 0
    cplob.nused = 0
    cplob.width = 0
    cplob.refcount = 0

def vplob_add(vplob, addr):
    cplob = vplob.cplob
    cplob_add_nta(cplob, addr)

def vplob_get_list(vplob):
    cplob = vplob.cplob
    return cplob.list

def vplob_val(vplob, index):
    cplob = vplob.cplob
    return cplob_val(cplob, index)

def vplob_get_nstore(vplob):
    cplob = vplob.cplob
    ret = 0
    for i in range(cplob.nused):
        if cplob.list[i] is not None:
            ret += 1
    return ret

def vplob_delete_store(vplob, f_delete):
    cplob = vplob.cplob
    n = vplob.vplob_get_nstore()
    for i in range(n):
        addr = cplob.list[i]
        if addr is not None:
            f_delete(addr)
    return 0

def vplob_remove_from_list(vplob, addr_to_remove):
    cplob = vplob.cplob
    n = vplob.vplob_get_nstore()
    for i in range(n):
        addr = cplob.list[i]
        if addr == addr_to_remove:
            cplob_remove_index(cplob, i)
    return 0

def null_list(lob, start, stop):
    for i in range(start, stop):
        lob.list[i] = None



