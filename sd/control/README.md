

Control scripts allow vendors to perform tasks and operations, in addition to those 
that the tasks perform. The swinstall, swverify, and swremove utilities may each 
execute one or more vendorsupplied scripts. The presence of these scripts in the 
distribution is optional. Vendors of software to be installed need only provide 
those scripts that meet a particular need of the software.

The identifier of the control_file.
All control files are loaded and maintained within the distribution and installed 
software catalogs by the utilities defined in this Software Administration 
specification. These utilities execute control scripts with particular tags at 
various steps in the execution of the utility.  The values for the control_file 
tag attribute for which this Software Administration specification defines 
behavior are as follows:

  request
  response
  checkinstall
  preinstall
  postinstall
  unpreinstall
  unpostinstall
  verify
  fix
  checkremove
  preremove
  postremove
  configure
  unconfigure
  space

