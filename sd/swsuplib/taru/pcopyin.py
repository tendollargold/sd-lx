# pcopyin.c - cpio2tar translator function
#
#   Copyright (C) 1998, 1999  Jim Lowe
#   Copyright (C) 2023-2024 Paul Weber, conversion to Python
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  

import os
import sys

from sd.debug import E_DEBUG2, DEBUG_CPIO
from sd.swsuplib.misc.cstrings import strcmp, strncmp
from sd.swsuplib.taru.hllist import hllist_open, hllist_close, hllist_add_record, hllist_find_file_entry
from sd.swsuplib.taru.ahs import ahsstaticdeletefilehdr, ahsstaticcreatefilehdr, ahsstaticsettarfilename, ahsstaticgettarfilename, ahsstaticsetpaxlinkname, ahsstaticgettarlinkname
from sd.swsuplib.taru.copyout import taru_write_archive_member_header, taru_tape_pad_output
from sd.swsuplib.taru.otar import taru_tape_skip_padding
from sd.swsuplib.taru.taru import af, taru_create, taru_delete
from sd.swsuplib.taru.readinheader import taru_tape_buffered_read, taru_pump_amount2, taru_read_in_header, taru_read_amount, taru_read_header
from sd.swsuplib.taru.filetypes import ft
from sd.swsuplib.uxfio import uxfio_fcntl, uc, uxfio_opendup, uxfio_unix_safe_write, uxfio_close
from sd.swsuplib.uinfile.uinfile import uinc, uinfile_opendup

HPUX_CDF = False


def delete_trio(f1, f2, f3):
    ahsstaticdeletefilehdr(f1)
    ahsstaticdeletefilehdr(f2)
    ahsstaticdeletefilehdr(f3)


def file_hdr_copy(dst, src):

    dst.c_magic = src.c_magic
    dst.c_ino = src.c_ino
    dst.c_mode = src.c_mode
    dst.c_uid = src.c_uid
    dst.c_gid = src.c_gid
    dst.c_nlink = src.c_nlink
    dst.c_mtime = src.c_mtime
    dst.c_filesize = src.c_filesize
    dst.c_dev_maj = src.c_dev_maj
    dst.c_dev_min = src.c_dev_min
    dst.c_rdev_maj = src.c_rdev_maj
    dst.c_rdev_min = src.c_rdev_min
    dst.c_namesize = src.c_namesize
    dst.c_chksum = src.c_chksum

    ahsstaticsettarfilename(dst, ahsstaticgettarfilename(src))
    ahsstaticsetpaxlinkname(dst, ahsstaticgettarlinkname(src))
    return dst

# read any format on input_fd, write a tar archive on output_fd
def taru_process_copy_in(fp_taru, i_fd, output_fd):
    done = False # True if trailer reached.
    magbuf = str(['\0' for _ in range(10)])
    file_hdr = ahsstaticcreatefilehdr() # Output header information.
    file_hdr_links = ahsstaticcreatefilehdr()
    file_hdr_links_last = ahsstaticcreatefilehdr()
    uinfile = 0
    convert_flags = 0
    zeros_512 = [0] * 512
    # times = [0] * 2
    link_record = ""
    end_of_archive = 0

    if fp_taru is None:
        taru = taru_create()
    else:
        taru = fp_taru

    # Initialize the copy in.

    ahsstaticsettarfilename(file_hdr, None)
    ahsstaticsettarfilename(file_hdr_links, None)
    ahsstaticsettarfilename(file_hdr_links_last, None)
    ahsstaticsetpaxlinkname(file_hdr, None)
    ahsstaticsetpaxlinkname(file_hdr_links, None)
    ahsstaticsetpaxlinkname(file_hdr_links_last, None)


    E_DEBUG2("taru->taru_tarheaderflagsM=%d", taru.taru_tarheaderflagsM)


    convert_flags |= uinc.UINFILE_DETECT_FORCEUNIXFD
    convert_flags |= uinc.UINFILE_DETECT_NATIVE

    # Initialize this in case it has members we don't know to set.
    # While there is more input in the collection, process the input.

    if (input_fd := uinfile_opendup(i_fd, 0, uinfile, convert_flags)) < 0:
        delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
        return -1
    uxfio_fcntl(input_fd, uc.UXFIO_F_SET_BUFACTIVE, 0)
    # translate type to the archive_format enumeration
    if uinfile.real == uinc.CPIO_CRC_FILEFORMAT:
        archive_format_in = af.arf_crcascii
    elif uinfile.real == uinc.CPIO_NEWC_FILEFORMAT:
        archive_format_in = af.arf_newascii
    elif uinfile.real == uinc.CPIO_POSIX_FILEFORMAT:
        archive_format_in = af.arf_oldascii
    elif uinfile.real == uinc.USTAR_FILEFORMAT:
        archive_format_in = af.arf_ustar
    else:
        ahsstaticdeletefilehdr(file_hdr)
        ahsstaticdeletefilehdr(file_hdr_links)
        ahsstaticdeletefilehdr(file_hdr_links_last)
        return -1

    if archive_format_in is not af.arf_ustar:
        link_record = hllist_open()
    while (not done) != '\0':
        ahsstaticsettarfilename(file_hdr, None)
        # link_name = None

        # Start processing the next file by reading the header.
        if taru_read_in_header(taru, file_hdr, input_fd, archive_format_in, end_of_archive, 0) < 0:
            print("error returned by taru_read_in_header")
            return -1

        if DEBUG_CPIO:
            h = file_hdr
            sys.stderr.write("magic = 0%o, ino = %d, mode = 0%o, uid = %d, gid = %d\n"%h.c_magic %h.c_ino % h.c_mode % h.c_uid  %h.c_gid)
            sys.stderr.write("nlink = %d, mtime = %d, filesize = %d, dev_maj = 0x%x\n"% h.c_nlink % h.c_mtime % h.c_filesize % h.c_dev_maj)
            sys.stderr.write("dev_min = 0x%x, rdev_maj = 0x%x, rdev_min = 0x%x, namesize = %d\n"% h.c_dev_min% h.c_rdev_maj% h.c_rdev_min% h.c_namesize)
            sys.stderr.write("chksum = %d, name = \"%s\", tar_linkname = \"%s\"\n"% h.c_chksum% ahsstaticgettarfilename(h)% ahsstaticgettarlinkname(h))


        E_DEBUG2("processing PATH [%s]", ahsstaticgettarfilename(file_hdr))
        E_DEBUG2("processing with LINKPATH [%s]", ahsstaticgettarlinkname(file_hdr))

        # Is this the header for the TRAILER file?
        if strcmp("TRAILER!!!", ahsstaticgettarfilename(file_hdr)) == 0:
            break
        skip_file = False
        if skip_file != '\0':
            sys.exit(4)
        else:
            archive_format_tar = af.arf_ustar
            # Copy the input file into the directory structure.

            # See if the file already exists.
            # existing_dir = 1 if False else 0

            if archive_format_in is not af.arf_ustar and archive_format_in is not af.arf_tar:

                if file_hdr.c_nlink > 1 and (((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFREG) or ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFBLK) or ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFCHR) or ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFSOCK)):

                    # in cpio format, the last entry holds the data, so read ahead
                    # and get it
                    hllist_add_record(link_record, ahsstaticgettarfilename(file_hdr), devno := os.makedev(file_hdr.c_dev_maj, file_hdr.c_dev_min), file_hdr.c_ino)

                    if input_fd < uc.UXFIO_FD_MIN:
                        if (input_fd := uxfio_opendup(input_fd, uc.UXFIO_BUFTYPE_DYNAMIC_MEM)) < 0:
                            delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                            return -1
                    uxfio_fcntl(input_fd, uc.UXFIO_F_ARM_AUTO_DISABLE, 0)
                    uxfio_fcntl(input_fd, uc.UXFIO_F_SET_BUFACTIVE, 1)
                    uxfio_fcntl(input_fd, uc.UXFIO_F_SET_BUFTYPE, uc.UXFIO_BUFTYPE_DYNAMIC_MEM)
                    orig_offset = os.lseek(input_fd, 0, os.SEEK_CUR)
                    # determine number of entries encountered so far
                    nfound = 0
                    # link_record_buf = hllist_find_file_entry(link_record, devno, file_hdr.c_ino, -1, nfound)

                    if nfound == 1 and (((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFREG) or ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFBLK) or ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFCHR) or ((file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFSOCK)):

                        # first occurrence, therefore must read ahead in the cpio archive
                        # to get the data, which cpio stores in the last entry and tar stores
                        # in the first entry.

                        nmore = file_hdr.c_nlink - 1 # must now go and find nmore occurances of the file
                        #										   in the cpio archive because thats where the data is.
                        data_offset = orig_offset
                        data_size = int(file_hdr.c_filesize)
                        count = 0
                        found_data = 0
                        done_links = False
                        # must check for data here
                        if data_size != 0:
                            done_links = True
                            found_data = 1
                        while (not done_links) != '\0':
                            taru_read_header(taru, file_hdr_links, input_fd, archive_format_in, end_of_archive, 0)

                            if strcmp("TRAILER!!!", ahsstaticgettarfilename(file_hdr_links)) == 0:
                                # done_links = True
                                break
                            if file_hdr_links.c_ino == file_hdr.c_ino and devno is os.makedev(file_hdr_links.c_dev_maj, file_hdr_links.c_dev_min):
                                # LINK FOUND
                                count += 1
                                file_hdr_copy(file_hdr_links_last, file_hdr_links)
                                data_offset = os.lseek(input_fd, 0, os.SEEK_CUR)
                                data_size = file_hdr_links.c_filesize
                                if (file_hdr_links.c_filesize > 0) or count == nmore:
                                    found_data = 1
                                    # done_links = True
                                    break

                            if ((file_hdr_links.c_mode & ft.CP_IFMT) == ft.CP_IFREG) and file_hdr_links.c_filesize:
                                if os.lseek(input_fd, file_hdr_links.c_filesize, os.SEEK_CUR) < 0:
                                    sys.stderr.write("error from uxfio_lseek 100a.1.")
                                    delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                                    return -1
                                taru_tape_skip_padding(input_fd, file_hdr_links.c_filesize, archive_format_in) # while


                        if found_data == 0 and count != 0:
                            # could be a archive that didn't store all the links to a
                            # zero length file, this a legitimate case
                            # the header is in file_hdr_links_last and the data is at data_offset

                            file_hdr_copy(file_hdr_links, file_hdr_links_last)
                            if os.lseek(input_fd, data_offset, os.SEEK_SET) < 0:
                                sys.stderr.write("taru_process_copy_in, read ahead failed, lseek")
                                delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                                return -1
                        elif found_data == 0 and count == 0:
                            # first and only occurrence of link, the data must be there

                            file_hdr_copy(file_hdr_links, file_hdr_links_last)
                            if os.lseek(input_fd, orig_offset, os.SEEK_SET) < 0:
                                sys.stderr.write("taru_process_copy_in, read ahead failed, lseek")
                                delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                                return -1
                        elif found_data != 0:
                            # file pointer at the data already
                            file_hdr_copy(file_hdr_links, file_hdr)
                        file_hdr.c_filesize = data_size
                        # taru_write_out_tar_header2(taru, file_hdr, output_fd, (char *)(NULL), (char *)(NULL), (char *)(NULL), 0);
                        taru_write_archive_member_header(taru, None, file_hdr, None, None, 0, output_fd, af.arf_ustar, None, taru.taru_tarheaderflagsM)
                        taru_pump_amount2(output_fd, input_fd, data_size, -1)
                        taru_tape_skip_padding(input_fd, data_size, archive_format_in)
                        taru_tape_pad_output(output_fd, data_size, archive_format_tar)
                        # just store a link in the tar file, already stored the file
                    else:
                        nfound = 0
                        link_record_buf = hllist_find_file_entry(link_record, devno, file_hdr.c_ino, 1, nfound)
                        ahsstaticsetpaxlinkname(file_hdr, link_record_buf.path_)
                        # taru_write_out_tar_header2(taru, file_hdr, output_fd, (char *)(NULL), (char *)(NULL), (char *)(NULL), 0);
                        taru_write_archive_member_header(taru, None, file_hdr, None, None, 0, output_fd, af.arf_ustar, None, taru.taru_tarheaderflagsM)

                    if os.lseek(input_fd, orig_offset, os.SEEK_SET) < 0:
                        sys.stderr.write("taru_process_copy_in, read ahead failed, lseek")
                        delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                        return -1
                    # skip over the data if any, there shouldn't be any except for the linked last entry
                    # but some versions of cpio left data here, so we really don't know yet if there is
                    # data to skip over

                    if taru_tape_buffered_read(input_fd, magbuf, 6) != 6:
                        sys.stderr.write("taru_tape_buffered_read error in taru_process copy out")
                        delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                        return -1
                    os.lseek(input_fd, orig_offset, os.SEEK_SET)

                    if (not strncmp(magbuf, "070701", 6)) or (not strncmp(magbuf, "070702", 6)) or (not strncmp(magbuf, "070707", 6)):

                        if taru_read_in_header(taru, file_hdr_links, input_fd, archive_format_in, end_of_archive, 0) < 0:
                            sys.stderr.write("cpio header magic probably match accidentally, attempting to recover.")
                            if os.lseek(input_fd, orig_offset, os.SEEK_SET):
                                sys.stderr.write("lseek failed\n")
                                delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                                return -1
                            taru_read_amount(input_fd, file_hdr.c_filesize)
                            # tape_toss_input (input_fd, file_hdr->c_filesize);
                            taru_tape_skip_padding(input_fd, file_hdr.c_filesize, archive_format_in)
                            taru_tape_buffered_read(input_fd, magbuf, 6)
                            if (not strncmp(magbuf, "070701", 6)) or (not strncmp(magbuf, "070702", 6)):
                                # Ok
                                os.lseek(input_fd, -6, os.SEEK_CUR)
                        else:
                            if os.lseek(input_fd, orig_offset, os.SEEK_SET) < 0:
                                sys.stderr.write("taru_process_copy_in, read ahead failed, lseek\n")
                                delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
                                return -1
                    else:
                        taru_read_amount(input_fd, file_hdr.c_filesize)
                        taru_tape_skip_padding(input_fd, file_hdr.c_filesize, archive_format_in)
                    uxfio_fcntl(input_fd, uc.UXFIO_F_ARM_AUTO_DISABLE, 1)
                    continue
                elif file_hdr.c_nlink > 1 and (file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFLNK:
                    sys.stderr.write("linked soft link, HEEEE exiting..\n")
                    sys.exit(1)
                elif file_hdr.c_nlink > 1 and (file_hdr.c_mode & ft.CP_IFMT) != ft.CP_IFDIR:
                    sys.stderr.write("something else linked. EEEEE exiting..\n")
                    sys.exit(1) # end of handling links
            # Do the real copy or link.
            if (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFREG) != 0:
                # taru_write_out_tar_header2(taru, file_hdr, output_fd, (char *)(NULL), (char *)(NULL), (char *)(NULL), 0);
                taru_write_archive_member_header(taru, None, file_hdr, None, None, 0, output_fd, af.arf_ustar, None, taru.taru_tarheaderflagsM)
                taru_pump_amount2(output_fd, input_fd, int(file_hdr.c_filesize), -1)
                taru_tape_skip_padding(input_fd, file_hdr.c_filesize, archive_format_in)
                taru_tape_pad_output(output_fd, file_hdr.c_filesize, archive_format_tar)

            elif (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFLNK) != 0:
                link_name = None
                if archive_format_in is not af.arf_tar and archive_format_in is not af.arf_ustar:
                    link_name = [0] * (file_hdr.c_filesize + 1)
                    # link_name[file_hdr.c_filesize] = '\0'
                    taru_tape_buffered_read(input_fd, link_name, file_hdr.c_filesize)
                    # tape_buffered_read (link_name, input_fd, file_hdr->c_filesize);
                    taru_tape_skip_padding(input_fd, file_hdr.c_filesize, archive_format_in)
                    ahsstaticsetpaxlinkname(file_hdr, link_name)
                # taru_write_out_tar_header2(taru, file_hdr, output_fd, (char *)(NULL), (char *)(NULL), (char *)(NULL), 0);
                taru_write_archive_member_header(taru, None, file_hdr, None, None, 0, output_fd, af.arf_ustar, None, taru.taru_tarheaderflagsM)
                if link_name != '\0':
                    del link_name
                    # link_name = None

            elif (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFDIR) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFCHR) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFBLK) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFSOCK) or (file_hdr.c_mode & ft.CP_IFMT == ft.CP_IFIFO):
                # taru_write_out_tar_header2(taru, file_hdr, output_fd, (char *)(NULL), (char *)(NULL), (char *)(NULL), 0);
                taru_write_archive_member_header(taru, None, file_hdr, None, None, 0, output_fd, af.arf_ustar, None, taru.taru_tarheaderflagsM)

            else:
                print("%s: unknown file type" % ahsstaticgettarfilename(file_hdr))
                taru_read_amount(input_fd, file_hdr.c_filesize)
                taru_tape_skip_padding(input_fd, file_hdr.c_filesize, archive_format_in)

    uxfio_unix_safe_write(output_fd, zeros_512, 512)
    uxfio_unix_safe_write(output_fd, zeros_512, 512)


    if archive_format_in is not af.arf_ustar:
        hllist_close(link_record)
    uxfio_close(input_fd)
    delete_trio(file_hdr, file_hdr_links, file_hdr_links_last)
    if fp_taru is None:
        taru_delete(taru)
    return 0
