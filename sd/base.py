# Copyright 2005 Duke University
# Copyright (C) 2012-2018 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""
Supplies the Base class.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# import argparse
import sd

# from sd.util import _parse_specs
# from sd.job.history import SwdbInterface

import sd.conf
import sd.logging


#logger = logging.getLogger("sd")


class Base(object):

    def __init__(self, conf=None):
    #    self._conf = conf or self._setup_default_conf()
        self._transaction = None
        self._priv_ts = None
        self._comps = None
        self._history = None
        self._jobs = set()
        self._tempfiles = set()
    #    self._logging = sd.logging.Logging()
    #    self._depots = sd.depotdict.DepoDict()
        self.output = None

    def __enter__(self):
        return self

    def __exit__(self, *exc_args):
        return self
    # self.close()

    def __del__(self):
        return self
    # self.close()

    def _add_tempfiles(self, files):
        return self, files
    # if self._transaction:
    #        self._trans_tempfiles.update(files)
    #    elif self.conf.destdir:
    #        pass
    #    else:
    #        self._tempfiles.update(files)

