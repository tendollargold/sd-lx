#!/sbin/sh

# %W% %G% %U% - %Q%
#ident "%Z%isis:%M% %I%"

# $Copyright: Copyright (c) 2001 - 2004 VERITAS Software Corporation.
# All rights reserved.
# VERITAS, the VERITAS Logo and all other VERITAS product names and
# slogans are trademarks or registered trademarks of VERITAS Software
# Corporation. VERITAS and the VERITAS Logo Reg. U.S. Pat. & Tm. Off.
# Other product names and/or slogans mentioned herein may be trademarks or
# registered trademarks of their respective companies.
#
# UNPUBLISHED - RIGHTS RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED
# STATES.  USE OF A COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT
# IMPLY PUBLICATION OR DISCLOSURE.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
# VERITAS SOFTWARE.  USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT
# THE PRIOR EXPRESS WRITTEN PERMISSION OF VERITAS SOFTWARE.
#
# The Licensed Software and Documentation are deemed to be "commercial
# computer software" and "commercial computer software documentation" as
# defined in FAR Sections 12.212 and DFARS Section 227.7202.  $

UTILS=/usr/lbin/sw/control_utils

if [ ! -f $UTILS ]
then
   echo "ERROR:   control_utils not found."
   exit 1
#   PATH=${SW_PATH%:}:/usr/sbin:/usr/lbin/sw
else
   . $UTILS
fi

# Perform postremove processing for the ISIS package

rm /sbin/rc1.d/K999isisd 2>/dev/null
rm /sbin/rc2.d/S999isisd 2>/dev/null
rm /sbin/init.d/isisd 2>/dev/null
rm /etc/rc.config.d/veaservice 2>/dev/null
rm /usr/lib/libvxapi.sl 2>/dev/null
rm /usr/lib/vxicudt20b.dat 2>/dev/null

# links for executable 

rm /opt/VRTS/bin/vxalerttype  2>/dev/null
rm /opt/VRTS/bin/vxobjecttype 2>/dev/null
rm /opt/VRTS/bin/vxinterface 2>/dev/null
rm /opt/VRTS/bin/vxgetinterfaceinfo 2>/dev/null
rm /opt/VRTS/bin/vxfinger 2>/dev/null
rm /opt/VRTS/bin/vxsecaz 2>/dev/null
rm /opt/VRTS/bin/vxregctl     2>/dev/null
rm /opt/VRTS/bin/vxsvc        2>/dev/null
rm /opt/VRTS/bin/vxsvcctrl    2>/dev/null
rm /opt/VRTS/bin/vxguidgen    2>/dev/null
rmdir /opt/VRTS/bin 2>/dev/null

# links for libraries

rm /opt/VRTS/lib/libvxapi.sl 2>/dev/null
rm /opt/VRTS/lib/vxicudt20b.dat 2>/dev/null
rmdir /opt/VRTS/lib 2>/dev/null

######################################
# Removing /opt/VRTS/man from $MANPATH
######################################
mod_pathfile -d MP /opt/VRTS/man

# man pages

rm /opt/VRTS/man/man1m/vxalerttype.1m 2>/dev/null
rm /opt/VRTS/man/man1m/vxobjecttype.1m 2>/dev/null
rm /opt/VRTS/man/man1m/vxregctl.1m 2>/dev/null
rm /opt/VRTS/man/man1m/vxsvc.1m 2>/dev/null
rmdir /opt/VRTS/man/man1m 2>/dev/null
rmdir /opt/VRTS/man/ 2>/dev/null
rmdir /opt/VRTS 2>/dev/null

# temp fix for docs
#rm -rf /opt/VRTSob/docs

# remove copyright
rm /opt/VRTSob/copyright.veas 2>/dev/null

#logs
rm /opt/VRTSob/log/vxisis.log 2>/dev/null
rm /opt/VRTSob/log/alert 2>/dev/null
rmdir /opt/VRTSob/log 2>/dev/null

#bins
rm /opt/VRTSob/bin/vxregctl 2>/dev/null
rm /opt/VRTSob/bin/vxsvc 2>/dev/null
rm /opt/VRTSob/bin/vxobjecttype 2>/dev/null
rm /opt/VRTSob/bin/vxalerttype 2>/dev/null
rm /opt/VRTSob/bin/vxinterface 2>/dev/null
rm /opt/VRTSob/bin/vxfinger 2>/dev/null
rm /opt/VRTSob/bin/vxgetinterfaceinfo 2>/dev/null
rm /opt/VRTSob/bin/vxsecaz 2>/dev/null
rm /opt/VRTSob/bin/vxsvcctrl   2>/dev/null
rm /opt/VRTSob/bin/vxguidgen   2>/dev/null
rmdir /opt/VRTSob/bin 2>/dev/null

#lib
rm /opt/VRTSob/lib/libvxapi.sl 2>/dev/null
rm /opt/VRTSob/lib/vxicudt20b.dat 2>/dev/null
rmdir /opt/VRTSob/lib/cat 2>/dev/null
rmdir /opt/VRTSob/lib 2>/dev/null

#providers
rm /opt/VRTSob/providers/alertlog/alertlog.sl 2>/dev/null
rm /opt/VRTSob/providers/alertlog/alerror.1033.cat 2>/dev/null
rm /opt/VRTSob/providers/controlPanel/controlpanel.sl 2>/dev/null
rm /opt/VRTSob/providers/genericlogger/libgenericlogger.sl 2>/dev/null
rmdir /opt/VRTSob/providers/genericlogger 2>/dev/null
rmdir /opt/VRTSob/providers/alertlog 2>/dev/null
rmdir /opt/VRTSob/providers/controlPanel 2>/dev/null
rmdir /opt/VRTSob/providers/ 2>/dev/null

#extensions
rm -rf /opt/VRTSob/extensions/alertlog/* 2>/dev/null
rm -rf /opt/VRTSob/extensions/controlPanel/* 2>/dev/null
rmdir /opt/VRTSob/extensions/alertlog 2>/dev/null
rmdir /opt/VRTSob/extensions/controlPanel 2>/dev/null
rmdir /opt/VRTSob/extensions 2>/dev/null

rm -rf /opt/VRTSob/extensions/GlobalObject/* 2>/dev/null
rm -rf /opt/VRTSob/extensions/System/* 2>/dev/null
rmdir /opt/VRTSob/extensions/GlobalObject 2>/dev/null
rmdir /opt/VRTSob/extensions/System 2>/dev/null

rm -rf /opt/VRTSob/extensions/ObguiPrint/* 2>/dev/null
rm -rf /opt/VRTSob/extensions/ObguiGraphics/* 2>/dev/null
rm -rf /opt/VRTSob/extensions/PrintController/* 2>/dev/null
rm -rf /opt/VRTSob/extensions/genericlogger/* 2>/dev/null

rmdir /opt/VRTSob/extensions/ObguiPrint 2>/dev/null
rmdir /opt/VRTSob/extensions/ObguiGraphics 2>/dev/null
rmdir /opt/VRTSob/extensions/PrintController 2>/dev/null
rmdir /opt/VRTSob/extensions/genericlogger 2>/dev/null

rm -rf /opt/VRTSob/extensions/umi/* 2>/dev/null
rm -rf /opt/VRTSob/extensions/AdvancedPrint/* 2>/dev/null
rmdir /opt/VRTSob/extensions/AdvancedPrint 2>/dev/null
rmdir /opt/VRTSob/extensions/umi 2>/dev/null

rmdir /opt/VRTSob/extensions 2>/dev/null

#config
rm /opt/VRTSob/config/Registry 2>/dev/null
rm /opt/VRTSob/config/master.alerttypes 2>/dev/null
rm /opt/VRTSob/config/master.interfaceform 2>/dev/null
rm /opt/VRTSob/config/master.securityform 2>/dev/null
rm /opt/VRTSob/config/master.objecttypes 2>/dev/null
rmdir /opt/VRTSob/config 2>/dev/null

#msgcats
rm -rf /opt/VRTSob/msgcats/system 2>/dev/null
rm -rf /opt/VRTSob/msgcats/cli 2>/dev/null
rmdir /opt/VRTSob/msgcats 2>/dev/null
rmdir /opt/VRTSob 2>/dev/null

rm /etc/vx/isis/Registry 2>/dev/null
rm /etc/vx/isis/alerttypes.dtd 2>/dev/null
rm /etc/vx/isis/alerttypes.xml 2>/dev/null
rm /etc/vx/isis/objecttypes.dtd 2>/dev/null
rm /etc/vx/isis/objecttypes.xml 2>/dev/null
rm /etc/vx/isis/interfaceform.dtd 2>/dev/null
rm /etc/vx/isis/interfaceform.xml 2>/dev/null
rm /etc/vx/isis/securityform.dtd 2>/dev/null
rm /etc/vx/isis/securityform.xml 2>/dev/null
rmdir /etc/vx/isis 2>/dev/null 

rm -Rf /var/vx/isis 2>/dev/null

exit 0
