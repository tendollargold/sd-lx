#	$OpenBSD: progressmeter.h,v 1.1 2003/01/10 08:19:07 fgsch Exp $

#
#  MODIFIED by jhlowe for swbis as noted below. 2003-11-20
#
# Copyright (c) 2002 Nils Nordman.  All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import time
import os
# import fcntl
import signal
# import termios


global start, last_update, of_start, file, end_pos, cur_pos, counter, stalled, g_ofd, win_size
class ProgressConst:
    start = time.time()
    of_start = time.time()
    last_update = time.time()
    file = ""
    end_pos = 0
    overcount = 0
    over_pos = 0
    cur_pos = 0
    counter = []
    stalled = 0
    bytes_per_second = 0
    win_size = 0
    oldcur_speed = 0
    avg_speed = 0
    g_ofd = 1
    DEFAULT_WINSIZE = 80
    MAX_WINSIZE = 512
    PADDING = 1
    UPDATE_INTERVAL = 1
    STALL_TIME = 3
    unit = " KMGT"

pcc = ProgressConst()

def can_output():
    return os.getpgrp() == os.tcgetpgrp(g_ofd)

def format_rate(buf, size, bytes_):
    i = 0
    while bytes_ >= 10000 and pcc.unit[i] != 'T':
        bytes_ //= 1024
        i += 1
    dp = int(bytes_)
    # buf = "{:5}{}{}".format(dp, pcc.unit[i], "B" if i else "B")
    return f"{dp:5}{pcc.unit[i]}B"

def format_size(buf, size, bytes_):
    i = 0
    j = 0
    # obytes = 0
    if pcc.over_pos:
        obytes = pcc.over_pos
        while obytes >= 10000 and pcc.unit[j] != 'T':
            obytes = obytes / 1024
            j += 1
        for i in range(j):
            bytes_ = bytes_ / 1024
    else:
        obytes = 0
        while bytes_ >= 10000 and pcc.unit[i] != 'T':
            bytes_ = bytes_ / 1024
            i += 1
    bytes_ += obytes
    dp = int(bytes_)
    buf = "{:4}{}{}".format(dp, pcc.unit[i], "B" if i else "B")
    return buf

def set_progress_meter_fd(fd):
    global g_ofd
    g_ofd = fd

def get_progress_meter_fd():
    return g_ofd

def refresh_progress_meter():
    buf = [""] * (pcc.MAX_WINSIZE + 1)
    now = time.time()
    if pcc.counter[0] < pcc.cur_pos:
        over_pos = pcc.cur_pos
        pcc.overcount += 1
        pcc.of_start = now
    transferred = pcc.counter[0]
    newtransferred = pcc.counter[0] - pcc.cur_pos
    cur_pos = pcc.counter[0]
    newelapsed = now - pcc.last_update
    elapsed = now - pcc.start
    of_elapsed = now - pcc.of_start
    if newelapsed < 1 or of_elapsed < 1:
        cur_speed = pcc.oldcur_speed
        avg_speed = cur_speed
    else:
        cur_speed = newtransferred / newelapsed
        avg_speed = transferred / of_elapsed
    oldcur_speed = cur_speed
    bytes_per_second = cur_speed
    buf[0] = ''
    file_len = pcc.win_size

    if file_len > 0:
        length = len(buf)
        if length < 0:
            length = 0
        for i in range(length, file_len):
            buf[i] = ' '
        buf[file_len] = ''
    file_len = pcc.win_size - 54
    buf[file_len] = ''
    if pcc.end_pos != 0:
        percent = int((float(cur_pos) / pcc.end_pos) * 100)
        buf.append(" {}% ".format(percent))
    else:
        # percent = 100
        buf.append(" ---% ")
    format_size(buf, pcc.win_size - len(buf), cur_pos)
    buf += " @ "
    format_rate(buf, pcc.win_size - len(buf), avg_speed)
    buf.append("/s[Avg]  ")
    if bytes_per_second < 10:
        pcc.stalled += 1
    else:
        stalled = 0
    if pcc.stalled >= pcc.STALL_TIME:
        buf.append("- stalled -")
    else:
        format_rate(buf, pcc.win_size - len(buf), bytes_per_second)
        buf.append("/s ")
    seconds = int(elapsed)
    hours = seconds // 3600
    seconds -= hours * 3600
    minutes = seconds // 60
    seconds -= minutes * 60
    if hours != 0:
        buf.append("{}:{:02d}:{:02d}".format(hours, minutes, seconds))
    else:
        buf.append("  {:02d}:{:02d}".format(minutes, seconds))
    buf.append("    ")
    os.write(g_ofd, g_ofd.writelines(buf))
    # last_update = now

def update_progress_meter(ignore):
    save_errno = os.error
    if can_output():
        refresh_progress_meter()
    # signal.signal(signal.SIGALRM, update_progress_meter)
    signal.alarm(pcc.UPDATE_INTERVAL)
    errno = save_errno

def start_progress_meter(ofd, f, filesize, stat):
    global start, last_update, of_start, file, end_pos, cur_pos, counter, stalled, g_ofd, win_size
    # winsize = fcntl.ioctl(ofd, termios.TIOCGWINSZ, '12345678')
    start = last_update = time.time()
    of_start = start
    file = f
    end_pos = filesize
    cur_pos = 0
    counter = stat
    stalled = 0
    g_ofd = ofd
    try:
        winsize = os.get_terminal_size(g_ofd)
        if winsize.columns != 0:
            if winsize.columns > pcc.MAX_WINSIZE:
                win_size = pcc.MAX_WINSIZE
            else:
                win_size = winsize.columns
    except OSError:
        win_size = 80
    win_size += 1
    if can_output():
        refresh_progress_meter()
    signal.signal(signal.SIGALRM, update_progress_meter(None))
    signal.alarm(pcc.UPDATE_INTERVAL)

def stop_progress_meter():
    signal.alarm(0)
    if not can_output():
        return
    if pcc.cur_pos != pcc.end_pos or pcc.end_pos == 0:
        refresh_progress_meter()
    os.write(g_ofd, b"\n")
