#! /sbin/sh
# $Id: checkremove,v 1.9 2001/03/14 13:12:17 agraham Exp $
#ident "$Source: /project/unixvm-cvs/src/hp/package/vxvm/pkg_scripts/checkremove,v $"

# Copyright (c) 2001 VERITAS Software Corporation.  ALL RIGHTS RESERVED.
# UNPUBLISHED -- RIGHTS RESERVED UNDER THE COPYRIGHT
# LAWS OF THE UNITED STATES.  USE OF A COPYRIGHT NOTICE
# IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
# OR DISCLOSURE.
# 
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND
# TRADE SECRETS OF VERITAS SOFTWARE.  USE, DISCLOSURE,
# OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR
# EXPRESS WRITTEN PERMISSION OF VERITAS SOFTWARE.
# 
#               RESTRICTED RIGHTS LEGEND
# USE, DUPLICATION, OR DISCLOSURE BY THE GOVERNMENT IS
# SUBJECT TO RESTRICTIONS AS SET FORTH IN SUBPARAGRAPH
# (C) (1) (ii) OF THE RIGHTS IN TECHNICAL DATA AND
# COMPUTER SOFTWARE CLAUSE AT DFARS 252.227-7013.
#               VERITAS SOFTWARE
# 1600 PLYMOUTH STREET, MOUNTAIN VIEW, CA 94043

#
# The global variables SUCCESS, FAILURE, WARNING, EXCLUDE, PATH, ROOT,
# SW_CTL_SCRIPT_NAME, _pf, PRODUCT, and FILESET are all set by control_utils.
#

UTILS=/usr/lbin/sw/control_utils
if [ ! -f $UTILS ]
then
	print "ERROR:   Cannot find the sh functions library $UTILS."
	exit 1
fi

. $UTILS

ret=${SUCCESS}

volisroot()
{
	echo "ERROR:   The root device is configured as a VxVM volume."
	echo "         The $PRODUCT package can not be removed."
}

volopenmsg()
{
	echo "ERROR:   The $PRODUCT package cannot be removed.  Unmount any"
	echo "         filesystems on these volumes and ensure that no application is"
	echo "         using these volumes.  Move the above volumes to LVM logical"
	echo "         volumes or take backups of these volumes.  Otherwise you may"
	echo "         lose all data on these volumes when $PRODUCT package is removed."
}

#
# Check to see if root disk is under vxvm control
#

if [ -x /sbin/is_vxvmroot ]
then
	if /sbin/is_vxvmroot
	then
		volisroot
		ret=${FAILURE}
	fi
fi

#
# Check to see if volumes are open.
#
# On HP-UX, the control scripts are not interactive.  So for
# now simply print the $VOLOPENMSG and exit with Failure
#

if [ -x /sbin/vxprint ]
then
	OPENVOLS=`/sbin/vxprint -QAqne 'v_use_type!="root" && \
		 v_use_type!="swap" && v_open' 2>/dev/null`
	if [ $? -eq 0 -a ! -z "$OPENVOLS" ]
	then
		echo "NOTE:    The following volumes are still open:"
		for i in ${OPENVOLS}
		do
			echo "         $i "
		done
		volopenmsg
		ret=${FAILURE}
	fi
fi

exit $ret
