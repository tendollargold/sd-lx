#!/sbin/sh
########
#  Product: iCOD
#  Fileset: ICOD-COMMON
#  configure
#  @(#) $Revision: 72.88 $
########
#
# (C) Copyright 2000-2007 Hewlett-Packard Development Company, L.P.
#
# Confidential computer software. Valid license from HP required for
# possession, use or copying. Consistent with FAR 12.211 and 12.212,
# Commercial Computer Software, Computer Software Documentation, and
# Technical Data for Commercial Items are licensed to the U.S. Government
# under vendor's standard commercial license.
#
########

function KillProcess {
        pid=$(ps -e | while read _PID _TTY _TIME _CMD
             do
                 [[ $_CMD = "$1" ]] && echo $_PID
             done)
        if [[ -n $pid ]]; then
            # msg BLANK "Killing process $pid"
            kill -s SIGTERM $pid
            if [ $? -ne 0 ]; then
                msg WARNING "Unable to remove Instant Capacity daemon ($1)."
                [[ $exitval -ne $FAILURE ]] && exitval=$WARNING
            fi
        fi

        typeset -i i=0
        if [[ -n $pid ]]; then
            while [ 1 ]; do
                 typeset -i pidstatus=0
                 pidstatus=$(ps -e | while read _PID _TTY _TIME _CMD
                           do
                               [[ $_PID = $pid ]] && echo 1
                           done)

               if [[ $pidstatus -ne 1 ]]; then
                    #
                    # We didn't find the process, so it is dead
                    #
                    funcRet=0
                    return                    
                fi

                if (( i < 10 )); then
                    (( i+=1 ))
                    sleep 1
                else
                    kill -s SIGKILL $pid
                    if [ $? -ne 0 ]; then
                      msg WARNING "Unable to force restart of Instant Capacity daemon ($1).  System must be rebooted."
                      exitval=$FAILURE
                    fi
                    funcRet=1
                    return  
                fi
            done
        fi
        funcRet=0
        return  
}

function doIfDifferent {
	infil=${1}
	outfil=${2}
	cmd=${3}
	
	# 1 means "failure", don't copy.
	# 0 means "success", copy.
	cpy=1
    if [[ -f $infil ]]
    then
        if [[ -f $outfil ]]
        then
            cksum $outfil|read outchk outcnt outfile
            cksum $infil|read inchk incnt infile
            if [[ $outchk = $inchk && $outcnt = $incnt ]]
            then
                cpy=1
            else
                cpy=0
            fi
        else
            cpy=0
        fi
    else
        cpy=1
    fi
    if [[ $cpy = 0 ]] 
    then
        $cmd -f $infil $outfil
    fi
    return $cpy
}

#########################################################################

    UTILS="/usr/lbin/sw/control_utils"
    if [[ ! -f $UTILS ]]
    then
        echo "ERROR:   Cannot find $UTILS"
        exit 1
    fi
    . $UTILS
    exitval=$SUCCESS

################################################################################

########
# Remove obsolete symlinks
########
    rm -f /sbin/rc2.d/S670icod
    rm -f /sbin/rc1.d/S330icod
    rm -f /sbin/rc1.d/K100icod

########
# Remove obsolete doc files
########
    rm -f /usr/share/doc/icodRelNotes.pdf
    rm -f /usr/share/doc/icodUserGuide.pdf

########
# Remove obsolete iCOD cron jobs (pre-V6) 
########
    {
        crontab -l 2>/dev/null |
        grep -v iCOD | grep -v icod_notify
    } | crontab 2>/dev/null

########
# Define some variables for convenience
########
    configdir=/etc/opt/iCAP
    icoddir=/opt/icod

########
# Register the iCAP WBEM providers
########

    arch=$(uname -m)
    if [[ $arch = "ia64" ]]; then
        lib_ext=so
    else
        lib_ext=sl
    fi
    w_bin=/opt/wbem/bin
    mods="HP_iCODProviderModule HP_GiCAPProviderModule HP_iCAPProviderModule"
	classes="HP_iCODComplex HP_iCAPPartition HP_iCAPComplex  HP_GiCAPManager HP_GiCAPGroup HP_GiCAPMember"
    namesp=root/cimv2/npar
    i_lib=${icoddir}/lib/libiCODProviderModule.1
    w_lib=/opt/wbem/providers/lib/libiCODProviderModule.$lib_ext
    i_mofp=${icoddir}/mof
    
    # get_sw_rev returns "02 05" for WBEM A.02.05. We need to strip leading
    # zeros in order to use arithmetic comparison in sh full POSIX mode (though
    # in this context we might not need to, it seems safer.) 
    echo "$(get_sw_rev WBEMServices)" | read majort minort
    major=${majort#0}
    minor=${minort#0}
    # msg NOTE WBEM version is $majort.$minort: $major, $minor

    if (( ( major > 2 ) || ( major == 2 && minor >= 5 ) ))
    then
        # msg NOTE using run-as-requestor MOF files
        reg_mofs="$i_mofp/HP_iCODProviderReg25.mof \
              $i_mofp/HP_iCAPProviderReg25.mof \
              $i_mofp/HP_GiCAPProviderReg25.mof"
    else
        # msg NOTE using PRE-run-as-requestor MOF files
        reg_mofs="$i_mofp/HP_iCODProviderReg.mof \
              $i_mofp/HP_iCAPProviderReg.mof \
              $i_mofp/HP_GiCAPProviderReg.mof"
    fi             
    i_mofs="$i_mofp/HP_iCODComplexClass.mof \
            $i_mofp/HP_iCAPProviderClasses.mof \
            $i_mofp/HP_GiCAPProviderClasses.mof"

    #
    # First we need to make sure the cimserver is running
    #
    # msg BLANK "Checking if cimserver is running"
    junk=$( /usr/bin/ps -e | /usr/bin/grep cimserver | /usr/bin/grep -vq cimserverd 2>&1 )
    if [ $? -ne 0 ]; then
        # msg BLANK "Starting cimserver"
        /opt/wbem/sbin/cimserver > /dev/null 2>&1
        rv=$?
        if [ $rv -ne 0 ]; then
            exitval=$FAILURE
            msg ERROR "Failed to start cimserver"
        fi
    fi

    #
    # First we need to unregister the iCAP WBEM provider module
    #
    # msg BLANK "Checking Instant Capacity WBEM provider registration using cimprovider."
    providers=$( $w_bin/cimprovider -l 2>&1 )
    rv=$?
    if [ $rv -ne 0 ]; then
        msg ERROR "$w_bin/cimprovider failed: $out"
    else
        for mod in $mods
        do
            # If mod is in the list of providers disable and remove it. Note
            # that WBEM documentation suggests using "cimprovider -d" to disable
            # and then "cimprovider -r" to remove. In fact the "-d" is not
            # necessary, and can cause problems on some versions of WBEM.
            #
            if [[ " $providers " = *[[:space:]]$mod[[:space:]]* ]]
            then
                # msg BLANK "Removing Instant Capacity WBEM provider $mod using cimprovider."
                out=$( $w_bin/cimprovider -r -m $mod 2>&1 )
                rv=$?
                if [ $rv -ne 0 ]; then
                    msg ERROR "Unable to unregister $mod: $out"
                fi
            fi
        done 
    fi

    #
    # Create the symbolic links to the shared library
    #
    # msg BLANK "Removing old symbolic link $w_lib"
    rm -f $w_lib 2> /dev/null
    rv=$?
    if [ $rv -ne 0 ]; then
        exitval=$FAILURE
        msg ERROR "Failed to remove $w_lib"
    fi

    # msg BLANK "Creating symbolic link to $i_lib"
    /usr/bin/ln -sf $i_lib $w_lib 2> /dev/null
    rv=$?
    if [ $rv -ne 0 ]; then
        exitval=$FAILURE
        msg ERROR "Could not create the symbolic link to $i_lib"
    fi
    
    # 
    # Update the class definition if the old one exists, in all namespaces
    #
    # msg BLANK "Listing namespaces in which to register Instant Capacity WBEM provider class definitions."
    namespaces=$( /opt/nparprovider/lbin/nparnamespaces 2> /tmp/$$nmsp )
    if [ $? -ne 0 ]; then
        exitval=$FAILURE
        msg ERROR "Unable to list WBEM namespaces: $(cat /tmp/$$nmsp)"
    else
        for ns in $namespaces
        do 
            #
            # We are deleting classes, not provider modules, and we don't have a
            # list of the classes elsewhere so we list them here.  icoddeleteclass
            # ignores errors about non-existent classes so this list had better
            # be correct because we will never know if we specified a class that
            # doesn't exist.
            #            
            for clss in $classes
            do
                # msg BLANK "Removing $clss WBEM class from namespace $ns."
                out=$(${SW_CONTROL_DIRECTORY}icoddeleteclass $ns $clss 2>&1)
                rv=$?
                if [ $rv -ne 0 ]; then
                    exitval=$FAILURE
                    msg ERROR "Unable to delete $clss WBEM class from namespace $ns: $out"
                fi
            done

            for mof in $i_mofs
            do
                # msg NOTE "Updating $mof in namespace $ns using cimmof."
                out=$($w_bin/cimmof -w -uc -n $ns $mof 2>&1)
                rv=$?
                if [ $rv -ne 0 ]; then
                    exitval=$FAILURE
                    msg ERROR "Unable to update $mof: $out"
                fi
            done
        done
    fi
    rm -f /tmp/$$nmsp
 
    #
    # Now we can register the Instant Capacity WBEM provider module
    #
    for mof in $reg_mofs
    do
        # msg NOTE "Registering $mof using cimmof."
        out=$( $w_bin/cimmof -w -n root/PG_InterOp $mof 2>&1 )
        if [ $? -ne 0 ]; then
            exitval=$FAILURE
            msg ERROR "Unable to register $mof: $out"
        fi
    done

########
# Configure GiCAP/SSH keys (if possible)
########
    # msg BLANK "Configuring GiCAP Security."

    mkdir -p ${configdir}
    chown root:sys ${configdir}
    chmod 755 ${configdir}

    ln -sf ${icoddir}/bin/GiCAP_keygen ${configdir}/GiCAP_keygen >/dev/null
    if [[ ! -f ${configdir}/.GiCAPKey ]]
    then
        if [[ -f /opt/ssh/bin/ssh-keygen ]]
        then
            /opt/ssh/bin/ssh-keygen -t rsa -f ${configdir}/.GiCAPKey -N "" > /dev/null
            if [[ $? -ne 0 ]]
            then
                msg ERROR "could not generate GiCAP host key"
                exit $FAILURE
            fi
        fi
    fi

    mkdir -p ~root/.ssh
    chmod 755 ~root/.ssh

#########
# Configure the XML file we need for GiCAP Disaster Recovery
#########

    msg BLANK "Configuring GiCAP XML files."

    # We don't package /etc/opt/iCAP, so we carry stuff in /opt/icod and copy
    # from there. We leave a copy in /opt/icod so that multiple swconfig
    # commands won't "eat" the only copy.
    doIfDifferent ${icoddir}/GiCAP.checkScavengedMember.xml ${configdir}/GiCAP.checkScavengedMember.xml cp    

########
# Skip the rest of the Instant Capacity configuration on non-supported systems.
########
    msg BLANK "Checking for partitionable system."
    out=$(/usr/sbin/icapstatus -z 2>&1)
    supportedStatus=$?
    if [[ $supportedStatus = 4 ]]; then            
        ### This is not a supported system
        msg NOTE "System is not an Instant Capacity supported system; done."
        exit $exitval
    elif [[ $supportedStatus != 0 ]]; then
        msg ERROR "Software configuration has failed.  After addressing the issues in the following output, configure this software with 'swconfig iCOD'." 
        echo "$out"
        exit $FAILURE
    fi

########
# Configure the Instant Capacity software.
########
    typeset -i returnStatus

    # msg BLANK "Configuring Instant Capacity software."
    out=$(/usr/sbin/icapmodify -M 2>&1)

    returnStatus=$?

    ### If this is an iCAP system, output all the details.
    ### If icapmodify exits with a 2, it means the software is not supported
    ### on the OS, OR the system does not currently have any iCAP resources
    ### (iCAP Cells or CPUs, or temporary capacity).

    if [[ 2 -ne $returnStatus ]] 
    then
        echo "$out"
        if [[ 0 -ne $returnStatus ]]
        then
            exitval=$FAILURE
        fi
    fi

########
# Remove obsolete icodd reference from inittab if it is already there.
########

    typeset -i entryExists
    typeset -i funcRet=0
    entryExists=$(grep icodd /etc/inittab | grep -v ^# | wc -l)
    if [ "$entryExists" -ne 0 ]; then
        # msg NOTE "Removing obsolete icodd entry from /etc/inittab."
        grep -v 'icodd' /etc/inittab > /tmp/inittab$$
        cp /tmp/inittab$$ /etc/inittab
        rm -f /tmp/inittab$$
        KillProcess "icodd" 
        if [ "$funcRet" -ne 0 ]
        then
            msg ERROR "Error killing process icodd"
            # don't even try to continue if we couldn't get rid of icodd       
            exit $exitval
        fi
    fi

######
# Remove /usr/lbin/icodd if it still exists.  If they don't
# do a swremove before upgrading to V8, then the file likely still
# exists and we really don't want it hanging around forever, possibly
# causing confusion. Do it here after the daemon has been killed, to
# avoid getting "file busy" errors.
######

    rm -f /usr/lbin/icodd
    
########
# Add Instant Capacity daemon to inittab if it is not already there.
########
    typeset -i killStatus
    entryExists=$(grep icapd /etc/inittab | grep -v ^# | wc -l)
    if [ "$entryExists" -eq 0 ]; then
        msg NOTE "Adding icapd entry to /etc/inittab."
        echo "icap:23456:respawn:/usr/lbin/icapd # Instant Capacity daemon" >> /etc/inittab
        /sbin/init q
    else                ### kill the deamon if it is running 
                        ### init should automatically respawn the new one

        # msg BLANK "Restarting Instant Capacity daemon (icapd)."
        KillProcess "icapd"
    fi

exit $exitval
