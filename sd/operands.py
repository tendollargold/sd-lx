# 
# operands.py
# 
#
# Copyright (C) 2012-2024 Paul Weber conversion to Python
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

print("Entering operands.py")
print("")


# 3.4 Operands page 38
# There are two types of operands that may be specified on the command line, 
# software_selections and targets. The software_selections refer to the 
# software objects (bundles, products, subproducts and filesets) to be operated
# on. The targets refer to the target software_collections where the software 
# selections are applied. These two operand types is separated by the @ 
# operand. With the exception of swpackage, the behavior of all utilities 
# defined in this Software Administration specification is undefined if no 
# software_selections are provided.

# 3.4.1 Software Specification and Logic
# The following specifies the syntax for software selections in utilities 
# (software_spec) and in dependency specifications (dependency_spec). This 
# syntax is applied by the utilities to search a software_collection catalog 
# for software. Note that the tokens shown below are defined in the Glossary.

# %token FILENAME_CHARACTER_STRING /* as defined in Glossary */
# %token NEWLINE_STRING /* as defined in Glossary */
# %token PORTABLE_CHARACTER_STRING /* as defined in Glossary */
# %token SOFTWARE_PATTERN_MATCH_STRING /* as defined in Glossary */
# %token WHITE_SPACE_STRING /* as defined in Glossary */

FILENAME_CHARACTER_STRING=""
NEWLINE_STRING=""
PORTABLE_CHARACTER_STRING=""
SOFTWARE_PATTERN_MATCH_STRING=""
WHITE_SPACE_STRING=""


# %start software_selections
# %%
# software_selections : software_selections ws software_spec
#                     | software_spec
#                     ;

# software_spec       : bundle_software_spec
#                     | product_software_spec
#                     ;

# bundle_software_spec : bundle_qualifier version
#                      | bundle_qualifier ’.’ product_qualifier version
#                      ;

# bundle_qualifier     : bundle_qualifier ’.’ bundle_tag
#                      | bundle_tag
#                      ;

# product_software_spec : product_qualifier version
#                       ;

# product_qualifier     : product_tag subproduct_qualifier fileset_qualifier
#                       ;

# subproduct_qualifier  : /* empty */
#                       | subproduct_qualifier ’.’ subproduct_tag
#                       | ’.’ subproduct_tag
#                       ;

# fileset_qualifier     : /* empty */
#                       | ’.’ fileset_tag
#                       ;

# bundle_tag            : sw_pattern
#                       ;

# product_tag           : sw_pattern
#                       ;

# fileset_tag           : sw_pattern
#                       ;

# subproduct_tag        : sw_pattern
#                       ;

# version               : /* empty */
#                       | ’,*’
#                       | version_qualifier
#                       ;

# version_qualifier     : version_qualifier ver_item
#                       | ver_item
#                       ;

# ver_item              : ’,’ ver_id ’=’
#                       | ’,’ ver_id ’=’ sw_pattern
#                       | ’,’ ’r’ rel_op dotted_string
#                       ;

# sw_pattern            : SOFTWARE_PATTERN_MATCH_STRING
#                       ;

# ver_id                : ’r’ | ’a’ | ’v’ | ’l’ | ’q’
#                       ;

# rel_op                : ’==’ | ’!=’ | ’>=’ | ’<=’ | ’<’ | ’>’
#                       ;

# dotted_string         : FILENAME_CHARACTER_STRING
#                       ;

# ws                    : WHITE_SPACE_STRING
#                       ;

# %start dependency_spec
# %%

# dependency_spec       : dependency_spec ’|’ software_spec
#                       | software_spec
#                       ;


# If the software_spec identifies a bundle, product or subproduct software object, then all filesets contained within that object are included as part of that specification. For software selections, this means that all of these filesets are included. For dependency specifications, this means that all of these filesets are needed in order to meet the dependency.

# If a software_spec identifies a set of filesets that is less that the entire set of filesets within a bundle or product, the software_spec identifies a partial bundle or product.

# Only the specified strings is used to generate a software_spec. Blanks do not appear between items. The sw_pattern and dotted_string must be enclosed in quotes if they contain blanks or commas. The bundle_tag, product_tag, subproduct_tag, and fileset_tag consists of one or more characters from the filename character set, with the exception that the following three characters . , : (period, comma, and colon) are not used.

# Searching a software_collection catalog for software using a software_spec yields a list of zero or more software objects that match the software_spec . The rules to be used in the search are the following:
# 1. The software_spec is compared against software in the software collection. The leftmost sw_pattern of the software_spec is matched against the tag attribute of all bundles and products in the software collection. All objects that match are initially included for consideration. If the sw_pattern does does not match any bundle or product, no objects are included.

# The version specified in the software_spec is compared against the revision, architecture, vendor_tag, location , and qualifier attributes of the objects matching the leftmost sw_pattern . If any ver_id in the software_spec does not match its corresponding attribute, that object is removed from consideration. If the same ver_id is given more than once, all the comparisons specified are performed and all must succeed to be considered a match.



# Table 3-1 Software_spec Version Identifiers
#         -------------------------
#         | ver_id | Attribute    |
#         -------------------------
#         |   r    | revision     |
#         |   a    | architecture |
#         |   v    | vendor_tag   |
#         |   l    | location     |
#         |   q    | qualifier    |
#         -------------------------

# An implementation may define additional ver_id items along with the attributes
# and objects to which they apply.

# For each object still included for consideration, each successive sw_pattern,
# left to right, is applied to the bundles, products, subproducts and filesets 
# within that object. The same sw_pattern may match multiple bundle, product, 
# subproduct and fileset objects. If any sw_pattern does not match any objects 
# within the current object, the current object is removed from consideration. 
# If a fileset matches a sw_pattern but there is still an unmatched sw_pattern
# in the software_spec, that fileset is not selected.

# When there are no more sw_patterns left in the software_spec, all the objects
# identified by the rightmost sw_pattern of the software_spec are included in 
# the list of software that match the software_spec.

#  2. The comparison performed when the operator is = will be a software pattern
#     match as described in the Glossary. If the ver_id is specified and the 
#     value is an empty string, then the comparison is successful only if the 
#     corresponding attribute is not specified. See Section 3.4.1.1 on page 42.

#  3. When rel_op is used,(As defined in the syntax for ver_item, this 
#     comparison is required for revision only and any other comparison is 
#     undefined.) the comparison is performed on the specified attribute by 
#     dividing it into segments separated by the . (period) character. If there
#     is no period in an attribute, it contains one segment. The segments are 
#     compared with the corresponding segments of the dotted_string . If all 
#     characters in both segments to be compared are decimal digit characters 
#     (0-9),5 the comparison is based on the decimal numeric value of the 
#     segments, starting with the leftmost segment. If either segment includes 
#     a any character other than a decimal digit character, a string comparison
#     is made to determine the relation. String comparisons is made using, as a
#     collation sequence, the order of characters in If one operand has fewer 
#     segments than the other, the unmatched segments is compared against the 
#     value 0 (zero).

#  4. When applied to software in installed software collections, use of either
#     the l (location) or q (qualifier) ver_id causes comparison with the value
#     of the location or qualifier attribute respectively for each product or 
#     bundle in the installed_software object.

#     For distributions, use of either the l (location) or q (qualifier) ver_id
#     is ignored for the purpose of comparisons. Although not used for 
#     comparisons, the location and qualifier ver_ids are used by the swinstall
#     utility as the location attribute for installing the software and the 
#     qualifier attribute for the software respectively.

#  5. When software selections are applied to a source or target, and a 
#     software_spec resolves to more than one software object, then the 
#     software_spec is considered ambiguous. An ambiguous selection may be 
#     elective or incidental. An elective ambiguous selection occurs when a 
#     sw_pattern in a software_spec contains a wildcard character or when the 
#     version contains a rel_op , or when the sw_pattern is missing.
#     In all other cases the selection is an incidental ambiguous selection. 
#     An incidental ambiguous selection is only valid for swlist, and for other
#     utilities generates an event.  (SW_ERROR: SW_SELECTION_NOT_FOUND_AMBIG)

#  6. If the software_spec begins with a bundletag definition, then that bundle
#     definition is copied or installed with swcopy or swinstall. Thus, a 
#     software_spec which matches one or more bundles can be used with all 
#     other utilities, but only if they were explicitly installed or copied. 
#     However, all subproduct definitions are copied or installed independently
#     of whether they were explicitly selected. Thus, a software_spec which 
#     matches subproducts can always be used on existing products.

# For both bundles and subproducts, if some part of their contents exist, then 
# the selection can be found; therefore, any operation will succeed. If none of
# their contents exist, then the selection cannot be found; therefore, the 
# operation will fail (for those selections not found) as defined in the 
# individual Extended Description section of each utility.


#
# 3.4.1.1 Fully-qualified Software_spec

# A fully-qualified software_spec is one in which no fields contain a shell 
# pattern match string and all version distinguishing attributes are specified 
# as "ver_id=<value>" (if a value is 
# supplied) or as "ver_id=" (if no value is supplied or if the value supplied 
# is an empty string).  Note that a fully-qualified software_spec always 
# identifies a software object unambiguously.

# When a software_spec is generated by swlist, only the following tags are 
# included: the product tag for products, the bundle tag for bundles, the 
# product and fileset tag for filesets, and the product and subproduct tag for 
# subproducts.

# 3.4.1.2 Software Compatibility
# Products contain attributes (os_name, os_version, os_release, and 
# machine_type) related to the uname( ) function defined by POSIX.1. These 
# attributes are used by the swinstall, swconfig, and swverify utilities to 
# determine if software is compatible with a target host. A product is 
# considered compatible with a target host if each of the uname attributes of 
# the product contains a pattern in its definition that matches the 
# corresponding values returned by the uname( ) function on the target host. 
# If any of these attributes is undefined, it is considered to match any target
# host. The compatibility test applies to all components of a product, including
# subproducts and filesets.

# Bundles, like products, possess uname attributes. The values of the bundle 
# uname attributes determine the compatibility of the bundle in conjunction 
# with the corresponding attributes of products within the bundle. A product 
# specified as part of a bundle is considered compatible if both the product and
# bundle uname attributes designate that the software is compatible. As with 
# products, if any of these attributes is undefined, it is considered to match 
# any target host.

#
# 3.4.2 Source and Target Specification and Logic

# Source and target software_collections are specified using the following 
# syntax.

# %token HOST_CHARACTER_STRING /* as defined in Glossary */
# %token PATHNAME_CHARACTER_STRING /* as defined in Glossary */

HOST_CHARACTER_STRING="localhost"
PATHNAME_CHARACTER_STRING="/"

# %start target

# %%

# target : software_collection_spec
#        ;

# software_collection_spec : HOST_CHARACTER_STRING ’:’ PATHNAME_CHARACTER_STRING
#                                                   | HOST_CHARACTER_STRING ’:’
#                                                   | HOST_CHARACTER_STRING
#                                                   | PATHNAME_CHARACTER_STRING
#                                                   ;

# %start source

# %%

# source       : software_collection_spec
#              ;

# The : (colon) is required if both the host and pathname are specified, or if 
# the host portion starts with a / (slash). The pathname portion is an absolute
# path. The colon is not allowed by itself.


# The HOST_CHARACTER_STRING portion refers to the implementation defined identifier for a host. If it is not specified, then the local host is assumed.

# The PATHNAME_CHARACTER_STRING portion refers to the software_collection path attribute (the location on the host of the distribution or installed_software object).

# When the PATHNAME_CHARACTER_STRING is not specified for installed_software, the directory / is used.

# A PATHNAME_CHARACTER_STRING other than / for an installed_software object is 
# referred to as an alternate root directory. When the PATHNAME_CHARACTER_STRING
# is not specified for source distributions, the value of the 
# distribution_source_directory default option is used. When the 
# PATHNAME_CHARACTER_STRING is not specified for target distributions, the value
# of the distribution_target_directory default option is used.

# For installed_software objects, the value of the installed_software_catalog 
# option is used to further clarify which installed software object is actually 
# being targeted. Multiple installed_software objects may share the same path 
# attribute, but they have separate catalog information because they are 
# distinct objects. The installed_software path attribute prepended to the value
# of the installed_software_catalog option forms the key for the object into 
# the catalog information. Use of the installed_software_catalog is independent 
# of the -c option.

# An implementation supports source and target distributions in the directory 
# format described in Section 5.3 on page 147, for all utilities. An 
# implementation supports a source distribution in the serial format for swask,
# swinstall, and swcopy utilities. An implementation supports a target 
# distribution in the serial format for swlist, swcopy, and swpackage. Whether 
# data on an existing target distribution in serial format is overwritten or 
# merged is implementation defined. An implementation need not support a target 
# distribution in the serial format for swverify, swremove, and swmodify. 
# Unless otherwise stated, support for serial distributions includes support for
# both extended tar and extended cpio archives. See Section 5.3 on page 147. The
# format of these archives is defined in POSIX.1.

print("Exiting operands.py")
print("")

