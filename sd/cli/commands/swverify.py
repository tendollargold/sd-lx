#!/usr/bin/python3

# swverify.py - swverify verify software installation or in a depot

# Copyright (C) 2023 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
cmd = os.path.split(sys.argv[0])[1]

swverify_parser = argparse.ArgumentParser(prog='swverify',
			usage="""%(prog)s [-d|-r] [-F][-v] [-R] [-g] [-C session_file] 
			[-f software_file] [-Q date] [-S session_file] [-t target_file] 
			[-x option=value] [-X option_file] [software_selections] [-T target_selections]""",
        formatter_class=argparse.RawTextHelpFormatter,
        epilog='''

 Software selections are specified as:

     product[.subproduct][.fileset][,version] ...

 where version is either

     [r=revision][,a=arch][,v=vendor][,l=location]
     instance_id

 Target selections are specified as:

     -T [host][:][path] ...
 
''')

swverify_parser.add_argument("-d --depot", metavar='',
		    help="target selections are software depots")
swverify_parser.add_argument("-r --root",metavar='',
			help="target selections are alternate root directories")
swverify_parser.add_argument("-F --fix-script", metavar='fix_script',
			help="run a fix script")
swverify_parser.add_argument("-v --verbose",action='store_true',
			help="write verbose output to stdout")
swverify_parser.add_argument("-R --rebuild-index",metavar='',
			help="rebuild software selection INDEX db")
swverify_parser.add_argument("-C", metavar='session_file',
			help="save the session to session_file")
swverify_parser.add_argument("-f", metavar='software_file',
			help="read software selections from this file")
# HP/HPE SD-UX adds -Q for swverify
swverify_parser.add_argument("-Q", metavar='MM/DD[/YYYY][,HH:MM][AM|PM]',
			help="schedule the verify for this date")
swverify_parser.add_argument("-S",metavar='session_file',
			help="read options, software selections, and target selections from this file")
swverify_parser.add_argument("-t", metavar='target_file',
			help="read target selections from this file")
swverify_parser.add_argument("-x option=value",metavar='',
			help="set the option to value")
swverify_parser.add_argument("-X",metavar='option_file', 
			help="read option definitions from this file")
# -g --gnu option added for GNU Copyleft
swverify_parser.add_argument("-g", "--gnu",action='store_true',
			help="print GNU Copyleft")
swverify_parser.add_argument("software_selections", nargs='*',
			help="software selections: XServer.MAN ...")
# B.3.4 page 188 Using -T vs @ for target selection as it is easier in argparse. :)
swverify_parser.add_argument("-T", nargs="+",metavar="[host][:][path]",
			help="Target selections specified as: [host][:][path] \n default is localhost:/")
swverify_parser.parse_args()
args = swverify_parser.parse_args()
#print("DEBUG: Args in: ", cmd,".py ", args)


# No arguments to swverify yields:
#ERROR:   No software selections have been specified.  You must specify
#         a list of products (or use the "\*" wildcard).
#ERROR:   Cannot continue the "swverify" task.
#       * Selection had errors.

