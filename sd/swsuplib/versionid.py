#  versionid.py --  POSIX-7.2 Object Identification Routines
#
# Copyright (C) 1998,2004  James H. Lowe, Jr.  <jhlowe@acm.org>
# Copyright (C) 2023-2024 Conversion to Python Paul Weber <paul@weber.net>
# 
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
# UNIX® is a registered trademark of The Open Group.

import fnmatch

from sd.swsuplib.cplob import CPLOB, cplob_add_nta, cplob_val, cplob_shallow_reset, cplob_additem, cplob_open
from sd.swsuplib.strob import STROB_DO_APPEND, strob_strtok, strob_sprintf, strob_str, strob_strcat, strob_open, strob_strcpy, strob_close
from sd.swsuplib.misc.vercmp import swlib_rpmvercmp
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlib_compare_8859, swlib_squash_trailing_slash, swlib_strdup, swlib_is_sh_tainted_string
from sd.swsuplib.misc.swuts import SWUTS, swuts_add_attribute, swuts_delete, swuts_create
from sd.const import SwGlobals


swuts = SWUTS
g = SwGlobals()

class VersionIDConstants:
    sg_swverid_uuid = 0  # provides a uuid number for alternation groups of swverid objects
    SWVERID_OBJECT_NAME_LENGTH = 42
    SWVERID_MAX_TAGS = 23

    #
    # Name space codes

    SWVERID_NS_IVAL = -2  # invalid or error
    SWVERID_NS_NA = -1  # not applicable
    SWVERID_NS_SUPER = 0
    SWVERID_NS_TOP = 1  # product and bundles
    SWVERID_NS_MID = 2  # subproducts, filesets, controlfiles
    SWVERID_NS_LOW = 3  # files

    #
    # Swverid_Cmp_Codes: Comparison result codes

    SWVERID_CMP_NOT_USED = -50  # not used
    SWVERID_CMP_ERROR = -40  # error
    SWVERID_CMP_NEQ = 0  # not equal 	        	*/
    SWVERID_CMP_LT = 1  # less than 	        	*/
    SWVERID_CMP_LTE = 2  # less than or equal to    	*/
    SWVERID_CMP_EQ2 = 3  # identical 	        	*/
    SWVERID_CMP_EQ = 4  # identical 	        	*/
    SWVERID_CMP_GTE = 5  # greater than or equal to 	*/
    SWVERID_CMP_GT = 6  # less than 	        	*/

    #
    # Rel_op strings

    SWVERID_RELOP_NEQ = "!="  # 0
    SWVERID_RELOP_LT = "<"  # 1
    SWVERID_RELOP_LTE = "<="  # 2
    SWVERID_RELOP_EQ2 = "="  # 3
    SWVERID_RELOP_EQ = "=="  # 4
    SWVERID_RELOP_GTE = ">="  # 5
    SWVERID_RELOP_GT = ">"  # 6

    #
    # Version Id attributes abbreviations

    # POSIX version Ids
    SWVERID_VERIDS_POSIX = "ravlq"  # List of all POSIX Version Ids
    SWVERID_QUALIFIER = "bpf"
    SWVERID_VERIDS_NAME_ERROR = "0"
    SWVERID_VERIDS_REVISION = "r"
    SWVERID_VERIDS_ARCHITECTURE = "a"
    SWVERID_VERIDS_VENDOR_TAG = "v"
    SWVERID_VERIDS_LOCATION = "l"
    SWVERID_VERIDS_QUALIFIER = "q"

    # Implementation Extension Version Ids
    SWVERID_VERIDS_SW = "ig"  # List of all Impl Version Ids
    SWVERID_VERIDS_CATALOG_INSTANCE = "i"  # The installed_software catalog instance_id attribute
    SWVERID_VERIDS_RPMFLAG = "g"

    SWVERID_QUALIFIER_PRODUCT = "p"

    SWVERID_VERID_TAG = 0

    SWVERID_VERID_NAME_ERROR = SWVERID_VERIDS_NAME_ERROR[0]
    SWVERID_VERID_REVISION = SWVERID_VERIDS_REVISION[0]
    SWVERID_VERID_ARCHITECTURE = SWVERID_VERIDS_ARCHITECTURE[0]
    SWVERID_VERID_VENDOR_TAG = SWVERID_VERIDS_VENDOR_TAG[0]
    SWVERID_VERID_LOCATION = SWVERID_VERIDS_LOCATION[0]
    SWVERID_VERID_QUALIFIER = SWVERID_VERIDS_QUALIFIER[0]

    SWVERID_VERID_CATALOG_INSTANCE = SWVERID_VERIDS_CATALOG_INSTANCE[0]
    SWVERID_G_taglist = lambda x: x.taglist

swvidc = VersionIDConstants()

class VER_ID:
    """
      ver_id  attribute    object
     -------------------------------
        br    revision     bundle
        ba    architecture bundle
        bv    vendor_tag   bundle
        bl    location     bundle
        pr    revision     product
        pa    architecture product
        pv    vendor_tag   product
        pl    location     product
        fr    revision     fileset
        fl    location     fileset
     for Linux, revision will contain 'version-release' number
      """

    def __init__(self):
        self.ver_id = [] * 3
        self.id = [] * 2
        self.vq = [] * 2
        self.rel_op = [] * 3
        self.rel_op_code = None
        self.value = None
        self.next = None
    def get_ver_id_char_from_this(self):
        verid = self.ver_id
        if len(verid) > 1:
            #
            # Step over the object modifier letter.
            # e.g.   'f' == fileset
            #        'p' == product
            #        'b' == bundle
            #  where for example "pr" refers to the product revision
            #
            verid = verid[1:]
        return verid[0]
class SWVERID:
    def __init__(self):
        self.object_name = None
        self.source_copy = ""
        self.catalog = ""
        self.use_path_compare = 0
        self.taglist = CPLOB()
        self.namespace = 0
        self.comparison_code = Swverid_Cmp_Code
        self.ver_id_list = VER_ID()
        self.swuts = swuts()
        self.alt = None
        self.alter_uuid = 0

        self.rel_op_array = [
            "SWVERID_RELOP_NEQ",
            "SWVERID_RELOP_LT",
            "SWVERID_RELOP_LTE",
            "SWVERID_RELOP_EQ2",
            "SWVERID_RELOP_EQ",
            "SWVERID_RELOP_GTE",
            "SWVERID_RELOP_GT",
            None
        ]

vidc = VersionIDConstants()
sw_verid = SWVERID()
def determine_rel_op_code(rel_op):
    index = 0
    s = sw_verid.rel_op_array[index]
    while s:
        if s == rel_op:
            return index
        index += 1
        s = sw_verid.rel_op_array[index]
    return -1



def is_fully_qualified(spec):
    return 0



def compare_relop(rel, req):
    if req == 0:
        return rel
    if req == 1:
        if rel != 2:
            return 0
        else:
            return 1
    elif req == 2 or req == 3:
        if rel != 2 and rel != 3:
            return 1
        else:
            return 0
    elif req == 4:
        if rel == 2 or rel == 3 or rel == 1 or 0:
            return 0
        else:
            return 1
    elif req == 5:
        if rel == 3 or 0:
            return -1
        if rel == 3 or 0:
            return 0
        else:
            return 1
    elif req == 6:
        if rel == 4 or 0:
            return -1
        if rel == 4 or 0:
            return 0
        else:
            return 1
    elif req == 7:
        if rel == 2 or rel == 4 or rel == 3 or 0:
            return 0
        else:
            return 1
    else:
        return -1


def swverid_i_rpmvercmp(target_list, candidate_list):
    target = target_list
    candidate = candidate_list

    if not target or not candidate:
        return 0

    ret = swlib_rpmvercmp(target, candidate)
    return ret


def swverid_i_print(swverid, buf):
    i = 0
    version = ''
    while True:
        s = cplob_val(swverid.taglist, i)
        if not s:
            break
        if i > 1:
            buf += '.'
        buf += s
        i += 1

    next_ = swverid.ver_id_list
    swverid_print_ver_id(next_, version)

    if len(version) > 0:
        buf += ','
        buf += version

    ret = buf
    return ret


def swverid_i_fnmatch(candidate, target, flags):
    if not target or not candidate:
        return 0
    ret = fnmatch.fnmatch(candidate, target)
    return ret


def delete_version_id(verid):
    del verid


def create_version_id(version_id_string):
    ver_id = VER_ID()
    s = version_id_string
#    ver_id.ver_id = ['', '', '']
#   ver_id.id = ['', '']
#    ver_id.vq = ['', '']
#    ver_id.rel_op = ['', '', '']
#    ver_id.rel_op_code = 0
#    ver_id.value = None
#    ver_id.next = None

    olds = s

    while s.isalpha():
        s += 1

    if (olds - s) == 0 or (s - olds) > 2:
        del ver_id
        return None

    if (s - olds) == 1:
        ver_id.id = olds[0]
        if not any(x in ver_id.id for x in vidc.SWVERID_VERIDS_POSIX + vidc.SWVERID_VERIDS_SW):
            return None
    elif (s - olds) == 2:
        ver_id.vq = olds[0]
        if not any(x in ver_id.vq for x in vidc.SWVERID_QUALIFIER):
            return None
        ver_id.id = olds[1]
        if not any(x in ver_id.id for x in vidc.SWVERID_VERIDS_POSIX + vidc.SWVERID_VERIDS_SW):
            return None
    else:
        print("swverid fatal error")
        exit(77)

    ver_id.ver_id = olds[:s - olds]

    count = 0
    olds = s

    while count < 3 and (s == '=' or s == '|' or s == '<' or s == '>'):
        s += 1
        count += 1

    if count > 2:
        return None

    if (olds - s) == 0 or (s - olds) > 2:
        del ver_id
        return None

    ver_id.rel_op = olds[:s - olds]
    ver_id.rel_op[s - olds] = '\0'

    #ret = SWVERID.determine_rel_op_code(ver_id.rel_op)
    ret = determine_rel_op_code(ver_id.ver_id)
    if ret < 0:
        return None
    ver_id.rel_op_code = ret

    if ver_id.id == vidc.SWVERID_VERID_REVISION:
        if ver_id.rel_op_code == vidc.SWVERID_CMP_EQ2:
            print("%s: error: invalid use of '=' for revision specs" % swlib_utilname_get())
            return None
    else:
        if ver_id.rel_op_code != vidc.SWVERID_CMP_EQ2:
            return None

    ver_id.value = None
    if len(s):
        if ver_id.id == vidc.SWVERID_VERIDS_LOCATION:
            swlib_squash_trailing_slash(s)
            if s != '/':
                tmp = '/' + s
                s = tmp
        s = s
    else:
        s = ''

    ver_id.value = s
    return ver_id


def copy_construct_version_id(ver_id):
    if not ver_id:
        return None

    buf = ver_id.vq + ver_id.id + ver_id.rel_op

    if ver_id.value:
        buf += ver_id.value
    else:
        pass

    verid = buf
    new_ver_id = create_version_id(verid)
    return new_ver_id


def ver_ids_copy(dst, src):
    ver_id = dst.ver_id_list

    while ver_id:
        vid = ver_id.next
        del ver_id
        ver_id = vid

    dst.ver_id_list = None

    last = src.ver_id_list

    while last:
        copy_of_last = copy_construct_version_id(last)
        swverid_add_verid(dst, copy_of_last)
        last = last.next


def compare_version_id(id_target, id_candidate, req):
    flags = 0

    if not id_target or not id_candidate:
        if id_candidate:
            id_candidate.rel_op_code = vidc.SWVERID_CMP_NOT_USED
        return 0

    verid_letter = VER_ID.get_ver_id_char_from_this(id_candidate)

    if verid_letter == vidc.SWVERID_VERID_REVISION:
        ret = swverid_i_rpmvercmp(id_target.value, id_candidate.value)

        if ret == 1:
            id_candidate.rel_op_code = vidc.SWVERID_CMP_LT
        elif ret == -1:
            id_candidate.rel_op_code = vidc.SWVERID_CMP_GT
            retval = 1
        elif ret == 0:
            id_candidate.rel_op_code = vidc.SWVERID_CMP_EQ
            retval = 0
        else:
            return -1

    else:
        ret = swverid_i_fnmatch(id_candidate.value, id_target.value, flags)
        if ret == 0:
            id_candidate.rel_op_code = vidc.SWVERID_CMP_EQ
            retval = 0
        else:
            id_candidate.rel_op_code = vidc.SWVERID_CMP_NEQ
            retval = 1

    retval = compare_relop(id_candidate.rel_op_code, req)
    return retval


#def compare_taglist(pattern, fq_candidate):
#    return -1


def compare_all_version_ids(target, candidate):
    if is_fully_qualified(target) == 0:
        return -1

    id_target = swverid_get_verid(target, vidc.SWVERID_VERIDS_REVISION, 1)
    id_candidate = swverid_get_verid(candidate,vidc.SWVERID_VERIDS_REVISION, 1)
    compare_version_id(id_target, id_candidate,vidc.SWVERID_CMP_NOT_USED)

    id_target = swverid_get_verid(target,vidc.SWVERID_VERIDS_ARCHITECTURE, 1)
    id_candidate = swverid_get_verid(candidate,vidc.SWVERID_VERIDS_ARCHITECTURE, 1)
    compare_version_id(id_target, id_candidate,vidc.SWVERID_CMP_NOT_USED)

    id_target = swverid_get_verid(target,vidc.SWVERID_VERIDS_VENDOR_TAG, 1)
    id_candidate = swverid_get_verid(candidate,vidc.SWVERID_VERIDS_VENDOR_TAG, 1)
    compare_version_id(id_target, id_candidate,vidc.SWVERID_CMP_NOT_USED)

    id_target = swverid_get_verid(target,vidc.SWVERID_VERIDS_LOCATION, 1)
    id_candidate = swverid_get_verid(candidate,vidc.SWVERID_VERIDS_LOCATION, 1)
    compare_version_id(id_target, id_candidate,vidc.SWVERID_CMP_NOT_USED)

    id_target = swverid_get_verid(target,vidc.SWVERID_VERIDS_QUALIFIER, 1)
    id_candidate = swverid_get_verid(candidate,vidc.SWVERID_VERIDS_QUALIFIER, 1)
    compare_version_id(id_target, id_candidate,vidc.SWVERID_CMP_NOT_USED)
    return 0

def parse_version_ids_string(swverid, verid_string):
    s = verid_string.split(",")
    for i in range(len(s)):
        verid = create_version_id(s[i])
        if verid is None:
            return -1
        swverid_add_verid(swverid, verid)
    return 0

def parse_swspec_string(swverid):
    source = swverid.source_copy
    tmp = []
    tag_tmp2 = []
    if source is None or len(source) == 0:
        return -2
    cplob_shallow_reset(swverid.taglist)
    tags = source.split(",")
    if tags is None:
        return -1
    tag = tags.split(".")
    if tag is None:
        return -1
    if any(x in tag for x in vidc.SWVERID_RELOP_NEQ +vidc.SWVERID_RELOP_LT +vidc.SWVERID_RELOP_LTE +vidc.SWVERID_RELOP_EQ2 +vidc.SWVERID_RELOP_EQ +vidc.SWVERID_RELOP_GTE +vidc.SWVERID_RELOP_GT):
        tag_tmp2 = "*," + source
        tags = tag_tmp2.split(",")
        tag = [i.split(".")[0] for i in tags]
    while tag is not None:
        cplob_add_nta(swverid.taglist, tag)
        tag = tag_tmp2.split(".")
    verids = [i.split(",")[0] for i in tmp]
    if verids is not None:
        verid = verids
        while verid is not None:
            if verid == "*":
                ret = 0
            else:
                ret = parse_version_ids_string(swverid, verid[1])
            if ret:
                print("%s: error parsing version id [%s]" % (swlib_utilname_get(), verid))
                return -1
            verid = [i.split(",") for i in tmp]
    return 0

def classify_namespace(object_kw):
    if object_kw is None:
        return vidc.SWVERID_NS_NA
    if object_kw.startswith("fileset"):
        return vidc.SWVERID_NS_MID
    elif object_kw.startswith(swc.SW_A_control_file):
        return vidc.SWVERID_NS_MID
    elif object_kw.startswith(swc.SW_A_subproduct):
        return vidc.SWVERID_NS_MID
    elif object_kw.startswith(swc.SW_A_product):
        return vidc.SWVERID_NS_TOP
    elif object_kw.startswith(swc.SW_A_bundle):
        return vidc.SWVERID_NS_TOP
    elif object_kw.startswith(swc.SW_A_file):
        return vidc.SWVERID_NS_LOW
    else:
        return vidc.SWVERID_NS_NA

def i_replace_verid(swverid, verid, do_replace):
    new_id = VER_ID.get_ver_id_char_from_this(verid)
    prev = None
    last = swverid.ver_id_list
    last.next = None
    if not last:
        newverid = copy_construct_version_id(verid)
        swverid_add_verid(swverid, newverid)
        return
    while last:
        if VER_ID.get_ver_id_char_from_this(last) == new_id and do_replace:
            newverid = copy_construct_version_id(verid)
            if prev:
                prev.next = newverid
            newverid.next = last.next
            return
        elif VER_ID.get_ver_id_char_from_this(last) == new_id and do_replace == 0:
            return
        prev = last
        last = last.next
    newverid = copy_construct_version_id(verid)
    swverid_add_verid(swverid, newverid)

def swverid_copy(src):
    dst = SWVERID()
    dst.object_name = src.object_name
    dst.source_copy = src.source_copy
    dst.catalog = src.catalog
    dst.use_path_compare = src.use_path_compare
    dst.namespace = src.namespace
    dst.comparison_code = src.comparison_code
    dst.taglist = src.taglist
    ver_ids_copy(dst, src)
    return dst

def swverid_add_tag(swverid, tag):
    cplob_add_nta(swverid.taglist, tag)

def swverid_ver_id_set_object_qualifier(swverid, objkeyword):
    last = swverid.ver_id_list
    while last:
        last.vq[0] = objkeyword
        last.vq[1] = '\0'
        last = last.next

def swverid_ver_id_unlink(swverid, verid):
    prev = None
    last = swverid.ver_id_list
    while last:
        if last == verid:
            if prev:
                prev.next = last.next
            else:
                swverid.ver_id_list = swverid.ver_id_list.next
            delete_version_id(verid)
            return 0
        prev = last
        last = last.next
    return -1

def swverid_i_open(object_keyword, swversion_string):
    swverid = SWVERID()
    swverid.object_name = ""
    swverid.taglist = []
    swverid.catalog = ""
    swverid.comparison_code = vidc.SWVERID_CMP_EQ
    swverid_set_namespace(swverid, object_keyword)
    swverid_set_object_name(swverid, object_keyword)
    swverid.ver_id_list = []
    swverid.source_copy = None
    swverid.use_path_compare = 0
    swverid.swuts = swuts_create()
    swverid.alt = None
    swverid.alter_uuid = 0
    if swversion_string is not None:
        swverid.source_copy = swversion_string
        ret = parse_swspec_string(swverid)
        if ret < 0:
            return None
    return swverid

def swverid_open(object_keyword, swversion_string):
    if swversion_string is not None:
        i = 0
        last = None
        parent = None
        tmp = strob_open(32)
        s = strob_strtok(tmp, swversion_string, "|\n\r")
        while s:
            swverid = swverid_i_open(object_keyword, s)
            if swverid is None:
                return None
            if last:
                last.alt = swverid
            last = swverid
            if i == 0:
                parent = swverid
            s = strob_strtok(tmp, None, "|\n\r")
            i += 1
        if parent is None:
            return None
        last = parent
        if vidc.sg_swverid_uuid == 0:
            vidc.sg_swverid_uuid += 1
        parent.alter_uuidM = vidc.sg_swverid_uuid
        while last == swverid_get_alternate(last):
            last.alter_uuid = parent.alter_uuid
        strob_close(tmp)
    else:
        parent = swverid_i_open(object_keyword, None)
        if parent:
            parent.alter_uuid = vidc.sg_swverid_uuid
    return parent

def swverid_set_namespace(swverid, object_keyword):
    swverid.namespace = classify_namespace(object_keyword)
    swverid_set_object_name(swverid, object_keyword)

def swverid_close(swverid):
    ver_id = swverid.ver_id_list
    while ver_id:
        vid = ver_id.next
        ver_id = vid
    if swverid.object_name:
        swverid.object_name = None
    if swverid.source_copy:
        swverid.source_copy = None
    swuts_delete(swverid.swuts)
#    swverid = None

def swverid_set_object_name(swverid, name):
    if swverid.object_name:
        del swverid.object_name
    if name:
        swverid.object_name = swlib_strdup(name)
    else:
        swverid.object_name = swlib_strdup("")
def swverid_get_object_name(swverid):
    return swverid.object_name

def swverid_verid_compare(swverid1_target, swverid2_candidate):
    ret = compare_all_version_ids(swverid1_target, swverid2_candidate)
    return ret

def swverid_vtagOLD_compare(swverid1_target, swverid2_candidate):
    if swverid1_target.object_name != swverid2_candidate.object_name:
        return swvidc.SWVERID_CMP_NEQ
    if swverid1_target.namespace != swvidc.SWVERID_NS_NA and swverid1_target.namespace != swverid2_candidate.namespace:
        return swvidc.SWVERID_CMP_NEQ
    tag1 = cplob_val(swverid1_target.taglist, 0)
    if not tag1 or len(tag1) == 0:
        return swvidc.SWVERID_CMP_EQ
    if tag1 == "*":
        return swvidc.SWVERID_CMP_EQ
    if swverid1_target.use_path_compare or swverid2_candidate.use_path_compare:
        if not swlib_compare_8859(cplob_val(swverid1_target.taglist, 0), cplob_val(swverid2_candidate.taglist, 0)):
            return swvidc.SWVERID_CMP_EQ
        else:
            return swvidc.SWVERID_CMP_NEQ
    else:
        if cplob_val(swverid1_target.taglist, 0) == cplob_val(swverid2_candidate.taglist, 0):
            return swvidc.SWVERID_CMP_EQ
        else:
            return swvidc.SWVERID_CMP_NEQ
def swverid_add_attribute(swverid, object_keyword, keyword, value):
    ver_id = [] * 3
    if object_keyword == "file" or object_keyword == "control_file":
        swverid.use_path_compare = 1
    c = swverid_get_ver_id_char(object_keyword, keyword)
    if c < 0:
        swuts_add_attribute(swverid.swuts, keyword, value)
        return 0
    ver_id[0] = ver_id[1] = ver_id[2] = '\0'
    if c == 0:
        if swverid.source_copy:
            swverid.source_copy = None
        swverid.source_copy = value
        cplob_additem(swverid.taglist, 0, swverid.source_copy)
        return 1
    if 0 and object_keyword:
        ver_id[0] = object_keyword
        ver_id[1] = c
    else:
        ver_id[0] = c
    verid_string = ver_id
    if ver_id[0] == 'r' or ver_id[1] == 'r':
        verid_string += "=="
    else:
        verid_string += "="
    verid_string += value
    version_id = create_version_id(verid_string)
    if version_id is None:
        return -1
    swverid_add_verid(swverid, version_id)
    return 1

#def swverid_get_comparison_sense(swverid1, swverid2):
#    return 0

def Swverid_Cmp_Code(swverid):
    swverid_get_comparison_code(swverid)

def swverid_get_comparison_code(swverid):
    return swverid.comparison_code

def swverid_set_comparison_code(swverid, code):
    swverid.comparison_code = code

def swverid_get_tag(swverid, n):
    return swverid.taglist[n]

def swverid_set_tag(swverid, key, value):
    if value is None:
        return
    if key == "catalog":
        if swverid.catalog:
            swverid.catalog = None
        swverid.catalog = value
    else:
        if cplob_val(swverid.taglist, 0):
            swverid.taglist = []
        cplob_additem(swverid.taglist, 0, value)

def swverid_get_ver_id_char(object_, attr_name):
    if attr_name == swc.SW_A_revision:
        return vidc.SWVERID_VERID_REVISION
    elif attr_name == swc.SW_A_architecture:
        return vidc.SWVERID_VERID_ARCHITECTURE
    elif attr_name == swc.SW_A_vendor_tag:
        return vidc.SWVERID_VERID_VENDOR_TAG
    elif attr_name == swc.SW_A_location:
        return vidc.SWVERID_VERID_LOCATION
    elif attr_name == swc.SW_A_qualifier:
        return vidc.SWVERID_VERID_QUALIFIER
    elif attr_name == swc.SW_A_tag:
        return 0
    elif attr_name == swc.SW_A_path:
        if object_ == "file" or object_ == swc.SW_A_distribution:
            return 0
        else:
            return -1
    else:
        return -1

def swverid_get_verid(swverid, fp_verid, occno):
    last = swverid.ver_id_list
    verid = fp_verid
    occ_count = 0
    if len(verid) > 2:
        print("invalid version id: %s" % fp_verid)
        return None
    if len(verid) > 1:
        verid = verid[1:]
    if len(verid) > 1:
        qualifier = verid
    else:
        qualifier = ""
    while last:
        if last.id[0] == verid:
            if len(qualifier) > 0:
                if len(last.vq) > 0:
                    if last.vq[0] == qualifier:
                        if occ_count >= occno - 1:
                            return last
                        occ_count += 1
                else:
                    if occ_count >= occno - 1:
                        return last
                    occ_count += 1
            else:
                if occ_count >= occno - 1:
                    return last
                occ_count += 1
        last = last.next
    return None

def swverid_print_ver_id(next_, buf):
    n = 0
    while next_:
        if n != 0:
            buf += ","
        buf += next_.vq
        buf += next_.id
        buf += next_.rel_op
        if next_.value is not None:
            buf += next_.value
        else:
            pass
        next_ = next_.next_
    return buf

def swverid_add_verid(swverid, verid):
    prev = None
    last = swverid.ver_id_list
    if verid is not None:
        verid.next = None
    if not last:
        swverid.ver_id_list = verid
        return
    while last:
        prev = last
        last = last.next
    prev.next = verid

def swverid_add_verid_if(swverid, verid):
    i_replace_verid(swverid, verid, 0)

def swverid_replace_verid(swverid, verid):
    i_replace_verid(swverid, verid, 1)

def swverid_get_alternate(swverid):
    return swverid.alt

def swverid_disconnect_alternates(swverid):
    parent = swverid
    while parent:
        oldparent = parent
        parent = parent.alt
        oldparent.alt = None

def swverid_debug_print(swverid, buf):
    tmp = strob_open(32)
    swverid_print(swverid, tmp)
    strob_strcpy(buf, "")
    strob_sprintf(buf, 0, "%d: %s", swverid.alter_uuid, strob_str(tmp))
    strob_close(tmp)
    return strob_str(buf)

def swverid_print(swverid, fp_buf):
    nb = None
    if fp_buf:
        buf = fp_buf
    else:
        if nb is None:
            nb = strob_open(32)
        buf = nb
    strob_strcpy(buf, "")
    swverid_i_print(swverid, buf)
    next_ = swverid
    while next_:
        strob_strcat(buf, "|")
        swverid_i_print(next_, buf)
        next_ = swverid_get_alternate(next_)
    ret = strob_str(buf)
    return ret

def swverid_show_object_debug(swverid, fp_buf, prefix):
    xx = swverid
    buf = None
    if fp_buf is None:
        if buf is None:
            buf = strob_open(32)
    else:
        buf = fp_buf
    buf1 = strob_open(10)
    strob_strcpy(buf, "")
    swverid_print(swverid, buf1)
    strob_sprintf(buf, 1, "%s%p (SWVERID*)\n", prefix, xx)
    strob_sprintf(buf, 1, "%s%p swverid_print() = [%s]\n", prefix, xx, swverid_print(swverid, buf1))
    strob_sprintf(buf, 1, "%s%p->object_name = [%s]\n", prefix, xx, xx.object_name)
    strob_sprintf(buf, 1, "%s%p->source_copy = [%s]\n", prefix, xx, xx.source_copy)
    strob_sprintf(buf, 1, "%s%p->catalog = [%s]\n", prefix, xx, xx.catalog)
    strob_sprintf(buf, 1, "%s%p->use_path_compare = [%d]\n", prefix, xx, xx.use_path_compare)
    strob_sprintf(buf, 1, "%s%p->comparison_code = [%d]\n", prefix, xx, xx.comparison_code)
    strob_sprintf(buf, 1, "%s%p->ver_id_list = (struct VER_ID*)%p\n", prefix, xx, xx.ver_id_list)
    strob_sprintf(buf, 1, "%s%p->ver_id_list = [%s]\n", prefix, xx, swverid_print_ver_id(xx.ver_id_list, buf1))
    in_ = 0
    strob_strcpy(buf1, "")
    while True:
        tag = cplob_val(xx.taglist, in_)
        if tag is None:
            break
        if in_ > 1:
            strob_sprintf(buf1, 1, ".", tag)
        strob_sprintf(buf1, 1, "%s", tag)
        in_ += 1
    strob_sprintf(buf, 1, "%s%p->taglist = (CPLOB*)(%p)\n", prefix, xx, xx.taglist)
    strob_sprintf(buf, 1, "%s%p->taglist = [%s]\n", prefix, xx, strob_str(buf1))
    strob_sprintf(buf, 1, "%s%p->namespace = [%d]\n", prefix, xx, xx.namespace)
    strob_sprintf(buf, STROB_DO_APPEND,
            "os_name=%s\n"
            "os_version=%s\n"
            "os_release=%s\n"
            "machine_type=%s\n",
            xx.swuts.sysnameM,
            xx.swuts.versionM,
            xx.swuts.releaseM,
            xx.swuts.machineM)
    return strob_str(buf)

def swverid_u_parse_swspec(swverid, swspec):
    savecplob = swverid.taglist
    list_ = cplob_open(3)
    swverid.taglist = list_
    savesource = swverid.source_copy
    swverid.source_copy = swspec
    parse_swspec_string(swverid)
    del swverid.source_copy
    swverid.taglist = savecplob
    swverid.source_copy = savesource
    return list_

def swverid_create_version_id(verid_string):
    ver_id = create_version_id(verid_string)
    return ver_id

def swverid_get_verid_value(swverid, fp_verid, occno):
    verid = swverid_get_verid(swverid, fp_verid, occno)
    if not verid:
        return None
    ret = verid.value
    return ret

def swverid_delete_non_fully_qualified_verids(swverid):
    retval = 0
    last = swverid.ver_id_list
    while last:
        if swlib_is_sh_tainted_string(last.value):
            tmp = last
            last = last.next
            swverid_ver_id_unlink(swverid, tmp)
            retval += 1
        else:
            last = last.next
    return retval




