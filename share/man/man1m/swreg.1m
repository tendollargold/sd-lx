.\" $Header: Bsheader placeholder $
.\" #BrickHeader# swreg.1m # 2023/08/20 # #
.TA s
.TH swreg 1M
.SH NAME
swreg \- register or unregister depots and roots
.SH SYNOPSIS
.HP
.C swreg
.C \-l
.I level
.B [\| \-u \|]
.B [\| \-v \|]
.B [\| \-C\|
.IR session_file \|]
.B [\| \-f
.IR object_file \|]
.ifn .br
.B [\| \-S
.IR session_file \|]
.B [\| \-t
.IR target_file \|]
.ift .br
.B [\| \-x
.IR option=value \|]
.ifn .br
.B [\| \-X
.IR option_file \|]
.RI [\| objects_to_(un)register \|]
.B [\| @
.IR target_selections \|]
.SS Remarks
.RS
.TP 3
\(bu
This command supports operations on remote systems.  See
.CT "Remote Operation"
below.
.TP
\(bu
For an overview of all SD commands, see the
.IR sd (5)
man page by
typing
.CR "man 5 sd"
on the command line.
.RE
.SH DESCRIPTION
The
.C swreg
command controls the visibility of depots and roots
to users who are performing software management tasks.
It must be used to register depots created by
.CR swpackage .
.PP
By default, the
.C swcopy
command registers newly created depots.
By default, the
.C swinstall
command registers newly created alternate roots (the root,
.CR "/" ,
is not automatically registered).  The
.C swremove
command unregisters a depot, or root,
when or if the depot is empty.  The user invokes
.C swreg
to explicitly (un)register a depot when the automatic behaviors of
.CR swcopy ,
.CR swinstall ,
.CR swpackage ,
and
.C swremove
do not suffice.  For example:
.RS
.TP 3
\(bu
Making a CD-ROM or other removable media available as a registered depot.
.TP
\(bu
Registering a depot created directly by
.CR swpackage .
.TP
\(bu
Unregistering a depot without removing it with
.CR swremove .
.RE
.SS Remote Operation
You can enable SD to manage software on remote
systems.  To let the root user from a central SD
.I controller
(also called the
.I "central management server"
or
.IR "manager node" ")"
perform operations on a remote
.I target
(also called the
.I "host"
or
.IR "agent" "):"
.P
.RS 0
.TP 4
1)
Set up the root, host, and template Access Control Lists (ACLs) on the
remote machines to permit root access from the controller system.  To do
this, run the following command on each remote system:
.IP
.CI "/usr/lib/sw/mx/setaccess " "controller"
.IP
NOTES:
.RS
.TP 3
\(bu
.I controller
is the name of the central management server.
.TP
\(bu
If remote system is 11.00, make sure SD patch PHCO_22526 or a
superseding patch is installed on remote system before running
.CR setaccess .
.TP
\(bu
If remote system is older than 11.00 or for some other
reason does not have
.CR setaccess
in place, copy
.CR setaccess
script from an
11.11 or higher system to the remote system.
.RE
.TP
2)
.CR swinstall ,
.CR swcopy ,
and
.CR swremove
have enhanced GUI interfaces for remote operations.  Enable
the enhanced GUIs by creating the
.CR ".sdkey"
file on the controller.  Use this command:
.IP
.C "touch /var/adm/sw/.sdkey"
.IP
See
.IR sd (5),
.IR swinstall (1M),
.IR swcopy (1M),
.IR swjob (1M),
.IR swlist (1M),
or
.IR swremove (1M)
for more information on interactive operations.
.RE
.PP
NOTE: You can also set up remote access by using
.CR swacl
directly on the remote machines to grant root or non-root access to
users from the controller system.
.SS Options
The
.C swreg
command supports the following options:
.RS
.TP 15
.CI -l \0level
Specify the
.I level
of the object to register or unregister.
Exactly one level
must be specified.  The supported levels are:
.PD 0
.RS
.TP 10
.C depot
The object to be registered is a depot.
.TP
.C root
The object to be registered is a root.
.TP
.C "shroot"
The object to register is a shared root (HP-UX 10.X only).
.TP
.C "prroot"
The object to register is a private root (HP-UX 10.X only).
.PD
.RE
.TP 15
.C -u
Causes
.C swreg
to unregister the specified objects instead of registering them.
.TP
.C -v
Turns on verbose output to stdout.
(The
.C swreg
logfile is not affected by this option.)
Verbose output is enabled by default, see the
.C verbose
option below.
.TP
.CI -C \0session_file
Save the current options and operands only to the
.IR session_file .
You can enter a relative or absolute path with the file name.
The default directory for session files is
.CR $HOME/.sw/sessions/ .
Without this option, by default, the session file is saved only
in the default directory
.CR $HOME/.sw/sessions/ .
.IP
You can recall a session file with the
.CR -S
option.
.TP
.CI -f \0object_file
Read the list of depot or root objects
to register or unregister from
.I object_file
instead of (or in addition to) the command line.
.TP
.CI -S \0session_file
Execute
.C swreg
based on the options and operands saved from a previous session,
as defined in
.IR session_file .
You can save session information to a file with the
.C -C
option.
.TP
.CI -t \0target_file
Read the list of target
.I hosts
on which to register the depot or root objects from
.I target_file
instead of (or in addition to) the command line.
.TP
.CI -x \0option=value
Set the session
.I option
to
.I value
and override the default value (or a value in an alternate
.IR option_file
specified with
the
.C -X
option).
Multiple
.C -x
options can be specified.
.TP
.CI -X \0option_file
Read the session options and behaviors from
.IR option_file .
.RE
.SS Operands
The
.C swreg
command supports the following syntax for each
.IR object_to_register :
.IP
.C "path"
.PP
Each operand specifies an object to be registered or unregistered.
.PP
The
.C swreg
command supports the following syntax for each
.IR target_selection :
.IP
.RI [\| host \|]
.SH EXTERNAL INFLUENCES
.SS Default Options
.PP
In addition to the standard options, several SD behaviors and policy options
can be changed by editing the default values found in:
.RS
.TP 30
.C /var/adm/sw/defaults
the system-wide default values.
.TP
.C $HOME/.swdefaults
the user-specific default values.
.RE
.P
Values must be specified in the defaults file using this syntax:
.P
.RS
.nf
.C "\f1[\|\fP\s-1\f2command_name\fP\s0.\f1\|]\fP\s-1\f2option\fP\s0=\s-1\f2value\fP\s0"
.fi
.RE
.PP
The optional
.EM "command_name"
prefix denotes one of the SD commands.  Using the prefix limits the
change in the default value to that command.  If you leave the prefix
off, the change applies to all commands.
.PP
You can also override default values from the command line with the
.C "-x"
or
.C "-X"
options:
.P
.RS
.nf
.IC "command " "-x " "option" "=" "value"
.P
.IC "command " "-X " "option_file"
.fi
.RE
.PP
The following list describes keywords supported by the
.C swreg
command.  If a default value exists, it is listed after the
.CR = .
.PP
.RS
.TP 10
.PD 0
.CR "admin_directory=/var/adm/sw" " (for normal mode)"
.PD 0
.TP
.CR "admin_directory=/var/home/LOGNAME/sw" " (for nonprivileged mode)"
.PD
The location for SD logfiles and the default parent directory for the
installed software catalog.  The default value is
.CR "/var/adm/sw"
for normal SD operations.  When SD operates in nonprivileged mode
(that is, when the
.CR run_as_superuser
default option is set to
.CR "true" "):"
.RS
.TP 3
\(bu
The default value is foBed to
.CR "/var/home/LOGNAME/sw" .
.TP
\(bu
The path element
.CR "LOGNAME"
is replaced with the name of the invoking user, which SD reads from
the system password file.
.TP
\(bu
If you set the value of this option to
.CR "HOME/"\c
.IR "path" ","
SD replaces
.CR HOME
with the invoking user's home directory (from the system password
file) and resolves
.I path
relative to that directory.  For example,
.CR HOME/my_admin
resolves to the
.CR my_admin
directory in your home directory.
.\" This doesn't apply because the installed_software_catalog doesn't
.\" apply to swjob: 4/30/00, 11.11
.\" .TP
.\" \(bu
.\" If you set the value of the
.\" .C installed_software_catalog
.\" default option to a relative path, that path is resolved relative to
.\" the value of this option.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.
.IP
See also the
.CR run_as_superuser
option.
.TP
.C distribution_target_directory=/var/spool/sw
Defines the location of the depot object to register if no objects
are specified and the
.C -l
option is specified.
.TP
.C level=
Defines the default level of objects to register or unregister.  The
valid levels are:
.RS
.PD 0
.TP 15
.C "depot"
Depots which exist at the specified target hosts.
.TP
.C root
All alternate roots.
.TP
.C shroot
All registered shared roots \c
.IR "(HP-UX 10.X only)" "."
.TP
.C prroot
All registered private roots  \c
.IR "(HP-UX 10.X only)" "."
.PD
.RE
.\"
.TP
.C log_msgid=0
Adds numeric identification numbers at the beginning of SD logfile
messages:
.RS
.PD 0
.TP 3
.C 0
(default) No identifiers are attached to messages.
.TP
.C 1
Adds identifiers to ERROR messages only.
.TP
.C 2
Adds identifiers to ERROR and WARNING messages.
.TP
.C 3
Adds identifiers to ERROR, WARNING, and NOTE messages.
.TP
.C 4
Adds identifiers to ERROR, WARNING, NOTE, and certain other
informational messages.
.PD
.RE
.\"
.TP
.C logfile=/var/adm/sw/swreg.log
Specifies the default command log file for the
.C swreg
command.
.TP
.C logdetail=false[\|true\|]
The
.C logdetail
option controls the amount of detail written to the log file.  When set
to
.CR true ,
this option adds detailed task information (such as options specified,
progress statements, and additional summary information) to the
log file.  This information is in addition to log information controlled
by the
.C loglevel
option.  See the
.IR sd (5)
man page for additional information by typing
.CR "man 5 sd" .
.TP
.C loglevel=1
Controls the log level for the events logged to the command logfile, the
target agent logfile, and the souBe agent logfile.  This information
is in addition to the detail controlled by the
.C logdetail
option.  (See also
.CR logdetail .)
A value of:
.RS
.PD 0
.TP 4
.C 0
provides no information to the logfile.
.TP
.C 1
enables verbose logging to the logfiles.
.TP
.C 2
enables very verbose logging to the logfiles.
.PD
.RE
.TP
.C lookupcache_timeout_minutes=10
Controls the time in minutes to cache and re-use the results of hostname
or IP address resolution lookups.  A value of 0 disables the facility to
cache and re-use lookup results.  The maximum value allowed is 10080 minutes,
which is one week.
.IP
A value of:
.RS
.PD 0
.TP 10
.C 0
disables the lookup caching mechanism.
.TP
.C 10080
is the maximum value allowed.
.PD
.RE
.TP
.C objects_to_register=
Defines the default objects to register or unregister.  There is no
supplied default (see
.C distribution_target_directory
above).
If there is more than one object, they must be separated by spaces.
.TP
.C return_warning_exitcode=false
This option controls the exit code returned by SD's controller commands.
This option is applicable only for a single target operation,
and ignored when multiple targets are used.
.IP
When set to the default value of
.CR false ,
swreg returns:
.RS
.PD
.TP 3
0
If there were no errors, with or without warnings.
.TP
1
If there were errors.
.P 0
When set to
.CR true ,
swreg returns :
.TP 3
0
If there were no warnings and no errors.
.TP
1
If there were errors.
.TP
2
If there were warnings but no errors.
.PD
.RE
.TP
.C rpc_binding_info=ncacn_ip_tcp:[\|2121\|] ncadg_ip_udp:[\|2121\|]
Defines the protocol sequence(s) and endpoint(s) on which the daemon
listens and which the other commands use to contact the daemon.
If the connection fails for one protocol sequence, the next is
attempted.
SD supports both the tcp
.B ( ncacn_ip_tcp:[\|2121\|] )
and udp
.B ( ncadg_ip_udp:[\|2121\|] )
protocol sequence on most platforms.
.IP
See the
.IR sd (5)
man page by typing
.C man 5 sd
for details on specifying this option.
.TP
.C rpc_timeout=5
Relative length of the communications timeout.  This is a value in the
range from 0 to 9 and is interpreted by the DCE RPC.  Higher values
mean longer times; you may need a higher value for a slow or busy
network.  Lower values will give faster recognition on attempts to
contact hosts that are not up or are not running
.CR swagentd .
Each value is approximately twice as long as the preceding value.
A value of 5 is about 30 seconds for the
.C ncadg_ip_udp
protocol sequence.
This option may not have any noticeable impact when using the
.C ncacn_ip_tcp
protocol sequence.
.\"
.TP
.C run_as_superuser=true
This option controls SD's nonprivileged mode.  This option is ignored
(treated as true) when the invoking user is super-user.
.IP
When set to the default value of true, SD operations are performed
normally, with permissions for operations either granted to a local
super-user or set by SD ACLs.  (See
.IR swacl (1M)
for details on ACLs.)
.IP
When set to false and the invoking user is local and is
.I not
super-user, nonprivileged mode is invoked:
.RS
.TP 3
\(bu
Permissions for operations are based on the user's file system
permissions.
.TP
\(bu
SD ACLs are ignored.
.TP
\(bu
Files created by SD have the uid and gid of the invoking user, and the
mode of created files is set according to the invoking user's umask.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.
.IP
See also the
.CR admin_directory
option.
.TP
.C select_local=true
If no
.IR target_selections
are specified,
select the default
.C distribution_target_directory
of the local host as the
.IR target_selection
for the command.
.TP
.C targets=
Defines the default
.IR target
hosts on which to register or unregister the specified root or depot objects.
There is no supplied default (see
.C select_local
above).
If there is more than one target selection, they must be separated by spaces.
.TP
.C verbose=1
Controls the verbosity of the
.C swreg
output (stdout).
A value of
.RS
.PD 0
.TP 4
.C 0
disables output to stdout.  (Error and warning messages
are always written to stderr).
.TP
.C 1
enables verbose messaging to stdout.
.PD
.RE
.RE
.SS Session File
Each invocation of the
.C swreg
command defines a registration session.  The invocation options,
souBe information, software selections, and target hosts are saved
before the installation or copy task actually commences.  This lets
you re-execute the command even if the session ends before proper
completion.
.PP
Each session is saved to the file
.CR $HOME/.sw/sessions/swreg.last .
This file is overwritten by each invocation of
.CR swreg .
.PP
You can also save session information to a specific file by executing
.C swreg
with the
.C -C
.I session_file
option.
.PP
A session file uses the same syntax as the defaults files.
You can specify an absolute path for the session file.  If you do
not specify a directory, the default location for a session file is
.CR $HOME/.sw/sessions/ .
.PP
To re-execute a session file, specify the session file as the argument for the
.C -S
.I session_file
option of
.CR swreg .
.PP
Note that when you re-execute a session file, the values in the session
file take precedence over values in the system defaults file.
Likewise, any command line options or parameters that you specify when
you invoke
.CR swreg
take precedence over the values in the session file.
.SS Environment Variables
SD programs are affected by external environment variables.
.PP
SD programs that execute control scripts set environment variables for
use by the control scripts.
.PP
In addition,
.CD swinstall
sets environment variables for use when updating the HP-UX operating
system and modifying the HP-UX configuration.
.PP
The environment variables that affect the
.C swreg
command are:
.PP
.RS
.TP 13
.EV LANG
Determines the language in which messages are displayed.
If
.EV LANG
is not specified or is set to the empty string, a
default value of
.C C
is used.
See the
.IR lang (5)
man pages by typing
.CR "man 5 lang"
for more information.
.IP
NOTE: The language in which the SD agent and daemon log messages
are displayed is set by the system configuration variable script,
.CR /etc/B.config.d/LANG .
For example,
.CR /etc/B.config.d/LANG ,
must be set to
.CR LANG=ja_JP.SJIS
or
.CR LANG=ja_JP.eucJP
to make the agent and daemon log messages display in Japanese.
.TP
.EV LC_ALL
Determines the locale to be used to override any values for locale
categories specified by the settings of
.EV LANG
or any environment variables beginning with
.EV LC_ .
.TP
.EV LC_CTYPE
Determines the interpretation of sequences of bytes of text data as
characters (for example, single versus multibyte characters in values for
vendor-defined attributes).
.TP
.EV LC_MESSAGES
Determines the language in which messages should be written.
.TP
.EV LC_TIME
Determines the format of dates
.RI ( create_date
and
.IR mod_date )
when displayed by
.CR swlist .
Used by all utilities when displaying dates and times in
.CR stdout ,
.CR stderr ,
and
.CR logging .
.TP
.EV TZ
Determines the time zone for use when displaying dates and times.
.RE
.SS Signals
The
.C swreg
command
catches the signals SIGQUIT and SIGINT.
If these signals are received,
.C swreg
prints a message, sends
a Remote Procedure Call (RPC) to the daemons to wrap up, and then exits.
.SH RETURN VALUES
The
.C swreg
command returns:
.RS
.TP 4
.C 0
The
.I objects_to_register
were successfully (un)registered.
.PD 0
.TP
.C 1
The register or unregister operation failed on
all
.IR target_selections .
.PD 0
.TP
.C 2
The register or unregister operation failed on
some
.IR target_selections .
.PD
.RE
.SH DIAGNOSTICS
The
.C swreg
command writes to stdout, stderr, and to the daemon logfile.
.SS Standard Output
The
.C swreg
command writes messages for significant events.
These include:
.RS
.TP 3
\(bu
a begin and end session message,
.PD 0
.TP
\(bu
selection and execution task messages for each
.IR target_selection .
.PD
.RE
.SS Standard Error
The
.C swreg
command writes messages for all WARNING and ERROR
conditions to stderr.
.SS Logging
The
.C swreg
command logs summary events at the host where the command was invoked.
It logs events about each (un)register operation to
the
.C swagentd
logfile associated with each
.IR target_selection .
.SS swagentd Disabled
If the
.CR swagentd
daemon has been disabled on the host, it can be enabled
by the host's system administrator by setting the
.CR SW_ENABLE_SWAGENTD
entry in
.CR /etc/B.config.d/swconfig
to
.CR 1
and executing
.CR "/usr/sbin/swagentd -r" .
.SH EXAMPLES
Create a new depot with
.CR swpackage ,
then register it with
.CR swreg :
.RS
.nf
.P
.C "swpackage -s psf -d /var/spool/sw"
.C "swreg -l depot  /var/spool/sw"
.fi
.RE
.PP
Unregister the default depot at several hosts:
.P
.RS
.nf
.C "swreg -u -l depot /var/spool/sw @ hostA hostB hostC"
.fi
.RE
.PP
Unregister a specific depot at the local host:
.P
.RS
.nf
.C "swreg -u -l depot /cdrom"
.fi
.RE
.SH FILES
.RS 0
.TP 8
.C $HOME/.swdefaults
Contains the user-specific default values for some or all SD options.
.TP
.C /usr/lib/sw/sys.defaults
Contains the master list of current SD options with their default values.
.TP
.C /var/adm/sw/
The directory which contains all of the configurable
and non-configurable data for SD.
This directory is also the default location of logfiles.
.TP
.C /var/adm/sw/defaults
Contains the active system-wide default values for some or all SD options.
.TP
.C /var/adm/sw/host_object
The file which stores the list of depots registered at the local host.
.RE
.SH AUTHOR
.C swreg
was developed by the Hewlett-Packard Company.
.SH SEE ALSO
install-sd(1M),
swacl(1M),
swagentd(1M),
swask(1M),
swconfig(1M),
swcopy(1M),
swinstall(1M),
swjob(1M),
swlist(1M),
swmodify(1M),
swpackage(1M),
swremove(1M),
swverify(1M),
sd(4),
swpackage(4),
sd(5).
.P
.CT "Software Distributor Administration Guide" ","
available at
.CR "http://docs.hp.com" .
.P
SD customer web site at
.\" JAGaf61810: Updated the URL for the SD customer web site.
.\"   NOTE TO LOCALIZERS: Currently, only the English version of this
.\"   web site exists.
.CR "http://docs.hp.com/en/SD/" .
.\"
.\" index@\f4swreg\f1 \- register or unregister depots and roots@@@\f3swreg(1M)\f1
.\" index@\f1depots and roots; register or unregister@@@\f3swreg(1M)\f1
.\" index@\f1roots and depots; register or unregister@@@\f3swreg(1M)\f1
.\" index@\f1register or unregister depots and roots@@@\f3swreg(1M)\f1
.\" index@\f1unregister or register depots and roots@@@\f3swreg(1M)\f1
.\" toc@\f3swreg(1M)\f1:\0\0\f4swreg\f1@@@register or unregister depots and roots
