

#https://pubs.opengroup.org/onlinepubs/9629899/docix.htm

# The descriptions in the following tables describe the conditions that lead 
# to this event, and the set of possible event status values for the event. 
# The tables also include "Manager info" and "Target info" that describe the
# additional information that may be logged for manager and target role event 
# logging respectively. See Logging .

# General Error Events lists general source and target role events. The way in
# which some of these events are generated (if at all) may be different for 
# different implementation

class GeneralErrorEvents(Exception):
  def __init__(self, message, event, error_code):
    super().__init__(message)
    self.event = event
    self.error_code = error_code


GENERAL_ERROR_EVENTS = {
  1: "SW_ILLEGAL_STATE_TRANSITION",
  2: "SW_BAD_SESSION_CONTEXT",
  3: "SW_ILLEGAL_OPTION",
  4: "SW_ACCESS_DENIED",
  5: "SW_MEMORY_ERROR",
  6: "SW_RESOURCE_ERROR",
  7: "SW_INTERNAL_ERROR", 
  8: "SW_IO_ERROR"

}

# Session Events lists the source and target role events related to 
# initialization of a session and ending a session. The way in which some of 
# these events are generated (if at all) may be different for different 
# implementations

SESSION_EVENTS = {
  10: "SW_AGENT_INITIALIZATION_FAILED",
  11: "SW_SERVICE_NOT_AVAILABLE",
  12: "SW_OTHER_SESSIONS_IN_PROGRESS",
  28: "SW_SESSION_BEGINS",
  29: "SW_SESSION_ENDS",
  30: "SW_CONNECTION_LIMIT_EXCEEDED",
  31: "SW_SOC_DOES_NOT_EXIST",
  32: "SW_SOC_IS_CORRUPT",
  34: "SW_SOC_CREATED",
  35: "SW_CONFLICTING_SESSION_IN_PROGRESS",
  36: "SW_SOC_LOCK_FAILURE",
  37: "SW_SOC_IS_READ_ONLY",
  38: "SW_SOC_IS_REMOTE",
  39: "SW_SOC_INCORRECT_MEDIA_TYPE",
  40: "SW_SOC_IS_SERIAL",
  41: "SW_SOC_INCORRECT_TYPE",
  42: "SW_CANNOT_OPEN_LOGFILE",
  49: "SW_SOC_AMBIGUOUS_TYPE",
  50: "SW_TERMINATION_DELAYED",
  51: "SW_CANNOT_INITIATE_REBOOT"
}

SW_ERROR = {
  1: "SW_ILLEGAL_STATE_TRANSITION, The manager is requesting a phase out of order. Manager info: target. Target info: current phase.",
  2: "SW_BAD_SESSION_CONTEXT,	The manager has contacted the wrong target, or this is not a valid manager for this session. Manager info: target. Target info: information about the initiator.",
  3: "SW_ILLEGAL_OPTION,	An illegal or unrecognized option was sent. Manager info: target, number of options. Target info: option names and values.",
  4: "SW_ACCESS_DENIED, The user has insufficient privilege to perform the requested operation. Manager info: target. Target info: information about the initiator.",
  5: "SW_MEMORY_ERROR, The target role had a memory allocation error (for example, out of swap). Manager info: target. Target info: reasons for error.",
  6: "SW_RESOURCE_ERROR, The target role had a resource allocation error such as maximum number of processes reached, maximum number of files open, etc. Manager info: target. Target info: reasons for error.",
  7: "SW_INTERNAL_ERROR, The target role had an internal implementation error. Manager info: target. Target info: reasons for error.",
  8: "SW_IO_ERROR, An I/O error occurred while performing this command. Manager info: target. Target info: reasons for error.",
  10: "SW_AGENT_INITIALIZATION_FAILED,	Failed to initialize a target session. Manager info: target. Target info: reasons for error.",
  11: "SW_SERVICE_NOT_AVAILABLE, The target role is not accepting new requests. Manager info: target. Target info: reasons for error.",
  29: "SW_SESSION_ENDS, The command ends on the target successfully, with warnings, or with errors. Manager info: target. Target info: none.",
  30: "SW_CONNECTION_LIMIT_EXCEEDED, The limit of source or target role sessions on this host has already been reached. Manager info: target, number of sessions. Target info: number of sessions, limit.",
  31: "SW_SOC_DOES_NOT_EXIST, The requested target or source software collections does not exist. Manager info: target. Target info: reasons for error.",
  32: "SW_SOC_IS_CORRUPT,	The software_collection exists, but the information is corrupt. Manager info: target. Target info: reasons for error.",
  35: "SW_CONFLICTING_SESSION_IN_PROGRESS, A conflicting session is in progress that will prevent this operation (error), or cause its results to possibly be invalid (warning). Manager info: target. Target info: information about other sessions.",
  36: "SW_SOC_LOCK_FAILURE, Can not set the proper access control to this source or target. Manager info: target. Target info: reasons for error.",
  37: "SW_SOC_IS_READ_ONLY, The target to be modified is a read only source.",
  38: "SW_SOC_IS_REMOTE, The software_collection is on a remote file system. (Whether note or error is implementation defined). Manager info: target. Target info: none.",
  39: "SW_SOC_INCORRECT_MEDIA_TYPE, The distribution is an incorrect type for the command (for example, a tape for swremove). Manager info: target. Target info: reasons for error.",
  41: "SW_SOC_INCORRECT_TYPE, The software_collection is of the wrong type (distribution or installed_software) for the operation. Manager info: target. Target info: target type.",
  42: "SW_CANNOT_OPEN_LOGFILE, Cannot open logfile to log the software_collection events. Manager info: target. Target info: reasons for error.",
  49: "SW_SOC_AMBIGUOUS_TYPE, The software collection is inadequately specified for the operation. Manager info: target. Target info: reason for error",
  53: "SW_ANALYSIS_ENDS, The analysis phase ends on the target. The analysis may have succeeded, had warnings, and/or errors. Manager info: target. Target info: none.",
  59: "SW_SELECTION_IS_CORRUPT, The software selection was found, but its state was corrupt or transient. Manager info: target, number of selections Target info: software_specs.",
  60: "SW_SOURCE_ACCESS_ERROR, Failure contacting or retrieving information from the source. Manager info: target. Target info: reasons for error.",
  61: "SW_SOURCE_NOT_FIRST_MEDIA, The source does not have a media number of 1 (needed for retrieval of the INDEX). Manager info: target, current media number. Target info: current media number.",
  62: "SW_SELECTION_NOT_FOUND, One or more software selections can not be found. This is an error for install or copy; otherwise, a note. Manager info: target, number of selections. Target info: software_specs.",
  63: "SW_SELECTION_NOT_FOUND_RELATED, One or more software selections can not be found as specified, but another version exists. This is an error for install or copy; otherwise, a note. Manager info: target, number of selections. Target info: software_specs.",
  64: "SW_SELECTION_NOT_FOUND_AMBIG, One or more software selections can not be unambiguously determined. Manager info: target, number of selections. Target info: software_specs.",
  65: "SW_FILESYSTEMS_NOT_MOUNTED, One or more file systems on the target are not mounted. Manager info: target, number of file systems. Target info: file system names.",
  67: "SW_HIGHER_REVISION_INSTALLED, One or more filesets have a higher revision already installed. Whether error or warning is controlled by allow_downdate option. Manager info: target, number of filesets. Target info: software_specs.",
  68: "SW_NEW_MULTIPLE_VERSION, One or more products would create a new version in an installation. Whether error or warning is controlled by allow_multiple_versions option. Manager info: target, number of products. Target info: software_specs.",	
  69: "SW_EXISTING_MULTIPLE_VERSION, The command is operating on an existing multiple version of one or more products. If trying to install two versions into one location, generate an event. Warning or note controlled by allow_multiple_versions option. Manager info: target, number of products. Target info: software_specs.",
  70: "SW_DEPENDENCY_NOT_MET, One or more dependencies can not be met. Whether error or warning is controlled by enforce_dependencies option. Manager info: target, number of filesets. Target info: software_specs, dependency_specs",
  71: "SW_NOT_COMPATIBLE, One or more products are incompatible for this target. Whether error or warning is controlled by allow_incompatible option. Manager info: target, number of products. Target info: software_specs.",
  73: "SW_CHECK_SCRIPT_ERROR, One or more checkinstall, checkremove or verify scripts failed. Manager info: target, number of filesets. Target info: software_specs.",
  74: "SW_DSA_INTO_MINFREE, Disk space analysis is over the minimum free limit, but not the overall limit on the target. Whether error or warning is controlled by enforce_dsa option. Manager info: target, number of file systems. Target info: file system names, amount over the minimum free.",
  75: "SW_DSA_OVER_LIMIT, Disk space analysis is over the absolute limit. Whether error or warning is controlled by enforce_dsa option. Manager info: target, number of file systems. Target info: file system names, amount over the limit.",
  76: "SW_DSA_FAILED_TO_RUN, Disk space analysis had an internal error and failed to run. Whether error or warning is controlled by enforce_dsa option. Manager info: target, number of filesets. Target info: software_specs",
  85: "SW_FILE_ERROR, One or more files had errors in analysis or execution. Manager info: target, number of files. Target info: file paths.",
  86: "SW_NOT_LOCATABLE, A fileset is not locatable. Controlled by the enforce_locatable option. Manager info: target, number of filesets. Target info: software_specs",
  89: "SW_EXECUTION_ENDS, The execution phase ends on the target. Manager info: target. Target info: none.",
  91: "SW_SELECTION_NOT_ANALYZED, One or more software selections were found, but were not analyzed. Manager info: target, number of filesets. Target info: software_specs.",
  92: "SW_WRONG_MEDIA_SET, The source media current being used is not the same as that used for analysis. Manager info: target. Target info: information about current media and needed media.",
  96: "SW_PRE_SCRIPT_ERROR, One or more preinstall, preremove, unpreinstall, or fix scripts failed. Manager info: target, number of scripts. Target info: software_spec , script tag",
  98: "SW_FILESET_ERROR, One or more filesets had an error. Manager info: target, number of filesets. Target info: software_spec s",
  100: "SW_POST_SCRIPT_ERROR, One or more postinstall, postremove, or unpostinstall scripts failed. Manager info: target, number of filesets. Target info: software_specs",
  102: "SW_POSTKERNEL_ERROR, The kernel failed to build. Manager info: target. Target info: reasons for error.",
  104: "SW_CONFIGURE_ERROR, One or more configure or unconfigure scripts failed. Manager info: target, number of scripts. Target info: software_specs, script tags.",
  105: "SW_DATABASE_UPDATE_ERROR, An update to the catalog information for installed_software or distributions failed. Manager info: target. Target info: reasons for error.",
  107: "SW_REQUEST_ERROR, One or more request scripts failed. Manager info: software_specs.",
  112: "SW_COMPRESSION_FAILURE, A file could not be compressed or uncompressed. Manager info: target. Target info: filepath.",
  113: "SW_FILE_NOT_FOUND, File is missing from the source or target software_collection. Manager info: target, number of files. Target info: file path."

}

SW_WARNING = {
  12: "SW_OTHER_SESSIONS_IN_PROGRESS, There are other sessions in progress that may affect the results of this command. Manager info: target, number of sessions. Target info: information about other sessions.",
  29: "SW_SESSION_ENDS, The command ends on the target successfully, with warnings, or with errors. Manager info: target. Target info: none.",
  35: "SW_CONFLICTING_SESSION_IN_PROGRESS, A conflicting session is in progress that will prevent this operation (error), or cause its results to possibly be invalid (warning). Manager info: target. Target info: information about other sessions.",
  51: "SW_CANNOT_INITIATE_REBOOT, The target role failed to initiate the reboot operation of an install command and requires manual reboot. Manager info: target. Target info: reasons for error.",
  53: "SW_ANALYSIS_ENDS, The analysis phase ends on the target. The analysis may have succeeded, had warnings, and/or errors. Manager info: target. Target info: none.",
  65: "SW_FILESYSTEMS_NOT_MOUNTED, One or more file systems on the target are not mounted. Manager info: target, number of file systems. Target info: file system names.",
  66: "SW_FILESYSTEMS_MORE_MOUNTED,	One or more file systems mounted are not in file system table. Manager info: target, number of file systems. Target info: file system names.",
  67: "SW_HIGHER_REVISION_INSTALLED, One or more filesets have a higher revision already installed. Whether error or warning is controlled by allow_downdate option. Manager info: target, number of filesets. Target info: software_specs.",
  69: "SW_EXISTING_MULTIPLE_VERSION, The command is operating on an existing multiple version of one or more products. If trying to install two versions into one location, generate an event. Warning or note controlled by allow_multiple_versions option. Manager info: target, number of products. Target info: software_specs.",
  70: "SW_DEPENDENCY_NOT_MET, One or more dependencies can not be met. Whether error or warning is controlled by enforce_dependencies option. Manager info: target, number of filesets. Target info: software_specs, dependency_specs	70",
  71: "SW_NOT_COMPATIBLE, One or more products are incompatible for this target. Whether error or warning is controlled by allow_incompatible option. Manager info: target, number of products. Target info: software_specs",
  72: "SW_CHECK_SCRIPT_WARNING, One or more checkinstall, checkremove or verify scripts had a warning. Manager info: target, number of filesets. Target info: software_specs.",
  74: "SW_DSA_INTO_MINFREE, Disk space analysis is over the minimum free limit, but not the overall limit on the target. Whether error or warning is controlled by enforce_dsa option. Manager info: target, number of file systems. Target info: file system names, amount over the minimum free.",
  75: "SW_DSA_OVER_LIMIT, Disk space analysis is over the absolute limit. Whether error or warning is controlled by enforce_dsa option. Manager info: target, number of file systems. Target info: file system names, amount over the limit.",
  76: "SW_DSA_FAILED_TO_RUN, Disk space analysis had an internal error and failed to run. Whether error or warning is controlled by enforce_dsa option. Manager info: target, number of filesets. Target info: software_specs",
  81: "SW_FILE_IS_REMOTE, One or more files would be created or removed on a remote file system. (Policy for loading remote files is implementation defined). Manager info: target, number of files. Target info: file paths.",
  82: "SW_FILE_IS_READ_ONLY, One or more files will not be attempted to be created or removed on a read only file system. Manager info: target, number of files. Target info: file paths.",
  83: "SW_FILE_NOT_REMOVABLE, One or more files could not be removed (for example, text busy, or non-empty directories). Manager info: target, number of files. Target info: file paths.",
  84: "SW_FILE_WARNING, One or more files had warnings in analysis or execution. Manager info: target, number of files. Target info: file paths.",
  86: "SW_NOT_LOCATABLE, A fileset is not locatable. Controlled by the enforce_locatable option. Manager info: target, number of filesets. Target info: software_specs",
  89: "SW_EXECUTION_ENDS, The execution phase ends on the target. Manager info: target. Target info: none.",
  95: "SW_PRE_SCRIPT_WARNING, One or more preinstall , preremove , unpreinstall , or fix scripts had a warning. Manager info: target, number of scripts. Target info: software_spec , script tag",
  97: "SW_FILESET_WARNING, One or more filesets had a warning. Manager info: target, number of filesets. Target info: software_specs",
  99: "SW_POST_SCRIPT_WARNING, One or more postinstall, postremove, or unpostinstall scripts had a warning. Manager info: target, number of filesets. Target info: software_specs",
  96: "SW_PRE_SCRIPT_ERROR, One or more preinstall , preremove , unpreinstall , or fix scripts failed. Manager info: target, number of scripts. Target info: software_spec , script tag",
  100: "SW_POST_SCRIPT_ERROR, One or more postinstall , postremove , or unpostinstall scripts failed. Manager info: target, number of filesets. Target info: software_specs",
  101: "SW_POSTKERNEL_WARNING, The postkernel kernel build script had a warning. Manager info: target. Target info: reasons for error.",
  103: "SW_CONFIGURE_WARNING, One or more configure or unconfigure scripts had a warning. Manager info: target, number of scripts. Target info: software_specs, script tag",
  104: "SW_CONFIGURE_ERROR, One or more configure or unconfigure scripts failed. Manager info: target, number of scripts. Target info: software_specs, script tag",
  106: "SW_REQUEST_WARNING, One or more request scripts had a warning. Manager info: software_specs."
}

SW_NOTE = {
  28: "SW_SESSION_BEGINS, The command begins on the target. Manager info: target. Target info: information about the initiator of the command.",
  29: "SW_SESSION_ENDS, The command ends on the target successfully, with warnings, or with errors. Manager info: target. Target info: none.",
  34: "SW_SOC_CREATED, The target software_collection did not previously exist and was created. Manager info: target. Target info: none.",
  37: "SW_SOC_IS_READ_ONLY,	The software_collection is a read only source for a read source or target. Manager info: target. Target info: none.",
  38: "SW_SOC_IS_REMOTE, The software_collection is on a remote file system. (Whether note or error is implementation defined). Manager info: target. Target info: none.",
  40: "SW_SOC_IS_SERIAL, The distribution has a serial format (for example, a tape). Manager info: target. Target info: none.",
  50: "SW_TERMINATION_DELAYED, The target role is currently analyzing or executing a command and will terminate the session once completed. Manager info: target. Target info: none.",
  52: "SW_ANALYSIS_BEGINS, The analysis phase begins on the target. Manager info: target. Target info: none.",
  53: "SW_ANALYSIS_ENDS, The analysis phase ends on the target. The analysis may have succeeded, had warnings, and/or errors.",
  56: "SW_EXREQUISITE_EXCLUDE, One or more filesets were excluded automatically as the software identified as exrequisites was also specified to be selected. Manager info: target, number of filesets. Target info: software_specs.",
  57: "SW_CHECK_SCRIPT_EXCLUDE,	One or more checkinstall or checkremove scripts have caused the software to be unselected and excluded from further processsing. Manager info: target, number of filesets. Target info: software_specs.",
  58: "SW_CONFIGURE_EXCLUDE, One or more configure or unconfigure scripts have caused the software to be unselected and excluded from further processsing. Manager info: target, number of filesets. Target info: software_specs.",
  62: "SW_SELECTION_NOT_FOUND, One or more software selections can not be found.",
  63: "SW_SELECTION_NOT_FOUND_RELATED, One or more software selections can not be found as specified, but another version exists. This is an error for install or copy; otherwise, a note. Manager info: target, number of selections. Target info: software_specs.",
  68: "SW_NEW_MULTIPLE_VERSION, One or more products would create a new version in an installation. Whether error or warning is controlled by allow_multiple_versions option. Manager info: target, number of products. Target info: software_specs.",	
  69: "SW_EXISTING_MULTIPLE_VERSION, The command is operating on an existing multiple version of one or more products. If trying to install two versions into one location, generate an event. Warning or note controlled by allow_multiple_versions option. Manager info: target, number of products. Target info: software_specs.",
  77: "SW_SAME_REVISION_INSTALLED, One or more filesets have the same revision and are being reinstalled because reinstalltrue or recopytrue. Manager info: target, number of filesets. Target info: software_specs.",
  78: "SW_ALREADY_CONFIGURED, One or more filesets are already configured Whether they are reconfigured is controlled by reconfigure option. Manager info: target, number of filesets. Target info: software_specs.",
  79: "SW_SKIPPED_PRODUCT_ERROR, One or more filesets will be skipped because of another error within their product. (Error handling is implementation defined). Manager info: target, number of filesets. Target info: software_specs.",
  80: "SW_SKIPPED_GLOBAL_ERROR, One or more filesets will be skipped because of a global error (such as disk space analysis failure) within the analyze phase. (Error handling is implementation defined). Manager info: target, number of filesets. Target info: software_specs.",
  81: "SW_FILE_IS_REMOTE, One or more files would be created or removed on a remote file system. (Policy for loading remote files is implementation defined). Manager info: target, number of files. Target info: file paths.",
  87: "SW_SAME_REVISION_SKIPPED, One or more filesets have the same revision and are being skipped because reinstallfalse or recopyfalse. Manager info: target, number of filesets. Target info: software_specs",
  88: "SW_EXECUTION_BEGINS, The execution phase begins on the target. Manager info: target. Target info: none.",
  89: "SW_EXECUTION_ENDS, The execution phase ends on the target. Manager info: target. Target info: none.",
  90: "SW_SELECTION_SKIPPED_IN_ANALYSIS, One or more selections will not be included for execution because they were determined to be skipped in analysis. Manager info: target, number of filesets. Target info: software_specs",
  93: "SW_NEED_MEDIA_CHANGE, The target needs the next media. (Interactive support for media change is implementation defined). Manager info: target, needed media sequence number. Target info: needed media sequence number.",
  94: "SW_CURRENT_MEDIA, The current media sequence number for the target Manager info: target, current media sequence number. Target info: current media sequence number.",
  117: "SW_FILESET_BEGINS, The execution phase of a fileset begins. Manager info: target. Target info: software_spec.",
  118: "SW_CONTROL_SCRIPT_BEGINS, The execution of a control script begins. Manager info: target. Target info: software_spec, control script tag",
  119: "SW_FILE_BEGINS, The execution phase of file begins. Manager info: target. Target info: file path.",
  260: "SW_SOURCE_ACCESS_BEGINS",
  261: "SW_SOURCE_ACCESS_ENDS",
  262: "SW_CONTROL_SCRIPT_ENDS",
  263: "SW_SOC_LOCK_CREATED",
  264: "SW_SOC_LOCK_REMOVED",
  265: "SW_SELECTION_EXECUTION_BEGINS",
  266: "SW_SELECTION_EXECUTION_ENDS",
  267: "SW_ISC_INTEGRITY_CONFIRMED",
  268: "SW_ISC_INTEGRITY_NOT_CONFIRMED",
  269: "SW_SPECIAL_MODE_BEGINS",
  270: "SW_SOC_INTEGRITY_CONFIRMED",
  271: "SW_SOC_INTEGRITY_NOT_CONFIRMED",
  272: "SW_ABORT_SIGNAL_RECEIVED",
  273: "SW_FILE_EXISTS",
  280: "SWI_PRODUCT_SCRIPT_ENDS",
  281: "SWI_FILESET_SCRIPT_ENDS",
  302: "SWI_CATALOG_UNPACK_BEGINS",
  303: "SWI_CATALOG_UNPACK_ENDS",
  304: "SWI_TASK_BEGINS",
  305: "SWI_TASK_ENDS",
  306: "SWI_SWICOL_ERROR",
  307: "SWI_CATALOG_ANALYSIS_BEGINS",
  308: "SWI_CATALOG_ANALYSIS_ENDS",
  309: "SW_TARGET_BEGINS",
  310: "SW_TARGET_ENDS",
  311: "SWI_NORMAL_EXIT",
  312: "SWI_SELECTION_BEGINS",
  313: "SWI_SELECTION_ENDS",
  314: "SWI_MSG",
  315: "SWI_ATTRIBUTE",
  316: "SWI_GROUP_BEGINS",
  317: "SWI_GROUP_ENDS",
  318: "SWI_TASK_CTS",
  319: "SWI_MAIN_SCRIPT_ENDS"

}

