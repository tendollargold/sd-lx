# readinheader.py - read in an archive header
#
# Copyright (C) 1990, 1991, 1992 Free Software Foundation, Inc.
# Copyright (C) 2002 Jim Lowe
# Copyright (C) 2023 - 2024 Paul Weber, conversion to Python
#
# Portions of this code are derived from code (found in GNU cpio)
# copyrighted by the Free Software Foundation.  Retention of their
# copyright ownership is required by the GNU GPL and does *NOT* signify
# their support or endorsement of this work.
#   						jhl
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import sys
import ctypes

from sd.debug import E_DEBUG, E_DEBUG2, E_DEBUG3
from sd.swsuplib.misc.cstrings import strcmp, memcpy, strchr, strncmp
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlib_atoul, swlib_i_pipe_pump, swlib_atoi2
from sd.swsuplib.strob import strob_str, strob_memcpy, strob_chr_index
from sd.swsuplib.taru.ahs import ahsstaticgettarfilename, ahsstaticsettarfilename, ahsstaticsetpaxlinkname, ahsstaticsettargroupname, ahsstaticsettarusername
from sd.swsuplib.taru.copyinnew import taru_read_in_new_ascii
from sd.swsuplib.taru.copyinold import taru_read_in_old_ascii2
from sd.swsuplib.taru.filetypes import ft
from sd.swsuplib.taru.otar import taru_tape_skip_padding
from sd.swsuplib.taru.taruib import taruib_get_fd, taruib_get_buffer, taruib_get_datalen, taruib_get_nominal_reserve, taruib_get_overflow_release, taruib_set_datalen, taruib_get_reserve
from sd.swsuplib.taru.taru import tc, af, taru_exattlist_get_free, taru_exatt_parse_fqname, taru_read_in_tar_header2
from sd.swsuplib.taru.cpiohdr import chc
from sd.swsuplib.uxfio import uxfio_sfa_read, uxfio_write
def set_unsigned_long_value(al, al_nsec, value, valuelen):
    result = 0
    endptr = '\0'

    if al_nsec != 0:
        al_nsec = 0

    ret = swlib_atoul(value, result, endptr)
    if result != 0:
        al = -1 #error
        return result
    else:
        al = ret

    if al_nsec != 0 and endptr[0] == '.':
        # parse the nanosecond part 
        ret = swlib_atoul(endptr[1:], result, str(None))
        if result != 0:
            return result
        else:
            al_nsec = ret
    return result

def taru_pump_amount2(discharge_fd, suction_fd, amount, adjunct_ofd):
    ret = 0
    i = amount
    if ret := swlib_i_pipe_pump(suction_fd, discharge_fd, i, adjunct_ofd, None, taru_tape_buffered_read):
        return -1
    return i

def taru_read_amount(suction_fd, amount):
    return taru_pump_amount2(-1, suction_fd, amount, -1)

def taru_tape_buffered_read(fd, buf, pcount):
    E_DEBUG("ENTERING")

    taruib_fd = taruib_get_fd()
    count = uxfio_sfa_read(fd, buf, pcount, None)

    E_DEBUG3("taruib_fd=%d,  count=%d", taruib_fd, int(count))

    if taruib_fd > 0 and count > 0:
        E_DEBUG("buffer taruib")
        buffer = taruib_get_buffer()
        data_len = taruib_get_datalen()
        buffer_reserve = taruib_get_nominal_reserve() - data_len

        if buffer_reserve < count and taruib_get_overflow_release() == 0:
            E_DEBUG3("CASE1 res %d  count %d", buffer_reserve, count)
            buffer_dst = buffer
            taruib_set_datalen(count)
            uxfio_write(taruib_fd, buffer_dst, data_len)
        else:
            E_DEBUG3("CASE2 res %d  count %d", buffer_reserve, count)
            if data_len + int(pcount) > taruib_get_reserve():
                sys.stderr.write("taru_tape_buffered_read fatal error: stop, stop, stop.\n")
                sys.exit(2)
            buffer_dst = buffer + data_len
            taruib_set_datalen(data_len + count)
        memcpy(buffer_dst, buf, count)
    E_DEBUG("LEAVING")
    return count

def taru_exatt_override_ustar(file_hdr, exatt):
    ret = 0
    if len(exatt.namespaceM) == 0 and str(int((str(strob_str(exatt.attrname))))).islower():
        #		
        # 		 * POSIX ustar attribute
        # 		 * Update the file_hdr object with its value
        # 		 


        #		
        # 		 * Since the value is being transferred to the  file_hdr structure proper
        # 		 * set the is_in_use flag to zero
        # 		 
        exatt.is_in_use = 0

        attr = strob_str(exatt.attrname)
        value = strob_str(exatt.attrvalue)
        valuelen = exatt.len

        if strcmp(attr, tc.PAX_KW_PATH) == 0:
            ahsstaticsettarfilename(file_hdr, value)
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_PATH
        elif strcmp(attr, tc.PAX_KW_LINKPATH) == 0:
            ahsstaticsetpaxlinkname(file_hdr, value)
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_LINKPATH
        elif strcmp(attr, tc.PAX_KW_GNAME) == 0:
            ahsstaticsettargroupname(file_hdr, value)
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_GNAME
        elif strcmp(attr, tc.PAX_KW_UNAME) == 0:
            ahsstaticsettarusername(file_hdr, value)
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_UNAME
        elif strcmp(attr, tc.PAX_KW_ATIME) == 0:
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_ATIME
            temp_ref_value = value
            ret = set_unsigned_long_value(file_hdr.c_atime, file_hdr.c_atime_nsec, temp_ref_value, valuelen)
            value = temp_ref_value
            if ret != 0:
                E_DEBUG3("%s: error evaluating attribute in extended header: %s\n", swlib_utilname_get(), "atime")
        elif strcmp(attr, tc.PAX_KW_MTIME) == 0:
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_MTIME
            temp_ref_value2 = value
            ret = set_unsigned_long_value(file_hdr.c_mtime, 0, temp_ref_value2, valuelen)
            value = temp_ref_value2
            if ret != 0:
                E_DEBUG3("%s: error evaluating attribute in extended header: %s\n", swlib_utilname_get(), "mtime")
        elif strcmp(attr, tc.PAX_KW_CTIME) == 0:
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_CTIME
            temp_ref_value3 = value
            ret = set_unsigned_long_value(file_hdr.c_ctime, file_hdr.c_ctime_nsec, temp_ref_value3, valuelen)
            value = temp_ref_value3
            if ret != 0:
                E_DEBUG3("%s: error evaluating attribute in extended header: %s\n", swlib_utilname_get(), "ctime")
        elif strcmp(attr, tc.PAX_KW_GID) == 0:
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_GID
            temp_ref_value4 = value
            ret = set_unsigned_long_value(file_hdr.c_gid, 0, temp_ref_value4, valuelen)
            value = temp_ref_value4
            if ret != 0:
                E_DEBUG3("%s: error evaluating attribute in extended header: %s\n", swlib_utilname_get(), "gid")
        elif strcmp(attr, tc.PAX_KW_UID) == 0:
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_UID
            temp_ref_value5 = value
            ret = set_unsigned_long_value(file_hdr.c_uid, 0, temp_ref_value5, valuelen)
            value = temp_ref_value5
            if ret != 0:
                E_DEBUG3("%s: error evaluating attribute in extended header: %s\n", swlib_utilname_get(), "uid")
        elif strcmp(attr, tc.PAX_KW_SIZE) == 0:
            file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_SIZE
            temp_ref_value6 = value
            ret = set_unsigned_long_value(file_hdr.c_uid, 0, temp_ref_value6, valuelen)
            value = temp_ref_value6
            if ret != 0:
                E_DEBUG3("%s: error evaluating attribute in extended header: %s\n", swlib_utilname_get(), "size")
        else:
            pass
            # this should be an error or at least a warning
            exatt.is_in_use = 1
            E_DEBUG3("%s: warning: lower case attribute name not processed: %s\n", swlib_utilname_get(), attr)
    return ret

def taru_read_ext_header_record(file_hdr, data, errorcode):
    pend = '\0'
    result = 0
    n_data = data
    length = swlib_atoi2(n_data, pend, result)

    pos = pend
    if not pos.isspace():
        # error 
        if errorcode:
            errorcode = -1
        return None

    #	
    # Skip over the space, there really should only be one
    # 	 	
    if pos.isspace():
        pos += 1

    #	
    # pos now points to the first charactor of the attribute name
    # 	 
    attr = pos
    p = strchr(attr, '=')
    if (not p) != '\0':
        if errorcode:
            errorcode = -2
        return None
    attrlen = p - attr
    if attrlen < 0:
        if errorcode:
            errorcode = -3
        return None

    p += 1

    #	
    # p now points to the first char of the value
    # 	 
    value = p
    valuelen = length - int((p - data)) - 1
    if valuelen < 0:
        if errorcode:
            errorcode = -4
        return None

    exatt = taru_exattlist_get_free(file_hdr)

    retval = taru_exatt_parse_fqname(exatt, attr, attrlen)
    if retval < 0:
        if errorcode:
            errorcode = -5
        return None

    #	
    #  Now copy the value
    # 	 
    strob_memcpy(exatt.attrvalue, value, valuelen)

    #	
    # append a NUL to the end of the value which is not part of the value
    # 	 
    strob_chr_index(exatt.attrvalue, valuelen, ord('\0'))
    exatt.len = valuelen
    exatt.is_in_use = 1

    #	
    # return first byte after the trailing newline of the
    # extended header record.
    # 	 
    return value[valuelen:] + 1

def taru_read_all_ext_header_records(file_hdr, data, len, errorcode):
    current_pos = data
    num_of_records = 0

    file_hdr.extHeader_usage_mask |= chc.TARU_EHUM_ANY
    retpos = data
    while retpos != '\0' and (current_pos - data) < len:
        temp_ref_current_pos = current_pos
        retpos = taru_read_ext_header_record(file_hdr, temp_ref_current_pos, errorcode)
        current_pos = temp_ref_current_pos
        if retpos is None:
            return -num_of_records
        #		
        # Note: it is possible retpos points one byte after the last byte of allocated memory
        # when the last byte of a 512 byte tar block is the trailing newline of the last
        # extended header record.  This is OK here because it is never deference.
        # 		 
        current_pos = retpos
        num_of_records += 1
    return num_of_records

def taru_read_header(taru, file_hdr, in_des, format, eoa, flags):
    E_DEBUG("ENTERING")
    E_DEBUG2("fildes=%d", in_des)

    ret = taru_read_in_header(taru, file_hdr, in_des, format, eoa, flags)
    if ret < 0:
        return -1
    retval = ret
    if format is not af.arf_tar and format is not af.arf_ustar:
        if (file_hdr.c_mode & ft.CP_IFMT) == ft.CP_IFLNK:
            link_name = ctypes.create_string_buffer(file_hdr.c_filesize + 1)
            link_name[int(file_hdr.c_filesize)] = '\0'
            ret = taru_tape_buffered_read(in_des, link_name, int(file_hdr.c_filesize))
            if ret < 0:
                return -retval
            retval += ret
            ret = taru_tape_skip_padding(in_des, file_hdr.c_filesize, format)
            if ret < 0:
                return -retval
            retval += ret
            ahsstaticsetpaxlinkname(file_hdr, link_name.value)
        else:
            ahsstaticsetpaxlinkname(file_hdr, None)
    E_DEBUG("LEAVING")
    return retval


def taru_read_in_header(taru, file_hdr, in_des, archive_format_in0, p_eoa, flags):
    bytes_skipped = 0 # Bytes of junk found before magic number.
    # Search for a valid magic number.

    E_DEBUG("ENTERING")
    if archive_format_in0 is af.arf_unknown:
        sys.stderr.write(" format unknown in read_in_header.\n")
        return -1

    if archive_format_in0 is af.arf_tar or archive_format_in0 is af.arf_ustar:
        E_DEBUG("")
        if (retval := taru_read_in_tar_header2(taru, file_hdr, in_des, None, p_eoa, flags, tc.TARRECORDSIZE)) < 0:
            E_DEBUG("LEAVING")
            return -1
        else:
            E_DEBUG("LEAVING")
            return retval

    ahsstaticsetpaxlinkname(file_hdr, None)

    if (retval := taru_tape_buffered_read(in_des, file_hdr, int((6)))) != 6:
        E_DEBUG("")
        E_DEBUG2("error reading magic. retval= %d\n", retval)
        E_DEBUG("LEAVING")
        return -1

    while True:
        if archive_format_in0 is af.arf_newascii and not strncmp(str(file_hdr), "070701", 6):
            E_DEBUG("")
            if bytes_skipped > 0:
                E_DEBUG2("warning: skipped %ld bytes of junk", bytes_skipped)
            if (ret := taru_read_in_new_ascii(taru, file_hdr, in_des, archive_format_in0)) < 0:
                E_DEBUG("error from taru_read_in_new_ascii")
                E_DEBUG("LEAVING")
                return -retval
            if not strcmp(ahsstaticgettarfilename(file_hdr), tc.CPIO_INBAND_EOA_FILENAME):
                if p_eoa:
                    p_eoa = 1
            retval += ret
            break
        if archive_format_in0 is af.arf_crcascii and not strncmp(str(file_hdr), "070702", 6):
            E_DEBUG("")
            if bytes_skipped > 0:
                E_DEBUG2("warning: skipped %ld bytes of junk", bytes_skipped)
            if (ret := taru_read_in_new_ascii(taru, file_hdr, in_des, archive_format_in0)) < 0:
                E_DEBUG("LEAVING")
                return -retval
            if not strcmp(ahsstaticgettarfilename(file_hdr), tc.CPIO_INBAND_EOA_FILENAME):
                if p_eoa:
                    p_eoa = 1
            retval += ret
            break
        if (archive_format_in0 is af.arf_oldascii or archive_format_in0 is af.arf_hpoldascii) and not strncmp(str(file_hdr), "070707", 6):
            E_DEBUG("")
            if bytes_skipped > 0:
                E_DEBUG2("warning: skipped %ld bytes of junk", bytes_skipped)
            ret = taru_read_in_old_ascii2(taru, file_hdr, in_des, str(None))
            if ret <= 0:
                E_DEBUG("LEAVING")
                return -retval
            if not strcmp(ahsstaticgettarfilename(file_hdr), tc.CPIO_INBAND_EOA_FILENAME):
                if p_eoa:
                    p_eoa = 1
            retval += ret
            break
        if (archive_format_in0 is af.arf_binary or archive_format_in0 is af.arf_hpbinary) and (file_hdr.c_magic == 0o70707 or file_hdr.c_magic == ((((0o70707) << 8) & 0xff00) | (((0o70707) >> 8) & 0x00ff))):
            E_DEBUG("")
            # Having to skip 1 byte because of word alignment is normal.  
            sys.stderr.write("arf_binary af.arf_hpbinary not supported.\n")
            break
            #	   
            #	  if (bytes_skipped > 0)
            #	    fprintf  (stderr,
            #			"warning: skipped %ld bytes of junk", bytes_skipped)
            #	  ret = taru_read_in_binary(file_hdr, in_des)
            #	  if (ret <= 0) {
            #		E_DEBUG("LEAVING")
            #		return -retval
            #	  }
            #	  retval += ret
            #	  break
            #	  
        bytes_skipped += 1
        # memmove(str(file_hdr), str(file_hdr) + 1, 5)
        if (ret := taru_tape_buffered_read(in_des, (file_hdr + 5), int(1))) <= 0:
            sys.stderr.write("error: header magic not found and subsequent read error.\n")
            E_DEBUG("LEAVING")
            return -retval
        retval += ret
    E_DEBUG("LEAVING")
    return retval






# Return 16-bit integer I with the bytes swapped.
