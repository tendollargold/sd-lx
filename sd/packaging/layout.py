
# packaging.layout.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

# 5.0 Software Packaging Layout  Page 125
# Systems Management: Distributed Software Administration

# This section describes the software packaging layout. The software packaging
# layout consists of:

#   1. The directory structure consisting of these major components:
#        · The exported catalog structure containing software information 
#          including  software definition files and customize scripts used by the
#           install/update and copy utilities

#        · The file storage structure that contains the actual software files for
#          each fileset

#   2. The software definition file formats and the objects and attributes they 
#      contain, INDEX for software definitions and INFO for file and control_file 
#      definitions (used by all the utilities defined in this Software Administration 
#      specification), the PSF for product specification (used by swpackage), and
#      the space file (used by disk space analysis in install).

#   3. The serial format of the layout containing an archive of files in the 
#      directory structure.

#      This ordering of directories and files also applies to distributions on a 
#      set of hierarchical file systems which span multiple media.

# Thus, two distinct (but related) formats for the software packaging layout are 
# supported by this Software Administration specification:

#   · A directory structure format which resides within a POSIX.1 hierarchical file 
#     system (disk, CD-ROM, etc.)

#   · A bit stream serial format which resides within a POSIX.1 extended cpio or 
#     extended tar archive

# A conformant implementation does not contain any other files or directories besides 
# those explicitly entered in the distribution catalog. However, it can contain other 
# files and directories besides those belonging to the distribution.

#
# 5.1 Directory Structure
#

# This section describes the directory structure for the software packaging 
# layout, and how this representation stores the definitions of the software and 
# file objects contained within it. The directory structure is a POSIX.1 
# hierarchical file system containing files in the software packaging layout.

# This Software Administration specification defines a single directory structure
# for directory and serial distributions. The software packaging layout can be 
# stored in two forms:

#   · A direct access file system as described in this section

#   · A serial access extended cpio or extended tar archive as described in POSIX.1

# The same structure offers optimal load performance for serial distributions 
# while providing a simple structure for directory distributions. This structure
# applies to each media if the distribution spans multiple media.

# The structure supports multiple versions of a product contained within a single 
# distribution, where versions are distinguished by a unique combination the 
# product tag, revision, architecture, and vendor_tag attributes.

# Table 5-1 shows the directory structure of a software packaging layout located 
# under a directory path:

# The directory structure for the software packaging layout is divided into two 
# areas:

#   · The exported catalog structure, consisting of control directories containing
#     the software definition files that describe the products contained in the 
#     distribution, as well as the software control_files

#   · The software file storage structure, consisting of the product and fileset 
#     storage directories, under which the actual software files for each fileset
#     are located

#
# 5.1.1 Exported Catalog Structure
#

# The catalog structure describes the software contained in the distribution. It 
# is organized by product, and each product is organized by fileset. The 
# specific contents are described in the following sections.

#
# 5.1.1.1 INDEX File
#
# The distribution catalog contains a global INDEX file:

#   · catalog/INDEX

#     This INDEX file contains the definition of all software objects in the 
#     distribution.

#
# 5.1.1.2 Distribution Files
#

# The catalog/dfiles/ directory contains files used to store certain attributes
# of the distribution object. The distribution information stored can include:

#   · <attribute>
#     A distribution attribute can be stored as a separate file, the file name of 
#     which can be the name of the attribute.

# |Directory or File                      |Purpose
# -----------------------------------------------------------------------------
# |path/catalog/                          |Contains all information about the 
# |                                       |distributions contents
# |path/catalog/INDEX                     |Global index of distribution and its
# |                                       |contents
# |path/catalog/dfiles/                   |Contains distribution attributes 
# |                                       |stored in files
# |path/catalog/dfiles/...
# -----------------------------------------------------------------------------
# |path/catalog/product1/                 |Storage for information on the first
# |                                       |product
# |path/catalog/product1/pfiles           |Contains all product attributes 
# |                                       |stored in files
# |path/catalog/product1/pfiles/INFO      |Control_file information for this 
# |                                       |product
# |path/catalog/product1/pfiles/script    |Vendor defined control_files
# |path/catalog/product1/pfiles/...       |
# -----------------------------------------------------------------------------
# |path/catalog/product1/fileset1         |Storage for information and scripts
# |                                       |on this fileset
# |path/catalog/product1/fileset1/INFO    |File and control_file information 
# |                                       |for this fileset
# |path/catalog/product1/fileset1/scripts |Vendor defined control_files
# |path/catalog/product1/fileset1/...
# -----------------------------------------------------------------------------
# |path/catalog/product1/fileset2/        |Storage for information and scripts
# |                                       |on the next fileset
# |path/catalog/product1/fileset2/...     |
# -----------------------------------------------------------------------------
# |path/catalog/product2/                 |Storage for information on the next
# |                                       |product
# |path/catalog/product2/...
# =============================================================================
# |path/product1/                         |Storage for this product’s filesets
# |path/product1/fileset1/                |Storage for this filesets files
# |path/product1/fileset1/files           |Actual directory structure of files
# |path/product1/fileset1/...
# -----------------------------------------------------------------------------
# |path/product1/fileset2/                |Storage for next fileset’s files
# |path/product1/fileset2/files           |Actual directory structure of files
# |path/product1/fileset2/...
# -----------------------------------------------------------------------------
# |path/product2/                         |Storage for next product’s filesets
# |path/product2/...
# -----------------------------------------------------------------------------

#
# 5.1.1.3 Product Catalog
#

# The catalog files for each product are stored under a directory 
# catalog/<product_control_directory>/. The way in which the value of each 
# product control directory is determined is defined below.

#
# 5.1.1.4 Product Control Files
#

# The catalog/<product_control_directory>/pfiles/ directory contains the 
# control_files for the product object. The product control_files include:

#   · <attribute>

#      A product attribute can be stored as a separate file, the file name of 
#      which can be the name of the attribute.


#   · INFO
#     Contains the definitions for the control_file objects contained within the
#     product.

#   · checkinstall
#     preinstall
#     ...
#     postremove
#     The vendor-supplied control scripts for the product.

#   · <control_file>

#     All other vendor-defined control_files for this product.

#
# 5.1.1.5 Fileset Control Files
#

# The catalog/<product_control_directory>/<fileset_control_directory> directory 
# contains the control_files for the fileset object. The way in which the value 
# of each fileset control directory is determined is defined below. The fileset 
# control_files include:

#  · <attribute>

#    A fileset attribute can be stored as a separate file, the filename of which 
#    can be the name of the attribute.

#  · INFO
#    Contains the definitions for the control_file and file objects contained 
#    within the fileset.

#  · checkinstall
#    preinstall
#    ...
#    postremove
#    The vendor-supplied control scripts for the fileset.

#  · <control_file>
#    All other vendor-defined control_files for this fileset.

#
# 5.1.2 File Storage Structure
#

# The second portion of a distribution contains the actual software files 
# contained in each fileset object.

# The files of each fileset are store in a directory with the name 
# <fileset_control_directory> which is itself in a directory called 
# <product_control_directory> .

# Each regular file (ones for which file.type is f) is stored in a location 
# defined by appending the file.path attribute to the path of the fileset file 
# storage directory. This may require the creation of additional directories. 
# Other file types (directories, except as needed to store files, hard links 
# and symbolic links) are not required to exist in the distribution. The POSIX.1 
# file permissions for files in the file storage area are undefined.

#
# 5.1.2.1 Control Directory Names
#

# In the simplest case, the value of the product.tag attribute is the name of
# the product control directory. The fileset.tag attribute is used as the 
# name of the fileset control directory. Two conditions complicate this simple 
# naming:

#   1. Length of the tag attribute exceeds {POSIX_PATH_MAX} of the system 
#      where the distribution resides.

#   2. Name collision with an existing product control directory.

#      Given that multiple versions of a product may be contained in the same 
#      distribution, collisions from product control directories named by the 
#      tag attribute are common.

# These conditions are met by defining a control_directory attribute for each 
# product and fileset that is unique within the distribution. The attribute 
#  uses the syntax:

# %token           FILENAME_CHARACTER_STRING /* as defined in 2.2.2.37 */

# %start control_directory
# %%

# control_directory           : tag_part
#                             | tag_part "." instance_id_part
#                             ;

# tag_part                    : FILENAME_CHARACTER_STRING
#                             ;

# instance_id_part            : FILENAME_CHARACTER_STRING
#                             ;


# The tag_part may be the product or fileset tag attribute, truncated as 
# necessary to meet any filename length restrictions of the operating system.

# The instance_id_part is a string that, when added after the ‘‘.’’ (period), 
# defines a control_directory which, for products, is unique within the 
# distribution and, for filesets, is unique within the product. For products,
# this instance_id_part may be the instance_id of the product if that 
# instance_id was generated considering other products tag_parts in addition 
# to tag attributes.

#
# 5.2 Software Definition File Format
#

# The software definition files contain the software structure and the detailed 
# attributes for distributions, installed_software, bundles, products, 
# subproducts, filesets, files, and control_files.  While information on 
# installed software is represented in this form as input to or output from the 
# various software administration utilities, the actual storage of this metadata 
# for installed software is undefined. This section describes the format of the 
# software definition files:

#  · The INDEX file contains the definition of distribution or 
#    installed_software objects as well as the software objects contained within 
#    those software_collections. The information in this file is primarily used 
#    in selection phases of the utilities.

#  · The INFO file contains the definition of the software files and control_files
#    for a product or fileset within a distribution or installed_software object.
#    The information in this file is primarily used in analysis and execution 
#    phases of the utilities.

#  · The PSF (product specification file) also contains the definition of 
#    distribution attributes, software objects, and the software files and 
#    control_files for the product and fileset software objects. This file is 
#    created by the software vendor and used by the packaging tool to create the 
#    distribution, represented by the INDEX and INFO files, in the software 
#    packaging layout.

#    The PSF supports the same syntax as the INDEX and INFO files. Additional 
#    syntactic constructs are supported for specifying files and control_files. 
#    This file is used in selection, analysis and packaging Phases of the 
#    swpackage command.

# Additionally, there is a space file that is created by the software vendor for 
# additional disk space needed for a product or fileset. This file is used in 
# analysis phase of the swinstall command to account for additional disk space 
# required.

#
# 5.2.1 Software Definition File Syntax
#

# The INDEX and INFO files have essentially the same syntax and semantics as the PSF. One key difference is that the INDEX file does not contain control_file and file definitions, the INFO file contains only control_file and file definitions, and the PSF file contains all definitions. In a distribution, each product and fileset has a separate INFO file.

# The software specification file syntax is as follows.16

# %token FILENAME_CHARACTER_STRING /* as defined in Glossary */
# %token NEWLINE_STRING /* as defined in Glossary */
# %token PATHNAME_CHARACTER_STRING /* as defined in Glossary */
# %token SHELL_TOKEN_STRING /* as defined in Glossary */
# %token WHITE_SPACE_STRING /* as defined in Glossary */
# %start software_definition_file
# %%
# software_definition_file : INDEX
"""
| INFO
| PSF
;
Software Definition File Format
The software definition files contain the software structure and the detailed attributes for
distributions, installed_software, bundles, products, subproducts, filesets, files, and control_files.
While information on installed software is represented in this form as input to or output from
the various software administration utilities, the actual storage of this metadata for installed
software is undefined. This section describes the format of the software definition files:
· The INDEX file contains the definition of distribution or installed_software objects as well as
the software objects contained within those software_collections. The information in this file
is primarily used in selection phases of the utilities.
· The INFO file contains the definition of the software files and control_files for a product or
fileset within a distribution or installed_software object. The information in this file is
primarily used in analysis and execution phases of the utilities.
· The PSF (product specification file) also contains the definition of distribution attributes,
software objects, and the software files and control_files for the product and fileset software
objects. This file is created by the software vendor and used by the packaging tool to create
the distribution, represented by the INDEX and INFO files, in the software packaging
layout.
The PSF supports the same syntax as the INDEX and INFO files. Additional syntactic
constructs are supported for specifying files and control_files. This file is used in selection,
analysis and packaging Phases of the swpackage command.
Additionally, there is a space file that is created by the software vendor for additional disk
space needed for a product or fileset. This file is used in analysis phase of the swinstall command
to account for additional disk space required.
5.2.1 Software Definition File Syntax
The INDEX and INFO files have essentially the same syntax and semantics as the PSF. One key
difference is that the INDEX file does not contain control_file and file definitions, the INFO file
contains only control_file and file definitions, and the PSF file contains all definitions. In a
distribution, each product and fileset has a separate INFO file.
The software specification file syntax is as follows.16
%token FILENAME_CHARACTER_STRING /* as defined in Glossary */
%token NEWLINE_STRING /* as defined in Glossary */
%token PATHNAME_CHARACTER_STRING /* as defined in Glossary */
%token SHELL_TOKEN_STRING /* as defined in Glossary */
%token WHITE_SPACE_STRING /* as defined in Glossary */
%start software_definition_file
%%
software_definition_file : INDEX
| INFO
| PSF
;
fileset_contents : fileset_contents NEWLINE_STRING fileset_content_items
| fileset_content_items
;
fileset_content_items : control_files
| files
;
info_contents : info_contents NEWLINE_STRING info_content_items
| info_content_items
;
info_content_items : control_files
| files
;
product_contents : product_contents NEWLINE_STRING product_content_items
| product_content_items
;
product_content_items : control_files
/* control_files not valid in INDEX files */
| subproducts
| filesets
;
soc_contents : soc_contents NEWLINE_STRING soc_content_items
| soc_content_items
;
soc_content_items : vendors
| bundles
| products
;
soc_definition : distribution_definition
| installed_software_definition
;
distribution_definition : software_definition
media
;
media_definition : software_definition
;
installed_software_definition : software_definition
;
vendor_definition : software_definition
;
bundle_definition : software_definition
;
product_definition : software_definition
;
subproduct_definition : software_definition
;
fileset_definition : software_definition
;
control_file_definition : software_definition
| extended_definition
/* extended_definition only valid in PSF files */
;
file_definition : software_definition
| extended_definition
/* extended_definition only valid in PSF files */
;
software_definition : object_keyword NEWLINE_STRING
attribute_value_list
;
attribute_value_list : /* empty */
| attribute_value_list attribute_definition NEWLINE_STRING
| attribute_definition NEWLINE_STRING
;
attribute_definition : attribute_keyword WHITE_SPACE_STRING attribute_value
;
object_keyword : FILENAME_CHARACTER_STRING
;
attribute_keyword : FILENAME_CHARACTER_STRING
;
extended_definition : extended_keyword WHITE_SPACE_STRING attribute_value
;
extended_keyword : FILENAME_CHARACTER_STRING
;
attribute_value : attribute_value WHITE_SPACE_STRING single_value
| single_value
| ’<’ WHITE_SPACE_STRING PATHNAME_CHARACTER_STRING
| ’<’ PATHNAME_CHARACTER_STRING
;
single_value : SHELL_TOKEN_STRING
;
The following syntax rules are applicable to software definition files:
1. All keywords and values are represented as character strings.
2. Each keyword is located on a separate line. Keywords can be preceded by white space
(tab, space). White space separates the keyword from the value.
3. Comments can be placed on a line by themselves or after the keyword-value syntax. They
are designated by preceding them with the # (pound) character. The way in which
comments are used in INDEX and INFO is undefined
[200~4. All object keywords have no values. All attribute keywords have one or more values.
5. An attribute value ends on the same line as the keyword with one exception. Attribute
values can span lines if and only if the value is prefixed and suffixed with the " (double
quote) character.
6. When an attribute value begins with < (less than), the remainder of the string value is
interpreted as a filename whose contents will be used as a quoted string value for the
attribute. For INDEX files, the filename is a path relative to the control directory for that
distribution, product, or fileset. For PSF files, the filename is a path to a file on the host that
contains the file.
7. The use of " (double quote) is not required when defining a single line string value that
contains embedded white space. Trailing white space is removed; embedded white space
is used. The quotes can be used.
8. The " (double quote), # (pound), and \ (backslash) characters can be included in multiline
string values by ‘‘escaping’’ them with \ (backslash).
9. The order of attributes is not significant, except that the layout_version is the first attribute
defined in an INDEX file for a distribution or installed_software object.
"""