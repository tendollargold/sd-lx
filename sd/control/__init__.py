
# sd.control.__init__.py
# SD cli.control subpackage.
#
# Copyright (C) 2021-2024 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

#####
# 2.15 Control_Files
#
# Control_files can be scripts, data files, or INFO files. The product and 
# fileset INFO files in the software packaging layout are included as 
# control_files. Control scripts are the vendor-supplied scripts executed at 
# various steps by the software administration utilities.

# The control_file class inherits attributes from the software_file common class.

# A particular control_file object is identified within a product or fileset by 
# the tag attribute. The path attribute is the storage location of the file 
# relative to the control directory. For distributions, the control directory is 
# the directory in the software packaging layout where the control_files are
# stored. For installed_software objects, this control directory location is 
# undefined.

#########
# Control_File Attributes

# These attributes, along with the attributes listed in Table 2-7 on page 14, 
# describe each instance of the control_file class:

####
# interpreter
#
#   The name of the interpreter used to execute those control_files that are 
#   executed as part of the utilities defined in this Software Administration 
#   specification.

#   Within a distribution, a value for this attribute other than sh implies that 
#   the distribution is not a conformant one. Such a distribution may be one 
#   which is conformant with extensions.

#   See Section 1.3 on page 4.

####
# path
#
#   The filename of the control_file.

#   Multiple control_file entries can have the same value of the path attribute. 
#   This implies that the same script is executed in different steps within the 
#   execution of a utility.

####
# result
#
#   Contains the result of the execution of the control script.

#   This attribute is only valid for control_files in installed_software. A 
#   complete list of legal results is contained in Table 2-14.

####
# tag
#
#   The identifier of the control_file.

#   All control files are loaded and maintained within the distribution and 
#   installed software catalogs by the utilities defined in this Software 
#   Administration specification. These utilities execute control scripts with 
#   particular tags at various steps in the execution of the utility.  The values 
#   for the control_file tag attribute for which this Software Administration 
#   specification defines behavior are as follows:

#      request
#      response
#      checkinstall
#      preinstall
#      postinstall
#      unpreinstall
#      unpostinstall
#      verify
#      fix
#      checkremove
#      preremove
#      postremove
#      configure
#      unconfigure
#      space


