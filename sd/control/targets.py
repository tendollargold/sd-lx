# targets.py  validate target for listing, installing, removing
#  Copyright (C) 2023 Paul Weber Convert to python

#  COPYING TERMS AND CONDITIONS
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
# This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
# * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

#		 There are three (3) target possibilities:
#			1) Distribution of a local tar archive file
#			2) Installed Software
#              The target is a directory where the IPD (installed Product Database)
#			   is located (for removal, listing, modifying, installing)
#
#				<target_path>/<installed_software_catalog>/<tag>/<tag>
#			   `tag' is from the software spec on the command line e.g.
#				`swlist <tag> -T 192.168.1.50:<target_path>'
#               'swremove <tag> -T 15.25.3.4:<target_path>'
#               'swmodify <tag> -a <attribute> -t 15.110.2.33:<target>
#               'swinstall <tag>

#			3) A distribution directory, this must be specifically
#			   commanded by use of the '-d' option, otherwise when
#			   'target_path' is a directory it is a depot of software used for installation
#              Remove, list, modify software in a depot
#              Install from software in a depot
class DetermineTargetAccess:
   # check acl for user access to the target
    pass

class DetermineTargeType:
    # Is target remote host
    # Is a target local (local IPD)
    # Is each target a file, depot, or installed software catalog
    # is target valid?
    pass