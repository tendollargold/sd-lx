#!/bin/sh
# Copyright 2002 Hewlett-Packard Company.
# All rights reserved.
#
# Title:         Setup cron Utility
# Author:        Zahid Khan 
# Organization:  HPCS R&D Mobile/E-Service
# RCS:           $Header: /partners/SysInfo/Client/HPUXBuild/bin/setSysInfoCronEntry.sh 5 2005/04/22 15:51:44 -0600 jnewman $
#                @(#) $Revision: 5 $ $Date: 2005/04/22 15:51:44 -0600 $
#
# NOTE: This is a no-charge contributed utility. Support for it
#       will be provided on a best effort basis by Hewlett-Packard. 
#       Customers should direct any requests for updates, bug fixes, 
#       or enhancements to an HP support engineer.
#       HP support engineers should direct such requests to their
#       region LeadRCE.
#
# Version/Date/Comments(wrapped):       
# 1.00  04/2002      Created to allow SysInfo Mega Map to run under cron.
#                    This script when launched will create an enytry in cron for 
#                    SysInfo Mega Map to be run as specified by the user. 
# 1.01  04/2005      Added support for -tm and -both options (pass thru
#		     to SysInfoRunMap.sh).  Had to massively rework
#		     cmdline argument handling.  Also fixed up Usage a bit.
#                    userid == root is now enforced.

export PATH=/usr/bin
  
TRACE=0
MIN_TM=2
REAL_MIN_TM=10	# comprehensive .mmap's force 10 if not run from Insight!!
MAX_TM=50
TM_RANGE="$MIN_TM..$MAX_TM"

Prog=`basename $0`
#===================================================================
# fInitialize
#        This function initializes values used by this utility.
#===================================================================
function fInitialize
{
# set constants
  partnerConfig="/opt/hpservices/RemoteSupport/bin/partnerConfig"
  TIMEKEY="SysInfoCollectionTimeOfDay"
  WEEKDAYKEY="SysInfoCollectionDayOfWeek"
  CRONENABLEDKEY="SysInfoCronEnabled"
  LOGFILE="/var/opt/hpservices/contrib/SysInfo/adm/setSysInfoCronEntry.log" 
  CRONLOG="/var/opt/hpservices/contrib/SysInfo/adm/SysInfoRunMap.cronlog" 
  CMD="/opt/hpservices/contrib/SysInfo/bin/SysInfoRunMap.sh"
  CMD_SUFFIX=" >> $CRONLOG 2>&1 &"
  CMD_OPTS=""
  INTERACTIVE=0 
  DELETE=0 
  REMOVE="/opt/hpservices/contrib/SysInfo/bin/"
  BATCH=0

# set patterns
        timePat1="[0][0-9][0-5][0-9]"
        timePat2="[1][0-9][0-5][0-9]"
        timePat3="[2][0-3][0-5][0-9]"
        weekdayPat="[0-6]"

# set default cron parameters
        MINUTE="0"
        HOUR="0"
        MONTHDAY="*"
        MONTH="*"
        WEEKDAY="2"
        TIME=""
        timeValue="2300"
        weekdayValue="1"
        cron="Y"

# initialize other program values
	VERSION="1.01"
}			# end of fInitialize

#===================================================================
# fShortBanner 
#     This function will print the short banner       
#===================================================================
function fShortBanner
{
  print "\nWelcome to setSysInfoCronEntry utility script, version $VERSION."
  print "Copyright 2002-2005 Hewlett-Packard Company. "
  print "All rights reserved. "
  print "`date`"
  print " "
  # return to main


}			# end of fShortBanner

#===================================================================
# fSubmitCrontab
#		This function will submit to crontab.
#===================================================================
function fSubmitCrontab
{
 /usr/bin/crontab $tempCrontab > /dev/null 2>&1
 retval=$?
 if [ $retval -ne 0 ]
 then
  if (($BATCH))
  then
   print "WARNING: Unable to modify current crontab configuration." >> $LOGFILE
   print "         (crontab command exited with $retval)" >> $LOGFILE
   print "         Will not be able to run SysInfoRunMap.sh " >> $LOGFILE
   print "         script through cron." >> $LOGFILE
   /usr/bin/rm -f $tempCrontab > /dev/null 2>&1
   exit 4
  else
   print "WARNING: Unable to modify current crontab configuration."
   print "         (crontab command exited with $retval)"
   print "         Will not be able to run SysInfoRunMap.sh      "
   print "         script through cron."
   /usr/bin/rm -f $tempCrontab > /dev/null 2>&1
   fExit  
  fi
 else
   /usr/bin/rm -f $tempCrontab > /dev/null 2>&1
 fi

}			#end of fSubmitCrontab

#============================================================================
# fGetCronInfoFromISEE
#                      This function will get cron parameters from ISEE        
#                       partnerConfig utility. 
#============================================================================
function fGetCronInfoFromISEE
{

    if [ -n "$_DEBUG" ] ; then set -x; fi
    cron=`$partnerConfig -g "$CRONENABLEDKEY"`
    retValue=$?
    if [ $retValue -ne 0 ]
    then
      print "WARNING: ISEE partnerConfig returned $retValue." >> $LOGFILE
      print "         while accessing $CRONENABLEDKEY value." >> $LOGFILE
      print "         SysInfoRunMap.sh cron entry will not be set." >> $LOGFILE
      fPrintTrailer
      exit 2
    else
# validate the keyValue, cronenabled
      case $cron in
           @("Y"|"y"|"Yes"|"YES"|"yes"))
                continue
                ;;

           *)   print "Cron enabled flag is set to N." >> $LOGFILE
                print "SysInfoRunMap.sh cron entry will not be set." >> $LOGFILE
                exit 5
                ;;
      esac
    fi
    
    timeValue=`$partnerConfig -g "$TIMEKEY"`
    retValue=$?
    if [ $retValue -ne 0 ]
    then
      print "WARNING: ISEE partnerConfig returned $retValue." >> $LOGFILE
      print "         while accessing $TIMEKEY value." >> $LOGFILE
      print "         SysInfoRunMap.sh cron entry will not be set." >> $LOGFILE
      fPrintTrailer
      exit 2
    else
# validate the keyValue, timeValue
      case $timeValue in
        @($timePat1|$timePat2|$timePat3))
            continue
            ;;
        *)  print "$timeValue, invalid value for time of day." >> $LOGFILE
            print "Valid values are 24hr time, 1600 for 4:00pm." >> $LOGFILE
            print "SysInfoRunMap.sh cron entry will not be set." >> $LOGFILE
            exit 3
            ;;
      esac
# parse out hour and minute from resultValue
      HOUR=${timeValue%??}
      MINUTE=${timeValue#??}
    fi
    
    weekdayValue=`$partnerConfig -g "$WEEKDAYKEY"`
    retValue=$?
    if [ $retValue -ne 0 ]
    then
      print "WARNING: ISEE partnerConfig returned $retValue." >> $LOGFILE
      print "         while accessing $WEEKDAYKEY value." >> $LOGFILE
      print "         SysInfoRunMap.sh cron entry will not be set." >> $LOGFILE
      fPrintTrailer
      exit 2
    else
# validate the keyValue
      case $weekdayValue in
        @($weekdayPat))                     
           continue
           ;;

        *)  print "$weekdayValue, invalid value for day of the week." >> $LOGFILE
            print "Valid values are 0-6, 0=Sunday, 1=Monday,..." >> $LOGFILE
            print "SysInfoRunMap.sh cron entry will not be set." >> $LOGFILE
            exit 3
            ;;
      esac
      WEEKDAY=$weekdayValue
    fi
}

#===================================================================
# fCreateAddEntryToCronsettingsFile
#		Create a cronsettings file             
#===================================================================
function fCreateAddEntryToCronsettingsFile 
{
   # remove existing SysInfoRunMap.sh entries (if any)from cronsetting file
   fRemoveEntryFromCronsettingsFile   

   # create a copy crontab file      
   tempCrontab=`/usr/bin/mktemp`
   /usr/bin/touch $tempCrontab > /dev/null 2>&1
   /usr/bin/crontab -l > $tempCrontab 2>/dev/null

   if (($BATCH)) ; then
    {
    print "Adding the following entry to root's crontab:\n"
    print "  $MINUTE $HOUR $MONTHDAY $MONTH $WEEKDAY $CMD$CMD_OPTS$CMD_SUFFIX\n"
    } >> $LOGFILE
   else
    print "Adding the following entry to root's crontab:\n"
    print "  $MINUTE $HOUR $MONTHDAY $MONTH $WEEKDAY $CMD$CMD_OPTS$CMD_SUFFIX\n"
   fi

   now=`date`
   echo "# Set programmatically by $Prog: $now" >> $tempCrontab
   echo "$MINUTE $HOUR $MONTHDAY $MONTH $WEEKDAY $CMD$CMD_OPTS$CMD_SUFFIX" >> $tempCrontab

   # submit to cron
   fSubmitCrontab 

}			#end of fCreateAddEntryToCronsettingsFile 

#===================================================================
# fGetUserInput 	get cron input from the user
#===================================================================
function fGetUserInput
{

   if [ -n "$_DEBUG" ] ; then set -x; fi

# prompt for day of the week
   integer n=100

   while ((n)) 
   do
    WEEKDAY=""
    print
    print -n "Please enter day of the week, 0-6, 0=Sunday? >> "              
    read WEEKDAY
    case $WEEKDAY in
      @($weekdayPat))
        n=0
        break
        ;;
      " "|"") 
        WEEKDAY="0"
	print "\t(Using default of 0=Sunday)"
        n=0
        break
        ;;
      "E"|"e"|"exit"|"EXIT")
        exit 0
        ;;
      *)
        print "$WEEKDAY is not valid - try again ('e' to abort)"
        continue
        ;;
    esac
   done

   integer n=100

   while ((n)) 
   do
    TIME=""
    print
    print -n "Please enter time of day, 24 hr time, 2300 for 11:00pm >> "
    read TIME
    case $TIME in
      @($timePat1|$timePat2|$timePat3))
        n=0
        break
        ;;
      " "|"") 
        TIME="0000"
	print "\t(Using default of 0000)"
        n=0
        break
        ;;
      "E"|"e"|"exit"|"EXIT")
        exit 0
        ;;
      *)
        print "$TIME is not a valid entry - try again ('e' to abort)"
        continue
        ;;
    esac
   done

   # break TIME into hours and minutes
   HOUR=${TIME%??}
   MINUTE=${TIME#??}

   integer n=100

   while ((n)) 
   do
       print -n "\nDo you wish to run both the Comprehensive and Addl_Comprehensive maps (y/n)? n\c"
       read ANS
       case "$ANS" in
	[yY]*)
	    CMD_OPTS="${CMD_OPTS} -both"
	    n=0
	    break
	    ;;
	[nN]*)
	    n=0
	    break
	    ;;
        "E"|"e"|"exit"|"EXIT")
            exit 0
	    ;;
	"")
	    print "\t(Using default of 'n')"
	    n=0
	    break
	    ;;
	*)
	    print "Invalid response '$ANS' - you must enter 'y' or 'n' ('e' to abort)."
	    continue
	    ;;
	esac
   done

   integer n=100

   while ((n)) 
   do
       print -n "\nEnter a timeout multiplier (in range $TM_RANGE," \
		"<Enter> for default of 1) >> "
       read MULT
       case "$MULT" in
         "")  # default; do nothing
	    print "\t(Using default of 1, no timeout multiplier)"
	    n=0
	    break
	    ;;
         "E"|"e"|"exit"|"EXIT")
            exit 0
	    ;;
         [0-9]*)
	    # is it an integer, and within accepted range?
    	    MULT_int=`expr match "$MULT" '^\([0-9][0-9]*\)$'` # try to parse it
	    if [ -z "$MULT_int" -o "$MULT_int" != "$MULT" ] ; then
		print "Invalid response '$MULT' - you must enter an integer - try again ('e' to abort)"
		continue	# keep prompting
	    elif [ "$MULT" -lt "$MIN_TM" -o "$MULT" -gt "$MAX_TM" ] ; then
		print "Invalid response '$MULT' - not in range $TM_RANGE - try again ('e' to abort)"
		continue	# keep prompting
	    elif [ "$MULT" -gt "$REAL_MIN_TM" ] ; then
		# This is hokey...we ignore anything from 1..10
		# (see defn of REAL_MIN_TM above)
		CMD_OPTS="${CMD_OPTS} -tm $MULT"
	        n=0
	        break
	    else # ignore anything 10 or below
		n=0
		break
	    fi
	    ;;
         *)
	    print "Invalid response - you must enter 'y' or 'n'."
	    continue
	    ;;
	esac
   done

   # TEMPORARY
   ##print "cron entry: $MINUTE $HOUR $MONTHDAY $MONTH $WEEKDAY $CMD$CMD_OPTS$CMD_SUFFIX" 

}			#end of fGetUserInput

#===================================================================
# fShowUsage    	get cron input from the user
#===================================================================
function fShowUsage     
{

/bin/cat <<MESSMARK
NAME
    setSysInfoCronEntry.sh

DESCRIPTION
    setSysInfoCronScript.sh will set a simple cron entry for the 
    SysInfoRunMap.sh script to run on the chosen schedule. 
                                                  
USAGE
  setSysInfoCronEntry.sh -i                                   
      User will be prompted for <DayOfWeek> and <TimeOfDay> values,
      as well as whether to run both HPUX Comprehensive and Additional
      maps, and what timeout value to use.

  setSysInfoCronEntry.sh -d
      Remove any cron entry for SysInfoRunMap.sh.

  setSysInfoCronEntry.sh -w <DayOfWeek> -t <TimeOfDay> [-both] [-tm <number>]
      <DayOfWeek>   : 0-6, where 0=Sunday, 1=Monday, ...
      <TimeOfDay>   : Use 24 hr time, 2300 for 11:00pm

      -both         Both the ComprehensiveSystemInfo and the
	            Addl_ComprehensiveInformation MAPs will be scheduled to run
		    sequentially.  The default is to only run the
		    ComprehensiveSystemInfo MAP.

      -tm <number>  Apply the timeout multiplier of <number> to all collection
		    timeouts.  Must be in range $MIN_TM .. $MAX_TM.  Default is 1.

      Example:
        setSysInfoCronEntry.sh -w 1 -t 2200 -both
          This will create a cron entry for SysInfoRunMap.sh script to run
          both maps every Monday at 10:00pm.
MESSMARK

}			#end of fShowUsage   

#===================================================================
# fRemoveEntryFromCronsettingsFile
#		Remove sysinfo RunMap entry from cronsettings file             
#===================================================================
function fRemoveEntryFromCronsettingsFile   
{
   # create a copy crontab file without SysInfoRunMap entry     
   origCrontab=`/usr/bin/mktemp`
   tempCrontab=`/usr/bin/mktemp`
   /usr/bin/crontab -l | tee $origCrontab | \
	grep -v -E "SysInfoRunMap|$Prog" > $tempCrontab 2>/dev/null

   if diff $origCrontab $tempCrontab > /dev/null 2>&1 ; then
	# They're the same, do nothing
	:
   else # we need to remove something

       print "Removing existing crontab entry." >> $LOGFILE
       print "Removing existing crontab entry."

       # submit to cron
       fSubmitCrontab
   
       /usr/bin/rm -f $tempCrontab $origCrontab

   fi
}			#end of fRemoveEntryFromCronsettingsFile 

#===================================================================
# fPrintHeader    	Print header message
#===================================================================
function fPrintHeader     
{
	if (($BATCH))
	then
	  print "# $Prog (Version $VERSION) #" >> $LOGFILE
	  print "# Started: `date` #" >> $LOGFILE
	else
	  print "$Prog (Version $VERSION)" 
	  print "Started: `date`" 
	fi
}

#===================================================================
# fPrintTrailer    	Print trailer message
#===================================================================
function fPrintTrailer     
{
	if (($BATCH))
	then
	  print "# Stopped: `date` #" >> $LOGFILE
          print "#################################################" >> $LOGFILE
	else
	  print "Stopped: `date` $Prog" 
	fi
}

#===================================================================
# fExit
#		exiting installation utility
#===================================================================
function fExit
{
    ecode=$1
    if (($BATCH))
    then
      fPrintTrailer
    fi
    if [ -n "$ecode" ] ; then
	exit $ecode
    fi
    exit 0
}			#end of fExit

#===================================================================
# fEvaluateArgs
#		Evaluate command-line options/args
#===================================================================
function fEvaluateArgs
{
	typeset argerr=0

	if [ -n "$_DEBUG" ] ; then set -x; fi
	case "$1" in
	    "")
		BATCH=1
		return
		;;
	    "-?"|-h)
		fShowUsage
		exit 0
		;;
	    -i)
		INTERACTIVE=1
		return
		;;
	    -d)
		DELETE=1
		return
		;;
	esac

	typeset argerr=0

	WEEKDAY="" # override defaults
	TIME=""

	while ((1)) ; do
	    case "$1" in
		"")  # seen them all
		    break
		    ;;
		-w)
		    if [ -n "$2" ] ; then
			WEEKDAY="$2"
			shift
			case "$WEEKDAY" in
        		    @($weekdayPat))
        		        #break
        			;;
      			    *)
				print "$WEEKDAY is not valid day of week -" \
				  "use 0-6, 0=Sunday, 1=Monday, ..."
				argerr=1
				;;
			esac
		    else
        		print "The -w option requires a numeric argument."
			argerr=1
		    fi
		    ;;
		-t)
		    if [ -n "$2" ] ; then
			TIME="$2"
			shift
			case "$TIME" in
			    @($timePat1|$timePat2|$timePat3))
				#break
				;;
			    *)
				print "$TIME is not valid time of day -" \
				  "use 24 hr time (e.g. 2300 for 11:00pm)"
				argerr=1
				;;
			esac
		    else
        		print "The -t option requires a numeric argument."
			argerr=1
		    fi
		    ;;
		-both)
		    CMD_OPTS="${CMD_OPTS} -both"
		    ;;
		-tm)
		    if [ -n "$2" ] ; then
			MULT=$2
			shift
			# is it valid?
    	    		MULT_int=`expr match "$MULT" '^\([0-9][0-9]*\)$'` # try to parse it
	    		if [ -z "$MULT_int" -o "$MULT_int" != "$MULT" ] ; then
			    print "The -tm option requires a numeric argument."
			    argerr=1
    			elif [ "$MULT" -lt "$MIN_TM" -o "$MULT" -gt "$MAX_TM" ] ; then
			    print "The -tm option requires a numeric argument in the range $TM_RANGE."
			    argerr=1
			elif [ "$MULT" -gt "$REAL_MIN_TM" ] ; then
			    # Hokey...see above
			    CMD_OPTS="${CMD_OPTS} -tm $MULT"
			# else ignore anything 10 or below
			fi
		    else
		        print "The -tm option requires a numeric argument in the range $TM_RANGE."
			argerr=1
		    fi
		    ;;
		-i)
		    print "-i (interactive) option ignored."
		    ;;
		*)
		    print "Unrecognized option: $1"
		    argerr=1
		    ;;
	    esac
	    shift
	done

	if [ $argerr -ne 0 ] ; then
	    print "Use the -h option to show the usage information."
	    fExit 1
	fi

	# Look for required options
	if [ -z "$WEEKDAY" -o -z "$TIME" ] ; then
	    print "Both the -t and -w options are required"
	    argerr=1
	elif [ -z "$TIME" ] ; then
	    print "The -t option is also required"
	    argerr=1
	elif [ -z "$WEEKDAY" ] ; then
	    print "The -w option is also required"
	    argerr=1
	fi

	if [ $argerr -ne 0 ] ; then
	    print "Use the -h option to show the usage information."
	    fExit 1
	fi
}

#===================================================================
#===================================================================
# Main Program
#===================================================================
#===================================================================

if [ -n "$_DEBUG" ] ; then set -x; fi

if [ X`/usr/bin/whoami` != "Xroot" ] ; then
    print "You must be root to run this script."
    fExit 1
fi

fInitialize     # Initialize configuration variables

# fShortBanner    # Display utility information.

fEvaluateArgs "$@"

if [ $BATCH -eq 1 ] ; then

   fPrintHeader

   # look for partnerConfig utility on the system
   if [[ -f $partnerConfig ]]
   then
      fGetCronInfoFromISEE
      fCreateAddEntryToCronsettingsFile 
   else
      print "WARNING: ISEE partnerConfig utility." >> $LOGFILE
      print "         not found or is not accessible." >> $LOGFILE
      print "         SysInfoRunMap.sh cron entry not set." >> $LOGFILE
      fExit 1
   fi

elif [ $INTERACTIVE -eq 1 ] ; then

   fShortBanner    # Display utility information.
   fGetUserInput
   fCreateAddEntryToCronsettingsFile 

elif [ $DELETE -eq 1 ] ; then

   BATCH=1 # why this? for fExit?
   fPrintHeader
   fRemoveEntryFromCronsettingsFile

else

   fShortBanner    # Display utility information.
   HOUR=${TIME%??}
   MINUTE=${TIME#??}
   fCreateAddEntryToCronsettingsFile

fi

fExit           # Exit the utility.    
  
# end of MAIN
