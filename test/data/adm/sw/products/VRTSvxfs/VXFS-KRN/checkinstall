#!/sbin/sh
# @(#)src/unix/bufcache/hp/11.23/pkg/VRTSvxfs/pkg_scripts/checkinstall	4.1.68.5 05/26/05 03:52:26
#ident "@(#)vxfs:src/unix/bufcache/hp/11.23/pkg/VRTSvxfs/pkg_scripts/checkinstall	4.1.68.5"
#
# (c)  VERITAS Software Corporation. All rights reserved.
#

#
# The global variables SUCCESS, FAILURE, WARNING, EXCLUDE, PATH, ROOT,
# SW_CTL_SCRIPT_NAME, _pf, PRODUCT, and FILESET are all set by control_utils.
#

#       SW_INITIAL_INSTALL - When this variable is set to "1", the
#                            swinstall session is being run as the
#                            back end of an initial system software
#                            installation ("cold" install).  This
#                            variable is unset at all other times.
#

UTILS=/usr/lbin/sw/control_utils
if [[ ! -f $UTILS ]]
then
	echo "ERROR:   Cannot find the sh functions library $UTILS"
	exit $FAILURE
fi
. $UTILS

LOCAL_UTILS=${SW_CONTROL_DIRECTORY}vxfs_ctrl_utils
if [[ ! -f $LOCAL_UTILS ]]
then
	echo "WARNING: Cannot find the VxFS functions library $LOCAL_UTILS."
	exit $WARNING
fi
. $LOCAL_UTILS checkinstall

#
# disallow installation to an alternate root
#

if [[ -n "$SW_DEFERRED_KERNBLD" ]]
then
	echo "ERROR:   This product cannot be installed on an alternate root."
	exit $FAILURE
fi

err=0

echo "NOTE:    Running the \"checkinstall\" script for \"VRTSvxfs.VXFS-KRN\""

# This script originally removed /etc/mnttab in all cases, which
# causes a problem in the cold-install environment when we are running
# in a chroot environment.  The contents of /etc/mnttab in this case
# reflect the filesystem relative to the chroot, not the actual root.
# There is special functionality in the mount command which
# understands this and does not rewrite /etc/mnttab during initial
# install if it exists.  However, if /etc/mnttab does not exist, it is
# rewritten with all paths relative to the real root, not the chroot.

if [[ ${SW_INITIAL_INSTALL} -ne 1 ]]
then

    # Only remove /etc/mnttab and regenerate it when NOT doing a cold
    # install.

    echo "NOTE:    The \"checkinstall\" script for \"VRTSvxfs.VXFS-KRN\""
    echo "         is regenerating \"/etc/mnttab\"."

    /bin/rm /etc/mnttab >/dev/null 2>&1

    for dev in `/sbin/mount -p  | /sbin/awk '{ if ($3 == "vxfs") print $1}' `
      do
      vers=`/sbin/fstyp -v $dev 2>/dev/null | /sbin/awk '{ if ($1 == "version:") print $2 }' 2>/dev/null`
      if [[ -n "$vers" && $vers -lt 4 ]]; then 
          echo "NOTE:    $dev has disk version $vers.  Filesystems"
          echo "         with version < 4 are not supported with the"
          echo "         new version of VxFS which is being installed."
          mntpt=`/bin/df $dev | /sbin/awk '{print $1}'`
          for t in / /var /usr /tmp /opt
            do
            if [ "$t" = "$mntpt" ]; then
                err=1
                echo "ERROR:   '$t' ($dev) has disk version $vers"
                echo "         Please upgrade the filesystem to version 4"
                echo "         using vxupgrade(1M)."
            fi
          done

      fi
    done
    if [ $err -ne 0 ]; then
	exit $FAILURE
    fi
fi    # if [[ ${SW_INITIAL_INSTALL} -ne 1 ]]

# Check if the VRTSfsnbl package is installed on the machine.
# Return value 0 means that grep succeeded, i.e. the product is installed.
swlist -l fileset 2>/dev/null | grep -Fq "VRTSfsnbl.VXFSNBL-LIBS" 2>/dev/null

fsnbl_installed=$?

# Check if the VRTSfsnbl package is selected for installation in this session.
# Return value 0 means it is selcted.
is_software_selected VRTSfsnbl.VXFSNBL-LIBS
fsnbl_selected=$?

# If the VRTSfsnbl package is installed or is being selected, we should
# return an $EXCLUDE error.
if [[ $fsnbl_installed -eq 0 || $fsnbl_selected -eq 0 ]]; then
	echo "ERROR:   VRTSfsnbl package should be removed / excluded before"
	echo "         installing VRTSvxfs 4.1"
	exit $EXCLUDE
fi

exit $SUCCESS
