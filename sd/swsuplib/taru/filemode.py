# filemode.py -- make a string describing file modes
#   Copyright (C) 1985, 1990, 1993 Free Software Foundation, Inc.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# This may not be needed at all using python stat module - Paul Weber

import stat


def mode_string(mode, str1):
    str1[0] = ftypelet(mode)
    rwx((mode & 0o700) << 0, str1[1:])
    rwx((mode & 0o070) << 3, str1[4:])
    rwx((mode & 0o007) << 6, str1[7:])
    setst(mode, str1)


# ftypelet(bits)
# Look at read, write, and execute bits in BITS and set
#   flags in CHARS accordingly.  


def rwx(bits, chars):
    chars[0] = 'r' if (bits & stat.S_IRUSR) != 0 else '-'
    chars[1] = 'w' if (bits & stat.S_IWUSR) != 0 else '-'
    chars[2] = 'x' if (bits & stat.S_IXUSR) != 0 else '-'


# Set the 's' and 't' flags in file attributes string CHARS,
# according to the file mode BITS.


def setst(bits, chars):
    if (bits & stat.S_ISUID) != 0:
        if chars[3] != 'x':
            # Set-uid, but not executable by owner.
            chars[3] = 'S'
        else:
            chars[3] = 's'

    if (bits & stat.S_ISGID) != 0:
        if chars[6] != 'x':
            # Set-gid, but not executable by group.
            chars[6] = 'S'
        else:
            chars[6] = 's'
    if (bits & stat.S_ISVTX) != 0:
        if chars[9] != 'x':
            # Sticky, but not executable by others.
            chars[9] = 'T'
        else:
            chars[9] = 't'


# filemodestring - fill in string STR with an ls-style ASCII
#   representation of the st_mode field of file stats block STATP.
#   10 characters are stored in STR; no terminating null is added.
#   The characters stored in STR are:
#
#   0	File type.  'd' for directory, 'c' for character
#	special, 'b' for block special, 'm' for multiplex,
#	'l' for symbolic link, 's' for socket, 'p' for fifo,
#	'-' for regular, '?' for any other file type
#
#   1	'r' if the owner may read, '-' otherwise.
#
#   2	'w' if the owner may write, '-' otherwise.
#
#   3	'x' if the owner may execute, 's' if the file is
#	set-user-id, '-' otherwise.
#	'S' if the file is set-user-id, but the execute
#	bit isn't set.
#
#   4	'r' if group members may read, '-' otherwise.
#
#   5	'w' if group members may write, '-' otherwise.
#
#   6	'x' if group members may execute, 's' if the file is
#	set-group-id, '-' otherwise.
#	'S' if it is set-group-id but not executable.
#
#   7	'r' if any user may read, '-' otherwise.
#
#   8	'w' if any user may write, '-' otherwise.
#
#   9	'x' if any user may execute, 't' if the file is "sticky"
#	(will be retained in swap space after execution), '-'
#	otherwise.
#	'T' if the file is sticky but not executable.  


# def set_perms(fps, set1, p_error):
def set_perms(fps, set1): # Not sure how p_error used as it is set in the function
    s = fps
    # set == 0 is user
    # set == 1 is group
    # set == 2 is other
    if set1 == 0:
        m_r = stat.S_IRUSR
        m_w = stat.S_IWUSR
        m_x = stat.S_IXUSR
        m_s = stat.S_ISUID
        m_t = 0
    elif set1 == 1:
        m_r = stat.S_IRGRP
        m_w = stat.S_IWGRP
        m_x = stat.S_IXGRP
        m_s = stat.S_ISGID
        m_t = 0
    elif set1 == 2:
        m_r = stat.S_IROTH
        m_w = stat.S_IWOTH
        m_x = stat.S_IXOTH
        m_s = 0
        m_t = stat.S_ISVTX
    else:
        m_r = 0
        m_w = 0
        m_x = 0
        m_s = 0
        m_t = 0
    # p_error[0] = 0
    if s == '':
       # p_error[0] = 1
        return 0
    r_b = s
    s += 1
    if s == '':
        # p_error[0] = 1
        return 0
    w_b = s
    s += 1
    if s == '':
        # p_error[0] = 1
        return 0
    x_b = s
    ret_mode = 0
    if r_b == 'r':
        ret_mode |= m_r
    if w_b == 'w':
        ret_mode |= m_w
    if x_b == 'x':
        ret_mode |= m_x
    if x_b == 's':
        ret_mode |= m_x
        ret_mode |= m_s
    if x_b == 't':
        ret_mode |= m_x
        ret_mode |= m_t
    return ret_mode


def swlib_filemodestring(mode, str1):
    mode_string(mode, str1)


def filemodestring(statp, str1):
    mode_string(statp.st_mode, str1)


# Return a character indicating the type of file described by
#   file mode BITS:
#   'd' for directories
#   'b' for block special files
#   'c' for character special files
#   'm' for multiplexor files
#   'l' for symbolic links
#   's' for sockets
#   'p' for fifos
#   '-' for regular files
#   '?' for any other file type.  


def ftypelet(bits):
    if stat.S_ISBLK(bits):
        return 'b'

    if stat.S_ISCHR(bits):
        return 'c'

    if stat.S_ISDIR(bits):
        return 'd'

    if stat.S_ISREG(bits):
        return '-'

    if stat.S_ISFIFO(bits):
        return 'p'

    if stat.S_ISLNK(bits):
        return 'l'

    if stat.S_ISSOCK(bits):
        return 's'

    #    if stat.S_ISMPC(bits):
    #        return 'm'

    #    if stat.S_ISNWK(bits):
    #        return 'n'

    # Cray migrated dmf file.
    #    if stat.S_ISOFD(bits):
    #        return 'M'
    # Cray migrated dmf file.  
    #    if stat.S_ISOFL(bits):
    #       return 'M'

    return '?'


def swlib_filestring_to_mode(str1, do_include_type):
    mode_ret = 0
    error = 0
    s = str1
    type1 = s[0]
    if type1 == 'd' or type1 == '-' or type1 == 'l' or type1 == 's':
        if do_include_type:
            # FIXME, set type
            pass
    else:
        return 0
    s += 1
    if s == '':
        return 0
    #ret = set_perms(s, 0, error) # user
    ret = set_perms(s, 0)  # user
    if error:
        return 0
    mode_ret |= ret
    s += 1
    if s == '':
        return 0
    s += 1
    if s == '':
        return 0
    s += 1
    if s == '':
        return 0
    # ret = set_perms(s, 1, error) # group
    ret = set_perms(s, 1)  # group
    if error:
        return 0
    mode_ret |= ret
    s += 1
    if s == '':
        return 0
    s += 1
    if s == '':
        return 0
    s += 1
    if s == '':
        return 0
    # ret = set_perms(s, 2, error) # other
    ret = set_perms(s, 2)  # other
    if error:
        return 0
    mode_ret |= ret
    return mode_ret