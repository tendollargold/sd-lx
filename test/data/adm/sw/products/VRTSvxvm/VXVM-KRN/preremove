#! /sbin/sh
# $Id: preremove,v 1.17.2.1.58.1 2004/09/16 21:35:24 yatin Exp $
#ident "$Source: /project/unixvm-cvs/src/hp/package/vxvm/pkg_scripts/preremove,v $"

# Copyright (c) 2001 VERITAS Software Corporation.  ALL RIGHTS RESERVED.
# UNPUBLISHED -- RIGHTS RESERVED UNDER THE COPYRIGHT
# LAWS OF THE UNITED STATES.  USE OF A COPYRIGHT NOTICE
# IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
# OR DISCLOSURE.
# 
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND
# TRADE SECRETS OF VERITAS SOFTWARE.  USE, DISCLOSURE,
# OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR
# EXPRESS WRITTEN PERMISSION OF VERITAS SOFTWARE.
# 
#               RESTRICTED RIGHTS LEGEND
# USE, DUPLICATION, OR DISCLOSURE BY THE GOVERNMENT IS
# SUBJECT TO RESTRICTIONS AS SET FORTH IN SUBPARAGRAPH
# (C) (1) (ii) OF THE RIGHTS IN TECHNICAL DATA AND
# COMPUTER SOFTWARE CLAUSE AT DFARS 252.227-7013.
#               VERITAS SOFTWARE
# 1600 PLYMOUTH STREET, MOUNTAIN VIEW, CA 94043

#
# Perform preremove processing for the VxVM package.
#

#
# The global variables SUCCESS, FAILURE, WARNING, EXCLUDE, PATH, ROOT,
# SW_CTL_SCRIPT_NAME, _pf, PRODUCT, and FILESET are all set by control_utils.
#

UTILS=/usr/lbin/sw/control_utils
if [ ! -f $UTILS ]
then
	print "ERROR:   Cannot find the sh functions library $UTILS"
	exit 1
fi
. $UTILS

#
# Searches all the parameters in the system file. Returns the number of 
# occurences.
#
find_driver()
{
	integer j=0;
	for i in $@
	do
		if [[ -n "`awk '{ print $1 }' < ${SW_SYSTEM_FILE_PATH} | grep "^${i}$" `" ]]
		then
			j=`expr $j + 1`
		fi
	done
	return $j;
}

#
# Print an (almost) generic msg to advising the user to build kernel. 
# $1 --- either "added" or "removed"
# $2 --- either "installation" or "remove" etc.
#
vol_krn_build_msg()
{
	echo "NOTE:    The VxVM drivers have been $1 to the ${SW_SYSTEM_FILE_PATH}";
	echo "         file. You must build the kernel and reboot. You need to build";
	echo "         the kernel and reboot only if the $2 process does not";
	echo "         automatically do it for you.";
	echo "         Use \" /usr/sbin/mk_kernel -s ${SW_SYSTEM_FILE_PATH} \"";
	echo "         to build the new kernel"
}

VX_DRIVERS="vxvm vxdmp vol vols"
VXVM_TUNABLES_STORE="/etc/vx/tunables.config"

exitval=$SUCCESS                # Anticipate success

VOLBUSY="\
ERROR: The Volume Daemon (vxconfigd) could not be stopped.  The
${NAME} package can not be removed.  Kill the vxconfigd process
and try again."

#
# Disabling scsi3 discovery
#

if [ -x /usr/sbin/vxddladm ]
then
	/usr/sbin/vxddladm disablescsi3
fi

#
# Stop vxconfigd if it is running and kill all iods.
#

if [ -x /sbin/vxdctl ]
then
	x=`/sbin/vxdctl mode 2> /dev/null`
	if [ ! -z "$x" ] && [ "X$x" != "Xmode: not-running" ]
	then
		echo "NOTE:    Stopping vxconfigd..."
		/usr/sbin/vxdctl stop
	fi
	/sbin/vxdctl -k stop 2> /dev/null
fi
/sbin/vxiod -f set 0 2> /dev/null

RELOCD_FILE=vxrelocd
RELOC_PROC=`ps -e | grep ${RELOCD_FILE} | grep -v grep | awk '{ print $1}'`
if [ ! -z "${RELOC_PROC}" ]
then
	for pno in ${RELOC_PROC}
	do
        	kill -9 ${pno} 2> /dev/null
	done
fi

CACHED_FILE=vxcached
CACHE_PROC=`ps -e | grep ${CACHED_FILE} | grep -v grep | awk '{ print $1}'`
if [ ! -z "${CACHE_PROC}" ]
then
	for pno in ${CACHE_PROC}
	do
        	kill -9 ${pno} 2> /dev/null
	done
fi

BACKUPD_FILE=vxconfigbackupd
BACKUPD_PROC=`ps -e | grep ${BACKUPD_FILE} | grep -v grep | awk '{ print $1}'`
if [ ! -z "${BACKUPD_PROC}" ]
then
	for pno in ${BACKUPD_PROC}
	do
        	kill -9 ${pno} 2> /dev/null
	done
fi

NOTIFY_FILE=vxnotify
NOTIFY_PROC=`ps -e | grep ${NOTIFY_FILE} | grep -v grep | awk '{ print $1}'`
if [ ! -z "${NOTIFY_PROC}" ]
then
	for pno in ${NOTIFY_PROC}
	do
        	kill -9 ${pno} 2> /dev/null
	done	# We have VxVM driver 
fi

#
# Stop vradmind if it is running.
/sbin/init.d/vras-vradmind.sh stop

#
# Stop in.vxrsyncd if it is running.
/sbin/init.d/vxrsyncd.sh stop

#
# Undo the changes to /stand/system.
# 1. Remove drivers
# 2. Should also remove any tunables; that may have been set
#

find_driver $VX_DRIVERS
driver_found=$?
driver_found=0

if [ $driver_found -ne 0 ]
then
    
	echo "NOTE:    Removing changes to ${SW_SYSTEM_FILE_PATH}..."

	#
	# Remove drivers from the /stand/system file.
	#

	for driver in "$VX_DRIVERS"
	do
		mod_systemfile $SW_SYSTEM_FILE_PATH -d ${driver}
		if [ $? -ne 0 ]
		then
			print "ERROR:   Cannot update $SW_SYSTEM_FILE_PATH to"
			print "         remove ${driver} ($PRODUCT functionality)."
			exitval=$FAILURE 
		fi
	done

	#
	# Now go for any VxVM tunables that the user may have set in the system
	# file. Pick up the original list from the vxvm master file, either the
	# one that is in /usr/conf/master.d OR the one with the current fileset.
	# Then pick out the ones that have actually been set in the system file
	# and remove them.
	#

	master="/usr/conf/master.d/vxvm"
	[ -s $master ] || \
		master="${SW_ROOT_DIRECTORY}${SW_LOCATION}usr/conf/master.d/vxvm"
	tunables=`awk '{ \
		if ($1 == "$TUNABLE") { # Start of tunables section
			var = 1;
			next;
		}
		if ($1 == "$$$") { # End of tunables section
			var = 0;
			next;
		}
		if ( (var == 1) && (index($1,"*") != 1) ) #skip comments
			print $1;
	 }' < $master`

	 if [ -n "$tunables" ]
	 then
	 	for i in $tunables
	 	do
			actual_tunables=`awk ' {
				if ( $1 == d ) print $1;
			}' < $SW_SYSTEM_FILE_PATH d=$i`
	 	done
	 else
		actual_tunables=""
	 fi

	 if [ -n "$actual_tunables" ]
	 then
	 	for i in $actual_tunables
	 	do
		mod_systemfile $SW_SYSTEM_FILE_PATH -d $i
			if [ $? -ne 0 ]
			then
				print "ERROR:  Cannot update $SW_SYSTEM_FILE_PATH to"
				print "\tremove the tunable ${i} for the "
				print "\t($PRODUCT functionality)."
				exitval=$FAILURE 
			fi
		done
	fi

	#
	# Config doesn't care if there are multiple instances of 
	# "dump lvol", so don't bother checking.
	#

	if [ $exitval -ne $FAILURE ]
	then
		vol_krn_build_msg "removed" "remove"
	fi

fi

echo "NOTE:    Removing files under /etc/vx..."
rm -rf /etc/vx/tempdb 2>/dev/null

# Removing the files generated by vras
rm -f /var/vx/vras/log/vradmind_log_A
rm -f /var/vx/vras/log/vradmind_log_B
rm -f /var/vx/vras/log/vradmind_log_C

exit $exitval
