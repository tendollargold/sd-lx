#! /sbin/sh
########
# Product: OS-CORE
# Fileset: KERN-RUN
# bootlif_funcs
# @(#)B11.23_LR 
#########
#
# (c) Copyright Hewlett-Packard Company, 1994,1995,1996
#
#########

##########################################################
#
# bootlif_funcs: a source-able file of bootlif functions
#                used by the KERN-RUN control scripts.
# side effects:  The following mnemonic values are set
#                whenever this file is sourced:
#
# requirements: The function sd_msg must be defined in the
#               calling environment, theoretically directly
#		defined by checkinstall & postinstall &
#		sucked in from tscript_utils by the tscripts.
#
##########################################################

#
# Boot Configuration File path definition
#

BOOTCONF=${SW_ROOT_DIRECTORY}/stand/bootconf

#
# FindBoot exit codes
#

LVM_SUCCESS=3
LVM_WARNING=4
PARTITION_SUCCESS=5
PARTITION_WARNING=6
WHOLE_SUCCESS=7
WHOLE_WARNING=8
ALT_ROOT_FAILURE=9

#
# FindBoot (boot configuration file, effective root)
#
# Echos the device file(s) corresponding to the physical device
# containing the bootlif for the effective root if any exists.
# Uses the contents of the boot configuration file if it exists,
# otherwise tries a heuristic search on the physical device containing 
# the effective root.
# 
# Return values:
#	LVM_SUCCESS - found boot partition and all mirrors on a root
#		      file system mounted via LVM.
#	LVM_WARNING - found LVM root filesystem and may not have located
#		      a good boot partition - double check to see if any
#		      partitions have been named.
#	PARTITION_SUCCESS - successfully found the boot partition, the
#		      disk has hard partitions.
#	PARTITION_WARNING - the boot configuration file claims a hard
#		      partitioned device, but no boot areas have been found.
#	WHOLE_SUCCESS - successfully found the boot area, the disk is a
#		      9.0 700 style "whole disk".
#	WHOLE_WARNING - not used.
#	ALT_ROOT_FAILURE - the effective root is neither /, nor a mount
#		      point: no processing has been done.
#	FAILURE -     Guess.
#
# Error messages to stderr if called incorrectly
#
FindBoot()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    if [[ $# -ne 2 ]]; then
        sd_msg -e "Function $0 called incorrectly: $@" 1>&2
	sd_msg -b "Proper calling sequence is: $0 ConfigFile RootDirectory" 1>&2
        return 1
    fi

    typeset ConfigFile=$1	# set up mnemonics for calling parameters
    typeset RootDir=$2

    typeset TestedBootAreas=""	# Initialize to null.

    typeset FAILURE=1		# Just in case calling environment 
				# doesn't have it set.

    # see if lif cmds work -they may not work if shared libraries are improperly
    # installed  ie /usr/lib/libc.<n> does not exist.  These cmds are needed 
    # later

    lifls /dev/null > /dev/null 2> /tmp/liflib$$
    if [[ $? != 1 ]]; then  # the result of one is returned just because
                            # /dev/null is sent as the device
	print "ERROR:   Abort above due to the following shared library problem(s)"
	sed -e 's/^/         /' /tmp/liflib$$
	rm -f /tmp/liflib$$
	exit $FAILURE
    fi
    rm -f /tmp/liflib$$

    # Local variables
    typeset BootDevices _typeflag RootDevFile
    typeset -i _result ReturnCode

    InstallSupported $RootDir		#Check that updating the LIF makes sense
    if [[ $? -ne 0 ]]; then
	if [[ -z "$SW_DEFERRED_KERNBLD" ]]; then
	    sd_msg -p "Local root device not found in mount table.  \
			Installation of boot utilities cannot proceed." 1>&2
	    return $FAILURE
	else
	    return $ALT_ROOT_FAILURE
	fi
    fi

    if [[ -f $ConfigFile ]]; then
	BootDevices=`ReadConfig $ConfigFile`
	_result=$?
	if [[ $_result -eq 1 ]]; then
	    sd_msg -p "The boot area configuration file $ConfigFile is either \
			empty or improperly formatted.  The proper format is \
			one boot area per line, with each line containing:" 1>&2
	    sd_msg -b "" 1>&2
	    sd_msg -b '<flag> <device file>' 1>&2
	    sd_msg -b "" 1>&2
	    sd_msg -b "Where <flag> is one of the letters l, p, or w, and \
			<device file> is the absolute path to the device file \
			which controls the physical device containing the boot \
			area.  The flags indicate whether the root device is \
			an LVM volume, a hard partitioned disk, or a 9.0x \
			release series 700 style \"whole\" disk." 1>&2
	    sd_msg -b  "Either correct the file or move it aside and restart \
			snoop, upgrade, or swinstall." 1>&2
	    return $FAILURE
	else
	    _typeflag=${BootDevices%% *}
	    case $_typeflag in
		"l")	if [[ $_result -eq 0 ]]; then
			    ReturnCode=$LVM_SUCCESS
			else
			    ReturnCode=$LVM_WARNING
			fi
			;;
		"p")	if [[ $_result -eq 0 ]]; then
			    ReturnCode=$PARTITION_SUCCESS
			else
			    ReturnCode=$PARTITION_WARNING
			fi
			;;
		"w")	if [[ $_result -eq 0 ]]; then
			    ReturnCode=$WHOLE_SUCCESS
			else
			    ReturnCode=$WHOLE_WARNING
			fi
			;;
		*)	sd_msg -p "$@: unknown device type $_typeflag." 1>&2
			;;
	    esac
	    for item in $BootDevices
	    do
		if [[ "$item" = "l" || "$item" = "p" || "$item" = "w" ]]; then
					# ignore flags
		    continue
		fi
		if LifTest $item; then
		    TestedBootAreas="$item $TestedBootAreas"
		else
		    sd_msg -w  "Boot device $item in boot configuration file \
				$ConfigFile does not contain a valid boot \
				LIF.  Skipping." 1>&2
		    case $_typeflag in
			"l")	ReturnCode=$LVM_WARNING
				;;
			"p")	ReturnCode=$PARTITION_WARNING
				;;
			"w")	ReturnCode=$WHOLE_WARNING
				;;
		    esac
		fi	# LifTest $item
	    done	# each item in $BootDevices
	fi		# BootDevices=`ReadConfig $ConfigFile`; then
    else		# [[ -f $ConfigFile ]]
	RootDevFile=`GetRootDev $RootDir`
	if [[ $? -ne 0 ]]; then
	    sd_msg -p  "Unable to find a device file corresponding to $RootDir \
			other than /dev/rroot.  Possible solutions are: Change \
			your system so that a \"mount -u\" will respond with a \
			\"real\" device file for a disk.  On 9.X macines check \
			whether /etc/checklist is valid, if not correct \
			it, then \"rm /etc/mnttab\" and \"mount -u\".  Or fill \
			out the file $ConfigFile with the location(s) of your \
			boot area(s).  The format of $ConfigFile is one boot \
			area per line, with each line containing:" 1>&2
	    sd_msg -b "" 1>&2
	    sd_msg -b '<flag> <device file>' 1>&2
	    sd_msg -b "" 1>&2
	    sd_msg -b  "Where <flag> is one of the letters l, p, or w, and \
			<device file> is the absolute path to the device file \
			which controls the physical device containing the boot \
			area.  The flags indicate whether the root device is \
			an LVM volume, a hard partitioned disk, or a 9.0x \
			release series 700 style \"whole\" disk." 1>&2
	    return $FAILURE
	fi
	BootDevices=`ReadLvol $RootDevFile`
	if [[ $? -eq 0 ]]; then			# Root is an logical volume
	    ReturnCode=$LVM_SUCCESS
	    for item in $BootDevices		# Test all boot areas 
						# (may be mirrored)
	    do
		if LifTest $item; then
		    TestedBootAreas="$item $TestedBootAreas"
		else
		    ReturnCode=$LVM_WARNING
		    sd_msg -w  "Physical device $item identified by LVM \
			commands to be the root directory does not contain a \
			valid boot LIF.  Skipping and searching for LVM \
			mirrors with a boot area." 1>&2
		fi	# LifTest $item
	    done	# each item in $BootDevices
	elif BootDevices=`ReadWholeDisk $RootDevFile`; then
			# Root is "whole disk" (function does lifls, 
			# so we don't have to)
	    print "$BootDevices"			# output root device
	    return $WHOLE_SUCCESS
	elif BootDevices=`GetSectionSix $RootDevFile`; then	
			# Root is a hard partitioned volume (function does 
			# lifls, so we don't have to)
	    print "$BootDevices"			# output root device
	    return $PARTITION_SUCCESS
	else				# Don't know what root is - give up.
	    sd_msg -p  "Unable to find a boot area.  Create the file \
			$ConfigFile with the device file that \
			points to the boot LIF.  The format of $ConfigFile is \
			one boot area per line, with each line containing:" 1>&2
	    sd_msg -b "" 1>&2
	    sd_msg -b '<flag> <device file>' 1>&2
	    sd_msg -b "" 1>&2
	    sd_msg -b  "Where <flag> is one of the letters l, p, or w, and \
			<device file> is the absolute path to the device file 
			which controls the physical device containing the boot \
			area.  The flags indicate whether the root device is \
			an LVM volume, a hard partitioned disk, or a 9.0x \
			release series 700 style \"whole\" disk." 1>&2
	    return $FAILURE
	fi	# BootDevices=`ReadLvol $RootDevFile`
    fi		# [[ -f $ConfigFile ]]

    # The following test is only for reading from /stand/bootconf, since there
    # might be multiple devices specified.  If any of them are good, we proceed.
    # The tests in the lower level scripts don't keep track of whether they 
    # found anything good, so we check here. 

    if [[ -z "$TestedBootAreas" ]]; then	# If we haven't identified 
						# a good boot LIF
	sd_msg -p "Did not find any valid boot areas to update.  See the \
		WARNINGS (above) for more information." 1>&2
	return $FAILURE
    fi

    # There currently is no test for having "excess" boot areas defined. 
    # (Since we only do existing boot areas, this may not be a problem.

    print "$TestedBootAreas"	# Echo the good stuff to stdout, 
				# as per function "prototype"

    return $ReturnCode	#  Exit with whatever we found.
}	# End of FindBoot()

#
# GetRootDev (root directory)
#
# Prints root device file (may be an LVM volume group) to stdout.
# Returns 0 on success, 1 on error
# Error messages to stderr if called incorrectly
#
GetRootDev()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    # Local Variables
    typeset _device

    if [[ $# -ne 1 ]]; then
	sd_msg -e "Function $0 called incorrectly: $@" 1>&2
	sd_msg -b "Proper calling sequence is: $0 RootDirectory" 1>&2
	return 1
    fi
    _device=`devnm $1 | awk '{print $1}'`
    if [[ -z "$_device" || "$_device" = "/dev/root" \
	|| "$_device" = "/dev/rroot" ]]; then
	mount -u	# Update mount table and try again
	_device=`devnm $1 | awk '{print $1}'`
	if [[ -z "$_device" || "$_device" = "/dev/root" \
	    || "$_device" = "/dev/rroot" ]]; then
	    return 1	# Give it up
	fi
    fi
    print "$_device"
    return 0
}

#
# GetSectionSix (device file)
# Prints device file for section six and returns 0 if HPUX is in section 6.
# Returns 1, otherwise.
# Error message to stderr if called incorrectly.
#
GetSectionSix()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    # Local variables
    typeset _rev _offset

    if [[ $# -ne 1 ]]; then
	sd_msg -e "Function $0 called incorrectly: $@" 1>&2
	sd_msg -b "Proper calling sequence is: $0 DeviceFile" 1>&2
	return 1
    fi

    #
    # Use the device file for the whole disk to search for LIF
    #

    _rev=`uname -r | grep '10.[0-9][0-9]' | wc -l`	# 1 if 10.x, 0 otherwise
    if [[ $_rev -eq 0 ]]; then
	_device=${1%s[0-9]*}s2
    else
	_device=${1%s[0-9]*}
    fi

    #
    # Find the offset for HPUX (if it's there)
    #

    _offset=$(lifls -l ${_device}:HPUX 2>/dev/null | \
	awk '$1 == "HPUX" {print $3}')

    #
    # If no offset (no HPUX) or if offset is > 8192 sectors (256 bytes/sector,
    # section six limited to first 2Mb), then HPUX is not in section six
    # (return 1, silently)
    #

    if [[ -z "$_offset" || $_offset -ge 8192 ]]; then
	return 1
    else
	print ${1%s[0-9]*}s6
	return 0
    fi
}

#
# InstallSupported (directory)
# Returns 0 if directory is a mount point (including /)
# Returns 1 otherwise.
# Error messages to stderr and return value of 1 if called incorrectly
#
InstallSupported()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    typeset _x=""	# Declare local variables to keep them local.

    if [[ $# -ne 1 ]]; then
	sd_msg -e "Function $0 called incorrectly: $@" 1>&2
	sd_msg -b "Proper calling sequence is: $0 Directory" 1>&2
	return 1
    fi

    for _dir in `mount -p | awk '{print $2}'`	# Scan mount table
    do
        _type=`mount -p | grep " $_dir " | awk '{print $3}'`	
					# skipping NFS mounts
	if [[ "$1" = $_dir ]] && [[ $_type != "nfs" ]]; then
					# Found dir in mnttab - supported.
	    return 0
	fi
    done
    return 1		# Scanned whole table - didn't find it - not supported
}

#
# LifTest (device file)
# Returns 0 if there is a LIF with HPUX in it on device file.
# Returns 1 otherwise.
# Error messages to stderr and return value of 1 if called incorrectly.
#
LifTest ()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    typeset _x=""	# Declare local variables to keep them local
    typeset _files

    if [[  $# -ne 1 || ( ! -b $1 && ! -c $1) ]]; then
        sd_msg -e "Function $0 called incorrectly: $@" 1>&2
        sd_msg -b "Proper calling sequence is: $0 DeviceFile" 1>&2
	return 1
    fi
    
    _files=`lifls ${1}:HPUX 2>/dev/null`
    if [[ $? -ne 0 ]]; then
	return 1	# lifls couldn't find any kind of LIF on device
    fi
    if [[ "${_files%% *}" = "HPUX" ]]; then
	return 0	# Found the old boot kernel, success
    fi

    return 1	# Scanned entire LIF, didn't find old boot kernel - failure
}

#
# PutConfig (BootConf, BootFlag, Device [, device ...])
# Moves existing boot config file to "file.prev," 
# Prints canned header to file
# Appends "flag device" to file for each device 
# NOTE: doesn't check the lines for correctness/sanity.
# NOTE: assumes all devices are of the same "type".
# Error messages on incorrect calling sequence to stderr
# Returns 0 on no failures, 1 otherwise
#
PutConfig()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    if [[ $# -lt 3 ]]; then
	sd_msg -e "Function $0 called incorrectly: $@" 1>&2
        sd_msg -b "Proper calling sequence is: $0 BootConf BootFlag Device \
		[...]" 1>&2
	return 1
    fi

    
    typeset _ConfFile=$1 _Flag=$2	# Save ConfigFile & flags in local vars.
    shift 2

    if [[ -f $_ConfFile ]]; then
	mv ${_ConfFile} ${_ConfFile}.prev
	if [[ $? -ne 0 ]]; then
	    sd_msg -w "Unable to move $_ConfFile to ${_ConfFile}.prev.  Not \
		saving latest boot configuration." 1>&2
	    return 1
	fi
    fi

    print "# Boot Device configuration file" > $_ConfFile
    if [[ $? -ne 0 ]]; then		# Can't write/create file.
	sd_msg -w "Unable to create/write $_ConfFile.  Not saving latest boot \
		configuration and restoring previous $_ConfFile (if any)." 1>&2
	[[ -f ${_ConfFile}.prev ]] && mv ${_ConfFile}.prev $_ConfFile
	return 1
    fi

    print "# This File contains information regarding the location" \
	>> $_ConfFile
    print "# of the boot LIF.  It is used by the KERN-RUN fileset to" \
	>> $_ConfFile
    print "# update the boot kernel."  >> $_ConfFile

    typeset _device=""
    for _device in $@
    do
	print "$_Flag $_device" >> $_ConfFile
    done
    return 0
}

#
# ReadConfig (BootConf)
# Returns 1 and prints message on format errors (where problem is determinable)
#
# Returns 2 and issues a warning to stderr if file refers to an unsupported 
# format (hardware address).
#
# Returns 0 and prints flags and device files to stdout, if format OK and file
# refers to an existing LIF volume.
#
# Returns 2, issues a warning to stderr, and prints flags and device files to 
# stdout if format OK, but file points to an area which is not currently a 
# boot LIF.
#
# Returns 1 and prints error to stderr if called incorrectly.
#

# Format of Boot Config file: <flag> <device file>
# Where
# h = hardware address (not supported yet).
# l = lvm
# p = partitions (hard partitions)
# w = whole disk

ReadConfig()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    if [[ $# -ne 1 || ! -f $1 || ! -r $1 ]]; then	# called wrong, 
							# or Bootconf invalid
	sd_msg -e "Function $0 called incorrectly: $@" 1>&2
	sd_msg -b "Proper calling sequence is: $0 BootConfigFile" 1>&2
	return 1
    fi

    typeset _flag="" _device="" _catchall""	# Declare local variables to 
						# keep them local.

    typeset _liftype=`grep -v '^#' $1 | awk 'NR == 1 {print $1}'`	
						# Save first flag from the file

    if [[ $_liftype = "h" ]]; then		# Warning exit on unsupported 
						# functionality.

	sd_msg -w "The boot configuration file, $1 is specifying the boot LIF \
		using a hardware address.  This is not yet supported." 1>&2

	return 2
    fi

    if [[ $_liftype != "l" && $_liftype != "p" && $_liftype != "w" ]]; then 
						# unrecognized flag

	sd_msg -p "The boot configuration file, $1, is missing or has an \
		invalid character in the flag field (\"$_liftype\").  The only \
		supported characters are:" 1>&2
	sd_msg -b "h - for hardware address (not yet supported)." 1>&2
	sd_msg -b "l - for LVM root disk." 1>&2
	sd_msg -b "p - for hard partitioned disk." 1>&2
	sd_msg -b "w - for whole disk format." 1>&2

	return 1
    fi

    typeset -i _exitval=0	# need this variable, because of sub-shell 
				# weirdness in following do loop
    awk '$1 !~ /^#/ && NF > 0' $1 | while read _flag _device _catchall	
				# reading from the bootconf file to variables
    do
	if [[ -n "$_catchall" && "${_catchall#\#}" = "$_catchall" ]]; then
				# extra characters on the line

	    sd_msg -p  "The boot configuration file, $1, has unrecognized \
			extra characters on a line:" 1>&2
	    sd_msg -b "\"$_flag $_device $_catchall\" " 1>&2

	    _exitval=1
	    return $_exitval
	fi
	
	if [[ "$_flag" != "$_liftype" ]]; then	# if device type changed.
	    sd_msg -p  "The boot configuration file, $1, contains \
			contradictory boot LIF types in the flag field.  \
			It specifies both \"$_liftype\" and \"$_flag\"." 1>&2
	    _exitval=1
	    return $_exitval
	fi

	if [[ ! -b "$_device" && ! -c "$_device" ]]; then	
					# is this a real special device file?

	    sd_msg -p "The boot configuration file, $1, has a line that \
			does not contain a valid special device file:" 1>&2
	    sd_msg -b "\"$_flag $_device\"." 1>&2

	    _exitval=1
	    return $_exitval
	fi

	#
	# Verify that there really is a bootLIF where we are pointing.
	#
	case $_flag in
	    l)	typeset _testfile=`lifls $_device:LABEL 2>/dev/null`
					# see if the lif has a LABEL file
		lifls $_device > /dev/null 2>&1	
					# see if there is any LIF there at all
		if [[ ${_testfile%% *} != "LABEL" && $? -eq 0 ]]; then	
					# No LABEL file and there is a LIF

		    sd_msg -w  "The boot configuration file, $1, indicates \
				that $_device is a device with a boot LIF on \
				an LVM root disk.  The LIF on this device is \
				not compatible with LVM (does not have a LABEL \
				file).  Skipping this device (skipping this \
				line in $1)." 1>&2

		    _exitval=2
		    continue
		elif [[ ${_testfile%% *} != "LABEL" ]]; then
					# NO LABEL file and no LIF.

		    sd_msg -w  "The boot configuration file, $1, indicates \
				that $_device is a device with a boot LIF on \
				an LVM root disk.  There is no LIF on this \
				device.  Skipping this device (skipping this \
				line in $1)." 1>&2

		    _exitval=2
		    continue
		fi		# past this point, there is a LABEL file & a LIF
		typeset _testfile=`lifls $_device:HPUX 2>/dev/null`
				# see if the lif has a HPUX file
		if [[ ${_testfile%% *} != "HPUX" ]]; then

                    sd_msg -w  "The boot configuration file, $1, indicates \
				that $_device is the proper location for \
				the boot LIF.  The LIF specified by this \
				device is not a boot LIF, because it does not \
				currently contain a boot kernel.  Skipping \
				this device (skipping this line in $1)." 1>&2

		    _exitval=2
		    continue
		fi
		;;
	    w)  typeset _offset=$(lifls -l ${_device}:HPUX 2>/dev/null | \
		awk '$1 == "HPUX" {print $3}')
		if [[ -z "$_offset" ]]; then

		    sd_msg -w  "The boot configuration file, $1, indicates \
				that $_device is a device with a boot LIF on \
				a \"whole disk\" root disk.  There is either \
				no LIF currently on this device or the \
				existing LIF does not have a boot kernel.  \
				Skipping this device (skipping this line in \
				$1)." 1>&2

		    _exitval=2
		    continue
		fi
		if [[ $_offset -lt 8192 ]]; then

		    sd_msg -w  "The boot configuration file, $1, indicates \
				that $_device is a device with a boot LIF on \
				a \"whole disk\" root disk.  The LIF on this \
				device is for a hard partitioned disk.  \
				Skipping this device (skipping this line in \
				$1)." 1>&2

		    _exitval=2
		    continue
		fi
		;;
	    p)	typeset _offset=$(lifls -l ${_device}:HPUX 2>/dev/null | \
			awk '$1 == "HPUX" {print $3}')
		if [[ -z "$_offset" ]]; then

		    sd_msg -w  "The boot configuration file, $1, indicates \
				that $_device is a device with a boot LIF on \
				a \"hard partitioned\" root disk.  There is \
				either no LIF currently on this device or the \
				existing LIF does not have a boot kernel.  \
				Skipping this device (skipping this line in \
				$1)" 1>&2

		    _exitval=2
		    continue
		fi
		if [[ $_offset -ge 8192 ]]; then
	
		    sd_msg -w  "The boot configuration file, $1, indicates \
				that $_device is a device with a boot LIF \
				on a \"hard partitioned\" root disk.  The LIF \
				on this device is for a \"whole disk\" layout \
				root disk.  Skipping this device (skipping \
				this line in $1)." 1>&2

		    _exitval=2
		    continue
		fi
		;;
	esac

	print "$_flag $_device"		# print the information to stdout for 
					# calling routine
    done 

    return $_exitval		# If you got this far, you're golden 
}	# End of ReadConfig

#
# ReadLvol (logical volume device file)
# Prints nothing and returns 1 if lvlnboot is not found and runnable
# Prints nothing and returns 1 if lvlnboot `dirname DeviceFile` gives
# 	no answer.
# Prints physical device(s) to stdout and returns 0 otherwise.
# (Errors to stderr and return of 1 if called incorrectly.)
#
ReadLvol()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    if [[ $# -ne 1 ]]; then
	sd_msg -e "Function $0 called incorrectly: $@" 1>&2
	sd_msg -b "Proper calling sequence is: $0 DeviceFile" 1>&2
	return 1
    fi

    if [[ -x /sbin/lvlnboot ]]; then	# 10.X LVM location
	typeset _lboot="/sbin/lvlnboot"
    elif [[ -x /etc/lvlnboot ]]; then	# 9.X LVM location
	typeset _lboot="/etc/lvlnboot"
    else				# No LVM commands
	return 1
    fi

    # lvlnboot works on volume groups, like vgdisplay, so strip off the
    # filename and work on the directory (would be /dev/vg00 in the default
    # LVM case).
    typeset _device=`$_lboot -v ${1%/*} 2>/dev/null | \
	awk '/Boot Disk/ {print $1}'`
    if [[ -n "$_device" ]]; then	# Found bootlif(s)
	print "$_device"
	return 0
    else
	return 1
    fi
}	# End of ReadLvol

#
# ReadWholeDisk (device file)
# Prints parameter to stdout and returns 0 if lifls -l HPUX DeviceFile
#       shows a boot kernel with offset of > 2MB (start > 8192 blocks)
# Returns 1, otherwise
# Error messages to stderr and return of 1 if called incorrectly.
#
ReadWholeDisk()
{
    [[ -n "$KERN_DEBUG" ]] && typeset PS4='    + bootlif_funcs:$LINENO -> ' \
	&& set -x

    if [[ $# -ne 1 ]]; then
	sd_msg -e "Function $0 called incorrectly: $@" 1>&2
	sd_msg -b "Proper calling sequence is: $0 DeviceFile" 1>&2
	return 1
    fi

    # get the start location of HPUX in the lif (0, if no lif or not there)

    typeset -i _offset=`lifls -l $1 2>/dev/null | awk '$1 == "HPUX" {print $3}'`

    if [[ $_offset -ge 8192 ]]; then
	print $1
	return 0
    else
	return 1
    fi
}

