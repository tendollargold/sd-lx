#
# Copyright (c) 1989, 1993
# 	The Regents of the University of California.  All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
# This product includes software developed by the University of
# California, Berkeley and its contributors.
# 4. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# * From: OpenBSD: vis.c,v 1.2 1996/08/19 08:27:36 tholo Exp
#
import sys
import string

class VisConst:
    VIS_UNSET = 0
    VIS_OCTAL = 0x01
    VIS_CSTYLE = 0x02
    VIS_NONE = 0x80

    VIS_SP = 0x04
    VIS_TAB = 0x08
    VIS_NL = 0x10
    VIS_WHITE = VIS_SP | VIS_TAB | VIS_NL
    VIS_SAFE = 0x20

    VIS_NOSLASH = 0x40

    UNVIS_VALID = 1
    UNVIS_VALIDPUSH = 2
    UNVIS_NOCHAR = 3
    UNVIS_SYNBAD = -1
    UNVIS_ERROR = -2

    UNVIS_END = 1

def isoctal(string):
    try:
        value = int(string, 8)
        return True
    except ValueError:
        return False
def vis(dst, c, flag, nextc):
    if flag & VisConst.VIS_NONE:
        dst += chr(c)
        return dst
    if ((c <= sys.maxunicode and c in string.printable) or
       ((flag & VisConst.VIS_SP) == 0 and c == ord(' ')) or
       ((flag & VisConst.VIS_TAB) == 0 and c == ord('\t')) or
       ((flag & VisConst.VIS_NL) == 0 and c == ord('\n')) or
       ((flag & VisConst.VIS_SAFE) and (c == ord('\b') or c == ord('\007') or c == ord('\r')))):
        dst += chr(c)
        if c == ord('\\') and (flag & VisConst.VIS_NOSLASH) == 0:
            dst += '\\'
        return dst
    if flag & VisConst.VIS_CSTYLE:
        if c == ord('\n'):
            dst += '\\n'
            return dst
        elif c == ord('\r'):
            dst += '\\r'
            return dst
        elif c == ord('\b'):
            dst += '\\b'
            return dst
        elif c == ord('\007'):
            dst += '\\a'
            return dst
        elif c == ord('\v'):
            dst += '\\v'
            return dst
        elif c == ord('\t'):
            dst += '\\t'
            return dst
        elif c == ord('\f'):
            dst += '\\f'
            return dst
        elif c == ord(' '):
            dst += '\\s'
            return dst
        elif c == ord('\0'):
            dst += '\\0'
            if isoctal(nextc):
                dst += '0'
                dst += '0'
            return dst
    if ((c & 0o0177) == ord(' ')) or (flag & VisConst.VIS_OCTAL):
        dst += '\\'
        dst += chr((c >> 6 & 0o07) + ord('0'))
        dst += chr((c >> 3 & 0o07) + ord('0'))
        dst += chr((c & 0o07) + ord('0'))
        return dst
    if (flag & VisConst.VIS_NOSLASH) == 0:
        dst += '\\'
    if c & 0o0200:
        c &= 0o0177
        dst += 'M'
    if c < 32 or c == 0o0177:
        dst += '^'
        if c == 0o0177:
            dst += '?'
        else:
            dst += chr(c + ord('@'))
    else:
        dst += '-'
        dst += chr(c)
    return dst

def strvis(dst, src, flag):
    start = dst
    for i in range(len(src)):
        c = ord(src[i])
        dst = vis(dst, c, flag, ord(src[i+1]))
    return dst - start

def strvisx(dst, src, length, flag):
    start = dst
    for i in range(length):
        c = ord(src[i])
        dst = vis(dst, c, flag, ord(src[i+1]))
    if length:
        dst = vis(dst, ord(src[length]), flag, '\0')
    return dst - start


