# atomicio.py
# $OpenBSD: atomicio.c,v 1.11 2012/12/04 02:24:47 deraadt Exp $
#
#  Copyright (c) 2023 Paul Weber, converted to python
#  Copyright (c) 2006 Damien Miller. All rights reserved.
#  Copyright (c) 2005 Anil Madhavapeddy. All rights reserved.
#  Copyright (c) 1995,1999 Theo de Raadt.  All rights reserved.
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#  1. Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  2. Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
#  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
#  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
#  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
#  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import errno
import os
import sys
import time

import fcntl

from sd.swsuplib.misc.swutilname import swlib_utilname_get


def timed_atomic_read5(fd, buf, n):
    return timed_atomicio(os.read, fd, buf, n, 5)


def timed_atomicio(f, fd, buf, n, timeout):
    pipe_buf_size = 512
    flags = fcntl.fcntl(fd, fcntl.F_GETFL, os.O_NONBLOCK)
    ret = fcntl.fcntl(fd, fcntl.F_SETFL, os.O_NONBLOCK)
    if flags < 0 or ret < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: non-blocking I/O request ignored, fcntl() status={flags}: {errno}\n")
        return atomicio(f, fd, buf, n)
    tm0 = time.time()
    tm = tm0
    s = buf
    pos = 0
    while int(tm - tm0) < timeout and n > pos:
        tm = time.time()
        rq = n - pos
        if rq > pipe_buf_size:
            rq = pipe_buf_size
        res = safeio(f, fd, s + pos, rq)
        if res == -1:
            if errno.EAGAIN:
                continue
            return -1
        elif res == 0:
            return 0
        else:
            if res < 0:
                return -1
            pos += res
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)
    if int(tm - tm0) >= timeout:
        sys.stderr.write(
            f"{swlib_utilname_get()}: {timeout} second I/O time limit exceeded: {pos} bytes transferred, {n} expected\n")
    return pos


def safeio(f, fd, _s, n):
    s = _s
    pos = 0
    while pos == 0:
        res = f(fd, s, n)
        if res == -1:
            if errno.EINTR:
                continue
        elif res == 0:
            return res
        else:
            pos += res
    return pos


def atomicio(f, fd, _s, n):
    pipe_buf_size = 512
    s = _s
    pos = 0
    while n > pos:
        rq = n - pos
        if rq > pipe_buf_size:
            rq = pipe_buf_size
        res = safeio(f, fd, s + pos, rq)
        if res == -1:
            return -1
        elif res == 0:
            return 0
        else:
            if res < 0:
                return -1
            pos += res
    return pos
