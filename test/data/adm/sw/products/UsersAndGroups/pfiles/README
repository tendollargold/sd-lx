Copyright Notice
================

(c)Copyright 1983-2004 Hewlett-Packard Development Company, L.P.
(c)Copyright 1979, 1980, 1983, 1985-1993 The Regents of the Univ. of California
(c)Copyright 1980, 1984, 1986 Novell, Inc.
(c)Copyright 1986-2000 Sun Microsystems, Inc.
(c)Copyright 1985, 1986, 1988 Massachusetts Institute of Technology
(c)Copyright 1989-1993  The Open Software Foundation, Inc.
(c)Copyright 1990 Motorola, Inc.
(c)Copyright 1990, 1991, 1992 Cornell University
(c)Copyright 1989-1991 The University of Maryland
(c)Copyright 1988 Carnegie Mellon University
(c)Copyright 1991-2003 Mentat Inc.
(c)Copyright 1996 Morning Star Technologies, Inc.
(c)Copyright 1996 Progressive Systems, Inc.

Proprietary computer software. Valid license from HP required for
possession, use or copying.Consistent with FAR 12.211 and 12.212,
Commercial Computer Software, Computer Software Documentation, and
Technical Data for Commercial Items are licensed to the U.S.
Government under vendor's standard commercial license.

Legal Notices
=============

The information contained herein is subject to change without notice.
The only warranties for HP products and services are set forth in the express
warranty statements accompanying such products and services. Nothing herein
should be construed as constituting an additional warranty. HP shall not be
liable for technical or editorial errors or omissions contained herein.

Printed in the US.

Confidential computer software. Valid license from HP required for possession,
use or copying. Consistent with FAR 12.211 and 12.212, Commercial Computer 
Software, Computer Software Documentation, and Technical Data for Commercial
Items are licensed to the U.S. Government under vendor's standard commercial
license.

Trademark Notices
=================
X Window System is a trademark of the Massachusetts Institute of Technology.
MS-DOS and Microsoft are U.S. registered trademarks of Microsoft Corporation.
OSF/Motif is a trademark of the Open Software Foundation, Inc. in the U.S. and
other countries.

Announcement
============

HP-UX 11i v2 contains a new user-friendly Security Attributes tool that enables
you to set systemwide and per-user values for security attributes of local and
Network Information Service (NIS) users.

Whats In This Version
=====================

The Security Attributes tool has the following new features:

* A Web-based GUI and TUI
* Configure systemwide values of security attributes.
* Configure per-user values of security attributes of local users.
* Configure per-user values of security attributes of NIS users.

Known Problems and Workarounds
==============================
Not Applicable.

Compatibility Information and Installation Requirements
=======================================================

This version of the product is compatible and runs on the HP-UX 11i v2
operating system only.

The TrustedMigration product must be installed on an HP-UX system for
the using the Security Attributes tool.

The product contains the following filesets:

* TERM      Security Attributes Terminal Interface
* DOC-EN    Security Attributes English Help and Manpages
* WEB       Security Attributes Web User Interface
* CMDS      Security Attributes Command Lines
* COM       Security Attributes Library

Patches and Fixes in This Version
=================================
Not Applicable.

Software Availability in Native Languages
=========================================
This version of the product does not provide native language support.

List of Documents available with the Product
============================================
Refer Security Attributes online help, secweb(1M) manpage for more
information on the Security Attributes tool. 
