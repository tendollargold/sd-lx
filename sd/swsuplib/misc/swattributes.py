# swattributes.py -Attribute mappings to other format's attributes
#
# Copyright (C) 2005  James H. Lowe, Jr.
# Copyright (C) 2024 Paul Weber, Conversion to Python
# All Rights Reserved.
#
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# from sd.swsuplib.rpm.rpmtag import rtt

SWMAP_MAX_LENGTH = 163
RPMPSF_DEF_DFILES = "rpm_dfiles"
RPMPSF_FILESET_SOURCE = "sources"
RPMPSF_FILESET_BUILD = "build_control"
RPMPSF_FILESET_PATCHES = "patches"
RPMPSF_BIN_FILESET_TAG = "binpackage"
SWATMAP_NAME_RPMTAG = 0  # rpmtag name RPMTAG
SWATMAP_NAME_SWLIST_ARGS = 1  # args to swlist
SWATMAP_NAME_DEB = 2  # Debian attribute name
SWATMAP_NAME_SWBIS = 3  # swbis attribute name
SWATMAP_NOT_LSB = 0
SWATMAP_IS_LSB = 1
SWATMAP_ATT_Header = 0  # Use the package attributes
SWATMAP_ATT_SIGNATURE = 1  # Use the signature attributes
SWATTMAP_MAP_NA = "NA"  # Mapping Not applicable
SWATTMAP_MAP_NONE = "NONE"  # Mapping undefined


# Structs are translated to Python classes
class SWATTMAP:
    def __init__(self):
        self.map_table_ = None


class SwAttributesMap:
    def __init__(self, rpmtag_number, rpmtag_type, is_lsb, lsb_status, count, names):
        self.rpmtag_number = rpmtag_number
        self.rpmtag_type = rpmtag_type
        self.is_lsb = is_lsb
        self.lsb_status = lsb_status
        self.count = count
        self.names = names[:4]  # Assuming names is a list with at least 4 elements


# Data instances
swdef_sig_maptable = [
    SwAttributesMap(0, 0, -1, -1, 0, [None, "", "", ""])
]

swdef_pkg_maptable = [
    SwAttributesMap(
        1000, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_NAME", "-d -a distributions", "", "product.tag"]
    ),
    SwAttributesMap(
        1001, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_VERSION", "-d -l distribution -a revision", "", "product.revision"]
    ),
    SwAttributesMap(
        1002, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_RELEASE", "-d -l distribution -a release", "", "product.vendor_tag"]
    ),
    SwAttributesMap(
        1003, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_SERIAL", "-d -l product -a number", "", SWATTMAP_MAP_NONE]
    ),
    SwAttributesMap(
        1004, rtt.RPM_I18NSTRING_TYPE, 1, 1, 1,
        ["RPMTAG_SUMMARY", "-d -l distribution -a title", "", "product.title"]
    ),
    SwAttributesMap(
        1005, rtt.RPM_I18NSTRING_TYPE, 1, 1, 1,
        ["RPMTAG_DESCRIPTION", "-d -l distribution -a description", "", "product.description"]
    ),
    SwAttributesMap(
        1006, rtt.RPM_INT32_TYPE, 1, 0, 1,
        ["RPMTAG_BUILDTIME", "-d -l distribution -a create_time", "", "distribution.create_time"]
    ),
    SwAttributesMap(
        1007, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_BUILDHOST", "-d -l product -a build_host", "", "product.build_host"]
    ),
    SwAttributesMap(
        1008, rtt.RPM_INT32_TYPE, 0, -1, 1,
        ["RPMTAG_INSTALLTIME", "-d -l product -a mod_time", "", "product.mod_time"]
    ),
    SwAttributesMap(
        1009, rtt.RPM_INT32_TYPE, 1, 1, 1,
        ["RPMTAG_SIZE", "-d -l fileset -a size", "", "fileset.size"]
    ),
    SwAttributesMap(
        1010, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_DISTRIBUTION", "-d -l distribution -a dfiles", "", "distribution.dfiles"]
    ),
    SwAttributesMap(
        1011, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_VENDOR", "-d -l vendor -a title *,vq=seller", "", "vendor.tag"]
    ),
    SwAttributesMap(
        1012, rtt.RPM_BIN_TYPE, 0, -1, 1,
        ["RPMTAG_GIF", "", "", ""]
    ),
    SwAttributesMap(
        1013, rtt.RPM_BIN_TYPE, 0, -1, 1,
        ["RPMTAG_XPM", "", "", ""]
    ),
    SwAttributesMap(
        1014, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_LICENSE", "-d -l distribution -a license", "", ""]
    ),
    SwAttributesMap(
        1015, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_PACKAGER", "-d -l vendor -a title *,vq=packager", "", ""]
    ),
    SwAttributesMap(
        1016, rtt.RPM_I18NSTRING_TYPE, 1, 1, 1,
        ["RPMTAG_GROUP", "-d -l category -a title", "", ""]
    ),
    SwAttributesMap(
        1017, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1018, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_SOURCE", "-d -l product -a source_package", "", ""]
    ),
    SwAttributesMap(
        1019, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PATCH", "-d -l product -a all_patches", "", ""]
    ),
    SwAttributesMap(
        1020, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_URL", "-d -l product -a url", "", ""]
    ),
    SwAttributesMap(
        1021, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_OS", "-d -l host -a os_name", "", ""]
    ),
    SwAttributesMap(
        1022, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_ARCH", "-d -l distribution -a architecture", "", ""]
    ),
    SwAttributesMap(
        1023, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_PREIN", "-d -l control_file -a preinstall *", "", ""]
    ),
    SwAttributesMap(
        1024, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_POSTIN", "-d -l control_file -a postinstall *", "", ""]
    ),
    SwAttributesMap(
        1025, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_PREUN", "-d -l control_file -a unpreinstall *", "", ""]
    ),
    SwAttributesMap(
        1026, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_POSTUN", "-d -l control_file -a unpostinstall *", "", ""]
    ),
    SwAttributesMap(
        1027, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_FILENAMES|RPMTAG_OLDFILENAMES", "-d -l file -l control_file -a path", "", ""]
    ),
    SwAttributesMap(
        1028, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_FILESIZES", "-d -l file -l control_file -a size", "", ""]
    ),
    SwAttributesMap(
        1029, rtt.RPM_INT8_TYPE, -1, -1, 0,
        ["RPMTAG_FILESTATES", "-d -l file -l control_file -a state", "", ""]
    ),
    SwAttributesMap(
        1030, rtt.RPM_INT16_TYPE, 1, 1, 0,
        ["RPMTAG_FILEMODES", "-d -l file -l control_file -a mode", "", ""]
    ),
    SwAttributesMap(
        1031, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1032, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1033, rtt.RPM_INT16_TYPE, 1, 1, 0,
        ["RPMTAG_FILERDEVS", "-d -l file -l control_file -x one_liner='major minor'", "", ""]
    ),
    SwAttributesMap(
        1034, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_FILEMTIMES", "-d -l file -l control_file -a mtime", "", ""]
    ),
    SwAttributesMap(
        1035, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_FILEMD5S", "-d -l file -l control_file -a md5sum", "", ""]
    ),
    SwAttributesMap(
        1036, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_FILELINKTOS", "-d -l file -l control_file -a link_source", "", ""]
    ),
    SwAttributesMap(
        1037, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_FILEFLAGS", "-d -l file -l control_file -a rpm_fileflags", "", ""]
    ),
    SwAttributesMap(
        1038, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_ROOT", "", "", ""]
    ),
    SwAttributesMap(
        1039, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_FILEUSERNAME", "-d -l file -l control_file -a owner", "", ""]
    ),
    SwAttributesMap(
        1040, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_FILEGROUPNAME", "-d -l file -l control_file -a group", "", ""]
    ),
    SwAttributesMap(
        1041, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_EXCLUDE", "", "", ""]
    ),
    SwAttributesMap(
        1042, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_EXCLUSIVE", "", "", ""]
    ),
    SwAttributesMap(
        1043, rtt.RPM_BIN_TYPE, -1, -1, 1,
        ["RPMTAG_ICON", "", "", ""]
    ),
    SwAttributesMap(
        1044, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_SOURCERPM", "-d -l product -a sourcerpm", "", ""]
    ),
    SwAttributesMap(
        1045, rtt.RPM_INT32_TYPE, 1, 0, 1,
        ["RPMTAG_FILEVERIFYFLAGS", "-d -l file -l control_file -a rpm_verifyflags", "", ""]
    ),
    SwAttributesMap(
        1046, rtt.RPM_INT32_TYPE, 1, 0, 1,
        ["RPMTAG_ARCHIVESIZE", "", "", ""]
    ),
    SwAttributesMap(
        1047, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_PROVIDENAME", "-d -l fileset -a rpm_provides *.", RPMPSF_BIN_FILESET_TAG, "", ""]
    ),
    SwAttributesMap(
        1048, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_REQUIREFLAGS", "-d -l fileset -a prerequisites *.", RPMPSF_BIN_FILESET_TAG, "", ""]
    ),
    SwAttributesMap(
        1049, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_REQUIRENAME", "-d -l fileset -a prerequisites *.", RPMPSF_BIN_FILESET_TAG, "", ""]
    ),
    SwAttributesMap(
        1050, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_REQUIREVERSION", "-d -l fileset -a prerequisites *.", RPMPSF_BIN_FILESET_TAG, "", ""]
    ),
    SwAttributesMap(
        1051, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1052, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1053, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_CONFLICTFLAGS", "-d -l fileset -a exrequisites *.", RPMPSF_BIN_FILESET_TAG, "", ""]
    ),
    SwAttributesMap(
        1054, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_CONFLICTNAME", "-d -l fileset -a exrequisites *.", RPMPSF_BIN_FILESET_TAG, "", ""]
    ),
    SwAttributesMap(
        1055, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_CONFLICTVERSION", "-d -l fileset -a exrequisites *.", RPMPSF_BIN_FILESET_TAG, "", ""]
    ),
    SwAttributesMap(
        1056, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_DEFAULTPREFIX", "-d -l distribution -a rpm_default_prefix", "", ""]
    ),
    SwAttributesMap(
        1057, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_BUILDROOT", "-d -l product -a build_root", "", ""]
    ),
    SwAttributesMap(
        1058, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_INSTALLPREFIX", "-d -l product -a directory", "", ""]
    ),
    SwAttributesMap(
        1059, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_EXCLUDEARCH", "-d -l fileset -a excluded_arch *.", RPMPSF_FILESET_BUILD, "", ""]
    ),
    SwAttributesMap(
        1060, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_EXCLUDEOS", "-d -l fileset -a exclude_os *.", RPMPSF_FILESET_BUILD, "", ""]
    ),
    SwAttributesMap(
        1061, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_EXCLUSIVEARCH", "-d -l fileset -a exclusive_arch *.", RPMPSF_FILESET_BUILD, "", ""]
    ),
    SwAttributesMap(
        1062, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_EXCLUSIVEOS", "-d -l fileset -a exclusive_os *.", RPMPSF_FILESET_BUILD, "", ""]
    ),
    SwAttributesMap(
        1063, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1064, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_RPMVERSION", "-d -l distribution -a rpmversion", "", ""]
    ),
    SwAttributesMap(
        1065, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1066, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1067, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1068, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1069, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1070, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1071, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1072, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1073, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1074, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1075, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1076, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1077, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1078, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1079, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_VERIFYSCRIPT", "-d -l control_file -a verify *.", RPMPSF_FILESET_BUILD, "", ""]
    ),
    SwAttributesMap(
        1080, rtt.RPM_INT32_TYPE, 1, 0, 1,
        ["RPMTAG_CHANGELOGTIME", "-d -l fileset -a change_log *.", RPMPSF_FILESET_BUILD, "*.", RPMPSF_BIN_FILESET_TAG,
         "", ""]
    ),
    SwAttributesMap(
        1081, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_CHANGELOGNAME", "-d -l fileset -a change_log *.", RPMPSF_FILESET_BUILD, "*.", RPMPSF_BIN_FILESET_TAG,
         "", ""]
    ),
    SwAttributesMap(
        1082, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_CHANGELOGTEXT", "-d -l fileset -a change_log *.", RPMPSF_FILESET_BUILD, "*.", RPMPSF_BIN_FILESET_TAG,
         "", ""]
    ),
    SwAttributesMap(
        1083, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1084, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["NOT_USED", "", "", ""]
    ),
    SwAttributesMap(
        1085, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_PREINPROG", "-d -l control_file -a interpreter preinstall", "", ""]
    ),
    SwAttributesMap(
        1086, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_POSTINPROG", "-d -l control_file -a interpreter postinstall", "", ""]
    ),
    SwAttributesMap(
        1087, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_PREUNPROG", "-d -l control_file -a interpreter unpreinstall", "", ""]
    ),
    SwAttributesMap(
        1088, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_POSTUNPROG", "-d -l control_file -a interpreter unpostinstall", "", ""]
    ),
    SwAttributesMap(
        1089, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_BUILDARCHS", "-d -l distribution -a rpm_buildarchs", "", ""]
    ),
    SwAttributesMap(
        1090, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_OBSOLETENAME", "-d -l distribution -a rpm_obsoletes", "", ""]
    ),
    SwAttributesMap(
        1091, rtt.RPM_STRING_TYPE, -1, -1, 1,
        ["RPMTAG_VERIFYSCRIPTPROG", "-d -l control_file -a interpreter verify", "", ""]
    ),
    SwAttributesMap(
        1092, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_TRIGGERSCRIPTPROGS", "", "", ""]
    ),
    SwAttributesMap(
        1093, rtt.RPM_INT16_TYPE, -1, -1, 1,
        ["RPMTAG_DOCDIR", "", "", ""]
    ),
    SwAttributesMap(
        1094, rtt.RPM_INT32_TYPE, 1, 0, 1,
        ["RPMTAG_COOKIE", "", "", ""]
    ),
    SwAttributesMap(
        1095, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_FILEDEVICES", "-d -l file -l control_file -x one_liner='major minor'", "", ""]
    ),
    SwAttributesMap(
        1096, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_FILEINODES", "", "", ""]
    ),
    SwAttributesMap(
        1097, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_FILELANGS", "", "", ""]
    ),
    SwAttributesMap(
        1098, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_PREFIXES", "", "", ""]
    ),
    SwAttributesMap(
        1099, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_TRIGGERRUN", "", "", ""]
    ),
    SwAttributesMap(
        1100, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_TRIGGERRIN", "", "", ""]
    ),
    SwAttributesMap(
        1101, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_TRIGGERPOSTUN", "", "", ""]
    ),
    SwAttributesMap(
        1102, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_TRIGGERPOSTUNl", "", "", ""]
    ),
    SwAttributesMap(
        1103, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_AUTOREQ", "", "", ""]
    ),
    SwAttributesMap(
        1104, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_AUTOPROV", "", "", ""]
    ),
    SwAttributesMap(
        1105, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_CAPABILITY", "", "", ""]
    ),
    SwAttributesMap(
        1106, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_SOURCEPACKAGE", "", "", ""]
    ),
    SwAttributesMap(
        1107, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_OLDORIGFILENAMES", "", "", ""]
    ),
    SwAttributesMap(
        1108, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_BUILDPREREQ", "", "", ""]
    ),
    SwAttributesMap(
        1109, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_BUILDREQUIRES", "", "", ""]
    ),
    SwAttributesMap(
        1110, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_BUILDCONFLICTS", "", "", ""]
    ),
    SwAttributesMap(
        1111, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_BUILDMACROS", "", "", ""]
    ),
    SwAttributesMap(
        1112, rtt.RPM_INT32_TYPE, 1, 1, 0,
        ["RPMTAG_PROVIDEFLAGS", "", "", ""]
    ),
    SwAttributesMap(
        1113, rtt.RPM_STRING_ARRAY_TYPE, 1, 1, 0,
        ["RPMTAG_PROVIDEVERSION", "", "", ""]
    ),
    SwAttributesMap(
        1114, rtt.RPM_INT32_TYPE, 1, 0, 1,
        ["RPMTAG_OBSOLETEFLAGS", "", "", ""]
    ),
    SwAttributesMap(
        1115, rtt.RPM_STRING_TYPE, 1, 0, 0,
        ["RPMTAG_OBSOLETEVERSION", "", "", ""]
    ),
    SwAttributesMap(
        1116, rtt.RPM_INT32_TYPE, 1, 0, 0,
        ["RPMTAG_DIRINDEXES", "", "", ""]
    ),
    SwAttributesMap(
        1117, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_BASENAMES", "", "", ""]
    ),
    SwAttributesMap(
        1118, rtt.RPM_STRING_ARRAY_TYPE, 1, 0, 0,
        ["RPMTAG_DIRNAMES", "", "", ""]
    ),
    SwAttributesMap(
        1119, rtt.RPM_STRING_ARRAY_TYPE, -1, -1, 0,
        ["RPMTAG_ORIGDIRINDEXES", "", "", ""]
    ),
    SwAttributesMap(
        1120, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_ORIGBASENAMES", "", "", ""]
    ),
    SwAttributesMap(
        1121, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_ORIGDIRNAMES", "", "", ""]
    ),
    SwAttributesMap(
        1122, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_OPTFLAGS", "", "", ""]
    ),
    SwAttributesMap(
        1123, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_DISTURL", "", "", ""]
    ),
    SwAttributesMap(
        1124, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_PAYLOADFORMAT", "", "", ""]
    ),
    SwAttributesMap(
        1125, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_PAYLOADCOMPRESSOR", "", "", ""]
    ),
    SwAttributesMap(
        1126, rtt.RPM_STRING_TYPE, 1, 1, 1,
        ["RPMTAG_PAYLOADFLAGS", "", "", ""]
    ),
    SwAttributesMap(
        1127, rtt.RPM_STRING_TYPE, -1, -1, 0,
        ["RPMTAG_MULTILIBS", "", "", ""]
    ),
    SwAttributesMap(
        1128, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_INSTALLTID", "", "", ""]
    ),
    SwAttributesMap(
        1129, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_REMOVETID", "", "", ""]
    ),
    SwAttributesMap(
        1130, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_SHA1RHN", "", "", ""]
    ),
    SwAttributesMap(
        1131, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_RHNPLATFORM", "", "", ""]
    ),
    SwAttributesMap(
        1132, rtt.RPM_STRING_TYPE, 1, 0, 1,
        ["RPMTAG_PLATFORM", "", "", ""]
    ),
    SwAttributesMap(
        1133, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PATCHESNAME", "", "", ""]
    ),
    SwAttributesMap(
        1134, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PATCHESFLAGS", "", "", ""]
    ),
    SwAttributesMap(
        1135, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PATCHESVERSION", "", "", ""]
    ),
    SwAttributesMap(
        1136, rtt.RPM_INT32_TYPE, 0, -1, 1,
        ["RPMTAG_CACHECTIME", "", "", ""]
    ),
    SwAttributesMap(
        1137, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_CACHEPKGPATH", "", "", ""]
    ),
    SwAttributesMap(
        1138, rtt.RPM_INT32_TYPE, 0, -1, 1,
        ["RPMTAG_CACHEPKGSIZE", "", "", ""]
    ),
    SwAttributesMap(
        1139, rtt.RPM_STRING_TYPE, 0, -1, 1,
        ["RPMTAG_CACHEPKGMTIME", "", "", ""]
    ),
    SwAttributesMap(
        1140, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_FILECOLORS", "", "", ""]
    ),
    SwAttributesMap(
        1141, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_FILECLASS", "", "", ""]
    ),
    SwAttributesMap(
        1142, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_FILECLASS", "", "", ""]
    ),
    SwAttributesMap(
        1143, rtt.RPM_INT32_TYPE, 0, -1, 1,
        ["RPMTAG_FILEDEPENDSX", "", "", ""]
    ),
    SwAttributesMap(
        1144, rtt.RPM_INT32_TYPE, 0, -1, 1,
        ["RPMTAG_FILEDEPENDSN", "", "", ""]
    ),
    SwAttributesMap(
        1145, rtt.RPM_INT32_TYPE, 0, -1, 1,
        ["RPMTAG_DEPENDSDICT", "", "", ""]
    ),
    SwAttributesMap(
        1146, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 1,
        ["RPMTAG_SOURCEPKGID", "", "", ""]
    ),
    SwAttributesMap(
        1147, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_FILECONTEXTS", "", "", ""]
    ),
    SwAttributesMap(
        1148, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_FSCONTEXT", "", "", ""]
    ),
    SwAttributesMap(
        1149, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_RECONTEXTS", "", "", ""]
    ),
    SwAttributesMap(
        1150, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_POLICIES", "", "", ""]
    ),
    SwAttributesMap(
        1151, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PRETRANS", "", "", ""]
    ),
    SwAttributesMap(
        1152, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_POSTTRANS", "", "", ""]
    ),
    SwAttributesMap(
        1153, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PRETRANSPROG", "", "", ""]
    ),
    SwAttributesMap(
        1154, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_POSTTRANSPROG", "", "", ""]
    ),
    SwAttributesMap(
        1155, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_DISTTAG", "", "", ""]
    ),
    SwAttributesMap(
        1156, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_SUGGESTSNAME", "", "", ""]
    ),
    SwAttributesMap(
        1157, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_SUGGESTSVERSION", "", "", ""]
    ),
    SwAttributesMap(
        1158, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_SUGGESTSFLAGS", "", "", ""]
    ),
    SwAttributesMap(
        1159, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_ENHANCESNAME", "", "", ""]
    ),
    SwAttributesMap(
        1160, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_ENHANCESVERSION", "", "", ""]
    ),
    SwAttributesMap(
        1161, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_ENHANCESFLAGS", "", "", ""]
    ),
    SwAttributesMap(
        1162, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PRIORITY", "", "", ""]
    ),
    SwAttributesMap(
        1163, rtt.RPM_STRING_ARRAY_TYPE, 0, -1, 0,
        ["RPMTAG_PRIORITY", None, "", ""]
    ),
    SwAttributesMap(
        0, 0, -1, -1, 0,
        [None, None, None, None]
    )
]


def get_table(table_id):
    if table_id == SWATMAP_ATT_Header:
        return swdef_pkg_maptable
    else:
        return swdef_sig_maptable


def find_entry_by_rpmtag(table_id, rpmtag):
    table = get_table(table_id)
    for entry in table:
        if entry.rpmtag_number == rpmtag:
            return entry
        entry += 1
    return None


def swatt_get_rpmtype(table_id, rpmtag):
    entry = find_entry_by_rpmtag(table_id, rpmtag)
    if entry:
        return entry.count
    else:
        return -1


def swatt_get_rpmcount(table_id, rpmtag):
    entry = find_entry_by_rpmtag(table_id, rpmtag)
    if entry:
        return entry.rpmtag_type
    else:
        return -1
