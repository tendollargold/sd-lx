#! /sbin/sh
########
#  Product: hpuxwsAPACHE
#  Fileset: APACHE2
#  SD script: configure
########
#
#  "(c)
#
########
#

SUCCESS=0; FAILURE=1; WARNING=2; EXCLUDE=3
APACHE2=$SW_LOCATION
DT="`date +\"%Y%m%d%H%M%S\"`"

if [ -n "$SW_DEFERRED_KERNBLD" ]; then
    echo "WARNING: HP Apache-based web server  will not function from"
    echo "         an NFS Diskless client, although swlist (1M) will"
    echo "         report the product as installed."
    exit $WARNING
fi

UTILS="/usr/lbin/sw/control_utils"
if [[ ! -f $UTILS ]]; then
    echo "ERROR:   Cannot find $UTILS"
    exit $FAILURE
fi
. $UTILS

function bail
{
    msg error "$*"
    exit $FAILURE
}

function get_host
# Prints a string of the form <host name> <IP address>
# The host name and IP address are retrieved from the
# output of nslookup, or default to the local host name
# and loopback address.
{
    typeset host=$(uname -n) domain lookup name addr local="127.0.0.1"
    if [[ -f /etc/resolv.conf ]]; then
        domain=$(grep domain /etc/resolv.conf |  grep -v "#" | awk '{print $2}')
    fi

    if [[ -z $domain ]]; then
        print "$host $local"
        return 0
    fi

    lookup=$(nslookup $host 2> /dev/null)
    if [[ $? -ne 0 ]]; then 
	print "$host $local"
        return 0
    fi

    lookup=$(print "$lookup" | awk 'BEGIN {name= ""; addr= ""}
			   /Name:/ {name= $2}
			   /Address:/ {addr= $2}
			   END { print "name=" name; print "addr=" addr}')
    eval $lookup

    if [[ ! -z $domain ]]; then
        if [[ "x`echo $name | grep $domain`" = "x" ]]; then
            name="$name.$domain"
        fi
        if [[ "x`echo $host | grep $domain`" = "x" ]]; then
            host="$host.$domain"
        fi
    fi

    if [[ -z $name ]]; then       # shouldn't happen
	print "$host $local"
        return 0
    fi

    [[ -z $addr ]] && addr=$local # shouldn't happen

    print "$name $addr"
    return 0
}

function hostconf
{
    rv=$SUCCESS

    cp ${APACHE2}newconfig${APACHE2}conf/httpd.conf  ${APACHE2}newconfig${APACHE2}conf/httpd.conf.$DT
    sed -e "s/^ServerAdmin .*/ServerAdmin www@$1/" \
        -e "s/^ServerName .*:/ServerName $1:/" \
        ${APACHE2}newconfig${APACHE2}conf/httpd.conf.$DT > ${APACHE2}newconfig${APACHE2}conf/httpd.conf
    [[ $? -ne 0 ]] && bail sed of ${APACHE2}newconfig${APACHE2}conf/httpd.conf fails
    rm -f ${APACHE2}newconfig${APACHE2}conf/httpd.conf.$DT
    
    cp ${APACHE2}newconfig${APACHE2}conf/httpd-std.conf  ${APACHE2}newconfig${APACHE2}conf/httpd-std.conf.$DT
    sed -e "s/^ServerAdmin .*/ServerAdmin www@$1/" \
        -e "s/^ServerName .*:/ServerName $1:/" \
        ${APACHE2}newconfig${APACHE2}conf/httpd-std.conf.$DT > ${APACHE2}newconfig${APACHE2}conf/httpd-std.conf
    [[ $? -ne 0 ]] && bail sed of ${APACHE2}newconfig${APACHE2}conf/httpd-std.conf fails
    rm -f ${APACHE2}newconfig${APACHE2}conf/httpd-std.conf.$DT

    IPD_addfile ${APACHE2}newconfig${APACHE2}conf/httpd.conf
    IPD_addfile ${APACHE2}newconfig${APACHE2}conf/httpd-std.conf
}

function make_symlink
{
    if [ -e $2 ]; then
        rm -f $2
        if [ $? -ne 0 ]; then
            print "NOTE:    Could not remove ${APACHE2}lib/$2"
            return $EXCLUDE
        fi
    fi
    ln -s $1 $2
    if [ $? -eq 0 ]; then
        IPD_addfile ${APACHE2}lib/$2
    fi
    return $SUCCESS
}

# create symlinks to shared libs
cd ${APACHE2}lib

# configure the httpd.conf with the hostname
hostconf $(get_host) 
if [[ $? -ne 0 ]]; then
   print "ERROR:   Couldn't configure the server."
   exit $FAILURE
fi
################################################################################
# NEW CONFIG STUFF
################################################################################

FILES="/opt/hpws/apache/conf/httpd-std.conf \
      /opt/hpws/apache/conf/httpd.conf"

for i in $FILES
do
    newconfig_cp $i
    newconfig_msgs $?
done

################################################################################

###* no longer need to be made in configure *###
###make_symlink libapr-0.sl.9.2     libapr-0.sl
###make_symlink libapr-0.sl.9.2     libapr-0.sl.9
###make_symlink libaprutil-0.sl.9.2 libaprutil-0.sl
###make_symlink libaprutil-0.sl.9.2 libaprutil-0.sl.9
###make_symlink libexpat.sl.1.0     libexpat.sl
###make_symlink libexpat.sl.1.0     libexpat.sl.1

exit $SUCCESS
