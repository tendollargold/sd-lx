#!/usr/bin/python3

# swlist.py - swlist - list information about software that has been 
# installed on a system or is in a distribution.
# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>

#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
# import sd.conf.config
from sd.conf.config import BaseConfig
#from sd.defaults import
from sd.cli.option_parser import clioption_args
#from sd.cli.cli import version_info
# SWLIST OPERANDS
# The swlist utility supports the software_selections and targets operands 
# described in Chapter 3. If no software_selections are provided, all software 
# in the catalog (either distribution or installed software) is selected.

# GNU options (i.e. --help) were added to the parser in conjunction with the 
# POSIX (-h) options

cmd = os.path.split(sys.argv[0])[1].split('.')[0]

swlist_parser = argparse.ArgumentParser(prog='swlist',
	usage="""%(prog)s [-d|-r] [-i] [-R] [-v] [-g] [-a attribute] [-c catalog] 
       [-C session_file] [-f software_file] [-l level ] [-s source]
       [-S session_file] [-t target_file] [-x option=value]
       [-X option_file] [software_selections] [-T target_selections]""",
	formatter_class=argparse.RawDescriptionHelpFormatter,
	epilog='''
 Software selections are specified as:
 
     product[.subproduct][.fileset][,version] ...
 
 where version is either
 
     [r=revision][,a=arch][,v=vendor][,l=location]
     instance_id
 
 Target selections are specified as:
 
     -T [host][:][path] ...
 
''')
# -v in swlist does not enable verbosity. It specifies that the format of the output
# is in the INDEX file format, as defined in Section 5.2 on page 130.
swlist_parser.add_argument("-v",action='store_true', 
			  help="List all the attribute value pairs of the objects specified")
swlist_parser.add_argument("-r", "--root",metavar='root', 
			  help="target selections are alternate root directories")
swlist_parser.add_argument("-d", "--depot",metavar='depot', 
			  help="target selections are software depots")
swlist_parser.add_argument("-R",action='store_true', 
			  help="shorthand for -l bundle -l subproduct -l fileset")
swlist_parser.add_argument("-l",metavar='',
	choices=['depot', 'bundle', 'product', 'subproduct', 'fileset','file', 'root', 'shroot', 'prroot', 'category', 'patch', 'control_file'],
    default='product',
	help="levels of objects to view, one of: 'depot' 'bundle', \n'product', 'subproduct', 'fileset','file', 'root', 'shroot',\n'prroot', 'category', 'patch', 'control_file'")
swlist_parser.add_argument("-a", "--attribute",nargs="+",action='append',metavar='attribute',
			  help="list this attribute (multiple -a options can be specified)")
swlist_parser.add_argument("-c --catalog", metavar='catalog',
			  help="Store a copy of a response file or other files created by a request script in catalog")
swlist_parser.add_argument("-s", "--source",metavar='source', 
			  help="read the software selections from this source")
swlist_parser.add_argument("-x", nargs="+",action='append',metavar='option=value',
			  help="-x option=value, set the option to value")
swlist_parser.add_argument("-X", "--option-file",metavar='option_file',
			  help="read option definitions from this file")
swlist_parser.add_argument("-f", "--software-file",metavar='software_file',
			  help="read software selections from this file")
swlist_parser.add_argument("-t", "--target-file",metavar='target_file',
			  help="read target selections from this file")
swlist_parser.add_argument("-C", "--session-file",metavar='session_file',
			  help="save the session to session_file")
swlist_parser.add_argument("-S",metavar='session_file',
			help="read options, software selections, and target \nselections from this file")
# -g --gnu option added for GNU Copyleft
swlist_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")
swlist_parser.add_argument("software_selections", nargs='*', 
 		  help="software selections: XServer.MAN ...")
# B.3.4 page 188 Using -T vs @ for target selection as it is easier in argparse. :)
swlist_parser.add_argument("-T", nargs="+",metavar="[host][:][path]",
			  help="Target selections specified as: [host][:][path] \n default is localhost:/")

args = swlist_parser.parse_args()

BaseConfig()
#print("Call clioption_args with cli arguments")
clioption_args(cmd, args)
swselect=args.software_selections

