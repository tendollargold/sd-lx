

# phases.swconfig.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

"""
  4.0 Execution Phase Systems Management: Distributed Software Administration
   swconfig utility Page 82 - 83

   Extended Options

  The swconfig utility supports the following extended options. The description in 
Chapter 3 applies.
"""

swconfig_options={
    'allow_incompatible': bool(False),
    'allow_multiple_versions': bool(False),
    'ask': bool(False),
    'autoselect_dependencies': bool(True),
    'autoselect_dependents': bool(False),
    'enforce_dependencies': bool(True),
    'installed_software_catalog': "/var/adm/sw/products/",
    'logfile': "/var/adm/sw/swconfig.log",
    'loglevel': "1",
    'reconfigure': bool(False),
    'select_local': bool(True),
    'software': "",
    'targets': "",
    'verbose': "1"

}
def swconfigptions():
    """Collection of extended options for swconfig utility. :api"""
    # implementation_defined_value is set via the setting in swdefaults?
    swconfig_options.update(globals())


# There are three key phases in the swconfig utility:
#   1. Selection phase
#   2. Analysis phase
#   3. Execution phase

#
# Selection Phase
#

# Software selections apply to the software installed on the target. Each specified 
# selection is added to the selection list after it passes the following checks:

#   · If the selection is not found, generate an event.
#     (SW_WARNING: SW_SELECTION_NOT_FOUND)

#   · If the selection is not found at that product location but it does exist in 
#     another location, generate an event.
#     (SW_WARNING: SW_SELECTION_NOT_FOUND_RELATED)

# Add any dependencies to the selection list if autoselect_dependencies=true and the 
# task is configure. Add any dependents to the selection list if the 
# autoselect_dependents=true and the task is unconfigure.

# If ask=true then execute the request scripts for the selected software as described 
# in the swask utility. See swask extended description.

#
# Analysis Phase
#

# The following checks are made:

#    · If configuring, check for compatibility of selections, and if the software is 
#      not compatible, generate an event. If allow_incompatible=true generate an 
#      event. See Section 3.4.1.2 on page 42.
#      (SW_WARNING: SW_NOT_COMPATIBLE)

#    · If allow_incompatible=false generate an event.
#      (SW_ERROR: SW_NOT_COMPATIBLE)

#    · If configuring and the state is corrupt or transient , generate an event.
#      (SW_ERROR: SW_SELECTION_IS_CORRUPT)

#    · If the state is already configured when configuring, or not configured when 
#      unconfiguring, generate an event. This event is independent of whether the 
#      software will be reconfigured or not, which in turn is controlled by the 
#      reconfigure option.
#      (SW_NOTE: SW_ALREADY_CONFIGURED)

#    · Check whether configuring this software will create a new configured version 
#      of a fileset, and generate an event if it will. See the definition for version 
#      in the Glossary. If allow_multiple_versions=true generate an event.
#      (SW_WARNING: SW_NEW_MULTIPLE_VERSION)

#      If allow_multiple_versions=false generate an event.
#      (SW_ERROR: SW_NEW_MULTIPLE_VERSION)

#    · If the dependencies are not in the configured state and have not been 
#      autoselected to be configured, generate an event. If enforce_dependencies=false
#      generate an event.
#      (SW_WARNING: SW_DEPENDENCY_NOT_MET)

#      If enforce_dependencies=true, generate an event.
#      (SW_ERROR: SW_DEPENDENCY_NOT_MET)

#
# Execution Phase
#

# The sequential relationship between the configure operations is shown in the 
# following list. Products are ordered by prerequisite dependencies if any. Fileset 
# operations are also ordered by any prerequisites.
# Configure each product
#   1. Run configure script for the product.
#   2. Configure each fileset in the product.
#      a. Run the configure script for the fileset.
#      b. Update the result of the script (control_file). Update the state of the 
#         fileset to configured in the database for the installed_software object.

# If there is no configure script for a fileset, the state of the fileset is still 
# changed as specified above.

# For unconfigure operations, the order of the products and filesets within the 
# products is the reverse of the order of products and filesets for configure. The 
# operations are:
#  Unconfigure each product:
#    1. Unconfigure each fileset in the product:
#       a. Run the unconfigure script for the fileset.
#       b. Update the result of the script. Update the state of the fileset in the 
#          product to installed in the database for the installed_software object.
#    2. Run the unconfigure script for the product.

# If there is no unconfigure script for a fileset the state of the fileset is still 
# changed as specified above.

# Executing Configure Scripts
# In this operation, swconfig executes vendor-supplied configure or unconfigure 
# scripts.

# The configure scripts are interactive; however, they may access the response file 
# generated by an interactive request script. If any response file has been generated,
# it will exist in the directory pointed to by SW_CONTROL_DIRECTORY.

#    · If a configure script returns an error and enforce_scripts=true, generate an 
#      event and invoke the implementation defined error handling procedures.
#      (SW_ERROR: SW_CONFIGURE_ERROR)

#    · If a configure script returns an error and enforce_scripts=false, generate an 
#      event.
#      (SW_WARNING: SW_CONFIGURE_ERROR)

#    · If the configure script returns a warning, generate an event.
#      (SW_WARNING: SW_CONFIGURE_WARNING)

#    · If a configure script has a return code of 3, generate an event, and exclude 
#      the fileset (or all filesets within the product for a product level script) 
#      from any state change between configured and installed).
#      (SW_NOTE: SW_CONFIGURE_EXCLUDE)

# EXIT STATUS
#  See Chapter 3.
# CONSEQUENCES OF ERRORS
# See Chapter 3.


