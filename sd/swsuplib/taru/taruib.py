# taruib.py - verify archive digests
#
# Copyright (C) 2000  James H. Lowe, Jr.
# Copyright (C) 2023 - 2024 Paul Weber, conversion to Python
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
import fnmatch
import os
import pty
import signal
import stat
import sys
import tarfile
from ctypes import sizeof, c_int

import rpm

from sd.debug import E_DEBUG
from sd.debug import E_DEBUG2, E_DEBUG3
from sd.swsuplib.misc.cstrings import strstr, strcmp
# from sd.swsuplib.misc.vrealpath import  swlib_vrealpath
from sd.swsuplib.misc.swgp import swgp_signal_block
from sd.swsuplib.misc.swlib import (swlibc, swpath_parse_path, fr, swlib_unix_dircat)
from sd.swsuplib.misc.swpath import SWPATH_CTYPE_CAT, SWPATH_CTYPE_DIR, SWPATH_CTYPE_STORE
from sd.swsuplib.misc.swpath import swpath_get_prepath, swpath_get_is_catalog, swpath_open, swpath_close, \
    swpath_get_dfiles, swpath_get_basename
from sd.swsuplib.strob import strob_open, strob_close, strob_str, strob_strcpy
from sd.swsuplib.taru.ahs import ahsstaticsettarfilename
from sd.swsuplib.taru.copyout import taru_write_archive_trailer
from sd.swsuplib.taru.mtar import taru_is_tar_filename_too_long
from sd.swsuplib.taru.otar import taru_otoul
from sd.swsuplib.taru.readinheader import taru_pump_amount2
from sd.swsuplib.taru.tar import tarc
from sd.swsuplib.taru.taru import af, tc, taru_get_recorded_header, taru_create
from sd.swsuplib.taru.xformat import (xformat_get_name, xformat_copy_pass, xformat_get_ifd,
                                      xformat_read_header, xformat_file_has_data, xformat_copy_pass2,
                                      xformat_is_end_of_archive, xformat_get_tar_typeflag, xformat_get_perms,
                                      xformat_get_filesize, xformat_get_linkname, xformat_copy_pass_file_data,
                                      xformat_decrement_bytes_written, xformat_get_tarheader_flags, xformat_vfile_hdr,
                                      xformat_set_ofd)
from sd.swsuplib.uxfio import uxfio_write

global __FILE__, __LINE__, __FUNCTION__, g_taruib_gst_datalen


class TaruIB:
    TARUIB_N_OTHER = -1
    TARUIB_N_MD5 = 0
    TARUIB_N_ADJUNCT_MD5 = 1
    TARUIB_N_SIG = 2
    TARUIB_N_SHA1 = 3
    TARUIB_N_SIG_HDR = 4
    TARUIB_N_SIZE = 5
    TARUIB_N_SHA512 = 6
    TARUIB_N_MAX_INDEX = 6
    TARUIB_TEXIT_512 = 11  # signature is 512 bytes long
    TARUIB_TEXIT_1024 = 13  # signature is 1024 bytes long
    TARUIB_TEXIT_NOT_SIGNED = 10
    TARUIB_TEXIT_ERROR_CONDITION = 120
    TARUIB_TEXIT_ERROR_CONDITION_0 = 120
    TARUIB_TEXIT_ERROR_CONDITION_1 = 121
    TARUIB_TEXIT_ERROR_CONDITION_2 = 122
    TARUIB_TEXIT_ERROR_CONDITION_3 = 123
    TARUIB_TEXIT_ERROR_CONDITION_4 = 124


tib = TaruIB

# global g_taruib_gst_overflow_release, g_taruib_gst_fd, g_taruib_gst_len, g_taruib_gst_buffer
g_taruib_gst_overflow_release = 0
g_taruib_gst_fd = 0
g_taruib_gst_len = 0
g_taruib_gst_buffer = str(['\0' for _ in range(tc.TARU_BUFSIZ_RES)])

md5sum_adjunct_pattern = [
    "catalog/*/adjunct_md5sum",
    "*/catalog/*/adjunct_md5sum",
    None
]

md5sum_full_pattern = [
    "catalog/*/md5sum",
    "*/catalog/*/md5sum",
    None
]

sha1sum_pattern = [
    "catalog/*/sha1sum",
    "*/catalog/*/sha1sum",
    None
]

sha512sum_pattern = [
    "catalog/*/sha512sum",
    "*/catalog/*/sha512sum",
    None
]

signature_pattern = [
    "catalog/*/" + swlibc.SWBN_SIGNATURE,
    "*/catalog/*/" + swlibc.SWBN_SIGNATURE,
    None
]

signature_header_pattern = [
    "catalog/*/" + swlibc.SWBN_SIG_HEADER,
    "*/catalog/*/" + swlibc.SWBN_SIG_HEADER,
    None
]


def determine_pattern_array(detpat, name):
    E_DEBUG("ENTERING")
    if "/md5sum" in name:
        detpat[0] = tib.TARUIB_N_MD5
        return md5sum_full_pattern
    elif "/adjunct_md5sum" in name:
        detpat[0] = tib.TARUIB_N_ADJUNCT_MD5
        return md5sum_adjunct_pattern
    elif "/sha1sum" in name:
        detpat[0] = tib.TARUIB_N_SHA1
        return sha1sum_pattern
    elif "/sha512sum" in name:
        detpat[0] = tib.TARUIB_N_SHA512
        return sha512sum_pattern
    elif "/SWBN_SIGNATURE" in name and len(name.split("/SWBN_SIGNATURE")[-1]) == len("/SWBN_SIGNATURE"):
        detpat[0] = tib.TARUIB_N_SIG
        return signature_pattern
    elif "/SWBN_SIG_HEADER" in name and len(name.split("/SWBN_SIG_HEADER")[-1]) == len("/SWBN_SIG_HEADER"):
        detpat[0] = tib.TARUIB_N_SIG_HDR
        return signature_header_pattern
    else:
        detpat[0] = tib.TARUIB_N_OTHER
        return str(None)


def check_useArray(useArray, digest_type, dfd):
    E_DEBUG("ENTERING")
    if useArray:
        if digest_type > 4:
            return -1
        if useArray[digest_type] != 0 and digest_type != tib.TARUIB_N_SIG:
            #			
            # More than one sig or digest file in the catalog.
            # This is sanity security check.
            #			
            ret = -1
            print("swbis: catalog file error : type = {}".format(digest_type))
        else:
            useArray[digest_type] += 1
            ret = dfd
    else:
        ret = dfd
    return ret


def verbose_decode(useArray, detpat, prepath, name, nullfd, digest_type, verbose, md5digestfd, sha1digestfd, sigfd,
                   sha512digestfd):
    i = 0
    passfd = nullfd

    E_DEBUG("ENTERING")
    if verbose == 0:
        return nullfd

    if prepath and len(prepath) != 0:
        #		
        # Normalize the name (exclude the prepath).
        #		
        s = name.find(prepath)
        if s is None:
            #			 
            # error
            #			
            return -1
        elif s == name:
            #			
            # Normalize name
            #			
            name = name[len(prepath):]
            if name[0] == '/':
                name = name[1:]
        else:
            #			 
            # error
            #			
            return -1

    if detpat is None:
        if digest_type == tib.TARUIB_N_ADJUNCT_MD5:
            pattern_array = md5sum_adjunct_pattern
        elif digest_type == tib.TARUIB_N_SIG:
            pattern_array = signature_pattern
        elif digest_type == tib.TARUIB_N_MD5:
            pattern_array = md5sum_full_pattern
        elif digest_type == tib.TARUIB_N_SHA1:
            pattern_array = sha1sum_pattern
        elif digest_type == tib.TARUIB_N_SHA512:
            pattern_array = sha512sum_pattern
        elif digest_type == tib.TARUIB_N_SIG_HDR:
            pattern_array = signature_header_pattern
        else:
            sys.stderr.write("internal error in verbose_decode")
            return -1
    else:
        temp_ref_name = name
        pattern_array = determine_pattern_array(detpat, temp_ref_name)
        name = temp_ref_name
        digest_type = detpat
        if pattern_array is None:
            return nullfd

    tmp = strob_open(10)
    while pattern_array[i]:
        if not name or len(name) == 0:
            sys.stderr.write("internal error in taruib::verbose_decode\n")
            strob_close(tmp)
            return nullfd
        ret = fnmatch.fnmatch(pattern_array[i], name)
        if ret == 0:
            if digest_type == tib.TARUIB_N_MD5 and md5digestfd >= 0:
                passfd = check_useArray(useArray, digest_type, md5digestfd)
            elif digest_type == tib.TARUIB_N_SIG and sigfd >= 0:
                passfd = check_useArray(useArray, digest_type, sigfd)
            elif digest_type == tib.TARUIB_N_SHA1 and sha1digestfd >= 0:
                passfd = check_useArray(useArray, digest_type, sha1digestfd)
            elif digest_type == tib.TARUIB_N_SHA512 and sha512digestfd >= 0:
                passfd = check_useArray(useArray, digest_type, sha512digestfd)
            elif digest_type == tib.TARUIB_N_SIG_HDR:
                passfd = nullfd
            else:
                if detpat:
                    passfd = nullfd
                else:
                    passfd = pty.STDERR_FILENO
            break
        i += 1
    strob_close(tmp)
    return passfd


def check_signature_name(sigfilename, prevname):
    E_DEBUG("ENTERING")
    s = strstr(sigfilename, "/" + swlibc.SWBN_SIGNATURE)
    if not s or sigfilename[s + len("/SWBN_SIGNATURE")] != '':
        return 1
    s = s[1:]
    s[0] = ''
    p = prevname.find(sigfilename)
    if p is None or p != prevname:
        ret = 1
    else:
        ret = 0
    s[0] = 's'
    return ret


def do_header_safety_checks(package, name, prevname, signature_header):
    ret = 0

    E_DEBUG("ENTERING")
    if check_signature_name(name, prevname) != 0:
        print("Warning: *** The file name [%s]\n"
              " ** indicate strong possibility of tampering.\n"
              " ** Use of this package is a security risk.\n" % name)
        return -1

    header_bytes = taru_get_recorded_header(package.taru, c_int())
    type = xformat_get_tar_typeflag(package)
    mode = xformat_get_perms(package)
    linkname = xformat_get_linkname(package, None)
    sigsize = xformat_get_filesize(package)
    if stat.S_ISDIR(mode) == 0 and ((mode & stat.S_ISUID) is not None or (mode & stat.S_ISGID) is not None or (
            mode & stat.S_ISVTX) is not None):
        print("Warning: *** The file mode permissions on %s\n"
              " ** indicate strong possibility of tampering.\n"
              " ** Use of this package is a security risk.\n" % name)
        return -2

    if type != tarfile.REGTYPE:
        print("Warning: *** The file type flag on %s\n"
              " ** indicate strong possibility of tampering.\n"
              " ** Use of this package is a security risk.\n" % name)
        return -2

    if linkname and len(linkname):
        return -3

    if signature_header:
        """
        trusted_sig_size is the file data length of the
        sig_header attribute file.
        """
        trusted_sigsize = rpm.readHeaderFromFD(signature_header).__sizeof__
        taru_otoul(signature_header + tc.THB_BO_SIZE)
        if header_bytes and int(trusted_sigsize) == sigsize:
            ret = bytes(signature_header, header_bytes, tc.TARRECORDSIZE)
            if ret:
                ret = -5
        else:
            ret = -6
    else:

        pass

    #	
    # <= 0 is an error
    # > 0 (512 or 1024) is OK
    #	
    if ret < 0:
        return ret
    if ret > 0:
        return -ret
    return sigsize


# -------------------------------------------------------------- 
#  Public Routines.                                              
# -------------------------------------------------------------- 

def taruib_initialize_pass_thru_buffer():
    global g_taruib_gst_overflow_release, g_taruib_gst_fd, g_taruib_gst_len
    g_taruib_gst_overflow_release = 0
    g_taruib_gst_fd = -1
    g_taruib_gst_len = 0


def taruib_write_pass_files(vp, ofd, adjunct_ofd_p):
    package = vp
    nullfd = os.open("/dev/null", os.O_RDWR, 0)
    ifd = xformat_get_ifd(package)
    retval = 0
    total_sym_bytes = 0

    E_DEBUG("ENTERING")
    adjunct_ofd = adjunct_ofd_p
    if adjunct_ofd < 0:
        adjunct_ofd = -1
    taruib_set_overflow_release(0)
    taruib_set_fd(ofd)
    while (ret := xformat_read_header(package)) > 0:
        retval += ret
        if xformat_is_end_of_archive(package):
            os.close(nullfd)
            break

        if adjunct_ofd >= 0:
            #			
            # Hack
            #			
            if xformat_get_tar_typeflag(package) == tarc.SYMTYPE:
                total_sym_bytes += g_taruib_gst_len
                adjunct_ofd = -2
                pass
            else:
                #				
                # write the header.
                #				
                uxfio_write(adjunct_ofd, g_taruib_gst_buffer, int(g_taruib_gst_len))
        #		
        #		* write the header.
        #		
        taruib_set_fd(ofd)
        taruib_clear_buffer()

        #		
        #		* Write the data.
        #		
        if xformat_file_has_data(package):
            taruib_set_fd(-1)
            taruib_set_overflow_release(1)
            if xformat_copy_pass2(package, ofd, ifd, adjunct_ofd) < 0:
                sys.stderr.write("error from xformat_copy_pass2\n")
            taruib_set_overflow_release(0)
            taruib_set_datalen(0)
            taruib_set_fd(ofd)

        if adjunct_ofd == -2:
            adjunct_ofd = adjunct_ofd_p

    if adjunct_ofd_p >= 0:
        uxfio_write(adjunct_ofd_p, g_taruib_gst_buffer, int(g_taruib_gst_len))
    taruib_clear_buffer()
    taruib_set_fd(-1)
    taruib_set_overflow_release(1)

    ret = taru_pump_amount2(ofd, ifd, -1, adjunct_ofd_p)

    if ret < 0:
        sys.stderr.write("swbis: error in taruib_write_pass_files\n")
        retval = -1
    else:
        retval += ret
    return retval


def taruib_get_nominal_reserve():
    return taruib_get_reserve() - tc.TARRECORDSIZE


def taruib_get_reserve():
    return sizeof(g_taruib_gst_buffer)


def taruib_set_overflow_release(i):
    global g_taruib_gst_overflow_release
    g_taruib_gst_overflow_release = i


def taruib_get_overflow_release():
    return g_taruib_gst_overflow_release


def taruib_set_fd(fd):
    global g_taruib_gst_fd
    g_taruib_gst_fd = fd


def taruib_get_fd():
    return g_taruib_gst_fd


def taruib_get_buffer():
    return g_taruib_gst_buffer


def taruib_get_datalen():
    return g_taruib_gst_len


def taruib_get_bufferlen():
    return sizeof(g_taruib_gst_buffer)


def taruib_set_datalen(n):
    global g_taruib_gst_datalen
    g_taruib_gst_datalen = n


def taruib_unread(n):
    global g_taruib_gst_len
    if n > g_taruib_gst_len:
        g_taruib_gst_len = 0
    else:
        g_taruib_gst_len -= n


def taruib_clear_buffer():
    global g_taruib_gst_len
    ret = uxfio_write(g_taruib_gst_fd, g_taruib_gst_buffer, int(g_taruib_gst_len))
    if ret < 0:
        sys.stderr.write("swbis: module taruib: write error (fatal)\n")
        sys.exit(14)
    g_taruib_gst_len -= ret

    if g_taruib_gst_len > 0:
        return -ret
    if g_taruib_gst_len < 0:
        return ret
    return ret


#
# taruib_write_catalog_stream
# write out the catalog half of the package.
#
# version 0 is OBSOLETE.
# version 1 is production version.
#
def taruib_write_catalog_stream(vp, ofd, version, verbose):
    package = vp
    ifd = xformat_get_ifd(package)
    retval = 0
    prevname = strob_open(100)
    namebuf = strob_open(100)
    got_sig = 0
    swpath = swpath_open("")
    taru = taru_create()

    E_DEBUG("ENTERING")
    nullfd = os.open("/dev/null", os.O_RDWR, 0)
    if swpath is None:
        return -21
    if nullfd < 0:
        return -22
    if ifd < 0:
        return -32
    if taru is None:
        return -20

    E_DEBUG("")
    taruib_set_fd(ofd)
    while (ret := xformat_read_header(package)) > 0:
        E_DEBUG("")
        retval += ret
        if xformat_is_end_of_archive(package):
            """	
                This is probably a package with no storage files.	
                jhl 2006-09-08  warning eliminated
                sys.stderr.write("taruib_write_catalog_stream: warning: end of archive.\n")
            """
            break
        E_DEBUG("")
        xformat_get_name(package, namebuf)
        name = strob_str(namebuf)
        path_ret = swpath_parse_path(swpath, name)
        if path_ret < 0:
            E_DEBUG2("taruib_write_catalog_stream: error parsing path [%s].\n", name)
            E_DEBUG("LEAVING")
            return -1
        prepath = swpath_get_prepath(swpath)

        E_DEBUG("")
        """
            version 0: include the leading directories.
            version 1: exclude the leading directories.
        """
        is_catalog = swpath_get_is_catalog(swpath)
        if (version == 0 and (is_catalog == SWPATH_CTYPE_CAT or is_catalog == SWPATH_CTYPE_DIR)) or (
                version == 1 and is_catalog == SWPATH_CTYPE_CAT):
            #			
            #  Leading directories and the /catalog/ files.
            #			 

            E_DEBUG("")
            if strcmp(swpath_get_basename(swpath), swlibc.SWBN_SIGNATURE) == 0 and len(
                    swpath_get_dfiles(swpath)) != 0 and True:
                #				 
                # Skip it
                # It's the signature file in the dfiles
                # directory,
                #				 
                E_DEBUG("")
                temp_ref_null = None
                check_ret = do_header_safety_checks(package, strob_str(namebuf), strob_str(prevname), temp_ref_null)
                if check_ret < 0:
                    E_DEBUG("LEAVING")
                    return -1

                E_DEBUG("")
                got_sig = 1
                taruib_unread(ret)
                retval -= ret
                taruib_set_overflow_release(0)  # OFF
                taruib_set_fd(-1)

                E_DEBUG("")
                if verbose != 0 and version == 1:
                    temp_ref_null2 = None
                    temp_ref_null3 = None
                    temp_ref_prepath = prepath
                    passfd = verbose_decode(temp_ref_null2, temp_ref_null3, temp_ref_prepath, name, nullfd, 2, verbose,
                                            -1, -1, -1, -1)
                else:
                    passfd = nullfd
                xformat_copy_pass(package, passfd, ifd)
                taruib_set_fd(ofd)
            else:
                #				
                # Copy it.
                #				
                E_DEBUG("")
                taruib_set_overflow_release(0)  # OFF

                """			
                With taruib fd set, this has the affect of writting it out.  The nominal file 
                descriptor is set to /dev/null.  That is the taruib_ buffer is maintained 
                (and flushed) by the read routine in xformat_copy_pass().
                """
                E_DEBUG("")
                E_DEBUG3("nullfd=%d  ifd=%d", nullfd, ifd)
                copyret = xformat_copy_pass(package, nullfd, ifd)
                if copyret < 0:
                    E_DEBUG("")
                    swpath_close(swpath)
                    strob_close(namebuf)
                    strob_close(prevname)
                    E_DEBUG("LEAVING : xformat_copy_pass error.")
                    sys.stderr.write("swbis : xformat_copy_pass() error\n")
                    return copyret

                E_DEBUG("")
                if copyret == 0:
                    #					
                    # If there was nothing to write, then flush the buffer.
                    #					
                    E_DEBUG("")

                    if taruib_clear_buffer() < 0:
                        fr()
                        E_DEBUG2("Internal error in taruib_write_catalog_stream : %d\n", __LINE__)
                        return -1
                else:
                    retval += copyret
                E_DEBUG("")
            E_DEBUG("")
        else:
            E_DEBUG("")
            if version == 1:
                E_DEBUG("")
                if is_catalog == SWPATH_CTYPE_DIR:
                    #					
                    # skip leading dirs.
                    # continue
                    #					
                    E_DEBUG("")
                    taruib_unread(ret)
                    retval -= ret
                    taruib_set_overflow_release(0)  # OFF
                    taruib_set_fd(-1)
                    xformat_copy_pass(package, nullfd, ifd)
                    taruib_set_fd(ofd)
                elif is_catalog == SWPATH_CTYPE_STORE:
                    #					
                    # into storage structure.  Stop
                    #					
                    E_DEBUG("")
                    taruib_unread(ret)
                    retval -= ret
                    break
                else:
                    sys.stderr.write("internal error : taruib_write_catalog_stream\n")
                    E_DEBUG("LEAVING")
                    return -1
            elif version == 0:
                #				 
                # read past then catalog, unread the header
                # Assertion: is_catalog == 0.
                # Stop.
                #				
                E_DEBUG("")
                taruib_unread(ret)
                retval -= ret
                break
            else:
                E_DEBUG("")
                sys.stderr.write("internal error: default else \n")
                E_DEBUG("LEAVING")
                return -1
        E_DEBUG("")
        #		
        # Prepare to read the next header
        #		
        taruib_set_overflow_release(1)  # ON
        strob_strcpy(prevname, strob_str(namebuf))

    if ret >= 0:
        E_DEBUG("")
        if taruib_clear_buffer() < 0:
            E_DEBUG2("Internal error in taruib_write_catalog_stream : %d\n", __LINE__)
            return -1

    E_DEBUG("")
    if version == 1:
        #		
        # write two (null) trailer blocks if tar format.
        #		
        E_DEBUG("")
        retval += taru_write_archive_trailer(taru, af.arf_ustar, ofd, tc.TARRECORDSIZE, 0, 0)
        if got_sig == 0:
            sys.stderr.write("swbis: signature file not found.\n")

    os.close(nullfd)
    E_DEBUG("")
    strob_close(namebuf)
    E_DEBUG("")
    swpath_close(swpath)
    E_DEBUG2("LEAVING retval = %d", retval)
    return retval


#
#  swlib_write_storage_stream
#  write out the storage half of the package.
#
# version 0 is OBSOLETE.
# version 1 is production version.
#
#
def taruib_write_storage_stream(vp, ofd, version, ofd2, verbose, digest_type):
    package = vp
    nullfd = os.open("/dev/null", os.O_RDWR, 0)
    ifd = xformat_get_ifd(package)
    retval = 0
    namebuf = strob_open(100)
    swpath = swpath_open("")
    debug = 0
    taru = taru_create()

    #	
    # digest_type is defined above
    #	md5sum    	0
    #	adjunct_md5sum  1
    #	signature 	2
    #	sha1sum   	3
    #	
    E_DEBUG("ENTERING")

    if ofd2 > 0:
        do_adjunct = 1
        digest_type = 1

    if taru is None:
        return -20
    if swpath is None:
        return -21
    if nullfd < 0:
        return -22
    if ifd < 0:
        return -32
    if debug != 0:
        E_DEBUG3("ofd = %d version = %d\n", ofd, version)

    taruib_set_fd(nullfd)
    taruib_set_overflow_release(0)
    while (ret := xformat_read_header(package)) > 0:
        header_bytes = ret
        retval += ret
        if xformat_is_end_of_archive(package):
            # sys.stderr.write("taruib: empty payload section\n"); 
            retval = taru_write_archive_trailer(taru, af.arf_ustar, ofd, 512, 0, 0)
            os.close(nullfd)
            swpath_close(swpath)
            strob_close(namebuf)
            return retval
        xformat_get_name(package, namebuf)
        name = strob_str(namebuf)
        path_ret = swpath_parse_path(swpath, name)
        if path_ret < 0:
            E_DEBUG2("taruib_write_storage_stream:" + " error parsing path [%s].\n", name)
            E_DEBUG("LEAVING")
            return -1
        prepath = swpath_get_prepath(swpath)
        is_catalog = swpath_get_is_catalog(swpath)
        if debug != 0:
            E_DEBUG3("%s : %d\n", name, is_catalog)
        if version == 0:
            if is_catalog != SWPATH_CTYPE_STORE:
                """			
                    Either -1 leading dir, or 1 catalog.
                    skip it.
                """

                taruib_set_overflow_release(0)  # OFF
                taruib_set_fd(0)  # Turn OFF
                copyret = xformat_copy_pass(package, nullfd, ifd)
                if copyret < 0:
                    swpath_close(swpath)
                    strob_close(namebuf)
                    os.close(nullfd)
                    return copyret
                taruib_set_fd(nullfd)
                taruib_clear_buffer()
            else:
                #				
                # = 0, the storage structure.
                #				
                taruib_set_fd(ofd)  # Turn ON
                taruib_clear_buffer()
                retval = ret
                # turn pass-thru buffer off 
                taruib_set_fd(0)
                break
        elif version == 1:
            #			
            # Include the leading directories in the
            # storage file.
            #			
            if is_catalog == SWPATH_CTYPE_CAT:
                #				
                # 1 == catalog.
                # skip it.
                #				

                taruib_set_overflow_release(0)  # OFF
                taruib_set_fd(0)  # Turn OFF

                temp_ref_null = None
                temp_ref_null2 = None
                temp_ref_prepath = prepath
                passfd = verbose_decode(temp_ref_null, temp_ref_null2, temp_ref_prepath, name, nullfd, digest_type,
                                        verbose, -1, -1, -1, -1)
                prepath = temp_ref_prepath
                copyret = xformat_copy_pass_file_data(package, passfd, ifd)

                if copyret < 0:
                    swpath_close(swpath)
                    strob_close(namebuf)
                    os.close(nullfd)
                    return copyret
                xformat_decrement_bytes_written(package, copyret + header_bytes)
                taruib_set_fd(nullfd)
                taruib_clear_buffer()

            elif is_catalog == SWPATH_CTYPE_DIR:
                #				
                #  leading dir  -- pass it thru.
                #				
                if ofd2 >= 0:
                    #					
                    #  Hack
                    #					
                    uxfio_write(ofd2, taruib_get_buffer(), int(taruib_get_datalen()))
                if debug != 0:
                    sys.stderr.write("in leading dirs\n")
                taruib_set_fd(ofd)
                taruib_clear_buffer()
            else:
                #				
                #  0, the storage structure.
                #				
                if ofd2 >= 0:
                    #					
                    #  Hack
                    #					
                    uxfio_write(ofd2, g_taruib_gst_buffer, int(g_taruib_gst_len))
                taruib_set_fd(ofd)  # Turn ON
                taruib_clear_buffer()
                retval = ret
                # turn pass-thru buffer off 
                taruib_set_fd(0)
                #				
                # Now break, the rest of the package is written below.
                #				
                break

        #		
        # Prepare to read the next header
        #		 
        taruib_set_overflow_release(1)  # ON

    #	
    #  write the storage structure to the end.
    #	
    if version == 0:
        ret = taru_pump_amount2(ofd, ifd, -1, -1)
    else:
        #		
        # ofd2 is adjunct_ofd

        # The current file may have data. need to clear it.
        #		
        if xformat_file_has_data(package):
            if xformat_copy_pass2(package, ofd, ifd, ofd2) < 0:
                sys.stderr.write("error from xformat_copy_pass2\n")
        ret = taruib_write_pass_files(vp, ofd, ofd2)

    os.close(nullfd)
    strob_close(namebuf)
    swpath_close(swpath)
    if ret < 0:
        return -100
    retval += ret
    return retval


class alarm_handlerDelegate:
    def invoke(self, UnnamedParameter):
        pass


def taruib_arfcopy(xpackage, xswpath, xofd, leadingpath, do_preview, statbytes, deadman, alarm_handler):
    package = xpackage  # translated as: XFORMAT(xpackage)
    swpath = xswpath  # translated as: SWPATH(xswpath)
    aret = 0
    depth = 0
    bytes = 0
    do_gnu_long_link = 0
    nullfd = os.open("/dev/null", os.O_RDWR, 0)
    newnamebuf = strob_open(100)
    namebuf = strob_open(100)
    # resolved_path = strob_open(10)
    tarheaderflags = xformat_get_tarheader_flags(package)
    file_hdr = (xformat_vfile_hdr(package))
    ifd = xformat_get_ifd(package)

    if ifd < 0:
        return -32

    #	
    # Here's the trick (Uh.. Hack):
    # ofd is really /dev/null; and the real output
    # is set into taruib_set_fd() which enables the write
    # thru cache resulting in the output being the same bytes
    # as the input.
    #	
    ofd = nullfd
    retval = 0
    xformat_set_ofd(package, ofd)
    taruib_initialize_pass_thru_buffer()
    taruib_set_fd(xofd)
    if alarm_handler:
        signal.signal(signal.SIGALRM, alarm_handler)
    while (ret := xformat_read_header(package)) > 0 and (not deadman or (deadman and deadman[0] is None)):
        if alarm_handler:
            if statbytes != 0:
                statbytes[0] = bytes
            # swgp_signal_unblock(signal.SIGALRM, None)
            signal.alarm(0)
            pendmask = signal.sigpending()
            if signal.SIGALRM is pendmask:
                alarm_handler.invoke(signal.SIGALRM)
        if xformat_is_end_of_archive(package):
            break
        bytes += ret
        xformat_get_name(package, namebuf)
        name = strob_str(namebuf)
        strob_strcpy(newnamebuf, leadingpath)
        swlib_unix_dircat(newnamebuf, name)

        #		
        # Make sure it's a normal looking posix pathname.
        #		
        parseret = swpath_parse_path(swpath, name)
        if parseret < 0:
            retval = -3
            # goto error

        # pathret = swlib_vrealpath("", name, depth, resolved_path)

        if depth <= 1 and strstr(name, ".."):
            #			
            # Somebody tried to sneak in too many ".."
            #			
            retval = -4
            # goto error

        namelengthret = taru_is_tar_filename_too_long(strob_str(newnamebuf), tarheaderflags, do_gnu_long_link, 1)

        if namelengthret != 0:
            #			
            # Name too long for the output format.
            #			
            retval = -5
            # goto error

        #		
        # FIXME: Need to test symlinks for too many ".."
        #		

        #		
        # Set the New name in the archive.
        #		
        ahsstaticsettarfilename(file_hdr, strob_str(newnamebuf))

        #		
        # Clear the buffer, this has the effect of writing the
        # header.
        #			
        if alarm_handler:
            swgp_signal_block(signal.SIGALRM, None)
        taruib_clear_buffer()

        #		
        # Now write any data associated with the current
        # archive member.
        #			
        aret = xformat_copy_pass(package, ofd, ifd)
        if aret < 0:
            return retval
        bytes += aret
        # ret = xformat_read_header(package)
    if alarm_handler:
        signal.signal(signal.SIGALRM, alarm_handler)
    if deadman and deadman[0]:
        return -1
    taruib_clear_buffer()
    retval = 0

    #	
    # Pump out the trailer bytes.
    #	
    ret = taru_pump_amount2(ofd, ifd, -1, -1)
    if ret < 0:
        sys.stderr.write("swbis: error in taruib_arfcopy\n")
        retval = -1
    bytes += aret
    #	
    # Now do a final clear.
    #	
    taruib_clear_buffer()
    if alarm_handler:
        signal.alarm(0)

    return retval
