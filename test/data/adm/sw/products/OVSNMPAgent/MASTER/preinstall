#!/sbin/sh
########
#  Product:	Networking
#  SubProduct:	SnmpAgent
#  Fileset:	AGENT-RUN
#  File:	preinstall
########
#
#(c)Copyright 1983-1995 Hewlett-Packard Co.,  All Rights Reserved.
#(c)Copyright 1979, 1980, 1983, 1985-1993 The Regents of the Univ. of California
#(c)Copyright 1980, 1984, 1986 Novell, Inc.
#(c)Copyright 1986-1992 Sun Microsystems, Inc.
#(c)Copyright 1985, 1986, 1988 Massachusetts Institute of Technology
#(c)Copyright 1989-1993  The Open Software Foundation, Inc.
#(c)Copyright 1986 Digital Equipment Corp.
#(c)Copyright 1990 Motorola, Inc.
#(c)Copyright 1990, 1991, 1992 Cornell University
#(c)Copyright 1989-1991 The University of Maryland
#(c)Copyright 1988 Carnegie Mellon University
#
#                           RESTRICTED RIGHTS LEGEND
#Use, duplication, or disclosure by the U.S. Government is subject to
#restrictions as set forth in sub-paragraph (c)(1)(ii) of the Rights in
#Technical Data and Computer Software clause in DFARS 252.227-7013.
#
#
#                           Hewlett-Packard Company
#                           3000 Hanover Street
#                           Palo Alto, CA 94304 U.S.A.
#
#Rights for non-DOD U.S. Government Departments and Agencies are as set
#forth in FAR 52.227-19(c)(1,2).
#
########

    exitval=0			# Anticipate success

    UTILS=/usr/lbin/sw/control_utils

    if [ ! -f $UTILS ]
    then
	echo "ERROR:   Cannot find $UTILS"
	exit 1
    fi

    . $UTILS
    
###############################################################################



# RemoveOldAgentFileset <SDproduct.SDfileset>
#
#	Remove the HP-UX 10.0 Networking.AGENT-RUN fileset if it exists on
#	the system.  Note that the tpdb handles transition from 9.x to 10.01
#	and the preinstall must handle the transition from 10.0 to 10.01.
#	The removal of the fileset should not occur on an alternate root etc.
#	It should only occur on the file server.

RemoveOldAgentFileset()
{
  if [[ "${SW_ROOT_DIRECTORY:-/}" != "/" ]] ; then	# Alternate root?
    return
  fi

  STATE=$(get_install_state $1)

  case "$STATE" in
    configured )
      swremove	-x autoselect_dependents=false	\
		-x enforce_dependencies=false $1
      ;;

    * )
      :		# It's not on the system so do nothing.
      ;;
  esac
}




# RemoveSnmpFile <file>
#	Remove the specified file (if it exists) from the location in the
#	particular $SW_ROOT_DIRECTORY.  The argument is specified w/o
#	prepending $SW_ROOT_DIRECTORY and this function will take care of the
#	psuedo-root considerations.  Note that the routine works for regular
#	files and symbolic links but not directories.  Also note the use of the
#	posix shell OR operator "||".  The old syntax of "-o" does not work 
#	with the posix shell and thus this script only works with the posix sh.

RemoveSnmpFile()
{
  SnmpFile="${SW_ROOT_DIRECTORY:-/}${1:-xxxx}"

  if [[ -f "$SnmpFile" || -h "$SnmpFile" ]] ; then
    rm -f "$SnmpFile"
  fi
}




# MoveSnmpFile <oldfile> <newdir> <newfile>
#	Move the specified file (if it exists) from it's old location to the
#	new one.  Specify the arguments w/o prepending $SW_ROOT_DIRECTORY 
#	since this script does any necessary prepending.  Specify the <oldfile>
#	as a complete absolute path.  Specify the new location as an absolute
#	path to a directory and a component in that directory.  This
#	facilitates making MoveSnmpFile() smart enough to create any necessary
#	subDirectories to contain the <newfile>.

MoveSnmpFile()
{
  OldSnmpFile="${SW_ROOT_DIRECTORY:-/}${1}"	# Prepend $SW_ROOT_DIRECTORY.
  NewSnmpDir="${SW_ROOT_DIRECTORY:-/}${2}"
  NewSnmpFile="${NewSnmpDir}/${3}"

  if [[ -f "$OldSnmpFile" ]] ; then
    if [[ ! -d  "$NewSnmpDir" ]] ; then		# Make destination dir if
      mkdir -p  "$NewSnmpDir"			# necessary.
      chown bin "$NewSnmpDir"
      chgrp bin "$NewSnmpDir"
    fi

    mv "$OldSnmpFile" "$NewSnmpFile"		# Actually mv the file.
  fi
}







# Kill the old version of the snmp agent if it's currently running and this
# preinstall script is invoked on the file server (i.e. The snmpd should not
# be killed if installing onto an alternate root).  The old agent may be the
# monolithic snmpd or the modular snmpdm.  SubAgents are not killed by this
# script unless the particular subAgent elects to kill itself when the binding
# to the master breaks.

if [[ "$SW_ROOT_DIRECTORY" = "/" ]] ; then
  kill_named_procs snmpd	# Kill old monolithic agent
  retval=$?

  if [[ "$retval" -ne 0 ]] ; then
    echo "WARNING: Could not kill network management agent \"snmpd\""
    [[ "$exitval" -ne 1 ]] && exitval=2
  fi
  
  kill_named_procs snmpdm	# Kill new master agent
  retval=$?

  if [[ "$retval" -ne 0 ]] ; then
    echo "WARNING: Could not kill network management agent \"snmpdm\""
    [[ "$exitval" -ne 1 ]] && exitval=2
  fi
fi



RemoveOldAgentFileset Networking.AGENT-RUN	# Remove the 10.0 fileset.
RemoveOldAgentFileset Networking.MASTER		# Remove the 10.01 fileset.


# Deal with the location change of /usr/newconfig/etc/SnmpAgent.d/snmpd.conf.
# The old location was /usr/newconfig/etc/snmpd.conf.  Move the file to the new
# name before calling newconfig_prep in the loop below.  Don't know if the
# SD database needs to know about this or not.

SNMP_AGENT_D_OLD="${SW_ROOT_DIRECTORY:-/}usr/newconfig/etc"
SNMP_AGENT_D_NEW="$SNMP_AGENT_D_OLD/SnmpAgent.d"
SNMPD_CONF_OLD="$SNMP_AGENT_D_OLD/snmpd.conf"

if [[ -f "$SNMPD_CONF_OLD" ]] ; then
  if [[ ! -d "$SNMP_AGENT_D_NEW" ]] ; then
    mkdir -p "$SNMP_AGENT_D_NEW"
    chown bin "$SNMP_AGENT_D_NEW"
    chgrp bin "$SNMP_AGENT_D_NEW"
  fi
  
  mv "$SNMPD_CONF_OLD" "$SNMP_AGENT_D_NEW"
fi





# Remove the old RC startup file from HP-UX 10.0 along with the related 
# symbolic links.  The names have changed for HP-UX 10.00.01 so we will not
# automatically copy over them.

RemoveSnmpFile /sbin/init.d/agent_run		# rm old Startup script
RemoveSnmpFile /sbin/rc1.d/K440agent_run	# rm old Startup Sym Link
RemoveSnmpFile /sbin/rc2.d/S560agent_run	# rm old Startup Sym Link


# Move the old RC startup environment variable file from it's old HP-UX 10.0
# location to the new HP-UX 10.00.01 location.  The configure script may also
# need to convert certain variable names inside.

MoveSnmpFile /etc/rc.config.d/agent_run	/etc/rc.config.d SnmpMaster


# Move the old RC startup DEFAULT environment variable file from it's old 
# HP-UX 10.0 location to the new HP-UX 10.00.01 location.  Conversion should
# not be done here.

MoveSnmpFile /usr/newconfig/etc/rc.config.d/agent_run	\
	     /usr/newconfig/etc/rc.config.d	SnmpMaster



SNMP_CONFIG_FILES="\
/etc/rc.config.d/SnmpMaster	\
/etc/SnmpAgent.d/snmpd.conf	\
/etc/SnmpAgent.d/snmpv2.party	\
/etc/SnmpAgent.d/snmpv2.ctx	\
/etc/SnmpAgent.d/snmpv2.acl	\
/etc/SnmpAgent.d/snmpv2.view	\
"

# Handle conditional placement of newconfig files.  That is, copy the old
# file located in the /usr/newconfig pseudo-root to the same location in the
# /usr/old/usr/newconfig pseudo-root.  This will later be used by the configure
# scripts newconfig_cp call to compare the "real" file (beneath /) with the
# versions in the two pseudo-roots.  Depending on exactly what has changed the
# configure script will conditionally copy over the new default.

for file in $SNMP_CONFIG_FILES ; do
  newconfig_prep $file
  retval=$?
  [[ "$exitval" -ne 1 && "$retval" -ne 0 ]] && exitval=$retval
done

exit $exitval

