# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
# Copyright 2004 Duke University

"""
Core SD Errors.
"""

#from __future__ import unicode_literals
from sd.i18n import ucd, _
import warnings

class Error(Exception):
    # :api
    """Base Error. All other Errors thrown by DNF should inherit from this.

    :api

    """
    def __init__(self, value=None):
        super(Error, self).__init__()
        self.value = None if value is None else ucd(value)

    def __str__(self):
        return "{}".format(self.value)

    def __unicode__(self):
        return ucd(self.__str__())

class MarkingError(Error):
    # :api

    def __init__(self, value=None, software_spec=None):
        """Initialize the marking error instance."""
        super(MarkingError, self).__init__(value)
        self.software_spec = None if software_spec is None else ucd(software_spec)

    def __str__(self):
        string = super(MarkingError, self).__str__()
        if self.software_spec:
            string += ': ' + self.software_spec
        return string

class MarkingErrors(Error):
    # :api
    def __init__(self, no_match_bundle_specs=(), error_bundle_specs=(), no_match_software_specs=(),
                 error_software_specs=(), module_depsolv_errors=()):
        """Initialize the marking error instance."""
        msg = _("Problems in request:")
        if no_match_software_specs:
            msg += "\n" + _("missing products: ") + ", ".join(no_match_software_specs)
        if error_software_specs:
            msg += "\n" + _("broken products: ") + ", ".join(error_software_specs)
        if no_match_bundle_specs:
            msg += "\n" + _("missing bundles: ") + ", ".join(no_match_bundle_specs)
        if error_bundle_specs:
            msg += "\n" + _("broken or empty bundles: ") + ", ".join(error_bundle_specs)


        super(MarkingErrors, self).__init__(msg)
        self.no_match_group_specs = no_match_bundle_specs
        self.error_group_specs = error_bundle_specs
        self.no_match_software_specs = no_match_software_specs
        self.error_bundle_specs = error_bundle_specs
        self.module_depsolv_errors = module_depsolv_errors

    @property
    def module_debsolv_errors(self):
        msg = "Attribute module_debsolv_errors is deprecated. Use module_depsolv_errors " \
              "attribute instead."
        warnings.warn(msg, DeprecationWarning, stacklevel=2)
        return self.module_depsolv_errors

class Prerequisite(Error):
    pass

class Corequisite(Error):
    pass

class Exrequisite(Error):
    pass


class ConfigError(Error):
    def __init__(self, value=None, raw_error=None):
        super(ConfigError, self).__init__(value)
        self.raw_error = ucd(raw_error) if raw_error is not None else None


class DatabaseError(Error):
    pass

class DepsolveError(Error):
    pass

class SwcopyError(Error):
    pass

class DownloadError(Error):
    pass

class LockError(Error):
    pass

#class MarkingError(Error):
#    pass

class BundleMarkingError(Error):
    pass

class SubProductMarkingError(Error):
    pass

class ProductMarkingError(Error):
    pass

class FilesetMarkingError(Error):
    pass

class MetadataError(Error):
    pass

class SelectionPhaseError(Error):
    pass

class AnalysisPhaseError(Error):
    pass

class ExecutionPhaseError(Error):
    pass

class MiscError(Error):
    pass

class BundleNotFoundError(MarkingError):
    pass

class BundlesNotAvailableError(MarkingError):
    def __init__(self, value=None, bundle_spec=None, bundles=None):
        super(BundlesNotAvailableError, self).__init__(value, bundle_spec)
        self.bundles = bundles or []

class ProductNotFoundError(MarkingError):
    pass

class SubproductNotFoundError(MarkingError):
    pass

class FilesetNotFoundError(MarkingError):
    pass

#class FileNotFoundError(MarkingError):
#    pass


# class SoftwareNotInstalledError(MarkingError):
#    def __init__(self, value=None, bundle=None, product=None):
#        super(SoftwareNotInstalledError, self).__init__(value, software_specs)
#        self.software_specs = software_specs or []


class ProcessLockError(LockError):
    def __init__(self, value, pid):
        super(ProcessLockError, self).__init__(value)
        self.pid = pid

    def __reduce__(self):
        """Pickling support."""
        return ProcessLockError, (self.value, self.pid)


class DepotError(Error):
    # :api
    pass


class ThreadLockError(LockError):
    pass


class TransactionCheckError(Error):
    pass
