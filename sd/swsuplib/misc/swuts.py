# swuts.py -  The swbis system identification object.
#
#   Copyright (C) 2006,2007 Jim Lowe
#   Copyright (c) 2023-2024 Paul Weber Conversion to Python
#   All Rights Reserved.
#  
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  

# UNIX® is a registered trademark of The Open Group.

import fnmatch
import ctypes
from sd.swsuplib.misc.swevents import *
from sd.const import LegacyConstants
# from sd.swsuplib.misc.malloc_fprintf import malloc_fprintf
from sd.packaging.rpmvercmp import strcmp
from sd.swsuplib.strob import strob_open, strob_strtok, strob_close



lc = LegacyConstants


class SWUTS:

    def __init__(self):
        self.machine = '\0' # machine_type
        self.sysname = '\0' # os_name
        self.release = '\0' # os_release
        self.version = '\0' # os_version
        self.arch_triplet = '\0' # output of config.guss
        self.result_machine = '\0'
        self.result_sysname = '\0'
        self.result_release = '\0'
        self.result_version = '\0'
        self.match_result = 0

def swuts_store_value(uts, dest, source):
    if dest[0]:
        del dest
    else:
        dest[0] = source


def compare_name(target, sw, verbose):
    if sw is None or len(sw) == 0:
        return 0
    if target is None or len(target) == 0:
        return 0
    ret = fnmatch.fnmatch(sw, target)
    return ret


def swuts_create():
    #uts = malloc_fprintf(ctypes.sizeof(SWUTS), "MALLOC  %18s:%-5d , %s : ", __FILE__, __LINE__, __FUNCTION__)
    uts = ctypes.sizeof(SWUTS)
    uts.machine = None
    uts.sysname = None
    uts.release = None
    uts.version = None
    uts.arch_triplet = None
    uts.result_machine = 0
    uts.result_sysname = 0
    uts.result_release = 0
    uts.result_version = 0
    uts.match_result = -1
    return uts


def swuts_delete(uts):
    if uts.machine:
        uts.machine = None
    if uts.sysname:
        uts.sysname = None
    if uts.release:
        uts.release = None
    if uts.version:
        uts.version = None
    if uts.arch_triplet:
        uts.arch_triplet = None
    uts = None
    return uts


def swuts_read_from_events(uts, events):
    line = '\0'
    value = '\0'
    attribute = '\0'
    ret = 0
    tmp = None

    #	
    #	The line to parse looks something like this
    #
    #	swinstall: swicol: 315:machine_type=i686
    #	swinstall: swicol: 315:os_name=Linux
    #	swinstall: swicol: 315:os_release=2.4.18
    #	swinstall: swicol: 315:os_version=#1 Tue Dec 30 20:43:14 EST 2003
    #	swinstall: swicol: 315:architecture=686-pc-linux-gnu
    #	

    tmp = strob_open(10)
    line = strob_strtok(tmp, events, "\r\n")
    while line != '\0':
        ret = swevent_parse_attribute_event(line, attribute, value)
        if ret == 0:
            if strcmp(attribute, lc.SW_A_os_name) == 0:
                uts.sysname = value
            elif strcmp(attribute, lc.SW_A_os_version) == 0:
                uts.version = value
            elif strcmp(attribute, lc.SW_A_os_release) == 0:
                uts.release = value
            elif strcmp(attribute, lc.SW_A_machine_type) == 0:
                uts.machine = value
            elif strcmp(attribute, lc.SW_A_architecture) == 0:
                uts.arch_triplet = value
            else:
                sys.stderr.write("bad message in swi_uts_read_from_events: %s\n"+attribute)
                ret = -1
        line = strob_strtok(tmp, None, "\r\n")
    strob_close(tmp)
    return ret


def swuts_read_from_swdef(uts):
    return 0


def swuts_compare(uts_target, uts_swdef, verbose):
    #	 uts_target values are actual, they have no patterns
    #    uts_swdef values may be patterns

    temp_ref_sysname = uts_target.sysname
    temp_ref_sysname2 = uts_swdef.sysname
    ret = compare_name(temp_ref_sysname, temp_ref_sysname2, verbose)
    uts_swdef.sysname = temp_ref_sysname2
    uts_target.sysname = temp_ref_sysname
    uts_swdef.result_sysname = ret
    temp_ref_machine = uts_target.machine
    temp_ref_machine2 = uts_swdef.machine
    ret = compare_name(temp_ref_machine, temp_ref_machine2, verbose)
    uts_swdef.machine = temp_ref_machine2
    uts_target.machine = temp_ref_machine
    uts_swdef.result_machine = ret
    temp_ref_release = uts_target.release
    temp_ref_release2 = uts_swdef.release
    ret = compare_name(temp_ref_release, temp_ref_release2, verbose)
    uts_swdef.release = temp_ref_release2
    uts_target.release = temp_ref_release
    uts_swdef.result_release = ret
    temp_ref_version = uts_target.version
    temp_ref_version2 = uts_swdef.version
    ret = compare_name(temp_ref_version, temp_ref_version2, verbose)
    uts_swdef.version = temp_ref_version2
    uts_target.version = temp_ref_version
    uts_swdef.result_version = ret

    if uts_swdef.result_sysname == 0 and uts_swdef.result_machine == 0 and uts_swdef.result_release == 0 and uts_swdef.result_version == 0 and True:
        uts_swdef.match_result = 0
        return 0
    else:
        uts_swdef.match_result = 1
        return 1


def swuts_add_attribute(uts, attribute, value):
    if strcmp(attribute, lc.SW_A_os_name) == 0:
        swuts_store_value(uts, uts.sysname, value)
    elif strcmp(attribute, lc.SW_A_os_version) == 0:
        swuts_store_value(uts, uts.version, value)
    elif strcmp(attribute, lc.SW_A_os_release) == 0:
        swuts_store_value(uts, uts.release, value)
    elif strcmp(attribute, lc.SW_A_machine_type) == 0:
        swuts_store_value(uts, uts.machine, value)
    elif strcmp(attribute, lc.SW_A_architecture) == 0:
        swuts_store_value(uts, uts.arch_triplet, value)


def swuts_is_uts_attribute(name):
    if strcmp(name, lc.SW_A_os_name) and strcmp(name, lc.SW_A_os_release) and strcmp(name, lc.SW_A_os_version) and strcmp(name, lc.SW_A_machine_type) and strcmp(name, lc.SW_A_architecture):
        return 0
    else:
        return 1
