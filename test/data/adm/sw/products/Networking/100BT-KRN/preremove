#! /sbin/sh
########################################################################
# Product: Networking
# Fileset: 100BT-KRN
# preremove
# @(#)B11.23_LR 
#
# (c) Copyright Hewlett-Packard Company 2002
#
########################################################################
# FUNCTIONS


########
# DeleteDriverEntry
#
#   Purpose:  To delete from the system file all the drivers that this fileset
#       has delivered.  Set the $exitval value based on the return value from
#       mod_systemfile().  If multiple deletions, ensure that a FAILURE value
#       of $exitval is not reduced by a later SUCCESS return.
#
DeleteDriverEntry()
{
   typeset retval

   for driver_name in btlan
   do
   	mod_systemfile ${SW_SYSTEM_FILE_PATH} -d $driver_name
   	retval=$?
   	if [[ $retval -ne $SUCCESS ]]
   	then
       		MSG_DriverDeleteFailed
       		[[ $retval -ne $SUCCESS && $exitval -ne $FAILURE ]] && exitval=$retval
   	fi
   done

}

########
# MSG_DriverDeleteFailed
#
#   Purpose:  To issue an ERROR message to the agent log file.  
#
MSG_DriverDeleteFailed()
{
    cat <<- !!EOF
ERROR:   The attempt to remove driver "$driver_name" from the system file
         has failed for the above reason.  You must ensure that the system
         file at "${SW_SYSTEM_FILE_PATH}" does not contain an entry for
         "$driver_name" before a kernel build is attempted.  Manually editing
         "${SW_SYSTEM_FILE_PATH}" to delete references to "$driver_name" is
         one option you have.
!!EOF
}


########
# MSG_ParmDeleteFailed
#
#	Purpose:  To issue a WARNING message to the agent log file.  
#
MSG_ParmDeleteFailed()
{
    cat <<- !!EOF
WARNING: The attempt to delete the configurable parameter "$parm" from the
         system file has failed for the above reason.  Leaving this parameter
	 in the system file will not cause a failure, but it should be 
	 manually deleted from ${SW_SYSTEM_FILE_PATH} as soon as practical.
!!EOF
} 



########################################################################
# MAIN

    typeset exitval

    UTILS=/usr/lbin/sw/control_utils
    if [ -f $UTILS ]
    then
        . $UTILS
    else
        echo "ERROR:   Cannot find $UTILS"
        exit 1
    fi
    exitval=$SUCCESS

########

    if [[ ${SW_SESSION_IS_KERNEL:-FALSE} = "TRUE" ]]
    then
	DeleteDriverEntry
    fi

    exit $exitval
