# swicat_e.py -- catalog entry parsing
#
# Copyright (C) 2007 Jim Lowe
# Copyright (C) 2024 Paul Weber conversion to Python
# All Rights Reserved.
#
# COPYING TERMS AND CONDITIONS:
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#

import os
import sys
import fnmatch


from sd.debug import E_DEBUG3, E_DEBUG2, E_DEBUG
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.cstrings import strstr, strrchr, strcmp, strdup
from sd.swsuplib.misc.swgpg import swgpg_reset, swgpg_run_gpg_verify, swgpg_disentangle_status_lines, swgpg_set_status_array
from sd.swsuplib.misc.strar import strar_add, strar_get
from sd.swsuplib.misc.swpath import swpath_reset
from sd.swsuplib.misc.swheader import SWHEADER_STATE, swheader_store_state, swheader_reset, swheader_open, \
    swheader_set_image_head, swheaderline_get_type, swheader_get_next_object, swheaderline_get_keyword, \
    swheader_get_next_attribute, swheaderline_get_value, swheader_restore_state
from sd.swsuplib.cplob import vplob_open, vplob_add
from sd.swsuplib.strob import strob_str, strob_open, strob_strcpy, strob_strcat, strob_close
from sd.swsuplib.swparse.swparse import sw_yyparse, swpc
from sd.swsuplib.taru.taru import af, tc, tarc, taru_delete, taru_create
from sd.swsuplib.taru.ahs import ahs_copy
from sd.swsuplib.taru.copyout import taru_process_copy_out
from sd.swsuplib.taru.xformat import (xformat_u_open_file, xformat_get_next_dirent, xformat_is_end_of_archive, 
                                      xformat_u_close_file, xformat_close, xformat_open, xformat_open_archive_by_fd,
                                      xformat_set_tarheader_flag, xformat_set_ifd)
from sd.swsuplib.misc.swlib import SWLIB_ERROR, SWLIB_ERROR2, SWLIB_ERROR3, swlib_pipe_pump, swlib_squash_all_trailing_vnewline, swlib_ascii_text_fd_to_buf, swlib_open_memfd, swlibc, swlib_check_clean_path, swlib_unix_dircat, swlib_squash_trailing_slash
from sd.swsuplib.swi.swi import swi_delete, swi_com_get_fd_mem, swi_make_file_list, swi_create, swi_decode_catalog
from sd.swsuplib.misc.swvarfs import swvarfs_close, swvarfs_u_open, swvarfs_get_uinformat, swvarfs_u_close, swvarfs_get_next_dirent, swvarfs_opendup, swvarfs_get_ahs
from sd.swsuplib.uinfile.uinfile import uinc, uinfile_get_swpath
from sd.swsuplib.uxfio import (uc, uxfio_fcntl, uxfio_lseek, uxfio_close, uxfio_read, uxfio_write,
                               uxfio_show_all_open_fd)

UCHAR_MAX = 255
class SwicatConstants:
    SWICAT_DEACTIVE_ENTRY = 0
    SWICAT_ACTIVE_ENTRY = 1
    SWICAT_RETVAL_NULLARCHIVE = 33

swicatc = SwicatConstants()

class SWICAT_E:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self. entry_fd = 0
        self.start_offset = 0
        self.end_offset = 0
        self.entry_prefix = '\0'
        self.xformat = None
        self.taru = None
        self.swi = None
        self.tmp_fd = 0
        self.restore_offset = 0
        self.xformat_close_on_e_delete = 0
        self.taru_close_on_e_delete = 0
        self.swi_close_on_e_delete = 0

"""
 This file contains routines to parse a tarball blob that looks something
 like this:
 
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/vendor_tag
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/export/
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/export/catalog.tar
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/export/catalog.tar.sig
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/session_options
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/control.sh
 var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/INSTALLED
 
 this can be made with the command:
 
 swprogs/swlist -c - bz\*  @ /  | tar tf -

"""

def show_nopen():
    ret = os.open("/dev/null", os.O_RDWR)
    if ret < 0:
        print("fcntl error: " + os.strerror(ret))
    os.close(ret)
    return ret


def fix_offset(e):
    E_DEBUG("")
    ret = uxfio_fcntl(e. entry_fd, uc.UXFIO_F_GET_VEOF, 0)
    E_DEBUG2("ret=%d", ret)
    if ret < 0:
        # virtual file not set 
        ret = uxfio_lseek(e. entry_fd, 0, os.SEEK_CUR)
        E_DEBUG2("ret=%d", ret)
        if ret < 0:
            e.restore_offset = -1
            return -1
        E_DEBUG2("setting restore_offset to %d", ret)
        e.restore_offset = ret
        ret = uxfio_lseek(e. entry_fd, e.start_offset, os.SEEK_SET)
        E_DEBUG2("ret=%d", ret)
        if ret < 0:
            SWLIB_ERROR("setting initial position")
            return -1
        ret = uxfio_fcntl(e. entry_fd, uc.UXFIO_F_SET_VEOF, int(e.end_offset))
        E_DEBUG2("ret=%d", ret)
        if ret < 0:
            SWLIB_ERROR("setting veof")
            return -1
    else:
        if ret != int(e.end_offset):
            # internal error 
            SWLIB_ERROR("INTERNAL ERROR")
            return -1
        ret = uxfio_lseek(e. entry_fd, 0, uc.UXFIO_SEEK_VSET)
        E_DEBUG2("ret=%d", ret)
        if ret < 0:
            SWLIB_ERROR("error setting uc.UXFIO_SEEK_VSET with offset 0")
            return -1
    E_DEBUG("")
    return 0


def unfix_offset(e):
    uxfio_fcntl(e. entry_fd, uc.UXFIO_F_SET_VEOF, -1)
    ret = uxfio_lseek(e. entry_fd, e.restore_offset, os.SEEK_SET)
    if ret < 0:
        SWLIB_ERROR("setting veof")
        return -1
    return 0


def set_entry_fd(e):
    if e.xformat:
        xformat_set_ifd(e.xformat, e. entry_fd)
    if e.xformat.swvarfs:
        e.xformat.swvarfs.fd = e. entry_fd


def init_catalog_entry(e, swvarfs, fpath):
    st = os.stat(fpath)

    E_DEBUG("ENTERING")
    # cfd = e.entry_fd

    if fpath is None:
        path = swvarfs_get_next_dirent(swvarfs, st)
    else:
        path = fpath

    if path is None or len(path) == 0:
        return -1
    E_DEBUG2("new entry: %s", path)

    e.entry_prefix = path
    e.start_offset = uxfio_lseek(swvarfs.fd, 0, os.SEEK_CUR)
    e.end_offset = -1
    E_DEBUG("")
    while (path := swvarfs_get_next_dirent(swvarfs, st)) is not None and swvarfs.eoa == 0 and len(path) != 0 and strstr(path, e.entry_prefix) == path:
        E_DEBUG2("    entry: %s", path)
        pass
    E_DEBUG("")
    e.end_offset = uxfio_lseek(swvarfs.fd, 0, os.SEEK_CUR)
    E_DEBUG("LEAVING")
    return 0


def initialize_file_conflicts_fd(swi, swheader):
    state1 = SWHEADER_STATE()

    E_DEBUG("")
    ex_memfd = -1
    re_memfd = -1
    swheader_store_state(swheader, state1)
    swheader_reset(swheader)

    # Loop over the objects in the SW_A_INSTALLED file 

    E_DEBUG("")
    while next_line := swheader_get_next_object(swheader, int(UCHAR_MAX), int(UCHAR_MAX)):
        E_DEBUG2("next_line=%s\n", next_line)
        if swheaderline_get_type(next_line) != swpc.SWPARSE_MD_TYPE_OBJ:
            # Sanity check 
            # SW_IMPL_ERROR_DIE(1)
            return -1
           
        keyword = swheaderline_get_keyword(next_line)

        if strcmp(keyword, swlibc.SW_A_product) == 0:
            pass
        elif strcmp(keyword, swlibc.SW_A_fileset) == 0:
            while line := swheader_get_next_attribute(swheader):
                keyword = swheaderline_get_keyword(line)
                value = swheaderline_get_value(line, None)
                if strcmp(keyword, swlibc.SW_A_excluded_from_install) == 0:
                    if ex_memfd < 0:
                        ex_memfd = swlib_open_memfd()
                    uxfio_write(ex_memfd, value, len(value))
                    uxfio_write(ex_memfd, "\n", 1)
                elif strcmp(keyword, swlibc.SW_A_replaced_by_install) == 0:
                    if re_memfd < 0:
                        re_memfd = swlib_open_memfd()
                    uxfio_write(re_memfd, value, len(value))
                    uxfio_write(re_memfd, "\n", 1)
            if ex_memfd > 0:
                uxfio_write(ex_memfd, "\0", 1)
            if re_memfd > 0:
                uxfio_write(re_memfd, "\0", 1)
            break
    swheader_restore_state(swheader, state1)
    swi.excluded_file_conflicts_fd = ex_memfd
    swi.replaced_file_conflicts_fd = re_memfd
    return 0


def swicat_e_create():
    swicat_e = SWICAT_E()
    swlibc.SWLIB_ASSERT(swicat_e is not None)

    swicat_e. entry_fd = -1
    swicat_e.start_offset = -1
    swicat_e.end_offset = -1
    swicat_e.entry_prefix = None
    swicat_e.xformat = None
    swicat_e.taru = None
    swicat_e.swi = None
    swicat_e.tmp_fd = swlib_open_memfd()
    swicat_e.restore_offset = -1
    swicat_e.xformat_close_on_e_delete = 0
    swicat_e.taru_close_on_e_delete = 0
    swicat_e.swi_close_on_e_delete = 0
    return swicat_e


def swicat_e_delete(swicat_e):
    if swicat_e.entry_prefix != '\0':
        del swicat_e.entry_prefix

    if swicat_e.xformat_close_on_e_delete != 0:
        if swicat_e.xformat:
            xformat_close(swicat_e.xformat)

    if swicat_e.swi_close_on_e_delete != 0:
        if swicat_e.swi:
            swi_delete(swicat_e.swi)

    if swicat_e.taru_close_on_e_delete != 0:
        if swicat_e.taru:
            taru_delete(swicat_e.taru)

    if swicat_e. entry_fd >= 0:
        uxfio_close(swicat_e. entry_fd)
    if swicat_e.tmp_fd >= 0:
        uxfio_close(swicat_e.tmp_fd)



def swicat_e_reset_fd(e):
    fix_offset(e)
    return fix_offset(e)


"""
 swicat_e_open_entry_tarball - read a catalog entry
 @swicat_e:  object, previously created.
 @ifd: file descriptor containing the archive
 
 This routine assumes the entire archive has only one entry.
 
"""

def swicat_e_open_entry_tarball(swicat_e, ifd):

    length = 0
    E_DEBUG("")
    mem_fd = swlib_open_memfd()
    swlibc.SWLIB_ASSERT(mem_fd >= 0)

    taru = taru_create()
    swlibc.SWLIB_ASSERT(taru is not None)

    E_DEBUG("")
    ret = taru_process_copy_out(taru, ifd, mem_fd, None, None, af.arf_ustar, -1, -1, length, None)
    if ret < 0:
        taru_delete(taru)
        uxfio_close(mem_fd)
        SWLIB_ERROR2("taru_process_copy_out returned %d", ret)
        return -1

    E_DEBUG("")
    ret = uxfio_lseek(mem_fd, 0, os.SEEK_SET)
    if ret != 0:
        taru_delete(taru)
        uxfio_close(mem_fd)
        return -2

    E_DEBUG("")
    # Check for empty i.e. end-of-archive 
    nullarchive = 0
    zerobuf = str(tarc.TARRECORDSIZE)
    readbuf = str(tarc.TARRECORDSIZE)
    uxfio_read(mem_fd, readbuf, tarc.TARRECORDSIZE)
    if zerobuf == readbuf:
        uxfio_read(mem_fd, readbuf, tarc.TARRECORDSIZE)
        if zerobuf == readbuf:
            # empty archive, handle this as a special
            # error case
            nullarchive = 1
    E_DEBUG("")
    del zerobuf
    del readbuf

    E_DEBUG("")
    ret = uxfio_lseek(mem_fd, 0, os.SEEK_SET)
    if ret != 0:
        taru_delete(taru)
        uxfio_close(mem_fd)
        return -2

    xformat = xformat_open(-1, -1, af.arf_ustar)
    swlibc.SWLIB_ASSERT(xformat is not None)

    E_DEBUG("")
    if nullarchive == 0:
        ret = xformat_open_archive_by_fd(xformat, mem_fd, uinc.UINFILE_DETECT_NATIVE, 0)
        if ret != 0:
            E_DEBUG("error, returning -3")
            uxfio_close(mem_fd)
            taru_delete(taru)
            xformat_close(xformat)
            return -3

    E_DEBUG("")
    swicat_e.xformat = xformat
    swicat_e.taru = taru
    swicat_e.taru_close_on_e_delete = 1
    swicat_e.start_offset = 0
    swicat_e.end_offset = int(length)
    swicat_e. entry_fd = mem_fd
    swicat_e.entry_prefix = None

    E_DEBUG("")
    # sanity check 
    ret = uxfio_lseek(mem_fd, 0, os.SEEK_END)
    if ret < 0:
        SWLIB_ERROR("")
        return -4
    if ret != int(length):
        SWLIB_ERROR3("sanity check failed ret=%d, length=%d", ret, int(length))
        return -5

    E_DEBUG("")
    uxfio_lseek(mem_fd, 0, os.SEEK_SET)

    E_DEBUG("")
    if nullarchive == 0:
        ret = init_catalog_entry(swicat_e, swicat_e.xformat.swvarfs, str(None))
        if ret != 0:
            SWLIB_ERROR("init_catalog_entry failed")
            return -6
        retval = 0
    else:
        retval = swicatc.SWICAT_RETVAL_NULLARCHIVE
    E_DEBUG("")
    uxfio_lseek(mem_fd, 0, os.SEEK_SET)

    E_DEBUG2("retval = %d", retval)
    return retval

"""
 swicat_e_open_catalog_tarball - read a catalog tarball containing
 multiple entries.
 @ifd: file descriptor containing the archive

 Returns a list of (SWICAT_E*) objects, NUL on error.
 
"""

def swicat_e_open_catalog_tarball(cfd):
    st = ""
    E_DEBUG("ENTERING")
    list = vplob_open()
    if list is None:
        return None

    uxfio_lseek(cfd, 0, os.SEEK_SET)

    swvarfs = swvarfs_opendup(cfd, uinc.UINFILE_DETECT_NATIVE, 0)
    E_DEBUG2("swvarfs=%p", swvarfs)
    if swvarfs is None:
        return None

    while (path := swvarfs_get_next_dirent(swvarfs, st)) is not None and len(path) != 0 and strcmp(tc.CPIO_INBAND_EOA_FILENAME, path):
        E_DEBUG2("new entry: %s", path)
        e = swicat_e_create()
        vplob_add(list, e)
        e. entry_fd = cfd

    swvarfs_close(swvarfs)
    E_DEBUG("LEAVING")
    return list


def swicat_e_find_attribute_file(swicat_e, attribute, pbuf, ahs):
    st = ""
    tmp = strob_open(32)
    strob_strcpy(pbuf, "")

    E_DEBUG("")
    ret = fix_offset(swicat_e)
    if ret < 0:
        return -1

    E_DEBUG("")
    # memfd = swicat_e.entry_fd
    xformat = swicat_e.xformat
    set_entry_fd(swicat_e)

    E_DEBUG("")
    strob_strcpy(tmp, "*/")
    strob_strcat(tmp, attribute)

    retval = 1
    E_DEBUG("")
    while (name := xformat_get_next_dirent(xformat, st)) is not None:
        E_DEBUG2("name=%s", name)
        if fnmatch.fnmatch(strob_str(tmp), name) == 0:
            ufd = xformat_u_open_file(xformat, name)
            if ufd < 0:
                retval = -1
                break
            if ahs:
                ahs_copy(ahs, swvarfs_get_ahs(xformat.swvarfs))
            ret = swlib_ascii_text_fd_to_buf(pbuf, ufd)
            E_DEBUG2("swlib_ascii_text_fd_to_buf returned %d", ret)
            if ret != 0:
                retval = -2
            else:
                retval = 0
            # ret = xformat_u_close_file(xformat, ufd)
    if retval != 0:
        # FIXME 
        # fill in default value 
        if retval < 0:
            SWLIB_ERROR("need default value")

    swlib_squash_all_trailing_vnewline(strob_str(pbuf))
    unfix_offset(swicat_e)
    strob_close(tmp)
    E_DEBUG3("name=%s  value=[%s]", attribute, strob_str(pbuf))
    E_DEBUG2("retval=%d", retval)
    return retval


def swicat_e_isf_parse(e, pstatus, ahs):
    ifd = os.open("memfd", os.O_RDWR)
    ofd = os.open("memfd", os.O_RDWR)
    datalen = 0

    E_DEBUG("BEGIN")
    tmp = strob_open(12)
    if pstatus:
        pstatus = 0
    ret = swicat_e_find_attribute_file(e, "SW_A_INSTALLED", tmp, ahs)
    E_DEBUG2("swicat_e_find_attribute_file ret=[%d]", ret)
    if ret != 0:
        # fatal error 
        E_DEBUG("ret != 0")
        SWLIB_ERROR("")
        #if pstatus:
        #    pstatus = 1
        E_DEBUG("returning NULL")
        return None

    os.write(ifd, tmp)
    # os.write(ifd, "\n\n\n")
    uxfio_write(ifd, "\n\n\n", 2) # the _e_find_attribute_file()
    #					      routine strips the trailing newline,
    #					      we have to put it back 
    os.lseek(ifd, 0, os.SEEK_SET)

    E_DEBUG2("running sw_yyparse on fd=%d", ofd)
    ret = sw_yyparse(ifd, ofd, swc.SW_A_INSTALLED, 0, swpc.SWPARSE_FORM_MKUP_LEN)
    E_DEBUG2("sw_yyparse: ret=[%d]", ret)
    if ret != 0:
        sys.stderr.write(swlib_utilname_get() + ": error parsing " + "SW_A_INSTALLED" + "\n")
        if pstatus:
            pstatus = 2
        return None, pstatus
    os.lseek(ofd, 0, os.SEEK_SET)
    img = swi_com_get_fd_mem(ofd, datalen)
    img2 = bytearray(datalen+1)
    img2[:datalen] = img
    # img2[datalen] = '\0'
    swheader = swheader_open(None, None)
    swheader_set_image_head(swheader, img2)
    os.close(ifd)
    os.close(ofd)
    return swheader

def swicat_e_make_file_list(e, swuts, swi):

    E_DEBUG("")
    tmp = strob_open(12)

    if swi is None:
        SWLIB_ERROR("")
        unfix_offset(e)
        strob_close(tmp)

    if fix_offset(e) < 0:
        SWLIB_ERROR("")
        unfix_offset(e)
        strob_close(tmp)

    """
     find the location attribute, which is stored as a separate file
     because it is a version attribute
    """
    E_DEBUG("")

    ret = swicat_e_find_attribute_file(e, swc.SW_A_location, tmp, None)
    if ret < 0:
        # fatal error 
        SWLIB_ERROR("")
        unfix_offset(e)
        strob_close(tmp)
    elif ret > 0:
        """
         attribute file not found
         SWLIB_WARN("location file unexpectedly not found")
        """
        strob_strcpy(tmp, "/")

    location = strdup(strob_str(tmp))

    flist = swi_make_file_list(swi, swuts, location, swlibc.SW_FALSE)

    return flist


def swicat_e_verify_gpg_signature(e, swgpg):
    st = ""
    retval = 0

    swgpg_reset(swgpg)

    gpg_status = strob_open(12)
    sig_buf = strob_open(12)
    tmp = strob_open(12)

    E_DEBUG("BEGIN")

    ret = fix_offset(e)
    if ret < 0:
        SWLIB_ERROR("")
        return -1
    while (name := xformat_get_next_dirent(e.xformat, st)) is not None:
        if xformat_is_end_of_archive(e.xformat):
            break
        E_DEBUG2("2name=%s", name)
        if fnmatch.fnmatch(name, "*/export/catalog.tar.sig") == 0 or fnmatch.fnmatch(name, "*/export/catalog.tar.sig*") == 0:
            E_DEBUG("")
            sig_fd = xformat_u_open_file(e.xformat, name)
            swlibc.SWLIB_ASSERT(sig_fd >= 0)
            ret = swlib_ascii_text_fd_to_buf(tmp, sig_fd)
            swlibc.SWLIB_ASSERT(ret == 0)
            strar_add(swgpg.list_of_sig_namesM, name)
            strar_add(swgpg.list_of_sigsM, strob_str(tmp))
            xformat_u_close_file(e.xformat, sig_fd)
            E_DEBUG("")

    ret = fix_offset(e)
    if ret < 0:
        SWLIB_ERROR("")
        return -1

    catalog_tar_fd = -1
    while (name := xformat_get_next_dirent(e.xformat, st)) is not None:
        if fnmatch.fnmatch("*/export/catalog.tar", name) == 0:
            catalog_tar_fd = xformat_u_open_file(e.xformat, name)
            break
    if catalog_tar_fd < 0:
        retval = -1
        unfix_offset(e)
        strob_close(gpg_status)
        strob_close(sig_buf)
        strob_close(tmp)
        return retval
    uxfio_lseek(catalog_tar_fd, 0, os.SEEK_SET)

    # Now loop over the list of sigs 

    i = 0
    while (sig := strar_get(swgpg.list_of_sigsM, i)) is not None:
        name = strar_get(swgpg.list_of_sig_namesM, i)
        E_DEBUG2("signame=%s", name)
        E_DEBUG2("sig=%s", sig)
        ret = uxfio_lseek(catalog_tar_fd, 0, os.SEEK_SET)
        swlibc.SWLIB_ASSERT(ret >= 0)
        # ret = swgpg_run_gpg_verify(swgpg, catalog_tar_fd, sig, 2, gpg_status)
        # strar_add(swgpg->list_of_status_blobs, strob_str(gpg_status));
        swgpg_disentangle_status_lines(swgpg, strob_str(gpg_status))
        # fprintf(stderr, "%s", strob_str(gpg_status)); 
        i += 1

    xformat_u_close_file(e.xformat, catalog_tar_fd)

    # Now classify each signature as to BAD, GOOD, NO_SIG, or NOKEY 

    swgpg_set_status_array(swgpg)

    # swgpg_show_all_signatures(swgpg, STDERR_FILENO); )
    return retval

"""
  swicat_e_open_swi - find the .../export/catalog.tar file and open
  a (SWI*) swi object.

  Returns (SWI*) pointer, NUL on error.
"""

def swicat_e_open_swi(e):

    isf_status = 0

    st = os.stat(e)

    E_DEBUG("BEGIN")
    E_DEBUG2("LOWEST ENTERING AA swicat_e_open_swi FD=%d", show_nopen())
    ret = fix_offset(e)
    if ret < 0:
        SWLIB_ERROR("")
        return None
    E_DEBUG2("LOWEST FD=%d", show_nopen())
    newfd = swlib_open_memfd()

    ifd = e. entry_fd
    ret = swlib_pipe_pump(newfd, ifd)

    """	 
       e-> entry_fd is a file that contains multiple catalog entries
       such as:
    
       var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/
       var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/...
       var/lib/sw/catalog/bzip2-devel/bzip2-devel/1.0.2/0/INSTALLED
       var/lib/sw/catalog/foo/foo/1.0.2/0/
       var/lib/sw/catalog/foo/foo/1.0.2/0/..
       var/lib/sw/catalog/foo/foo/1.0.2/0/INSTALLED
     
     	   newfd contains only the entry that belongs to this SWICAT_E object
     
    """

    E_DEBUG2("swlib_pipe_pump returned %d", ret)

    E_DEBUG2("LOWEST FD=%d", show_nopen())
    uxfio_lseek(newfd, 0, os.SEEK_SET)
    E_DEBUG2("CALLING swvarfs_opendup(%d", newfd)
    swvarfs = swvarfs_opendup(newfd, uinc.UINFILE_DETECT_NATIVE, 0)
    E_DEBUG2("LOWEST AFTER swvarfs_opendup FD=%d", show_nopen())
    if swvarfs is None:
        E_DEBUG("")
        return None
    swvarfs.did_dup = 1 # This causes swvarfs_close to close the fd
    E_DEBUG("searching: START")

    #	
    # Now find the ../export/catalog.tar file and return a file descriptor
    #	 	

    E_DEBUG2("LOWEST BEFORE swvarfs_get_next_dirent FD=%d", show_nopen())
    catalog_tar_fd = -1
    while (name := swvarfs_get_next_dirent(swvarfs, st)) is not None and len(name) != 0:
        E_DEBUG2("searching: name=%s", name)
        if fnmatch.fnmatch("*/export/catalog.tar", name) == 0:
            E_DEBUG("found catalog.tar")
            catalog_tar_fd = swvarfs_u_open(swvarfs, name)
            break
    E_DEBUG2("LOWEST AFTER swvarfs_get_next_dirent FD=%d", show_nopen())
    E_DEBUG("searching: END")
    E_DEBUG2("catalog_tar_fd=%d", catalog_tar_fd)
    if catalog_tar_fd < 0:
        E_DEBUG("error")
        unfix_offset(e)
        return None

    #	
    # Now transfer this file to its own dedicated file descriptor
    #	 

    E_DEBUG2("UXFIO fd: %s",(uxfio_show_all_open_fd(sys.stderr), ""))
    E_DEBUG2("LOWEST FD=%d", show_nopen())
    E_DEBUG2("UXFIO fd: %s",(uxfio_show_all_open_fd(sys.stderr), ""))
    newfd2 = swlib_open_memfd()
    E_DEBUG("")
    E_DEBUG2("UXFIO fd: %s",(uxfio_show_all_open_fd(sys.stderr), ""))
    # ret = swlib_pipe_pump(newfd2, catalog_tar_fd)
    E_DEBUG("")
    E_DEBUG2("UXFIO fd: %s",(uxfio_show_all_open_fd(sys.stderr), ""))
    uxfio_lseek(newfd2, 0, os.SEEK_SET)
    E_DEBUG2("catalog_tar_fd is %d", catalog_tar_fd)
    E_DEBUG2("UXFIO fd: %s",(uxfio_show_all_open_fd(sys.stderr), ""))
    swvarfs_u_close(swvarfs, catalog_tar_fd)
    E_DEBUG("")
    E_DEBUG("")
    E_DEBUG2("UXFIO fd: %s",(uxfio_show_all_open_fd(sys.stderr), ""))
    swvarfs_close(swvarfs)
    E_DEBUG2("LOWEST FD=%d", show_nopen())
    E_DEBUG2("UXFIO fd: %s",(uxfio_show_all_open_fd(sys.stderr), ""))

    # uxfio_close(newfd); 
    ifd = newfd2
    E_DEBUG2("LOWEST FD=%d", show_nopen())

    #	
    # Now create the SWI object which models the in-memory structures
    # of the exported catalog contained in catalog.tar
    #	 

    E_DEBUG("")
    E_DEBUG2("LOWEST FD=%d", show_nopen())
    swi = swi_create()

    E_DEBUG2("LOWEST FD=%d", show_nopen())
    #	
    # open the package catalog
    #	 

    xformat = xformat_open(-1, -1, af.arf_ustar)
    E_DEBUG2(" entry_fd = %d", e. entry_fd)
    ret = xformat_open_archive_by_fd(xformat, ifd, uinc.UINFILE_DETECT_IEEE, 0)
    if ret < 0:
        E_DEBUG("error")
        unfix_offset(e)
        return None

    E_DEBUG2("LOWEST FD=%d", show_nopen())
    #	
    # fill in the (SWI*) structure
    #	 

    E_DEBUG2("LOWEST FD=%d", show_nopen())
    swi.xformat = xformat
    swi.xformat_close_on_delete = 1
    if e.xformat:
        xformat_close(e.xformat)
    e.xformat = xformat
    swi.swvarfs = xformat.swvarfs
    xformat.swvarfs_is_external = 1
    swi.swvarfs_close_on_delete = 1
    swi.uinformat = swvarfs_get_uinformat(swi.swvarfs)
    uinformat = swvarfs_get_uinformat(swi.swvarfs)
    swi.swvarfs.uinformat_close_on_delete = 0
    swi.uinformat_close_on_delete = 1
    swpath = uinfile_get_swpath(uinformat)
    swpath_reset(swpath)
    xformat_set_tarheader_flag(xformat, tc.TARU_TAR_FRAGILE_FORMAT, 1)
    xformat_set_tarheader_flag(xformat, tc.TARU_TAR_RETAIN_HEADER_IDS, 1)
    swi.uinformat = uinformat
    swi.swpath = swpath
    swi.swi_pkg.target_host = "localhost"
    xformat_set_ifd(swi.xformat, ifd)
    E_DEBUG("")

    E_DEBUG2("LOWEST FD=%d", show_nopen())
    # FIXME, also check the GPG signature here 

    #	
    # Decode the package catalog
    #	 

    E_DEBUG2("LOWEST FD=%d", show_nopen())
    ret = swi_decode_catalog(swi)
    if ret < 0:
        E_DEBUG("error")
        unfix_offset(e)
        return None
    E_DEBUG2("LOWEST FD=%d", show_nopen())
    uxfio_close(ifd) # newfd2
    unfix_offset(e)
    set_entry_fd(e)
    E_DEBUG2("LOWEST FD=%d", show_nopen())
    E_DEBUG2("swi=%p", swi)
    E_DEBUG2("LOWEST LEAVING AA swicat_e_open_swi FD=%d", show_nopen())

    installed_header = swicat_e_isf_parse(e, isf_status, None)
    if installed_header is None:
        return None

    initialize_file_conflicts_fd(swi, installed_header)

    return swi


def swicat_e_form_catalog_path(e, buf, isc_path, active_name):
    p = e.entry_prefix
    if p is None:
        return None
    if swlib_check_clean_path(p):
        return None

    if isc_path:
        """
          risc = swlib_return_relative_path(isc_path); 
          the path has had the file:/// URL syntax processed, hence
          if it is absolute it really should be absolute
        """
        buf = isc_path
    else:
        buf = ""

    if active_name == swicatc.SWICAT_ACTIVE_ENTRY:
        swlib_unix_dircat(buf, p)
        p = buf.str
        swlib_squash_trailing_slash(p)
        return p
    elif active_name == swicatc.SWICAT_DEACTIVE_ENTRY:

        """
          make an path name with a leading underscore in the
          sequence number part, for example
          var/lib/sw/catalog/emacs/emacs/21.3.1/_1
          This tells all the utilities to act as if it does not exist.
        """

        swlib_unix_dircat(buf, p)
        p = strob_str(buf)
        swlib_squash_trailing_slash(p)
        seq = strrchr(p, '/')
        if seq is None:
            return None
        if seq == p:
            return None
        seq += 1
        # make sanity check 
        if seq == '\0' or seq.isdigit() == 0:
            # sanity error 
            SWLIB_ERROR("badly formed catalog entry path")
            return None
        buf.set_length(len(p) + 1)
        # memmove(seq + 1, seq, len(seq) + 1)
        # seq = '_'
        return buf.str
    return None




