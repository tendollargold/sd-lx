# sd.swsuplib.misc.swutillib.py -- The top-level common routines for the sw<*> utilities.
#
#   Copyright (C) 2006 Jim Lowe
#   Copyright (C) 2023-2024 Paul Weber, conversion to Python
#   All Rights Reserved.
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import sys
import time

from sd.const import LegacyConstants
from sd.swsuplib.misc.swextopt import swextopt_is_option_true, eOpts
from sd.swsuplib.misc.swlib import swlib_doif_writeap, swlibc
from sd.swsuplib.strob import STROB
from sd.swsuplib.swutillib import swutil_doif_writef, swutil_do_log_is_true
from sd.swsuplib.uinfile.uinfile import UinfileConstants
from sd.swsuplib.taru.xformat import xformat_close, xformat_open_archive_by_fd_and_name, xformat_open_archive_dirfile
from sd.swsuplib.misc.swutilname import swlib_utilname_get

swlib_doif_writef = swutil_doif_writef
swlib_do_log_is_true = swutil_do_log_is_true
uic = UinfileConstants
lc = LegacyConstants


class SwUtilLibConst:
    FILENEEDDEBUG = 1
    SWUTIL_NAME_SWLIST = "swlist"
    SWUTIL_NAME_SWINSTALL = "swinstall"
    SWUTIL_NAME_SWCOPY = "swcopy"
    SWUTIL_NAME_SWPACKAGE = "swpackage"
    SWUTIL_NAME_SWVERIFY = "swverify"
    SWUTIL_NAME_SWREMOVE = "swremove"


class sw_logspec:
    def __init__(self):
        self.loglevel = 0
        self.logfd = 0
        self.fail_loudly = 0


class SWLOG:
    def __init__(self):
        self.swu_efd = sys.stderr.fileno()
        self.swu_logspec = None
        self.swu_verbose = 1
        self.swu_fail_loudly = 0


class XFORMAT:
    def __init__(self):
        pass


def main_sig_handler(signum):
    pass


def safe_sig_handler(signum):
    pass


def printlogline(buf, sptr):
    tm = time.time()
    result = time.strftime("%a %b %d %H:%M:%S %Y", time.localtime(tm))
    s = result.split(' ')[-1]
    if s:
        s = s[1:]
    else:
        s = result
    buf.append(f"{s} [{os.getpid()}] {sptr}")


def swutil_open():
    swutil = SWLOG()
    return swutil


def swutil_close(swutil):
    pass


def swutil_set_stderr_fd(swutil, fd):
    swutil.swu_efd = fd


def doif_i_writef(verbose_level, write_at_level, logspec, fd, format, *args):
    buffer = []
    newret = 0
    level = verbose_level
    if write_at_level >= 0 and level < write_at_level:
        newret = 0
    else:
        buffer.append("")
        if write_at_level >= swlibc.SWC_VERBOSE_7:
            buffer.append("debug> ")
            buffer.append(swlib_utilname_get())
            buffer.append(f"[{os.getpid()}]: ")
        else:
            buffer.append(f"{swlib_utilname_get()}: ")
        newret = swlib_doif_writeap(fd, buffer, format, *args)
    if logspec and logspec.logfd > 0:
        if swlib_do_log_is_true(logspec, verbose_level, write_at_level):
            if fd == sys.stderr.fileno() or fd == sys.stdout.fileno() or fd < 0:
                logbuf = []
                buffer.append("")
                if write_at_level >= swlibc.SWC_VERBOSE_7:
                    buffer.append("debug> ")
                    buffer.append(swlib_utilname_get())
                    buffer.append(f"[{os.getpid()}]: ")
                else:
                    buffer.append(f"{swlib_utilname_get()}: ")
                printlogline(logbuf, ''.join(buffer))
                newret = swlib_doif_writeap(logspec.logfd, logbuf, format, *args)
    return newret


def swutil_setup_xformat(swutil, xformat, source_fd0, source_path, opta, is_seekable, g_verbose, g_logspec,
                         uinfile_open_flags):
    ret = xformat
    flags = uinfile_open_flags
    if opta and (swextopt_is_option_true(eOpts.SW_E_swbis_allow_rpm, opta) or swextopt_is_option_true(
            eOpts.SW_E_swbis_any_format, opta)):
        flags |= uic.UINFILE_DETECT_UNRPM
    if source_fd0 >= 0 and (source_path == None or uinfile_open_flags & uic.UINFILE_DETECT_IEEE):
        flags |= uic.UINFILE_UXFIO_BUFTYPE_DYNAMIC_MEM
        swlib_doif_writef(g_verbose, swlibc.SWC_VERBOSE_7, g_logspec, swutil.swu_efd, "using dynamic mem\n")
    else:
        swlib_doif_writef(g_verbose, swlibc.SWC_VERBOSE_7, g_logspec, swutil.swu_efd, "not using dynamic mem\n")
        flags &= ~uic.UINFILE_UXFIO_BUFTYPE_DYNAMIC_MEM
    ret = xformat
    if source_fd0 >= 0 and source_path:
        if xformat_open_archive_by_fd_and_name(xformat, source_fd0, flags, 0, source_path):
            swlib_doif_writef(g_verbose, g_logspec.fail_loudly, g_logspec, swutil.swu_efd, "archive open failed\n")
            xformat_close(xformat)
            ret = None
    elif source_fd0 < 0:
        if xformat_open_archive_dirfile(xformat, source_path, flags, 0):
            swlib_doif_writef(g_verbose, g_logspec.fail_loudly, g_logspec, swutil.swu_efd,
                              "open failed on %s (by dir)\n", source_path)
            xformat_close(xformat)
            ret = None
    else:
        swlib_doif_writef(g_verbose, g_logspec.fail_loudly, g_logspec, swutil.swu_efd, "%s not found\n", source_path)
        ret = None
    return ret


def swutil_writelogline(logfd, sptr):
    buf = STROB().strob_open(64)
    buf.printlogline(sptr)
    ret = os.pwrite(logfd, buf.strob_str(), buf.strob_strlen())
    if ret != buf.strob_strlen():
        raise Exception("error writing logfile")
    buf.strob_close()
    return ret
def swutil_doif_writef2(swutil, write_at_level, format, *args):
    ret = doif_i_writef(swutil.swu_verbose, write_at_level, swutil.swu_logspec, swutil.swu_efd, format, *args)
    return ret


def swutil_do_log_is_true(logspec, verbose_level, write_at_level):
    if (logspec.logfd == 1 and write_at_level < swlibc.SWC_VERBOSE_6) or (
            logspec.logfd == 2 and write_at_level < swlibc.SWC_VERBOSE_7) or (
            logspec.logfd == 2 and verbose_level > swlibc.SWC_VERBOSE_7):
        return 1
    else:
        return 0


def swutil_doif_writef(verbose_level, write_at_level, logspec, fd, format, *args):
    ret = doif_i_writef(verbose_level, write_at_level, logspec, fd, format, *args)
    return ret


def cpp_doif_i_writef_orig(verbose_level, write_at_level, logspec, fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format, *args):
    b = []
    b.append(f"{cpp_FILE}:{cpp_LINE} ({cpp_FUNCTION}): {format}")
    ret = doif_i_writef(verbose_level, write_at_level, logspec, fd, ''.join(b), *args)
    return ret


def swutil_cpp_doif_writef_orig(verbose_level, write_at_level, logspec, fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format,
                                *args):
    return cpp_doif_i_writef(verbose_level, write_at_level, logspec, fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format, *args)



def cpp_doif_i_writef(verbose_level, write_at_level, logspec, fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format_str, *args):
    message = f"{cpp_FILE}:{cpp_LINE} ({cpp_FUNCTION}): {format_str}"
    return doif_i_writef(verbose_level, write_at_level, logspec, fd, message, *args)


def swutil_cpp_doif_writef(verbose_level, write_at_level, logspec, fd, cpp_FILE, cpp_LINE, cpp_FUNCTION, format_str,
                           *args):
    if verbose_level >= write_at_level:
        message = format_str.format(*args)
        os.write(fd, message.encode())
