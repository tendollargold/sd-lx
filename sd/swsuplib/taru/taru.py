# taru.py - Low-level tarball utility functions
#
#   Copyright (C) 2000,2004 Jim Lowe
# Copyright (C) Paul Weber, Python conversion
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

from enum import Enum
import math
import os
import sys
from ctypes import sizeof, c_longlong, c_long

from sd.swsuplib.misc.swlib import SWLIB_FATAL
from sd.debug import E_DEBUG2
from sd.swsuplib.misc.cstrings import strcmp, memcpy
from sd.swsuplib.ls_list.gen_subs import lslistc, ls_list_set_encoding_by_lang, ls_list_safe_print_to_strob, ls_list_set_encoding_flag, ls_list_to_string, ls_list_get_encoding_flag

from sd.swsuplib.cplob import cplob_val, cplob_open, cplob_add_nta, cplob_shallow_close
from sd.swsuplib.taru.cpiohdr import chc, FILEDIGS, EXATT
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlib_strncpy
from sd.swsuplib.strob import strob_open, strob_close, strob_str, strob_sprintf,strob_strncpy, STROB_DO_APPEND
from sd.swsuplib.strob import strob_strcpy,  strob_strtok, strob_strlen, strob_strcat, strob_set_length, strob_set_memlength
from sd.swsuplib.taru.ahs import (ahsstaticgettarfilename, ahsstaticsettarfilename, \
    ahsstaticgettarlinkname, ahsstaticsetpaxlinkname, ahsstaticsettargroupname,
                                  ahsstaticgettargroupname, ahsstaticgettarusername, ahsstaticsettarusername)
from sd.swsuplib.taru.etar import etar_open, Etar
from sd.swsuplib.taru.mtar import taru_is_tar_filename_too_long, taru_is_tar_linkname_too_long
from sd.swsuplib.taru.otar import taru_get_tar_filetype, taru_exatt_override_ustar
from sd.swsuplib.taru.tar import tarc
from sd.swsuplib.uxfio import uxfio_write

LLONG_MAX = c_longlong
LONG_MAX = c_long
class TARU:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.id = '\0'
        self.header = None
        self.header_length = 0
        self.taru_tarheaderflags = 0
        self.do_record_header = 0
        self.u_name_buffer = None
        self.u_ent_buffer = None
        self.nullfd = 0
        self.md5buf = None
        self.linkrecord_disable = 0
        self.preview_fd = 0
        self.preview_level = 0
        self.preview_buffer = None
        self.read_buffer = None
        self.read_buffer_pos = 0
        self.exthdr_data = None
        self.etar = None
        self.localbuf = None

class ArchiveFormat(Enum):
    arf_unknown = 0
    arf_binary = 1
    arf_oldascii = 2
    arf_newascii = 3
    arf_crcascii = 4
    arf_tar = 5
    arf_ustar = 6
    arf_hpoldascii = 7
    arf_hpbinary = 8
    arf_filesystem = 9

af = ArchiveFormat

class TaruConstants:
    TARU_AR_HEADER_SIZE = 60 # GNU ar format
    TARU_AR_SIZE_OFFSET = 48 # GNU ar format
    THB_BO_NAME = 0
    THB_BO_MODE = 100
    THB_BO_UID = 108
    THB_BO_GID = 116
    THB_BO_SIZE = 124
    THB_BO_MTIME = 136
    THB_BO_CHKSUM = 148
    THB_BO_TYPEFLAG = 156
    THB_BO_LINKNAME = 157
    THB_BO_MAGIC = 257
    THB_BO_VERSION = 263
    THB_BO_UNAME = 265
    THB_BO_GNAME = 297
    THB_BO_DEVMAJOR = 329
    THB_BO_DEVMINOR = 337
    THB_BO_PREFIX = 345
    THB_FL_NAME = 100
    THB_FL_MODE = 8
    THB_FL_UID = 8
    THB_FL_GID = 8
    THB_FL_SIZE = 12
    THB_FL_MTIME = 12
    THB_FL_CHKSUM = 8
    THB_FL_TYPEFLAG = 1
    THB_FL_LINKNAME = 100
    THB_FL_MAGIC = 6
    THB_FL_VERSION = 2
    THB_FL_UNAME = 32
    THB_FL_GNAME = 32
    THB_FL_DEVMAJOR = 8
    THB_FL_DEVMINOR = 8
    THB_FL_PREFIX = 155
    XHDTYPE = 'x' # Extended header
    XGLTYPE = 'g' # Global extended header
    GNUTYPE_LONGNAME = 'L'
    GNUTYPE_LONGLINK = 'K'
    PREFIX_FIELD_SIZE = 155 # Tar Header field size, do not change
    NAME_FIELD_SIZE = 100 # Tar Header field size, do not change
    SNAME_LEN = 32 # Tar Header field size, do not change
    DIRECTORY_SEPARATOR = '/'

    GNU_LONG_LINK = "././@LongLink"
    CPIO_INBAND_EOA_FILENAME = "TRAILER!!!"
    TARRECORDSIZE = 512
    TAR_MAX_7_OCTAL = 2097151 # ``7777777 ''
    TAR_MAX_SIZE = 8589934591 # ``77777777777 ''
    STAR_VENDOR = "SCHILY"
    SW_VENDOR = "unknown"
    PAX_KW_PATH = "path"
    PAX_KW_LINKPATH = "linkpath"
    PAX_KW_GNAME = "gname"
    PAX_KW_UNAME = "uname"
    PAX_KW_ATIME = "atime"
    PAX_KW_MTIME = "mtime"
    PAX_KW_CTIME = "ctime"
    PAX_KW_GID = "gid"
    PAX_KW_UID = "uid"
    PAX_KW_SIZE = "size"
    PAX_KW_DEVMAJOR = "devmajor"
    PAX_KW_DEVMINOR = "devminor"

    TARU_SYSDBNAME_LEN = 120
    TARU_FORMAT_TAR = 0
    TARU_FORMAT_CPIO = 1
    TARU_BUFSIZ = 8192
    TARU_BUFSIZ_RES = TARU_BUFSIZ + tarc.TARRECORDSIZE
    TARU_PV_0 = 0 # preview level, no preview
    TARU_PV_1 = 1 # preview level, print filename
    TARU_PV_2 = 2 # preview level, print tar listing
    TARU_PV_3 = 3 # preview level, print verbose ad-hoc tar listing
    TARU_HE_LINKNAME_4 = -4 # write_header() return value for linkname too long  -4
    TARU_C_BY_UNA = 4 # not available, not set
    TARU_C_BY_UNAME = 3 # get uid from uname.
    TARU_C_BY_UID = 2 # get uname from uid
    TARU_C_BY_USYS = 1 # Default existing policy
    TARU_C_BY_UNONE = 0 # no database lookups, use id and name as is
    TARU_C_BY_GNA = 4 # not available, not set
    TARU_C_BY_GNAME = 3 # get gid from gname
    TARU_C_BY_GID = 2 # get gname from gid
    TARU_C_BY_GSYS = 1 # Default existing policy
    TARU_C_BY_GNONE = 0 # no database lookups, use id and name as is
    TARU_C_DO_NUMERIC = "__SwbisInternalDoNumeric__" # FIXME, inband signalling

    ISASCII = lambda char: char.isascii()

    ISODIGIT = lambda char: (ord('0') <= ord(char) <= ord('9'))
    ISSPACE = lambda char: (char.isascii() and char.isspace())

    CPIO_NEWASCII_MAGIC = 0o70701
    CPIO_CRCASCII_MAGIC = 0o70702
    CPIO_OLDASCII_MAGIC = 0o70707

    TARU_BUFSIZ = 8192


    TARU_TAR_BE_LIKE_PAX = (1 << 0) # i.e. utility, not format, off by default
    TARU_TAR_NUMERIC_UIDS = (1 << 1) # off by default taru_tarheaderflags
    TARU_TAR_GNU_LONG_LINKS = (1 << 2) # off by default taru_tarheaderflags
    TARU_TAR_GNU_GNUTAR = (1 << 3) # Same as GNU tar 1.15.x
    TARU_TAR_GNU_BLOCKSIZE_B1 = (1 << 4) # taru_tarheaderflags
    TARU_TAR_BE_LIKE_STAR = (1 << 5) # taru_tarheaderflags
    TARU_TAR_FRAGILE_FORMAT = (1 << 6) # Do not recover from skipped bytes, bad cksum, etc.
    TARU_TAR_DO_STRIP_LEADING_SLASH = (1 << 7) # taru_tarheaderflags
    TARU_TAR_RETAIN_HEADER_IDS = (1 << 8) #
    TARU_TAR_GNU_OLDGNUTAR = (1 << 9) # Same as 1.13.25 default compilation settings
    TARU_TAR_GNU_OLDGNUPOSIX = (1 << 10) # Same as 1.13.25 --posix for short names only
    TARU_TAR_NAMESIZE_99 = (1 << 11) # Use 99 char name size split like oldgnu format
    TARU_TAR_PAXEXTHDR = (1 << 12) # Use Pax Extended Headers where needed
    TARU_TAR_PAXEXTHDR_MD = (1 << 13) # Mandatory Pax Extended Headers
    TARU_TAR_ACLEXTHDR = (1 << 14) # NOT Used in 1.13.1
    TARU_TAR_SELINUXEXTHDR = (1 << 15)  # NOT Used in 1.13.1
    TARU_TAR_NO_OVERFLOW = (1 << 16) # Do not allow any fields to overflow

tc = TaruConstants

def i_taru_needy_message(filename, field_name):
    sys.stderr.write( "%s: %s: %s value too long for selected format\n"+ swlib_utilname_get()+ filename+ field_name)
    return


def taru_create():
    taru = TARU()
    taru.id = 'A'
    taru.header = strob_open(1024)
    taru.header_length = 0
    taru.taru_tarheaderflags = 0
    taru.do_record_header = 0
    taru.u_name_buffer = strob_open(8)
    taru.u_ent_buffer = strob_open(tc.SNAME_LEN + tc.SNAME_LEN)
    taru.nullfd = os.open("/dev/null", os.O_RDWR, 0)
    # taru->do_md5 = 0;
    taru.md5buf = strob_open(64)
    # strob_set_memlength(taru.md5buf, 64)
    taru.linkrecord_disable = 0
    taru.preview_fd = -1
    taru.preview_level = tarc.TARU_PV_0
    taru.preview_buffer = strob_open(64)
    taru.read_buffer = strob_open(tc.TARRECORDSIZE * 4)
    taru.read_buffer_pos = 0
    taru.exthdr_data = strob_open(16)
    taru.etar = etar_open(0)
    taru.localbuf = strob_open(12)
    return taru


def taru_delete(taru):
    strob_close(taru.header)
    strob_close(taru.u_name_buffer)
    strob_close(taru.u_ent_buffer)
    strob_close(taru.md5buf)
    strob_close(taru.preview_buffer)
    os.close(taru.nullfd)
    Etar.etar_close(taru.etar)

class vfreadDelegate:
    def invoke(self, UnnamedParameter, UnnamedParameter2, UnnamedParameter3):
        pass



def taru_read_pax_data_blocks(taru, fd, fsource_buffer, data_len, vfread):
    retval = 0
    strb = taru.read_buffer

    n_blocks_to_read = math.trunc(data_len / float(int(tc.TARRECORDSIZE)))

    if int(math.fmod(data_len, tarc.TARRECORDSIZE)):
        n_blocks_to_read += 1

    #
    # preallocate the space for the ustar header at the end of the pax header
    # data blocks
    #
    strob_set_length(strb, taru.read_buffer_pos + n_blocks_to_read * tarc.TARRECORDSIZE + tarc.TARRECORDSIZE + 10)
    buf = int(strob_str(strb)) + taru.read_buffer_pos

    n = 0
    while n < n_blocks_to_read:
        if fsource_buffer:
            memcpy(buf + n * tarc.TARRECORDSIZE, (fsource_buffer + (n + 1) * tarc.TARRECORDSIZE), int(tarc.TARRECORDSIZE))
            ret = tarc.TARRECORDSIZE
        else:
            ret = vfread(fd, buf + n * tarc.TARRECORDSIZE, tarc.TARRECORDSIZE)
        if ret != tarc.TARRECORDSIZE:
            return -1

        if taru.do_record_header != 0:
            strob_set_memlength(taru.header, taru.header_length + 512)
            memcpy((strob_str(taru.header) + taru.header_length), (buf + n * tarc.TARRECORDSIZE), 512)
            taru.header_length += 512
        retval += ret
        taru.read_buffer_pos += ret
        n += 1
    buf[retval] = '\0' # Null terminate the data blocks
    return retval


def taru_set_header_recording(taru, n):
    taru.do_record_header = n


def taru_get_recorded_header(taru, len):
    if len:
        len = taru.header_length
    return strob_str(taru.header)


def taru_clear_header_buffer(taru):
    taru.header_length = 0

def taru_hdr_get_filesize(file_hdr):
    check_off_t = 0
    if file_hdr.c_filesize >= LLONG_MAX:
        SWLIB_FATAL("c_filesize too big: >=LLONG_MAX")
    if check_off_t == 0:
        st = os.stat(file_hdr)
        if sizeof(st.st_size) < 5:
            check_off_t = 1
        else:
            check_off_t = 2
    if check_off_t == 1 and file_hdr.c_filesize >= LONG_MAX:
        SWLIB_FATAL("c_filesize too big: >=LONG_MAX")
    return int(file_hdr.c_filesize)


def taru_append_to_header_buffer(taru, buf, len):
    s = '\0'
    strob_set_length(taru.header, taru.header_length + len + 1)
    s = strob_str(taru.header)
    memcpy(s[taru.header_length:], buf, len)
    taru.header_length += len


def taru_digs_print(digs, buffer):

    if digs.do_md5 == chc.DIGS_ENABLE_ON:
        strob_sprintf(buffer, STROB_DO_APPEND, " [%s]", digs.md5)
    else:
        strob_sprintf(buffer, STROB_DO_APPEND, " []")


    if digs.do_sha1 == chc.DIGS_ENABLE_ON:
        strob_sprintf(buffer, STROB_DO_APPEND, " [%s]", digs.sha1)
    else:
        strob_sprintf(buffer, STROB_DO_APPEND, " []")


def taru_digs_delete(digs):
    digs = None


def taru_digs_create():
    digs = FILEDIGS()
    if digs is None:
        return digs
    taru_digs_init(digs, chc.DIGS_ENABLE_OFF, 0)
    return digs


def taru_digs_init(digs, enable, poison):
    digs.do_poison = poison
    digs.md5[0] = '\0'
    digs.sha1[0] = '\0'
    digs.sha512[0] = '\0'
    digs.size[0] = '\0'
    if enable >= 0:
        digs.do_md5 = enable
        digs.do_sha1 = enable
        digs.do_sha512 = enable
        digs.do_size = enable


def taru_fill_exthdr_needs_mask(file_hdr, tarheaderflags, is_pax_header_allowed, be_verbose):
    # file_hdr->extHeader_needs_mask = 0;
    mask = 0


    #
    # These tests represent tests of the limits of the ustar header fields
    #

    if taru_is_tar_filename_too_long(ahsstaticgettarfilename(file_hdr), 0, None, 0):
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "path")
        mask |= chc.TARU_EHUM_PATH


    if taru_is_tar_linkname_too_long(ahsstaticgettarlinkname(file_hdr), 0, None):
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "linkname")
        mask |= chc.TARU_EHUM_LINKPATH


    if file_hdr.c_filesize > tc.TAR_MAX_SIZE:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "size")
        mask |= chc.TARU_EHUM_SIZE


    if file_hdr.c_uid > tarc.TAR_MAX_UID:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "uid")
        mask |= chc.TARU_EHUM_UID


    if file_hdr.c_gid > tarc.TAR_MAX_GID:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "gid")
        mask |= chc.TARU_EHUM_GID


    if len(ahsstaticgettarusername(file_hdr)) >= tc.THB_FL_UNAME:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "owner name")
        mask |= chc.TARU_EHUM_UNAME

    if len(ahsstaticgettargroupname(file_hdr)) >= tc.THB_FL_GNAME:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "group name")
        mask |= chc.TARU_EHUM_GNAME

    if file_hdr.c_filesize > tarc.TAR_MAX_SIZE:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "size")
        mask |= chc.TARU_EHUM_SIZE

    if file_hdr.c_dev_maj > tarc.TAR_MAX_DEVICE:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "dev maj")
        mask |= chc.TARU_EHUM_DEV_MAJ

    if file_hdr.c_dev_min > tarc.TAR_MAX_DEVICE:
        if be_verbose != 0 and is_pax_header_allowed == 0:
            i_taru_needy_message(ahsstaticgettarfilename(file_hdr), "dev min")
        mask |= chc.TARU_EHUM_DEV_MIN

    # FIXME check size field also 


    if (tarheaderflags & tarc.TARU_TAR_PAXEXTHDR_MD) != 0:
        #		
        # Mandatory PAX Header
        # 		 
        mask |= chc.TARU_EHUM_PATH
        mask |= chc.TARU_EHUM_LINKPATH
        mask |= chc.TARU_EHUM_SIZE
        mask |= chc.TARU_EHUM_UID
        mask |= chc.TARU_EHUM_GID
        mask |= chc.TARU_EHUM_UNAME
        mask |= chc.TARU_EHUM_GNAME
        mask |= chc.TARU_EHUM_MTIME
        # mask |=  chc.TARU_EHUM_DEV_MIN; 
        # mask |=  chc.TARU_EHUM_DEV_MAJ; 
        mask |= chc.TARU_EHUM_SIZE
    return mask


def taru_exatt_init(hnew):
    hnew.is_in_use = 0
    hnew.len = -1
    hnew.vendor[0] = '\0'
    hnew.namespace[0] = '\0'
    strob_strcpy(hnew.attrname, "")
    strob_strcpy(hnew.attrvalue, "")


def taru_exatt_create():
    hnew = EXATT()
    hnew.magic = "XH"
    hnew.attrname = strob_open(22)
    hnew.attrvalue = strob_open(22)
    taru_exatt_init(hnew)
    return hnew


def taru_exatt_delete(p):
    strob_close(p.attrname)
    strob_close(p.attrvalue)
    p = None


def taru_exattshow_debug(file_hdr):
    p = None
    index = 0
    if file_hdr.extattlist is None:
        return
    index = 0

    sys.stderr.write( "extended header begin\n")
    while (p := cplob_val(file_hdr.extattlistM, index)):
        sys.stderr.write( "attribute=[%s] value=[%s]\n"+ strob_str(p.attrname)+ strob_str(p.attrvalue))
        index += 1
    sys.stderr.write( "extended header end\n")


def taru_exattlist_delete_all(file_hdr):
    p = None
    index = 0
    if file_hdr.extattlist is None:
        return
    index = 0
    while p := cplob_val(file_hdr.extattlistM, index):
        taru_exatt_delete(p)
        index += 1


def taru_exattlist_get_free(file_hdr):
    if file_hdr.extattlist is None:
        file_hdr.extattlist = cplob_open(3)
        file_hdr.extattlist.refcount = 1

    # Loop over the records and find the first one that is not used 
    index = 0
    while p := cplob_val(file_hdr.extattlist, index):
        if p.is_in_use == 0:
            taru_exatt_init(p)
            return p
        index += 1
    p = taru_exatt_create()
    cplob_add_nta(file_hdr.extattlistM, str(p))
    return p


def taru_exattlist_init(file_hdr):
    if file_hdr.extattlist is None:
        file_hdr.extattlist = cplob_open(22)
        file_hdr.extattlist.refcount = 1

    # Loop over the records and set all to not used 
    index = 0
    while p := cplob_val(file_hdr.extattlist, index):
        p.is_in_use = 0
        index += 1


def taru_exatt_parse_fqname(exatt, attr, attrlen):
    has_prefix = 0
    has_namespace = 0
    has_name = 0

    retval = 0
    tmp = strob_open(32)
    strob_strncpy(tmp, attr, attrlen)

    swlib_strncpy(exatt.vendor, "", 2)
    swlib_strncpy(exatt.namespace, "", 2)
    strob_strcpy(exatt.attrname, "")

    s = strob_strtok(tmp, strob_str(tmp), ".")
    while s != '\0':
        if s.isupper() and has_prefix == 0:
            #			
            # This would be SCHILY, RMT, et al.
            # 			 
            swlib_strncpy(exatt.vendor, s, chc.EXATT_PREFIX_LEN)
            has_prefix = 1
        elif not s.isupper() and has_namespace == 0:
            #			
            # This would be selinux, acl, et.al,
            # or an unqualified POSIX ustar header attribute
            # 			 
            swlib_strncpy(exatt.namespace, s, chc.EXATT_NAMESPACE_LEN)
            has_namespace = 1
        elif not s.isupper() and has_name == 0:
            strob_strcpy(exatt.attrname, s)
            has_name = 1
        else:
            #			
            # this case would be a 4 field dot delimited field
            # 			 
            # for now consider this an error 
            retval = -1
            break
        s = strob_strtok(tmp, None, ".")

    if has_namespace != 0 and has_name == 0 and has_prefix == 0:
        #		
        # This is the case for an unqualified lower case
        # POSIX attribute
        # 		 
        strob_strcpy(exatt.attrname, exatt.namespace)
        swlib_strncpy(exatt.namespace, "", 2)
    return retval


def taru_init_header_digs(file_hdr):
    if file_hdr.digs is None:
        file_hdr.digs = taru_digs_create()
    else:
        taru_digs_init(file_hdr.digs, chc.DIGS_ENABLE_OFF, 0)


def taru_free_header_digs(file_hdr):
    if file_hdr.digs:
        taru_digs_delete(file_hdr.digs)
        file_hdr.digs = None


class new_cpio_header:
    def __init__(self):
        self.c_name = None
        self.c_tar_linkname = None
        self.c_username = None
        self.c_groupname = None
        self.digs = None
        self.extattlist = None
        self.usage_mask = 0
        self.extHeader_usage_mask = 0
        self.extHeader_needs_mask = 0
        self.c_mode = 0o400
        self.c_uid = 0
        self.c_gid = 0
        self.c_nlink = 0
        self.c_mtime = os.stat('.').st_mtime
        self.c_ctime = os.stat('.').st_mtime
        self.c_atime = os.stat('.').st_mtime
        self.c_atime_nsec = os.stat('.').st_mtime_ns
        self.c_ctime_nsec = os.stat('.').st_mtime_ns
        self.c_filesize = 0
        self.c_dev_maj = 0
        self.c_dev_min = 0
        self.c_rdev_maj = 0
        self.c_rdev_min = 0
        self.c_cu = tc.TARU_C_BY_USYS
        self.c_cg = tc.TARU_C_BY_GSYS
        self.c_is_tar_lnktype = -1

def taru_make_header():
    hnew = new_cpio_header()
    taru_init_header(hnew)
    return hnew

def taru_init_header(file_hdr):
    index = 0
    while p := file_hdr.extattlist[index]:
        p.is_in_use = 0
        index += 1


def taru_set_tarheader_flag(taru, flag, n):
    if n != 0:
        taru.taru_tarheaderflags |= flag
    else:
        taru.taru_tarheaderflags &= ~flag


def taru_set_preview_level(taru, preview_level):
    taru.preview_level = preview_level


def taru_get_preview_level(taru):
    return taru.preview_level


def taru_set_preview_fd(taru, fd):
    taru.preview_fd = fd


def taru_get_preview_fd(taru):
    return taru.preview_fd


def taru_free_header(h):
    p = None
    taru_free_header_digs(h)
    ahsstaticsettargroupname(h, None) # these have effect of calling
    ahsstaticsettarusername(h, None) # strob_close()
    ahsstaticsetpaxlinkname(h, None)
    ahsstaticsettarfilename(h, None)

    if h.extattlistM:
        p = h.extattlist
        p.refcount -= 1
        if p.refcount <= 0:
            # really delete 
            taru_exattlist_delete_all(h)
            cplob_shallow_close(p)
    del h


def taru_print_tar_ls_list(buf, file_hdr, vflag):
    strob_strcpy(buf, "")
    type = taru_get_tar_filetype(file_hdr.c_mode)

    if (file_hdr.c_is_tar_lnktype == 1) or (type == tarc.REGTYPE and len(ahsstaticgettarlinkname(file_hdr)) != 0):
        type = tarc.LNKTYPE
    if type >= 0:
        ls_list_to_string(ahsstaticgettarfilename(file_hdr), ahsstaticgettarlinkname(file_hdr), file_hdr, (0), buf, ahsstaticgettarusername(file_hdr), ahsstaticgettargroupname(file_hdr), type, vflag)
    else:
        sys.stderr.write( "%s: unrecognized file type in mode [%d] for file: %s\n"+ swlib_utilname_get()+ int((file_hdr.c_mode))+ ahsstaticgettarfilename(file_hdr))

    # Test line:  taru_digs_print(file_hdr->digsM, buf); 
    strob_strcat(buf, "\n")
    return 0


def taru_write_preview_line(taru, file_hdr):
    fd = 0
    flag = 0
    level = 0
    filename = '\0'
    buffer = None

    flag = 0
    if taru.preview_fd < 0 or taru.preview_level == tarc.TARU_PV_0:
        return

    fd = taru.preview_fd
    level = taru.preview_level
    buffer = taru.preview_buffer
    filename = ahsstaticgettarfilename(file_hdr)

    if os.isatty(fd):
        flag = ls_list_get_encoding_flag()
        ls_list_set_encoding_by_lang()


    if level >= tarc.TARU_PV_3:
        taru_print_tar_ls_list(buffer, file_hdr, lslistc.LS_LIST_VERBOSE_L2)
    elif level >= tarc.TARU_PV_2:
        taru_print_tar_ls_list(buffer, file_hdr, lslistc.LS_LIST_VERBOSE_L1)
    elif level >= tarc.TARU_PV_1:
        # strob_sprintf(buffer, 0, "%s\n", filename); 
        strob_strcpy(buffer, "")
        ls_list_safe_print_to_strob(filename, buffer, 0)
        strob_strcat(buffer, "\n")
    else:
        strob_strcpy(buffer, "")
    if strob_strlen(buffer) != 0:
        uxfio_write(fd, strob_str(buffer), strob_strlen(buffer))
    if os.isatty(fd):
        ls_list_set_encoding_flag(flag)


def taru_set_tar_header_policy(taru, user_format, p_arf_format):
    xx = 0
    if not p_arf_format:
        format = xx
    else:
        format = p_arf_format
    if not strcmp(user_format,"ustar"):
        #		
        # Default POSIX ustar format
        # GNU tar-1.15.1 --format=ustar
        #		 
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUTAR, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_LONG_LINKS, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_BE_LIKE_PAX, 0)
        format = af.arf_ustar
    elif not strcmp(user_format,"gnu") or not strcmp(user_format,"gnutar") or False:
        #		
        # same as GNU tar 1.15.1 --format=gnu
        #		 
        taru_set_tarheader_flag(taru,  tarc.TARU_TAR_GNU_OLDGNUPOSIX, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUTAR, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_BE_LIKE_PAX, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_LONG_LINKS, 1)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_BLOCKSIZE_B1, 1)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_GNUTAR, 1)
        format = af.arf_ustar
    elif not strcmp(user_format,"ustar.star"):
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUTAR, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUPOSIX, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_BE_LIKE_PAX, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_BE_LIKE_STAR, 1)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_BLOCKSIZE_B1, 1)
        format = af.arf_ustar
    elif not strcmp(user_format,"oldgnu") or False:
        #		
        # GNU tar-1.13.25 -b1  // default compilation
        #		 
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUTAR, 1)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_LONG_LINKS, 1)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_BLOCKSIZE_B1, 1)
        format = af.arf_ustar
    elif not strcmp(user_format,"ustar0"):
        #
        # GNU tar-1.13.25 --posix -b1
        #		 
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUTAR, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_LONG_LINKS, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_BE_LIKE_PAX, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_BLOCKSIZE_B1, 1)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUPOSIX, 1)
        format = af.arf_ustar
    elif not strcmp(user_format,"bsdpax3"):
        #		
        #  Emulate /bin/pax ustar format as found of some GNU and BSD systems.
        #		 
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_BLOCKSIZE_B1, 1)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_BE_LIKE_PAX, 0)
        format = af.arf_ustar
    elif not strcmp(user_format,"newc"):
        format = af.arf_newascii
    elif not strcmp(user_format,"crc"):
        format = af.arf_crcascii
    elif not strcmp(user_format,"odc"):
        format = af.arf_oldascii
    elif not strcmp(user_format,"pax") or not strcmp(user_format,"posix"):
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUPOSIX, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_OLDGNUTAR, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_GNU_LONG_LINKS, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_BE_LIKE_PAX, 0)
        taru_set_tarheader_flag(taru, tarc.TARU_TAR_PAXEXTHDR, 1)
        format = af.arf_ustar
    else:
        E_DEBUG2("%s: unrecognized format: %s\n", swlib_utilname_get()+ user_format)
        return -1
    return 0


def taru_check_devno_for_tar(file_hdr):
    if file_hdr.c_rdev_maj > 2097151: # & 07777777 != file_hdr->c_rdev_maj)
        return 1
    if file_hdr.c_rdev_min > 2097151: # & 07777777 != file_hdr->c_rdev_min)
        return 2
    return 0


def taru_check_devno_for_system(file_hdr):
    d = os.makedev(file_hdr.c_rdev_maj, file_hdr.c_rdev_min)
    if int(file_hdr.c_rdev_maj) != int(os.minor(d)):
        return 1
    if int(file_hdr.c_rdev_min) != int(os.minor(d)):
        return 2
    return 0

def taru_read_in_tar_header2(taru, file_hdr, in_des, fsource_buffer, eoa, tarheaderflags, fsource_buffer_len):
    p = None
    sys.stderr.write("")
    if taru:
        taru.read_buffer_pos = 0
    if taru and taru.do_record_header:
        taru.header_length = 0
    file_hdr.extHeader_usage_mask = 0
    taru_exattlist_init(file_hdr)
    ahsstaticsettarfilename(file_hdr, "")
    ahsstaticsetpaxlinkname(file_hdr, "")
    sys.stderr.write("fsource_buffer=%p"+ fsource_buffer)
    sys.stderr.write("fsource_buffer_len=%d"+ fsource_buffer_len)
    ret = taru.i_taru_read_in_tar_header2(file_hdr, in_des, fsource_buffer, eoa, tarheaderflags,
                                     fsource_buffer_len, 0)
    if file_hdr.extHeader_usage_mask:
        extlist = file_hdr.extattlist
        if extlist:
            index = 0
            while p == extlist[index]:
                taru_exatt_override_ustar(file_hdr, p)
                index += 1
    return ret

