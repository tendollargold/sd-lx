# phases.swremove.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

"""
    4.0 Execution Phase Systems Management: Distributed Software Administration
    swremove utility Page 115 - 120

    Check for extended options
    Extended Options
    The swremove utility supports the following extended options. The description in
    Chapter 3 applies.
"""

swremoveoptions={
    'autoselect_dependents': bool(False),
    'distribution_target_directory': "/var/spool/sw",
    'enforce_dependencies': bool(True),
    'enforce_scripts': bool(True),
    'installed_software_catalog': "/var/adm/sw/products",
    'logfile': "/var/adm/sw/swcopy.log",
    'loglevel': "1",
    'select_local': bool(True),
    'software': "",
    'targets': "",
    'verbose': "1"
}
def swremove_options():
    """Collection of options for swremove utility. :api"""
    swremoveoptions.update(globals())

# The swremove utility consists of three phases:
#  1. Selection Phase
#  2. Analysis Phase
#  3. Execution Phase

# Selection Phase

# As opposed to swinstall, software selections apply to the target distribution or
# installed_software.

# Each specified selection must pass the following checks. If a specification does not
# pass a check, the implementation defined error handling procedure is invoked.

#  · If the selection is not found, generate an event.
#    [SW_WARNING: SW_SELECTION_NOT_FOUND]

#  · If the selection is not found at that product directory, generate an event.
#    [SW_WARNING: SW_SELECTION_NOT_FOUND_RELATED]

#  · If a single version of the software is not uniquely identified from the product 
#    and product attributes specified, generate an event.
#    [SW_ERROR: SW_SELECTION_NOT_FOUND_AMBIG]

# Add any dependent software to the selection list if autoselect_dependents=true.

# Analysis Phase

# This section details the analysis phase. The analysis phase occurs before the 
# removing of files begins, and involves executing checks to determine whether or 
# not the removal should be attempted. No aspect of the target host environment is 
# modified, so canceling the removal after these operations has no negative effect.

# The target role makes the following checks:

#  · When removing installed software, execute vendor-supplied checkremove scripts to
#    perform product-specific checks of the target. If enforce_scripts=true and the 
#    checkremove script returns an error, an event is generated and the implementation
#    defined error handling procedure is invoked.
#    [SW_ERROR: SW_CHECK_SCRIPT_ERROR]

#    If the checkremove script returns an error and enforce_scripts=false, generate 
#    an event.
#    [SW_WARNING: SW_CHECK_SCRIPT_ERROR]

#    If the script had a warning, generate an event.
#    [SW_WARNING: SW_CHECK_SCRIPT_WARNING]

#    If the script has a return code of 3, generate an event and unselect the fileset 
#    (or all filesets in the product for a product level script).
#    [SW_NOTE: SW_CHECK_SCRIPT_EXCLUDE]

#  · Verify that the dependencies are met. The swremove utility does not remove a 
#    fileset if it is required by other filesets that have not been selected for 
#    removal or cannot be removed.

#    When enforce_dependencies=true and a non-selected fileset depends on a selected 
#    fileset, an event is generated and the implementation defined error handling 
#    procedure is invoked.
#    [SW_ERROR: SW_DEPENDENCY_NOT_MET]

#    When enforce_dependencies=false, generate an event.
#    [SW_WARNING: SW_DEPENDENCY_NOT_MET]

# If a software object has been specified for removal, and there is a bundle referring
# to that object that has not also been specified for removal, the behavior is 
# implementation defined. Likewise, if a fileset or subproduct object has been 
# specified for removal, and there is a subproduct referring to that object that has 
# not also been specified for removal, the behavior is implementation defined.

# Execution Phase

# For installed_software, the sequential relationship between the unconfigure, 
# preremove, and postremove scripts, and removing files for swremove is shown in the 
# following list. The unconfigure scripts are only run if the target directory is /.

#  1. Unconfigure each product:

#     If the fileset has been configured more than once, the unconfigure script must
#     unconfigure each instance.

#       a. Unconfigure each fileset in the product:

#            i. Run the unconfigure script for the fileset.

#           ii. Update the result of the script. Update the state of the fileset in 
#               the product to installed in the database for the installed_software 
#               object.

#       b. Run the unconfigure script for the product.

#  2. Remove each product:

#       a. Run the preremove script for the product.

#       b. Remove each fileset in the product:

#            i. Update the state of the fileset to transient in the catalog for the 
#               installed_software object.

#           ii. Run the preremove script for the fileset.

#          iii. Remove the files for the fileset.

#           iv. Run the postremove script for the fileset.

#            v. Update the results of the scripts. Update the state of the fileset to 
#               removed in the catalog for the installed_software object or remove the
#                catalog information for the fileset.

#       c. Run the postremove script for the product.

#       d. If the catalog information has been removed for all filesets in the 
#          product, an implementation can also remove the catalog information for the 
#          product and its contained subproducts.

#  3. Remove each bundle:

#       a. Remove the installed_software catalog information for the bundle

#  4. If the catalog information has been removed for all products and bundles in the 
#     installed_software object, an implementation can also remove the catalog 
#     information for the installed_software object.

# For each fileset that failed to be removed, the installed_software catalog 
# information is updated to the state corrupt.

#  · Executing Pre-remove Scripts

#    In this step of the execution phase, swremove executes the software preremove 
#    scripts.

#    — If a preremove script returns an error and "enforce_scripts=true", generate an 
#      event and invoke the implementation defined error handling procedures.  
#      [SW_ERROR: SW_PRE_SCRIPT_ERROR]

#    — If the preremove script returns an error and enforce_scripts=false, generate 
#      an event.
#      [SW_WARNING: SW_PRE_SCRIPT_ERROR]

#    — If a preremove script returns an warning, generate an event.
#      [SW_WARNING: SW_PRE_SCRIPT_WARNING]

#    When swremove is removing from a distribution, no scripts must be run.

#  · File Removing

#     In this step, swremove removes the files from the target. The target role 
#     attempts to remove each file from the target file system according to 
#     information obtained in the software_selections sent.

#     If swremove cannot remove a file (either because the file is busy [ETXTBSY], or 
#     for some other reason), the file name and the reason are logged so an 
#     administrator can take corrective action.
#     [SW_WARNING: SW_FILE_NOT_REMOVABLE]

#     If a filename is a symbolic link, the target is not removed. To achieve this 
#     behavior, the swremove utility handles symbolic links according to these rules:

#     — If a file was recorded in the catalog as a symbolic link to another file, and 
#       it is still a symbolic link on the file system, remove the symbolic link, but 
#       do not remove the target file.

#     — If a file was recorded in the catalog as a file, but exists as a symbolic link
#       on the file system, remove the symbolic link, but do not remove the target 
#       file.

#     — If the pathname to the file includes a symbolic link, this path is followed 
#       and the correct file is removed.

#     All files that are targets of symbolic links are removed when the fileset to 
#     which they belong is removed.

#  · Executing Post-remove Scripts

#    In this step of the execution phase, swremove executes software postremove 
#    scripts.

#    — If a postremove script returns an error and enforce_scripts=true, generate an 
#      event and invoke the implementation defined error handling procedures.
#      [SW_ERROR: SW_POST_SCRIPT_ERROR]

#    — If the postremove script returns an error and enforce_scripts=false, generate 
#      an event.
#      [SW_WARNING: SW_POST_SCRIPT_ERROR]

#    — If a postremove script returns an warning, generate an event.
#      [SW_WARNING: SW_POST_SCRIPT_WARNING]

#  · Kernel Reconfiguration

#    If the is_kernel attribute of the fileset is true, then a warning message to 
#    rebuild the kernel is displayed and also recorded in the log file. However, 
#    swremove does not modify any of the kernel generation files.

#  · Removing from a Distribution

#    The list of operations is simpler for removing filesets from a distribution than 
#    for installed_software.

#      1. Remove each product:

#           a. Remove each fileset in the product:

#               i. Update the state of the fileset to transient in the catalog for 
#                  the distribution.

#              ii. Remove the files for the fileset.

#             iii. Update the state of the fileset to removed in the catalog for the 
#                  distribution or remove the catalog information for the fileset.

#           b. If the catalog information has been removed for all filesets in the 
#              product, an implementation can also remove the catalog information for
#              the product and its contained subproducts. For each fileset that failed
#              to be removed, the distribution catalog information is updated to the 
#              state corrupt.

#      2. Remove each bundle:

#           a. Remove the distribution catalog information for the bundle.

#      3. If the catalog information has been removed for all products and bundles in 
#         the distribution object, an implementation can also remove the catalog 
#         information for the distribution object.

#  · Patches

#    Removal of the base fileset of a patch fileset via sw
#    remove will also result in removal of all patches to that fileset. Likewise when 
#    a particular base fileset is overwritten during a swinstall update operation, all
#    patches for that base are removed as well. Saved rollback data is also removed 
#    when the base fileset to which it applies is updated or removed from the system.

#    Removal of a patch automatically rolls back previous files, unless the rollback 
#    has been disabled as described for swinstall on page 90 or swmodify on page 108, 
#    or unless the base fileset is also being removed or updated. The patch fileset 
#    has the value of the saved_files_directory stored as an attribute, so that it can
#    correctly find those files even if multiple saved_files_directory values have
#    been used for various filesets.

#    Only patches with the state applied can be rolled back. If a patch with a state 
#    of committed or superseded is attempted to be removed, generate an event. If the
#    files for a patch do not exist in the saved_files_directory, or that directory no
#    longer exists, generate an event.
#    [SW_ERROR:SW_DEPENDENCY_NOT_MET]

#    An installed patch that has been superseded may not be rolled back unless the 
#    superseding patch is also rolled back.
