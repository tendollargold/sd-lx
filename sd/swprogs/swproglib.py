#  swproglib.py -
# Copyright (C) 2006 James H. Lowe, Jr.
# Copyright (C) 2023-2024 Paul Weber, conversion to Python
# COPYING TERMS AND CONDITIONS:
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#
import os
import io
import re

import math
import sys
import fnmatch
import pty
import tarfile
import stat
import time
import subprocess
from typing import Tuple

from sd.debug import E_DEBUG, E_DEBUG2
from sd.errors import SW_ERROR
from sd.swsuplib.misc.shcmd import SHCMD
from sd.swsuplib.misc.swgpg import swgpg_create, swgpg_determine_signature_status, swgpg_delete, \
    swgpg_get_package_signature_command, swgpg_get_package_signature
from sd.swsuplib.misc.swvarfs import swvarfs_opendup, swvarfs_get_next_dirent, swvarfs_dirent_reset, swvarfs_close
from sd.swsuplib.misc.swevents import swevent_code
from sd.swsuplib.versionid import swverid_open, swverid_get_alternate, swverid_disconnect_alternates
from sd.swcommon import sw_e_msg, swcc, swc_get_stderr_fd, sw_l_msg
from sd.swsuplib.atomicio import atomicio
from sd.swsuplib.misc.cstrings import strcmp, strchr
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swuts import swuts_read_from_events
from sd.swsuplib.strob import (STROB, strob_open, strob_close, strob_sprintf, strob_str, strob_strtok,
                               strob_strcpy, strob_set_length, strob_strstr, strob_strcat, STROB_DO_APPEND,
                               STROB_NO_APPEND, strob_strlen, strob_memset)
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.cplob import vplob_val, vplob_add, cplob_shallow_reset, vplob_open, cplob_val, vplob_close
from sd.swsuplib.misc.shlib import shlib_get_function_text_by_name
from sd.swsuplib.misc.swevents import ec, TEVENT, swevent_parse_attribute_event
from sd.swsuplib.misc.swextopt import swextopt_is_option_true, eOpts, swextopt_writeExtendedOptions_strob, get_opta_isc, \
    Ec
from sd.swsuplib.misc.swheader import (swheader_get_attribute, swheaderline_get_value, swheader_reset,
                                       swheader_get_next_object, swheader_store_state, swheader_set_current_offset,
                                       swheader_get_single_attribute_value, swheader_restore_state,
                                       swheader_get_attribute_list, swheader_state_create, swheader_state_delete)
from sd.swsuplib.misc.swicol import (icolc, swicol_set_master_alarm, swicol_rpsh_task_send_script2,
                                     swicol_rpsh_task_expect, swicol_clear_task_idstring,
                                     swicol_rpsh_get_event_status, swicol_show_events_to_fd,
                                     swicol_rpsh_get_event_message, swicol_print_events, swicol_get_master_alarm_status,
                                     swicol_set_task_idstring)
from sd.swsuplib.misc.swicat import swicatc, swicat_write_script_cases, swicat_isf_installed_software, \
    swicat_write_isc_script, swicat_squash_null_bytes, swicat_req_create, swicat_req_analyze, swicat_req_get_pre_result, \
    swicat_req_get_ex_result, swicat_req_print, swicat_req_delete
from sd.swsuplib.misc.swicat_e import swicatc, swicat_e_form_catalog_path
from sd.swsuplib.misc.swlib import (swlibc, SWLIB_INTERNAL, swlib_doif_writef, swlib_unix_dircat,
                                    swlib_squash_leading_dot_slash,
                                    swlib_check_clean_path, swlib_unix_dirtrunc_n, swlib_squash_all_leading_slash,
                                    SWLIB_ASSERT, swlib_pipe_pump,
                                    swlib_writef, SWLIB_FATAL, swlib_pump_amount, swlib_pad_amount, SW_ERROR_IMPL,
                                    swlib_dir_compare,
                                    swlib_squash_trailing_slash, swlib_compare_8859, swlib_is_sh_tainted_string,
                                    swlib_expand_escapes, swlib_open_memfd, swlib_synct_suck, swlib_append_synct_eof,
                                    swlib_close_memfd)
from sd.swsuplib.ls_list.ls_list import lslistc
from sd.swsuplib.swi.swi import (swic, swi_xfile_get_control_script_by_id, swi_package_get_product,
                                 swi_product_has_control_file, swi_product_get_control_script_by_tag,
                                 swi_fl_qsort_forward, swi_fl_get_path, swi_fl_get_type, swi_fl_is_volatile,
                                 SWI_internal_error, swi_com_check_clean_relative_path,
                                 swi_fl_set_is_volatile, swi_product_get_fileset, swi_xfile_get_control_script_by_tag,
                                 SWI, swi_xfile_set_state, swi_get_global_index_header, swi_examine_signature_blocks)
from sd.swsuplib.taru.ahs import ahsstaticgettarfilename, ahsstaticsettarfilename, ahs_get_tar_username, ahs_get_mode, ahs_get_tar_groupname
from sd.swsuplib.taru.etar import etar_write_trailer_blocks, Etar, etar_open, etar_emit_data_from_buffer
from sd.swsuplib.taru.filemode import swlib_filestring_to_mode
from sd.swsuplib.taru.cpiohdr import chc
from sd.swsuplib.taru.taru import af, tc, taru_make_header, taru_create, taru_set_tar_header_policy, taru_delete
from sd.swsuplib.taru.copyout import taru_process_copy_out
from sd.swsuplib.taru.xformat import xformat_get_ifd
from sd.swsuplib.uxfio import uxfio_get_fd_mem, uxfio_close

UCHAR_MAX = 255
class ProgLibConstants:
    D_ZERO = 0
    D_ONE = 1
    D_TWO = 2
    SCAR_ARRAY_LEN = 20
    CISF_ID_PRODUCT = D_ONE
    CISF_ID_FILESET = D_TWO
    SWP_RP_STATUS_NO_INTEGRITY = 54
    SWP_RP_STATUS_NO_GNU_TAR = 55
    SWP_RP_STATUS_NO_LOCK = 56
    SWP_RP_STATUS_NO_PERMISSION = 57
    SWLIST_PMODE_TEST1 = "test1" # provides a scratch test area for distributions
    SWLIST_PMODE_DEP1 = "dep1" # dependency check format that is used internally by swinstall
    SWLIST_PMODE_PROD = "products"
    SWLIST_PMODE_DIR = "directory"
    SWLIST_PMODE_FILE = "files"
    SWLIST_PMODE_CAT = "list_catalog"
    SWLIST_PMODE_INDEX = "INDEX" # POSIX -v mode
    SWLIST_PMODE_ATT = "ATT" # POSIX -a mode
    SWLIST_PMODE_UTS_ATT = "UTS_ATT" # POSIX -a mode for uts attributes only

swplc = ProgLibConstants()
g_script_array = [None for _ in range(swplc.SCAR_ARRAY_LEN + 1)]
class CISFBA:
    """
      CISFBA base array, this should eventually replace
      the array g_script_array[SCAR_ARRAY_LEN+1] of (*SCAR) objects
    """
    def __init__(self):
        self.base_array = None


class CISF_BASE:

    def __init__(self):
        self.typeid = 0
        self.ixfile = None
        self.at = None
        self.cf_index = 0

class CISF_PRODUCT:
    """
    CISF_PRODUCT: this struct maps the location in INSTALLED with the product_index
    wi->swi_pkg->swi_co[i] and it also stores the objects.  It
    stores the list of CISF_FILESET pointers associated with this product

    FIXME, this data structure assumes that there is only one product being operated on
    (due to CISFBA * cba of which there only should be one
    """
    def __init__(self):
        self.cisf_base = CISF_BASE()
        self.swi = None
        self.product = None
        self.isets = None
        self.cba = None

class CISF_FILESET:
    """
    CISF_FILESET: this struct associates the location in INSTALLED (or INDEX)
    SWHEADER object with the corresponding fileset object in (SWI*),
    namely the pointer held in swi->swi_pkg->swi_co[i]->swi_co[ii]
    """

    def __init__(self):
        self.cisf_base = CISF_BASE()
class SCAR:
    """
    The SCAR struct is deprecated, it will eventually be replaced
    with the CISF* data structures.  The CISF data structures model
    selections and installed software (as opposed to assuming all
    (or exactly one) filesets and exactly one product
    """
    def __init__(self):
        self.tag = '\0'
        self.tagspec = '\0'
        self.script = None


def select_error_codes(script_tag, event_error_value, event_warning_value):

    #
    # script_tag is either preinstall, postinstall, configure, or
    # postremove, preremove
    #

    if strcmp(script_tag,  swc.SW_A_postinstall) == 0 or strcmp(script_tag,  swc.SW_A_postremove) == 0 or False:
        event_error_value = ec.SW_POST_SCRIPT_ERROR
        event_warning_value = ec.SW_POST_SCRIPT_WARNING
    elif strcmp(script_tag,  swc.SW_A_preinstall) == 0 or strcmp(script_tag,  swc.SW_A_preremove) == 0 or False:
        event_error_value = ec.SW_PRE_SCRIPT_ERROR
        event_warning_value = ec.SW_PRE_SCRIPT_WARNING
    elif strcmp(script_tag,  swc.SW_A_configure) == 0 or strcmp(script_tag,  swc.SW_A_unconfigure) == 0 or False:
        event_error_value = ec.SW_CONFIGURE_ERROR
        event_warning_value = ec.SW_CONFIGURE_WARNING
    else:
        event_error_value = ec.SW_CONFIGURE_ERROR
        event_warning_value = ec.SW_CONFIGURE_ERROR
        SWLIB_INTERNAL("")


def make_padding_for_login_shell(blocks, buf):
    for k in range(0, blocks):
        for j in range(0, 8):
            for i in range(0, 7):
                strob_sprintf(buf, STROB_DO_APPEND, "########")
            strob_sprintf(buf, STROB_DO_APPEND, "#######\n")


def environ_exclude(varname):
    exclude_list = [":", "_", "PATH", "LD_LIBRARY_PATH", "IFS", "LD_PRELOAD",
                    "SHELL", "HOME", "LANG", "USER", "TMPDIR", "TERM", "TERMCAP", "TZ", "MAIL", "XAUTHORITY",
                    "LOGNAME", "HOME", "BASH_ENV", "CDPATH", "DISPLAY", "HOSTNAME", "HISTSIZE",
                    "SSH_ASKPASS", "SSH_AUTH_SOCK", "SSH_CONNECTION", "SSH_ORIGINAL_COMMAND", "SSH_TTY",
                    "SSH_AGENT_PID", "SSH_AGENT_PID",
                    "GPG_AGENT_INFO", "PWD", "LS_COLORS", "WINDOWID",
                    "G_BROKEN_FILENAMES", "LESSOPEN", "SHLVL", "ENV", "OLDPWD",
                    ""]
    pattern_list = [":CVS*", ":SSH*", ":JMM*", ":GPG*", ":JX_*"]
    if ":" in varname:
        return 1
    if varname in exclude_list:
        return 1
    for pattern in pattern_list:
        if fnmatch.fnmatch(varname, pattern):
            return 1
    return 0


def abort_script(swicol, G, fd):
    swicol_set_master_alarm(swicol)
    swpl_send_abort(swicol, fd, G.g_swi_event_fd, "")

def swpl_send_abort(swicol, ofd, event_fd, msgtag):
    ret = swpl_send_null_task(swicol, ofd, event_fd, msgtag, swc.SW_ERROR)
    return ret

def swpl_send_null_task(swicol, ofd, event_fd, msgtag, retcode):
    tmp = strob_open(10)
    strob_sprintf(tmp, 0, "sw_retval=%d", retcode)
    ret = swpl_send_null_task2(swicol, ofd, event_fd, msgtag, strob_str(tmp))
    strob_close(tmp)
    return ret


def swpl_send_null_task2(swicol, ofd, event_fd, msgtag, status_expression):
    ts_msg = strob_open(10)
    tmp = strob_open(10)

    strob_strcat(ts_msg, msgtag)
    E_DEBUG("")
    #
    # Here is the minimal task scriptlet that throws an error
    #
    strob_sprintf(tmp, 0, swlibc.swlibc.CSHID+"# sw_retval=%%d\n" + "%s\n" + "sleep 0\n" + "dd bs=512 count=1 of=/dev/null 2>/dev/null\n" + "case $sw_retval in 0) exit 0 ;; *) exit $sw_retval;; esac\n", status_expression)
    E_DEBUG("")

    #
    # Send the script into stdin of the POSIX shell
    # invoked with the '-s' option.
    #
    E_DEBUG("")
    ret = swicol_rpsh_task_send_script2(swicol, ofd, 512, ".", strob_str(tmp), strob_str(ts_msg))
    E_DEBUG2("ret=%d", ret)

    if ret == 0:
        #
        #  Now send the stdin payload which must be
        #  exactly stdin_file_size bytes long.
        #

        #
        # Send the payload
        #
        ret = etar_write_trailer_blocks(ofd, 1)
        if ret < 0:
            sys.stderr.write("%s: swpl_get_utsname_attributes(): etar_write_trailer_blocks(): ret=%d\n"+ swlib_utilname_get()+ ret)

        #
        # Reap the events from the event pipe.
        #
        E_DEBUG2("ret=%d",ret)
        if ret > 0:
            ret = swicol_rpsh_task_expect(swicol, event_fd, icolc.SWICOL_TL_12)

        E_DEBUG2("ret=%d",ret)
        # swicol_show_events_to_fd(swicol, STDERR_FILENO, -1);
    strob_close(ts_msg)
    strob_close(tmp)
    swicol_clear_task_idstring(swicol)
    E_DEBUG2("ret=%d",ret)
    return ret


def set_ls_list_flags():
    ls_verbose = 0
    ls_verbose &= ~(lslistc.LS_LIST_VERBOSE_WITH_MD5 | lslistc.LS_LIST_VERBOSE_WITH_SHA1 | lslistc.LS_LIST_VERBOSE_WITH_SHA512)
    ls_verbose &= ~(lslistc.LS_LIST_VERBOSE_WITH_SIZE | lslistc.LS_LIST_VERBOSE_WITH_ALL_DATES)
    return ls_verbose


def merge_name_id(ls_verbose, available_attributes):
    x = 0
    x = ls_verbose
    if (available_attributes & lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_IDS) != 0:
        x |= lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_IDS
    if (available_attributes & lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_NAMES) != 0:
        x |= lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_NAMES
    ls_verbose = x


def apply_location_to_path(buf, file_hdr, location):
    strob_strcpy(buf, location)
    swlib_squash_leading_dot_slash(strob_str(buf))
    swlib_squash_all_leading_slash(strob_str(buf))
    swlib_unix_dircat(buf, ahsstaticgettarfilename(file_hdr))
    ahsstaticsettarfilename(file_hdr, strob_str(buf))


def determine_ls_list_flags(attr, file_hdr, check_contents):
    ls_verbose = 0
    E_DEBUG("")

    if (attr & chc.TARU_UM_UID) != 0 and (attr & chc.TARU_UM_OWNER) != 0:
        E_DEBUG("Has owner and uid")
        ls_verbose |= lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_IDS
        ls_verbose |= lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_NAMES
    elif (attr & chc.TARU_UM_UID) != 0:
        E_DEBUG("Has uid only")
        ls_verbose |= lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_IDS
        ls_verbose &= ~lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_NAMES
    elif (attr & chc.TARU_UM_OWNER) != 0:
        E_DEBUG("Has owner only")
        ls_verbose &= ~lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_IDS
        ls_verbose |= lslistc.LS_LIST_VERBOSE_WITH_SYSTEM_NAMES

    if check_contents == 0:
        ls_verbose &= ~(lslistc.LS_LIST_VERBOSE_WITH_MD5 | lslistc.LS_LIST_VERBOSE_WITH_SHA1 | lslistc.LS_LIST_VERBOSE_WITH_SHA512)
        ls_verbose &= ~lslistc.LS_LIST_VERBOSE_WITH_SIZE

    digs = file_hdr.digs
    if digs:
        if digs.do_md5 == chc.DIGS_ENABLE_ON:
            E_DEBUG("Turning on md5")
            ls_verbose |= lslistc.LS_LIST_VERBOSE_WITH_MD5
        if digs.do_sha1 == chc.DIGS_ENABLE_ON:
            E_DEBUG("Turning on sha1")
            ls_verbose |= lslistc.LS_LIST_VERBOSE_WITH_SHA1
        if digs.do_sha512 == chc.DIGS_ENABLE_ON:
            E_DEBUG("Turning on sha512")
            ls_verbose |= lslistc.LS_LIST_VERBOSE_WITH_SHA512
    else:
        E_DEBUG("Turning off digests")
        ls_verbose &= ~(lslistc.LS_LIST_VERBOSE_WITH_MD5 | lslistc.LS_LIST_VERBOSE_WITH_SHA1 | lslistc.LS_LIST_VERBOSE_WITH_SHA512)
    return ls_verbose


def determine_here_document_stop_word_bystr(here_payload, stopbuf):
    n = 0
    L_tmpnam = 1
    E_DEBUG("")
    loop_condition = True
    while loop_condition:
        strob_set_length(stopbuf, L_tmpnam + 1) #L_tmpnam is not set anywhere - Paul W
    ret_t = strob_str(stopbuf)
    if ret_t is None:
        return 2
    ret = strob_strstr(here_payload, strob_str(stopbuf))
    if ret is None:
        E_DEBUG("")
        return 0  # unique character sequence found
    n += 1
    loop_condition = ret is None and n < 10
    E_DEBUG("")
    return 1  # unique character sequence not found


def determine_here_document_stop_word(fl, stopbuf):
    payload = strob_open(100)
    swpl_print_file_list_to_buf(fl, payload)
    ret = determine_here_document_stop_word_bystr(payload, stopbuf)
    strob_close(payload)
    return ret

def swpl_print_file_list_to_buf(fl, buf):
    for path in fl.ar:
        buf.append(path + '\n')


def swpl_bashin_posixsh(buf):
    prog = swlib_utilname_get()
    if strcmp(prog, "swremove") == 0:
        prog = "/"
        swbis = "/"
    else:
        swbis = "/_swbis"

    buf = ("{\n"
          "export XXX_PKG\n"
          "export XXX_PGM\n"
          "export XXX_SH\n"
          "export PATH\n"
          "XXX_PKG=\"%s\"\n"
          "XXX_PGM=\"%s\"\n"
          "XPATH=`getconf PATH`\n"
          "case $? in\n"
          "0)\n"
          "PATH=\"$XPATH\"\n"
          ";;\n"
          "*)\n"
          "PATH=/bin:/usr/bin\n"
          "echo \"swbis: $XXX_PGM: getconf not found (required by --shell-command=posix option)\" 1>&2\n"
          "echo \"swbis: $XXX_PGM: fatal, exiting with status 125\" 1>&2\n"
          "exit 125\n"
          ";;\n"
          "esac\n"
          "xxx_missing_which() {\n"
          "# Prefix: xxabb\n"
          "xxabb_pgm=\"$1\"\n"
          "xxabb_name=`which $xxabb_pgm 2>/dev/null`\n"
          "test -f \"$xxabb_name\" -o -h \"$xxabb_name\"\n"
          "case \"$?\" in\n"
          "0) echo \"$xxabb_name\"; return 0; ;;\n"
          "*) echo \"/\"; return 1; ;;\n"
          "esac\n"
          "return 0\n"
          "}\n"
          "XXX_SH=`xxx_missing_which sh`\n"
          "test -x \"$XXX_SH\" &&\n"
          "{\n"
          "\"$XXX_SH\" -s 1>/dev/null 2>&1 <<HERE\n"
          "(read A; exit 0);\n"
          "/\n"
          "HERE\n"
          "case $? in\n"
          "0)\n"
          "exec \"$XXX_SH\" -s  \"$XXX_PKG\" /\"_$XXX_PGM\" / / PSH=\"\\\"\"$XXX_SH\" -s\\\"\"\n"
          "exit 121\n"
          ";;\n"
          "esac\n"
          "}\n"
          "echo \"swbis: $XXX_PGM: a POSIX shell in PATH=getconf PATH is not suitable on host `hostname`\" 1>&2\n"
          "echo \"swbis: $XXX_PGM: fatal, exiting with status 124\" 1>&2\n"
          "exit 124\n"
          "}\n").format(swbis, prog)

    make_padding_for_login_shell(16, buf)

def swpl_bashin_testsh(buf, shell):
    binsh = construct_binsh_name(shell)

    prog = swlib_utilname_get()
    if prog == "swremove":
        prog = "/"
        swbis = "/"
    else:
        swbis = "/_swbis"

    buf.append(
        "{\n"
        "export XXX_PKG\n"
        "export XXX_PGM\n"
        "export XXX_SH\n"
        "XXX_PKG=\"%s\"\n"
        "XXX_PGM=\"%s\"\n"
        "XXX_SH=\"%s\"\n"
        "test -x $XXX_SH &&\n"
        "{\n"
        "$XXX_SH -s 1>/dev/null 2>&1 <<HERE\n"
        "(read A; exit 0);\n"
        "/\n"
        "HERE\n"
        "case $? in\n"
        "0)\n"
        "exec $XXX_SH -s  $XXX_PKG /_$XXX_PGM / / PSH=\"\\\"\"$XXX_SH\" -s\\\"\"\n"
        "exit 121\n"
        ";;\n"
        "esac\n"
        "}\n"
        "echo \"swbis: $XXX_PGM: the specified shell is not suitable on host `hostname`\" 1>&2\n"
        "echo \"swbis: $XXX_PGM: fatal, exiting with status 126\" 1>&2\n"
        "exit 126\n"
        "}\n",
        swbis, prog, binsh
    )

    make_padding_for_login_shell(16, buf)

def write_control_script_code_fragment(G, swi, script, swspec_tags, buf):
    tmp = STROB()
    tmp2 = STROB()
    tmp3 = STROB()
    tmp.str_ = bytearray(100)
    tmp2.str_ = bytearray(100)
    tmp3.str_ = bytearray(100)
    tmp.str_ = ("%s %s" % (swspec_tags, script.base.b_tag)).encode()
    tmp2.str_ = tmp.str_
    if G.g_opt_preview:
        tmp2.str_ += (": " + ec.SWEVENT_ATT_STATUS_PREVIEW).encode()
    else:
        tmp2.str_ += (": " + ec.SWEVENT_STATUS_PFX + "$sw_retval").encode()
    tmp3.str_ = (swc.SW_A_SCRIPT_ID + "=%d" % script.sid).encode()
    buf.str_ = (swlibc.swlibc.CSHID +
        "# Start of swpl_write_control_script_code_fragment\n" +
        "    cscf_opt_preview=\"%d\"\n" +
        "    cd \"%s\" || exit 44\n" +
        "    cd \"%s\" || exit 45\n" +
        "    script_control_val=$?\n" +
        "    case \"$swbis_ignore_scripts\" in yes) script_control_val=_ignore_;; esac\n" +
        "    case \"$cscf_opt_preview\" in 1) script_control_val=_preview_;; esac\n" +
        "    case $script_control_val in  # Case_swproglib.c_002\n" +
        "        0)\n" +
        "            %s\n" +
        "            %s\n" +
        "            " + swlibc.SW_PGM_SH + " " + swlibc.SW_A_CONTROL_SH + " \"%s\" \"%s\"\n" +
        "            sw_retval=$?\n" +
        "            %s\n" +
        "            ;;\n" +
        "        _preview_)\n" +
        "            %s\n" +
        "            %s\n" +
        "            sw_retval=0\n" +
        "            %s\n" +
        "            ;;\n" +
        "        _ignore_)\n" +
        "            sw_retval=0\n" +
        "            ;;\n" +
        "        *)\n" +
        "            sw_retval=" + swlibc.SW_STATUS_COMMAND_NOT_FOUND + "\n" +
        "            ;;\n" +
        "    esac  # Case_swproglib.c_002\n" +
        "# End of swpl_write_control_script_code_fragment\n") % (
        G.g_opt_preview,
        swi.swi_pkg.target_path,
        swi.swi_pkg.catalog_entry,
        TEVENT(2, swi.verbose, swlibc.SW_CONTROL_SCRIPT_BEGINS, tmp.str_),
        TEVENT(2, swi.verbose, ec.SWI_MSG, tmp3.str_),
        swspec_tags,
        script.base.b_tag,
        TEVENT(2, swi.verbose, swlibc.SW_CONTROL_SCRIPT_ENDS, tmp2.str_),
        TEVENT(2, swi.verbose, swlibc.SW_CONTROL_SCRIPT_BEGINS, tmp.str_),
        TEVENT(2, swi.verbose, ec.SWI_MSG, tmp3.str_),
        TEVENT(2, swi.verbose, swlibc.SW_CONTROL_SCRIPT_ENDS, tmp2.str_)
    )
    return 0



def construct_script_cases(G, buf, swi, script, tagspec, script_tag, error_event_value, warning_event_value):
    tmp2 = strob_open(48)
    tmp3 = strob_open(48)
    opta = swi.opta
    assert script is not None

    #
    # write main script fragment
    #
    E_DEBUG("")

    ret = write_control_script_code_fragment(G, swi, script, tagspec, buf)
    assert ret == 0

    #
    # write some shell code that monitors
    # return status of the script
    #

    if False and strcmp(swlib_utilname_get(), swlibc.SW_UTN_CONFIG) == 0:
        # disable this
        #		 enforce_script will be used by swconfig in this
        #		   implementation

        #		 the enforce_scripts option is not used by swconfig
        #		   therefore avoid using it if the utility is 'swconfig'
        script_retcode = swlibc.SW_WARNING
        swi_retcode = 0
    else:
        E_DEBUG("")
        if swextopt_is_option_true(eOpts.SW_E_enforce_scripts, opta):
            E_DEBUG("SW_E_enforce_scripts is true")
            script_retcode = swlibc.SW_ERROR
            swi_retcode = 1
        else:
            E_DEBUG("SW_E_enforce_scripts is false")
            script_retcode = swlibc.SW_WARNING
            swi_retcode = 0

    strob_sprintf(tmp2, 0, "%s: status=$sw_retval", script_tag)
    strob_sprintf(tmp3, 0, "%s: status=$script_retval", script_tag)

    strob_sprintf(buf, STROB_DO_APPEND,
                  swlibc.CSHID +
                  "# Start of construct_script_cases\n"
        "    case \"$sw_retval\" in\n"
        "        " + swlibc.SW_STATUS_COMMAND_NOT_FOUND +")\n"
        "            # Special return code of control.sh which means\n"
        "            # no script for the specified control_script tag\n"
        "            sw_retval=0\n"
        "            ;;\n"
        "        %d)\n"
        "            sw_retval=0\n"
        "            ;;\n"
        "        %d)\n"
        "            %s\n"
        "            sw_retval=0\n"
        "            ;;\n"
        "        %d)\n"
        "            script_retval=%d\n"
        "            %s\n"
        "            sw_retval=\"%d\"\n"
        "            ;;\n"
        "        *)\n"
        "            sw_retval=1\n"
        "            ;;\n"
        "    esac\n"
        "# End of construct_script_cases\n",
                  swlibc.SW_SUCCESS,
                  swlibc.SW_WARNING,
                  TEVENT(2, swi.verbose, warning_event_value, strob_str(tmp2)),
                  swlibc.SW_ERROR,
                  script_retcode,
                  TEVENT(2, swi.verbose, error_event_value, strob_str(tmp3)),
                  swi_retcode
                  )        
    strob_close(tmp2)
    strob_close(tmp3)
    E_DEBUG("")
    return 0


def form_entry_names(G, e, iscpath, tmp_epath, tmp_depath):
    E_DEBUG("Entering")
    E_DEBUG2("iscpath=[%s]", iscpath)
    s_ret1 = swicat_e_form_catalog_path(e, tmp_epath, iscpath, swicatc.SWICAT_ACTIVE_ENTRY)
    epath = strob_str(tmp_epath)
    E_DEBUG2("epath=[%s]", epath)

    s_ret2 = swicat_e_form_catalog_path(e, tmp_depath, iscpath, swicatc.WICAT_DEACTIVE_ENTRY)
    depath = strob_str(tmp_depath)
    E_DEBUG2("dpath=[%s]", depath)

    if swlib_check_clean_path(epath) != 0:
        sw_e_msg(G, "tainted path name: %s\n", epath)
        strob_strcpy(tmp_epath, "")
        s_ret1 = None

    if swlib_check_clean_path(depath) != 0:
        sw_e_msg(G, "tainted path name: %s\n", depath)
        strob_strcpy(tmp_depath, "")
        s_ret2 = None

    if s_ret1 is None or s_ret2 is None:
        return -1

    return 0


def common_remove_catalog_entry(G, swicol, ofd, epath, depath, task_id):
    btmp = ''
    dir2 = ''
    retval = 0
    ret = 0
    retval = 0
    btmp = ''
    dir2 = ''
    btmp = ''
    dir2 = ''

    strob_strcpy(dir2, epath)
    swlib_unix_dirtrunc_n(dir2, 2)

    if task_id == 'SWBIS_TS_remove_catalog_entry':
        btmp += f"       # Remove all old catalog entries\n"
        btmp += f"       (cd \"{dir2}\" && find . -type d -name '_[0-9]*' -exec rm -fr {{}} \\; 2>/dev/null ) </dev/null \n"
    btmp += f"       rm -fr \"{depath}\" 1>&2\n"
    btmp += f"       mv -f \"{epath}\" \"{depath}\" 1>&2\n"
    btmp += f"       sw_retval=$?\n"
    btmp += f"       (cd \"{dir2}\" && rmdir * 2>/dev/null ) </dev/null \n"
    btmp += f"       case $sw_retval in 0) ;; *) sw_retval=2 ;; esac\n"
    btmp += f"       dd count=1 bs=512 of=/dev/null 2>/dev/null\n"
    ret = swicol_rpsh_task_send_script2(swicol, ofd, 512, swicol.targetpathM, btmp, task_id)
    if ret < 0:
        retval += 1
        return retval

    ret = etar_write_trailer_blocks(None, ofd)
    if ret < 0:
        retval += 1
        # goto error_out
        return retval

    ret = swicol_rpsh_task_expect(swicol, G.g_swi_event_fd, icolc.SWICOL_TL_9)

    if ret < 0:
        retval = ret
        SWLIB_INTERNAL("")
        return retval

    if ret > 0:
        retval = ret
    return retval


def alter_catalog_entry(remove_restore, G, swicol, e, ofd, iscpath):
    retval = 0
    tmp = strob_open(32)
    tmp2 = strob_open(32)

    ret = form_entry_names(G, e, iscpath, tmp, tmp2)
    if ret != 0:
        return 1
    epath = tmp.str_
    depath = tmp2.str_
    if remove_restore == 0:
        ret = common_remove_catalog_entry(G, swicol, ofd, epath, depath, "SWBIS_TS_remove_catalog_entry")
    else:
        ret = common_remove_catalog_entry(G, swicol, ofd, depath, epath, "SWBIS_TS_restore_catalog_entry")
    if ret:
        return 2
    return retval


def parse_ls_ld_output(swi, ls_output):
    value = '\0'
    attribute = '\0'


    #
    #	The line to parse looks something like this
    #
    #	swinstall: swicol: 315:ls_ld=drwxr-x--- jhl/other 0 2008-10-30 20:46 var/lib/swbis/catalog/
    #

    E_DEBUG2("ls output = [%s]", ls_output)
    ret = 0
    tmp = strob_open(10)
    line = strob_strtok(tmp, ls_output, "\r\n")
    while line != '\0':
        ret = swevent_parse_attribute_event(line, attribute, value)
        if ret == 0:
            E_DEBUG("")
            if strcmp(attribute, "ls_ld") == 0:
                """
                 fprintf(stderr, "attribute=[%s]\n", attribute);
                 fprintf(stderr, "value=[%s]\n", value);
                
                Now parse the value to get the perms, owner, and group
                The value looks like
                GNU tar:  drwxr-x--- jhl/other 0 2008-10-30 20:46 var/lib/swbis/catalog/
                					--or--
                STAR:     0 drwxr-s---  jhl/other Dec 14 20:53 2010 var/lib/swbis/catalog/
                
                bsdtar:   drwxr-s---  0  jhl  other  0 Dec 14 20:53 2010 var/lib/swbis/catalog/
                """
                E_DEBUG("")

                E_DEBUG2("value=[%s]", value)
                # Step over white space and numbers, 'star' places the size first
                # in  ``tar tvf -`` listing
                E_DEBUG("")
                xp = value
                while xp != '\0' and xp != '\0' and str(int * xp).isspace() is not None:
                    xp += 1
                while xp != '\0' and xp != '\0' and str(int * xp).isdigit() is not None:
                    xp += 1
                value = xp
                E_DEBUG("")

                s_mode = strob_strtok(tmp, value, " ")
                if (not s_mode) != '\0':
                    return -1
                s = strob_strtok(tmp, None, " ")
                if (not s) != '\0':
                    return -2
                owner = s
                E_DEBUG("")

                # In bsdtar this could be a single digit (link count?)
                if len(s) == 1 and str(ord(s)).isdigit():
                    # Step over the number
                    s = strob_strtok(tmp, None, " ")
                    owner = s
                E_DEBUG("")

                s = strchr(s, '/')
                if (not s) != '\0':
                    # must have space separated owner and group.
                    # bsdtar does this
                    s = strob_strtok(tmp, None, " ")
                    if (not s) != '\0':
                        return -4
                    group = s
                else:
                    s = '\0'
                    s += 1
                    group = s

                mode = swlib_filestring_to_mode(s_mode, 0)

                if len(owner) != 0 and len(group) != 0 and mode > 0o666:
                    E_DEBUG2("owner=%s", owner)
                    E_DEBUG2("group=%s", group)
                    E_DEBUG2("mode=%s", s_mode)
                    swi.swi_pkg.installed_catalog_owner = owner
                    swi.swi_pkg.installed_catalog_group = group
                    swi.swi_pkg.installed_catalog_mode = mode
                else:
                    E_DEBUG2("FAILED:  owner=%s", owner)
                    E_DEBUG2("FAILED:  group=%s", group)
                    E_DEBUG2("FAILED:  mode=%s", s_mode)
                    return -5

                # Test point
                # taru_mode_to_chars(mode, buf, sizeof(buf), '\0')
                # fprintf(stderr, "[%s] [%s] [%s] [%s]\n", owner, group, s_mode, buf)
                #
                ret = 0
                break
            else:
                E_DEBUG("")
                sys.stderr.write("bad message: %s\n" % attribute)
                ret = -1
                break
        else:
            # this happens
            E_DEBUG("")
            ret = 0
        E_DEBUG("")
        line = strob_strtok(tmp, None, "\r\n")
    E_DEBUG("")
    strob_close(tmp)
    return ret


def i_construct_analysis_script(G, script_name, buf, swi, p_script):
    opta = swi.opta

    prod = swi_package_get_product(swi.swi_pkg, 0)
    if swi_product_has_control_file(prod, script_name):
        buf.str_ = "       cd \"%s\"\n" % swi.swi_pkg.target_pat

        #
        # Write the code for the check script
        #
        script = swi_product_get_control_script_by_tag(prod, script_name)
        SWLIB_ASSERT(script is not None)
        ret = write_control_script_code_fragment(G, swi, script, prod.p_baseM.b_tag, buf)
        SWLIB_ASSERT(ret == 0)
        #
        # write some shell code that monitors
        # return status of the check script
        #

        if swextopt_is_option_true(swlibc.SW_E_enforce_scripts, opta):
            checkscript_retcode = SW_ERROR
            swi_retcode = 1
        else:
            checkscript_retcode = swlibc.SW_WARNING
            swi_retcode = 0

        if G.g_force:
            check_def_retcode = 0
        else:
            check_def_retcode = swlibc.SW_DESELECT

        p_script[0].copy_from(script)
        buf.str_ = (
                "case \"$sw_retval\" in\n"
                "       %d)\n"
                "               sw_retval=0\n"
                "               ;;\n"
                "       %d)\n"
                "               sw_retval=0\n"
                "               %s\n"
                "               ;;\n"
                "       %d)\n"
                "               script_retval=%d\n"
                "               sw_retval=%d\n"
                "               %s\n"
                "               sw_retval=0\n"
                "               ;;\n"
                "       %d)\n"
                "               script_retval=%d\n"
                "               sw_retval=%d\n"
                "               %s\n"
                "               sw_retval=0\n"
                "               ;;\n"
                "       *)\n"
                "               sw_retval=4\n"
                "               ;;\n"
                "esac\n"
                "dd of=/dev/null 2>/dev/null\n" %
                (
                    swlibc.SW_SUCCESS,
                    swlibc.SW_WARNING,
                    TEVENT(2, (swi.verbose), swlibc.SW_CHECK_SCRIPT_WARNING, "status=$sw_retval"),
                    SW_ERROR,
                    checkscript_retcode,
                    swi_retcode,
                    TEVENT(2, (swi.verbose), swlibc.SW_CHECK_SCRIPT_ERROR, "status=$script_retval"),
                    swlibc.SW_DESELECT,
                    swlibc.SW_DESELECT,
                    check_def_retcode,
                    TEVENT(2, (swi.verbose), swlibc.SW_CHECK_SCRIPT_EXCLUDE, "status=$sw_retval")
                )
        )
    else:
        # p_script = None
        buf.str_ = "sw_retval=0\n"
    return 0

def construct_binsh_name(shell):
    if strcmp(shell, swlibc.SH_A_sh) == 0:
        return swlibc.SWBIS_PGM_BIN_SH
    elif strcmp(shell, swlibc.SH_A_bash) == 0:
        return swlibc.SWBIS_PGM_BIN_BASH
    elif strcmp(shell, swlibc.SH_A_ksh) == 0:
        return swlibc.SWBIS_PGM_BIN_KSH
    elif strcmp(shell, swlibc.SH_A_mksh) == 0:
        return swlibc.SWBIS_PGM_BIN_MKSH
    else:
        print(f"{swlib_utilname_get()}: invalid selection in construct_binsh_name(), defaulting to sh", file=sys.stderr)
        return swlibc.SWBIS_PGM_BIN_SH


def swpl_write_chdir_catalog_fragment(tmp, catalog_path, id_str):
    tmp.str_ = f"""
        cd "{catalog_path}"
        case "$?" in
            0)
            ;;
            *)
            echo error: cd "{catalog_path}" failed for task "{id_str}" 1>&2
            exit 1
            ;;
        esac
    """

def swpl_set_detected_catalog_perms(swi, etar, tar_type):
    if swi.swi_pkg.installed_catalog_owner:
        Etar.etar_set_uname(etar, swi.swi_pkg.installed_catalog_owner)
    if swi.swi_pkg.installed_catalog_group:
        Etar.etar_set_gname(etar, swi.swi_pkg.installed_catalog_group)
    mode = swi.swi_pkg.installed_catalog_mode
    if mode > 0:
        if tar_type == tarfile.REGTYPE:
            mode &= ~(stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH | stat.S_ISVTX | stat.S_ISUID | stat.S_ISGID)
        Etar.etar_set_mode_ul(etar, mode)


def swpl_init_header_root(etar):
    Etar.etar_set_typeflag(etar, tarfile.REGTYPE)
    Etar.etar_set_mode_ul(etar, 0o640)
    Etar.etar_set_uid(etar, 0)
    Etar.etar_set_gid(etar, 0)
    Etar.etar_set_uname(etar, "root")
    Etar.etar_set_gname(etar, "root")


def swpl_scary_init_script_array():
    for i in range(0, swplc.SCAR_ARRAY_LEN):
        g_script_array[i] = None


def swpl2_find_by_id(id, bav, bpp):

    v = bav.base_array
    i = 0
    while (b := vplob_val(v, i)):
        i += 1
        E_DEBUG2("index=%d", i)
        E_DEBUG2("id = %d", id)
        sc = swi_xfile_get_control_script_by_id(b.ixfile, id)
        if sc:
            E_DEBUG2("Found id = %d", id)
            if bpp:
                bpp[0].copy_from(b)
            return sc
    i += 1
    E_DEBUG("Returning NULL")
    if bpp:
        bpp[0] = None
    return None


def swpl_get_script_array():
    return g_script_array


def swpl_scary_create(tag, tagspec, script):
    scary = SCAR()
    if scary is None:
        return scary
    scary.tag = tag # e.g. postinstall or preinstall
    scary.tagspec = tagspec # <product_tag> or <product_tag>.<fileset_tag>
    scary.script = script # pointer to the script object
    return scary


def swpl_scary_delete(scary):
    del scary.tag
    del scary.tagspec
    scary = None


def swpl_scary_add(array, script):
    for i in range(0, swplc.SCAR_ARRAY_LEN):
        if array[i] is None:
            array[i].copy_from(script)
            return script
    return None


def swpl2_cisf_base_array_add(cisf, b):
    v = cisf.cba.base_array
    vplob_add(v, b)


def swpl2_cisf_base_array_get(cisf, ix):
    v = cisf.cba.base_array
    b = vplob_val(v, ix)
    return b


def swpl_scary_show_array(array):
    tmp = strob_open(18)
    for i in range(0, swplc.SCAR_ARRAY_LEN):
        if array[i] is None:
            break
        else:
            swlib_writef(pty.STDERR_FILENO, tmp, "%d tag=[%s] tagspec=[%s] script=[%p]\n",( i, array[i].tag, array[i].tagspec, array[i].script))
    strob_close(tmp)
    return


def swpl_scary_find_script(array, tag, tagspec):
    retval = None

    tmp = strob_open(18)
    for i in range(0, swplc.SCAR_ARRAY_LEN):
        if array[i] is None:
            break
        else:
            if strcmp(array[i].tag, tag) == 0 and strcmp(array[i].tagspec, tagspec) == 0 and True:
                retval = array[i].scriptM
                break
    strob_close(tmp)
    return retval


def swpl_retrieve_files(G, swi, swicol, e, file_list, ofd, ifd, archive_fd, pax_write_command_key, ls_fd, ls_verbose, digs):
    scriptbuf = strob_open(300)
    shell_lib_buf = strob_open(300)

    E_DEBUG("")
    strob_sprintf(scriptbuf, STROB_DO_APPEND, "rm_retval=1\n")
    strob_sprintf(scriptbuf, STROB_DO_APPEND, "sw_retval=1\n")

    #	 Every task script read stdin a block from even if it doesn't
    #           need data

    # Add the requisite dd bs=512 count=1 of=/dev/null
    E_DEBUG("")
    strob_sprintf(scriptbuf, STROB_DO_APPEND, "dd bs=512 count=1 of=/dev/null 2>/dev/null\n" + "sw_retval=$?\n")

    E_DEBUG("")

    swpl_make_verify_command_script_fragment(G, scriptbuf, file_list, pax_write_command_key,1 if ls_fd < 0 else 0)

    E_DEBUG("")
    ret = swicol_rpsh_task_send_script2(swicol, ofd, 512, ".", strob_str(scriptbuf), swcc.SWBIS_TS_retrieve_files_archive)

    if ret != 0:
        # Set master_alarm ??
        swicol_set_master_alarm(swicol)
        return -1

    E_DEBUG("")
    # Send the gratuitous data block because 'count=0' is busted for
    # some 'dd' programs

    E_DEBUG("")
    ret = etar_write_trailer_blocks(ofd, 1)
    if ret <= 0:
        return -1

    # Now we have to read the archive

    E_DEBUG("")
    taru = taru_create()
    E_DEBUG("")

    #
    #  Set the tar header policy to decode pax headers
    #  This is required.
    #
    taru_set_tar_header_policy(taru, "pax", None)

    #	 here, the archive is read.  This is a tar archive of the
    #	   package files as lifted from the file system

    ret = taru_process_copy_out(taru, ifd, archive_fd, None, None, af.arf_ustar, ls_fd, ls_verbose, None, digs)

    E_DEBUG("")
    taru_delete(taru)
    if ret < 0:
        swicol_set_master_alarm(swicol)

    E_DEBUG("")
    ret = swicol_rpsh_task_expect(swicol, G.g_swi_event_fd, swicol.SWICOL_TL_12)

    E_DEBUG("")
    strob_close(scriptbuf)
    strob_close(shell_lib_buf)
    E_DEBUG("")
    return ret

def swpl_show_file_list(G, fl):
    swi_fl_qsort_forward(fl)
    ix = 0
    while (name := swi_fl_get_path(fl, ix)) is not None:
        sw_l_msg(G, swlibc.SWC_VERBOSE_1, f"{name}\n")
        ix += 1
    print("", end="")
    return 0


def swpl_make_here_document_source(G, scriptbuf, here_payload):
    stop_word = bytearray(32)
    ret = determine_here_document_stop_word_bystr(here_payload, stop_word)
    if ret != 0:
        # this really, really should never happen
        sys.exit(12)

    # Start of hear document
    scriptbuf.append("(\n")
    scriptbuf.append("cat <<'{}'\n".format(stop_word.decode()))
    # payload of here document
    scriptbuf.append(here_payload.decode())
    scriptbuf.append("\n{}\n)".format(stop_word.decode()))
    return 0


def swpl_make_verify_command_script_fragment(G, scriptbuf, fl, pax_write_command_key, be_silent):
    name = None

    shell_lib_buf = STROB()
    swpl_tag_volatile_files(fl)
    stop_word = STROB()
    ret = determine_here_document_stop_word(fl, stop_word)
    if ret != 0:
        exit(12)
    swi_fl_qsort_forward(fl)
    scriptbuf.str_ = "rm_retval=0\n"
    scriptbuf.str_ += f"""\
        {shlib_get_function_text_by_name("shls_check_for_gnu_tar", shell_lib_buf, None)}
        {shlib_get_function_text_by_name("shls_check_for_recent_gnu_gtar", shell_lib_buf, None)}
        {shlib_get_function_text_by_name("shls_check_for_gnu_gtar", shell_lib_buf, None)}
        {shlib_get_function_text_by_name("shls_missing_which", shell_lib_buf, None)}
        {shlib_get_function_text_by_name("shls_write_files_ar", shell_lib_buf, None)}
        """
    if pax_write_command_key == "gtar" or pax_write_command_key == "pax":
        scriptbuf.str_ += """\
            check_has_gnu_gtar=0
            check_has_gnu_tar=0
            """
    elif pax_write_command_key == "tar":
        scriptbuf.str_ += """\
            shls_check_for_gnu_tar
            check_has_gnu_tar=$?
            shls_check_for_gnu_gtar
            check_has_gnu_gtar=$?
            """
    elif pax_write_command_key == "detect":
        scriptbuf.str_ += """\
            check_has_gnu_tar=0
            """
    else:
        scriptbuf.str_ += """\
            check_has_gnu_tar=0
            """
    scriptbuf.str_ += """\
        GTAR=/
        case $check_has_gnu_gtar in
               0)
               GTAR=gtar
               ;;
               *)
               ;;
        esac
        case $check_has_gnu_tar in
               0)
               GTAR=tar
               ;;
               *)
               case $check_has_gnu_gtar in
                       0)
                       ;;
                       *)
                       # Neither gtar or GNU tar was found, fail now
                       # simulate the response of GNU tar to an empty list
                       dd count=2 bs=512 if=/dev/zero 2>/dev/null 
                       exit SWP_RP_STATUS_NO_GNU_TAR
                       ;;
                       esac
               ;;
        esac
        (
        cat <<'{strob_str(stop_word)}'
        """
    ix = 0
    if swlib_utilname_get() == "SWUTIL_NAME_SWVERIFY":
        check_volatile = swextopt_is_option_true(swlibc.SW_E_check_volatile, G.opta)
    else:
        check_volatile = 0
    while (name == swi_fl_get_path(fl, ix)) is not None:
        if G.devel_verbose:
            print(f"INFO; file=[{name}] type=[{swi_fl_get_type(fl, ix)}]", file=sys.stderr)
        is_volatile = 0
        if check_volatile == 0:
            is_volatile = swi_fl_is_volatile(fl, ix)
        if is_volatile == 0:
            scriptbuf.str_ += f"{name}\n"
        ix += 1
    scriptbuf.str_ += f"""\
        {strob_str(stop_word)}
        ) |
        """
    if G.g_do_dereference or pax_write_command_key == "tar" or pax_write_command_key == "gtar" or 0:
        scriptbuf.str_ += """\
            gtar_hdref=""
            case "$g_do_hard_dereferenceM" in
            yes)
                   shls_check_for_recent_gnu_gtar
                   case $? in
                           0)
                           gtar_hdref="--hard-dereference"
                           ;;
                           *)
                           echo "swinstall: warning: tar option ''--hard-derefence'' is not available" 1>&2
                           ;;
                   esac
            ;;
            esac
            case "$GTAR" in
                   /)
                   # FATAL error GNU tar is missing
                   # simulate the response of GNU tar to an empty list
                   dd count=2 bs=512 if=/dev/zero 2>/dev/null 
                   exit SWP_RP_STATUS_NO_GNU_TAR
                   ;;
            esac
             $GTAR %scf - -b 1 -H pax %s $gtar_hdref  --no-recursion --files-from=- %s
            """
    elif pax_write_command_key == "pax" or 0:
        scriptbuf.str_ += " pax -d -w -b 512\n"
    elif pax_write_command_key == "detect" or 0:
        scriptbuf.str_ += """\
            shls_write_files_ar
            # Support controlled bailout in the event 
            # neither GNU tar or pax is present 
            case $? in 126) exit SWP_RP_STATUS_NO_GNU_TAR;; esac
            """
    else:
        scriptbuf.str_ += """\
            # FIXME, sw_retval should be set here
            ### Not used   retrieve_retval=$?
            """
    scriptbuf.str_ += """\
        # FIXME, sw_retval should be set here
        ### Not used   retrieve_retval=$?
        """
    return 0

def swpl_tag_volatile_files(fl):
    ix = 0
    prefix = ""
    while True:
        sx = swi_fl_get_path(fl, ix)
        if sx is None:
            break
        type = swi_fl_get_type(fl, ix)
        is_volatile = swi_fl_is_volatile(fl, ix)
        if prefix is None:
            if type == tarfile.DIRTYPE and is_volatile:
                # tag every file in this directory as volatile
                if prefix:
                    prefix = prefix.decode()
                    del prefix
                prefix = sx.decode()
        else:
            if sx.startswith(prefix.encode()):
                swi_fl_set_is_volatile(fl, None, ix, 1)
            else:
                if prefix:
                    del prefix
                prefix = None
        ix += 1
    if prefix:
       del prefix

def swpl2_audit_cisf_bases(G, swi, cisf):
    E_DEBUG("entering")
    cplob_shallow_reset(cisf.cba.base_array)
    E_DEBUG("add top level")
    swpl2_cisf_base_array_add(cisf, cisf.cisf_base)

    #
    # Loop over the filesets
    #

    fileset_index = 0
    f = vplob_val(cisf.isets, fileset_index)
    fileset_index += 1
    while f:
        E_DEBUG2("GOT f fileset_index=%d\n", fileset_index)
        E_DEBUG("add fileset\n")
        swpl2_cisf_base_array_add(cisf, f.cisf_base)
        f = vplob_val(cisf.isets, fileset_index)
        fileset_index += 1
    E_DEBUG("leaving")


def swpl_audit_execution_scripts(G, swi, scary_scripts):
    fileset = None
    fileset_index = 0
    execution_scripts = [swc.SW_A_checkinstall,
                         swc.SW_A_postinstall,
                         swc.SW_A_preinstall,
                         swc.SW_A_configure,
                         swc.SW_A_checkremove,
                         swc.SW_A_preremove,
                         swc.SW_A_postremove,
                         swc.SW_A_unconfigure,
                         swc.SW_A_unpostinstall,
                         swc.SW_A_unpreinstall,
                         swc.SW_A_request,
                         str(None)]

    # SWBIS_DEBUG_PRINT();
    tagspec = strob_open(32)
    swpl_scary_init_script_array()

    #
    #	 * FIXME, allow more than one product
    #

    product = swi_package_get_product(swi.swi_pkg, 0)
    ts = execution_scripts
    for tag in ts:
        product_xxinstall_script = swi_product_get_control_script_by_tag(product, tag)
        if product_xxinstall_script:
            scary_script = swpl_scary_create(tag, product.p_base.b_tag, product_xxinstall_script)
            swpl_scary_add(scary_scripts, scary_script)

    #
    # Loop over the filesets
    #
    # SWBIS_DEBUG_PRINT();

    fileset_index = 0
    fileset = swi_product_get_fileset(product, fileset_index)
    fileset_index += 1
    while fileset:
        strob_sprintf(tagspec, STROB_NO_APPEND, "%s.%s", product.p_base.b_tag, fileset.base.b_tag)
        ts = execution_scripts
        for tag in ts:
            fileset_xxinstall_script = swi_xfile_get_control_script_by_tag(fileset, tag)
            if fileset_xxinstall_script:
                scary_script = swpl_scary_create(tag, strob_str(tagspec), fileset_xxinstall_script)
                swpl_scary_add(scary_scripts, scary_script)
            fileset_index += 1
            fileset = swi_product_get_fileset(product, fileset_index)

    strob_close(tagspec)

def swpl_write_case_block(swi, buf, tag):
    tmp = strob_open(100)
    swicat_write_script_cases(swi, tmp, tag)
    buf.str += f"""# generated by swpl_write_case_block
           {tag})
    {strob_str(tmp)}
                   ;;
        """

    buf.str_ = buf.str.encode()
    strob_close(tmp)
    return 0


def swpl_get_whole_block_size(size):
    if size == 0:
        return 0
    if int(math.fmod(size, tc.TARRECORDSIZE)):
        ret = ((size / tc.TARRECORDSIZE) + 1) * tc.TARRECORDSIZE
    else:
        ret = (size / tc.TARRECORDSIZE) * tc.TARRECORDSIZE
    return ret


def swpl_write_session_options_file(buf, swi):
    strob_strcpy(buf, "")
    swextopt_writeExtendedOptions_strob(buf, swi.opta, swi.swc_idM, 1)
    return strob_strlen(buf)


def swpl_write_single_file_tar_archive(swi, ofd, name, data, ahs):
    retval = 0

    etar = etar_open(swi.xformat.taru.taru_tarheaderflags)
    Etar.etar_init_hdr(etar)
    swpl_init_header_root(etar)
    Etar.etar_set_size(etar, len(data))
    if Etar.etar_set_pathname(etar, name):
        SWLIB_FATAL("name too long")
    swpl_set_detected_catalog_perms(swi, etar, tarfile.REGTYPE)
    if ahs:
        Etar.etar_set_uname(etar, ahs_get_tar_username(ahs))
        Etar.etar_set_gname(etar, ahs_get_tar_groupname(ahs))
        Etar.etar_set_mode_ul(etar, ahs_get_mode(ahs))
    Etar.etar_set_chksum(etar)

    #
    # Emit the header
    #
    ret = Etar.etar_emit_header(etar, ofd)
    if ret < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: swpl_write_single_file_tar_archive(): etar_emit_header(): ret={ret}")
        return ret
    retval += ret

    #
    # Emit the file data
    #
    ret = etar_emit_data_from_buffer(ofd, data, len(data))
    if ret < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: swpl_write_single_file_tar_archive(): etar_emit_data_from_buffer(): ret={ret}")
        return ret
    retval += ret

    #
    # Emit the trailer blocks
    #
    ret = etar_write_trailer_blocks(ofd, 2)
    if ret < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: swpl_write_single_file_tar_archive(): etar_write_trailer_blocks(): ret={ret}")
        return ret
    retval += ret

    Etar.etar_close(etar)
    return retval


def swpl_load_single_status_value(G, swi, ofd, event_fd, id_str, status_msg):
    tmp = strob_open(10)
    data = strob_open(10)

    # make up one 512 byte block of nuls with
    # the formatted integer at the beginning
    # of the 512 block

    strob_memset(data, '\0', 513)
    strob_sprintf(data, 0, "%d\n", status_msg)

    # Here is the task scriptlet to read a value
    # and exit with its status

    strob_sprintf(tmp, 0,
            "# Read the status from the the management host\n"
            "# and exit with its value.  This is used to \n"
            "# convey an error from the management host to the\n"
            "# target host\n"
            "MHOST_STATUS=$(dd bs=512 count=1);\n"
            "case \"$MHOST_STATUS\" in\n"
            "       0)\n"
            "               sw_retval=0\n"
            "               ;;\n"
            "       *)\n"
            "               sw_retval=1\n"
            "               ;;\n"
            "esac\n"
            )
    #  Now send the task script

    ret = swicol_rpsh_task_send_script2(
            swi.swicol,
            ofd,
            1,
            "/",
            strob_str(tmp),
            id_str
            )
    if ret == 0:
        # ret = atomicio(write, ofd, data.str_, 512)
        ret = atomicio('write', ofd, data.str_, 512)
        if ret == 512:
            ret = 0
        else:
            ret = 2
    else:
        ret = 1
        # internal error
    strob_close(data)
    strob_close(tmp)
    return ret


def swpl_load_single_file_tar_archive(G, swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd, id_str, name, data, ahs):
    tmp = strob_open(10)
    tmpv = strob_open(10)
    namebuf = strob_open(10)

    if G.g_verboseG > swlibc.SWC_VERBOSE_4:
        strob_strcpy(tmpv, "")
    else:
        strob_strcpy(tmpv, "1>/dev/null 2>&1")

    #
    # Compute the data size that will be sent to the stdin of the 'sh -s'
    #  process on the target host.
    #

    stdin_file_size = swpl_get_whole_block_size(len(data)) + (swlibc.swlibc.TARRECORDSIZE * 3) + 0
    #
    #  Here is the task scriptlet to load the installed file.
    #

    strob_sprintf(tmp, STROB_DO_APPEND,
        swlibc.CSHID (
        "cd \"%s\"\n"
        "case \"$?\" in\n"
        "       0)\n"
        "       ;;\n"
        "       *)\n"
        "       echo error: cd \"%s\" failed for task \"%s\" 1>&2\n"
        "       exit 1\n"
        "       ;;\n"
        "esac\n"), catalog_path, catalog_path, id_str)

    strob_sprintf(tmp, STROB_DO_APPEND, swlibc.CSHID ("dd 2>/dev/null | (%s %s; sw_retval=$?; dd of=/dev/null 2>/dev/null; exit $sw_retval) \n" + "sw_retval=$?\n" + "# dd of=/dev/null 2>/dev/null\n"), pax_read_command, strob_str(tmpv))

    #
    # Now send the task script to the target host 'sh -s'
    #

    ret = swicol_rpsh_task_send_script2(swi.swicol, ofd, stdin_file_size, swi.swi_pkg.target_path, strob_str(tmp), id_str)

    if ret == 0:
        #
        # Now send the data on the same stdin.
        # In this case it is a tar archive with one file member
        #

        ret = swpl_write_single_file_tar_archive(swi, ofd, name, data, ahs)
        if ret <= 0:
            SW_ERROR_IMPL()
            return -1

        #
        #		 * Assert what must be true.
        #

        if ret != int(stdin_file_size):
            sys.stderr.write("ret=%d  stdin_file_size=%lu\n"% ret+ stdin_file_size)
            SW_ERROR_IMPL()
            SWI_internal_error()
            return -1

        #
        # Reap the events from the event pipe.
        #
        ret = swicol_rpsh_task_expect(swi.swicol, event_fd, icolc.SWICOL_TL_12)
        if swi.debug_events:
            swicol_show_events_to_fd(swi.swicol, pty.STDERR_FILENO, -1)

        if ret < 0:
            SW_ERROR_IMPL()
            SWI_internal_error()
            return -1
    #
    # close and return
    #
    strob_close(tmpv)
    strob_close(tmp)
    strob_close(namebuf)
    return ret


def swpl_write_tar_installed_software_index_file(G, swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd, id_str, sw_a_installed, ahs):
    ret = 0
    name = None
    data = None

    name = strob_open(100)
    data = strob_open(100)

    swicat_isf_installed_software(data, swi)

    #
    # catalog_entry = [var/adm/sw/catalog/product/fileset/0.000/0]
    #

    strob_strcpy(name, sw_a_installed)

    ret = swpl_load_single_file_tar_archive(G, swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd, id_str, strob_str(name), strob_str(data), ahs)

    strob_close(name)
    strob_close(data)
    return ret


def swpl_construct_configure_script(G, cisf, buf, swi, do_configure):
    E_DEBUG("")
    strob_sprintf(buf, STROB_DO_APPEND,
            swlibc.CSHID (
            "       # Start of code generated by swpl_construct_configure_script \n"
            "               ssv_do_configure=%d\n"
            "               case \"$ssv_do_configure\" in\n"
            "               1)\n"),
                    do_configure)
    ret = swpl_construct_script(G, cisf, buf, swi, swlibc.SW_A_configure)

    strob_sprintf(buf, STROB_DO_APPEND,
            ("               ;;\n"
            "               esac\n"
            "       # End of code generated by swpl_construct_configure_script \n"),
    )
    E_DEBUG("")
    return 0


def swpl2_construct_control_script(G, cisf_base, buf, swi, script_tag, parent_tag):
    event_error_value = 0
    event_warning_value = 0
    E_DEBUG("START")
    tmp = strob_open(20)
    toap = strob_open(20)
    select_error_codes(script_tag, event_error_value, event_warning_value)
    E_DEBUG2("script_tag=%s", script_tag)
    xx_script = swi_xfile_get_control_script_by_tag(cisf_base.ixfile, script_tag)
    E_DEBUG("")

    if xx_script is None:
        #  no script
        return 0
    E_DEBUG("")

    strob_sprintf(toap, STROB_DO_APPEND,
            swlibc.CSHID (
            "# Begin code generated by swproglib.c:swpl_construct_control_script\n"))
    E_DEBUG("")
    if swi.swi_pkg.target_path:
        target_path = swi.swi_pkg.target_path
    else:
        target_path = "."

    E_DEBUG("")
    if parent_tag and len(parent_tag) != 0:
        strob_sprintf(tmp, 0, "%s.%s", parent_tag, cisf_base.ixfile.base.b_tag)
    else:
        strob_sprintf(tmp, 0, "%s", cisf_base.ixfile.base.b_tag)
    strob_sprintf(toap, STROB_DO_APPEND, "\tcd \"%s\"\n", target_path)

    E_DEBUG("")
    construct_script_cases(G, toap, swi, xx_script, strob_str(tmp), script_tag, event_error_value, event_warning_value)

    E_DEBUG("")
    strob_sprintf(toap, STROB_DO_APPEND, "# End of code generated by swproglib.c:swpl_construct_control_script\n")

    strob_sprintf(buf, STROB_DO_APPEND, "%s", strob_str(toap))

    strob_close(toap)
    strob_close(tmp)
    return 0


def swpl_construct_script(G, cisf, buf, swi, script_tag):
    E_DEBUG("START")
    tmp = STROB()
    toap = STROB()
    product_ix = cisf.cisf_base.cf_index
    fileset_ix = (vplob_val(cisf.isets, 0)).cisf_base.cf_index
    event_error_value = 0
    event_warning_value = 0
    # Remove this check when multiple products/filesets become supported
    if fileset_ix != 0 or product_ix != 0:
        print("***  ERROR in cisf object", file=sys.stderr)
        product_ix = fileset_ix = 0

    """
      script_tag is either preinstall, postinstall, configure, or
      postremove, preremove
    """
    select_error_codes(script_tag, event_error_value, event_warning_value)
    product = swi_package_get_product(swi.swi_pkg, product_ix )
    product_xxinstall_script = swi_product_get_control_script_by_tag(product, script_tag)
    fileset = swi_product_get_fileset(product, fileset_ix)
    fileset_xxinstall_script = swi_xfile_get_control_script_by_tag(fileset, script_tag)

    if product_xxinstall_script is None and fileset_xxinstall_script is None:
        #
        # no script
        #
        # strob_sprintf(toap, STROB_DO_APPEND, "sw_retval=0\n");
        return 0

    """
       FIXME -- need to set the state to "transient"
       write code to run the product {pre|post}install script
    """
    if swi.swi_pkg.target_path:
        target_path = swi.swi_pkg.target_path
    else:
        target_path = "."

    if product_xxinstall_script:
        #
        # construct the product script's execution
        #
        strob_sprintf(toap, STROB_DO_APPEND, swlibc.CSHID
        ("# Start of code generated by swproglib.c:swpl_construct_script\n",
                "cd \"%s\"\n"),
                target_path)
    E_DEBUG("")
    if fileset_xxinstall_script:
        strob_sprintf(toap, STROB_DO_APPEND, swlibc.CSHID
        ("       case \"$sw_retval\" in  # Case_construct_script_003\n"
        "       0)\n")
        )
        strob_sprintf(tmp, 0, "%s.%s", product.p_baseM.b_tagM, fileset.base.b_tag)
        strob_sprintf(toap, STROB_DO_APPEND, "\tcd \"%s\"\n", target_path)
        construct_script_cases(G, toap, swi,
                               fileset_xxinstall_script,
                               strob_str(tmp), script_tag,
                               event_error_value,
                               event_warning_value)
        strob_sprintf(toap, STROB_DO_APPEND,
                      "       ;;\n"
                      "       esac # Case_construct_script_003\n"
                      )
    E_DEBUG("")
    strob_sprintf(toap, STROB_DO_APPEND, "# End of code generated by swproglib.c:swpl_construct_script\n")

    strob_sprintf(buf, STROB_DO_APPEND, "%s", strob_str(toap))

    E_DEBUG("")
    strob_close(toap)
    strob_close(tmp)
    return 0


def swpl_construct_analysis_script(G, script_name, buf, swi, p_script):
    return i_construct_analysis_script(G, script_name, buf, swi, p_script)


def swpl_compare_name(name1, name2, att, filename):

    if len(name1) == 0 or len(name2) == 0:
        return 0
    ret = swlib_dir_compare(name1, name2, 1)
    if ret != 0:
        if swlib_compare_8859(name1, name2) != 0:
            print(f"{swlib_utilname_get()}: attribute mismatch: {filename}: att={att}: storage=[{name1}] INFO=[{name2}]", file=sys.stderr)
        else:
            ret = 0
    return 1 if ret != 0 else 0


def swpl_safe_check_pathname(s):
    if swlib_is_sh_tainted_string(s):
        SWLIB_FATAL("tainted string")


def swpl_sanitize_pathname(s):
    swlib_squash_all_leading_slash(s)


def swpl_get_attribute(header, att, length):
    line = swheader_get_attribute(header, att, None)
    value = swheaderline_get_value(line, length)
    return value


def swpl_enforce_one_prod_one_fileset(swi):
    if (swi.swi_pkg.swi_co[0]) is None or (swi.swi_pkg.swi_co[0].swi_co[0]) is None:
        #
        # No product or no fileset
        #
        sys.stderr.write(f"{swlib_utilname_get()}: currently, only one (1) products/filesets.")
        sys.exit(1)
        # swi_com_internal_fatal_error()

    if swi.swi_pkg.swi_co[1] or swi.swi_pkg.swi_co[0].swi_co[1]:
        #
        # More than one product or fileset
        #
        print(f"{swlib_utilname_get()}: currently, multiple products/filesets not yet supported")
        sys.exit(1)
        # SWI_internal_fatal_error()
    return


def swpl_does_have_prod_postinstall(swi):
    ret = 0
    prod = None
    prod = swi_package_get_product(swi.swi_pkg, 0)
    ret = swi_product_has_control_file(prod,  swc.SW_A_postinstall)
    return ret


def swpl_get_fileset_file_count(infoheader):
    count = 0
    swheader_reset(infoheader)
    next_line = swheader_get_next_object(infoheader, int(UCHAR_MAX), int(UCHAR_MAX))
    while next_line != '\0':
        count += 1
        next_line = swheader_get_next_object(infoheader, int(UCHAR_MAX), int(UCHAR_MAX))
    temp_var = count
    count += 1
    return temp_var


def swpl_write_out_signature_member(swi, ptar_hdr, file_hdr, ofd, signum, package_ret, installer_sig):

    xformat = swi.xformat
    retval = 0
    filesize = 0
    tmp = strob_open(16)
    ifd = xformat_get_ifd(xformat)
    etar = etar_open(swi.xformat.taru.taru_tarheaderflags)
    Etar.etar_init_hdr(etar)

    #
    # Set the basic tar header fields
    #
    swpl_init_header_root(etar)

    #
    # This reads the signature tar header from the package.
    #
    # ret = os.read(ifd, ptar_hdr, swlibc.TARRECORDSIZE)
    ret = os.read(ifd, swlibc.TARRECORDSIZE)
    if ret != swlibc.TARRECORDSIZE:
        # SWI_internal_fatal_error()
        sys.exit(1)

    # If this is the installer sig then seek backwards since there
    # really is not a archive member in the package for this signature

    if installer_sig and len(installer_sig):
        ret = os.lseek(ifd, -swlibc.TARRECORDSIZE, os.SEEK_CUR)
        if ret < 0:
            # SWI_internal_fatal_error()
            sys.exit(1)


    #
    # Determine the filesize of the signature file
    # It damn well should be 512 or 1024
    #
    filesize = int(ptar_hdr.size, 8)
    # taru_otoul(ptar_hdr.size, filesize)
    package_filesize = filesize + swlibc.TARRECORDSIZE
    package_ret[0] = package_filesize

    #
    # Do a sanity check on the size.
    #
    if filesize != 512 and filesize != 1024:
        sys.stderr.write(f"filesize={filesize}")
        # SWI_internal_fatal_error()
        sys.exit(1)


    #
    # allocate some temporary memory for this
    #
    sig = bytearray(filesize)
    if (not sig) != '\0':
        # SWI_internal_fatal_error()
        sys.exit(1)


    if installer_sig and len(installer_sig) != 0:
        sig[:] = b'\n' * filesize
        sig[:len(installer_sig)] = installer_sig.encode()
    else:
        #
        # This reads the sigfile archive member data
        #
        # ret = uxfio_read(ifd, sig, int(filesize))
        ret = os.read(ifd, filesize)
        if int(ret) != int(filesize):
            # SWI_internal_fatal_error()
            sys.exit(1)
        sig[:] = ret
    sig[-1] = 0
    siglen = len(sig)
    #
    # The package contains a sigfile padded with NULs.
    # We will only install the ascii bytes which means the
    # the sig file in the installed catalog will be shorter in
    # length than in the package.
    #

    Etar.etar_set_size(etar, siglen)

    #
    # set the mode of the catalog top level directory
    # was:  etar_set_mode_ul(etar, (unsigned int)(0640))
    #
    swpl_set_detected_catalog_perms(swi, etar, tarfile.REGTYPE)
    tmp = swicatc.SWINSTALL_INCAT_NAME.encode()
    swlib_unix_dircat(tmp, swicatc.SWINSTALL_CATALOG_TAR)
    swlib_squash_all_leading_slash(tmp.str_) # FIXME
    if signum <= 1:
        tmp.str_ += b".sig"
    else:
        tmp.str_ += f".sig{signum}".encode()

    if Etar.etar_set_pathname(etar, strob_str(tmp)):
        SWLIB_FATAL("name too long")
    Etar.etar_set_chksum(etar)

    # ret = uxfio_unix_safe_write(ofd, (etar_get_hdr(etar)), swlibc.TARRECORDSIZE)
    ret = os.write(ofd, Etar.etar_get_hdr(etar))
    if ret != swlibc.TARRECORDSIZE:
        # SWI_internal_fatal_error()
        sys.exit(1)

    retval += ret
    if siglen <= 512:
        filesize = 512
    elif siglen <= 1024:
        filesize = 1024
    else:
        # SWI_internal_fatal_error()
        sys.exit(1)


    ret = os.write(ofd, sig)
    if ret != int(filesize):
        print(f"{swlib_utilname_get()}: swpl_write_out_signature_member(): ret={ret} filesize={filesize}")
        if ret < 0:
            print(f"{swlib_utilname_get()}: swpl_write_out_signature_member(): {os.strerror(1)}")
        # SWI_internal_fatal_error()
        sys.exit(1)

    retval += ret
    Etar.etar_close(etar)
    strob_close(tmp)
    del sig
    return retval

def swpl_write_out_all_signatures(swi: 'SWI', ptar_hdr: 'struct tar_header', file_hdr: 'struct new_cpio_header', ofd: int, startoffset: int, endoffset: int, filesize: int) -> int:
    ifd = swi.xformat.ifd
    curpos = os.lseek(ifd, 0, os.SEEK_CUR)
    package_ret = []
    if curpos < 0:
        # SWI_internal_fatal_error()
        sys.exit(1)
    sig_block_length = endoffset - startoffset
    if sig_block_length < 1024:
        """
          FIXME: tar format specific.
          Sanity check.
        """
        return -1

    #
    # Seek to the beginning of the signature archive members.
    #

    if os.lseek(ifd, startoffset, os.SEEK_SET) < 0:
        # SWI_internal_fatal_error()
        sys.exit(1)


    """
      all the sigfiles are the same length, filesize.
      sig_number is the number of signatures present, this
      needs to be determined because it is decremented to
      '1'.  This means the last signature in the package
      will have the name 'catalog.tar.sig', the next to last
      will have the name 'catalog.tar.sig2', and so on.
    """

    sig_number = math.trunc(sig_block_length // (filesize + swlibc.TARRECORDSIZE))

    # fprintf(stderr, "eraseme filesize=%d sig_number=%d\n", (int)filesize, sig_number);

    #
    # do a sanity check
    #

    if math.fmod(sig_block_length, (filesize + swlibc.TARRECORDSIZE)) != 0:
        # SWI_internal_fatal_error()
        sys.exit(1)


    if swi.swi_pkg.installer_sig and sig_number >= 1:
        sig_number += 1
        ret = swpl_write_out_signature_member(swi, ptar_hdr, file_hdr, ofd, sig_number-1, package_ret, swi.swi_pkg.installer_sig)
        if ret < 0:
            return ret
        current = ret
    else:
        current = 0

    #
    # Loop to write out all the signatures.
    #
    package_current = 0
    while package_current < sig_block_length:
        if sig_number <= 0:
            # Sanity check
            # SWI_internal_fatal_error()
            sys.exit(1)
        ret = swpl_write_out_signature_member(swi, ptar_hdr, file_hdr, ofd, sig_number-1, package_ret, sig_number)
        if ret < 0:
            return ret
        package_current += package_ret
        current += ret
        sig_number -= 1

    #
    #	 * Now restore the old offset.
    #
    if os.lseek(ifd, curpos, os.SEEK_SET) < 0:
        sys.exit(1)
        # SWI_internal_fatal_error()
    return current

def swpl_write_catalog_data(swi, ofd, sig_block_start, sig_block_end):
    xformat = swi.xformat
    package = swi.swi_pkg
    E_DEBUG("START")
    ifd = xformat.ifd
    curpos = os.lseek(ifd, 0, os.SEEK_CUR)

    E_DEBUG2("current position = [%d]", curpos)

    if curpos < 0:
        # SWI_internal_fatal_error()          
        return -1 

    E_DEBUG2("ifd=%d", ifd)
    if os.lseek(ifd, package.catalog_start_offset, os.SEEK_SET) < 0:
        # SWI_internal_fatal_error()
        return -1

    E_DEBUG("")
    if sig_block_start > 0:
        iend = sig_block_start
    else:
        iend = package.catalog_end_offset
    amount = iend - package.catalog_start_offset

    E_DEBUG("")
    E_DEBUG2("amount before signature [%d]", amount)
    if swlib_pump_amount(ofd, ifd, amount) != amount:
        # SWI_internal_fatal_error()
        return -1
    retval = amount
    E_DEBUG("")
    if sig_block_start > 0:
        #
        # skip the signatures and write the remaining.
        #
        amount = sig_block_end - sig_block_start
        if os.lseek(ifd, amount, os.SEEK_CUR) < 0:
            SW_ERROR_IMPL()
            # SWI_internal_fatal_error()
        E_DEBUG2("amount = sig_block_end - sig_block_start  [%d]", amount)
        amount = package.catalog_end_offset - sig_block_end
        if swlib_pump_amount(ofd, ifd, amount) != amount:
            SW_ERROR_IMPL()
            # SWI_internal_fatal_error()
            return -1
        E_DEBUG2("remaining after signature [%d]", amount)
        retval += amount
    else:
        #
        #  done
        #
        pass
    if os.lseek(ifd, curpos, os.SEEK_SET) < 0:
        SW_ERROR_IMPL()
        # SWI_internal_fatal_error()

    E_DEBUG("")
    #
    # Now write 2 NUL trailer blocks
    #
    E_DEBUG2("retval before trailers [%d]", retval)
    ret = etar_write_trailer_blocks(ofd, 2)
    if ret < 0:
        sys.stderr.write(f"{swlib_utilname_get()}: swpl_write_catalog_data(): etar_write_trailer_blocks(): ret={ret}\n")
        return ret
    retval += ret
    E_DEBUG("")
    return retval


def swpl_report_status(swicol, ofd, event_fd):
    ret = swpl_send_null_task2(swicol, ofd, event_fd, "SWBIS_TS_report_status", "sw_retval=$rp_status")
    return ret


def swpl_send_success(swicol, ofd, event_fd, msgtag):
    ret = swpl_send_null_task(swicol, ofd, event_fd, msgtag, "SW_SUCCESS")
    return ret


def swpl_send_nothing_and_wait(swicol, ofd, event_fd, msgtag, tl, retcode):
    E_DEBUG("START")
    tmp = strob_open(10)

    #
    # Here is the minimal task scriptlet
    #
    strob_sprintf(tmp, 0,
            "sw_retval=%d\n"
            "dd of=/dev/null 2>/dev/null\n",
            retcode
            )
    #
    # Send the script into stdin of the POSIX shell
    #  invoked with the '-s' option.
    #
    E_DEBUG("")
    ret = swicol_rpsh_task_send_script2(
            swicol,
            ofd,
            512,
            ".",
            tmp.str_, msgtag
            )
    E_DEBUG("")
    if ret == 0:
        #
        # Now send the stdin payload which must be
        # exactly stdin_file_size bytes long.

        # Send the payload
        #
        E_DEBUG("")
        ret = etar_write_trailer_blocks(ofd, 1)
        if ret < 0:
            sys.stderr.write(f"{swlib_utilname_get()}: send_nothing_and_wait(): etar_write_trailer_blocks(): ret={ret}\n")
        #
        # Reap the events from the event pipe.
        #
        E_DEBUG("")
        ret = swicol_rpsh_task_expect(swicol, event_fd, tl)
        if ret < 0:
            SWLIB_INTERNAL("")
    strob_close(tmp)
    E_DEBUG("")
    return ret


def swpl_send_signature_files(swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd, ptar_hdr, sig_block_start, sig_block_end, filesize):
    E_DEBUG("START")
    tar_hdr = bytearray(swlibc.TARRECORDSIZE+1)
    tmp = strob_open(10)
    file_hdr = taru_make_header()
    sig_block_length = sig_block_end - sig_block_start
    stdin_file_size = sig_block_length + swlibc.TARRECORDSIZE + swlibc.TARRECORDSIZE # Trailer block

    if swi.swi_pkg.installer_sig and len(swi.swi_pkg.installer_sig) != 0:
        stdin_file_size += (filesize + swlibc.TARRECORDSIZE)

    #
    # Here is the task scriptlet.
    #
    strob_sprintf(tmp, STROB_DO_APPEND,
            swlibc.CSHID (
            "cd \"%s\"\n"
            "case \"$?\" in\n"
            "       0)\n"
            "       ;;\n"
            "       *)\n"
            "       echo error: cd \"%s\" failed in routine \"%s\" 1>&2\n"
            "       exit 1\n"
            "       ;;\n"
            "esac\n")
            , catalog_path, catalog_path, "__send_signature_files")

    strob_sprintf(tmp, STROB_DO_APPEND,
            swlibc.CSHID (
            "dd 2>/dev/null | %s\n"
            "sw_retval=$?\n"
            "dd of=/dev/null 2>/dev/null\n")
            ,
            pax_read_command
            )
    #
    # Send the script into stdin of the POSIX shell
    # invoked with the '-s' option.
    #
    # SWBIS_TS_Load_signatures
    # swicol_set_task_idstring(swi->swicol, SWBIS_TS_Load_signatures);
    ret = swicol_rpsh_task_send_script2(
            swi.swicol,
            ofd,
            stdin_file_size,
            swi.swi_pkg.target_path,
            strob_str(tmp), swcc.SWBIS_TS_Load_signatures
            )
    if ret == 0:
        #
        # Now send the stdin payload which must be
        # exactly stdin_file_size bytes long.
        #

        ret = swpl_write_out_all_signatures(swi, tar_hdr, file_hdr, ofd, sig_block_start, sig_block_end, filesize)

        if ret < 0:
            SW_ERROR_IMPL()
            return -1

        if ret > (stdin_file_size - swlibc.TARRECORDSIZE - swlibc.TARRECORDSIZE):
            #
            # Sanity check
            #
            SW_ERROR_IMPL()
            return -1

        #
        # Now pad the remaining output which will be at least
        # 2 * 512 bytes long.
        #
        padamount = stdin_file_size - ret

        ret = swlib_pad_amount(ofd, padamount)
        if ret < 0 or ret != padamount:
            SW_ERROR_IMPL()
            return -1

        #
        # Reap the events from the event pipe.
        #
        ret = swicol_rpsh_task_expect(swi.swicol, event_fd, icolc.SWICOL_TL_12)
        if swi.debug_events:
            swicol_show_events_to_fd(swi.swicol, pty.STDERR_FILENO, -1)
    del file_hdr
    strob_close(tmp)
    return 0 # ret;


def swpl_common_catalog_tarfile_operation(swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd, script, id_str):
    E_DEBUG("START")
    #
    # add a chdir operation to the front of this script
    #  to chdir into the catalog diretory
    #

    script_buf = f"cd \"{catalog_path}\"\n" \
                 "case \"$?\" in\n" \
                 "       0)\n" \
                 "       ;;\n" \
                 "       *)\n" \
                 "       echo error: cd \"{catalog_path}\" failed for task \"{id_str}\" 1>&2\n" \
                 "       exit 1\n" \
                 "       ;;\n" \
                 "esac\n"
    #
    #  Now append the caller's script
    #

    script_buf += f"\n{script}\n"

    #
    # Now send the script.
    #

    # swicol_set_task_idstring(swi->swicol, id_str);
    ret = swicol_rpsh_task_send_script2(swi.swicol, ofd, 512, swi.swi_pkg.target_path, strob_str(script_buf), id_str)
    if ret == 0:
        """
          Now Send the payload
          which in this case is just a gratuitous 512 bytes.
          This is needed because some dd(1) on some Un*xes can't
          read zero blocks.
        """
        ret = etar_write_trailer_blocks(ofd, 1)
        if ret < 0:
            print(f"{swlib_utilname_get()}: swpl_common_catalog_tarfile_operation(): etar_write_trailer_blocks(): ret={ret}", file=sys.stderr)
            SW_ERROR_IMPL()
            SWI_internal_error()
            return -1

        #
        #  Now reap the events from the event pipe.
        #
        ret = swicol_rpsh_task_expect(swi.swicol, event_fd, icolc.SWICOL_TL_12)

        if swi.debug_events:
            swicol_show_events_to_fd(swi.swicol, pty.STDERR_FILENO, -1)
    strob_close(script_buf)
    return ret


def swpl_update_state_by_cisf_base(cfb, state):
    cfb.ixfile.base.mod_time = time.time()
    swi_xfile_set_state(cfb.ixfile, state)


def swpl_update_fileset_state(swi, swsel, state):
    fileset_index = 0

    #
    # FIXME, supprt software selections
    #
    product = swi_package_get_product(swi.swi_pkg, 0)
    fileset = swi_product_get_fileset(product, fileset_index)
    fileset_index += 1
    while fileset:
        swi_xfile_set_state(fileset, state)
        fileset = swi_product_get_fileset(product, fileset_index)
        fileset_index += 1


def swpl2_update_fileset_state(cisf_base, script, status):
    if cisf_base.typeid == swplc.D_ONE:
        # a product does not have a state attribute
        return

    script_tag = script.base.b_tag

    if script_tag ==  swc.SW_A_configure:
        if status == 0:
            swpl_update_state_by_cisf_base(cisf_base, swc.SW_STATE_CONFIGURED)
        else:
            swpl_update_state_by_cisf_base(cisf_base, swc.SW_STATE_CORRUPT)
    elif script_tag == swc.SW_A_postinstall:
        if status == 0:
            swpl_update_state_by_cisf_base(cisf_base, swc.SW_STATE_INSTALLED)
        else:
            swpl_update_state_by_cisf_base(cisf_base, swc.SW_STATE_CORRUPT)
    elif script_tag == swc.SW_A_preinstall or script_tag == swc.SW_A_preremove:
        if status == 0:
            swpl_update_state_by_cisf_base(cisf_base, swc.SW_STATE_TRANSIENT)
        else:
            swpl_update_state_by_cisf_base(cisf_base,swc. SW_STATE_CORRUPT)
    elif script_tag == swc.SW_A_unconfigure:
        if status == 0:
            swpl_update_state_by_cisf_base(cisf_base, swc.SW_STATE_INSTALLED)
        else:
            swpl_update_state_by_cisf_base(cisf_base, swc.SW_STATE_CORRUPT)
    else:
        sys.stderr.write("bad script tag in swpl2_update_fileset_state")


def swpl2_update_execution_script_results(swi, swicol, cisf):
    cisf_base = CISF_BASE()
    event_index = -1
    E_DEBUG("START")
    num_processed = 0
    buf = strob_open(100)
    tmp = strob_open(100)

    E_DEBUG("")
    event_start_index = 0

    #
    # loop over the events
    #
    message = swicol_rpsh_get_event_message(swicol,  swlibc.SW_CONTROL_SCRIPT_BEGINS, event_start_index, event_index)
    event_index += 1
    while message != '\0':
        E_DEBUG2("message=%s", message)

        #
        #  Find the script_id from the SWI_MSG event
        #
        id_msg = swicol_rpsh_get_event_message(swicol, ec.SWI_MSG, event_index, event_index)
        assert id_msg is not None
        s = id_msg.find(swc.SW_A_SCRIPT_ID+'=')
        assert s >= 0
        s += len('SW_A_SCRIPT_ID=')
        script_id = int(id_msg[s:])

        #
        #  Now find the SWI_CONTROL_SCRIPT object that belongs to this script_id
        #
        E_DEBUG2("Looking for script id [%d]\n", script_id)
        script = swpl2_find_by_id(script_id, cisf.cba, cisf_base)
        assert script is not None
        E_DEBUG2("Found script id [%d]\n", script_id)

        status = swicol_rpsh_get_event_status(swicol, str(None), swlibc.SW_CONTROL_SCRIPT_ENDS, event_index, event_index)
        assert status >= 0

        # Now, if the script is unconfigure we need to set the result of the
        # configure script to UNSET

        # So find the configure script

        if cisf_base and script:
            if strcmp(script.base.b_tag,  swc.SW_A_unconfigure) == 0:
                ununtag =  swc.SW_A_configure
            elif strcmp(script.base.b_tag,  swc.SW_A_unpostinstall) == 0:
                ununtag =  swc.SW_A_postinstall
            else:
                ununtag = None
            if ununtag:
                ununscript = swi_xfile_get_control_script_by_tag(cisf_base.ixfile, ununtag)
                if ununscript:
                    ununscript.result = swic.SWI_RESULT_UNDEFINED
        script.result = status
        num_processed += 1

        swpl2_update_fileset_state(cisf_base, script, status)

        E_DEBUG2("script result=%d", status)
        message = swicol_rpsh_get_event_message(swicol,  swlibc.SW_CONTROL_SCRIPT_BEGINS, event_index + 1, event_index)
    strob_close(tmp)
    strob_close(buf)
    return num_processed

# If the filesets have no script, but the product does, refer the
#   fileset state from the product script results

def swpl2_normalize_configure_script_results(swi, script_name, cisf):
    E_DEBUG("ENTERING")
    i = 0
    base = swpl2_cisf_base_array_get(cisf, i)
    i += 1
    if base is None or base.typeid != swplc.D_ONE:
        # error
        E_DEBUG("error 1")
        return 1

    xfile = base.ixfile # actually PFILES

    # script_name should only be 'configure' or 'unconfigure'

    product_script = swi_xfile_get_control_script_by_tag(xfile, script_name)

    if not product_script:
        # This is OK, if there is no product script then
        # there is no need to refer the result into the fileset
        E_DEBUG("returning 0")
        return 0

    product_result = product_script.result
    E_DEBUG2("product_result=%d", product_result)

    # i equals 1 here, this is the first fileset
    while (base := swpl2_cisf_base_array_get(cisf, i)) is not None:
        E_DEBUG("GOT fileset")
        if base.typeid == swplc.D_ONE:
            # error, should not happen
            E_DEBUG("error 2")
            return 2
        elif base.typeid == swplc.D_TWO:
            xfile = base.ixfile
            if xfile is None:
                E_DEBUG("error 4")
                return 4
            script = swi_xfile_get_control_script_by_tag(xfile, script_name)
            if script is None:
                E_DEBUG2("updating fileset result to product_result=%d", product_result)
                swpl2_update_fileset_state(base, product_script, product_result)
            else:
                # do not refer the product result to this fileset
                pass
        else:
            # error, should not happen
            E_DEBUG("error 3")
            return 3
        i += 1
    E_DEBUG2("Leaving i=%d", i)
    return 0


def swpl_update_execution_script_results(swi, swicol, array):
    E_DEBUG("START")
    buf = strob_open(100)
    tmp = strob_open(100)
    event_index: int = -1
    event_start_index: int = 0
    E_DEBUG("")
    E_DEBUG2("event_start_index=%d", event_start_index)

    #
    # loop over the events
    #
    message = swicol_rpsh_get_event_message(swicol,  swlibc.SW_CONTROL_SCRIPT_BEGINS, event_start_index, event_index)
    event_index += 1
    while message:
        E_DEBUG2("message=%s", message)
        tagspec = message
        tag = message.split(" ")[0]
        SWLIB_ASSERT(tag is not None)
        savechp = tag
        tag += 1
        # fprintf(stderr, "tag=[%s] tagspec=[%s]\n", tag, tagspec)
        #
        # Now find the script that belongs to this tag and tagspec
        #
        script = swpl_scary_find_script(array, tag, tagspec)
        E_DEBUG2("script tag=%s", tag)
        E_DEBUG2("script tagspec=%s", tagspec)
        SWLIB_ASSERT(script is not None)

        id_msg = swicol_rpsh_get_event_message(swicol, "SWI_MSG", event_index, event_index)
        status = swicol_rpsh_get_event_status(swicol, None, "SW_CONTROL_SCRIPT_ENDS", event_index, event_index)
        SWLIB_ASSERT(status >= 0)
        script.result = status
        E_DEBUG2("script result=%d", status)
        message = swicol_rpsh_get_event_message(swicol, "SW_CONTROL_SCRIPT_BEGINS", event_index+1, event_index)
        tag[0] = " "

    strob_close(tmp)
    strob_close(buf)
    return 0


def swpl_unpack_catalog_tarfile(G, swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd):
    E_DEBUG("START")
    if G.g_verboseG >= swlibc.SWC_VERBOSE_6:
        tmpv = ""
    else:
        tmpv = "1>/dev/null"

    tmp = (
            ("#set -vx\n"
        "# echo unpacking catalog %s 1>&2\n"
        "dd of=/dev/null 2>/dev/null\n"
        "# pwd 1>&2\n"
        "cd  " + swlibc.SWINSTALL_INCAT_NAME + " || exit 1\n"
        "%s %s <catalog.tar %s\n"
        "sw_retval=$?\n")
        % (swi.exported_catalog_prefix, pax_read_command, swi.exported_catalog_prefix, tmpv)
    )
    E_DEBUG("")

    ret = swpl_common_catalog_tarfile_operation(swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd, tmp, swcc.SWBIS_TS_Catalog_unpack)

    E_DEBUG("")
    strob_close(tmp)
    strob_close(tmpv)
    return ret

#* swpl_remove_catalog_directory - remove the unpacked catalog from catalog
# *
#


def swpl_remove_catalog_directory(swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd):
    E_DEBUG("START")
    tmp = strob_open(10)
    tmptok = strob_open(10)
    rmdir_command = strob_open(10)

    #
    # perform some sanity checks on swi->exported_catalog_prefix
    # It should have the force of <path>/catalog
    #
    path = swi.exported_catalog_prefix
    if swi_com_check_clean_relative_path(path):
        return 1
    swlib_squash_trailing_slash(path)
    if "SW_A_catalog" not in path:
        return 2

    """
        OK , swi->exported_catalog_prefix is now qualified as sane

        swi->exported_catalog_prefix is typically :  <path>/catalog
        therefore we must execute:
             rmdir <path>
        If there is more than one leading path component
        then they must be rmdir'ed individually

        If the exported catalog path is
        aaaa/bbbb/cccc/catalog    then
        form a command string to remove aaaa/bbbb/cccc
        using rmdir since it is safer to user than rm -fr
        The command will look like this:
        rmdir aaaa/bbbb/cccc && rmdir aaaa/bbbb && rmdir aaaa
        
        One should be able to do this via os.rmdir - Pw
    """

    rmdir_command = b''
    if b'/' in path:
        start = path
        token = path.rfind(b'/')
        while token >= 0:
            path[token] = b'\0'
            rmdir_command += f" && rmdir \"{start.decode()}\"".encode()
            # rmdir_command += os.rmdir(path)
            token = path.rfind(b'/', 0, token)

    tmp[:] = (
        b"cd " + swlibc.SWINSTALL_INCAT_NAME.encode() + b" || exit 1\n"
        b"/bin/rm -fr \"" + swi.exported_catalog_prefix + b"\" " + rmdir_command + b"\n"
        b"sw_retval=$?\n"
        b"dd of=/dev/null 2>/dev/null\n"
    )
    ret = swpl_common_catalog_tarfile_operation(swi, ofd, catalog_path, pax_read_command, alt_catalog_root, event_fd, tmp, swlibc.SWBIS_TS_Catalog_dir_remove)
    strob_close(tmp)
    strob_close(path)
    strob_close(rmdir_command)
    strob_close(tmptok)
    return ret


def swpl_get_utsname_attributes(G, swicol, uts, ofd, event_fd):
    E_DEBUG("START")
    tmp = io.StringIO()
    tmp1 = io.StringIO()
    tmp.write(f"""
    sw_retval=0
    {shlib_get_function_text_by_name("shls_config_guess", tmp1, None)}
    {shlib_get_function_text_by_name("shls_run_config_guess", tmp1, None)}
    {TEVENT(2, 1, "SWI_ATTRIBUTE", f"{swlibc.SW_A_machine_type}=$(uname -m)")}
    {TEVENT(2, 1, "SWI_ATTRIBUTE", f"{swlibc.SW_A_os_name}=$(uname -s)")}
    {TEVENT(2, 1, "SWI_ATTRIBUTE", f"{swlibc.SW_A_os_release}=$(uname -r)")}
    {TEVENT(2, 1, "SWI_ATTRIBUTE", f"{swlibc.SW_A_os_version}=$(uname -v)")}
    {TEVENT(2, 1, "SWI_ATTRIBUTE", f"{swlibc.SW_A_architecture}=$(shls_run_config_guess)")}
    dd of=/dev/null 2>/dev/null
    exit $sw_retval
    """)
    # TS_uts_query
    ret = swicol_rpsh_task_send_script2(
        swicol,
        ofd,
        512,
        ".",
        tmp.getvalue(), "SWBIS_TS_uts"
    )
    if ret == 0:
        ret = etar_write_trailer_blocks(ofd, 1)
        if ret < 0:
            print(f"{swlib_utilname_get()}: swpl_get_utsname_attributes(): etar_write_trailer_blocks(): ret={ret}", file=sys.stderr)
        ret = swicol_rpsh_task_expect(swicol, event_fd, icolc.SWICOL_TL_100)
        # 100 seconds needed for cygwin hosts

        #
        # print the events to memory
        #
        swicol_print_events(swicol, tmp, swicol.event_index)

        #
        # Parse and Store the uts attributes
        #
        swuts_read_from_events(uts, strob_str(tmp))

        E_DEBUG2("utsname=[%s]", strob_str(tmp))
        # print(f"utsname=[{tmp.getvalue()}]", file=sys.stderr)
        if ret != 0:
            #
            # Examine the events in swicol->evemt_list to determine
            # what action to take
            #
            if ret == 2:
                swlib_doif_writef(G.g_verboseG, 1, None, sys.stderr.fileno(),
                                  "SW_RESOURCE_ERROR on target host:  %d second time limit expired\n", 5)
            ret = -1
    strob_close(tmp)
    strob_close(tmp1)
    return ret


def swpl_get_catalog_perms(G, swi, ofd, event_fd):
    E_DEBUG("START")
    swicol = swi.swicol
    tmp = io.StringIO()
    isc = get_opta_isc(G.optaM, "sw_e_installed_software_catalog")
    cmd = f"""
    sw_retval=0
    ISC="{isc}"
    LSO=$(tar chf - "$ISC" 2>/dev/null | tar tvf - 2>/dev/null | head -1)
    {TEVENT(2, 1, "SWI_ATTRIBUTE", "ls_ld=$LSO")}
    dd of=/dev/null 2>/dev/null
    exit $sw_retval
    """
    # TS_get_catalog_perms
    ret = swicol_rpsh_task_send_script2(swicol, ofd, 512, swi.swi_pkg.target_path, strob_str(tmp), swlibc.SWBIS_TS_get_catalog_perms)

    if ret == 0:
        ret = etar_write_trailer_blocks(ofd, 1)
        if ret < 0:
            print(f"{swlib_utilname_get()}: get_catalog_perms(): etar_write_trailer_blocks(): ret={ret}")

        ret = swicol_rpsh_task_expect(swicol, event_fd, icolc.SWICOL_TL_100)
        # 100 seconds needed for cygwin hosts

        if ret != 0:
            # FIXME, write error message
            return -1

        #
        # print the events to memory
        #
        swicol_print_events(swicol, tmp, swicol.event_index)
        # fprintf(stderr, "%s\n", strob_str(tmp));

        #
        # Parse and Store the ls -ld output into swi
        #

        ret = parse_ls_ld_output(swi, tmp.getvalue())
        if ret != 0:
            swi.swi_pkgM.installed_catalog_ownerM = "root"
            swi.swi_pkgM.installed_catalog_groupM = "root"
            swi.swi_pkgM.installed_catalog_modeM = 0o755
            print(f"{swlib_utilname_get()}: warning: (swpl_get_catalog_perms) parsing (retrieval) of tar listing failed: ret={ret}")
            print(f"{swlib_utilname_get()}: warning: root/root (0755) will be used for catalog ownerships")
            ret = 0
    else:
        ret = -3
    strob_close(tmp)
    return ret


def swpl_determine_tar_listing_verbose_level(swi):
    E_DEBUG("START")
    if swi.swc_id == Ec.SWC_U_I:
        if swi.verbose >= swlibc.SWC_VERBOSE_3:
            E_DEBUG("")
            taru_ls_verbose_level = lslistc.LS_LIST_VERBOSE_L1
        else:
            E_DEBUG("")
            taru_ls_verbose_level = lslistc.LS_LIST_VERBOSE_L0
    elif swi.swc_id == Ec.SWC_U_L:
        if swi.verbose >= swlibc.SWC_VERBOSE_2:
            E_DEBUG("")
            taru_ls_verbose_level = lslistc.LS_LIST_VERBOSE_L1
        else:
            E_DEBUG("")
            taru_ls_verbose_level = lslistc.LS_LIST_VERBOSE_L0
    else:
        E_DEBUG("")
        taru_ls_verbose_level = lslistc.LS_LIST_VERBOSE_L0
    E_DEBUG2("[%d]", taru_ls_verbose_level)
    return taru_ls_verbose_level


def swpl_assert_all_file_definitions_installed(swi, infoheader):
    E_DEBUG("START")
    ret = 0
    swheader_store_state(infoheader, None)
    swheader_reset(infoheader)
    while next_line := infoheader.get_next_object(ord(b'_'), ord(b'_')):
        keyword = infoheader.get_keyword(next_line)
        assert keyword is not None
        if keyword == "SW_A_control_file":
            continue
        if infoheader.get_flag1(next_line) == 0:
            missing_name = infoheader.get_attribute("SW_A_path", None)
            swi.verbose_write(1, None, sys.stderr, f"warning: missing from storage section: {missing_name}")
            ret += 1
    infoheader.restore_state(None)
    return ret


def swpl_get_same_revision_specs(G, swi, product_number, location):
    E_DEBUG("START")
    tmp = strob_open(10)
    swspecs = vplob_open()

    if location and len(location) == 0:
        location = None

    product = swi_package_get_product(swi.swi_pkg, product_number)
    SWLIB_ASSERT(product is not None)

    # Get the global INDEX access header object
    global_index = swi_get_global_index_header(swi)
    swheader_store_state(global_index, None)
    swheader_reset(global_index)

    swheader_set_current_offset(global_index, product.p_base.header_index)

    tag = swheader_get_single_attribute_value(global_index,  swc.SW_A_tag)
    assert tag is not None

    revision = swheader_get_single_attribute_value(global_index,  swc.SW_A_revision)
    assert revision is not None

    # First, LT
    strob_sprintf(tmp, STROB_NO_APPEND, "%s,r<%s", tag, revision)
    if location:
        strob_sprintf(tmp, STROB_DO_APPEND, ",l=%s", location)
    swverid = swverid_open(None, strob_str(tmp))
    vplob_add(swspecs, swverid)

    # Second,  EQ
    strob_sprintf(tmp, STROB_NO_APPEND, "%s,r==%s", tag, revision)
    if location:
        strob_sprintf(tmp, STROB_DO_APPEND, ",l=%s", location)
    swverid = swverid_open(None, strob_str(tmp))
    vplob_add(swspecs, swverid)

    # Third, GT
    strob_sprintf(tmp, STROB_NO_APPEND, "%s,r>%s", tag, revision)
    if location:
        strob_sprintf(tmp, STROB_DO_APPEND, ",l=%s", location)
    swverid = swverid_open(None, strob_str(tmp))
    vplob_add(swspecs, swverid)

    # Fouth, EQ but any location
    strob_sprintf(tmp, STROB_NO_APPEND, "%s,r==%s", tag, revision)
    swverid = swverid_open(None, strob_str(tmp))
    vplob_add(swspecs, swverid)

    swheader_restore_state(global_index, None)
    strob_close(tmp)
    return swspecs


def swpl_get_dependency_specs(G, swi, requisite_keyword, product_number, fileset_number):
    E_DEBUG("START")
    tmp = strob_open(10)
    tmp2 = strob_open(10)

    # Get the dependency specs from the fileset per spec

    swspecs = vplob_open() # List object that will contain a
    # list of SWVERID objects

    product = swi_package_get_product(swi.swi_pkg, product_number)
    assert product is not None

    fileset = swi_product_get_fileset(product, fileset_number)
    assert fileset is not None

    # Now get the dependencies which are stored in the 'prerequisites' attrbute

    # Get the global INDEX access header object
    global_index = swi_get_global_index_header(swi)
    swheader_store_state(global_index, None)
    swheader_reset(global_index)

    # Now set the offset of the relevant part of the global INDEX file
    swheader_set_current_offset(global_index, fileset.base.header_index)

    xlist = swheader_get_attribute_list(global_index, requisite_keyword, None)
    list = xlist
    while list:
        value = swheaderline_get_value(list[0], None)
        # Now token-ize this as space delimited

        # The data is in escaped internal form, hence we must
        # (de)expand the "\\n" (for example) sequences to turn them
        # into the real char (a '\n' for example
        swlib_expand_escapes(str(None), None, value, tmp)
        depspec = strob_strtok(tmp, strob_str(tmp), " \n\r\t") # FIXME supported quoted spaces ???
        while depspec:
            swverid = swverid_open(None, depspec)
            if swverid is None:
                sw_e_msg(G, f"error processing software spec: {depspec}")
            else:
                next = swverid
                vplob_add(swspecs, next)
                while next := swverid_get_alternate(next):
                    vplob_add(swspecs, next)
                swverid_disconnect_alternates(swverid)

            # strob_strcpy(tmp2, "")
            # swverid_show_object_debug(swverid, tmp2, "")
            # fprintf(stderr, "%s\n", strob_str(tmp2))
            #
            depspec = strob_strtok(None, None, " \n\r\t")
        list = list[1:]

    del xlist
    strob_close(tmp)
    strob_close(tmp2)
    swheader_restore_state(global_index, None)
    return swspecs


def swpl_get_catalog_entries(G, swspecs, pre_swspecs, co_swspecs, ex_swspecs, target_path, swicol, target_fd0, target_fd1, opta, pgm_mode):

    E_DEBUG("START")
    retval = 0
    isc_script_buf = strob_open(10)

    # This section writes the product_tag revision vendor_tag
    # to stdout

    # write the task script into a buffer
    if swpl_test_pgm_mode(pgm_mode, swlibc.SWLIST_PMODE_PROD) == 0:
        swicat_write_isc_script(isc_script_buf, G, swspecs, None, None, None, swlibc.SWICAT_FORM_P1)
    elif swpl_test_pgm_mode(pgm_mode, swlibc.SWLIST_PMODE_DIR) == 0:
        swicat_write_isc_script(isc_script_buf, G, swspecs, None, None, None, swlibc.SWICAT_FORM_DIR1)
    elif swpl_test_pgm_mode(pgm_mode, swlibc.SWLIST_PMODE_DEP1) == 0:
        swicat_write_isc_script(isc_script_buf, G, None, pre_swspecs, None, ex_swspecs, swlibc.SWICAT_FORM_DEP1)
    else:
        sw_e_msg(G, "bad internal mode: %s\n", pgm_mode)
        swicat_write_isc_script(isc_script_buf, G, None, pre_swspecs, None, ex_swspecs, swlibc.SWICAT_FORM_DEP1)

    # Now send the SWBIS_TS_Get_iscs_listing task script
    # swicol_set_task_idstring(swicol, SWBIS_TS_Get_iscs_listing);


    swicol.needs_synct_eoa = 1
    ret = swicol_rpsh_task_send_script2(swicol, target_fd1, 512, target_path, strob_str(isc_script_buf), swlibc.SWBIS_TS_Get_iscs_listing)
    swicol.needs_synct_eoa = 0
    if ret != 0:
        E_DEBUG("")
        print("swicol_rpsh_task_send_script2 error", file=sys.stderr)
        return -1

    #	 Send 1 block to satisfy the task shell, all task shells
    #	   get atleast 1 block because some dd()'s can't read zero (0)
    #	   block
    E_DEBUG("")
    ret = etar_write_trailer_blocks(target_fd1, 1)
    if ret < 0:
        print("etar_write_trailer_blocks error", file=sys.stderr)
        return -2

    #	 Open a mem file just to collect the output and
    #	   run a sanity check
    aa_fd = swlib_open_memfd()

    # Now read the output from the target process
    E_DEBUG("")
    ret = swlib_synct_suck(aa_fd, target_fd0)
    E_DEBUG("")

    if ret < 0:
        # This can happen for good reasons such as 'no such file'
        # as well as internal error bad reasons
        # fprintf(stderr, "swlib_synct_suck error\n");
        E_DEBUG("")
        return -3

    # Now reap the task shell events
    ret = swicol_rpsh_task_expect(swicol, G.g_swi_event_fd, swlibc.SWICOL_TL_30)
    if ret != 0:
        if ret < 0:
            SWLIB_INTERNAL("")
        sw_e_msg(G, "swicol_rpsh_task_expect failed: status=%d\n", ret)
        return -4
    E_DEBUG("")
    os.lseek(aa_fd, 0, os.SEEK_SET)
    swicat_squash_null_bytes(aa_fd)
    os.lseek(aa_fd, 0, os.SEEK_SET)
    retval = aa_fd
    strob_close(isc_script_buf)
    return retval


def swpl_do_list_catalog_entries2(G, swspecs, pre_swspecs, co_swspecs, ex_swspecs, target_path, swicol, target_fd0, target_fd1, opta, pgm_mode):
    E_DEBUG("START")
    retval = 0
    memfd = swpl_get_catalog_entries(G, swspecs, pre_swspecs, co_swspecs, ex_swspecs, target_path, swicol, target_fd0, target_fd1, opta, pgm_mode)

    if memfd < 0:
        return memfd
    os.lseek(memfd, 0, os.SEEK_SET)

    # Now write the response from the target to local stdout
    if (swpl_test_pgm_mode(pgm_mode, swlibc.SWLIST_PMODE_PROD) == 0 or
        swpl_test_pgm_mode(pgm_mode, swlibc.SWLIST_PMODE_DIR) == 0 or
        G.g_verboseG >= swlibc.SWC_VERBOSE_3 or
        0):
        swlib_pipe_pump(sys.stdout.fileno(), memfd)

    if swpl_test_pgm_mode(pgm_mode, swplc.SWLIST_PMODE_DEP1) == 0:
        # Analyze the dependencies

        req = swicat_req_create()
        # Now get the NUL terminated text image
        memtext = uxfio_get_fd_mem(memfd, None, None)
        ret = swicat_req_analyze(G, req, memtext, None)
        if ret < 0 or ret > 0:
            sw_e_msg(G, "internal error analyzing dependencies, ret=%d\n", ret)
        else:
            pass
            # no error, dependencies successfully
            # queried independent of query result
        if swicat_req_get_pre_result(req) != 0 or swicat_req_get_ex_result(req) != 0 or False:
            # Failed dependency
            retval = 1
            swicat_req_print(G, req)
        swicat_req_delete(req)
    uxfio_close(memfd)
    return retval


def swpl_test_pgm_mode(pgm_mode, test_mode):
    if strcmp(test_mode, pgm_mode) == 0:
        return 0
    else:
        return 1

def swpl_get_catalog_tar(G, swi, target_path, upgrade_specs, target_fd0, target_fd1):
    E_DEBUG("")
    st = None
    isc_script_buf = strob_open(100)

    # write a tar archive of the selection that will be upgraded because a
    # removal of the fileset file and catalog entry is required

    # Note: the following function is used in the 'swlist -c -' operation,
    # we reuse it here,  this will get the tarblob catalog entries of all
    # the specs in the list (VPLOB *) upgrade_specs

    E_DEBUG("")
    swicat_write_isc_script(isc_script_buf, G, upgrade_specs, None, None, None, swicatc.SWICAT_FORM_TAR1)

    # swicol_set_task_idstring(swi->swicol, SWBIS_TS_Get_iscs_entry); 

    E_DEBUG("")
    ret = swicol_rpsh_task_send_script2(swi.swicol, target_fd1, 512, target_path, strob_str(isc_script_buf), swlibc.SWBIS_TS_Get_iscs_entry)

    E_DEBUG("")
    if ret != 0:
        E_DEBUG("")
        swicol_set_master_alarm(swi.swicol)
        # sw_e_msg(G, "swicol: fatal: %s:%d ret=%d\n", __FILE__, __LINE__, ret); 

    if ret == 0:
        E_DEBUG("")
        ret = etar_write_trailer_blocks(target_fd1, 1)
    else:
        E_DEBUG("")
        ret = -1

    E_DEBUG("")
    cfd = swlib_open_memfd()
    if swicol_get_master_alarm_status(swi.swicol) == 0 and ret == 512:
        taru = taru_create()
        E_DEBUG("")
        ret = taru_process_copy_out(taru, target_fd0, cfd, None, None, af.arf_ustar, -1, -1, None, None)
        taru_delete(taru)
        if ret < 0:
            return -1
    else:
        E_DEBUG("")
        return -2
    E_DEBUG("")
    ret = swicol_rpsh_task_expect(swi.swicol, G.g_swi_event_fd, swlibc.SWICOL_TL_30)
    if os.lseek(cfd, 0, os.SEEK_SET) < 0:
        E_DEBUG("")
        return -3

    E_DEBUG("")
    if os.lseek(cfd, 0, os.SEEK_SET) < 0:
        E_DEBUG("")
        return -4
    #	
    #	swlib_tee_to_file("/tmp/_catalog1.tar", cfd, NULL, -1, 0)
    #	

    E_DEBUG("")
    if os.lseek(cfd, 0, os.SEEK_SET) < 0:
        return -5
    oflags = 0

    E_DEBUG("")
    swvarfs = swvarfs_opendup(cfd, oflags, 0)
    while (path := swvarfs_get_next_dirent(swvarfs, st)) and len(path):
        E_DEBUG("")
        if G.devel_verbose:
            sys.stderr.write(f"<debug>: path=[{path}]\n")

    E_DEBUG("")
    swvarfs_dirent_reset(swvarfs)
    E_DEBUG("")

    while (path := swvarfs_get_next_dirent(swvarfs, st)) is not None and len(path) != 0:
        if G.devel_verbose:
            sys.stderr.write(f"<debug>: path=[{path}]\n")
    E_DEBUG("")
    swvarfs_close(swvarfs)
    isc_script_buf.close()
    E_DEBUG2("returning %d", cfd)
    return cfd

def swpl_bashin_detect(buf):


    """

    New and Improved July 2014

        Here is the code to detect a shell that complies with
        the POSIX prescribed handling of STDIN (which is not to
        read ahead but rather read 1 byte at a time for the
        single command or compound command).  To get around shells
        that don't do this, initially we pad with 1024 bytes of "####..."
        which assumes that the shell is improperly gobbling only
        1024 bytes at a time, then we test and exec the POSIX shell.

        ASIDE: the original ash shell gobbles 8kb per read() call,
        IMHO this is terrible and does seem to be outside the mainstream
        of traditional /bin/sh behaviour, systems with ash as /bin/sh are
        broken as far as swbis interoperability is concerned. (Note, dash
        has the read() size reduced, dash is supported by swbis).
            For example the heirloom shell gobbles stdin at 128 bytes per read()
            ash and (unpatched) dash at 8192 and Debian dash at 512. Older
     	versions of the Korn shell read at 512 bytes per read().

     	 test for shell  with required POSIX feature
     		(echo "(sed -e s/a/d/);"; echo /)  | ksh -s 1>/dev/null 2>&1; echo $?
     	or better, for example
     		$ (echo "(read A; exit 0)"; echo /) | /bin/ash -s; echo $?
     		/: permission denied
     		126
     		$ (echo "(read A; exit 0)"; echo /) | /bin/sh -s; echo $?
    		0
    """

    prog = swlib_utilname_get()
    if prog == "swremove":
        # this if-else is here so when using the --cleansh option
        #   the process is disguised
        prog = "/" # assumes that "/" is harmless to execute
        swbis = "/"
    else:
        swbis = "/_swbis"

    buf.append(f"""{{
    export XXX_PKG
    export XXX_PGM
    export XXX_SH
    XXX_PKG="{swbis}"
    XXX_PGM="{prog}"
    XXX_SH=/bin/bash
    if [ -x "$XXX_SH" ]; then
        $XXX_SH -s 1>/dev/null 2>&1 << HERE
        (read A; exit 0);
        /
        HERE
        case $? in
        0)
        exec $XXX_SH -s $XXX_PKG /_$XXX_PGM / / PSH="\\"$XXX_SH -s\\""
        exit 121
        ;;
        esac
    }}
    XXX_SH=/bin/ksh
    if [ -x "$XXX_SH" ]; then
        $XXX_SH -s 1>/dev/null 2>&1 << HERE
        (read A; exit 0);
        /
        HERE
        case $? in
        0)
        exec $XXX_SH -s $XXX_PKG /_$XXX_PGM / / PSH="\\"$XXX_SH -s\\""
        exit 121
        ;;
        esac
    }}
    XXX_SH=/usr/xpg4/bin/sh
    if [ -x "$XXX_SH" ]; then
        $XXX_SH -s 1>/dev/null 2>&1 << HERE
        (read A; exit 0);
        /
        HERE
        case $? in
        0)
        exec $XXX_SH -s $XXX_PKG /_$XXX_PGM / / PSH="\\"$XXX_SH -s\\""
        exit 121
        ;;
        esac
    }}
    XXX_SH=/bin/sh
    if [ -x "$XXX_SH" ]; then
        $XXX_SH -s 1>/dev/null 2>&1 << HERE
        (read A; exit 0);
        /
        HERE
        case $? in
        0)
        exec $XXX_SH -s $XXX_PKG /_$XXX_PGM / / PSH="\\"$XXX_SH -s\\""
        exit 121
        ;;
        esac
    }}
    echo "swbis: $XXX_PGM: no suitable shell found on host `hostname`" 1>&2
    echo "swbis: $XXX_PGM: fatal, exiting with status 126" 1>&2
    exit 126
    }}""")
    make_padding_for_login_shell(16, buf)# needs to be 16 to support ash as the
    #						 account holders login shell because the
    #						 ash read() size is 8192 bytes

def swpl_check_package_signatures(G, swi, p_num_checked):
    sig_block_start = 0
    sig_block_end = 0
    if p_num_checked:
        p_num_checked[0] = 0
    num_checked = 0
    retval = 0
    swgpg = swgpg_create()
    gpg_status = strob_open(33)
    archive_files = swi.swi_pkg.dfiles.archive_files

    swi_examine_signature_blocks(swi.swi_pkg.dfiles, sig_block_start, sig_block_end)
    signed_bytes_fd = swlib_open_memfd()
    swpl_write_catalog_data(swi, signed_bytes_fd, sig_block_start, sig_block_end)

    i = 0
    while afile := cplob_val(archive_files, i):
        i += 1
        if (s := afile.pathname.find("/" + swc.SW_A_signature)) != -1 and afile.pathname[s + len("/" + swc.SW_A_signature)] == '\0':
            # Got a signature
            # fprintf(stderr, "GOT a sig [%s]\n", afile->pathname);

            os.lseek(signed_bytes_fd, 0, os.SEEK_SET)
            strob_memset(gpg_status, ord('\0'), 500)
            # ret = swgpg_run_gpg_verify(swgpg, signed_bytes_fd, afile.data, G.g_verboseG, gpg_status)
            ret = swgpg_determine_signature_status(gpg_status.decode(), -1)
            # FIXME for some reason swgpg_run_gpg_verify returns non-zero)
            if ret != 0 or G.g_verboseG >= swlibc.SWC_VERBOSE_3:
                print(gpg_status.decode(), file=sys.stderr)
            if ret == 0:
                retval += 1
            num_checked += 1

    if p_num_checked:
        p_num_checked[0] = num_checked
    os.close(signed_bytes_fd)
    swgpg_delete(swgpg)
    strob_close(gpg_status)
    return retval

def swpl_make_package_signature(G, swi):
    sig_block_start = 0
    sig_block_end = 0
    signer_status = 0
    #
    # always use agent and environment variables
    # get gpg_name and gpg_path from the environment
    # GNUPGHOME and GNUPGNAME
    #
    gpg_name = os.getenv("GNUPGNAME")
    gpg_path = os.getenv("GNUPGHOME")

    E_DEBUG("")
    if not gpg_name or not gpg_path:
        # FIXME message here
        if not gpg_name != '\0':
            print(f"{swlib_utilname_get()}: GNUPGNAME not set")
        if (not gpg_path) != '\0':
            print(f"{swlib_utilname_get()}: GNUPGHOME not set")
        return None

    E_DEBUG("")
    sigcmd: 'SHCMD' = swgpg_get_package_signature_command("GPG", gpg_name, gpg_path, "SWGPG_SWP_PASS_AGENT")

    E_DEBUG("")
    swi_examine_signature_blocks(swi.swi_pkg.dfiles, sig_block_start, sig_block_end)
    E_DEBUG("")
    signed_bytes_fd = swlib_open_memfd()
    E_DEBUG("")
    swpl_write_catalog_data(swi, signed_bytes_fd, sig_block_start, sig_block_end)
    E_DEBUG("")
    os.lseek(signed_bytes_fd, 0, os.SEEK_SET)

    E_DEBUG("")
    sig = swgpg_get_package_signature(sigcmd, signer_status, swlibc.SWGPG_SWP_PASS_AGENT, None, signed_bytes_fd, 0, None)

    #
    #	if (sig)
    #		fprintf(stderr, "%s", sig)
    #
    E_DEBUG("")
    uxfio_close(signed_bytes_fd)
    E_DEBUG("")
    return sig

def swpl_make_environ_transfer_image(buf):
    environ_exclude_chars = swlibc.SW_WS_TAINTED_CHARS + swlibc.SW_TAINTED_CHARS + "-:;"
    environ_list = os.environ.copy()
    buf.str = ""
    def environ_exclude(varname):
        return any(char in varname for char in environ_exclude_chars)

    for att in environ_list:
        q = att.find("=")
        if q == -1:
            continue
        varname = att[:q]
        value = att[q+1:]
        if environ_exclude(varname):
            continue
        if re.search(r"[`" + r"\s" + r"\-:;]", varname):
            continue
        if re.search(r"`", value):
            continue
        value = value.replace('"', r'\"')
        # Now escape " and $ chars
        value = value.replace('$', r'\$')
        # Now with the '"' and '$' escaped and tainted chars
        # checked the following shell expression should be safe
        #
        # VAR=${VAR="<value>"}
        buf.str += f"export {varname}\n{varname}=\"{value}\"\n"
    return buf.str


def swpl_run_check_script(G, script_name, id_string, swi, ofd, event_fd, p_check_status):
    ret: int = 0
    check_script: swlibc.SWI_CONTROL_SCRIPT = None
    stdin_file_size: int = 512
    this_index: int = 0
    buf = strob_open(400)

    ret = swpl_construct_analysis_script(G, script_name, buf, swi, check_script)
    assert ret == 0

    swicol_set_task_idstring(swi.swicol, id_string)
    ret = swicol_rpsh_task_send_script2(swi.swicol, ofd, 512, swi.swi_pkg.target_path, strob_str(buf), id_string)

    if ret == 0:
        #
        # Send the payload, in this case, it is a gratuitous block of nulls.
        #
        ret = etar_write_trailer_blocks(ofd, 1)
        if ret < 0:
            return -1
        #
        # wait for the script to finish
        #
        ret = swicol_rpsh_task_expect(swi.swicol, event_fd, swlibc.SWICOL_TL_9)
        if ret < 0:
            return -1

        #
        # this shows only the events for this task script
        #
        if swi.debug_events:
            swicol_show_events_to_fd(swi.swicol, pty.STDERR_FILENO, -1)

        check_message = swicol_rpsh_get_event_message(swi.swicol, swlibc.SW_CONTROL_SCRIPT_BEGINS, -1, this_index)

        p_check_status[0] = -1
        if check_message != '\0':
            #
            # Perform a sanity check on the message
            assert check_message.find(script_name) != -1
            # record the result
            script_status = swicol_rpsh_get_event_status(swi.swicol, None, swlibc.SW_CONTROL_SCRIPT_ENDS, this_index, this_index)
            check_script.result = script_status
            p_check_status[0] = script_status
        else:
            # This is OK
            # There must not have been a check script
            pass
    strob_close(buf)
    return ret

def swpl_cisf_product_create(product):
    E_DEBUG("")
    x = CISF_PRODUCT()
    x.swi = None
    x.product = None
    swpl_cisf_init_base(x.cisf_base)
    x.cisf_base.at = swheader_state_create()
    x.cisf_base.typeid = swplc.CISF_ID_PRODUCT
    x.cisf_base.ixfile = product.xfile
    # FIXME, need to copy all of product->pbase
    x.cisf_base.ixfile.base.b_tag = product.p_base.b_tag
    x.isets = vplob_open()
    x.cba = CISFBA()
    x.cba.base_array = vplob_open()
    return x

def swpl_cisf_init_base(x):
    x.ixfile = None
    x.at = None
    x.cf_index = -1
    x.typeid = -1
    return x
def swpl_cisf_fileset_create():
    E_DEBUG("")
    x = CISF_FILESET()
    swpl_cisf_init_base(x.cisf_base)
    x.cisf_base.at = swheader_state_create()
    x.cisf_base.typeid = swplc.CISF_ID_FILESET
    return x

def swpl_cisf_product_delete(x):
    swheader_state_delete(x.cisf_base.at)
    vplob_close(x.isets)
    del x

def swpl_cisf_fileset_delete(x):
    swheader_state_delete(x.cisf_base.at)
    del x

def swpl_cisf_init_single_single(x, swi):
    product = swi_package_get_product(swi.swi_pkg, 0)
    fileset = swi_product_get_fileset(product, 0)
    x.product = product
    x.cisf_base.cf_index = 0
    x.cisf_base.ixfile = product.xfile

    if (xf := vplob_val(x.isets, 0)) is None:
        xf = swpl_cisf_fileset_create()
        vplob_add(x.isets, xf)
    else:
        pass
    xf.cisf_base.ixfile = fileset
    xf.cisf_base.cf_index = 0

def swpl_run_make_installed_live(G, swi, ofd, target_path):
    tmp = None
    try:
        tmp = io.StringIO()
        swi.swicol.set_task_idstring(swcc.SWBIS_TS_MAKE_LIVE_INSTALLED)
        tmp.write(f"""
        cd "{swi.swi_pkg.catalog_entry}"
        case "$?" in
            0)
                sw_retval=0
                ;;
            *)
                sw_retval=1
                ;;
        esac
        case "$sw_retval" in
            0)
                mv -f {swc.SW_A__INSTALLED} {swc.SW_A_INSTALLED}
                sw_retval=$?
                ;;
        esac
        dd count=1 bs=512 of=/dev/null 2>/dev/null
        """)
        ret = swi.swicol.rpsh_task_send_script2(
            ofd,
            512,
            target_path,
            tmp.getvalue(),
            swcc.SWBIS_TS_MAKE_LIVE_INSTALLED
        )
        if ret != 0:
            return -1
        ret = etar_write_trailer_blocks(ofd, 1)
        if ret < 0:
            SW_ERROR_IMPL()
            SWI_internal_error()
            return -1
        ret = swi.swicol.rpsh_task_expect(G.g_swi_event_fd, icolc.SWICOL_TL_8)
        if ret != 0:
            return -1
    finally:
        if tmp:
            tmp.close()
    return 0


def swpl_run_check_overwrite(G, fl, swi, ofd, target_path, fp_keep_fd):
    retval = 0
    tmp = strob_open(32)
    stopbuf = strob_open(32)
    payload = strob_open(100)
    scriptbuf = strob_open(100)
    target_fd0 = G.g_target_fdar[0]
    swpl_print_file_list_to_buf(fl, payload)
    ret = swpl_make_here_document_source(G, tmp, payload)
    if ret:
        retval += 1
    swicol_set_task_idstring(swi.swicol, "SWBIS_TS_CHECK_OVERWRITE")

    #	 these two subshells are closed by the
    #	   shell code generated in swlib_append_synct_eof()
    scriptbuf.append("sw_retval=0\n"
                     "(\n"
                     "(\n"
                     "dd count=1 bs=512 of=/dev/null 2>/dev/null\n"
                     "# result=\"\"\n"
                     "IFS=\"\n\t\"\n"
                     "for file in `\n"
                     "%s"
                     "`\n"
                     "do\n"
                     "       if test -e \"$file\"; then\n"
                     "               # result=exist\n"
                     "               echo \"$file\"\n"
                     "       fi\n"
                     "#%s\n"
                     "done\n"
                     " " % (tmp.getvalue(), swevent_code(swlibc.SW_FILE_EXISTS)))
    swlib_append_synct_eof(scriptbuf)
    swi.swicol.needs_synct_eoa = 1
    ret = swicol_rpsh_task_send_script2(swi.swicol, ofd, 512, target_path, strob_str(scriptbuf), swcc.SWBIS_TS_CHECK_OVERWRITE)
    swi.swicol.needs_synct_eoa = 0

    if ret != 0:
        E_DEBUG("")
        strob_close(tmp)
        return -1

    E_DEBUG("")
    ret = etar_write_trailer_blocks(ofd, 1)
    if ret < 0:
        E_DEBUG("")
        strob_close(tmp)
        SW_ERROR_IMPL()
        SWI_internal_error()
        return -2

    # Open a mem file
    if fp_keep_fd < 0:
        keep_fd = swlib_open_memfd()
    else:
        keep_fd = fp_keep_fd

    # Now read the output from the target process
    E_DEBUG("")
    ret = swlib_synct_suck(keep_fd, target_fd0)
    E_DEBUG("")

    if ret < 0:
        E_DEBUG2("swlib_synct_suck ret=%d", ret)
        retval = -3

    E_DEBUG("")
    os.lseek(keep_fd, 0, os.SEEK_SET)
    swicat_squash_null_bytes(keep_fd)
    os.lseek(keep_fd, 0, os.SEEK_SET)

    E_DEBUG("")

    # Now reap the task shell events
    ret = swicol_rpsh_task_expect(swi.swicol, G.g_swi_event_fd, icolc.SWICOL_TL_500)
    E_DEBUG("")
    if ret != 0:
        E_DEBUG("")
        retval = -4

    if fp_keep_fd < 0:
        swlib_close_memfd(keep_fd)

    E_DEBUG("")
    strob_close(tmp)
    strob_close(stopbuf)
    strob_close(payload)
    E_DEBUG("")
    return retval

def missing_which(pgm: str) -> Tuple[str, bool]:
    try:
        name = subprocess.check_output(['which', pgm], stderr=subprocess.DEVNULL).decode().strip()
        return name, True
    except subprocess.CalledProcessError:
        return '/', False
def swpl_run_get_prelink_filelist(G, swicol, ofd, pfp_prelink_fd):
    target_path = "/"
    tmp = strob_open(32)
    scriptbuf = strob_open(100)
    target_fd0 = G.g_target_fdar[0]
    retval = 0
    script = f"""
    PATH=/usr/sbin:/usr/bin:/sbin:"$PATH"
    missing_which() {{
        xxa_pgm="$1"
        xxa_name=$(which "$xxa_pgm" 2>/dev/null)
        test -f "$xxa_name" -o -h "$xxa_name"
        case "$?" in
        0) echo "$xxa_name"; return 0;;
        *) echo "/"; return 1;;
        esac
        return 0
    }}
    PRELINK=$(missing_which prelink)
    case "$?" in
    0)
        "$PRELINK" -v -p |
        grep -v '^ ' |
        sed -e 's/[(\[].*' -e 's/:$' -e 's/[ \t]*$' |
        sort |
        dd 2>/dev/null
    ;;
    *)
    ;;
    esac
    """
    swicol.set_task_idstring(swicol, 'SWBIS_TS_Get_prelink_filelist')

    # these two subshells are closed by the
    # shell code generated in swlib_append_synct_eof()
    script += """
    sw_retval=0
    (
    (
    dd count=1 bs=512 of=/dev/null 2>/dev/null
    IFS=" \t\n"
    """
    script += script

    script += """
    )
    )
    """

    #
    # This routine closes the two sub-shell parentheses above
    #

    swlib_append_synct_eof(scriptbuf)
    swicol.needs_synct_eoa = 1
    ret = swicol.rpsh_task_send_script2(swicol, ofd, 512, target_path, script, 'SWBIS_TS_GET_PRELINK_FILELIST')
    swicol.needs_synct_eoa = 0

    if ret != 0:
        E_DEBUG("")
        strob_close(tmp)
        pfp_prelink_fd = -1
        return -1

    E_DEBUG("")

    #
    # send one (1) block of NULS (scripts that read no data read at least one block
    #
    ret = etar_write_trailer_blocks(ofd, 1)
    if ret < 0:
        E_DEBUG("")
        strob_close(tmp)
        SW_ERROR_IMPL()
        SWI_internal_error()
        return -2

    #
    # Open a mem file
    #
    prelink_fd = swlib_open_memfd()

    #
    # Now read the output from the target process
    #
    E_DEBUG("")
    ret = swlib_synct_suck(prelink_fd, target_fd0)
    E_DEBUG("")

    if ret < 0:
        E_DEBUG2("swlib_synct_suck ret=%d", ret)
        retval = -3

    E_DEBUG("")
    os.lseek(prelink_fd, 0, os.SEEK_SET)
    swicat_squash_null_bytes(prelink_fd)
    os.lseek(prelink_fd, 0, os.SEEK_SET)

    E_DEBUG("")

    #
    # Now reap the task shell events
    #
    ret = swicol_rpsh_task_expect(swicol, G.g_swi_event_fd, icolc.SWICOL_TL_500)
    E_DEBUG("")
    if ret != 0:
        E_DEBUG("")
        retval = -4

    E_DEBUG("")
    strob_close(tmp)
    E_DEBUG("")

    if retval == 0:
        pfp_prelink_fd = prelink_fd
    else:
        swlib_close_memfd(prelink_fd)
        pfp_prelink_fd = -1
    return retval

def swpl_agent_fail_message(G, current_arg, status):
    swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_1, G.g_logspec, swc_get_stderr_fd(G), "SW_AGENT_INITIALIZATION_FAILED for target %s: status=%d\n", current_arg, status)
    if status == 255:
        swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_1, G.g_logspec, swc_get_stderr_fd(G), "possible cause: invalid authentication credentials\n")
    elif status == 127:
        swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_1, G.g_logspec, swc_get_stderr_fd(G), "possible cause: the specified shell not found\n")
    elif status == 126:
        swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_1, G.g_logspec, swc_get_stderr_fd(G), "swbis is unable to operate because the specified or alternate shell program (sh)\n" + "%s: conforming to POSIX 1003.1 at locations /bin/bash, /bin/ksh, /bin/mksh,\n" + "%s: /usr/xpg4/bin/sh, or /bin/sh was not found (or found and not suitable).\n", swlib_utilname_get(), swlib_utilname_get())
    elif status == 124:
        swlib_doif_writef(G.g_verboseG, swlibc.SWC_VERBOSE_1, G.g_logspec, swc_get_stderr_fd(G), "swbis is unable to operate because the shell found in PATH returned by\n" + "%s: getconf was found not conforming to POSIX 1003.1\n", swlib_utilname_get())

