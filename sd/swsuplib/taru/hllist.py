# hllist.py hard link list object.
#
#   Copyright (C) 1999  James H. Lowe, Jr.
#   Copyright (C) 2023-2024 Paul Weber, conversion to python
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


class hllist_entry:
    def __init__(self, path_, dev_, ino_, v_dev_, v_ino_):
        self.path_ = path_
        self.dev_ = dev_
        self.ino_ = ino_
        self.v_dev_ = v_dev_
        self.v_ino_ = v_ino_

class HLLIST:
    def __init__(self):
        self.disable_find_ = 0
        self.disable_add_ = 0
        self.list_ = []

def hllist_open():
    hllist = HLLIST()
    hllist.list_.append(None)
    hllist.disable_find_ = 0
    hllist.disable_add_ = 0
    return hllist

def hllist_close(hllist):
    for en in hllist.list_:
        if en:
            free_entry(en)
    hllist.list_ = []
    del hllist

def hllist_add_record(hllist, path, dev, ino):
    if hllist.disable_add_:
        return
    hllist_add_vrecord(hllist, path, dev, ino, 0, 0)

def hllist_add_vrecord(hllist, path, dev, ino, v_dev, v_ino):
    en = hllist_entry(path, dev, ino, v_dev, v_ino)
    if hllist.disable_add_:
        return
    hllist.list_.insert(-1, en)
    hllist.list_.append(None)

def hllist_find_file_entry(hllist, dev, ino, occurance, nfound):
    i = 0
    for en in hllist.list_:
        if en:
            if en.ino_ == ino and en.dev_ == dev:
                nfound[0] += 1
                if 0 < occurance == nfound[0]:
                    return en
        i += 1
    return None

def hllist_show_to_file(hllist, fp):
    i = 0
    for en in hllist.list_:
        if en:
            fp.write(f"Entry {i}: BEGIN\n")
            fp.write(f"dev={en.dev_} ino={en.ino_}\n")
            fp.write(f"v_dev={en.v_dev_} v_ino={en.v_ino_}\n")
            fp.write(f"path={en.path_}\n")
            fp.write(f"Entry {i}: END\n")
        i += 1

def hllist_disable_add(hllist):
    hllist.disable_add_ = 1

def hllist_disable_find(hllist):
    hllist.disable_find_ = 1

def hllist_clear_entries_and_disable(hllist):
    for en in hllist.list_:
        if en:
            del en
    hllist.list_ = []
    hllist.list_.append(None)
    hllist.disable_find_ = 1

def free_entry(en):
    if en.path_:
        en.path_ = ""

