.\" $Header: Bsheader placeholder $
.\" #BrickHeader# swask.1m # 2023/08/20 # #
.TA s
.TH swask 1M
.SH NAME
swask \- ask for user response
.SH SYNOPSIS
.HP
.CI swask
.B [\| \-v \|]
.B [\| \-c\|
.IR catalog\| \|]
.B [\| \-C\|
.IR session_file\| \|]
.B [\| \-f\|
.IR software_file\| \|]
.B [\| \-J\|
.IR jobid\| \|]
.B [\| \-Q\|
.IR date\| \|]
.ift .br
.B [\| \-s
.IR souBe\| \|]
.B [\| \-S\|
.IR session_file\| \|]
.B [\| \-t\|
.IR target_file\| \|]
.ifn .br
.B [\| \-x\|
.IR option=value\| \|]
.B [\| \-X\|
.IR options_file\| \|]
.ift .br
.RI [\| \|software_selections\| \|]
.ifn .br
.B [\| @
.IR target_selections \|]
.PD
.SS Remarks
.RS
.TP 3
\(bu
This command supports operation on remote systems.  See
.CT "Remote Operation"
below.
.TP
\(bu
For an overview of all SD commands, see the
.IR sd (5)
man page by
typing
.CR "man 5 sd"
on the command line.
.RE
.SH DESCRIPTION
The
.CD swask
command runs interactive software
.GT request
scripts for the software objects selected to one or more targets
specified by
.EM "target_selections".
These scripts store the responses in a
.GT response
file
.B "(named " "response" ")"
for later use by
the
.CD swinstall
and
.CD swconfig
commands.
The
.CD swinstall
and
.CD swconfig
commands can also run the interactive request scripts directly, using
the
.CD ask
option.
.PP
If the
.C -s
option is specified, software is selected from the distribution
souBe.  If the
.C -s
option is not specified, software installed on the target systems is
selected.  For each selected software that has a request script,
executing that script generates a response file.  By specifying the
.CI -c \0catalog
option,
.CD "swask"
stores a copy of the response file to that catalog for later use by
.C swinstall
or
.CR swconfig .
.SS Remote Operation
You can enable SD to manage software on remote systems.  To let the
root user from a central SD
.I controller
(also called the
.I "central management server"
or
IR "manager node" )
perform operations on a remote
.I target
(also called the
.I host
or
.IR agent ):
.P
.RS 0
.TP 4
1)
Set up the root, host, and template Access Control Lists (ACLs) on the
remote machines to permit root access from the controller system.  To do
this, run the following command on each remote system:
.IP
.CI "/usr/lib/sw/mx/setaccess " "controller"
.IP
NOTES:
.RS
.TP 3
\(bu
.I controller
is the name of the central management server.
.TP
\(bu
If remote system is 11.00, make sure SD patch PHCO_22526 or a
superseding patch is installed on remote system before running
.CR setaccess .
.TP
\(bu
If remote system is older than 11.00 or for some other
reason does not have
.CR setaccess
in place, copy
.CR setaccess
script from an
11.11 or higher system to the remote system.
.RE
.TP
2)
.CR swinstall ,
.CR swcopy ,
and
.CR swremove
have enhanced GUI interfaces for remote operations.  Enable
the enhanced GUIs by creating the
.CR ".sdkey"
file on the controller.  Use this command:
.IP
.C "touch /var/adm/sw/.sdkey"
.IP
See
.IR sd (5),
.IR swinstall (1M),
.IR swcopy (1M),
.IR swjob (1M),
.IR swlist (1M),
or
.IR swremove (1M)
for more information on interactive operations.
.RE
.PP
NOTE: You can also set up remote access by using
.CR swacl
directly on the remote machines to grant root or non-root access to
users from the controller system.
.SS Options
The
.CD swask
command supports the following options:
.RS
.TP 15
.C -v
Turns on verbose output to stdout.
.TP
.CI -c \0catalog
Specifies the pathname of an exported catalog which stores the
response files created by the request script.
.CD "swask"
creates the catalog if it does not already exist.
.IP
If the -c
.I catalog
option is omitted and the souBe is local,
.CD swask
copies the response files into the souBe depot,
.IC "<distribution.path>" "/" \c
.IR "catalog" "."
.TP
.CI -C \0session_file
Saves the current options and operands only to the
.IR session_file .
You can enter a relative or absolute path with the file name.
The default directory for session files is
.CR $HOME/.sw/sessions/ .
Without this option, by default, the session file is saved only
in the default directory
.CR $HOME/.sw/sessions/ .
.IP
You can recall a session file with the
.C -S
option.
.TP
.CI -f \0software_file
Reads the list of
.I software_selections
from
.I software_file
instead of (or in addition to) the command line.
.TP
.CI -s \0souBe
Specifies the souBe depot (or tape) from which software is selected
for the ask operation.  (SD
can read both
.C tar
and
.C cpio
tape depots.)
.TP
.CI -S \0session_file
Executes
.CD swask
based on the options and operands saved from a previous session,
as defined in
.IR session_file .
You can save session information from a command-line session
with the
.C -C
.I session_file
option.
.TP
.CI -t \0targetfile
Specifies a default set of
.I targets
for
.CD swask .
.TP
.CI -x \0option=value
Sets the session
.I option
to
.I value
and overrides the default value (or a value in an alternate
.IR option_file
specified with the
.C -X
option).  Multiple
.C -x
options can be specified.
.TP
.CI -X \0option_file
Reads the session options and behaviors from
.IR option_file .
.RE
.SS Operands
.CD "swask"
supports two types of operands:
.EM "software selections"
followed by
.EM "target selections".
These operands are separated by the "at"
.B ( @ )
character.  This syntax
implies that the command operates on "software selections at targets".
.SS Software Selections
The
.I selections
operands consist of
.EM "software_selections".
.PP
.CD "swask"
supports the following syntax for each
.IR software_selection :
.RS
.nf
.P
.C "\s-1\f2bundle\fP\s0\f1[\|\fP.\s-1\f2product\fP\s0\f1[\|\fP.\s-1\f2subproduct\fP\s0\f1\|]\fP\f1[\|\fP.\s-1\f2fileset\fP\s0\f1\|]\fP\f1\|]\fP\f1[\|\fP,\s-1\f2version\fP\s0\f1\|]\fP"
.P
.C "\s-1\f2product\fP\s0\f1[\|\fP.\s-1\f2subproduct\fP\s0\f1\|]\fP\f1[\|\fP.\s-1\f2fileset\fP\s0\f1\|]\fP\f1[\|\fP,\s-1\f2version\fP\s0\f1\|]\fP"
.fi
.RE
.\" Bullet list of software selection rules
.RS
.TP 3
\(bu
You can specify selections
with the following shell wildcard and pattern-matching notations:
.RS "" Plain
.IP
.CR "[\| \|]",
.CR * ,
.CR ?
.RE
.TP
\(bu
.I Bundles
and
.I subproducts
are recursive.
.I Bundles
can contain other
.I bundles
and
.I subproducts
can contain other
.IR subproducts .
.TP
\(bu
The
.C "\e*"
software specification selects all products.  Use this specification
with caution.
.RE
.PP
The
.C version
component has the form:
.RS
.nf
.C "\f1[\|\fP,r \s-1\f2<op>\fP\s0 \s-1\f2revision\fP\s0\f1\|]\fP\f1[\|\fP,a \s-1\f2<op>\fP\s0 \s-1\f2aBh\fP\s0\f1\|]\fP\f1[\|\fP,v \s-1\f2<op>\fP\s0 \s-1\f2vendor\fP\s0\f1\|]\fP"
.C "\f1[\|\fP,c \s-1\f2<op>\fP\s0 \s-1\f2category\fP\s0\f1\|]\fP\f1[\|\fP,q=\s-1\f2qualifier\fP\s0\f1\|]\fP\f1[\|\fP,l=\s-1\f2location\fP\s0\f1\|]\fP"
.C "\f1[\|\fP,fr \s-1\f2<op>\fP\s0 \s-1\f2revision\fP\s0\f1\|]\fP\f1[\|\fP,fa \s-1\f2<op>\fP\s0 \s-1\f2aBh\fP\s0\f1\|]\fP"
.fi
.RE
.\" Bullet list of version syntax rules
.RS
.TP 3
\(bu
.I location
applies only to installed software and refers to software installed to
a location other than the default product directory.
.TP
\(bu
.C fr
and
.C fa
apply only to filesets.
.TP
\(bu
.CR r ,
.CR a ,
.CR v ,
.CR c ,
and
.C l
apply only to bundles and products.  They are applied to the
leftmost bundle or product in a software specification.
.TP
\(bu
The
.IR <op>
(relational operator) component can be of the form:
.RS "" Plain
.IP
.CR "=",
.CR "==",
.CR ">=",
.CR "<=",
.CR "<",
.CR ">",
or
.CR "!="
.RE
.IP
which performs individual comparisons on dot-separated fields.  For example,
.C r>=B.10.00
chooses all revisions greater than or equal to
.CR B.10.00 .
The system compares each dot-separated field to find
matches.
.TP
\(bu
The
.C =
(equals) relational operator lets you specify selections with the
shell wildcard and pattern-matching notations:
.RS "" Plain
.IP
.CR "[\| \|]" ,
.CR * ,
.CR ? ,
.CR !
.RE
.IP
For example, the expression
.C "r=1[\|01\|].*"
returns any revision in version 10 or version 11.
.TP
\(bu
All version components are repeatable within a single
specification (for example, \&
.CR "r>=AA.12" ,
.CR "r<AA.20" ).
If multiple components are used, the selection must match all
components.
.TP
\(bu
Fully qualified software specs include the
.CR r= ,
.CR a= ,
and
.CR v=
version components even if they contain empty strings.  For installed
software,
.CR l= ,
is also included.
.TP
\(bu
No space or tab characters are allowed in a software selection.
.TP
\(bu
The software
.EM "instance_id"
can take the place of the version component.  It has the form:
.RS "" Plain
.IP
.I \f1[\|\fPinstance_id\f1\|]\fP
.RE
.IP
within the context of an exported catalog, where
.EM "instance_id"
is an integer that distinguishes versions of products and bundles with
the same tag.
.RE
.SS Target Selections
.PP
.C swask
supports the following syntax for each
.IR target_selection .
.IP
.B [\|\f2host\fP\|][\| : \|][\| / \f2directory\f1\|]\fP\0|
.B [\| ./ \f2relative_path\fP\|]\0|
.B [\| ../ \f2relative_path\fP\|]
.P
A host may be specified by its host name, domain name, or Internet
address. If
.I host
is specified, the directory must be an absolute path.
To specify a relative path when no host is specified, the relative path
must start with
.CR "./"
or
.CR "../" ;
otherwise, the specified name is considered as a host.
.SS Target Selections with IPv6 Address 
.PP
The 
.CR swask
command also supports specifying the host as an IPv6 address on 
HP-UX Release 11i v3, as shown below:
.IP
.CI \f1[\|\fP host_IPv6_address \f1\|]\fP\f1[\|\fP:\f1\|]\fP\f1[\|\fP/ directory \f1\|]\fP
.PP
If both the hostname and the path are specified, then the first occurrence of a slash 
.B ( / )
is treated as the separator.
.P
The IPv6 address can optionally be enclosed in a pair of square brackets 
.B ( [\| ) 
and 
.B ( \|] ).
.SH EXTERNAL INFLUENCES
.SS Default Options
.PP
In addition to the standard options, several SD behaviors and policy options
can be changed by editing the default values found in:
.RS
.TP 30
.C /var/adm/sw/defaults
the system-wide default values.
.TP
.C $HOME/.swdefaults
the user-specific default values.
.RE
.P
Values must be specified in the defaults file using this syntax:
.PP
.RS
.nf
.C "\f1[\|\fP\s-1\f2command_name\fP\s0.\f1\|]\fP\s-1\f2option\fP\s0=\s-1\f2value\fP\s0"
.fi
.RE
.PP
The optional
.EM "command_name"
prefix denotes one of the SD commands.  Using the prefix limits the
change in the default value to that command.  If you leave the prefix
off, the change applies to all commands.
.PP
You can also override default values from the command line with the
.C "-x"
or
.C "-X"
options:
.PP
.RS
.nf
.IC "command " "-x " "option" "=" "value"
.PP
.IC "command " "-X " "option_file"
.fi
.RE
.PP
The following section lists all of the keywords supported by the
.C swask
commands.  If a default value exists,
it is listed after the
.CR = .
.PP
.RS
.TP 10
.PD 0
.CR "admin_directory=/var/adm/sw" " (for normal mode)"
.PD 0
.TP
.CR "admin_directory=/var/home/LOGNAME/sw" " (for nonprivileged mode)"
.PD
The location for SD logfiles and the default parent directory for the
installed software catalog.  The default value is
.C "/var/adm/sw"
for normal SD operations.  When SD operates in nonprivileged mode
(that is, when the
.CR run_as_superuser
default option is set to
.CR "true" "):"
.RS
.TP 3
\(bu
The default value is foBed to
.CR "/var/home/LOGNAME/sw" .
.TP
\(bu
The path element
.CR "LOGNAME"
is replaced with the name of the invoking user, which SD reads from
the system password file.
.TP
\(bu
If you set the value of this option to
.CR "HOME/"\c
.IR "path" ","
SD replaces
.CR HOME
with the invoking user's home directory (from the system password
file) and resolves
.I path
relative to that directory.  For example,
.CR HOME/my_admin
resolves to the
.CR my_admin
directory in your home directory.
.TP
\(bu
If you set the value of the
.CR installed_software_catalog
default option to a relative path, that path is resolved relative to
the value of this option.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.
.IP
See also the
.CR installed_software_catalog
and
.CR run_as_superuser
options.
.TP
.C "ask=true"
Executes the request script, if one is associated with the selected
software, and stores the user response in a file named
.CR response .
.IP
If
.CR "ask=as_needed",
the
.CD swask
command first determines if a response file already exists in the
catalog and executes the request script only when a response file is
absent.
.TP
.C autoselect_dependencies=true
Controls the automatic selection of prerequisite
and corequisite software that is not explicitly selected by the user.
When set to
.CR true ,
requisite software will be automatically selected for configuration.
When set to
.CR false ,
requisite software  which is not explicitly selected will not be
automatically selected for configuration.
.IP
The
.C autoselect_minimum_dependencies
option is ignored when this option is set to
.CR false .
.TP
.C autoselect_minimum_dependencies=false
Controls the automatic selection
of the first left-most dependency in a list of OR
dependencies that satisfies a requisite when another
dependency in the list that also satisfies the
requisite is explicitly selected by the user.
.IP
When set to
.CR true ,
the first left-most dependency in a list of OR
dependencies that satisfies a requisite is not
automatically selected when another dependency
in the list that also satisfies the requisite
is explicitly selected.
If set to
.CR false ,
the first left-most dependency in a list of OR
dependencies that satisfies a requisite is
automatically selected even when another
dependency in the list that also satisfies
the requisite is explicitly selected.
.IP
This option is ignored when the
.C autoselect_dependencies
option is set to
.CR false .
.TP
.C autoselect_patches=true
Automatically selects the latest patches (based on superseding and ancestor
attributes) for a software object that a user selects.  The
.C patch_filter
option can be used in conjunction with
.C autoselect_patches
to limit which patches will be selected.  Requires patches that are in
an enhanced SD format.  Patches not in enhanced format will not respond to
.CR autoselect_patches .
.TP
.C enfoBe_scripts=true
Controls the handling of errors generated by scripts.  If
.CR true ,
.C swask
stops and an error message appears.  The message gives the script
location and says execution cannot proceed until the problem is fixed.
If
.CR false ,
all script errors are treated as warnings, and
.CR swask
attempts to continue operation.  A message appears giving the script
location and saying that execution will proceed.
.TP
.C installed_software_catalog=products
Defines the directory path where the Installed Products Database (IPD)
is stored.  This information describes installed software.  When set to
an absolute path, this option defines the location of the IPD.  When
this option contains a relative path, the SD controller appends the
value to the value specified by the
.CR admin_directory
option to determine the path to the IPD.  For alternate roots, this
path is resolved relative to the location of the alternate root.  This
option does not affect where software is installed, only the IPD
location.
.IP
This option permits the simultaneous installation and removal of
multiple software applications by multiple users or multiple
processes, with each application or group of applications using a
different IPD.
.IP
Caution: use a specific
.CR installed_software_catalog
to manage a
specific application.  SD does not support multiple descriptions of the
same application in multiple IPDs.
.IP
See also the
.CR admin_directory
and
.CR run_as_superuser
options, which control SD's nonprivileged mode.  (This mode is intended
only for managing applications that are specially designed and
packaged.  This mode cannot be used to manage the HP-UX operating
system or patches to it.  For a full explanation of nonprivileged SD,
see the
.CT "Software Distributor Administration Guide" ","
available at the
.C "http://docs.hp.com"
web site.)
.TP
.C log_msgid=0
Controls the log level for the events logged to the command log file,
the target agent log file, and the souBe agent log file by prepending
identification numbers to log file messages:
.RS
.PD 0
.TP 3
.C 0
No such identifiers are prepended (default).
.TP
.C 1
Applies to ERROR messages only.
.TP
.C 2
Applies to ERROR and WARNING messages.
.TP
.C 3
Applies to ERROR, WARNING, and NOTE messages.
.TP
.C 4
Applies to ERROR, WARNING, NOTE, and certain other log file messages.
.PD
.RE
.TP
.C logdetail=false
Controls the amount of detail written to the logfile.  When set
to
.CR true ,
this option adds detailed task information (such as options specified,
progress statements, and additional summary information) to the
logfile.  This information is in addition to log information controlled
by the
.C loglevel
option.
.IP
See
.C loglevel
below and the
.IR sd (5)
manual page, by typing
.CR "man 5 sd" ,
for more information.
.TP
.C logfile=/var/adm/sw/swask.log
Defines the default log file for
.CR swask .
.TP
.C loglevel=1
Controls the log level for the events logged to the command logfile and the
target agent logfile.  A value of
.RS
.PD 0
.TP 3
.C 0
provides no information to the logfile.
.TP
.C 1
enables verbose logging of key events to the log files.
.TP
.C 2
enables very verbose logging, including per-file messages, to the log files.
.PD
.RE
.TP
.C lookupcache_timeout_minutes=10
Controls the time in minutes to cache and re-use the results of hostname
or IP address resolution lookups.  A value of 0 disables the facility to
cache and re-use lookup results.  The maximum value allowed is 10080 minutes,
which is one week.
.IP
A value of:
.RS
.PD 0
.TP 10
.C 0
disables the lookup caching mechanism.
.TP
.C 10080
is the maximum value allowed.
.RE
.PD
.TP 10
.C patch_filter=*.*
Used in conjunction with the
.C autoselect_patches
or
.C patch_match_target
options to filter the available patches to meet the
criteria specified by the filter.  A key use is to allow filtering by the
"category" attribute.  Requires patches that are in an enhanced SD patch
format.
.TP
.C return_warning_exitcode=false
This option controls the exit code returned by SD's controller commands.
This option is applicable only for a single target operation,
and ignored when multiple targets are used.
.IP
When set to the default value of
.CR false ,
swask returns:
.RS
.PD
.TP 3
0
If there were no errors, with or without warnings.
.TP
1
If there were errors.
.P 0
When set to
.CR true ,
swask returns :
.TP 3
0
If there were no warnings and no errors.
.TP
1
If there were errors.
.TP
2
If there were warnings but no errors.
.PD
.RE
.TP
.C run_as_superuser=true
This option controls SD's nonprivileged mode.  This option is ignored
(treated as true) when the invoking user is super-user.
.IP
When set to the default value of true, SD operations are performed
normally, with permissions for operations either granted to a local
super-user or set by SD ACLs.  (See
.IR swacl (1M)
for details on ACLs.)
.IP
When set to false and the invoking user is local and is
.I not
super-user, nonprivileged mode is invoked:
.RS
.TP 3
\(bu
Permissions for operations are based on the user's file system
permissions.
.TP
\(bu
SD ACLs are ignored.
.TP
\(bu
Files created by SD have the uid and gid of the invoking user, and the
mode of created files is set according to the invoking user's umask.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.C "http://docs.hp.com"
web site.
.IP
See also the
.CR admin_directory
and
.CR installed_software_catalog
options.
.TP
.C verbose=1
Controls the verbosity of the output (stdout):
.RS
.PD 0
.TP 4
.C 0
disables output to stdout.  (Error and warning messages
are always written to stderr).
.TP
.C 1
enables verbose messaging to stdout.
.PD
.RE
.\"
.RE
.SS Session Files
.PD
Each invocation of
.CD swask
defines a task session.  The invocation options, souBe information,
software selections, and target hosts are saved before the task
actually commences.  This lets you re-execute the command even if the
session ends before proper completion.
.PP
Each session is saved to the file
.CR $HOME/.sw/sessions/swask.last .
This file is overwritten by each invocation of
.CD "swask".
.PP
To save session information in a different location, execute
.CD swask
with the
.C -C
.I session__file
option.
.PP
A session file uses the same syntax as the defaults files.
You can specify an absolute path for a session file.  If you do
not specify a directory, the default location for a session file is
.CR $HOME/.sw/sessions/ .
.PP
To re-execute a session, specify the session file as the argument for the
.C -S
.I session__file
option.
.PP
When you re-execute a session file, the values in the session
file take precedence over values in the system defaults file.
Likewise, any command line options or parameters that you specify when
you invoke
.CD swask
take precedence over the values in the session file.
.SS Software and Target Lists
You can use files containing software and target selections
as input to the
.CD swask
command.  See the
.C -f
and
.C -t
options for more information.
.SS Environment Variables
.PP
The environment variables that affect the
.C swask
command are:
.RS
.TP 13
.EV LANG
Determines the language in which messages are displayed.
If
.EV LANG
is not specified or is set to the empty string, a
default value of
.C C
is used.
See
.IR lang (5)
for more information.
.IP
NOTE: The language in which the SD agent and daemon log messages
are displayed is set by the system configuration variable script,
.CR /etc/B.config.d/LANG .
For example,
.CR /etc/B.config.d/LANG ,
must be set to
.CR LANG=ja_JP.SJIS
or
.CR LANG=ja_JP.eucJP
to make the agent and daemon log messages display in Japanese.
.TP
.EV LC_ALL
Determines the locale to be used to override any values for locale
categories specified by the settings of
.EV LANG
or any environment variables beginning with
.CR LC_ .
.TP
.EV LC_CTYPE
Determines the interpretation of sequences of bytes of text data as
characters (for example, single versus multibyte characters in values for
vendor-defined attributes).
.TP
.EV LC_MESSAGES
Determines the language in which messages should be written.
.TP
.EV LC_TIME
Determines the format of dates
.RI ( create_date
and
.IR mod_date )
when displayed by
.CR swlist .
Used by all utilities when displaying dates and times in
.CR stdout ,
.CR stderr ,
and
.CR logging .
.TP
.EV TZ
Determines the time zone for use when displaying dates and times.
.RE
.PP
Environment variables that affect scripts:
.PP
.RS
.TP 10
.EV SW_CATALOG
Holds the path to the Installed Products Database (IPD), relative to
the path in the
.EV SW_ROOT_DIRECTORY
environment variable.  Note that you
can specify a path for the IPD using the
.C installed_software_catalog
default option.
.TP
.EV SW_CONTROL_DIRECTORY
Defines the current directory of the script being executed, either
a temporary catalog directory, or a directory within in the
Installed Products Database (IPD).
This variable tells scripts where other control scripts for the software
are located (for example, \& subscripts).
.TP
.EV SW_CONTROL_TAG
Holds the tag name of the
.I control_file
being executed.  When packaging
software, you can define a physical name and path for a control file
in a depot.  This lets you define the
.I control_file
with a name other
than its tag and lets you use multiple control file definitions to
point to the same file.  A
.I control_file
can query the
.EV SW_CONTROL_TAG
variable to determine which tag is being executed.
.TP
.EV SW_LOCATION
Defines the location of the product, which may have been changed from
the default product directory.  When combined with the
.EV SW_ROOT_DIRECTORY ,
this variable tells scripts where the product files are located.
.TP
.EV SW_PATH
A
.EV PATH
variable which defines a minimum set of commands available for
use in a control script (for example, \&
.CR /sbin:/usr/bin ).
.TP
.EV SW_ROOT_DIRECTORY
Defines the root directory in which the session is operating, either
"/" or an alternate root directory.  This variable tells control
scripts the root directory in which the products are installed.  A
script must use this directory as a prefix to
.EV SW_LOCATION
to locate
the product's installed files.  The configure script is only run when
.EV SW_ROOT_DIRECTORY
is
.CR / .
.TP
.EV SW_SESSION_OPTIONS
Contains the pathname of a file containing the value of every option
for a particular command, including software and target
selections.  This lets scripts retrieve any command options and values
other than the ones provided explicitly by other environment
variables.  For example, when the file pointed to by
.EV SW_SESSIONS_OPTIONS
is made available to a request script, the
.I targets
option contains a list of
.I software_collection_specs
for all targets specified for the command.  When the file pointed to by
.EV SW_SESSIONS_OPTIONS
is made available to other scripts, the
.I targets
option contains the single
.I software_collection_spec
for the targets on which the script is being executed.
.TP
.EV SW_SOFTWARE_SPEC
This variable contains the fully qualified software specification of
the current product or fileset.  The software specification allows the
product or fileset to be uniquely identified.
.RE
.SH RETURN VALUES
.CD swask
returns one of these codes:
.RS
.PD 0
.TP 4
.C 0
Command successful on all targets
.TP
.C 1
Command failed on all targets
.TP
.C 2
Command failed on some targets
.PD
.RE
.SH DIAGNOSTICS
The
.CD swask
command writes to stdout, stderr, and to the
.CD swask
logfile.
.SS Standard Output
An interactive
.CD swask
session does not write to stdout.
A non-interactive
.CD swask
session writes messages for significant events.
These include:
.RS
.TP 3
\(bu
a begin and end session message,
.PD 0
.TP
\(bu
selection, analysis, and execution task messages for each
.IR target_selection .
.PD
.RE
.SS Standard Error
An interactive
.CD swask
session does not write to stderr.
A non-interactive
.CD swask
session writes messages for all WARNING and ERROR
conditions to stderr.
.SS Logging
Both interactive and non-interactive
.CD swask
sessions log summary events at the host where the command was invoked.
They log detailed events to the
.C swask.log
logfile associated with each
.IR target_selection .
.RS 0
.TP
Command Log
The
.CD swask
command logs all stdout and stderr messages to the the logfile
.CR /var/adm/sw/swask.log .
Similar messages are logged by an interactive
.CD swask
session.
You can specify a different logfile by modifying the
.C logfile
option.
.RE
.SS swagentd Disabled
If the
.CR swagentd
daemon has been disabled on the host, it can be enabled
by the host's system administrator by setting the
.CR SW_ENABLE_SWAGENTD
entry in
.CR /etc/B.config.d/swconfig
to
.CR 1
and executing
.CR "/usr/sbin/swagentd -r" .
.SH EXAMPLES
.PP
Run all request scripts from the default depot
.B "(" "/var/spool/sw" ")"
depot and write the response file
.B "(named " "response" ")"
back to the same depot:
.PP
.RS
.nf
.C "swask -s /var/spool/sw \e*"
.fi
.RE
.\"
.PP
Run the request script for
.C "Product1"
from depot
.C "/tmp/sample.depot.1"
on remote host
.CR swposix ,
create the catalog
.C "/tmp/test1.depot"
on the local controller machine, and place the response file
.B "(named " "response" ")"
in the catalog:
.PP
.RS
.nf
.C "swask -s swposix:/tmp/sample.depot.1 -c /tmp/test1.depot Product1"
.fi
.RE
.PP
Run request scripts from remote depot
.C "/tmp/sample.depot.1"
on host
.C "swposix"
only when a response file is absent, create the catalog
.C "/tmp/test1.depot"
on the local controller machine, and place the response file
.B "(named " "response" ")"
in the catalog:
.PP
.RS
.nf
.C "swask -s swposix:/tmp/sample.depot.1 -c /tmp/test1.depot"
.C "-x ask=as_needed \e*"
.fi
.RE
.SH FILES
.RS 0
.TP 8
.C $HOME/.swdefaults
Contains the user-specific default values for some or all SD
options.  If this file does not exist, SD looks for user-specific
defaults in
.CI "$HOME/.sw/defaults" .
.TP
.C $HOME/.sw/sessions/
Contains session files automatically saved by the SD commands or
explicitly saved by the user.
.TP
.C /usr/lib/sw/sys.defaults
Contains the master list of current SD options, with their default
values, for documentation purposes only.
.TP
.C /var/adm/sw/
The directory which contains all of the configurable
(and non-configurable) data for SD.
This directory is also the default location of log files.
.TP
.C /var/adm/sw/defaults
Contains the active system-wide default values for some or all SD options.
.TP
.C /var/adm/sw/products/
The Installed Products Database (IPD), a catalog of all products
installed on a system.
.TP
.C /var/adm/sw/swask.log
Contains all stdout and stderr messages generated by
.CD "swask".
.RE
.SH AUTHOR
.C swask
was developed by the Hewlett-Packard Company.
.SH SEE ALSO
swconfig(1M),
swinstall(1M),
sd(5).
.P
.CT "Software Distributor Administration Guide" ","
available at
.CR "http://docs.hp.com" .
.P
SD customer web site at
.\" JAGaf61810: Updated the URL for the SD customer web site.
.\"   NOTE TO LOCALIZERS: Currently, only the English version of this
.\"   web site exists.
.CR "http://docs.hp.com/en/SD/" .
.\"
.\" index@\f4swask\f1 \- ask for user response for SD-UX@@@\f3swask(1M)\f1
.\" index@\f1ask for user response for SD-UX@@@\f3swask(1M)\f1
.\" index@\f1response for SD-UX; ask for user@@@\f3swask(1M)\f1
.\" index@\f1user; ask for user response for SD-UX@@@\f3swask(1M)\f1
.\"
.\" toc@\f3swask(1M)\f1:\0\0\f4swask\f1@@@ask for user response for SD-UX
