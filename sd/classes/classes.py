# sd.classes.py
#
# Copyright (C) 2023 Paul Weber
# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#


#
# Classes and Attributes
#
from __future__ import absolute_import
import sd.exceptions

class CliError(sd.exceptions.Error):
    """CLI Exception. :api"""
    pass

# from sd.cli.cli import CliError  # :api
# from sd.cli.commands import Command  # :api
from sd.cli.demand import  bool
print(" Entering sd.classes")


class SoftwareCollection:
  """
    2.2 Software Collection Class Attributes
    Use a 'class' or 'dictionary'?
    A software_collection can contain product and bundle software objects. A
    Software_collection can contain multiple versions of the same product or
    bundle software objects, namely products or bundles that share the same value
    for the tag attribute.
  """
  def __init__(self):
    self.path = ""          # Pathname character string
    self.dfiles = "dfiles"  # Filename character string, def value is 'dfiles'
    self.layout_version = float(1.0)  # Version of the layout
    self.data_model_revision = float(2.40)  # Filename character string
    self.pfiles = "pfiles"  # Filename character string, def value is 'pfiles'
    self.bundles = []  # List of bundle_software_specs
    self.products =[]  # List of product_software_specs

# SoftwareCollection in a dictionary

software_collection={
  'path': "",
  'dfiles': "dfiles",
  'layout_version':float(1.0),
  'data_model_revision':float(2.40),
  'pfiles': "pfiles",
  'bundles': [],
  'products': []
  }

class Distribution:
  """
    2.3 Distribution (Depot) Class Attributes
    A distribution contains product and bundle software objects. It is contained
    on a distribution media or may be part of the file store of a system.
    The distribution may contain a variety of software products and bundles, and
    that software may be applicable to a variety of hardware architectures or
    operating systems.
  """
  def __init__(self):
    SoftwareCollection()
    self.uuid = "\0" * 64   # Portable character string
    self.media = []  # List of media sequence_number values

# Dictionary version of Distribution class
distribution=software_collection|{
  'uuid':"",
  'media':""
  }

class Media:
  """
    2.4 Media Class Attributes
    The media class is used to describe the media attributes for distributions 
    that span multiple media.
  """
  def __init__(self):
    self.media = "\0" * 64 # Portable character string 64 char

media={'sequence_number':"1"}

class InstalledSoftware:
  """
    2.5  Installed_Software Class
    The installed_software class is used to describe the bundle and product software 
    that has been installed on a file system.
  """
  def __init__(self):
    SoftwareCollection()
    self.catalog = ""   # Portable character string

# installed_software class dictionary version
installed_software=software_collection|{
  'catalog':""
  }

class Tag:
  """
    Tag Class Attributes - Where did I find this table?
  """
  def __init__(self):
    self.tag = ""
    self.title = ""
    self.description = ""
    self.revision = ""

# tag class dictionary version
tag={
  'tag':"",
  'title':"",
  'description':"",
  'revision':""
  }

class Vendor:
  """
    2.6 Vendor Class Attributes
    The vendor class is used to describe the attributes of the vendors associated with 
    products and bundles.
  
    Each product or bundle identifies a vendor with a vendor_tag that identifies a particular
    vendor object. The vendor_tag attribute is used to distinguish products and bundles from 
    different vendors that share the same product or bundle tag.
  """
  def __init__(self):
    self.tag = "" # Filename character string
    self.title = "" # Portable character string 256 char, one line!
    self.description = "" # Portable character string, multi-line < data/description

# vendor class dictionary version
vendor={
  'tag':"",
  'title':"",
  'description':""
  }


class Category:
  """
    2.7 Category Class Attributes
    The category attributes class is used to describe the attributes of the category attributes 
    associated with products and bundles.
 
    Each product or bundle identifies a category attribute that identifies a particular object. tag.
  """
  def __init__(self):
    self.tag = ""  # Filename character string
    self.title = ""  # Portable character string 256 char, one line!
    self.description = "" # Portable character string, multi-line
    self.revision = ""  # Portable character string, i.e. 1.0


# category class dictionary version
category={
  'tag':"",
  'title':"",
  'description':"",
  'revision':""
  }


class SoftwareCommon:
  """
    2.8 Software Class attributes
    Software is the common class from which products, bundles, filesets and subproducts inherit.
  """
  def __init__(self):
    Category() # Get category tag values
    self.tag = ""  # Filename character string
    """ category_tag: Empty list, or patch if the object has the is_patchattribute set to true"""
    self.category_tag = []  # list of category.tag values []
    self.create_time = ""  # Integer character string
    self.description = "" # Portable character string, multi-line
    self.is_patch = bool(False)  # One of: true, false
    self.mod_time = str(int("")) # Integer character string
    self.size = str(int(""))# Integer character string
    self.titleself = "" # Portable character string

# software_common class dictionary version
software_common={
  'tag':"",
  'category_tag':[],
  'create_time':"",
  'description':"",
  'is_patch':bool(False),
  'mod_time':"",
  'size':"",
  'title':""
  }


class Product:
  """
    2.9 Product Class Attributes
    Products can contain filesets, which can be grouped into subproducts. Products
    are named by their tag attributes. A particular product object is uniquely 
    identified within a software_collection by the tag attribute and by the 
    version distinguishing attributes. The attributes that uniquely distinguish 
    a particular product version within a software_collection are revision, version,
    release, architecture, vendor_tag, location , and qualifier.
  """
  def __init__(self):
    SoftwareCommon()
    self.architecture = "" # Portable character string, one line
    self.location = "" # Pathname character string i.e. <product.directory>
    self.qualifier = "" # Portable character string
    self.revision = "" # Portable character string
    self.version = "" # Filename character string (Added by P. Weber)
    self.release = "" # Filename character string (Added by P. Weber)
    self.vendor_tag = "" # Filename character string
    self.all_filesets = [] # List of fileset tag values
    self.control_directory = "" # Filename character string
    self.copyright = "" # Portable character string
    self.changelog = "" # Multi-line string (Added by P. Weber)
    self.directory = "/" # Pathname character string, i.e. /
    self.instance_id = "1" # Filename character string, ei.e. 1
    self.is_locatable = bool(True) # one of: true, false, default true
    self.readme = "" # Multi-line string - HP
    self.postkernel = "" # Pathname character string
    self.layout_version = float(1.0)
    self.machine_type = "" # Software pattern matching string
    self.number = "" # Portable character string
    self.os_name = "" # Software pattern matching string, i.e. uname -m
    self.os_release = "" # Software pattern matching string, i.e. uname -r
    self.os_version = "" # Software pattern matching string, i.e. uname
    self.control_files = [] # List of control_filetag values
    self.subproducts = [] # List of subproduct tag values
    self.filesets = [] # List of fileset tag values
    self.share_link = "" # One-line string - HP SD-UX
    self.install_type = "" # One-line string - HP SD-UX
    self.install_source = "" # One-line string - HP SD-UX

# Product class dictionary version
product=software_common|{
  'architecture':"",
  'location':"",
  'qualifier':"",
  'revision':"",
  'version':"",
  'release':"",
  'vendor_tag':"",
  'all_filesets':[],
  'control_directory':"",
  'copyright':"",
  'directory':"/",
  'instance_id':"1",
  'is_locatable':bool(True),
  'readme':"",
  'postkernel':"",
  'machine_type':"",
  'number':"",
  'os_name':"",
  'os_release':"",
  'os_version':"",
  'control_files':[],
  'subproducts':[],
  'filesets':[],
  'share_link':"",
  'install_type':"",
  'install_source':""
  }


class Bundle:
  """
    2.10 Bundle Class Attributes
    Bundles are groupings of software objects. Bundles contain references to 
    products, parts of products, or other bundles. A software object can be 
    referenced by more than one bundle.
    
    The bundle class inherits the attributes of the software common class.
  """
  def __init__(self):
    SoftwareCommon()
    self.architecture = "" # Portable character string
    self.location = "" # Pathname character string, i.e. <<bundle.directory >
    self.qualifier = "" # Portable character string
    self.revision = "" # Portable character string
    self.version = "" # Portable character string (Added by Paul Weber)
    self.release = "" # Portable character string (Added by Paul Weber)
    self.vendor_tag = "" # Filename character string
    self.contents = [] # List of software specs
    self.copyright = "" # Portable character string
    self.directory = "" # Pathname character string
    self.instance_id = "" # Filename character string
    self.is_locatable = bool(True) # One of true, false
    self.layout_version = "1.0"
    self.machine_type = "" # Software pattern matching string
    self.number = "" # Portable character string
    self.os_name = "" # Software pattern matching string, i.e. uname
    self.os_release = "" # Software pattern matching string, i.e. uname -r
    self.os_version = "" # Software pattern matching string i.e. uname
    self.share_link = "" # HP SD-UX
    self.install_source = "" # HP SD-UX
    self.hp_ii = "" # HP SD-UX
    self.hp_mfg = "" # HP SD-UX

# Bundle dictionary
bundle=software_common|{
  'architecture':"",
  'location':"",
  'qualifier':"",
  'revision':"",
  'version':"",
  'release':"",
  'vendor_tag':"",
  'contents':[],
  'copyright':"",
  'directory':"",
  'instance_id':int(1),
  'is_locatable':bool(True),
  'layout_version':float(1.0),
  'machine_type':"",
  'number':"",
  'os_name':"",
  'os_release':"",
  'os_version':"",
  'share_link':"",
  'install_source':"",
  'hp_ii':"",
  'hp_mfg':""
  }


class Fileset:
  """
    2.11 Fileset Class Attributes
    The fileset class is used to define a set of software files. The fileset is 
    the smallest level of software that can be managed by the tasks defined in 
    this standard.
    
    The fileset class inherits attributes from the software common class.

    Filesets contain the actual files and control_files that make up the software 
    product.

    A particular fileset object is identified within a product by the tag attribute.
  """
  def __init__(self):
    SoftwareCommon()
    self.ancestor = [] # list of fileset software_specs of the form
                       # product.fileset,version i.e.
                       # product.tag.fileset.tag,r<revision,a=architecture,v=vendor_tag
    self.applied_patches = [] # List of patch software_specs of
                              # the form product.fileset,version
    self.control_directory = "" # Filename character string
    self.corequisites = [] # List of dependency_specs
    self.exrequisites = [] # List of dependency_specs
    self.is_kernel = bool(False) # One of: true, false
    self.is_locatable = bool(False) # One of: true, false
    self.is_reboot = bool(False) # One of: true, false
    self.is_sparse = bool(False) # One of: true, false
    self.location = "" # Pathname character string i.e. <product.directory>
    self.media_sequence_number = "1" # List of media_sequence_number values, i.e. 1
    self.patch_state = "" # One of applied, committed, superseded
    self.prerequisites = [] # List of dependency_specs
    self.revision = "" # Filename character string
    self.version = "" # Filename character string (added to specification)
    self.release = "" # Filename character string (added to specification)
    self.saved_files_directory = "" # Pathname character string
    self.state = "" # One of: configured, installed, corrupt, removed, available, transient
    self.supersedes = [] # list of patch software_specs
    self.superseded_by = [] # list of fully qualified fileset software_specs of the form product.fileset,version
    self.control_files = [] # List of control_filetag values
    self.files = [] # List of filepath values

# Patch state options
patch_state={
  '': "0",
  'applied': "1",
  'committed': "2",
  'superseded': "3"
}
# Fileset state options
filset_state={
  '': "0",
  'configured': "1",
  'installed': "2",
  'corrupt': "3",
  'removed': "4",
  'available': "5",
  'transient': "6"
}
# Fileset as a dictionary
fileset=software_common|{
  'ancestor': [],
  'applied_patches': [],
  'control_directory': "",
  'corequisites': [],
  'exrequisites': [],
  'is_kernel':  bool(False),
  'is_locatable':  bool(False),
  'is_reboot':  bool(False),
  'is_sparse':  bool(False),
  'location': "",
  'media_sequence_number': "1",
  'patch_state': "",
  'prerequisites': [],
  'revision': "",
  'version': "",
  'release': "",
  'saved_files_directory': "",
  'state': "",
  'supersedes': [],
  'superseded_by': [],
  'control_files': [],
  'files': []
  }


class Subproduct:
  """
    2.12 Subproduct Class Attributes
    Subproducts are groupings of filesets and subproducts within a single product.
    Subproducts do not contain filesets or subproducts within the name space of
    the subproduct, but instead refer to to them. A subproduct can refer to another
    subproduct. A subproduct or fileset can be referenced by more than one subproduct.
  
    The subproduct class inherits the attributes of the software common class.
  """
  def __init__(self):
    SoftwareCommon()
    self.contents=[]


subproduct=software_common|{
  'contents': [] # List of tag values
  }

class SoftwareFile:
  """
    2.13 Software_File Common Attributes
    Software_file is the common class that files and control_files inherit from. 
    A software_file is a file as defined in POSIX.1.
  """
  def __init__(self):
    self.cksum = "" # Integer character string
    self.compressed_cksum = "" # Integer character string
    self.compressed_size = "" # Integer character string
    self.compressed_state = "uncompressed" # One of: uncompressed,compressed,not_compressible
    self.compression_type = "" # Filename character string
    self.revision = "" # Portable character string
    self.size = "" # Integer character string
    self.source = "" # Pathname character string

# compressed_state options
compressed_state={
  '': "0",
  'uncompressed': "1",
  'compressed': "2",
  'not_compressible': "3"
}

# SoftwareFile as a dictionary
software_file={
  'cksum': "",
  'compressed_cksum': "",
  'compressed_size': "",
  'compressed_state': "uncompressed",
  'compression_type': "",
  'revision': "",
  'size': "",
  'source': ""
  }

class Files:
  """
    2.14 Files Class Attributes
    Files are the actual files and directories that make up the fileset. Many of 
    the file attributes (such as owner, group, and mode) are derived from, and 
    dependent upon, a POSIX.1 file system.
  
    The file class inherits attributes from the software_file common class.
  """
  def __init__(self):
    SoftwareFile()
    self.path = "" # Pathname character string
    self.archive_path = "" # Pathname character string
    self.gid = "" # Integer character string
    self.group = "" # Filename character string
    self.is_volatile = bool(False) # One of: true, false
    self.link_source = "" # Pathname character string
    self.major = "" # Portable character string
    self.minor = "" # Portable character string
    self.mode = 0x00 # Octal character string
    self.mtime = "" # Integer character string
    self.owner = "" # Filename character string
    self.type = "f" # One of: f, d, h, s, p, b, c, x, a
    self.uid = "" # Integer character string

# files.type options
files_type={
  '': "0",
  'f': "1",
  'd': "2",
  'h': "3",
  's': "4",
  'p': "5",
  'b': "6",
  'c': "6",
  'x': "8",
  'a': "9"
}


# Files class as a dictionary
files=software_file|{
  'path': "",
  'archive_path': "",
  'gid': "",
  'group': "",
  'is_volatile': bool(False),
  'link_source': "",
  'major': "",
  'minor': "",
  'mode': "",
  'mtime': "",
  'owner': "",
  'type': "f",
  'uid': ""
  }


class ControlFiles(object):
  """
    2.15 Control_Files Class Attributes
    Control_files can be scripts, data files, or INFO files. The product and 
    fileset INFO files in the software packaging layout are included as control_files. 
    Control scripts are the vendor-supplied scripts executed at various steps by the 
    software administration utilities.
  
    The control_file class inherits attributes from the software_file common class.
  """
  def __init__(self):
    SoftwareFile()
    self.tag = "" # Filename character string, options listed below
    self.interpreter = "/usr/bin/sh" # Filename character string
    self.path = "" # Filename character string
    self.result = "" # One of: none, success,failure, warning

# Controlfiles.result options
control_result={
  'none': "0",
  'success': "1",
  'failure': "2",
  'warning': "3"
}

# Controlfiles.tag options
controlfiles_result={
  '': "0",
  'request': "1",
  'response': "2",
  'checkinstall': "3",
  'preinstall': "4",
  'postinstall': "5",
  'unpreinstall': "6",
  'unpostinstall': "7",
  'verify': "8",
  'fix': "9",
  'checkremove': "10",
  'preremove': "11",
  'postremove': "12",
  'configure': "13",
  'warning': "14",
  'unconfigure': "15",
  'space': "16"
}

# ControlFiles as a dictionary
control_files=software_file|{
  'tag':"",
  'interpreter':"/usr/bin/sh",
  'path':"",
  'result':""
  }

print("Exiting sd.classes")
print("")
