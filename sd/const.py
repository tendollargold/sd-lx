# # sd.const.py
# sd constants and global variables
#
# Copyright (C) 2023 Paul Weber <paul@weber.net>
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

from __future__ import unicode_literals
import platform
import os
import sys
from sd.util import swlib_doif_writef
from sd.swcommon import (swc_get_stderr_fd, swc_lc_exit, swc_lc_raise,
                         g_pax_read_commands, g_pax_write_commands, g_pax_remove_commands)
from sd.swsuplib.misc.swlib import FrameRecord, swlibc
from sd.swsuplib.misc.swfork import SwForkConst

swfc = SwForkConst
fr = FrameRecord()
print("importing const.py")


class SwGlobals:
    #   global SW_SESSION_OPTIONS,SW_ROOT_DIRECTORY,SW_LOCATION,SW_CATALOG,SW_CONTROL_DIRECTORY
    #   global SW_CONTROL_TAG,SW_PATH,SW_SOFTWARE_SPEC,SW_ANCESTORS,SW_ACL_DIR,SW_DEFERRED_KERNBLD
    #   global SW_INITIAL_INSTALL, SW_KERNEL_PATH, SW_SESSION_IS_KERNEL, SW_SESSION_IS_REBOOT
    #   global SW_SYSTEM_FILE_PATH, SW_IS_COMPATIBLE

    # SW_SESSION_OPTIONS is the pathname of a file containing the value of every
    # option defined for the software utility being executed, using the options
    # syntax described in Section 3.5.3 on page 52.

    # The option syntax is restricted such that the command prefix is not used,
    # there are no spaces on either side of the = (equal sign), and # multiple
    # valued options have the values quoted.

    # Included SW_SESSION_OPTIONS to be in ini format

    # This environment variable allows scripts to retrieve any options and values
    # for this command other than the ones provided explicitly via environment
    # variables. When the file pointed to by SW_SESSION_OPTIONS is made available
    # to request scripts, the targets option contains a list of
    # software_collection_specs for all targets specified for the command. When the
    # file pointed to by SW_SESSION_OPTIONS is made available to other scripts, the
    # targets option contains the single software_collection_spec for the targets
    # on which the script is being executed.

    # An implementation should ensure that each software_collection_spec contained
    # in the value of the targets option is the same between invocations of
    # commands.
    # This will help ensure that any per-target information stored by the request
    # script can be located by the subsequent scripts.
    SW_SESSION_OPTIONS = "/var/adm/sw/defaults.conf"

    # SW_ROOT_DIRECTORY is the installed_software.path attribute of the installed
    # software object within which the software containing this control_file is
    # installed.
    # This is the directory relative to which all operations with the # script are
    # performed.  It is used to specify the directory portion of all target
    # operands.

    # HP SD
    # Defines the root directory in which the session is operating, either “/”
    # or an alternate root directory. This variable tells control scripts the
    # root directory in which the products are installed. A script must use
    # this directory as a prefix to SW_LOCATION to locate the product’s
    # installed files.

    # All control scripts (except for the configure and unconfigure scripts)
    # can be executed during an installation or remove task on an alternate
    # root. If the scripts reference any product files, each reference must
    # include the {SW_ROOT_DIRECTORY} in the file pathname.
    # The scripts may only need to perform actions when installing to
    # (removing from) the primary root directory (“/”). If so, then the
    # SW_ROOT_DIRECTORY can be used to cause a simple exit 0 when
    # the task is operating in an alternate root directory:
    #   if test "${SW_ROOT_DIRECTORY}" != "/"
    #   then
    #     exit 0
    #   fi

    SW_ROOT_DIRECTORY = "/"

    # SW_LOCATION is the base directory where the product or fileset will be
    # installed or is already installed.
    # This is the value of the location attribute.
    # HP SD:
    # Defines the location of the product, which may have been changed
    # from the default product directory (if the product is locatable).
    # When installing to (or removing from) the primary root directory
    # (“/”), this variable is the absolute path to the product directory. For
    # operations on an alternate root directory, the variable must be
    # prefixed by SW_ROOT_DIRECTORY to correctly reference product
    # files.
    # If a product is not locatable, then the value of SW_LOCATION will
    # always be the default product directory defined when the product is
    # packaged.
    SW_LOCATION = "/"

    # SW_CATALOG used to specify the value of the installed_software_catalog option
    # The value of the installed_software.catalog attribute indicating the location
    # or identification of the catalog relative to the SW_ROOT_DIRECTORY.

    # Holds the path to the Installed Products Database (IPD), relative
    # to the path in the SW_ROOT_DIRECTORY environment variable.
    # (One can specify a path for the IPD using the installed_software_catalog
    # default option.)
    SW_CATALOG = SW_ROOT_DIRECTORY + "var/adm/sw/products"

    # SW_CONTROL_DIRECTORY is the directory where the executing script is located.
    # This directory is readable from within control script execution and is
    # writable from commands within control scripts when the request script is
    # being executed. All control_files are readable by any control script.
    # Also contains the response file generated by a request script. Other
    # scripts that reference the response file access the file by referencing
    # this variable.
    SW_CONTROL_DIRECTORY = ""

    # SW_CONTROL_TAG is the tag of the script being executed.
    # This allows the control_script to tell what tag is being executed when the
    # actual script path is defined for more than one tag.

    # When packaging software, you can define a physical name and path for a
    # control file in a depot. This lets you define the control_file with a
    # name other than its tag and lets you use multiple control_file
    # definitions to point to the same file. A control_file can query the
    SW_CONTROL_TAG = ""

    # SW_PATH is a PATH which, at least, contains all utilities defined by the
    # POSIX.2
    # HP SD
    # PATH variable must begin with $SW.PATH. The PATH should be set
    # as follows:
    #   PATH=$SW_PATH
    #   export PATH
    SW_PATH = ""

    # SW_SOFTWARE_SPEC is the value of the fully-qualified software_spec
    # identifying the software object containing this control script.

    # In order to allow a control script to know whether it is a new install or an
    # update in order to change its behavior accordingly, the swinstall utility
    # sets the environment variable SW_ANCESTORS to the fully qualified
    # software_specs of the ancestor or ancestors of the fileset that currently
    # are installed.

    # HP SD:
    # The software specification allows the product or fileset to be uniquely
    # identified. (Fully qualified software specs include the r=, a=, and
    # v= version components even if they contain empty strings. For installed
    # #software, l= must also be included.)

    # For SD-LX we will include fileset version, release, architecture for Linux
    # variants.
    SW_SOFTWARE_SPEC = ""

    # Undefined, but noted in the documentation
    SW_ANCESTORS = ""

    # SW_ACL_DIR variable was not defined within CAE Specifiication for Systems
    # Distributed Software Administration
    SW_ACL_DIR = '/var/adm/sw/security'

    # An alternative configuration of nonprivileged mode sets up user-installed
    # software catalogs in each user’s home directory. You can use the
    # admin_directory option in /var/adm/sw/defaults to indicate a path beginning
    # with HOME or /HOME, so that the default administration directory used by
    # SD-UX during nonprivileged mode is in each user’s home directory. (A value of
    # HOME/.sw works well for this purpose.) Individual users can override this in
    # their $HOME/.swdefaults file or on the command line.

    # Admin Directory Option
    # This option lets you specify the location for logfiles and the default parent
    # directory for the installed software catalog. Values are as follows:
    # admin_directory=/var/adm/sw (for normal mode)
    # admin_directory=/var/home/LOGNAME/sw (for nonprivileged mode) The default
    # value is /var/adm/sw for normal operations. For nonprivileged mode (that is,
    # when the run_as_superuser option is set to true): The default value is forced
    # to /var/home/LOGNAME/sw. The path element LOGNAME is replaced with the name of
    # the invoking user, which SD-UX reads from the system password file. If you set
    # the value of this option to HOME/path, SD-UX replaces HOME with the invoking
    # user’s home directory (from the system password file) and resolves path
    # relative to that directory. For example, if you specified HOME/my_admin for
    # these options, the location would resolve to the my_admin directory in your
    # home directory. This option applies to swinstall, swcopy, swremove, swconfig,
    # swverify, swlist, swreg, swacl, swpackage, swmodify.

    # HP SD:
    # SW_DEFERRED_KERNBLD
    # • This variable is normally unset. If it is set, the actions necessary for
    #   preparing the system file /stand/system cannot be accomplished
    #   from within the postinstall scripts, but instead must be accomplished
    #   by the configure scripts. This occurs whenever software is installed
    #   to a directory other than /.
    # • This variable should be read only by the configure and postinstall
    #   scripts of a kernel fileset.
    SW_DEFERRED_KERNBLD = bool(False)

    # SW_INITIAL_INSTALL
    # • This variable is normally unset. If it is set, the swinstall session is
    # being run as the back end of an initial system software installation
    # (that is, a “cold” install).
    SW_INITIAL_INSTALL = bool(False)

    # SW_KERNEL_PATH
    # • The path to the kernel. The default value is /stand/vmunix.
    SW_KERNEL_PATH = "/stand/vmunix"

    # SW_SESSION_IS_KERNEL
    # • Indicates whether a kernel build is scheduled for the current
    #   install/remove session.
    # • A “true” value indicates that the selected kernel fileset is scheduled
    #   for a kernel build and that changes to /stand/system are required.
    # • A null value indicates that a kernel build is not scheduled and that
    #   changes to /stand/system are not required.
    # • The value of this variable is always equal to the value of
    #   SW_SESSION_IS_KERNEL
    SW_SESSION_IS_KERNEL = bool(False)

    # SW_SESSION_IS_REBOOT.
    # • Indicates whether a reboot is scheduled for a fileset selected for
    #   removal. Because all HP-UX kernel filesets are also reboot filesets,
    #   the values of these variables is always equal to the value of
    SW_SESSION_IS_REBOOT = bool(False)

    # SW_SYSTEM_FILE_PATH
    # • The path to the kernel’s system file. The default value is
    #   /stand/system.
    SW_SYSTEM_FILE_PATH = "/stand/system"

    # Variables That Affect swverify
    # SW_IS_COMPATIBLE
    # • Designed to help you determine if installed software is incompatible
    #   and should be removed from a system.
    # • For use during the execution of a verify script, which is called by the
    #   swverify command.
    # • The variable will be set to true if the software being considered is
    #   compatible with the system on which it is installed.
    # • Set to false if the software being considered incompatible with the
    #   system on which it is installed.
    SW_IS_COMPATIBLE = bool(False)

    ADMIN_DIRECTORY = '/var/adm/sw'
    DEFAULTS_FILENAME = '/var/adm/sw/defaults'
    USER_HOME = os.environ['HOME']

    if platform.system() == "Linux":
        DEFAULTS_FILENAME = '/etc/sw/sw.conf'
        user_swdefaults_file = USER_HOME + "/.sw/sw.conf"
        admin_directory = "/var/lib/sw"
        print("userdefaults in defaults: " + user_swdefaults_file)

    swdefaults_file = DEFAULTS_FILENAME

    PID_FILENAME = '/var/run/sw.pid'
    RUNDIR = '/run'
    USER_RUNDIR = '/run/user'
    SYSTEM_CACHEDIR = '/var/cache/sw'
    TMPDIR = '/var/tmp/'

    # CLI verbose values greater or equal to this are considered "verbose":

    PLUGINCONFPATH = '/etc/sw/plugins'
    SD_VERSION = '1.0'
    SW_RELEASE = SD_VERSION
    SW_DATE = "2023-11-30"  # Last Updated
    SWPACKAGE_VERSION = SD_VERSION
    SWINSTALL_VERSION = SD_VERSION
    SWCOPY_VERSION = SD_VERSION
    SWLIST_VERSION = SD_VERSION
    SWREMOVE_VERSION = SD_VERSION
    SWVERIFY_VERSION = SD_VERSION
    SWCONFIG_VERSION = SD_VERSION
    SWLIBDIR = '/usr/lib64'
    SW_BIN_DIR = '/usr/bin'
    SW_GPG_BIN = 'gpg'

    # Names of each utility
    SW_UTN_CONFIG = "swconfig"
    SW_UTN_INSTALL = "swinstall"
    SW_UTN_REMOVE = "swremove"
    SW_UTN_LIST = "swlist"
    SW_UTN_COPY = "swcopy"
    SW_UTN_PACKAGE = "swpackage"
    SW_UTN_VERIFY = "swverify"
    # POSIX level values for script results
    SW_SUCCESS = 0
    SW_ERROR = 1
    SW_WARNING = 2
    SW_DESELECT = 3  # not POSIX
    SW_NOTE = 3
    SW_NONE = 255  # not POSIX
    SW_INTERNAL_ERROR_127 = -127  # not POSIX
    SW_INTERNAL_ERROR_128 = -128  # not POSIX
    SW_NOTICE = 0  # not POSIX
    # POSIX level values for swlist
    SW_LEVEL_V_FILE = 0  # these are array indexes for blist_levels_i[] in swlist
    SW_LEVEL_V_CONTROL_FILE = 1
    SW_LEVEL_V_FILESET = 2
    SW_LEVEL_V_SUBPRODUCT = 3
    SW_LEVEL_V_PRODUCT = 4
    SW_LEVEL_V_BUNDLE = 5
    SW_LEVEL_V_DISTRIBUTION = 6
    SW_LEVEL_V_HOST = 7
    # POSIX fileset states   -- <state> attribute
    SW_STATE_UNSET = "__unset__"  # Impl extension
    SW_STATE_CONFIGURED = "configured"
    SW_STATE_INSTALLED = "installed"
    SW_STATE_CORRUPT = "corrupt"
    SW_STATE_REMOVED = "removed"
    SW_STATE_AVAILABLE = "available"
    SW_STATE_TRANSIENT = "transient"
    SW_STATUS_COMMAND_NOT_FOUND = "126"
    # Various shells
    SH_A_ash = "ash"
    SH_A_dash = "dash"
    SH_A_ksh = "ksh"
    SH_A_sh = "sh"
    SH_A_bash = "bash"
    SH_A_mksh = "mksh"
    SW_OBJ_file = "file"
    SW_OBJ_control_file = "control_file"
    SW_OBJ_product = "product"
    SW_OBJ_bundle = "bundle"

swg = SwGlobals
class LegacyConstants:
    RPMPSF_RPM_HAS_FILELANGS = 1
    RPMPSF_RPM_HAS_PAYLOADCOMPRESSOR = 1
    RPMPSF_RPM_VERSION_305 = 1
    USE_RPMTAG_BASENAMES = 1
    USE_WITH_RPM = 1
    USE_WITH_SELF_RPM = 1
    TMAGLEN = 6
    TVERSION = "00"
    TVERSLEN = 2
    SW_UTN_CONFIG = "swconfig"
    SW_UTN_INSTALL = "swinstall"
    SW_UTN_REMOVE = "swremove"
    SW_UTN_LIST = "swlist"
    SW_UTN_COPY = "swcopy"
    SW_UTN_PACKAGE = "swpackage"
    SW_UTN_VERIFY = "swverify"
    SW_SUCCESS = 0
    SW_ERROR = 1
    SW_WARNING = 2
    SW_DESELECT = 3  # not POSIX
    SW_NOTE = 3
    SW_NONE = 255  # not POSIX
    SW_INTERNAL_ERROR_127 = -127  # not POSIX
    SW_INTERNAL_ERROR_128 = -128  # not POSIX
    SW_NOTICE = 0  # not POSIX
    SW_LEVEL_V_FILE = 0  # these are array indexes for blist_levels_i[] in swlist
    SW_LEVEL_V_CONTROL_FILE = 1
    SW_LEVEL_V_FILESET = 2
    SW_LEVEL_V_SUBPRODUCT = 3
    SW_LEVEL_V_PRODUCT = 4
    SW_LEVEL_V_BUNDLE = 5
    SW_LEVEL_V_DISTRIBUTION = 6
    SW_LEVEL_V_HOST = 7
    SW_STATE_UNSET = "__unset__"  # Impl extension
    SW_STATE_CONFIGURED = "configured"
    SW_STATE_INSTALLED = "installed"
    SW_STATE_CORRUPT = "corrupt"
    SW_STATE_REMOVED = "removed"
    SW_STATE_AVAILABLE = "available"
    SW_STATE_TRANSIENT = "transient"
    SW_STATUS_COMMAND_NOT_FOUND = "126"
    SW_ITYPE_f = 'f'  # Reg
    SW_ITYPE_d = 'd'  # Dir
    SW_ITYPE_h = 'h'  # Link
    SW_ITYPE_s = 's'  # symlink
    SW_ITYPE_b = 'b'  # block device
    SW_ITYPE_c = 'c'  # char device
    SW_ITYPE_p = 'p'  # fifo
    SW_ITYPE_x = 'x'  # Not posix, C701 extension for 'to be deleted file
    SW_ITYPE_y = 'y'  # Not posix, Not C701, internal use only, means not dumped

    SH_A_ash = "ash"
    SH_A_dash = "dash"
    SH_A_ksh = "ksh"
    SH_A_sh = "sh"
    SH_A_bash = "bash"
    SH_A_mksh = "mksh"
    #
    # SWC swcopy variables
    SWC_PID_ARRAY_LEN = 30
    SWC_TARGET_FD_ARRAY_LEN = 10
    SWC_SCRIPT_SLEEP_DELAY = 0
    SWC_VERBOSE_SWIDB = 5  # Level at which scripts set -vx
    SWC_VERBOSE_IDB = 8  # Lowest debugging level
    SWC_VERBOSE_IDB2 = 9
    SWC_FC_NOOP = 0  # no op
    SWC_FC_NOAB = 1  # squash absolute path, then compare
    SWC_FC_NORE = 2  # resolve relative path to abs, then compare
    WOPT_LAST = 236

    SW_STDIO_FNAME = "-"
    SW_DEFAULT_CATALOG_USER = "root"
    SW_DEFAULT_CATALOG_GROUP = "root"
    SW_DEFAULT_DISTRIBUTION_USER = "root"
    SW_DEFAULT_DISTRIBUTION_GROUP = "root"
    SW_CATALOG_OWNER_ATT = "catalog_owner"
    SW_CATALOG_GROUP_ATT = "catalog_group"
    SW_CATALOG_MODE_ATT = "catalog_mode"
    SW_DIR_OWNER_ATT = "leading_dir_owner"
    SW_DIR_GROUP_ATT = "leading_dir_group"
    SW_DIR_MODE_ATT = "leading_dir_mode"
    SW_DIR_MODE_VAL = "0755"
    SW_DISTRIBUTION_OWNER_ATT = "owner"
    SW_DISTRIBUTION_GROUP_ATT = "group"
    SW_DISTRIBUTION_MODE_ATT = "mode"
    SW_DISTRIBUTION_MODE_VAL = "0755"
    SW_CATALOG_MODE_VAL = "0755"
    SW_SIGNATURE_ATT = "signature"
    SW_LOGGER_SIGTERM = "SIGUSR2"

    SW_ROOT_DIR = "/"
    SW_PROGS_LAYOUT_VERSION = "1.0"
    SW_EXIT_SUCCESS = 0
    SW_EXIT_ONE = 1  # Medium not modified.
    SW_EXIT_TWO = 2  # Medium modified.
    SW_FAILED_ALL_TARGETS = 1
    SW_FAILED_SOME_TARGETS = 2
    SW_SC_OPEN_MAX = 24  # Max No. of open fd's if sysconf can't tell us
    SW_MIN_FD_AVAIL = 16  # Minimum number of open fd to operate

    PAX_READ_COMMANDS_LEN = 10
    PAX_WRITE_COMMANDS_LEN = 10
    SW_UTILNAME = "swutility"
    REPORT_BUGS = "<bug-swbis@gnu.org>"

    swutil_x_mode = 1
    local_stdin = 0
    SWPARSE_AT_LEVEL = 0
    VERBOSE_LEVEL2 = 2
    VERBOSE_LEVEL3 = 3

    LOCAL_RSH_BIN = "rsh"
    REMOTE_RSH_BIN = "rsh"
    LOCAL_SSH_BIN = "ssh"
    REMOTE_SSH_BIN = "ssh"


lc = LegacyConstants

global G

class GB:
    def __init__(self):
        self.g_opta = None
        self.g_stdout_testfd = -1
        self.g_fail_loudly = -1
        self.g_verbose_threshold = swlibc.SWC_VERBOSE_4
        self.g_verbose = 1
        self.g_signal_flag = 0
        self.g_t_efd = -1
        self.g_s_efd = -1
        self.g_logger_pid = 0
        self.g_swevent_fd = -1
        self.g_vstderr = None
        self.g_pid_array = [0] * (lc.SWC_PID_ARRAY_LEN + lc.SWC_PID_ARRAY_LEN)
        self.g_status_array = [0] * lc.SWC_PID_ARRAY_LEN
        self.g_targetfd_array = [-1] * lc.SWC_TARGET_FD_ARRAY_LEN
        self.g_selectfd_array = [-1] * lc.SWC_TARGET_FD_ARRAY_LEN
        self.g_io_req = {'tv_sec': 0, 'tv_nsec': 0}
        self.g_target_kmd = None
        self.g_source_kmd = None
        self.g_killcmd = None
        self.g_pid_array_len = 0
        self.g_nullfd = os.open("/dev/null", os.O_RDWR, 0)
        self.g_logspec = {'logfd': -1, 'loglevel': 0, 'fail_loudly': -1}
        if self.g_nullfd < 0:
            swlib_doif_writef(self.g_verbose, self.g_fail_loudly, self.g_logspec, swc_get_stderr_fd(G), "Can't open /dev/null\n")
            exit(1)
        self.g_stderr_fd = -1
        self.g_target_fdar = None
        self.g_source_fdar = None
        self.g_save_fdar = None
        self.g_fork_pty = swfc.SWFORK_PTY
        self.g_fork_pty2 = swfc.SWFORK_PTY2
        self.g_fork_pty_none = swfc.SWFORK_NO_PTY
        self.g_fork_defaultmask = set()
        self.g_fork_blockmask = set()
        self.g_ssh_fork_blockmask = set()
        self.g_currentmask = set()
        self.g_do_progressmeter = 0
        self.g_loglevel = 0
        self.g_meter_fd = sys.stdout.fileno()
        self.g_xformat = None
        self.g_swi_event_fd = -1
        self.g_swlog = None
        self.g_pax_read_commands = g_pax_read_commands
        self.g_pax_write_commands = g_pax_write_commands
        self.g_pax_remove_commands = g_pax_remove_commands
        self.g_source_script_name = None
        self.g_target_script_name = None
        self.g_swi_debug_name = None
        self.g_is_seekable = 0
        self.g_do_debug_events = 0
        self.g_target_terminal_host = None
        self.g_source_terminal_host = None
        self.g_sh_dash_s = None
        self.g_opt_alt_catalog_root = 0
        self.g_opta = None
        self.g_do_task_shell_debug = 0
        self.g_do_distribution = 0
        self.g_do_installed_software = 0
        self.g_master_alarm = 0
        self.g_noscripts = 0
        self.g_swi = None
        self.g_e_in_control_script = 0
        self.g_devel_verbose = 0
        self.g_to_stdout = 0
        self.g_force = 0
        self.g_force_locks = 0
        self.g_in_shls_looper = 0
        self.g_swicol = None
        self.g_main_sig_handler = None
        self.g_safe_sig_handler = None
        self.g_psignal = 0
        self.g_running_signalset = 0
        self.g_running_signalusr1 = 0
        self.g_target_did_abort = 0
        self.g_save_stderr_fd = 0
        self.g_opt_preview = 0
        self.g_do_cleansh = 0
        self.g_gpuflag = 0
        self.g_do_create = 0
        self.g_do_extract = 0
        self.g_sourcepath_list = None
        self.g_no_extract = 0
        self.g_do_show_lines = 0
        self.g_catalog_info_name = None
        self.g_system_info_name = None
        self.g_justdb = 0
        self.g_do_dereference = 0
        self.g_do_hard_dereference = 0
        self.g_ignore_slack_install = 0
        self.g_gpufield = ''
        self.g_do_unconfig = 0
        self.g_config_postinstall = 0
        self.g_send_env = 0
        self.g_no_summary_report = 0
        self.g_no_prelink = 0
        self.g_output_form = 0
        self.g_cl_target_target = None
        self.g_no_script_chdir = 0
        self.g_prelink_fd = -1
        self.g_prelink_fd_mem = None
        self.g_target_script_name = ""

def gb_init():
    G = GB()
    return G
def gb_create():
    gb = GB()
    gb_init()
    return gb

def gb_delete(G):
    if G:
        del G
    return




def lc_exit(g, arg):
    swc_lc_exit(g, __file__, fr.__LINE__, arg)


def lc_raise(g, arg):
    swc_lc_raise(g, __file__, fr.__LINE__, arg)
