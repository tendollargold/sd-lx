#!/sbin/sh
########
# Product:
# Fileset: NETTL-RUN
# configure
# @(#)B11.23_LR
########
#
# (c) Copyright Hewlett-Packard Company 1993,2002
#
########
    exitval=0			# Anticipate success

    : ${UTILS:="/usr/lbin/sw/control_utils"}
    if [[ ! -f $UTILS ]]
    then
	echo "ERROR:   Cannot find $UTILS"
	exit 1
    fi
    . $UTILS

################################################################################
#
#			      VARIABLES
#
################################################################################

###
# Set variable names (use environment variables if they have been set)
###

: ${LITECONFIG_SAVE_FILE:="/var/adm/liteconfig/SavedRcConf"}
: ${PREV_NETTL_RC:="/usr/old/usr/newconfig/etc/rc.config.d/nettl"}
: ${NETTL_RC_FILE:="/etc/rc.config.d/nettl"}

: ${TRACE_DEV:="/dev/nettrace"}
: ${LOG_DEV:="/dev/netlog"}
: ${KL_DEV:="/dev/kernlog"}

: ${SNMPINFO_FILE:="/etc/SnmpAgent.d/snmpinfo.dat"}


################################################################################


################################################################################
#
#			      FUNCTIONS
#
################################################################################

################################################################################
#
#  CreateRCConfig - ensure that the rc.config.d configuration file is present 
#
################################################################################

CreateRCConfig ()
{

    newconfig_cp ${NETTL_RC_FILE}
    cmd_result=$?

    newconfig_msgs $cmd_result

}

################################################################################
#
#  CreateSNMPInfo - ensure that the snmpinfo.dat file is present
#
################################################################################

CreateSNMPInfo ()
{

    newconfig_cp ${SNMPINFO_FILE}
    cmd_result=$?

    newconfig_msgs $cmd_result

}

################################################################################
#
#  CheckLITECONFIG - check for the presence of the LITECONFIG fileset 
#  If the fileset is installed, set NETTL variable to 0
#
################################################################################
CheckLITECONFIG ()
{
    if [ -r ${LITECONFIG_SAVE_FILE} ]; then
	grep "NETTL" ${LITECONFIG_SAVE_FILE} >/dev/null 2>&1 &&
           if [[ -f ${NETTL_RC_FILE} && ! -f ${PREV_NETTL_RC} ]]; then
	      ch_rc -a -p NETTL=0 ${NETTL_RC_FILE}
		#
		# Print a log message indicating we have turned off nettl.
		#
	      if [[ $? -eq 0 ]]; then 
	         msg  NOTE  " Set \"NETTL\" to \"0\" in \"/etc/rc.config.d/nettl\".
	         This will disable the Network Tracing and Logging (NetTL)
	         subsystem, thereby freeing up approximately 500K bytes of
	         system memory.  NetTL is a network-based error
	         reporting/troubleshooting facility used by many HP networking
	         products.  If you choose to keep nettl disabled, then any
	         networking errors that occur during the operation of the system
	         will not be captured and logged.  For more details, run
	         swinstall interactively and look at the \"Readme\" description
	         file for the DesktopConfig product."
	      fi
	   fi
    fi
}

################################################################################
#
#	CreateDevs - Create trace and log special files
#
################################################################################
CreateDevs()
{
    if [ ! -c ${TRACE_DEV} ]; then 
	mknod ${TRACE_DEV} c 46 0x000000
    fi
    chmog 0644 2 2 ${TRACE_DEV}
    if [ ! -c ${LOG_DEV} ]; then 
	mknod ${LOG_DEV}   c 46 0x000001
    fi
    chmog 0644 2 2 ${LOG_DEV}
    if [ ! -c ${KL_DEV} ]; then
	mknod ${KL_DEV}	   c 46 0x000002
    fi
    chmog 0644 2 2 ${KL_DEV}
}

################################################################################
#
#	Main
#
################################################################################

# Add variables to control bootup behavior if not already present

    CreateRCConfig

# Check for the presence of the LITECONFIG fileset

    CheckLITECONFIG

# Add/Update the kernel configuration

    if [[ -n "${SW_DEFERRED_KERNBLD:-}" ]]; then
	mod_systemfile	 ${SW_SYSTEM_FILE_PATH} -a netdiag1
	retval=$?
	if [[ $retval -ne 0 ]]; then
	    msg ERROR "Could not enter \"netdiag1\" in the ${SW_SYSTEM_FILE_PATH} file."
	    exitval=$FAILURE
	fi
    fi

# Create device files for kernel daemon

    CreateDevs

# Install the snmpinfo.dat file for snmp formatting

    CreateSNMPInfo

    exit $exitval

#
# End of configure script
# 
