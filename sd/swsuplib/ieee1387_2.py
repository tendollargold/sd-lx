#  ieee1387.py -- IEEE 1387.2 Version structure.
#
# Copyright (C) 1998,2004  James H. Lowe, Jr.  <jhlowe@acm.org>
# Copyright (C) 2023 Conversion to Python Paul Weber <paul@weber.net>
# 
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
# 
# UNIX® is a registered trademark of The Open Group.


from sd.swsuplib.versionid import swverid_i_open, swvidc, swverid_get_alternate
from sd.swsuplib.strob import strob_strtok, strob_close
from sd.swsuplib.misc.swuts import swuts_delete
# global sg_swverid_uuid
# sg_swverid_uuid = 0 # provides a uuid number for alternation groups of swverid objects
class VerId:
  """ software_spec['ver_id'] = {'br': _('revision'),
                   'ba': _('architecture'),
                   'bv': _('vendor_tag'),
                   'bl': _('location'),
                   'pr': _('revision'),
                   'pa': _('architecture'),
                   'pv': _('vendor_tag'),
                   'pl': _('location'),
                   'fr': _('revision'),
                   'fl': _('location')
                   }.get(ps_stat[2], _('Unknown')) """
  def __init__(verid):
        verid.ver_id = str(['0' for _ in range(3)])
        verid.id = str(['0' for _ in range(2)])
        verid.vq = str(['0' for _ in range(2)])
        verid.rel_op = str(['0' for _ in range(3)])
        verid.rel_op_code = 0
        verid.value = '0'
        verid.next = None
        """
          * Name space codes
        """
        verid.ns_ival = -2  # invalid or error
        verid.ns_na = -1    # not applicable
        verid.ns_super = 0
        verid.ns_top = 1    # bundles and products
        verid.ns_mid = 2    # subproducts, filesets, control files
        verid.ns_low = 3    # files
        """
          * Comparison result codes
          * Maybe switch to version compare module
        """
        verid.cmp_not_used = -50  # not used
        verid.cmp_error = -40     # error
        verid.cmp_neq = 0
        verid.cmp_lt = 1
        verid.cmp_lte = 2
        verid.cmp_eq2 = 3
        verid.cmp_eq = 4
        verid.cmp_gte = 5
        verid.cmp_gt = 6
        """
          * Comparison result codes
          * Maybe switch to version compare module
        """
        verid.relop_neq = "!="
        verid.relop_lt = "<"
        verid.relop_lte = "<="
        verid.relop_eq2 = "="
        verid.relop_eq = "=="
        verid.relop_gte = ">="
        verid.relop_gt = ">"
        """
          * Version Id attributes abbreviations
          * Maybe switch to version compare module
          POSIX version Ids

          ver_id  attribute    object
         -------------------------------
            br    revision     bundle
            ba    architecture bundle
            bv    vendor_tag   bundle
            bl    location     bundle
            pr    revision     product
            pa    architecture product
            pv    vendor_tag   product
            pl    location     product
            fr    revision     fileset
            fl    location     fileset
         for Linux, revision will contain 'version-release' number
      """
        verid.posix = "ravlq"
        verid.qualifier = "bpf"
        verid.posix_name_error = "0"
        verid.posix_revision = "r"
        verid.posix_architecture = "a"
        verid.posix_vendor_tag = "v"
        verid.posix_location = "l"
        verid.posix_qualifier = "q"
        """
          * Extended Version Id attributes abbreviations
          * Maybe switch to version compare module
        """
        verid.posix_extension = "ig"  # Formerly swbis
        verid.posix_catalog_instance = "i"
        verid.posix_rpmflag = "g"

        verid.qualifier_product = "p"
        verid.tag = 0
        verid.name_error = verid.posix_name_error[0]
        verid.revision = verid.posix_revision[0]
        verid.architecture = verid.posix_architecture[0]
        verid.vendor_tag = verid.posix_vendor_tag[0]
        verid.location = verid.posix_location[0]
        verid.qualifier = verid.posix_qualifier[0]
        verid.catalog_instance = verid.posix_catalog_instance[0]

#class SwVerid:
class SwVerid:

    def __init__(swverid): # Object Version Spec
        swverid.object_name = '0'      # object name, i.e object keyword.
        swverid.source_copy = '0'      # copy of swverid version string
        swverid.catalog = '0'          # ``catalog'' attribute.
        swverid.use_path_compare = 0   # if true, use comparison that disregards leading slashes
        swverid.taglist = []           # list of tags, Note: The first tag in the list is the
                                       # left most tag to which the version spec applies
        swverid.namespace = 0          # namespace code
        swverid.comparison_code = 0    # `this object'  \<comparision_code_\> `other object'
        swverid.ver_id_list = None     #  linked list of version id's
        swverid.swuts = None           # POSIX System Id attributes
        swverid.alt = None             #  Next object, to support dependency alternation
        swverid.alter_uuid = 0         #  unique to an alternation group 

def swverid_open(object_keyword, swversion_string):
    swverid = None
    parent = None
    last = None
    tmp = []
    i = 0
    s = None
    if swversion_string is not None:
        i = 0
        last = None
        parent = None
        tmp = []
        s = swversion_string.split("|")
        for s in s:
            swverid = swverid_i_open(object_keyword, s)
            if swverid is None:
                return None
            if last:
                last.altM = swverid
            last = swverid
            if i == 0:
                parent = swverid
            s = strob_strtok(tmp, None, "|\n\r")
            i += 1
        if parent is None:
            return None
        last = parent
        if swvidc.sg_swverid_uuid == 0:
            swvidc.sg_swverid_uuid += 1
        parent.alter_uuid = swvidc.sg_swverid_uuid
        while last == swverid_get_alternate(last):
            last.alter_uuid = parent.alter_uuid
        strob_close(tmp)
    else:
        parent = swverid_i_open(object_keyword, None)
        if parent:
            parent.alter_uuid = swvidc.sg_swverid_uuid
    return parent

def swverid_close(swverid):
    vid = None
    ver_id = swverid.ver_id_list
    while ver_id:
        vid = ver_id.next
        ver_id = vid
    if swverid.object_name:
        swverid.object_name = None
    if swverid.source_copy:
        swverid.source_copy = None
    # if (swverid->alt) swverid_close(swverid->alt);

    # FIXME this causes a core dump: cplob_close(swverid->taglist;
    swuts_delete(swverid.swuts)



