# sd.util.py
# Basic sd utils.
#
# Copyright (C) 2023 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals
from datetime import datetime
from .pycomp import PY3
import argparse
import os
import sys
import struct
import locale
# import signal
import socket
import tempfile
import pwd
import shutil
import time
import sd
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlib_doif_writeap, swlibc
from sd.swsuplib.swutillib import swutil_doif_writef, swlib_do_log_is_true, SwLog

from sd.swsuplib.uinfile.uinfile import UinfileConstants

uic = UinfileConstants
swlib_doif_writef = swutil_doif_writef

def fprintf(stream, format_spec, *args):
    stream.write(format_spec % args)


# Let's simulate a simple dynamic memory allocator
# which resembles the C malloc but is implemented in Python

class MemoryAllocator:
    def __init__(self, size: int):
        # we're simulating memory with a bytearray
        self.memory = bytearray(size)
        # this dictionary will keep track of allocated blocks
        self.allocated_blocks = {}

    def malloc(self, size: int):
        # find a block of memory that is not allocated yet
        for i in range(len(self.memory) - size + 1):
            # check if the slice is free
            if all(b == 0 for b in self.memory[i:i + size]):
                # if free, this is our allocated block
                self.memory[i:i + size] = bytearray([1] * size)
                # memory_block = (ctypes.c_char * size).from_buffer(self.memory, i)
                memory_block = struct.pack("i", size)
                self.allocated_blocks[memory_block] = (i, size)
                return memory_block
        # if no blocks are free, we raise an error
        raise MemoryError('Out of memory')

    def free(self, memory_block):
        # free an allocated block
        start_index, size = self.allocated_blocks.pop(memory_block)
        self.memory[start_index:start_index + size] = bytearray([0] * size)


print("Entering util.py")
print("")

#logger = logging.getLogger('sd')
now = datetime.now()
default_target = "/"
hostname= socket.gethostname()
host_target = hostname+":"+default_target

# Select which sw<> to run?
MAIN_PROG = argparse.ArgumentParser().prog if argparse.ArgumentParser().prog == "sd" else "swlist"
MAIN_PROG_UPPER = MAIN_PROG.upper()

"""SD Utilities."""
# Rewrite sd_parse_specs 7/5/23
def sd_parse_specs(namespace, values):
    """
    Categorize :param values list into Bundles, products, and filesets

    :param namespace: argparse.Namespace, where specs will be stored
    :param values: list of specs, whether packages ('foo') or groups/modules ('@bar')
                   or filenames ('*.rmp', 'https://*', ...)

    To access filesets use: specs.filesets,
    to access products use: specs.products,
    to access filenames use: specs.filenames
    """
# software_specification Software specifications are used to specify software in
# dependencies, ancestors and other attributes
# Example:    SD,r=2.0,a=HP-UX_B.11.00_32
# Dependencies are specified as a software_specification value type
#    corequisites     SD.data
#    prerequisites    productA, r >= 2.1
#    exrequisites    productB, r >= 2.1

    setattr(namespace, "file_specs", [])
    setattr(namespace, "fileset_specs", []) #
    setattr(namespace, "software_specs", [])# pkg_specs
    setattr(namespace, "bundle_specs", []) # grp_specs
    setattr(namespace, "product_specs", []) #
    setattr(namespace, "subproduct_specs", [])
    tmp_set = set()
    for value in values:
        if value in tmp_set:
            continue
        tmp_set.add(value)
        schemes = sd.pycomp.urlparse.urlparse(value)[0]
        if value.endswith('.depot'):
            namespace.filenames.append(value)
        elif schemes and schemes in ('http', 'ssh', 'file', 'https'):
            namespace.filenames.append(value)
        elif value.startswith('@'):
            namespace.bundles.append(value[1:])
        else:
            namespace.products.append(value)
def _urlopen_progress(url, conf, progress=None):
    if progress is None:
        print("URL: "+url)
        pass
    try:
       pass
    except RuntimeError as e:
        if conf.strict:
            raise IOError(str(e))
        #logger.error(str(e))
    return #pload.local_path

def _urlopen(url, conf=None, repo=None, mode='w+b', **kwargs):
    """
    Open the specified absolute url, return a file object
    which respects proxy setting even for non-repo downloads
    """
    if PY3 and 'b' not in mode:
        kwargs.setdefault('encoding', 'utf-8')
    fo = tempfile.NamedTemporaryFile(mode, **kwargs)

#    try:
#        if depot:
#            depot._depot.downloadUrl(url, fo.fileno())
#        else:
       #     libsd.depot.Downloader.downloadURL(conf._config if conf else None, url, fo.fileno())
#            print("FIX ME")
#    except RuntimeError as e:
 #       raise IOError(str(e))

    fo.seek(0)
    return fo

def printlogline(buf, sptr):
    tm = time.time()
    result = time.asctime(time.localtime(tm))
    result = result[:len(result)-1]
    s = result
    s = result[result.index(' ')+1:] if ' ' in result else result
    result = result.rstrip('\n')
    buf = f"{s} [{os.getpid()}] {sptr}"
    return 0

def rtrim(s, r):
    if s.endswith(r):
        s = s[:-len(r)]
    return s

def normalize_time(timestamp):
    """Convert time into locale aware datetime string object."""
    t = time.strftime("%c", time.localtime(timestamp))
    if not sd.pycomp.PY3:
        current_locale_setting = locale.getlocale()[1]
        if current_locale_setting:
            t = locale.setlocale(locale.LC_ALL)
    return t
def rm_rf(path):
    try:
        shutil.rmtree(path)
    except OSError:
        pass

def swutil_open():
    swutil = SwLog()
    if swutil is None:
        return None
    swutil.swu_efd = sys.stderr.fileno()
    swutil.swu_logspec = None
    swutil.swu_verbose = 1
    swutil.swu_fail_loudly = 0
    return swutil

def swutil_close(swutil):
    swutil = None

def doif_i_writef(verbose_level, write_at_level, logspec, fd, format, *pap):
    buffer = ""
    newret = 0
    level = verbose_level
    if write_at_level >= 0 and level < write_at_level:
        pass
    else:
        buffer = ""
        if write_at_level >= swlibc.SWC_VERBOSE_7:
            buffer = "debug> "
            buffer += swlib_utilname_get()
            buffer += "[%d]: " % os.getpid()
        else:
            buffer += swlib_utilname_get()
        newret = swlib_doif_writeap(fd, buffer, format, *pap)
    if logspec and logspec.logfd > 0:
        if swlib_do_log_is_true(logspec, verbose_level, write_at_level):
            if fd == sys.stderr.fileno() or fd == sys.stdout.fileno() or fd < 0:
                logbuf = ""
                buffer = ""
                if write_at_level >= swlibc.SWC_VERBOSE_7:
                    buffer = "debug> "
                    buffer += swlib_utilname_get()
                    buffer += "[%d]: " % os.getpid()
                else:
                    buffer += swlib_utilname_get()
                printlogline(logbuf, buffer)
                newret = swlib_doif_writeap(logspec.logfd, logbuf, format, *pap)
        else:
            newret = 0
    return newret


def checkinstall():
    return

def checkacl():
    print("util.py checkacl")
    return


def sessionstart(cmd, username, host_target):
    print(host_target)
    print('util.py')
    print("=======", now.strftime("%m/%d/%Y %H:%M:%S"),"BEGIN ",cmd, "SESSION")
    print('')
    print('       * Session started for user ',username+' '+host_target)
    print('')
    return

def createdict(*args):
    return dict (((k, eval(k)) for k in args))

def delete_none(_dict):
    """Delete None values recursively from all of the dictionaries, tuples, lists, sets"""
    print('Deleting None Values in delete_none ')
    if isinstance(_dict, dict):
        for key, value in list(_dict.items()):
            if isinstance(value, (list, dict, tuple, set)):
                _dict[key] = delete_none(value)
            elif value is None or key is None:
                del _dict[key]

    elif isinstance(_dict, (list, set, tuple)):
        _dict = type(_dict)(delete_none(item) for item in _dict if item is not None)

    print(_dict)
    return _dict

def updtipddict(line, ipddict):
    line = line.strip()
    if line == '' or line == '"':
      data = "'empty':'line'"
    elif line == '"':
      data = "'end':'quote'"
    else:
      data = line.split(' ',1)
    ipddict.update( {data[0]:data[1]} )
    return ipddict

def findipdindexes(level, swselect, path):
    result = []
#    if level == 'bundle' or level == 'product':

    for root, dirs, files in os.walk(path):
        if 'INDEX' in files:
            result.append(os.path.join(root, 'INDEX'))
    # The first entry should be the 'global' INDEX file
    del result[0]
    print(result)
    # Remove the 'global'INDEX file.
    return result

def readipd(level, swselect, swattr, swindex, targets):

    print("Entered readipd function")
    print("Passed level selection: "+level)
    print("")
    print("Passed software selections ")
    print(swselect)
    print("")
    print("Passed software attributes ")
    print(swattr)
    print("")
    print("Passed software index ")
    print(swindex)
    print("")
    print("Passed software targets ")
    print(targets)
    print("")
    
    # open @target:/ or @target:/depot later
    # for now just local
    ipddict=dict()
    if level == 'bundle' and swselect is None:
      pass

    if level == 'product':
      # If level is set to product and swselect is a bundle
      # The prodcuts are listed within the  'contents' attribute
      with open(swindex, "rt") as index:
#       print("if level == product, for line in index")
        for line in index:
          line = line.rstrip()
          if line.startswith('vendor',0) and not line.startswith('vendor_tag',0):
            # Print 'vendor' line
            print(line)
#           print("elif line.startswith vendor, for line in index")
#           print("=================================================")
            for line in index:
              line = line.rstrip()
              print(line)
              if line.startswith('end'):
                break

          elif line.startswith('bundle',0):
            # Print 'bundle' or 'product' line
            print(line)
#           print("if line starts with bundle, for line in index")
#           print("=================================================")
            for line in index:
              line = line.rstrip()
              if line.startswith('description') or line.startswith('copyright'):
#               print("if line.startswith description, for line in index")
#               print("=================================================")
                lineno=0
                if line.startswith('description') or line.startswith('copywrite'):
                  print(line)
                  for line in index:
                    # Remove only the trailing newline from the text
                    line = line.rstrip()
                    if line.startswith('"') or line.endswith('"'):
#                     print("if line.startswith or endswith double quotes")
#                     print("=================================================")
                      print(line)
                      break
                    else:
                      print(line)
                  break
                break
              
          elif line.startswith('contents'):
#           print("line.starts with contents")
            for line in index:
              line = line.rstrip()
              if line.startswith('contents'):
#               print("contents: "+line)
                print(line)
              else:
                break

              # Print other non special lines
#         print("non special: "+line)
          print(line)

    if level == 'subproduct':
      with open(swindex, "rt") as index:
        for line in index:
          if line.startswith(level,0):
            # Print 'subproduct' line
#           print("line starts with "+level)
            line = line.strip()
            print("=================================================")
            print(line)
            for line in index:
              if line.startswith('contents'):
                line = line.strip()
                print(line.split(" "[1]))
                print("=================================================")
                while line.startswith('contents'):
                  line = line.strip()
                  print(line.split(" "[1]))
                break
              else:
                line = line.strip()
                print("=================================================")
                print(line)


def sessionend():
    print(host_target)
    return

def _depotavail_():
    print("util.py _depotavail_")
    return

def am_i_root():
    print("util.py am_i_root")
    return os.geteuid() == 0

def rm_depot(path):
    print("util.py rm_depot")
    """Remove all products and bundles under `depot path`

    Also see rm_rf()

    """
    for entry in os.listdir(path):
        contained_path = os.path.join(path, entry)
        rm_rf(contained_path)

def file_timestamp(fn):
    return os.stat(fn).st_mtime

def get_effective_login():
    try:
        return pwd.getpwuid(os.geteuid())[0]
    except KeyError:
        return "UID: %s" % os.geteuid()

def product_by_filter(fn, iterable):
    return

def filespace_calculation(pred, t1, t2):
    return sd.pycomp.filterfalse(pred, t1), filter(pred, t2)


def strip_prefix(s, prefix):
    if s.startswith(prefix):
        return s[len(prefix):]
    return None

