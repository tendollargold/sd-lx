
# phases.swverify.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals


"""
    4.0 Execution Phase Systems Management: Distributed Software Administration
    swverify utility Page 121 - 124

    Check for extended options
    Extended Options
    The swverify utility supports the following extended options. The description in
    Chapter 3 applies.
"""

swverifyoptions={
    'allow_incompatible': bool(False),
    'autoselect_dependencies': bool(True),
    'check_contents': bool(True),
    'check_permissions': bool(True),
    'check_requisites': bool(True),
    'check_scripts': bool(True),
    'check_volatile': bool(False),
    'distribution_target_directory': "/var/spool/sw",
    'enforce_dependencies': bool(True),
    'enforce_locatable': bool(True),
    'installed_software_catalog': "/var/adm/sw/products",
    'logfile': "/var/adm/sw/swverify.log",
    'loglevel': "1",
    'select_local': bool(True),
    'software': "",
    'targets': "",
    'verbose': "1"
}




def swverify_options():
    """Collection of options for swverify utility. :api"""
    swverifyoptions.update(globals())



# The swverify utility consists of three phases:
#  1. Selection Phase
#  2. Analysis Phase
#  3. Execution Phase

#
# Selection Phase
#

# Like swremove, software selections apply to the software installed (or available in
# the case of a distribution).

# Each specified selection is added to the selection list after it passes the 
# following checks:

#  · If the selection is not found, generate an event.
#    [SW_WARNING: SW_SELECTION_NOT_FOUND]

#  · If the selection is not found at that product location, but that product exists 
#    at another location, generate an event.
#    [SW_WARNING: SW_SELECTION_NOT_FOUND_RELATED]

# Add any dependencies to the selection list if autoselect_dependencies=true.

#
# Analysis Phase
#

# This section details the analysis phase for swverify. No aspect of the target host 
# environment is modified unless the -F option is specified. The target role accesses
# its software_collection catalog to get the information for the selected software.

# The target role makes the following checks:

#  · An event is generated for each product that is incompatible with the uname 
#    attributes of the target host. See Section 3.4.1.2 on page 42. If 
#    allow_incompatible=false, generate an event.
#    [SW_ERROR: SW_NOT_COMPATIBLE]

#    If allow_incompatible=true, generate an event.
#    [SW_WARNING: SW_NOT_COMPATIBLE]

#    Applies to installed software.

#  · An event is generated for each fileset whose state is other than installed, 
#    configured, available, or removed.
#    [SW_WARNING: SW_SELECTION_IS_CORRUPT]

#    Applies to distributions and installed software.

#  · An event is generated if a dependency can not be met. If 
#    enforce_dependencies=true, generate an event.
#    [SW_ERROR: SW_DEPENDENCY_NOT_MET]

#    If enforce_dependencies=false, generate an event.
#    [SW_WARNING: SW_DEPENDENCY_NOT_MET]

#    Applies to distributions and installed software.

#  · Executes vendor-supplied verify scripts, generating an event if a verify script 
#    returns either an error or a warning.
#    [SW_ERROR: SW_CHECK_SCRIPT_ERROR]
#    [SW_WARNING: SW_CHECK_SCRIPT_WARNING]

#    Applies to installed software.

#  · The following file level checks are made:

#    — Check for missing files and directories. For installed software, if 
#      check_volatile=false, then this check must not be made for files with file.
#      is_volatile=true.

#      Applies to distributions and installed software.

#    — Check for files that have been modified.  For distributions, check size, cksum,
#      and mtime.

#      For installed software, 
#        check mode, owner, group, size, cksum, mtime, revision, major, and minor,
#        if defined for that file object.
#        If check_volatile=false, then these checks must not be made for files with 
#        file.is_volatile=true.

#    — If a file is compressed, then the compressed_size and compressed_cksum 
#      attributes of the file should be checked instead of the size and cksum 
#      attributes.

#      Applies to distributions.

#    — Check symbolic links for correct values.  Applies to distributions and 
#      installed software.

#    If any of these checks fail for any file, generate an event.
#    [SW_ERROR: SW_FILE_ERROR]

# For patches, the verify operation on a patched fileset will check that the patched 
# files are properly installed. When installing a patch, the ancestor fileset will be 
# updated to have the correct attributes of the patched files. Verification of a patch
# fileset will verify that files in a patch are still properly installed (or in the 
# depot correctly). For installed patches that have the fileset saved_files_directory 
# defined, the saved files in the saved_files_directory will also be verified.  This 
# verification ensures that a patch can still be successfully rolled back.

#
# Execution Phase
#

# If the -F option is set, then the execution phase operations are run.

#  · Execute Fix Scripts

#    In this step, swverify executes vendor-supplied fix scripts if operating on 
#    installed software.

#    Scripts are executed in the same order as verify scripts. If a fix script returns
#    an error, generate an event.
#    [SW_ERROR: SW_PRE_SCRIPT_ERROR]

#    If a fix script returns a warning, generate an event.
#    [SW_WARNING: SW_PRE_SCRIPT_WARNING]

#    Control scripts adhere to the specifications in section Section 3.6.1 on page 56.

#  · File Level Fix

#    The following file level fixes are made:

#    — Missing directories are created (except volatile unless the check_volatile 
#      option is true)

#    — Files that have been modified (except volatile unless the check_volatile 
#      option is true) are fixed for mode, owner, group, major, and minor, as 
#      applicable

#    — Symbolic links are re-created to correct their values.

#    — Any files with type x (delete file) as a new item are removed.

#    If any of these fixes fail for any file, generate an event.
#    [SW_ERROR: SW_FILE_ERROR]
