# packaging.rpmvercmp.py
# rpmvercmp from rpm-4.16.3
#
## This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

from sd.swsuplib.misc.cstrings import strcmp

def rstreq(s1, s2):
    return 1 if (strcmp(s1, s2) == 0) else 0
def risalnum(c):
    return 1 if (risalpha(c) or risdigit(c)) else 0
def risalpha(c):
    return 1 if (rislower(c) or risupper(c)) else 0
def rislower(c):
    return 1 if ('a' <= c <= 'z') else 0
def risupper(c):
    return 1 if ('A' <= c <= 'Z') else 0

def risdigit(c):
    return 1 if ('0' <= c <= '9') else 0
def swrpmvercmp(a, b):
    # easy comparison to see if versions are identical
    if rstreq(a, b):
        return 0

 #   oldch1 = '\0'
 #   oldch2 = '\0'
    str1 = a
    str2 = b

    one = str1
    two = str2

    # loop through each version segment of str1 and str2 and compare them
    while one or two:
        while one and not risalnum(one) and one != '~' and one != '^':
            one += 1
        while two and not risalnum(two) and two != '~' and two != '^':
            two += 1

        # handle the tilde separator, it sorts before everything else
        if one == '~' or two == '~':
            if one != '~':
                return 1
            if two != '~':
                return -1
            one += 1
            two += 1
            continue

        #
        #         * Handle caret separator. Concept is the same as tilde,
        #         * except that if one of the strings ends (base version),
        #         * the other is considered as higher version.
        #
        if one[0] == '^' or two[0] == '^':
            if not one:
                return -1
            if not two:
                return 1
            if one[0] != '^':
                return 1
            if two[0] != '^':
                return -1
            one = one[1:]
            two = two[1:]
            continue

        # If we ran to the end of either, we are finished with the loop
        if not (one and two):
            break
        str1 = one
        str2 = two

        # grab first completely alpha or completely numeric segment
        # leave one and two pointing to the start of the alpha or numeric
        # segment and walk str1 and str2 to end of segment
        if str1.isdigit():
            while str1 and str1.isdigit():
                str1 += 1
            while str2 and str2.isdigit():
                str2 += 1
            isnum = 1
        else:
            while str1 and str1.isalpha():
                str1 += 1
            while str2 and str2.isalpha():
                str2 += 1
            isnum = 0

        # save character at the end of the alpha or numeric segment
        # so that they can be restored after the comparison
        oldch1 = str1[0]
        str1[0] = '\0'
        oldch2 = str2[0]
        str2[0] = '\0'

        # this cannot happen, as we previously tested to make sure that
        # the first string has a non-null segment
        if one == str1:
            return -1 # arbitrary

        # take care of the case where the two version segments are
        # different types: one numeric, the other alpha (i.e. empty)
        # numeric segments are always newer than alpha segments
        # XXX See patch #60884 (and details) from bugzilla #50977.
        if two == str2:
            return 1 if isnum else -1

        if isnum:
            one = one.lstrip('0')
            two = two.lstrip('0')
            # this used to be done by converting the digit segments
            # to ints using atoi() - it's changed because long
            # digit segments can overflow an int - this should fix that.

            # throw away any leading zeros - it's a number, right?
            while one == '0':
                one += 1
            while two == '0':
                two += 1

            # whichever number has more digits wins
            onelen = len(one)
            twolen = len(two)
            if onelen > twolen:
                return 1
            if twolen > onelen:
                return -1

        # strcmp will return which one is greater - even if the two
        # segments are alpha or if they are numeric.  don't return
        # if they are equal because there might be more segments to
        # compare
        rc = strcmp(one, two)
        if rc != 0:
            if rc < 1:
                return -1
            else:
                return 1

        # restore character that was replaced by null above
        str1 = oldch1
        one = str1
        str2 = oldch2
        two = str2

    # this catches the case where all numeric and alpha segments have
    # compared identically but the segment separating characters were
    # different
    if (not one != '\0') and (not two != '\0'):
        return 0

    # whichever version still has characters left over wins
    if not one != '\0':
        return -1
    else:
        return 1
