# swutilname.py - Set and Get the utility name at runtime
#

#
#   Copyright (C) 1998-2004  James H. Lowe, Jr.
#   Copyright (C) 2023-2024 Paul Weber, Python conversion
#   All rights reserved.
#
#   COPYING TERMS AND CONDITIONS
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

global swutilname
def swlib_utilname_set(s):
    global swutilname
    swutilname = s[:29]
    swutilname = swutilname + '\0'

def swlib_utilname_get():
    return swutilname