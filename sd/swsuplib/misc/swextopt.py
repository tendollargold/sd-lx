# swextopt.py - sw exteneded options
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import sys
import errno
import pty
import io

from sd.swsuplib.misc.cstrings import strrchr, strstr, strchr, strdup, strcmp, strcasecmp, strerror
from sd.swsuplib.misc.swlib import swlibc, swlib_unix_dircat, swlib_writef, swlib_tr, swlib_is_sh_tainted_string_fatal
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.strob import (STROB_DO_APPEND, strob_open, strob_close, strob_strtok, strob_strcpy, strob_str, strob_strlen, strob_sprintf)
from sd.swsuplib.uxfio import uc, uxfio_open, uxfio_get_dynamic_buffer, uxfio_fcntl, uxfio_close, uxfio_write, uxfio_lseek
from sd.swsuplib.atomicio import atomicio
from sd.swsuplib.misc.swheader import swheaderline_get_keyword, swheaderline_get_value
from sd.swsuplib.swparse.swparse import sw_yyparse, SwparseConstants
from sd.const import SwGlobals, lc

SwP = SwparseConstants
swextopt_statusG = 1
SYSTEM_DEFAULTS_FILE = os.path.join(SwGlobals.SWLIBDIR, "swbis", "swdefaults")
SYSTEM_SWDEFAULTS_FILE = os.path.join(SwGlobals.SWLIBDIR, "swbis", "swbisdefaults")

# Set default parameters with a dictionary sd.defaults.py. It contains each option with
# its system default value, Change the key value pair within the dictionary based on
# /etc/sw/sw.config, ~/.config, or command line option

# This file is the swbis method. Use this until we get the whole program translated
# to python

class extendedOptions:
    def __init__(self):
        self.is_bool = ''
        self.util_set = ''
        self.option_set = ''
        self.optionName = ''
        self.defaultValue = ''
        self.value = ''
        self.app_flags = 0

class EoptsConst:
    SWC_U_A = (1 << 0)
    SWC_U_C = (1 << 1)
    SWC_U_I = (1 << 2)
    SWC_U_G = (1 << 3)
    SWC_U_L = (1 << 4)
    SWC_U_M = (1 << 5)
    SWC_U_P = (1 << 6)
    SWC_U_R = (1 << 7)
    SWC_U_V = (1 << 8)
    SWC_U_X = (1 << 9)
    SW_E_TRUE = "True"
    SW_E_FALSE = "False"
    FILE_URL = "file:///"
    SWC_U_ASK = SWC_U_A
    SWC_U_COPY = SWC_U_C
    SWC_U_INSTALL = SWC_U_I
    SWC_U_CONFIG = SWC_U_G
    SWC_U_LIST = SWC_U_L
    SWC_U_MODICY = SWC_U_M
    SWC_U_PACKAGE = SWC_U_P
    SWC_U_REMOVE = SWC_U_R
    SWC_U_VERIFY = SWC_U_V
    SWC_U_NOTAPPLICABLE = SWC_U_X

Ec = EoptsConst
optionsArray = [
    {"allow_downdate": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {"allow_incompatible": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"allow_multiple_versions": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_G}},
    {"ask": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {"autoreboot": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {"autorecover": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {"autoselect_dependencies": {"enabled": 0, "required": 0, "default": 0, "value": "as_needed", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_C|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"autoselect_dependents": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_G}},
    {"check_contents": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_V}},
    {"check_permissions": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_V}},
    {"check_requisites": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_V}},
    {"check_scripts": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_V}},
    {"check_volatile": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_V}},
    {"compress_files": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": 0|Ec.SWC_U_C}},
    {"compression_type": {"enabled": 0, "required": 0, "default": 0, "value": "gzip", "choices": None, "flags": 0|Ec.SWC_U_C}},
    {"defer_configure": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {"distribution_source_directory": {"enabled": 0, "required": 0, "default": 0, "value": "-", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_C}},
    {"distribution_target_directory": {"enabled": 0, "required": 0, "default": 0, "value": "/", "choices": None, "flags": Ec.SWC_U_P|Ec.SWC_U_C|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V}},
    {"distribution_target_serial": {"enabled": 0, "required": 0, "default": 0, "value": "-", "choices": None, "flags": Ec.SWC_U_P}},
    {"enforce_dependencies": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_C|Ec.SWC_U_R|Ec.SWC_U_V}},
    {"enforce_dsa": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_P|Ec.SWC_U_I|Ec.SWC_U_C}},
    {"enforce_locatable": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_V}},
    {"enforce_scripts": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_R|Ec.SWC_U_G}},
    {"files": {"enabled": 0, "required": 0, "default": 0, "value": None, "choices": None, "flags": 0}},
    {"follow_symlinks": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"installed_software_catalog": {"enabled": 0, "required": 0, "default": 0, "value": "var/lib/swbis/catalog", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"logfile": {"enabled": 0, "required": 0, "default": 0, "value": "/var/log/sw.log", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_C|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"loglevel": {"enabled": 0, "required": 0, "default": 0, "value": "1", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_C|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"media_capacity": {"enabled": 0, "required": 0, "default": 0, "value": "0", "choices": None, "flags": Ec.SWC_U_P}},
    {"media_type": {"enabled": 0, "required": 0, "default": 0, "value": "serial", "choices": None, "flags": Ec.SWC_U_P}},
    {"psf_source_file": {"enabled": 0, "required": 0, "default": 0, "value": "-", "choices": None, "flags": Ec.SWC_U_P}},
    {"one_liner": {"enabled": 0, "required": 0, "default": 0, "value": "title revision tag", "choices": None, "flags": Ec.SWC_U_L}},
    {"reconfigure": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_G}},
    {"recopy": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": 0|Ec.SWC_U_C}},
    {"reinstall": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {"select_local": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_C|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"software": {"enabled": 0, "required": 0, "default": 0, "value": "", "choices": None, "flags": 0}},
    {"targets": {"enabled": 0, "required": 0, "default": 0, "value": "", "choices": None, "flags": 0}},
    {"uncompress_files": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": 0|Ec.SWC_U_C}},
    {"verbose": {"enabled": 0, "required": 0, "default": 0, "value": "1", "choices": None, "flags": Ec.SWC_U_P|Ec.SWC_U_I|Ec.SWC_U_C|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_cksum": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_file_digests": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_file_digests_sha2": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_files": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_signer_pgm": {"enabled": 0, "required": 0, "default": 0, "value": "GPG", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_sign": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_archive_digests": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_archive_digests_sha2": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_gpg_name": {"enabled": 0, "required": 0, "default": 0, "value": "", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_gpg_path": {"enabled": 0, "required": 0, "default": 0, "value": "~/.gnupg", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_gzip": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_bzip2": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_numeric_owner": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_absolute_names": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_format": {"enabled": 0, "required": 0, "default": 0, "value": "ustar", "choices": None, "flags": Ec.SWC_U_P|Ec.SWC_U_I}},
    {"swbis_no_analysis_phase": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_X}},
    {"swbis_archive_reader": {"enabled": 0, "required": 0, "default": 0, "value": "tar", "choices": None, "flags": Ec.SWC_U_X}},
    {"swbis_no_audit": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_C}},
    {"swbis_local_pax_write_command": {"enabled": 0, "required": 0, "default": 0, "value": "tar", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_remote_pax_write_command": {"enabled": 0, "required": 0, "default": 0, "value": "tar", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_local_pax_read_command": {"enabled": 0, "required": 0, "default": 0, "value": "tar", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_remote_pax_read_command": {"enabled": 0, "required": 0, "default": 0, "value": "tar", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_local_pax_remove_command": {"enabled": 0, "required": 0, "default": 0, "value": "gtar", "choices": None, "flags": Ec.SWC_U_R}},
    {"swbis_remote_pax_remove_command": {"enabled": 0, "required": 0, "default": 0, "value": "gtar", "choices": None, "flags": Ec.SWC_U_R}},
    {"swbis_quiet_progress_bar": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I}},
    {"swbis_no_remote_kill": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_no_getconf": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_shell_command": {"enabled": 0, "required": 0, "default": 0, "value": "detect", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_R|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_enforce_file_md5": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {"swbis_allow_rpm": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L}},
    {"swbis_remote_shell_client": {"enabled": 0, "required": 0, "default": 0, "value": "/usr/bin/ssh", "choices": None, "flags": Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_L}},
    {"swbis_install_volatile": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_I}},
    {"swbis_volatile_newname": {"enabled": 0, "required": 0, "default": 0, "value": "", "choices": None, "flags": Ec.SWC_U_I}},
    {"swbis_any_format": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_L|Ec.SWC_U_I|Ec.SWC_U_P|Ec.SWC_U_C}},
    {"swbis_forward_agent": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_L|Ec.SWC_U_C|Ec.SWC_U_I|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_ignore_scripts": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I|Ec.SWC_U_G}},
    {"swbis_sig_level": {"enabled": 0, "required": 0, "default": 0, "value": "0", "choices": None, "flags": Ec.SWC_U_R|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_enforce_all_signatures": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_R|Ec.SWC_U_I|Ec.SWC_U_L|Ec.SWC_U_V|Ec.SWC_U_G}},
    {"swbis_remove_volatile": {"enabled": 0, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_R}},
    {"swbis_check_mtime": {"enabled": 0, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_V}},
    {"swbis_check_duplicates": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_P}},
    {"swbis_check_owners": {"enabled": 1, "required": 0, "default": 0, "value": "true", "choices": None, "flags": Ec.SWC_U_V}},
    {"swbis_replacefiles": {"enabled": 1, "required": 0, "default": 0, "value": "false", "choices": None, "flags": Ec.SWC_U_I}},
    {None: {"enabled": 1, "required": 0, "default": 0, "value": None, "choices": None, "flags": 0}}
]
class eOpts:
    SW_E_allow_downdate = 0
    SW_E_allow_incompatible = 1
    SW_E_allow_multiple_versions = 2
    SW_E_ask = 3
    SW_E_autoreboot = 4
    SW_E_autorecover = 5
    SW_E_autoselect_dependencies = 6
    SW_E_autoselect_dependents = 7
    SW_E_check_contents = 8
    SW_E_check_permissions = 9
    SW_E_check_requisites = 10
    SW_E_check_scripts = 11
    SW_E_check_volatile = 12
    SW_E_compress_files = 13
    SW_E_compression_type = 14
    SW_E_defer_configure = 15
    SW_E_distribution_source_directory = 16
    SW_E_distribution_target_directory = 17
    SW_E_distribution_target_serial = 18
    SW_E_enforce_dependencies = 19
    SW_E_enforce_dsa = 20
    SW_E_enforce_locatable = 21
    SW_E_enforce_scripts = 22
    SW_E_files = 23
    SW_E_follow_symlinks = 24
    SW_E_installed_software_catalog = 25
    SW_E_logfile = 26
    SW_E_loglevel = 27
    SW_E_media_capacity = 28
    SW_E_media_type = 29
    SW_E_psf_source_file = 30
    SW_E_one_liner = 31
    SW_E_reconfigure = 32
    SW_E_recopy = 33
    SW_E_reinstall = 34
    SW_E_select_local = 35
    SW_E_software = 36
    SW_E_targets = 37
    SW_E_uncompress_files = 38
    SW_E_verbose = 39
    SW_E_swbis_cksum = 40
    SW_E_swbis_file_digests = 41
    SW_E_swbis_file_digests_sha2 = 42
    SW_E_swbis_files = 43
    SW_E_swbis_signer_pgm = 44
    SW_E_swbis_sign = 45
    SW_E_swbis_archive_digests = 46
    SW_E_swbis_archive_digests_sha2 = 47
    SW_E_swbis_gpg_name = 48
    SW_E_swbis_gpg_path = 49
    SW_E_swbis_gzip = 50
    SW_E_swbis_bzip2 = 51
    SW_E_swbis_numeric_owner = 52
    SW_E_swbis_absolute_names = 53
    SW_E_swbis_format = 54
    SW_E_swbis_no_analysis_phase = 55
    SW_E_swbis_archive_reader = 56
    SW_E_swbis_no_audit = 57
    SW_E_swbis_local_pax_write_command = 58
    SW_E_swbis_remote_pax_write_command = 59
    SW_E_swbis_local_pax_read_command = 60
    SW_E_swbis_remote_pax_read_command = 61
    SW_E_swbis_local_pax_remove_command = 62
    SW_E_swbis_remote_pax_remove_command = 63
    SW_E_swbis_quiet_progress_bar = 64
    SW_E_swbis_no_remote_kill = 65
    SW_E_swbis_no_getconf = 66
    SW_E_swbis_shell_command = 67
    SW_E_swbis_enforce_file_md5 = 68
    SW_E_swbis_allow_rpm = 69
    SW_E_swbis_remote_shell_client = 70
    SW_E_swbis_install_volatile = 71
    SW_E_swbis_volatile_newname = 72
    SW_E_swbis_any_format = 73
    SW_E_swbis_forward_agent = 74
    SW_E_swbis_ignore_scripts = 75
    SW_E_swbis_sig_level = 76
    SW_E_swbis_enforce_all_signatures = 77
    SW_E_swbis_remove_volatile = 78
    SW_E_swbis_check_mtime = 79
    SW_E_swbis_check_duplicates = 80
    SW_E_swbis_check_owners = 81
    SW_E_swbis_replacefiles = 82
    SW_E_swbis_last_option = 83

utilnames = [
    "swask",       # SWC_U_A  (1 << 0)
    "swcopy",      # SWC_U_C  (1 << 1)
    "swinstall",   # SWC_U_I  (1 << 2)
    "swconfig",    # SWC_U_G  (1 << 3)
    "swlist",      # SWC_U_L  (1 << 4)
    "swmodify",    # SWC_U_M  (1 << 5)
    "swpackage",   # SWC_U_P  (1 << 6)
    "swremove",    # SWC_U_R  (1 << 7)
    "swverify",    # SWC_U_V  (1 << 8)
    "swsign",
    "swreg",
    "swjob",
    "swacl",
    "",  # SWC_U_X  (1 << 9)
    None
]
def i_is_value_false(s):
    if (
        s == None or
        (
            s.lower() == "f" or
            s.lower() == "false" or
            s.lower() == "no" or
            s.lower() == "n"
        )
    ):
        return 1
    return 0

def i_is_value_true(s):
    if (
        s is not None and
        (
            s.lower() == "t" or
            s.lower() == "true" or
            s.lower() == "yes" or
            s.lower() == "y"
        )
    ):
        return 1
    return 0
def get_opta(opta, nopt):
    value = opta[nopt].value
    if value is None:
        print(f"{swlib_utilname_get()}: warning: sw option {nopt} [{opta[nopt].optionName}] is null, using default [{opta[nopt].defaultValue}]")
    if not value:
        value = opta[nopt].defaultValue
    return value
def find_pow(flag):
    i = 1
    if flag <= 1:
        return 0
    while flag/2 > 1:
        flag = flag >> 1
        i += 1
    return i

def i_getEnumFromName(optionname, peop):
    eop = peop
    while eop.optionName and eop.optionName != optionname:
        eop += 1
    if eop.optionName is None:
        return -1
    return (eop - peop) // sys.getsizeof(extendedOptions)
def check_applicability(opta, nopt):
    index = 0
    cu = swlib_utilname_get()
    names = utilnames
    ap_mask = opta[nopt].app_flags
    while names and cu != names:
        names += 1
        index += 1
    if not names:
        return 1
    bit = 1 << index
    ret = not (ap_mask & bit)
    if ret:
        print("%s: option not valid: %s" % (cu, opta[nopt].optionName), file=sys.stderr)
    return ret

def combine1(result, directory, soc_spec):
    if directory[0] == '@':
        directory = directory[1:]
    if soc_spec[0] == '@':
        directory = directory[1:]
    result = "@" + soc_spec
    x = directory
    if directory[0] == ':':
        x = x[1:]
    if directory[0] == '@':
        x = x[1:]
    if (
        soc_spec[len(soc_spec)-1] != ':' and
        directory[0] != ':' and
        1
    ):
        result += ":"
    if directory[0] == ':':
        directory = directory[1:]
    if (t := strrchr(result, ':')) and t[1] == '\0':
        result += directory
    else:
        swlib_unix_dircat(result, directory)

def close_stdio():
    os.close(sys.stdout.fileno())
    os.close(sys.stderr.fileno())

def i_set_opta(optsetflag, opta, nopt, value):
    if value and any(char in swlibc.SW_TAINTED_CHARS for char in value):
        print("%s: error: shell meta-characters detected for %s option" % (swlib_utilname_get(), opta[nopt].optionName), file=sys.stderr)
        sys.exit(1)
    opta[nopt].option_set = optsetflag
    if value:
        opta[nopt].value = value
    else:
        opta[nopt].value = value

def debug_writeBooleanExtendedOptions(ofd, opta):
    eop = opta
    op = 0
    tmp = strob_open(10)
    while eop.optionName:
        if eop.is_bool:
            swlib_writef(ofd, tmp, "%s=%d\n", eop.optionName)
        eop += 1
    strob_close(tmp)

def swextopt_write_session_options(buf, opta, SWC_FLAG):
    eop = opta
    op = 0
    buf = ''
    while eop.optionName:
        if (SWC_FLAG == 0) or (SWC_FLAG & eop.app_flags):
            if eop.is_bool:
                bv = swextopt_is_value_true(get_opta(opta, op))
                if bv == 1:
                    buf += "%s=\"true\"\n" % eop.optionName
                elif bv == 0:
                    buf += "%s=\"false\"\n" % eop.optionName
            else:
                value = get_opta(opta, op)
                if value:
                    if any(char in swlibc.SW_TAINTED_CHARS for char in value):
                        print("%s: error: shell meta-characters detected for %s option" % (swlib_utilname_get(), eop.optionName), file=sys.stderr)
                        value = ""
                else:
                    value = ""
                buf += "%s=\"%s\"\n" % (eop.optionName, value)
        op += 1
        eop += 1

def swextopt_is_value_false(s):
    if i_is_value_false(s):
        return 1
    elif i_is_value_true(s):
        return 0
    else:
        print("%s: warning: extended option boolean value '%s' is improperly formatted, assuming false" % (swlib_utilname_get(), s), file=sys.stderr)
    return 1

def swextopt_is_value_true(s):
    if i_is_value_true(s):
        return 1
    elif i_is_value_false(s):
        return 0
    else:
        print("%s: warning: extended option boolean value '%s' is improperly formatted, assuming false" % (swlib_utilname_get(), s), file=sys.stderr)
    return 0

def swextopt_is_option_false(nopt, options):
    val = get_opta(options, nopt)
    return swextopt_is_value_false(val)

def swextopt_is_option_true(nopt, options):
    val = get_opta(options, nopt)
    return swextopt_is_value_true(val)

def swextopt_is_option_set(nopt, options):
    return options[nopt].option_set



def initExtendedOption():
    return 0

def getLongOptionNameFromValue(arr, val):
    p = arr
    while p.val and p.name:
        if p.val == val:
            return p.name
        p += 1
    return None

def getEnumFromName(fp_optionname, peop):
    tmp = strob_open(10)
    strob_strcpy(tmp, fp_optionname)
    optionname = strob_str(tmp)
    swlib_tr(optionname, ord('_'), ord('-'))
    ret = i_getEnumFromName(optionname, peop)
    if ret >= 0:
        return ret
    swlib_tr(optionname, ord('-'), ord('_'))
    ret = i_getEnumFromName(optionname, peop)
    strob_close(tmp)
    return ret

def setExtendedOption(optionname, value, peop, optSet, is_util_option):
    if peop:
        eop = peop
    else:
        eop = optionsArray
    while eop.optionName and strcmp(eop.optionName, optionname):
        eop += 1
    if eop.optionName:
        if eop.util_set and is_util_option == 0:
            return 0
        if optSet and eop.option_set:
            return 0
        if is_util_option:
            eop.util_set = 1
        eop.value = value
        return 0
    else:
        return -1

def swextopt_writeExtendedOptions(ofd, eop, SWC_FLAG):
    ret = 0
    tmp = strob_open(10)
    swextopt_writeExtendedOptions_strob(tmp, eop, SWC_FLAG, 0)
    ret = atomicio(os.write, ofd, strob_str(tmp), strob_strlen(tmp))
    if ret != strob_strlen(tmp):
        return -1
    strob_close(tmp)
    return ret

def swextopt_writeExtendedOptions_strob(tmp, eop, SWC_FLAG, do_shell_protect):
    while eop.optionName:
        if (SWC_FLAG == 0) or (SWC_FLAG & eop.app_flags):
            s = getExtendedOption(eop.optionName, eop, None, None, None)
            if not s:
                s = ""
            if do_shell_protect == 0:
                strob_sprintf(tmp, STROB_DO_APPEND, "%s=%s\n", eop.optionName, s)
            else:
                swlib_is_sh_tainted_string_fatal(s)
                strob_sprintf(tmp, STROB_DO_APPEND, "%s=\"%s\"\n", eop.optionName, s)
        eop += 1

def getExtendedOption(optionname, peop, pis_set, isOptSet, isUtilSet):
    if peop:
        eop = peop
    else:
        eop = optionsArray
    while eop.optionName and strcmp(eop.optionName, optionname):
        eop += 1
    if eop.optionName:
        if eop.value:
            if isOptSet:
                isOptSet = eop.option_set
            if isUtilSet:
                isUtilSet = eop.util_set
            if pis_set:
                pis_set = 1
            return eop.value
        else:
            if isOptSet:
                isOptSet = eop.option_set
            if isUtilSet:
                isUtilSet = eop.util_set
            if pis_set:
                pis_set = 0
            return eop.defaultValue
    if isOptSet:
        isOptSet = 0
    if isUtilSet:
        isUtilSet = 0
    return None

def set_opta_boolean(opta, nopt, value):
    if opta[nopt].is_bool:
        if value != None and strcasecmp(value, "t") and strcasecmp(value, "f") and strcasecmp(value,
                                                                                              EoptsConst.SW_E_TRUE) and strcasecmp(
                value, EoptsConst.SW_E_FALSE) and strcasecmp(value, "no") and strcasecmp(value, "yes") and strcasecmp(value,
                                                                                                           "n") and strcasecmp(
                value, "y") and strcmp(value, "0") and strcmp(value, "1"):
            swextopt_statusG = 1
    if value:
        if value[0] == '0':
            value = EoptsConst.SW_E_FALSE
        if value[0] == '1':
            value = EoptsConst.SW_E_TRUE
    i_set_opta(1, opta, nopt, value)

def set_opta(opta, nopt, value):
    if opta[nopt].is_bool:
        set_opta_boolean(opta, nopt, value)
    else:
        i_set_opta(1, opta, nopt, value)

def set_opta_initial(opta, nopt, value):
    i_set_opta(0, opta, nopt, value)

def swbisoption_get_opta(opta, nopt):
    value = opta[nopt].value
    if value == None:
        sys.stderr.write(
            "internal warning: swbis option " + str(nopt) + " [" + opta[nopt].optionName + "] is null\n")
    if not value:
        value = opta[nopt].defaultValue
    return value

def get_opta_isc(opta, nopt):
    value = opta[nopt].value
    if nopt != eOpts.SW_E_installed_software_catalog:
        return get_opta(opta, nopt)
    if value is None:
        print(
            f"{swlib_utilname_get()}: warning: sw option {nopt} [{opta[nopt].optionName}] is null, using default [{opta[nopt].defaultValue}]")
    if not value:
        value = opta[nopt].defaultValue
    s = value.find(Ec.FILE_URL)
    if s == -1:
        print("processing ordinary path")
        value = value.replace("//", "/")
        value = value.replace("./", "")
        value = value.replace("/./", "/")
        return value
    else:
        print(f"{swlib_utilname_get()}: warning malformed file URL: {value}")
        return value



def parse_options_file(opta, filename, util_name):
    if os.access(filename, os.R_OK) == 0 or filename == lc.SW_STDIO_FNAME:
        ret = parseDefaultsFile(util_name, filename, opta, 1)
    else:
        ret = 0
    return ret
def parseDefaultsFile(utility_name, defaults_filename, options, doPreserveOptions):
    ret = 0
    is_option_set = 0
    is_util_set = 0
    nullb = []
    buf = None
    oldvalue = None
    is_set = 0
    uxfio_fd = uxfio_open("", os.O_RDONLY, 0)
    if uxfio_fd < 0:
        return 1
    uxfio_fcntl(uxfio_fd, uc.UXFIO_F_SET_BUFACTIVE, uc.UXFIO_ON)
    uxfio_fcntl(uxfio_fd, uc.UXFIO_F_SET_BUFTYPE, uc.UXFIO_BUFTYPE_DYNAMIC_MEM)
    if defaults_filename != lc.SW_STDIO_FNAME:
        fd = os.open(defaults_filename, os.O_RDONLY, 0)
    else:
        fd = pty.STDIN_FILENO
    if fd < 0:
        sys.stderr.write(defaults_filename + " : " + strerror(errno) + "\n")
        uxfio_close(uxfio_fd)
        return 1
    if sw_yyparse(fd, uxfio_fd, "OPTION", 0, SwP.SWPARSE_FORM_MKUP_LEN):
        uxfio_close(uxfio_fd)
        if fd != pty.STDIN_FILENO:
            os.close(fd)
        return 3
    if fd != pty.STDIN_FILENO:
        os.close(fd)
    uxfio_write(uxfio_fd, nullb, 1)
    if uxfio_lseek(uxfio_fd, 0, io.SEEK_SET):
        uxfio_close(uxfio_fd)
        return 4
    if uxfio_get_dynamic_buffer(uxfio_fd, buf, None, None) < 0:
        return 5
    tmp = strob_open(10)
    line = strob_strtok(tmp, buf, "\r\n")
    while line:
        is_util_option = 0
        is_global_option = 0
        option = swheaderline_get_keyword(line)
        value = swheaderline_get_value(line, None)
        if (strstr(option, utility_name) == option) and option[len(utility_name)] == '.':
            is_util_option = 1
            option += (len(utility_name) + 1)
        elif strchr(option, '.') == None:
            is_global_option = 1
        if is_util_option or is_global_option:
#            if (oldvalue = getExtendedOption(option, options, is_set, is_option_set, is_util_set)):
            if getExtendedOption(option, options, is_set, is_option_set, is_util_set):
                if ((is_util_set == 0 and is_global_option and is_option_set == 0) or (
                        (is_util_option or is_set == 0) and is_option_set == 0)):
                    if is_set and oldvalue:
                        del oldvalue
                    if value:
                        value = strdup(value)
                    setExtendedOption(option, value, options, doPreserveOptions, is_util_option)
            else:
                sys.stderr.write(utility_name + ": option " + option + " not recognized\n")
                ret = 1
                break
        line = strob_strtok(tmp, None, "\n")
    uxfio_close(uxfio_fd)
    return ret
def initialize_options_files_list(usethis):
    if usethis:
        ret = usethis
    else:
        tmp = os.path.join(SYSTEM_DEFAULTS_FILE, SYSTEM_SWDEFAULTS_FILE)
        if os.getenv("HOME"):
            tmp += " " + os.path.join(os.getenv("HOME"), ".swbis", "swdefaults")
            tmp += " " + os.path.join(os.getenv("HOME"), ".swbis", "swbisdefaults")
        ret = tmp
    return ret

def swextopt_parse_options_files(opta, option_files, util_name, reqd, show_only):
    do_check_access = 0
    skip = 0
    ret = 0
    tmp = option_files.split()
    if option_files and len(option_files) == 0:
        return 0
    if not option_files:
        return 0
    if show_only:
        do_check_access = 1
    do_check_access = 1
    for file in tmp:
        if do_check_access and file != lc.SW_STDIO_FNAME:
            if not os.access(file, os.R_OK):
                print(f"{file}: {os.error}")
                if reqd:
                    if show_only == 0:
                        close_stdio()
                        exit(1)
                else:
                    skip = 1
            else:
                skip = 0
                if show_only:
                    print(file)
                    skip = 1
        if show_only:
            skip = 1
        if not skip:
            ret = parse_options_file(opta, file, util_name)
            if ret:
                print(f"error processing option file: {file}")
                return 1
        skip = 0
    return ret

def has_file_part(soc):
    if not soc or len(soc) == 0:
        return 0
    if ":" in soc:
        if soc[-1] == ":":
            return 0
        else:
            return 1
    return 0

def is_host(soc):
    if not soc or not len(soc):
        return 0
    if soc[0] == "@":
        soc = soc[1:]
    if (soc != "." and soc != "./") and (
            (":" in soc and soc[0] != ":") or (":" not in soc and soc[0] != "/") or soc[-1] == ":" or 0):
        return 1
    return 0

def is_fq(soc_spec):
    if soc_spec[0] == "/":
        return 1
    trailing_colon = soc_spec.rfind(":")
    if trailing_colon and soc_spec[trailing_colon + 1] == "/":
        return 1
    if trailing_colon and soc_spec[trailing_colon + 1] == "." and soc_spec[trailing_colon + 2] == "":
        return 1
    return 0

def swextopt_combine_directory(result, soc_spec, directory):
    retval = 0
    if soc_spec:
        result = soc_spec
    if (
            soc_spec is None or
            directory is None or
            len(directory) == 0 or
            directory == "-" or
            directory == ":" or
            (soc_spec[0] == '.' and directory[0] == '.') or
            (soc_spec[0] == '-') or
            is_fq(soc_spec) or
            0
    ):
        return 0
    retval = 0
    if (
            (is_host(directory) and is_host(soc_spec)) or
            (is_host(directory) == 0 and is_host(soc_spec) == 0) or
            0
    ):
        retval = -1
    elif (
            (len(soc_spec) > 1 and soc_spec[0] == ':') or
            0
    ):
        if ':' in directory:
            result = directory
            if directory[-1] != '/':
                result += "/"
            result += soc_spec[1:]
        else:
            result = directory
            result += soc_spec
        if '/' in soc_spec:
            return -1
    elif (
            len(soc_spec) >= 1 and
            soc_spec[0] == '/' and
            ':' not in directory and
            directory[0] != '/' and
            1
    ):
        result = "@"
        result += directory
        result += ":"
        result += soc_spec
    elif (
            soc_spec[0] != ':' and
            soc_spec[0] != '/' and
            ':' not in soc_spec and
            directory[0] == '/' and
            ':' not in directory and
            len(directory) > 0 and
            1
    ):
        result = soc_spec
        if len(soc_spec) > 0:
            result += ":"
        result += directory
    elif (
            not is_host(directory) and
            is_host(soc_spec) and
            has_file_part(soc_spec) == 0 and
            1
    ):
        combine1(result, directory, soc_spec)
    elif (
            is_host(directory) and
            not is_host(soc_spec) and
            1
    ):
        combine1(result, soc_spec, directory)
    elif (
            is_host(soc_spec) and
            has_file_part(soc_spec) and
            1
    ):
        pass
    else:
        retval = -1
    return retval

def swextopt_get_status():
    return swextopt_statusG


