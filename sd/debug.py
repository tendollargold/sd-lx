import inspect

from sd.swsuplib.misc.swlib import swlib_assertion_fatal, FrameRecord

SWLIB_ASSERT = None
SWBISNEEDFAIL = 1  # Should be on even for production use.
DEBUG_CPIO = False
debug_flag = False
TARUNEEDDEBUG = False
SWVARFSNEEDDEBUG = False
SWHEADERNEEDDEBUG = False
SWSWINEEDDEBUG = False

global __FILE__, __LINE__, __FUNCTION__


def FrameRecord1():
    global __FILE__, __LINE__, __FUNCTION__
    frame = inspect.currentframe()
    __FILE__ = frame.f_code.co_filename
    __LINE__ = frame.f_lineno
    __FUNCTION__ = frame
    return __FILE__, __LINE__, __FUNCTION__


def SWLIB_ALLOC_ASSERT(arg):
    FrameRecord()
    swlib_assertion_fatal(arg, "out of memory", __FILE__, __LINE__, __FUNCTION__)


def SWERROR3(class_, format_, arg1, arg2):
    FrameRecord()
    print(f"{class_}[0]: {__FILE__}:{__LINE__}, {__FUNCTION__}: {format_}" % arg1, arg2)


def SWERROR2(class_, format_, arg):
    FrameRecord()
    print(f"{class_}[0]: {__FILE__}:{__LINE__}, {__FUNCTION__}: {format_}" % arg)


def SWERROR(class_, format_):
    FrameRecord()
    print(f"{class_}[0]: {__FILE__}:{__LINE__}, {__FUNCTION__}: {format_}")


def E_DEBUG(format_):
    SWERROR("DEBUG", format_)


def E_DEBUG2(format_, arg):
    SWERROR2("DEBUG", format_, arg)


def E_DEBUG3(format_, arg, arg1):
    SWERROR3("DEBUG", format_, arg, arg1)


def SW_E_FAIL(format_):
    SWERROR("INTERNAL ERROR: ", format_)


def SW_E_FAIL2(format_, arg):
    SWERROR2("INTERNAL ERROR: ", format_, arg)


def SW_E_FAIL3(format_, arg, arg1):
    SWERROR3("INTERNAL ERROR: ", format_, arg, arg1)


def SW_E_DEBUG(format_):
    SWERROR("DEBUG: ", format_)


def SW_E_DEBUG2(format_, arg):
    SWERROR2("DEBUG: ", format_, arg)


def SW_E_DEBUG3(format_, arg, arg1):
    SWERROR3("DEBUG: ", format_, arg, arg1)


def SWHEADER_E_DEBUG(format):
    SWERROR("SWHEADER DEBUG DEBUG: ", format)


def SWHEADER_E_DEBUG2(format_, arg):
    SWERROR2("SWHEADER DEBUG: ", format_, arg)


def SWHEADER_E_DEBUG3(format_, arg, arg1):
    SWERROR3("DEBUG: ", format_, arg, arg1)

def SWSWI_E_DEBUG(format):
    SWERROR("SWHEADER DEBUG DEBUG: ", format)

def SWSWI_E_DEBUG2(format_, arg):
    SWERROR2("SWHEADER DEBUG: ", format_, arg)

def SWSWI_E_DEBUG3(format_, arg, arg1):
    SWERROR3("DEBUG: ", format_, arg, arg1)

def SWVARFS_E_DEBUG(format):
    SWERROR("SWVARFS DEBUG: ", format)


def SWVARFS_E_DEBUG2(format, arg):
    SWERROR2("SWVARFS DEBUG: ", format, arg)


def SWVARFS_E_DEBUG3(format, arg, arg1):
    SWERROR3("SWVARFS DEBUG: ", format, arg, arg1)


#
# xformat debug
#
def XFORMATNEEDDEBUG():
    return 1


def XFORMAT_E_DEBUG(format_):
    SWERROR("XFORMAT DEBUG: ", format_)


def XFORMAT_E_DEBUG2(format_, arg):
    SWERROR2("XFORMAT DEBUG: ", format_, arg)


def XFORMAT_E_DEBUG3(format_, arg, arg1):
    SWERROR3("XFORMAT DEBUG: ", format_, arg, arg1)


def XFORMAT_E_FAIL(format_):
    SWERROR("XFORMAT INTERNAL ERROR: ", format_)


def XFORMAT_E_FAIL2(format_, arg):
    SWERROR2("XFORMAT INTERNAL ERROR: ", format_, arg)


def XFORMAT_E_FAIL3(format_, arg, arg1):
    SWERROR3("XFORMAT INTERNAL ERROR: ", format_, arg, arg1)


def HLLIST_E_DEBUG(format_):
    SWERROR("HLLIST DEBUG: ", format_)


def HLLIST_E_DEBUG2(format_, arg):
    SWERROR2("HLLIST DEBUG: ", format_, arg)


def HLLIST_E_DEBUG3(format_, arg, arg1):
    SWERROR3("HLLIST DEBUG: ", format_, arg, arg1)
