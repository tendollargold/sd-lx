#!/usr/bin/python3

# swpackage.py - swpackage 

# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
cmd = os.path.split(sys.argv[0])[1]

swpackage_parser = argparse.ArgumentParser(prog='swpackage',
    usage="""%(prog)s [-p] [-v] [-V] [-C session_file] [-d directory|device]
       [-f software_file] [-s product_specification_file|directory] 
       [-S session_file] [ -x option=value] [ -X option_file] [-g]
       [software_selections] [-T target_selection]""",
        formatter_class=argparse.RawTextHelpFormatter,
        epilog='''
 Software selections are specified as:

     {product|bundle[.product]}[.subproduct][.fileset][,version] ...

 where version is

     [r=revision][,a=arch][,v=vendor][,c=category]

 The target selection is specified as:

     @ path
 
''')

# -g --gnu option added for GNU Copyleft
swpackage_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")
swpackage_parser.add_argument("-p --preview",metavar='preview',
			help="preview the session without modifying anything")
swpackage_parser.add_argument("-v --verbose",action="store_true", 
			help="write verbose output to stdout")
swpackage_parser.add_argument("-V",metavar='', 
			help="list SD layout_versions this command supports")
swpackage_parser.add_argument("-C", metavar='session_file',
            choices=['directory', 'device'],
            default='directory',
			help="save the session to session_file")
swpackage_parser.add_argument("-f", metavar='software_file',
			help="read software selections from this file")
swpackage_parser.add_argument("-s",metavar='source',
			help="read the product specification from this file")
swpackage_parser.add_argument("-d",metavar='destination', 
			help="specify the path to the target directory or tape")
swpackage_parser.add_argument("-X",metavar='option_file', 
			help="read option definitions from this file")
swpackage_parser.add_argument("-S",metavar='session_file',
			help="read options, software selections, and target \nselections from this file")
swpackage_parser.add_argument("software_selections", nargs='*',
 		  help="software selections: XServer.MAN ...")
# B.3.4 page 188 Using -T vs @ for target selection as it is easier in argparse. :)
# swpackage target selection is either a directory or 'tape', aka serial file
swpackage_parser.add_argument("-T", nargs="+",metavar="selection",
			  help="Target selections specified as a directory or 'tape' aka serial file")


swpackage_parser.parse_args()
args = swpackage_parser.parse_args()
print("DEBUG: Args in: ", cmd,".py ",  args)

# no arguments to swpackage
#ERROR:   Cannot access the specified source, "psf": No such file or
#         directory (2).

