# ahs.py - Archive header accessor.
#
#   Copyright (C) 1998, 1999 Jim Lowe
#   Copyright (C) 2023-2024 Paul Weber, Python Conversion
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# most likely can be replaced with the python tarfile module


import sys
import stat

from sd.swsuplib.misc.cstrings import strstr, strchr, strcmp, atol
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.strob import strob_open, strob_set_memlength, strob_strcpy, strob_strncpy
from sd.swsuplib.strob import strob_close, strob_str, strob_strcat, strob_sprintf
from sd.swsuplib.taru.tar import tarc
from sd.swsuplib.taru.taru import (taru_make_header, taru_free_header, taru_init_header, taru_hdr_get_filesize,
                                   new_cpio_header)
from sd.swsuplib.taru.mtar import taru_filehdr2filehdr, taru_statbuf2filehdr, taru_tar_checksum, taru_filehdr2statbuf
from sd.swsuplib.taru.otar import (taru_get_tar_filetype, taru_get_uid_by_name, taru_get_gid_by_name,
                                   taru_get_tar_user_by_uid, taru_get_tar_group_by_gid, taru_set_filetype_from_tartype)
AHSNEEDDEBUG = False
class AhsConstants:
    AHS_USERNAME_NOBODY = "nobody"
    AHS_UID_NOBODY = 0o7777777
    AHS_GROUPNAME_NOBODY = "nobody"
    AHS_GID_NOBODY = 0o7777777
    AHS_ID_NOBODY = AHS_UID_NOBODY
    s_tmp = None
    s_error_owner_store = None
    s_error_group_store = None

ahsc = AhsConstants

class AHS:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.file_hdr = None



def ahsstaticcreatefilehdr():
    file_hdr = taru_make_header()
    sys.stderr.write("AHS DEBUG: "+ "")
    temp_ref_null = None
    ahsstaticsetpaxlinkname(file_hdr, temp_ref_null)
    temp_ref_null2 = None
    ahsstaticsettarfilename(file_hdr, temp_ref_null2)
    temp_ref_null3 = None
    ahsstaticsettarusername(file_hdr, temp_ref_null3)
    temp_ref_null4 = None
    ahsstaticsettargroupname(file_hdr, temp_ref_null4)
    return file_hdr


def ahs_vfile_hdr(xhs):
    sys.stderr.write("AHS DEBUG: "+ "")
    return xhs.file_hdr

if AHSNEEDDEBUG:
    def AHS_E_DEBUG(format):
        sys.stderr.write("AHS DEBUG: "% format)

    def AHS_E_DEBUG2(format, arg):
        sys.stderr.write("AHS DEBUG: "% format+ arg)
    def AHS_E_DEBUG3(format, arg, arg1):
        sys.stderr.write("AHS DEBUG: "% format+ arg+ arg1)

S_ISVTX = 0o0

global s_tmp, s_error_owner_store, s_error_group_store

#	
#  private: // ---------------- Private Functions ---------------
#

def is_numeric_name(name):
    s = name
    if not s or s == 0:
        return 0
    while s:
        if s.isalpha():
            return 0
        s += 1
    return 1


def ahsstaticsettarname_length_i(c_sb, length):
    sys.stderr.write("AHS DEBUG: "+ "")
    if c_sb[0] is None:
        c_sb[0] = strob_open(10)
    strob_set_memlength(c_sb[0], length)



def ahsstaticsettarname_i(c_sb, name, length):
    sys.stderr.write("AHS DEBUG: "+ "")
    if name is None:
        if c_sb[0]:
            strob_close(c_sb)
        c_sb[0] = None
        return
    if c_sb[0] is None:
        c_sb[0] = strob_open(100)
    if length < 0:
        # Nul terminated of undetermined length 
        if name != c_sb.str_:
            strob_strcpy(c_sb[0], name)
    else:
        # Copy at most len bytes, NUL terminate at len+1 
        strob_set_memlength(c_sb[0], length + 1)
        if name != c_sb.str_:
            strob_strncpy(c_sb[0], name, length)
        c_sb.str_[length] = '\0'
    return


def ahsstaticgettarname_i(c_sb):
    sys.stderr.write("AHS DEBUG: "+ "")
    if c_sb[0] is None:
        ahsstaticsettarname_i(c_sb, "", -1)
    return strob_str(c_sb)


def ahstatic_strip_leading_slash_i(name):
    # Strip leading `/' from the filename.
    sys.stderr.write("AHS DEBUG: "+ "")
    p = name
    while p == '/' and p + 1:
        p += 1
    if p != name:
        name = name[p:]    
    return name

def count_newlines(store, key):
    ret = 0
    s = store

    if key and strstr(store, key) == store:
        #		
        # It's the first message,
        # return 0
        #		 
        return 0
    while n := strchr(s, '\n'):
        ret += 1
        s = n[1:]
        if key and strstr(s, key) == s:
            #			
            # Stop because the answer we want is the
            # number of messages prior to the 'key' message.
            #			 
            break
    return ret


def error_msg_control(name, id, dbname, is_new):
    global s_tmp, s_error_owner_store, s_error_group_store
    if s_tmp is None:
        s_tmp = strob_open(80)
    if s_error_owner_store is None:
        s_error_owner_store = strob_open(80)
    if s_error_group_store is None:
        s_error_group_store = strob_open(80)

    tmp = s_tmp
    if strcmp(dbname, "user") == 0:
        store = s_error_owner_store
    else:
        store = s_error_group_store

    if name:
        strob_sprintf(tmp, 0, "%s: %s name [%s] not found in system database\n"+ swlib_utilname_get(), dbname, name)
    else:
        strob_sprintf(tmp, 0, "%s: %s id [%d] not found in system database\n"+ swlib_utilname_get(), dbname, int((id)))
    if strstr(strob_str(store), strob_str(tmp)):
        #		
        #		 * not a new error mesasge
        #		 
        if is_new:
            is_new = 0
        n = count_newlines(strob_str(store), strob_str(tmp))
    else:
        #		
        #		 * new error message
        #		 
        strob_strcat(store, strob_str(tmp))
        sys.stderr.write( "%s"+ strob_str(tmp))
        if is_new:
            is_new = 1
        temp_ref_null = None
        n = count_newlines(strob_str(store), temp_ref_null)
    return n

#	
#  public: // ---------------- Public Functions ---------------
#


def ahs_copy(ahs_to, ahs_from):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    taru_filehdr2filehdr(ahs_to.file_hdr, ahs_from.file_hdr)
    # FIXME 
    return 0


def ahsstaticdeletefilehdr(file_hdr):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    temp_ref_null = None
    ahsstaticsetpaxlinkname(file_hdr, temp_ref_null)
    temp_ref_null2 = None
    ahsstaticsettarfilename(file_hdr, temp_ref_null2)
    temp_ref_null3 = None
    ahsstaticsettarusername(file_hdr, temp_ref_null3)
    temp_ref_null4 = None
    ahsstaticsettargroupname(file_hdr, temp_ref_null4)
    taru_free_header(file_hdr)


def ahsstaticsettarlinknamelength(file_hdr, length):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    ahsstaticsettarname_length_i(file_hdr.c_tar_linkname, length)


def ahsstaticsettarfilenamelength(file_hdr, length):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    ahsstaticsettarname_length_i(file_hdr.c_name, length)


def ahsstatic_strip_name_leading_slash(file_hdr):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    name = ahsstaticgettarfilename(file_hdr)
    temp_ref_name = name
    ahstatic_strip_leading_slash_i(temp_ref_name)
    name = temp_ref_name


def ahsstaticgettarlinkname(file_hdr):
    AHS_E_DEBUG("")
    return ahsstaticgettarname_i(file_hdr.c_tar_linkname)


def ahsstaticgettarfilename(file_hdr):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    return ahsstaticgettarname_i(file_hdr.c_name)


def ahsstaticgettarusername(file_hdr):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    return ahsstaticgettarname_i(file_hdr.c_username)


def ahsstaticgettargroupname(file_hdr):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    return ahsstaticgettarname_i(file_hdr.c_groupname)


def ahsstaticsetpaxlinkname(file_hdr, name):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    ahsstaticsettarname_i(file_hdr.c_tar_linkname, name, -1)


def ahsstaticsettarlinkname(file_hdr, name):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    #	
    # Deliberately store more than TARLINKNAMESIZE so the routines that censor
    # this size will see the overlength field and complain.
    # Use ahsstaticsetpaxlinkname() to set a  long link name
    # 	 
    ahsstaticsettarname_i(file_hdr.c_tar_linkname, name, tarc.TARLINKNAMESIZE)


def ahsstaticsettarfilename(file_hdr, name):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    ahsstaticsettarname_i(file_hdr.c_name, name, -1)


def ahsstaticsettarusername(file_hdr, name):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    ahsstaticsettarname_i(file_hdr.c_username, name, -1)


def ahsstaticsettargroupname(file_hdr, name):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    ahsstaticsettarname_i(file_hdr.c_groupname, name, -1)


def ahs_close(xhs):
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    file_hdr = ahs_vfile_hdr(xhs)
    ahsstaticdeletefilehdr(file_hdr)
    del xhs


def ahs_open():
    AHS_E_DEBUG2("AHS DEBUG: ", "")
    xhs = AHS()
    if xhs is None:
        return None
    xhs.file_hdr = ahsstaticcreatefilehdr()
    taru_init_header(ahs_vfile_hdr(xhs))
    return xhs

def ahs_get_tar_typeflag(xhs):
    AHS_E_DEBUG("")
    return taru_get_tar_filetype(ahs_vfile_hdr(xhs).c_mode)

def ahs_init_header(ahs):
    taru_init_header(ahs_vfile_hdr(ahs))

def ahs_set_tar_chksum():
    return

def ahs_set_mode(xhs, mode):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_mode'] = int(mode)

def ahs_set_perms(xhs, perms):
    mode = ahs_get_mode(xhs)
    mode &= ~(stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO | stat.S_ISUID | stat.S_ISGID | stat.S_ISVTX)
    mode |= perms
    ahs_set_mode(xhs, mode)

def ahs_set_filetype_from_tartype(xhs, s):
    modet = int(ahs_vfile_hdr(xhs)['c_mode'])
    taru_set_filetype_from_tartype(s, modet, None)
    ahs_vfile_hdr(xhs)['c_mode'] = modet

def ahs_set_uid(xhs, uid):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_uid'] = uid

def ahs_set_uid_by_name(xhs, username):
    x = 0
    AHS_E_DEBUG("")
    if taru_get_uid_by_name(username, x):
        AHS_E_DEBUG("")
        ahs_vfile_hdr(xhs)['c_uid'] = ahsc.AHS_UID_NOBODY
    else:
        AHS_E_DEBUG("")
        ahs_vfile_hdr(xhs)['c_uid'] = int(x)

def ahs_set_gid_by_name(xhs, groupname):
    x = None
    AHS_E_DEBUG("")
    if taru_get_gid_by_name(groupname, x):
        AHS_E_DEBUG("")
        ahs_vfile_hdr(xhs)['c_gid'] = ahsc.AHS_GID_NOBODY
    else:
        AHS_E_DEBUG("")
        ahs_vfile_hdr(xhs)['c_gid'] = x

def ahs_set_gid(xhs, gid):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_gid'] = gid

def ahs_set_filesize(xhs, filesize):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_filesize'] = int(filesize)

def ahs_set_nlink(xhs, nlink):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_nlink'] = nlink

def ahs_set_inode(xhs, ino):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_ino'] = ino

def ahs_set_mtime(xhs, mtime):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_mtime'] = mtime

def ahs_set_devmajor(xhs, dev):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_rdev_maj'] = dev

def ahs_set_devminor(xhs, dev):
    AHS_E_DEBUG("")
    ahs_vfile_hdr(xhs)['c_rdev_min'] = dev

def ahs_set_name(xhs, name):
    AHS_E_DEBUG("")
    ahsstaticsettarfilename(ahs_vfile_hdr(xhs), name)

def ahs_set_linkname(xhs, linkname):
    AHS_E_DEBUG("")
    ahsstaticsetpaxlinkname(ahs_vfile_hdr(xhs), linkname)

def ahs_set_from_statbuf(xhs, st):
    AHS_E_DEBUG("")
    taru_statbuf2filehdr(new_cpio_header(), st, None, None, None)

def ahs_set_to_statbuf(xhs, st):
    AHS_E_DEBUG("")
    taru_filehdr2statbuf(st, new_cpio_header())

def ahs_set_from_new_cpio_header(xhs, vfh):
    AHS_E_DEBUG("")
    taru_filehdr2filehdr(new_cpio_header(), new_cpio_header())

def ahs_get_new_cpio_header(xhs):
    AHS_E_DEBUG("")
    return ahs_vfile_hdr(xhs)

def ahs_get_tar_chksum(xhs, tarhdr):
    AHS_E_DEBUG("")
    return taru_tar_checksum(tarhdr)

def ahs_get_system_username(xhs, buf):
    AHS_E_DEBUG("")
    if taru_get_tar_user_by_uid(ahs_vfile_hdr(xhs).c_uid, buf):
        return None
    else:
        return buf


######
def ahs_get_system_groupname(xhs, buf):
    AHS_E_DEBUG("")
    if taru_get_tar_group_by_gid(xhs.vfile_hdr.c_gid, buf):
        return None
    else:
        return buf

def ahs_set_user_systempair(xhs, name):
    ret = 0
    id_decrement = 0
    is_new = 0
    x = 0
    AHS_E_DEBUG("")
    if not is_numeric_name(name):
        AHS_E_DEBUG("")
        ahs_set_tar_username(xhs, name)
        if taru_get_uid_by_name(name, x) < 0:
            id_decrement = error_msg_control(name, 0, "user", is_new)
            x = ahsc.AHS_UID_NOBODY - id_decrement
            if is_new:
                print("%s: warning: user [%s] not found, setting uid to %d" % (swlib_utilname_get(), name, int(x)))
            ret = 1
        ahs_set_uid(xhs, x)
    else:
        AHS_E_DEBUG("")
        ahs_set_tar_username(xhs, "")
        ahs_set_uid(xhs, atol(name))
    return ret

def ahs_set_group_systempair(xhs, name):
    ret = 0
    is_new = 0
    x = 0
    AHS_E_DEBUG("")
    if not is_numeric_name(name):
        AHS_E_DEBUG("")
        ahs_set_tar_groupname(xhs, name)
        if taru_get_gid_by_name(name, x) < 0:
            id_decrement = error_msg_control(name, 0, "group", is_new)
            x = ahsc.AHS_GID_NOBODY - id_decrement
            if is_new:
                print("%s: warning: group [%s] not found, setting gid to %d" % (swlib_utilname_get(), name, int(x)))
            ret = 1
        ahs_set_gid(xhs, x)
    else:
        AHS_E_DEBUG("")
        ahs_set_tar_groupname(xhs, "")
        ahs_set_gid(xhs, atol(name))
    return ret

def ahs_set_sys_db_g_policy(xhs, c):
    AHS_E_DEBUG("")
    xhs.file_hdrM.c_cg = c

def ahs_set_sys_db_u_policy(xhs, c):
    AHS_E_DEBUG("")
    xhs.file_hdrM.c_cu = c

def ahs_set_tar_username(xhs, name):
    AHS_E_DEBUG("")
    ahsstaticsettarusername(xhs.file_hdrM, name)

def ahs_set_tar_groupname(xhs, name):
    AHS_E_DEBUG("")
    ahsstaticsettargroupname(xhs.file_hdrM, name)

def ahs_get_tar_username(xhs):
    AHS_E_DEBUG("")
    return ahsstaticgettarusername(xhs.file_hdrM)

def ahs_get_tar_groupname(xhs):
    AHS_E_DEBUG("")
    return ahsstaticgettargroupname(xhs.file_hdrM)

def ahs_get_filesize(xhs):
    AHS_E_DEBUG("")
    return taru_hdr_get_filesize(ahs_vfile_hdr(xhs))

def ahs_get_mtime(xhs):
    AHS_E_DEBUG("")
    return ahs_vfile_hdr(xhs).c_mtime

def ahs_get_name(xhs, buf):
    name = ahsstaticgettarfilename(ahs_vfile_hdr(xhs))
    AHS_E_DEBUG("")
    if buf:
        AHS_E_DEBUG("")
        strob_strcpy(buf, name)
        return strob_str(buf)
    else:
        AHS_E_DEBUG("")
        return name

def ahs_get_linkname(xhs, buf):
    name = ahsstaticgettarlinkname(ahs_vfile_hdr(xhs))
    if len(name) > 99:
        print("Warning, Link name [%s] is too long for tar format archives, truncating linkname." % name)
    if buf:
        buf = name[:100]
        buf[99] = '\0'
        name = buf
    return name

def ahs_get_mode(xhs):
    return ahs_vfile_hdr(xhs).c_mode

def ahs_get_perms(xhs):
    mode = ahs_get_mode(xhs)
    return mode & (stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO | stat.S_ISUID | stat.S_ISGID | stat.S_ISVTX)

def ahs_get_source_filename(xhs, buf):
    return buf




