# Copyright 2005 Duke University
# Copyright (C) 2012-2016 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Written by Seth Vidal
# Modified by Paul Weber 2023 for SD-LX

"""
Command line interface sd class and related.
"""

from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

import logging
import sd

from . import output
from sd.const import swg
import sd.cli.commands
#import sd.cli.commands.swlist
#import sd.cli.commands.swremove
#import sd.cli.commands.swpackage
import sd.cli.option_parser
import sd.conf
import sd.const
import sd.errors

logger = logging.getLogger('sd')


def version_string(cmd):
    print("{} (sdlx) version {}\n".format(cmd, swg.SD_VERSION))

def version_info(cmd):
    version_string(cmd)
    print("Copyright (C) 2007,2008,2010 Jim Lowe\n"
          "Copyright (C) 2023 Paul Weber conversion to Python\n"
          "Systems Management: Distributed Software Administration\n"
          "Copyright � 1997 The Open Group\n"
          "Portions are copyright 1985-2023 Free Software Foundation, Inc.\n"
          "This software is distributed under the terms of the GNU General Public License\n"
          "and comes with NO WARRANTY to the extent permitted by law.\n"
          "See the file named COPYING for details.\n")

def GnuNotice(cmd):
    def __call__(self, cmd):
        print(cmd+" Copyright (C) 2023 Paul Weber\n"
             "This program comes with ABSOLUTELY NO WARRANTY; for details type "+cmd+" -g\n"
             "This is free software, and you are welcome to redistribute it\n"
             "under certain conditions.")


class BaseCli(sd.Base):
    """This is the base class for sd-lx cli."""

    def __init__(self, conf=None):
        conf = conf or sd.conf.Conf()
        super(BaseCli, self).__init__(conf=conf)
    #    self.output = output.Output(self, self.conf)


