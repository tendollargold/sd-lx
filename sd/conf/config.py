# sd configuration classes.
#
# Copyright (C) 2016-2017 Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

from __future__ import absolute_import
from __future__ import unicode_literals
from sd.const import SwGlobals as swg
import configparser


#logger = logging.getLogger('sd')

#
# System-wide default options are set in /etc/sw/sw.conf file. The file
# is only modifiable by root. $HOME/.sw/sw.conf 

# 

# The defaults.hosts file contains lists of hosts that are used by the
# GUI/TUI programs. This lets you use preselected choices for source and
# target systems. These lists are stored in the $HOME/.swdefaults.hosts
# or /etc/sw/defaults.hosts files.

# For each interactive command, target hosts containing roots or depots
# are specified in this file by separate lists (hosts, hosts_with_depots).
# The list of hosts are enclosed in {} braces and separated by white space
# (blank, tab and newline). For example:
# swinstall.hosts={hostA hostB hostC hostD hostE hostF}
# swcopy.hosts_with_depots={hostS}
# When you use the program, dialog boxes that let you choose a source
# system from a list will display all hosts specified in defaults.hosts or
# remembered from a previous session. Once a source is successfully
# accessed, that host is automatically added to the list in the
# defaults.hosts file and displayed in the dialog.
# If there are no hosts specified in defaults.hosts, only the local host and
# default source host appear in the lists.
# If a host system does not appear in the list, you can enter a new name
# from the GUI/TUI program.

class BaseConfig(object):
    """Base class for storing configuration definitions.

       Subclass when creating your own definitions.

    """
    pass
class MainConf(BaseConfig):
    # :api
    """Configuration option definitions for sw.conf's [main] section."""

    def ConfigSectionMap(self, section):
        Config = configparser.ConfigParser()
        Config.read(swg.DEFAULTS_FILENAME)
        options = Config.options(section)
        dict1 = {}
        for option in options:
            try:
                dict1[option] = Config.get(section, option)
                if dict1[option] == -1:
                    print("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
            return dict1

        # setup different cache and log for non-privileged users
#        if sd.util.am_i_root():
#            cachedir = g.SYSTEM_CACHEDIR
#            logdir = '/var/log/sw'
#        else:
#            try:
#                cachedir = logdir = misc.getCacheDir()
#            except (IOError, OSError) as e:
#                msg = _('Could not set cachedir: {}').format(ucd(e))
#                raise sd.exceptions.Error(msg)

#        self._config.cachedir().set(g.PRIO_DEFAULT, cachedir)
#        self._config.logdir().set(PRIO_DEFAULT, logdir)

        # track list of temporary files created
#        self.tempfiles = []

#class DepotConf(BaseConfig):
#    """Option definitions for depot INI file sections."""

#    def __init__(self, parent, section=None, parser=None):
#        mainConfig = parent._config if parent else configparser.ConfigParser()
#        super(DepotConf, self).__init__(configparser.ConfigParser(mainConfig), section, parser)
#        self._mainConfigRefHolder = mainConfig
#        if section:
#            self._config.name().set(PRIO_DEFAULT, section)

