
from tkinter import *

root = Tk()
from datetime import datetime
import argparse
import os
import sys
import getpass
import socket

cmd = os.path.split(sys.argv[0])[1]
username = getpass.getuser()
hostname = socket.gethostname()

# Add hostname to the root.title
root.title('Software Distributor - Job Browser')
root.geometry("800x400")

# Check for X or ANSI terminal
# Start Tkinter if X is available
# Start ANSI terminial if no X and no command line options
# if ANSI terminal check for terminal type support

terminal_error = (' NOTE:    The interactive UI was invoked, since no software was specified.  \n        The TERM environment variable is set to unknown. \n sd does not support this type of terminal.  You must set TERM \n to a value that is supported by sd and compatible with your \n terminal.  \n \nDo you want to proceed in setting your TERM value and running \n sd?  (yes or no) [yes]')

#
sd_menu = Menu(root)
root.config(menu=sd_menu)

def sd_search():
    my_label = Label(root, text="You selected File -> Search")

    # Find a name, revision, info, bundle
def sd_print():
    my_label = Label(root, text="You selected File -> Print")

# Create a File menu dropdown
file_menu = Menu(sd_menu)
sd_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="Search...", command=sd_search)
file_menu.add_command(label="Print...", command=sd_print)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)

# Create a View definitions
def sd_columns():
    my_label = Label(root, text="You selected View -> Columns")

def sd_filter():
    my_label = Label(root, text="You selected View -> Filter")

def sd_sort():
    my_label = Label(root, text="You selected View -> Sort")

def sd_view():
    my_label = Label(root, text="You selected View -> Change software view")

def sd_filter():
    my_label = Label(root, text="You selected View -> Change software filter")

def sd_by_name_icon():
    my_label = Label(root, text="You selected View -> By Name and Icon")

def sd_by_properties():
    my_label = Label(root, text="You selected View -> By Properties")

def sd_save_view():
    my_label = Label(root, text="You selected View -> Save View as Default")

# Create a View menu dropdown
view_menu = Menu(sd_menu)
sd_menu.add_cascade(label="View", menu=view_menu)
view_menu.add_command(label="Columns...", command=sd_columns)
view_menu.add_command(label="Filter...", command=sd_filter)
view_menu.add_command(label="Sort", command=sd_sort)
view_menu.add_separator()
view_menu.add_command(label="By Name and Icon", command=sd_by_name_icon)
view_menu.add_command(label="By Properties", command=sd_by_properties)
view_menu.add_separator()
view_menu.add_command(label="Save view as Default ...", command=sd_save_view)

#
# Create a Options definitions
#
def sd_options():
    my_label = Label(root, text="You selected Options -> Change Options")

def sd_refresh_int():
    my_label = Label(root, text="You selected Options -> Refresh Interval")

def sd_refresh():
    my_label = Label(root, text="You selected Options -> Refresh Interval")

# Create a Options menu dropdown
options_menu = Menu(sd_menu)
sd_menu.add_cascade(label="Options", menu=options_menu)
options_menu.add_command(label="Change Refresh Interval...", command=sd_refresh_int)
  # Change Refresh Interval
  # x 1 second
  # x 5 seconds
  # x 10 seconds
  # x 30 seconds
  # x Never (refresh off)
  # x Save Interval as default

options_menu.add_command(label="Refresh list", command=sd_refresh)

# Create Actions definitions
def sd_match_target():
    my_label = Label(root, text="You selected Actions -> Match What Target Has")

def sd_add_sw_group():
    my_label = Label(root, text="You selected Actions -> Add Software Group")

def sd_save_sw_group():
    my_label = Label(root, text="You selected Actions -> Save Software Group")

def sd_patch():
    my_label = Label(root, text="You selected Actions -> Manage Patch Selection")

def sd_change_source():
    my_label = Label(root, text="You selected Actions -> Change Source")

def sd_change_target():
    my_label = Label(root, text="You selected Actions -> Change Source")

def sd_install():
    my_label = Label(root, text="You selected Actions -> Install")

# Create a Actions menu dropdown
actions_menu = Menu(sd_menu)
sd_menu.add_cascade(label="Actions", menu=actions_menu)
actions_menu.add_command(label="Match What Target Has", command=sd_match_target)
actions_menu.add_command(label="Add Software Group...", command=sd_add_sw_group)
actions_menu.add_command(label="Save Software Group...", command=sd_save_sw_group)
actions_menu.add_command(label="Manage Patch Selection...", command=sd_patch)
actions_menu.add_separator()
actions_menu.add_command(label="Change Source...", command=sd_change_source)
actions_menu.add_separator()
actions_menu.add_command(label="Change Target...", command=sd_change_target)
actions_menu.add_separator()
actions_menu.add_command(label="Install...", command=sd_install)

#
# Create Actions definitions
#
def sd_help_overview():
    my_label = Label(root, text="You selected Help -> Options")

def sd_help_keyboard():
    my_label = Label(root, text="You selected Help -> Keyboard")

def sd_help_usage():
    my_label = Label(root, text="You selected Help -> Using Help")

def sd_help_prod_info():
    my_label = Label(root, text="You selected Help -> Product Information")

# Create a Help menu dropdown
help_menu = Menu(sd_menu)
sd_menu.add_cascade(label="Help", menu=help_menu)
help_menu.add_command(label="Overview...", command=sd_help_overview)
help_menu.add_command(label="Keyboard...", command=sd_help_keyboard)
help_menu.add_command(label="Usage Help...", command=sd_help_usage)
help_menu.add_separator()
help_menu.add_command(label="Product Information...", command=sd_help_prod_info)

root.mainloop()
