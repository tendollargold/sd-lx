#!/usr/bin/python3

# lxpsf.py --- A Package reading and translation tool

# Copyright (C) 1999,2007,2008,2014  James H. Lowe, Jr.
# Copyright (C) 2023 Paul Weber conversioin to Python

# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.


import argparse
import os
import sys
from sd.const import SwGlobals, LegacyConstants
from sd.swsuplib.uxfio import UxfioConst
UxFC = UxfioConst()

G = SwGlobals
LC = LegacyConstants

cmd = os.path.split(sys.argv[0])[1]

lxpsf_parser = argparse.ArgumentParser(
    usage="""%(prog)s lxpsf [options] [file]""",
    formatter_class=argparse.RawTextHelpFormatter,
    epilog=''' Read any format (RPM,DEB,slackware) package and write a tar archive\n
    to stdout that is able to be converted to a POSIX package by swpackage\n
    when installed in the file system.  GNU swpackage has a special\n
    feature to read a tar archive in memory as a file system (See below).\n
    Currently the following package formats are supported:\n
        RPM v3 (source and binary), Debian Package Format (Deb v2.0),\n
        Slackware runtime tarballs, and plain vanilla tarballs\n
''')

lxpsf_parser.add_argument("--psf-form1", metavar='',
                          help="default form, full control directories")
lxpsf_parser.add_argument("--psf-form2", metavar='',
                          help="include path name prefix in archive")
lxpsf_parser.add_argument("--psf-form3", metavar='',
                          help="no control directories, with path name prefix")
lxpsf_parser.add_argument("--slackware-pkg-name=NAME", metavar='slackwarepkg',
                          help="Tarball filename")
lxpsf_parser.add_argument("--deb-control", metavar='',
                          help="write uncompressed control.tar.gz tar file to stdout")
lxpsf_parser.add_argument("--deb-data", metavar='',
                          help="write uncompressed data.tar.gz tar file to stdout")
lxpsf_parser.add_argument("--create-time=cal_time", metavar='caltime',
                          help="Use this time for archive header times")
lxpsf_parser.add_argument("--owner=NAME", metavar='pkgowner',
                          help="owner of the package")
lxpsf_parser.add_argument("--group=NAME", metavar='pkggroup',
                          help="group ownership of the package")
lxpsf_parser.add_argument("--exclude=NAME", metavar='',
                          help="add exclude directive to the PSF")
lxpsf_parser.add_argument("--exclude-system-dirs", metavar='',
                          help="exclude all system directories listed in manual page\n hier (7) conforming to (FHS 2.2)")
lxpsf_parser.add_argument("-d", "--debug-level=TYPE", metavar='',
                          help="always use 'link'")
lxpsf_parser.add_argument("-D checkdigest", metavar='',
                          help="checkdigest script, applies to plain tarball translation")
lxpsf_parser.add_argument("-x, --use-recursive-fileset", metavar='',
                          help="Use \"file *\" instead of individual file definitions")
lxpsf_parser.add_argument("-r", "--no-leading-path", metavar='',
                          help="use \".\" as the leading archive path")
lxpsf_parser.add_argument("-B buffertype", metavar='',
                          help="mem or file  Mem is default")
lxpsf_parser.add_argument("-H format", metavar='',
                          help="archive format: ustar, pax, newc, crc, odc (pax is default)")
lxpsf_parser.add_argument("-p", "--psf-only", metavar='',
                          help="Write the PSF to stdout")
lxpsf_parser.add_argument("-o", "--info-only", metavar='',
                          help="Write the INFO file for the rpm archive")
lxpsf_parser.add_argument("-L", "--smart-leading-path", metavar='',
                          help="Use leading path for srpm and not rpm.")
lxpsf_parser.add_argument("--construct-missing-files", metavar='',
                          help="Applies to RPM translation. Construct zero length\nfiles for header entries that have no files in the archive")
lxpsf_parser.add_argument("-v", metavar='',
                          help="verbose messages about the translation")
lxpsf_parser.parse_args()
args = lxpsf_parser.parse_args()

#def usage(progname, fp):
#    fp.write("lxpsf (swbis) version " + G.SW_RELEASE + "\n")

#def show_version(fp):
#    fp.write("lxpsf (swbis) version " + G.SW_RELEASE + "\n")

def copyright_info(fp):
    fp.write("Copyright (C) 2003,2004,2005,2007,2014 Jim Lowe\n")
    fp.write("Copyright (C) 2023-2024 Paul Weber Python Conversion\n")
    fp.write("This software is distributed under the terms of the GNU General Public License\n")
    fp.write("and comes with NO WARRANTY to the extent permitted by law.\n")
    fp.write("See the file named COPYING for details.\n")

def add_system_dirs(exclude_list):
    exclude_list.append(".")
    exclude_list.append("/bin")
    exclude_list.append("/boot")
    exclude_list.append("/dev")
    exclude_list.append("/etc")
    exclude_list.append("/etc/opt")
    exclude_list.append("/etc/sgml")
    exclude_list.append("/etc/skel")
    exclude_list.append("/etc/X11")
    exclude_list.append("/home")
    exclude_list.append("/lib")
    exclude_list.append("/media")
    exclude_list.append("/mnt")
    exclude_list.append("/opt")
    exclude_list.append("/proc")
    exclude_list.append("/root")
    exclude_list.append("/sbin")
    exclude_list.append("/srv")
    exclude_list.append("/tmp")
    exclude_list.append("/usr")
    exclude_list.append("/usr/X11R6")
    exclude_list.append("/usr/X11R6/bin")
    exclude_list.append("/usr/X11R6/lib")
    exclude_list.append("/usr/X11R6/lib/X11")
    exclude_list.append("/usr/X11R6/include/X11")
    exclude_list.append("/usr/bin")
    exclude_list.append("/usr/bin/X11")
    exclude_list.append("/usr/dict")
    exclude_list.append("/usr/doc")
    exclude_list.append("/usr/etc")
    exclude_list.append("/usr/games")
    exclude_list.append("/usr/include")
    exclude_list.append("/usr/include/X11")
    exclude_list.append("/usr/include/asm")
    exclude_list.append("/usr/include/linux")
    exclude_list.append("/usr/include/g++")
    exclude_list.append("/usr/lib")
    exclude_list.append("/usr/lib/X11")
    exclude_list.append("/usr/lib/gcc-lib")
    exclude_list.append("/usr/lib/groff")
    exclude_list.append("/usr/lib/uucp")
    exclude_list.append("/usr/local")
    exclude_list.append("/usr/local/bin")
    exclude_list.append("/usr/local/doc")
    exclude_list.append("/usr/local/etc")
    exclude_list.append("/usr/local/games")
    exclude_list.append("/usr/local/lib")
    exclude_list.append("/usr/local/include")
    exclude_list.append("/usr/local/info")
    exclude_list.append("/usr/local/man")
    exclude_list.append("/usr/local/sbin")
    exclude_list.append("/usr/local/share")
    exclude_list.append("/usr/local/src")
    exclude_list.append("/usr/man")
    exclude_list.append("/usr/sbin")
    exclude_list.append("/usr/share")
    exclude_list.append("/usr/share/dict")
    exclude_list.append("/usr/share/doc")
    exclude_list.append("/usr/share/games")
    exclude_list.append("/usr/share/info")
    exclude_list.append("/usr/share/locale")
    exclude_list.append("/usr/share/man")
    exclude_list.append("/usr/share/man/man1")
    exclude_list.append("/usr/share/man/man2")
    exclude_list.append("/usr/share/man/man3")
    exclude_list.append("/usr/share/man/man4")
    exclude_list.append("/usr/share/man/man5")
    exclude_list.append("/usr/share/man/man6")
    exclude_list.append("/usr/share/man/man7")
    exclude_list.append("/usr/share/man/man8")
    exclude_list.append("/usr/share/man/man9")
    exclude_list.append("/usr/share/misc")
    exclude_list.append("/usr/share/nls")
    exclude_list.append("/usr/share/sgml")
    exclude_list.append("/usr/share/terminfo")
    exclude_list.append("/usr/share/tmac")
    exclude_list.append("/usr/share/zoneinfo")
    exclude_list.append("/usr/src")
    exclude_list.append("/usr/src/linux")
    exclude_list.append("/usr/tmp")
    exclude_list.append("/var")
    exclude_list.append("/var/adm")
    exclude_list.append("/var/backups")
    exclude_list.append("/var/cache/man/cat1")
    exclude_list.append("/var/cache/man/cat2")
    exclude_list.append("/var/cache/man/cat3")
    exclude_list.append("/var/cache/man/cat4")
    exclude_list.append("/var/cache/man/cat5")
    exclude_list.append("/var/cache/man/cat6")
    exclude_list.append("/var/cache/man/cat7")
    exclude_list.append("/var/cache/man/cat8")
    exclude_list.append("/var/cache/man/cat9")
    exclude_list.append("/var/catman/cat1")
    exclude_list.append("/var/catman/cat2")
    exclude_list.append("/var/catman/cat3")
    exclude_list.append("/var/catman/cat4")
    exclude_list.append("/var/catman/cat5")
    exclude_list.append("/var/catman/cat6")
    exclude_list.append("/var/catman/cat7")
    exclude_list.append("/var/catman/cat8")
    exclude_list.append("/var/catman/cat9")
    exclude_list.append("/var/cron")
    exclude_list.append("/var/lib")
    exclude_list.append("/var/local")
    exclude_list.append("/var/lock")
    exclude_list.append("/var/log")
    exclude_list.append("/var/opt")
    exclude_list.append("/var/mail")
    exclude_list.append("/var/msgs")
    exclude_list.append("/var/preserve")
    exclude_list.append("/var/run")
    exclude_list.append("/var/spool")
    exclude_list.append("/var/spool/at")
    exclude_list.append("/var/spool/cron")
    exclude_list.append("/var/spool/lpd")
    exclude_list.append("/var/spool/mail")
    exclude_list.append("/var/spool/mqueue")
    exclude_list.append("/var/spool/news")
    exclude_list.append("/var/spool/rwho")
    exclude_list.append("/var/spool/smail")
    exclude_list.append("/var/spool/uucp")
    exclude_list.append("/var/tmp")
    exclude_list.append("/var/yp")
