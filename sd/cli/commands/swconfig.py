#!/usr/bin/python3

# swconfig.py - swconfig - configure software

# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
import getpass
import socket
cmd = os.path.split(sys.argv[0])[1]
username = getpass.getuser()
hostname = socket.gethostname()

swconfig_parser = argparse.ArgumentParser(prog='swconfig',
			usage="""%(prog)s [-p] [-u] [-v] [-g] [-c catalog] [-C session_file]
			[-f software_file] [-J jobid][-Q date] [-S session_file]
			[-t target_file]  [-x option=value] [-X option_file]
			[software_selections] [ @ target_selections]""",
        formatter_class=argparse.RawTextHelpFormatter,
epilog=''' Software selections are specified as:
 
     product[.subproduct][.fileset][,version] ...
 
 where version is either
 
     [r=revision][,a=arch][,v=vendor][,l=location]
     instance_id
 
 Target selections are specified as:
 
     -T [host][:][path] ...
 
''')

swconfig_parser.add_argument("-p --preview", metavar='',
			 help="preview the session without modifying anything \n(i.e. exit after the analysis phase)")
swconfig_parser.add_argument("-u --unconfigure", metavar='',
			 help="unconfigure the software selections")
swconfig_parser.add_argument("-v --verbose", metavar='verbose',
			 help="write verbose output to stdout")
swconfig_parser.add_argument("-c --catalog",metavar='catalog',
		     help="use the exported catalog structure at this path \nas the source of the 'response' files")
swconfig_parser.add_argument("-C", "--session-file",metavar='session_file',
			 help="save the session to session_file")
swconfig_parser.add_argument("-f", "--software-file", metavar='software_file',
	 		 help="read software selections from this file")
# -J jobid is from HP SD-UX
swconfig_parser.add_argument("-J", "--jobid",metavar='job_id',
			  help="Executes the previously scheduled job.  This is the syntax used by the daemon to start the job.")
swconfig_parser.add_argument("-Q", metavar='MM/DD[/YYYY][,HH:MM][AM|PM]',
		   help="schedule the job for this date")
swconfig_parser.add_argument("-S",metavar='session_file',
			help="read options, software selections, and target \nselections from this file")
swconfig_parser.add_argument("-x", nargs="+", action='append', metavar='option=value',
			help="-x option=value, set the option to value")
swconfig_parser.add_argument("-X", "--option-file", metavar='option_file',
			help="read option definitions from this file")
# -g --gnu option added for GNU Copyleft
swconfig_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")


if len(sys.argv) > 1:
  print( "~ Command: " + sys.argv[0] )
  print( "~ Args: " + sys.argv[1] )
#  sw_session_begins()
else:
  print( 'ERROR:   No software selections have been specified.  You must specify\n         a list of products (or use the "\*" wildcard).\nERROR:   Cannot continue the "swconfig" task.\n        * Selection had errors.')
  sys.exit()


swconfig_parser.parse_args()
args = swconfig_parser.parse_args()
