# phases.py
# SD phase control of software installation, update, removal
#
# Copyright (C) 2012-2016 Paul Weber
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#

from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

# sw_ phase definitions, act on installed, remove, copy software and distributions

# Section 3.7 Systems Management: Distributed Software Administration
# There are three phases in the utilities. Targets may be processed in any 
# order or in parallel.
#  1. Selection phase
#     When the user specifications are resolved, including source, target, and 
#     software selections.
#  2. Analysis phase
#     When the utility attempts to discover conditions which may cause failure 
#     when operating on the selected software.
#  3. Execution phase
#     When the actual operations on the software objects take place.
# When a utility initiates a session on a target, generate an event.
# (SW_NOTE: SW_SESSION_BEGINS)
# When the session completes, generate an event.
# (SW_NOTE: SW_SESSION_ENDS)

cmd = 'swlist'
soc = ""
sd_soc = ""
# 3.7.1 Selection Phase page 70
# This section summarizes the common tasks in the selection phase. Errors and 
# warnings are listed along with the tasks.
# 3.7.1.1 Starting a Session
# On invocation, each command processes options as defined in Section 3.5.3.1 on 
# page 54.
# The command exits if the user does not have appropriate privilege. Since 
# implementation of the security scheme is unspecified within this Software 
# Administration specification, appropriate privilege is implementation defined. 
# An implementation may generate the following events at any point in the execution 
# of the command if they are applicable to that implementation:
class SelectionStart:
  def __init__(self, arg):
    blah = ""
    if blah:
       print(blah, arg)
      # 3.7.1.2 Specifying Targets
      # A target is specified using the syntax in Section 3.4.2 on page 42. Each target
      # passes the following validation checks. If any of these checks fail, the command
      # invokes the implementation defined error handling procedure.
      #   · If the target role was unable to initialize the session on the target host,
      #     generate an event.
      #     (SW_ERROR: SW_AGENT_INITIALIZATION_FAILED)

  def target_init(self):
    print("for each target, determine its type, depot, ")
    print("validate user access permission to each target per utility")
    print("validate the target exists")
    print("if target doesn't exist, then error:")
    print("if the")
    print("SW_AGENT_INITIALIZATION_FAILED,  Failed to initialize a target session. Manager info: target. Target info: reasons for error.")

  # Determine security access to each target (ACL or SOC) permission
  def acl_check(self, arg):
    #   · If administrative access is denied by the target role on the target, generate
    #     an event.
    #     (SW_ERROR: SW_ACCESS_DENIED)
    #   · Except for swinstall, swcopy, and swpackage, if the target does not exist on a
    #     host, an error is generated:
    #     (SW_ERROR: SW_SOC_DOES_NOT_EXIST)
    if cmd == "swinstall" or cmd == 'swcopy' or cmd == 'swpackage':
      if not sd_soc: # an SOC is the security ACL container.
        print("SW_SOC_CREATED, The target software_collection did not previously exist and was created. Manager info: target. Target info: none.")
    else:
       print("SW_SOC_DOES_NOT_EXIST, The requested target or source software collections does not exist. Manager info: target. Target info: reasons for error.")
    if arg.v == "99":
      print("Validating connectivity to targets")

      if arg.v == "99":
         print("test access by user to listed target(s)")
         print("test access by user to a product")
         print("SW_ERROR: SW_ACCESS_DENIED? The user has insufficient privilege to perform the requested operation. Manager info: target. Target info: information about the initiator.")


    readable = bool(True)

    if sd_soc is not readable:
        #   · If the target is corrupt, generate an event.
       print("SW_SOC_IS_CORRUPT,   The software_collection exists, but the information is corrupt. Manager info: target. Target info: reasons for error.")
"""
  If the target is the wrong type (installed_software or distribution) for the 
      target type the user specified (with a -r or -d option respectively), generate 
      an event.
      (SW_ERROR: SW_SOC_INCORRECT_TYPE)

      If both a distribution object and an installed_software object exist at the 
      location specified in the target, and neither the -d nor the -r option is 
      specified, generate an event.
      (SW_ERROR: SW_SOC_AMBIGUOUS_TYPE)

    · If the target is a serial distribution, generate an event.
      (SW_NOTE: SW_SOC_IS_SERIAL)

      If the command is swcopy or swpackage, a serial distribution is overwritten by 
      default. If the command is swremove, swmodify, or swverify, and the 
      implementation does not support these on a serial distribution, an event is 
      generated.
      (SW_ERROR: SW_SOC_INCORRECT_MEDIA_TYPE)

    · If the target is not able to open the implementation defined logfile, generate 
      an event.
      (SW_ERROR: SW_CANNOT_OPEN_LOGFILE)

    · If the operation needs to modify the target, and it is on a read-only media or 
      file system, generate an event.
      (SW_ERROR: SW_SOC_IS_READ_ONLY)

    · An implementation may generate the following events, if they are applicable to 
      that implementation, when validating a target:
      (SW_ERROR: SW_SERVICE_NOT_AVAILABLE)
      (SW_WARNING: SW_OTHER_SESSIONS_IN_PROGRESS)
      (SW_ERROR: SW_CONNECTION_LIMIT_EXCEEDED)
      (SW_NOTE: SW_SOC_IS_REMOTE)
      (SW_ERROR: SW_FILESYSTEMS_NOT_MOUNTED)
      (SW_ERROR: SW_FILESYSTEMS_MORE_MOUNTED)

  3.7.1.3 Specifying the Source
  This section only applies to swcopy and swinstall. The source contains software 
  organized in the software packaging layout. A target can be specified using the 
  syntax in Section 3.4.2 on page 42.

  If source is specified, then it is resolved in the context of the management role. 
  If source is not specified, then a default value is supplied as defined in Section 
  3.5.3.1 on page 54.

  A source must satisfy the following validation checks:
    · Verify that the source exists, and may be accessed
      (SW_ERROR: SW_SOURCE_ACCESS_ERROR).
    · If administrative access is denied by the source role for that source, 
      generate an event.
      (SW_ERROR: SW_ACCESS_DENIED)
    · Obtain information on what software is available from the source. If the 
      information cannot be retrieved, or if an problem occurs while processing it, 
       generate an event.
       (SW_ERROR: SW_SOURCE_ACCESS_ERROR)
    · If the source is a serial media, and the mediasequence_number is not 1, then 
      generate an event. (Only the first media has the catalog information on it).
      (SW_ERROR: SW_SOURCE_NOT_FIRST_MEDIA)

  3.7.1.4 Software Selections
  Software selections can be specified on the command line or in an input file 
  using the syntax in Section 3.4.1 on page 38.

  3.7.2 Analysis Phase Systems Management: Distributed Software Administration 
  page 72

  The analysis phase occurs before the execution phase, and involves executing 
  checksto determine whether or not the execution should be attempted.

  When the analysis phase begins, generate an event
   (SW_NOTE: SW_ANALYSIS_BEGINS)

  To begin the analysis phase, the management role (the host on which the 
  utility was invoked) communicates the selection information to each target in 
  the target list. 

  The target role accesses the source (for swinstall or swcopy) or target (for 
  other utilities) to get the product catalog information for the software 
  selections. The product catalog information includes control script information
  in the control_files attribute of filesets within each product.

    · An implementation may generate any of the following events, if they are 
      applicable to that implementation, when attempting the analysis or execution 
      phase.
      (SW_ERROR: SW_AGENT_INITIALIZATION_FAILED)
      (SW_ERROR: SW_ILLEGAL_STATE_TRANSITION)
      (SW_ERROR: SW_BAD_SESSION_CONTEXT)

    · If the target role cannot access the source, generate an event.
      (SW_ERROR: SW_SOURCE_ACCESS_ERROR)

    · If an implementation supports access control for particular operations for 
      particular software objects, and if access is denied for any software object, 
      generate an event.
      (SW_ERROR: SW_ACCESS_DENIED)

    · If a fileset has an error for which there is not a more specific event defined,
      generate the generic event.
      (SW_ERROR: SW_FILESET_ERROR)

    · If a fileset has a warning for which there is not a more specific event defined,
      generate the generic event.
      (SW_WARNING: SW_FILESET_WARNING)

    · An implementation may generate the following events as applicable to the error 
      handling procedures for that implementation:
      (SW_NOTE: SW_SKIPPED_PRODUCT_ERROR)
      (SW_NOTE: SW_SKIPPED_GLOBAL_ERROR)
      (SW_WARNING: SW_FILE_IS_READ_ONLY)

    · If configuring, check for compatibility of selections, and if the software is
      not   compatible, generate an event. If allow_incompatible=true generate 
      an event. See Section 3.4.1.2 on page 42.
      (SW_WARNING: SW_NOT_COMPATIBLE)

    · If allow_incompatible=false generate an event.
      (SW_ERROR: SW_NOT_COMPATIBLE)

    · If configuring and the state is corrupt or transient, generate an event.
      (SW_ERROR: SW_SELECTION_IS_CORRUPT)

    · If the state is already configured when configuring, or not configured when 
      unconfiguring, generate an event. This event is independent of whether the 
      software will be reconfigured or not, which in turn is controlled by the 
      reconfigure option.
      (SW_NOTE: SW_ALREADY_CONFIGURED)

  Check whether configuring this software will create a new configured version of a 
  fileset, and generate an event if it will. See the definition for version in the 
  Glossary. 
    · If allow_multiple_versions=true generate an event.
      (SW_WARNING: SW_NEW_MULTIPLE_VERSION)

    · If allow_multiple_versions=false generate an event.
      (SW_ERROR: SW_NEW_MULTIPLE_VERSION)

  If the dependencies are not in the configured state and have not been autoselected
  to be configured, generate an event. 
    · If enforce_dependencies=false generate an event.
      (SW_WARNING: SW_DEPENDENCY_NOT_MET)

    · If enforce_dependencies=true, generate an event.
      (SW_ERROR: SW_DEPENDENCY_NOT_MET)

  See each utility section for the analysis operations specific to each utility. When
  the analysis phase ends, generate an event.
  (SW_NOTE: SW_ANALYSIS_ENDS)
"""

class TargetValidation(cmd):
  def __init__(self):
    self.target = ""
    if cmd:
      pass
  # If any of these checks fail, the command invokes the implementation defined error 
  # handling procedure.
  def targetconnection(self):
    print("Testing target connection...connecting to target")
    # If the target role was unable to initialize the session on the target host, 
    # generate an event.
    # SW_ERROR: SW_AGENT_INITIALIZATION_FAILED
    return None

  def targetnotavailable(self):
    print("Validating target is accepting re other sessions are in progress")
    # The target role is not accepting new requests. Manager info: target.
    # Target info: reasons for error.
    # SW_ERROR: SW_SERVICE_NOT_AVAILABLE)
    return None

  def targetsessionlimit(self):
    print("Source or Target session on host is reached ")
    # The limit of source or target role sessions on this host has already been 
    # reached. Manager info: target, number of sessions. 
    # Target info: number of sessions, limit.
    # SW_ERROR: SW_CONNECTION_LIMIT_EXCEEDED

# May need this to be a class
  def targetexists(self):
    print("Validating target exists on host")
    # Except for swinstall, swcopy, and swpackage, if the target does not exist on 
    # a host, an error is generated:
    # SW_ERROR: SW_SOC_DOES_NOT_EXIST
    return None

  def targetaclcheck(self, cmd):
    # If administrative access is denied by the target role on the target, generate 
    # an event.
    # SW_ERROR: SW_ACCESS_DENIED
    print("Validating user has permission to access target"+cmd)
    return None

  def targetdirectoryexists(cmd):
    print("Validating target directory exists on host")

    # For swinstall, swcopy, and swpackage, if the target directory does not exist
    # on a host, it is created with default attributes.
    # SW_NOTE: SW_SOC_CREATED
    return None

  def targetvalidation(cmd):
    print("Validating target is not corrupt on host")
    # If the target is corrupt, generate an event.
    # SW_ERROR: SW_SOC_IS_CORRUPT
    return None

  def targettypevalidation(cmd):
    print("Validating target type selected (-a or -r) on host")
    # If the target is the wrong type (installed_software or distribution) for the
    # target type the user specified (with a -r or -d option respectively), 
    # generate an event.
    # SW_ERROR: SW_SOC_INCORRECT_TYPE

    # If both a distribution object and an installed_software object exist at the 
    # location specified in the target, and neither the -d nor the -r option is 
    # specified, generate an event.
    # SW_ERROR: SW_SOC_AMBIGUOUS_TYPE
    return None

  def targettypeserial(cmd):
    print("Validating target type is 'serial' aka a tape drive")
    # If the target is a serial distribution, generate an event.
    # SW_NOTE: SW_SOC_IS_SERIAL

    # If the command is swcopy or swpackage, a serial distribution is overwritten by 
    # default. If the command is swremove, swmodify, or swverify, and the 
    # implementation does not support these on a serial distribution, an event is 
    # generated.
    # SW_ERROR: SW_SOC_INCORRECT_MEDIA_TYPE
    return None

  def targetlogfileaccess(cmd):
    print("Validating logfile is accessible; can be written on host")
    # If the target is not able to open the implementation defined logfile, generate 
    # an event.
    # SW_ERROR: SW_CANNOT_OPEN_LOGFILE
    return None

  def targetaccessvalidation(cmd):
    print("Validating target can be modified; can be written on host")
    # If the operation needs to modify the target, and it is on a read-only media or 
    # file system, generate an event.
    #  SW_ERROR: SW_SOC_IS_READ_ONLY
    return None

  def targetsession(cmd):
    print("Validating no other sessions are in progress")
    # One cannot simultaneously execute other utilities on the host:target 
    # Validate no swremove, swcopy, swinstall sessions are running; if true generate
    # an event
    # SW_WARNING: SW_OTHER_SESSIONS_IN_PROGRESS
    return None

class TargetFilesystemCheck(cmd):
  def targetlocalfilesystems(cmd):
    print("Validating local filesystems in /etc/fstab are be mounted")
    # The operation needs to validate all local filesytems are mounted on the target
    # to ensure space control file can accurately validate filesystem space 
    # availability
    # SW_ERROR: SW_FILESYSTEMS_NOT_MOUNTED)
    return None

  def targetlocalmorefilesystems(cmd):
    print("Validating only local filesystems in /etc/fstab are mounted")
    # One or more file systems mounted are not in file system table.
    # Manager info: target, number of file systems. Target info: file system names.
    # SW_WARNING: SW_FILESYSTEMS_MORE_MOUNTED)
    return None

  def TargetSessionRemote(cmd):
    print("Validating filesystem on local target is not NFS or Remote filesystem")
    # The software_collection is on a remote file system. (Whether note or error is 
    # implementation defined).
    # Manager info: target. Target info: none.
    # SW_NOTE: SW_SOC_IS_REMOTE
    return None


class SoftwareSession:
  def __init__(self):
    blah = 1
    if blah:
      print("Blah")


# need the jobid defined, tracked
#  jobid = 1
  def sessionstart(self, cmd):
    print("Starting session for "+cmd)
    pass

  def sessionends(self, cmd):
    print("Session ends"+cmd)
    pass

  def sw_analysisphase(self, cmd):
    print("sw_analysis phase for "+cmd)
    return None

  def sw_selectionphase(self, cmd):
    print("sw_selection phase for "+cmd)
    return None

  sessionstart(cmd)
  sessionends(cmd)



# 3.7.3 Execution Phase Systems Management: Distributed Software Administration 
# page 73

# This section summarizes the common operations and events in the execution phase.

# When the execution phase begins, generate an event.
# (SW_NOTE: SW_EXECUTION_BEGINS)

# The execution phase proceeds through the steps defined for each utility. The 
# following events are common to all utilities.

#   · When the execution phase executes a script, generate an event.
#     (SW_NOTE: SW_CONTROL_SCRIPT_BEGINS)

#   · When the execution phase begins the key operation on a fileset (such as 
#     loading or removing), generate an event.
#     (SW_NOTE: SW_FILESET_BEGINS)

#   · When the execution phase begins the key operation on a file (such as loading or
#     removing), generate an event.
#     (SW_NOTE: SW_FILE_BEGINS)

#   · If at any time there is an error rebuilding any catalog files, generate an 
#     event.
#     (SW_ERROR: SW_DATABASE_UPDATE_ERROR)

#   · If a fileset has an error for which there is not a more specific event defined,
#     generate the generic event.
#     (SW_ERROR: SW_FILESET_ERROR)

#   · If a fileset has a warning for which there is not a more specific event defined,
#     generate the generic event.
#     (SW_WARNING: SW_FILESET_WARNING)

#   · For swinstall and swcopy from a distribution that spans multiple media, an 
#     implementation may generate the following events to convey needed media change 
#     information. An implementation may, but need not, provide such support for 
#     other utilities.
#     (SW_ERROR: SW_WRONG_MEDIA_SET)
#     (SW_NOTE: SW_NEED_MEDIA_CHANGE)
#     (SW_NOTE: SW_CURRENT_MEDIA)

#   · An implementation may generate the following events for software that will not 
#     be executed due to analysis results for that software.
#     (SW_NOTE: SW_SELECTION_SKIPPED_IN_ANALYSIS)
#     (SW_ERROR: SW_SELECTION_NOT_ANALYZED)

#   · The default behavior for filesets in the removed state is the same as for 
#     filesets that are nonexistent.

# Implementations that support the removed state should define extensions to the 1387.
# 2 utilities providing operations on removed filesets.

# See each utility section for the execution operations specific to each utility. 
# When the execution phase completes, generate an event.
# (SW_NOTE: SW_EXECUTION_ENDS)

# Execution Phase
# The sequential relationship between the configure operations is shown in the 
# following list. Products are ordered by prerequisite dependencies if any. Fileset 
# operations are also ordered by any prerequisites.
# Configure each product
#   1. Run configure script for the product.
#   2. Configure each fileset in the product.
#      a. Run the configure script for the fileset.
#      b. Update the result of the script (control_file). Update the state of the 
#         fileset to configured in the database for the installed_software object.

# If there is no configure script for a fileset, the state of the fileset is still
# changed as specified above.

# For unconfigure operations, the order of the products and filesets within the 
# products is the reverse of the order of products and filesets for configure. The 
# operations are:
#  Unconfigure each product:
#    1. Unconfigure each fileset in the product:
#       a. Run the unconfigure script for the fileset.
#       b. Update the result of the script. Update the state of the fileset in the 
#          product to installed in the database for the installed_software object.
#    2. Run the unconfigure script for the product.

# If there is no unconfigure script for a fileset the state of the fileset is still 
# changed as specified above.

def sw_executionphase():
  print("sw_execution phase")
  return None

