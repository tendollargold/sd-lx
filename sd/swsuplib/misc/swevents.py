# swevents.py  - Event creation and reading.
# 
#   Copyright (C) 2003-2004 James H. Lowe, Jr.  <jhlowe@acm.org>
#   Copyright (c) 2023 Paul Weber <paul@weber.net> convert to python
#   All Rights Reserved.
#
#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
import os
import re
import sys

from sd.const import LegacyConstants
from sd.debug import E_DEBUG2
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlibc, swlib_squash_trailing_vnewline
from sd.swsuplib.uxfio import uxfio_unix_safe_write
from sd.swsuplib.cplob import vplob_open, vplob_val, vplob_add, vplob_close
from sd.swsuplib.strob import strob_str, strob_sprintf, strob_open, strob_close, strob_strcpy
from sd.errors import SW_ERROR, SW_WARNING

lc = LegacyConstants
class EventsConstants:
    SWEVENT_LV1 = 1
    SWEVENT_LV2 = 2
    SWEVENT_LV3 = 3
    SWEVENT_LV4 = 4
    SWEVENT_LV5 = 5
    SWEVENT_LV6 = 6
    SWEVENT_LV = SWEVENT_LV3
    SWI_EVENT = 1
    SWEVENT_VALUE_PREVIEW = "preview"
    SWEVENT_KW_STATUS = "status"
    SWEVENT_ATT_DELIM = "="
    SWEVENT_STATUS_PFX = SWEVENT_KW_STATUS + SWEVENT_ATT_DELIM
    SWEVENT_ATT_STATUS_PREVIEW = SWEVENT_STATUS_PFX + SWEVENT_VALUE_PREVIEW
    SW_ILLEGAL_STATE_TRANSITION = 1
    SW_BAD_SESSION_CONTEXT = 2
    SW_ILLEGAL_OPTION = 3
    SW_ACCESS_DENIED = 4
    SW_MEMORY_ERROR = 5
    SW_RESOURCE_ERROR = 6
    SW_INTERNAL_ERROR = 7
    SW_IO_ERROR = 8
    SW_AGENT_INITIALIZATION_FAILED = 10
    SW_SERVICE_NOT_AVALIABLE = 11
    SW_OTHER_SESSIONS_IN_PROGRESS = 12
    SW_SESSION_BEGINS = 28
    SW_SESSION_ENDS = 29
    SW_CONNECTION_LIMIT_EXCEEDED = 30
    SW_SOC_DOES_NOT_EXIST = 31
    SW_SOC_IS_CORRUPT = 32
    SW_SOC_CREATED = 34
    SW_CONFLICTING_SESSION_IN_PROGRESS = 35
    SW_SOC_LOCK_FAILURE = 36
    SW_SOC_IS_READ_ONLY = 37
    SW_SOC_IS_REMOTE = 38
    SW_SOC_IS_SERIAL = 40
    SW_SOC_INCORRECT_TYPE = 41
    SW_CANNOT_OPEN_LOGFILE = 42
    SW_SOC_AMBIGUOUS_TYPE = 49
    SW_ANALYSIS_BEGINS = 52
    SW_ANALYSIS_ENDS = 53
    SW_EXREQUISITE_EXCLUDE = 56
    SW_CHECK_SCRIPT_EXCLUDE = 57
    SW_CONFIGURE_EXCLUDE = 58
    SW_SELECTION_IS_CORRUPT = 59
    SW_SOURCE_ACCESS_ERROR = 60
    SW_SELECTION_NOT_FOUND = 62
    SW_SELECTION_NOT_FOUND_RELATED = 63
    SW_SELECTION_NOT_FOUND_AMBIG = 64
    SW_FILESYSTEMS_NOT_MOUNTED = 65
    SW_FILESYSTEMS_MORE_MOUNTED = 66
    SW_HIGHER_REVISION_INSTALLED = 67
    SW_NEW_MULTIPLE_VERSION = 68
    SW_EXISTING_MULTIPLE_VERSION = 69
    SW_DEPENDENCY_NOT_MET = 70
    SW_NOT_COMPATIBLE = 71
    SW_CHECK_SCRIPT_WARNING = 72
    SW_CHECK_SCRIPT_ERROR = 73
    SW_DSA_OVER_LIMIT = 75
    SW_DSA_FAILED_TO_RUN = 76
    SW_SAME_REVISION_INSTALLED = 77
    SW_ALREADY_CONFIGURED = 78
    SW_SKIPPED_GLOBAL_ERROR = 80
    SW_FILE_WARNING = 84
    SW_FILE_ERROR = 85
    SW_NOT_LOCATABLE = 86
    SW_SAME_REVISION_SKIPPED = 87
    SW_EXECUTION_BEGINS = 88
    SW_EXECUTION_ENDS = 89
    SW_FILESET_ERROR = 98
    SW_PRE_SCRIPT_WARNING = 95
    SW_PRE_SCRIPT_ERROR = 96
    SW_FILESET_WARNING = 97
    SW_POST_SCRIPT_WARNING = 99
    SW_POST_SCRIPT_ERROR = 100
    SW_CONFIGURE_WARNING = 103
    SW_CONFIGURE_ERROR = 104
    SW_DATABASE_UPDATE_ERROR = 105
    SW_FILESET_BEGINS = 117
    SW_CONTROL_SCRIPT_BEGINS = 118
    SW_CONTROL_SCRIPT_NOT_FOUND = 119
    SW_SOURCE_ACCESS_BEGINS = 260 # Not Posix
    SW_SOURCE_ACCESS_ENDS = 261 # Not Posix
    SW_CONTROL_SCRIPT_ENDS = 262 # Not Posix
    SW_SOC_LOCK_CREATED = 263 # Not Posix
    SW_SOC_LOCK_REMOVED = 264 # Not Posix
    SW_SELECTION_EXECUTION_BEGINS = 265 # Not Posix
    SW_SELECTION_EXECUTION_ENDS = 266 # Not Posix
    SW_ISC_INTEGRITY_CONFIRMED = 267 # Not Posix
    SW_ISC_INTEGRITY_NOT_CONFIRMED = 268 # Not Posix
    SW_SPECIAL_MODE_BEGINS = 269 # Not Posix
    SW_SOC_INTEGRITY_CONFIRMED = 270 # Not Posix
    SW_SOC_INTEGRITY_NOT_CONFIRMED = 271 # Not Posix
    SW_ABORT_SIGNAL_RECEIVED = 272 # Not Posix
    SW_FILE_EXISTS = 273 # Not Posix
    SWI_PRODUCT_SCRIPT_ENDS = 280 # Not Posix
    SWI_FILESET_SCRIPT_ENDS = 281 # Not Posix
    SWI_CATALOG_UNPACK_BEGINS = 302 # Not Posix
    SWI_CATALOG_UNPACK_ENDS = 303 # Not Posix
    SWI_TASK_BEGINS = 304 # Not Posix
    SWI_TASK_ENDS = 305 # Not Posix
    SWI_SWICOL_ERROR = 306 # Not Posix
    SWI_CATALOG_ANALYSIS_BEGINS = 307 # Not Posix
    SWI_CATALOG_ANALYSIS_ENDS = 308 # Not Posix
    SW_TARGET_BEGINS = 309 # Not Posix
    SW_TARGET_ENDS = 310 # Not Posix
    SWI_NORMAL_EXIT = 311 # Not Posix
    SWI_SELECTION_BEGINS = 312 # Not Posix
    SWI_SELECTION_ENDS = 313 # Not Posix
    SWI_MSG = 314 # Not Posix
    SWI_ATTRIBUTE = 315 # Not Posix
    SWI_GROUP_BEGINS = 316 # Not Posix
    SWI_GROUP_ENDS = 317 # Not Posix
    SWI_TASK_CTS = 318 # Not Posix  ClearToSend
    SWI_MAIN_SCRIPT_ENDS = 319 # Not Posix

ec = EventsConstants

"""
 -swc.SW_ERROR means event is always an error 
"""

class swEvents:
    def __init__(self, code, value, verbose_threshold, msg, is_swi_event, default_status):
        self.code = code  # POSIX event code
        self.value = value  # POSIX event value
        self.verbose_threshold = verbose_threshold
        self.msg = msg  # Message text
        self.is_swi_event = is_swi_event  # May occur in a swicol_<*> task
        self.default_status = default_status  # Worst status is default


def swevent_code(value):
    eop = swevent_get_events_array()
    while eop.value != -1:
        if eop.value == value:
            return eop.code
        eop += 1
    return eop.code

def arr_get_p():
    s_arr = None
    s_arr_index = 0
    if s_arr is None:
        s_arr = vplob_open()
    s = vplob_val(s_arr, s_arr_index)
    if s is None:
        s = strob_open(80)
        vplob_add(s_arr, s)
    else:
        strob_strcpy(s, "")
    s_arr_index += 1
    return s

# Source events
def SEVENT(p_fd, p_vlv, p_code, p_arg):
    return swevent_shell_echo(p_fd, p_vlv, "source", p_code, "noused",  p_arg)

# Target events
def TEVENT(p_fd, p_vlv, p_code, p_arg):
    return swevent_shell_echo(p_fd, p_vlv, "target", p_code, "noused",  p_arg)


eventsArray = [
    swEvents("", 0, 0, "", 0, lc.SW_NOTICE),
    swEvents("SW_ILLEGAL_STATE_TRANSITION", 1,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_BAD_SESSION_CONTEXT", 2,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_ILLEGAL_OPTION", 3,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_ACCESS_DENIED", 4,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_MEMORY_ERROR", 5,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_RESOURCE_ERROR", 6,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_INTERNAL_ERROR", 7,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_IO_ERROR", 8,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_AGENT_INITIALIZATION_FAILED", 10,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SERVICE_NOT_AVALIABLE", 11,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_OTHER_SESSIONS_IN_PROGRESS", 12,  ec.SWEVENT_LV, "", 0, SW_WARNING),
    swEvents("SW_SESSION_BEGINS", 28,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SW_SESSION_ENDS", 29,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_CONNECTION_LIMIT_EXCEEDED", 30,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_DOES_NOT_EXIST", 31,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_IS_CORRUPT", 32,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_CREATED", 34,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SW_CONFLICTING_SESSION_IN_PROGRESS", 35,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_LOCK_FAILURE", 36,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_IS_READ_ONLY", 37,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_IS_REMOTE", 38,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SW_SOC_IS_SERIAL", 40,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SW_SOC_INCORRECT_TYPE", 41,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_CANNOT_OPEN_LOGFILE", 42,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_AMBIGUOUS_TYPE", 49,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SW_ANALYSIS_BEGINS", 52,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_ANALYSIS_ENDS", 53,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_EXREQUISITE_EXCLUDE", 56,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_CHECK_SCRIPT_EXCLUDE", 57,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_CONFIGURE_EXCLUDE", 58,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_SELECTION_IS_CORRUPT", 59,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_SOURCE_ACCESS_ERROR", 60,  ec.SWEVENT_LV, ": File not found.", 0, lc.SW_ERROR),
    swEvents("SW_SELECTION_NOT_FOUND", 62,  ec.SWEVENT_LV1, "", 0, lc.SW_ERROR),
    swEvents("SW_SELECTION_NOT_FOUND_RELATED", 63,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SELECTION_NOT_FOUND_AMBIG", 64,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_FILESYSTEMS_NOT_MOUNTED", 65,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_FILESYSTEMS_MORE_MOUNTED", 66,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_HIGHER_REVISION_INSTALLED", 67,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_NEW_MULTIPLE_VERSION", 68,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_EXISTING_MULTIPLE_VERSION", 69,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_DEPENDENCY_NOT_MET", 70,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_NOT_COMPATIBLE", 71,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_CHECK_SCRIPT_WARNING", 72,  ec.SWEVENT_LV, "", 1, lc.SW_WARNING),
    swEvents("SW_CHECK_SCRIPT_ERROR", 73,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_DSA_OVER_LIMIT", 75,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_DSA_FAILED_TO_RUN", 76,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_ALREADY_CONFIGURED", 78,  ec.SWEVENT_LV1, "", 1, lc.SW_NOTICE),
    swEvents("SW_SKIPPED_GLOBAL_ERROR", 80,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_FILE_WARNING", 84,  ec.SWEVENT_LV, "", 1, lc.SW_WARNING),
    swEvents("SW_FILE_ERROR", 85,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_NOT_LOCATABLE", 86,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_SAME_REVISION_SKIPPED", 87,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_EXECUTION_BEGINS", 88,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_EXECUTION_ENDS", 89,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_PRE_SCRIPT_WARNING", 95,  ec.SWEVENT_LV, "", 1, lc.SW_WARNING),
    swEvents("SW_PRE_SCRIPT_ERROR", 96,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_FILESET_WARNING", 97,  ec.SWEVENT_LV, "", 1, lc.SW_WARNING),
    swEvents("SW_FILESET_ERROR", 98,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_POST_SCRIPT_WARNING", 99,  ec.SWEVENT_LV, "", 1, lc.SW_WARNING),
    swEvents("SW_POST_SCRIPT_ERROR", 100,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_CONFIGURE_WARNING", 103,  ec.SWEVENT_LV, "", 1, lc.SW_WARNING),
    swEvents("SW_CONFIGURE_ERROR", 104,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_DATABASE_UPDATE_ERROR", 105,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_FILESET_BEGINS", 117,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_CONTROL_SCRIPT_BEGINS", 118,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_CONTROL_SCRIPT_NOT_FOUND", 119,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_SOURCE_ACCESS_BEGINS", 260,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SW_SOURCE_ACCESS_ENDS", 261,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_CONTROL_SCRIPT_ENDS", 262,  ec.SWEVENT_LV2, "", 1, lc.SW_NOTICE),
    swEvents("SW_SOC_LOCK_CREATED", 263,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_SOC_LOCK_REMOVED", 264,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_SELECTION_EXECUTION_BEGINS", 265,  ec.SWEVENT_LV2, "", 0, lc.SW_NOTICE),
    swEvents("SW_SELECTION_EXECUTION_ENDS", 266,  ec.SWEVENT_LV2, "", 0, lc.SW_ERROR),
    swEvents("SW_ISC_INTEGRITY_CONFIRMED", 267,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SW_ISC_INTEGRITY_NOT_CONFIRMED", 268,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SPECIAL_MODE_BEGINS", 269,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_INTEGRITY_CONFIRMED", 270,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_SOC_INTEGRITY_NOT_CONFIRMED", 271,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SW_ABORT_SIGNAL_RECEIVED", 272,  ec.SWEVENT_LV1, "", 0, lc.SW_ERROR),
    swEvents("SW_FILE_EXISTS", 273,  ec.SWEVENT_LV1, "", 0, lc.SW_WARNING),
    swEvents("SWI_PRODUCT_SCRIPT_ENDS", 280,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_FILESET_SCRIPT_ENDS", 281,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_CATALOG_UNPACK_BEGINS", 302,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_CATALOG_UNPACK_ENDS", 303,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SWI_TASK_BEGINS", 304,  ec.SWEVENT_LV6, "", 1, lc.SW_NOTICE),
    swEvents("SWI_TASK_ENDS", 305,  ec.SWEVENT_LV6, "", 1, lc.SW_ERROR),
    swEvents("SWI_SWICOL_ERROR", 306,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SWI_CATALOG_ANALYSIS_BEGINS", 307,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_CATALOG_ANALYSIS_ENDS", 308,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SW_TARGET_BEGINS", 309,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SW_TARGET_ENDS", 310,  ec.SWEVENT_LV, "", 1, lc.SW_ERROR),
    swEvents("SWI_NORMAL_EXIT", 311,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SWI_SELECTION_BEGINS", 312,  ec.SWEVENT_LV, "", 0, lc.SW_NOTICE),
    swEvents("SWI_SELECTION_ENDS", 313,  ec.SWEVENT_LV, "", 0, lc.SW_ERROR),
    swEvents("SWI_MSG", 314,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_ATTRIBUTE", 315,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_GROUP_BEGINS", 316,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_GROUP_ENDS", 317,  ec.SWEVENT_LV, "", 1, lc.SW_NOTICE),
    swEvents("SWI_TASK_CTS", 318,  ec.SWEVENT_LV6, "", 1, lc.SW_NOTICE),
    swEvents("SWI_MAIN_SCRIPT_ENDS", 319,  ec.SWEVENT_LV6, "", 1, lc.SW_NOTICE),
    swEvents("<_nil_>", -1, 0, "", 0, lc.SW_NOTICE)
]
s_verbose = swlibc.SWC_VERBOSE_4

g_redir1 = ""
g_redir2 = "1>&2"
g_redirnull = "1>/dev/null"



get_struct_by_message_buf = str(['\0' for _ in range(100)])


def get_struct_by_message(line, evnt):
    buf = line[:100]
    event_code = re.search(r'SW\w+', buf)
    if event_code is None:
        return None
    ws = re.search(r'[: \r\n]', event_code.group())
    if ws is None:
        return None
    else:
        buf = buf.replace(ws.group(), '')
    eop = evnt
    while eop.value != -1:
        if eop.code == event_code.group():
            return eop
        eop += 1
    return None


def get_struct_by_value(value, evnt):
    eop = evnt
    while eop['value'] != -1:
        if eop['value'] == value:
            return eop
        eop += 1
    return None


def get_event_status(line, statusp, message):
    # statusp = -1
    #	
    # Example:
    #  swcopy: SW_SESSION_ENDS on source host low08-11 at line 56: status=0
    #	 
    if re.search(": SW_", line) or re.search(": SWI_", line) or re.search(": SW_", line):
        if  re.search(ec.SWEVENT_STATUS_PFX, line):
            #
            # The event has a status
            #
            if swevent_is_error(line, statusp) < 0 and statusp == 0:
                #
                # Make sure the status is non-zero
                # for internal errors
                #
                statusp = -1
                return statusp
        else:
            #
            # No explicit status.
            # Set the default status.
            #
            if swevent_is_error(line, statusp) < 0:
                sys.stderr.write("error from swevent_is_error\n")
                statusp = -1
                return statusp

        s = line.rfind(':')
        s1 = line.find(':')
        if s1:
            s1 = line.find(':', s1 + 1)
        if  re.search("SWI_ATTRIBUTE", line) and s1 and s != s1:
            #
            # Set the message.
            # Find the message by finding the 2nd ':'
            # or (less reliably) the last ':'
            #
            s = s1
            E_DEBUG2("s=[%s]", s)
            E_DEBUG2("s1=[%s]", s1)
        if s:
            s += 1
            while s and s == ' ':
                s += 1
            message = s
        return 0
    return -1



def swevent_message(evnt, buf, value, host, loc, msg, verbose_level):
    if verbose_level >= s_verbose:
        strob_sprintf(buf, 0, "%s: %s on %s host `hostname` at line $LINENO: %s", swlib_utilname_get(), swEvents.swevent_code(evnt, value), loc, msg)
    else:
        strob_sprintf(buf, 0, "%s: %s on %s host `hostname`: %s", swlib_utilname_get(), swEvents.swevent_code(evnt, value), loc, msg)
    return strob_str(buf)


def swevent_set_verbose(v):
    s_verbose = v
    return s_verbose

def swevent_shell_echo(to_fd, verbose_level, loc, value, host, msg):

    redir = g_redirnull

    sbuf = strob_open(10)
    buf = arr_get_p()

    evnt = swevent_get_events_array()
    eop = get_struct_by_value(value, evnt)
    if eop is None:
        return "echo \"\" 1>/dev/null"

    if True or eop.is_swi_event or verbose_level >= eop.verbose_threshhold or verbose_level < 0:
        if to_fd == 2 or eop.is_swi_eventM:
            #			
            # Internal swi protocol events are always on stderr.
            #			 
            redir = g_redir2
        if to_fd == 1:
            redir = g_redir1
        if to_fd == 0:
            redir = g_redirnull
    else:
        redir = g_redirnull

    strob_sprintf(buf, 0, "echo \"")
    strob_sprintf(buf, 1, "%s", swevent_message(evnt, sbuf, value, host, loc, msg, verbose_level))
    strob_sprintf(buf, 1, "\" %s", redir)
    strob_close(sbuf)
    ret = strob_str(buf)
    return ret


def swevent_get_value(evnt, msg):
    eop = evnt
    while eop['value'] != -1:
        if len(eop['code']) and msg.find(eop['code']) != -1:
            return eop['value']
        eop += 1
    return eop['value']

def swevent_is_error(line, statusp):
    n = 0
    does_have_status = 0
    evnts = eventsArray

    if s := re.search( ec.SWEVENT_STATUS_PFX, line):
        s = s.group(0)
        n = int(line[len( ec.SWEVENT_STATUS_PFX):])
        if n == float('inf') or n == float('-inf'):
            does_have_status = 0
        else:
            does_have_status = 1
    statusp = n
    ev = get_struct_by_message(line, evnts)
    if not ev:
        print("get_struct_by_message returned null", file=sys.stderr)
        return -1
    if (
        statusp == SW_ERROR or
        (ev.default_status == SW_ERROR and not does_have_status) or
        (ev.default_status == -SW_ERROR)
    ):
        return 1
    return 0

def swevent_write_rpsh_event(fd, event_line, length):
    msg = ""
    evnt = eventsArray
    status = 0
    event_value = swevent_get_value(evnt, event_line)
    if event_value < 0:
        sys.stderr.write("swevent_write_rpsh_event: event line not found [%s]\n" % event_line)
        return -10
    this_evnt = get_struct_by_value(event_value, evnt)
    if not this_evnt:
        return -20
    if this_evnt.is_swi_event == 0:
        return 0
    print("event line=[%s]" % event_line)
    get_event_status(event_line, status, msg)
    tmp = ""
    if msg and msg.find(ec.SWEVENT_STATUS_PFX) != -1:
        if msg:
            tmp = "%d:%s" % (event_value, msg)
        else:
            tmp = "%d:" % event_value
        swlib_squash_trailing_vnewline(tmp)
        tmp += "\n"
    else:
        tmp = "%d:%d\n" % (event_value, status)
    ret = uxfio_unix_safe_write(fd, tmp, len(tmp))
    if ret < 0 or ret != len(tmp):
        sys.stderr.write("swevent_write_rpsh_event: error: write: ret=%d fd=%d %s\n" % (ret, fd, os.error))
        ret = -1
    return ret



def swevent_parse_attribute_event(line, attribute, value):
    #
    #        The line to parse looks something like this
    #
    #        swinstall: swicol: 315:machine_type=i686
    #        swinstall: swicol: 315:os_name=Linux
    #        swinstall: swicol: 315:os_release=2.4.18
    #        swinstall: swicol: 315:os_version=#1 Tue Dec 30 20:43:14 EST 2003
    #
    s = '\0'
    s1 = '\0'
    ATTRIBUTE_EVENT = "315:"

    s = line.find(ATTRIBUTE_EVENT)
    if s == -1:
        return -1
    s += len(ATTRIBUTE_EVENT)
    s1 = s
    while s1 != len(line) and line[s1] != '=':
        s1 += 1
    if s1 == s:
        return -2
    if s1 == len(line):
        return -3
    attribute[0] = line[s:s1]
    value[0] = line[s1+1:]
    return 0



def swevent_get_events_array():
    return eventsArray


def swevents_get_struct_by_message(line, evnt):
    return get_struct_by_message(line, evnt)


def swevent_s_arr_reset():
    #	 This will force reuse of exiting string objects,
    #	   however the caller needs to be sure that the reused
    #	   objects have finished being used.  Aggressive use of
    #	   this routine can unexpectedly cause a malfunction 
    s_arr_index = 0
    return s_arr_index

def swevent_s_arr_delete(s_arr):
    s = None
    i = 0
    while vplob_val(s_arr, i) is not None:
        strob_close(s)
        i += 1
    vplob_close(s_arr)
