#!/bin/sh

export CIM_PROVIDER=/opt/wbem/bin/cimprovider
export SFM_INSTALL_ROOT=/opt/sfm
export VAR_SFM_INSTALL_ROOT=/var/opt/sfm
export SFM_MOF_DIR=${SFM_INSTALL_ROOT}/schemas/mof
export SFMCLCFGDIR=/var/stm/config/tools/monitor
export SFM_REPOSITORY_PATH=/var/opt/wbem
export SFM_NS=root/cimv2
export INTEROP_NS=root/PG_InterOp
export CIM_SERVER=/opt/wbem/sbin/cimserver
export CIMMOF=/opt/wbem/bin/cimmof
export CIM_SERVER_START=
export CIM_SERVER_STOP=-s
export PROVIDER_LIB=/opt/wbem/providers/lib
export SFM_LIB=${SFM_INSTALL_ROOT}/lib
export SFM_CIM_UTIL=${SFM_INSTALL_ROOT}/bin/CIMUtil
export CIM_SERVER_STARTED=0

export INSTALL_LOG=/var/opt/sfm/log/install.log
export PATH=$PATH:/usr/bin
mkdir -p /var/opt/sfm/log > /dev/null 2>&1


################################################################################
# newconfig_replace_prep
#       $1:     Absolute pathname of file to be delivered (without any
#               alternate root prefix).
#
#  This function should only be called from a preinstall script.
#
# Use this function instead of newconfig_prep when the file is always to
# be replaced, even if not present or if it had been changed.
# This must be used for files delivered to /var and /etc because they are
# not on shared roots available to NFS Diskless clients.
#################################################################################
newconfig_replace_prep ()
{
    typeset -i cmd_result=0

    if [[ $# -ne 1 ]]
    then
        echo "ERROR:   Usage: newconfig_replace_prep path"
        return 1
    fi
    _USRDIR=$SW_LOCATION
    [[ $SW_LOCATION = "/" ]] && _USRDIR=/usr/
    _NC_PATH=$1
    _NC_WRKG=$_NC_PATH
    _NC_NEW=${_USRDIR}newconfig${_NC_PATH}

    # First make sure that the old directory is created.
    mkdir 555 bin bin ${_NC_NEW%/*} 2>>$INSTALL_LOG 1>>$INSTALL_LOG 
    # Copy the current (working) file to the /usr/newconfig directory, so when
    # newconfig_cp compares it, it will find an exact match and therefore
    # replace it.
    if [[ -f $_NC_WRKG ]]
    then
        cp -p -f  $_NC_WRKG $_NC_NEW 2>>$INSTALL_LOG 1>>$INSTALL_LOG 
    fi
    # Now do the standard newconfig prep.
    newconfig_prep $_NC_PATH
} # End of newconfig_replace_prep()


################################################################################
# newconfig_old_prep
#       $1:     Absolute pathname of file to be delivered (without any
#               alternate root prefix).
#
# This function should only be called from a preinstall script.
# 
# Use this function instead of newconfig_prep when the file is always to
# be replaced, even if not present or if it had been changed, but the
# old file should be saved in <name>.old if it had been modified.
################################################################################


newconfig_old_prep ()
{
    typeset -i cmd_result=0

    if [[ $# -ne 1 ]]
    then
        echo "ERROR:   Usage: newconfig_old_prep path"
        return 1
    fi
    _USRDIR=$SW_LOCATION
    [[ $SW_LOCATION = "/" ]] && _USRDIR=/usr/
    _NC_PATH=$1
    _NC_WRKG=$_NC_PATH
    _NC_NEW=${_USRDIR}newconfig${_NC_PATH}

    if [[ -e "$_NC_WRKG" ]]
    then
        if [[ -e "$_NC_NEW" ]]
        then
            # If file has been changed, then log message and make .old.
            cmp $_NC_WRKG $_NC_NEW 2>>$INSTALL_LOG 1>>$INSTALL_LOG
            cmd_result=$?
            if [[ $cmd_result -ne 0 ]]
            then
                echo "NOTE:    A new version of \"$_NC_WRKG\""
                echo "         has been placed on the system.  The existing version of "
                echo "         \"$_NC_WRKG\" has been renamed"
                echo "         \"$_NC_WRKG.old\""
                echo "         since it appears that it has been modified by the administrator "
                echo "         since it was delivered."
                cp -p $_NC_WRKG ${_NC_WRKG}.old 2>>$INSTALL_LOG 1>>$INSTALL_LOG 
            fi
        else
            # If we can't tell if file has changed, then just make .old.
            cp -p $_NC_WRKG ${_NC_WRKG}.old 2>>$INSTALL_LOG 1>>$INSTALL_LOG 
        fi
    fi
    newconfig_replace_prep $_NC_PATH
} # newconfig_old_prep()


###########################################################
# Prepare files which are only overwritten if not changed.  Otherwise, a message
# should be generated suggesting the administrator merge in their changes.
#
############################################################
newconfig_new_prep (){
    newconfig_prep $1
}
#
###################################################################
# newconfig_msgs_quiet
#       $1:     The return value from newconfig_cp
#
# Issue default messages to the log file based on the actions taken (or not
# taken) by the newconfig_cp function above.
# This function will not generate the message saying that a new version of
# a config file has been delivered.  These greatly clutter up our swagent.log.
###################################################################

newconfig_msgs_quiet ()
{
    if [[ $# -ne 1 ]]
    then
        echo "ERROR:   Usage: newconfig_msgs_quiet <numeric return value>"
        return 1
    fi

    if [[ $1 -ne 0 ]]
    then
        newconfig_msgs $1
    fi
    return 0
} # End of newconfig_msgs_quiet()



#############################################################
# start the cim server if it is not running. 
# the control script exits if it fails to start the cimserver  
#############################################################
ifcimserver_running()
{
    message=$( /usr/bin/ps -e | /usr/bin/grep cimserver | /usr/bin/grep -vq cimserverd 2>>$INSTALL_LOG 1>>$INSTALL_LOG )
    if [ $? -ne 0 ]
    then        
        msg NOTE "Starting cimserver."
        # If the cimserver was started by this script then we will shut it down before exiting from this script.
       	${CIM_SERVER} ${CIM_SERVER_START} 2>/dev/null 1>/dev/null 
        rv=$?
        sleep 10
        if [ $rv -ne 0 ]
	then          
          msg ERROR "Failed to start cimserver."
	  exit 1 #error	
        fi
	CIM_SERVER_STARTED=1	
    fi
    return $CIM_SERVER_STARTED
}

#########################################################
# Stop the cim server if it is running. The first argument
# weather or not cimserver was started by this script. 
# this function takes an argument which indicates if the 
# cimservr is started by caller.
#########################################################
ifstarted_stop_cimserver()
{

    if [[ ${CIM_SERVER_STARTED} -eq 1 ]]
    then
       msg NOTE "Stopping the cimserver as it was started by configure script." 
       ${CIM_SERVER} ${CIM_SERVER_STOP} 2>/dev/null 1>/dev/null
       rv=$? 
       sleep 10
       if [[ $rv -ne 0 ]]
       then
           msg ERROR "Failed to stop the cimserver though it was started by configure script."
       fi
    fi
}
########################################################
# JAGag04350:
# update aborted: ERROR: Failed to remove SFMProviderModule
# A strange problem involving SFM provider removal due
# to timing issues. Under normal scenarios,
# 'cimprovider -rm SFMProviderModule'
# works without causing any problems. Apparently, this
# defect was observed in 11.31 IC141 testing (JAGag02769).
# The fix in JAGag02769 was to remove a subset of SFM
# indication Providers from SFMProviderModule, it appears
# that timing problems in cimserver is resulting in
# recursively removing all providers in the provider module.
########################################################
remove_providers_in_module()
{
    PROVIDER_MODULE="$1"
    PROVIDER_NAME="$2"

    echo "Removing $2 provider in module $1"  2>>$INSTALL_LOG 1>>$INSTALL_LOG

    message=$( ${CIM_PROVIDER} -l -m ${PROVIDER_MODULE}  2>>$INSTALL_LOG 1>>$INSTALL_LOG )
    if [ $? -eq 0 ]
    then
        if [[ $PROVIDER_NAME != "" ]] ; then
            # Mostly for indication providers in a module
            ${CIM_PROVIDER} -r -m $PROVIDER_MODULE -p $PROVIDER_NAME 2>>$INSTALL_LOG 1>>$INSTALL_LOG 
        else

            # Remove all providers in sequence
            for i in `${CIM_PROVIDER} -l -m $PROVIDER_MODULE`
            do
                ${CIM_PROVIDER} -r -m $PROVIDER_MODULE -p $i 2>>$INSTALL_LOG 1>>$INSTALL_LOG 
            done
            ${CIM_PROVIDER} -r -m $PROVIDER_MODULE 2>>$INSTALL_LOG 1>>$INSTALL_LOG 
        fi

    fi
}


########################################################
# This function removes the provider.
#   takes 1 parameter
#    1. module name
########################################################
remove_provider(){
    MODULE="$1"
    echo "remove_provider : Removing $1 module " >> $INSTALL_LOG 2>> $INSTALL_LOG
    message=$( ${CIM_PROVIDER} -l -m ${MODULE}   2>>$INSTALL_LOG 1>>$INSTALL_LOG )
    
    if [ $? -eq 0 ]
    then        
        ProviderStatus=$(  ${CIM_PROVIDER} -l -s | grep ${MODULE} | awk '{print $2}'  2>&1 )   
        if [[ ${ProviderStatus} = "OK" ]]
        then  
            message=$( ${CIM_PROVIDER} -d -m ${MODULE} >> $INSTALL_LOG 2>>$INSTALL_LOG )
        fi

        message=$( ${CIM_PROVIDER} -r -m ${MODULE} >> $INSTALL_LOG 2>> $INSTALL_LOG )
        message2=$( ${CIM_PROVIDER} -l | awk '{print $1}' | grep ${MODULE} 2>&1 )
        rv=$?
        if [[  $rv -eq 0 ]]
        then
            msg ERROR "Failed to remove $MODULE"
   	    msg $message
#            ifstarted_stop_cimserver 
#            exit $rv
        fi
    fi

}

#########################################################
# This function remove the link for the provider library.
# This function takes two argument 
#     1. Name of the module
#     2. Name of the library (lib<libraryname>.1) 	 
#	source library should be presnet in /opt/wbem/lib
#               destination library should be present in 
#                   /opt/wbem/providers/lib  
#########################################################
unlink_provider_lib()
{
    MODULE="${1}"
    LIB="${2}"

    echo "unlinking $1 in lib $2 " >>$INSTALL_LOG

    message=$( model | sed "s/.*9000.*/PA/g" 2>&1 )
    rv=$?
    if [[ $rv -ne 0 ]]
    then
        msg ERROR "Failed to find the platform type. Unable to remove the link for ${MODULE} library."
        msg $message    
        ifstarted_stop_cimserver
        exit 1 # error
    fi

    if [[ $message = "PA" ]]
    then
        message=$(rm -f ${PROVIDER_LIB}/lib${LIB}.sl 2>&1 )
        rv=$?
    else
        message=$( rm -f ${PROVIDER_LIB}/lib${LIB}.so 2>&1 )
        rv=$?
    fi
    
    if [[  $rv -ne 0 ]]
    then
        msg ERROR "Failed to remove the link for ${MODULE} library."
        msg $message
#        ifstarted_stop_cimserver $cimserver_started
#        exit 1 # error
   fi


}

#########################################################
# This function create a link for the provider library.
# This function takes two argument 
#     1. Name of the module
#     2. Name of the library (lib<libraryname>.1) 	 
# 		source library should be presnet in /opt/wbem/lib
#               destination library should be present in 
#                   /opt/wbem/providers/lib  
#########################################################
link_provider_lib()
{
    MODULE="${1}"
    LIB="${2}"

    echo " Creating link for lib$LIB "  >>$INSTALL_LOG 2>>$INSTALL_LOG

    message=$( model | sed "s/.*9000.*/PA/g" 2>&1 )
    rv=$?
    if [[ $rv -ne 0 ]]
    then
        msg ERROR "Failed to find the platform type. Unable to create the link for ${MODULE} library."
        msg $message    
        ifstarted_stop_cimserver
        exit 1 # error
    fi

    if [[ $message = "PA" ]]
    then
        message=$( ln -s -f ${SFM_LIB}/lib${LIB}.1 ${PROVIDER_LIB}/lib${LIB}.sl 2>&1 )
        rv=$?
    else
        message=$( ln -s -f ${SFM_LIB}/lib${LIB}.1 ${PROVIDER_LIB}/lib${LIB}.so 2>&1 )
        rv=$?
    fi
    
    if [[  $rv -ne 0 ]]
    then
        msg ERROR "Failed to create the link for ${MODULE} library."
        msg $message
        ifstarted_stop_cimserver $cimserver_started
        exit 1 # error
   fi
}


#########################################################
# Delete class 
# takes 2 parameter
#    1. class name
#    2. name space     
#########################################################
delete_class()
{
    CLASS_NAME="${1}"
    NAMESPACE="${2}"
    echo "Deleting class $1 from namespace $2 " >> $INSTALL_LOG 2>>$INSTALL_LOG
    message=$(${SFM_CIM_UTIL} -d ${NAMESPACE} ${CLASS_NAME} 2>&1 )
}

#########################################################
# Delete class if there is no module using this class.
# takes 2 parameter
#    1. class name
#    2. name space     
#########################################################

safe_delete_class()
{
    CLASS_NAME="${1}"
    NAMESPACE="${2}"
    message=$(${SFM_CIM_UTIL} -u "root/PG_InterOp" ${CLASS_NAME} | wc -l 2>&1)
    if [ ${message} -eq 0 ]
    then
         delete_class ${CLASS_NAME} ${NAMESPACE}
    fi 
}

#########################################################
# Register mof 
# takes 3 parameter
#   1. description
#   2. mof file name
#   3. namespace 
#########################################################
register_mof()
{
    DESCRIPTION="$1"
    MOF_FILE_NAME="$2"
    NAMESPACE="$3"

    echo "Registering $2 with namespace $3" >>$INSTALL_LOG

    if [ $# -eq 4 ]
    then
        MOF_DIR="$4"
    else
        MOF_DIR="$SFM_MOF_DIR"
    fi

    message=$( $CIMMOF  -n $NAMESPACE  $MOF_DIR/$MOF_FILE_NAME >>$INSTALL_LOG 2>>$INSTALL_LOG )
    rv=$?
    if [[  $rv -ne 0 ]]
    then
        msg ERROR "Failed to add $DESCRIPTION to $NAMESPACE namespace."
	msg $message    
	ifstarted_stop_cimserver 
	exit 1 #error
    fi
}

##########################################################################
# Read Version
# reads version using CIMUtil and returns its return code. The version of the 
# specified class is stored in $VERSION if no version info is present in the 
# class, sets it to 0 so that mof is updated.
# takes 2 parameter
#     1. name space
#     2. class
###########################################################################
read_version()
{
	NAMESPACE="$1"
	CLASS="$2"
	VERSION=$($SFM_CIM_UTIL -v $NAMESPACE $CLASS 2>&1)
	RETVAL=$?
	if [ $RETVAL -eq 3 ]
	then
	    msg ERROR "Error reading version of $CLASS in namespace $NAMESPACE."
	    $VERSION="0"
	    exit 1 # error
	fi
	
	if [ $RETVAL -eq 1 ]
	then
	    VERSION="0.0"
	fi
	return $RETVAL
}

###############################################################
# class needs update 
# take 4 param 
#    1. namespace
#    2. class
#    3. new version of the class
#    4. Install state - state of the previous dependent class.
#  return 0 - if update is required as this  a new class
#  return 1 - if update is required as previous version id present.
#  return 2 - if no update is required as same or newer version 
#             is present
###############################################################  
class_need_update()
{
	NAMESPACE="$1"
	CLASS="$2"
	NEW_VERSION="$3"
	INSTALL_STATE=$4
	if [ $INSTALL_STATE -ne 2 ]
	then
	      # This means one of the previous classes for which we checked version needs an 
              # update, so we update all the related classes
	      	return $INSTALL_STATE
	fi
	read_version "$NAMESPACE" "$CLASS"
        RETVAL=$? 
	if [ $RETVAL -eq 2 ]
	then 
                #class not found so install it afresh
		INSTALL_STATE=0
	else
		CURRENT_VERSION="$VERSION"
	        if [ $CURRENT_VERSION -lt $NEW_VERSION ]
	        then
                       #installation is required.
		       INSTALL_STATE=1
		else 
                        #installation is not required.
			INSTALL_STATE=2
		fi
	fi
	return $INSTALL_STATE 
}

get_platform_type() 
{
	message=$( model | sed "s/.*9000.*/PA/g" 2>&1 )
    rv=$?
    if [[ $rv -ne 0 ]]
    then
        msg ERROR "Failed to find the platform type. "
        msg $message    
        ifstarted_stop_cimserver
        exit 1 # error
    fi
	if [ "$message" = "PA" ]
	then
		PLATFORM_TYPE="PA"
	else
		PLATFORM_TYPE="IA"
	fi
}

######################################################
# Get the OS version
######################################################

get_os_version()
{
    get_os_rev | grep "11 11" 1>/dev/null 2>&1
    if [ $? -eq 0 ]; then
            VERSION="11.11"
    fi

    get_os_rev | grep "11 23" 1>/dev/null 2>&1
    if [ $? -eq 0 ]; then
            VERSION="11.23"
    fi

    get_os_rev | grep "11 31" 1>/dev/null 2>&1
    if [ $? -eq 0 ]; then
            VERSION="11.31"
    fi
    return
}

#Returns whether platform is blade or not
#Returns 1 if platform is blade BL860c
#else returns 0
isServerBlade()
{
   BLADE_MODEL="ia64 hp server BL???c"
   SYSTEM_MODEL="$(model)"
    
   if [[ ${SYSTEM_MODEL} = ${BLADE_MODEL} ]]
   then
       IS_SERVER_BLADE=1 
   else 
       IS_SERVER_BLADE=0 
   fi
    
}

###########################################################
# Create the directory if it is not created during install
###########################################################

create_crontab_file()
{
   SFM_CRON_DIR="/var/spool/cron/crontabs"
   SFM_CRON_FILE="$SFM_CRON_DIR/root"

   if [[ ! -d $SFM_CRON_DIR ]]
   then
        message=$( /usr/bin/mkdir -p $SFM_CRON_DIR 1>/dev/null 2>&1 )
        rv=$?
        if [[ $rv -ne 0 ]]
        then
            msg ERROR "Cannot create crontab entry."
            msg $message
            return 1
        fi
        /usr/bin/chmod 555 $SFM_CRON_DIR 1>/dev/null 2>&1
        /usr/bin/chown bin $SFM_CRON_DIR 1>/dev/null 2>&1
        /usr/bin/chgrp bin $SFM_CRON_DIR 1>/dev/null 2>&1
   fi

   if [[ ! -f $SFM_CRON_FILE ]]
   then
        /usr/bin/touch $SFM_CRON_FILE 1>/dev/null 2>&1
        /usr/bin/chmod 400  $SFM_CRON_FILE 1>/dev/null 2>&1
        /usr/bin/chown root $SFM_CRON_FILE 1>/dev/null 2>&1
        /usr/bin/chgrp sys  $SFM_CRON_FILE 1>/dev/null 2>&1
   fi
   return 0

}
###########################################################
# This function creates an entry in crontab that runs a 
# script in sfm directory. The job is executed once in 
# 15 minutes. This function does not take any args
###########################################################
createSFMCronJob()
{
    # Run CRON Job once in 15 minutes every hour starting 5 after
	SFM_CRON_JOB="5,20,35,50   *    *    *    *    /opt/sfm/bin/restart_sfm.sh"

    SFM_CRON_FILE="/var/spool/cron/crontabs/root"

    if [[ ! -f "$SFM_CRON_FILE" ]]
    then
        create_crontab_file
        if [[ $? -ne 0 ]]
        then
           msg ERROR "SFM Cron Job cannot be created."
           msg ERROR "Monitoring functionality of SFM will"
           msg ERROR "not be restarted in case of failure."
        fi
    fi

    # Check if cron job is already present and do not create the same JOB
    grep "$SFM_CRON_JOB" $SFM_CRON_FILE 1>/dev/null 2>&1
    if [ $? -ne 0 ]
    then
        tempfile=/var/tmp/cronjoblist_withoutsfm.cron;#temp file. 
        crontab -l > $tempfile  2>>$INSTALL_LOG; #Already added cron jobs.
        echo "$SFM_CRON_JOB" >> $tempfile 2>>$INSTALL_LOG;#Add sfm cron job.
        crontab < $tempfile 2>>$INSTALL_LOG;  #add it to cron. 
        rm -f $tempfile;#remove the file.     
    fi
}

#######################################################################
# This function removes the cron entry in crontab
#######################################################################
removeCronJob()
{
    CRON_FILE="/var/spool/cron/crontabs/root"
    if [[ -f "$CRON_FILE" ]]
    then
        tempfile=/var/tmp/cronjoblist_withsfm.cron;
        tempfile_withoutsfm=/var/tmp/cronjoblist_withoutsfm.cron
        crontab -l > $tempfile 2>>$INSTALL_LOG 
        /usr/bin/cat "$tempfile" | /usr/bin/sed -e "/\/opt\/sfm\/bin\/restart_sfm.sh/d" > $tempfile_withoutsfm 2>>$INSTALL_LOG 
        crontab < $tempfile_withoutsfm 2>>$INSTALL_LOG
        rm -f $tempfile#remove the file.
        rm -f $tempfile_withoutsfm#remove the file.    
    fi

}

##safe_remove_provider accepts 
##argument - providerName.
##Then with that provider name it tries,
##to know whether it is registered or not.
##If it's still registered,then it removes
##that provider from CIMOM using the 
##remove_provider method and also 
##it unlinks the library from WBEM 
##providers library directory.
safe_remove_provider()
{
    MODULE_NAME=$1;
    message=$( ${CIM_PROVIDER} -ls | grep $MODULE_NAME | wc -l);
    if [ ${message} -eq 1 ]
    then
           remove_provider $MODULE_NAME;
           unlink_provider_lib $MODULE_NAME; 
    fi
}

##Following function is just to take care
##about residues of previous releases.
##ideally we should have had all these in 
##update scripts.But If some 
##some customer upgrades only WBEM and 
##SFM then our upgrade scripts won't be called.
##Then we could have put these in 
##preinstall or postInstall but again,
##As it's not recommended that to 
##use wbem's "cimprovider " 
##during this time,we opted put 
##these in configure scripts.
##We will remove providers and library 
##links *only if* they are still present.

##TODO .Still we need to think about mof classes.
## and libraies like libfmdproviders.1 .
## we may need to rm them.

##0603 never had ThrottlingConfig.
##WBEMToEMSConsumerModule (and HPUX_ControlProvider).
cleanup_0603residues()
{
    #Delete All the mof class.
    #Remove Providers.
    #Remove links.    
    safe_remove_provider "HPUX_ControlProviderModule";
    safe_remove_provider "ErrorMetadataProviderModule";
    safe_remove_provider "EMArchiveConsumerModule";   
    safe_remove_provider "EMEmailConsumerModule";
    safe_remove_provider "SFMProviderModule";
    safe_remove_provider "FMDModule";  
    safe_remove_provider "HPHealthStateProviderModule";
}

##We introduced 2 more providers in 0606
##apart from whatever there in 0603.
cleanup_0606residues()
{
    cleanup_0603residues;
    safe_remove_provider "ThrottlingConfigProviderModule";
    safe_remove_provider "WBEMToEMSConsumerModule";
    
}

#We haven't introduced any new providers during 0609. 
cleanup_0609residues()
{
    cleanup_0606residues; 
}

#Just for namesake.
cleanup_allresidues()
{
    cleanup_0609residues;
}
