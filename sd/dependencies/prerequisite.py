
# Prerequisite
# An object requires another to be installed and/or configured correctly before
# it can be installed or configured respectively. Prerequisites do control 
# the order of operations. 
# The specification in a software object that it must not be installed until after 
# some other software object is installed, and configured until after the other 
# software object is configured,
# The manner of honoring such a prerequisite is described in swinstall on page 92.

