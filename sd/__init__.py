# __init__.py

# The toplevel Software Distributor package.
#
# Copyright (C) 2012-2016 Red Hat, Inc.
# Copyright (C) 2023 Paul Weber <paul@weber.net>
#

from __future__ import unicode_literals
import warnings

# Unneeded python2 to python3 compatibility
import sd.pycomp

warnings.filterwarnings('once', category=DeprecationWarning, module=r'^sd\..*$')

from sd.const import swg
__version__ = swg.SD_VERSION  # :api

import sd.plugin
Plugin = sd.plugin.Plugin

import sd.base
Base = sd.base.Base # :api


