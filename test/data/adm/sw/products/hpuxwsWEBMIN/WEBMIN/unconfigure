#!/sbin/sh
########
#  Product: hpuxwsWEBMIN
#  Fileset: WEBMIN
#  SD script: unconfigure
########
#
#  (c) Copyright Hewlett-Packard Company 2000
#
########
#



SUCCESS=0; FAILURE=1; WARNING=2; EXCLUDE=3

pidfile=${SW_LOCATION}logs/miniserv.pid

UTILS="/usr/lbin/sw/control_utils"

if [[ ! -f $UTILS ]]; then
	print "ERROR:   Cannot find $UTILS"
	exit $FAILURE
fi

. $UTILS

DOCS_LINK=`dirname ${SW_LOCATION}`/hp_docs/webmin
WEBMIN_HOME=${SW_LOCATION}

function stopWebmin
{
    typeset retval=$SUCCESS

    [[ ! -d $SW_LOCATION ]] && return $retval   # nothing to stop

    PATH=$PATH:/usr/bin

    if ${SW_LOCATION}/webmin-init stop >/dev/null 2>&1 ; then
        print "NOTE:    Webmin is stopped"
        retval=$SUCCESS
    else
        print "ERROR:   Could not stop webmin"
        exit $FAILURE
    fi

    return $retval
}


function webminStatus
{
    typeset retval=0 pid

    PATH=$PATH:/usr/bin

    if [ -s $pidfile ]; then
        pid=$(head -l $pidfile)
        kill -0 $pid >/dev/null 2>&1
        if [[ $? = 0 ]]; then
            #has pid file and is running
            retval=$SUCCESS
        else
            # pid file exists, but not running
            print "NOTE:    Webmin is stopped"
            retval=$FAILURE
        fi
    else
        # no pid file and not running
        print "NOTE:    Webmin is stopped"
        retval=$FAILURE
    fi

    return $retval
}

function ix_cleanup
{
    for mod_path in `ch_rc -A -l -p IEXPRESS_WEBMIN`
    do
        module=`basename $mod_path`

        for file in `ls ${WEBMIN_HOME}$module`
        do
            if [[ -h ${WEBMIN_HOME}$module/$file ]]; then
                rm -f ${WEBMIN_HOME}$module/$file
            fi
        done

        rm -f ${WEBMIN_HOME}conf/$module/admin.acl
        rm -f ${WEBMIN_HOME}conf/$module/config
    done
}


exitval=$SUCCESS

if [ -h $DOCS_LINK ]; then
    rm -f $DOCS_LINK 2>/dev/null
    [[ $? -ne 0 ]] && print "NOTE:    Could not remove $DOCS_LINK link."
fi

if [[ $SW_SESSION_IS_DRD -ne 1 ]]; then
    webminStatus  # check if webmin is running
    if [[ $? -eq 0 ]]; then
        stopWebmin  # since it's running, stop it
        exitval=$?
    fi
fi

ix_cleanup

print "NOTE:    This uninstallation of HP-UX Webmin-based Admin may lead to "
print "         inaccessibility of HP-UX Web Server Suite documentation by "
print "         other components of the suite that may still be installed."
print "         For more information on resolving this condition, please refer "
print "         to the one of the following files (if existing):"
print "           /opt/hpws/apache/hpws_docs/.hp_docs/README"
print "           /opt/hpws/tomcat/hpws_docs/.hp_docs/README"
print "           /opt/hpws/xmltools/hpws_docs/.hp_docs/README"

exit $exitval
