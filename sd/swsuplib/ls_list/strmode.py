# sd.swsupplib.ls_list.strmode.py

# Copyright (c) 1990 The Regents of the University of California.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#	This product includes software developed by the University of
#	California, Berkeley and its contributors.
# 4. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS `AS IS' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
import stat

def strmode(mode, p):
    if stat.S_ISDIR(mode):
        p.append('d')
    elif stat.S_ISCHR(mode):
        p.append('c')
    elif stat.S_ISBLK(mode):
        p.append('b')
    elif stat.S_ISREG(mode):
        p.append('-')
    elif stat.S_ISLNK(mode):
        p.append('l')
    elif stat.S_ISSOCK(mode):
        p.append('s')
    else:
        p.append('?')

    if mode & stat.S_IRUSR:
        p.append('r')
    else:
        p.append('-')
    if mode & stat.S_IWUSR:
        p.append('w')
    else:
        p.append('-')
    if mode & (stat.S_IXUSR | stat.S_ISUID):
        if mode & stat.S_ISUID:
            p.append('S')
        else:
            p.append('x')
    else:
        p.append('-')

    if mode & stat.S_IRGRP:
        p.append('r')
    else:
        p.append('-')
    if mode & stat.S_IWGRP:
        p.append('w')
    else:
        p.append('-')
    if mode & (stat.S_IXGRP | stat.S_ISGID):
        if mode & stat.S_ISGID:
            p.append('S')
        else:
            p.append('x')
    else:
        p.append('-')

    if mode & stat.S_IROTH:
        p.append('r')
    else:
        p.append('-')
    if mode & stat.S_IWOTH:
        p.append('w')
    else:
        p.append('-')
    if mode & (stat.S_IXOTH | stat.S_ISVTX):
        if mode & stat.S_ISVTX:
            p.append('T')
        else:
            p.append('x')
    else:
        p.append('-')

    p.append(' ')
    p.append('\0')