# shcmd.py  --  Shell-less Command pipeline object.
#  Copyright (C) 2001-2004 Jim Lowe
#  Copyright (C) 2023 Paul Weber convert to Python

#  All Rights Reserved.
#
#  COPYING TERMS AND CONDITIONS
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

import os
import sys
import stat
import signal
import errno
import time
import ctypes

from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import fr, swlib_pipe_pump
from sd.swsuplib.misc.swgp import swgp_close_all_fd
from sd.swsuplib.cplob import cplob_open
from sd.swsuplib.strob import STROB_DO_APPEND, strob_open, strob_sprintf, strob_close, strob_strlen, strob_str
from sd.swsuplib.uxfio import uxfio_write


SHCMD_TAINTED_CHARS = "'\"|*?;&<>`$"
SHCMD_UNSET_EXITVAL = 1000
SHCMD_INTERNAL_FILTER = "/."  # special never-used name, to mean run internal function
SHC_DIR_DOWN = 0
SHC_DIR_UP = 1

def shcmd_open():
    return shcmd_common_open()


def shcmd_common_open():
    mode = 0
    shcmd = SHCMD()

    if shcmd is None:
        return None

    shcmd.argvector_ = cplob_open(10)
    shcmd.argv_set_ = 0
    shcmd.argv_ = None
    shcmd.argstring_ = None
    shcmd.cmdstring_ = None
    shcmd.closefd_ = -1
    shcmd.pid_ = 0
    shcmd.status_ = 0
    shcmd.srcfd_ = sys.stdin.fileno()
    shcmd.dstfd_ = sys.stdout.fileno()
    shcmd.errfd_ = sys.stderr.fileno()
    shcmd.dstfile_ = None
    shcmd.srcfile_ = None
    shcmd.errfile_ = None
    shcmd.append1_ = 0
    shcmd.append2_ = 0
    shcmd.async_ = 0
    shcmd.close_all_fd_ = -1
    shcmd.child_gone_ = 0
    shcmd.proc_error_ = 0
    shcmd.esrch_ = 0
    shcmd.envp_ = os.environ.copy()
    mode = os.umask(mode)
    os.umask(mode)
    shcmd.umask_ = mode
    shcmd.user_ = '' * 64  # empty string
    shcmd.group_ = '' * 64  # empty string
    shcmd.f_filter_ = None
    shcmd.f_filter_ctrl_ = None
    shcmd.f_exec_ = shcmd_unix_execve
    for i in range(10):
        shcmd.close_list_[i] = -1

    return shcmd

class SHCMD:
    def __init__(self):
        self.cmdstring_ = None
        self.argstring_ = None
        self.argvector_ = cplob_open(10)
        self.argv_ = None
        self.argv_set_ = 0
        self.srcfd_ = sys.stdin.fileno()
        self.closefd_ = -1
        self.dstfd_ = sys.stdout.fileno()
        self.errfd_ = sys.stderr.fileno()
        self.dstfile_ = None
        self.srcfile_ = None
        self.errfile_ = None
        self.append1_ = 0
        self.append2_ = 0
        self.async_ = 0
        self.pid_ = 0
        self.status_ = 0
        self.envp_ = os.environ
        self.user_ = str(['\0' for _ in range(64)])
        self.group_ = str(['\0' for _ in range(64)])
        self.umask_ = os.umask(0)
        self.close_list_ = [0 for _ in range(10)]
        self.f_exec_ = shcmd_unix_execve
        self.f_filter_ = None
        self.f_filter_ctrl_ = None
        self.close_all_fd_ = -1
        self.child_gone_ = 0
        self.esrch_ = 0
        self.proc_error_ = 0
        self.CLOSE_LIST_LEN = len(self.close_list_) // ctypes.sizeof(ctypes.c_int)


def shcmd_getgid(shcmd):
    import grp
    if len(shcmd.group_):
        try:
            pw = grp.getgrnam(shcmd.group_)
            return pw.gr_gid
        except KeyError:
            return -2
    else:
        return -1

def close_list(shcmd):
    i = 0
    while i < shcmd.CLOSE_LIST_LEN:
        fd = shcmd.close_list_[i]
        if fd >= 0:
            if os.close(fd):
                sys.stderr.write("shcmd : close_list_: fd=%d : %s\n" % (fd, os.error))
        i += 1

def shcmd_getuid(shcmd):
    import pwd
    if len(shcmd.user_):
        try:
            pw = pwd.getpwnam(shcmd.user_)
            return pw.pw_uid
        except KeyError:
            return -2
    else:
        return -1

def cmd_invoke(shcmd):
    uid = shcmd.shcmd_getuid()
    gid = shcmd.shcmd_getgid()
    child_pid = os.fork()
    child_exitval = 0
    if child_pid < 0:
        sys.stderr.write("Can't create new process.\n")
        return -1
    elif child_pid == 0:
        if shcmd.closefd_ != -1:
            os.close(shcmd.closefd_)
        shcmd.close_list()
        if gid >= 0:
            try:
                os.setgid(gid)
            except OSError:
                sys.stderr.write("setgid failed in shcmd_cmd_invoke, group={} {}\n".format(shcmd.group_, os.error))
        elif gid == -2:
            sys.stderr.write("shcmd_getgid failed: gid not found for group={}\n".format(shcmd.group_))
        if uid >= 0:
            try:
                os.setuid(uid)
            except OSError:
                sys.stderr.write("setuid failed in shcmd_cmd_invoke, user={} {}\n".format(shcmd.user_, os.error))
        elif uid == -2:
            sys.stderr.write("shcmd_getuid failed: uid not found for user={}\n".format(shcmd.user_))
            internal_redirect(shcmd.srcfd_, shcmd.srcfile_, shcmd.dstfd_, shcmd.dstfile_,
                              shcmd.append1_, shcmd.errfd_, shcmd.errfile_, shcmd.append2_)
        os.umask(shcmd.umask_)
        if shcmd.argv_ and shcmd.argv_[0] and shcmd.argv_[0] == SHCMD_INTERNAL_FILTER:
            if shcmd.f_filter_:
                ret = shcmd.f_filter_(sys.stdout.fileno(), sys.stdin.fileno(), shcmd.f_filter_ctrl_)
            else:
                ret = swlib_pipe_pump(sys.stdout.fileno(), sys.stdin.fileno())
            child_exitval = 1 if ret < 0 else 0
        elif shcmd.argv_ and shcmd.argv_[0] and len(shcmd.argv_[0]):
            try:
                shcmd.f_exec_()
            except OSError:
                sys.stderr.write("{}: unable to execute {}\n".format(swlib_utilname_get(), shcmd.argv_[0]))
                child_exitval = 126
        else:
            sys.stderr.write("shcmd.c: illegal or null command name\n")
            child_exitval = 1
        sys.exit(child_exitval)
    else:
        if shcmd.srcfd_ > 0:
            os.close(shcmd.srcfd_)
        if shcmd.dstfd_ > 1:
            os.close(shcmd.dstfd_)
        shcmd.pid_ = child_pid
        return child_pid

def command_recurse(shcmd, cmd_vector, wpid, makepipe, pipefd):
    while True:
        if cmd_vector[1] is not None:
            shcmd.command_recurse(cmd_vector[1:], wpid, 1, [cmd_vector[0].dstfd_])

        if makepipe:
            pfd = os.pipe()
            cmd_vector[0].srcfd_ = pfd[0]
            cmd_vector[0].closefd_ = pfd[1]

        pid = shcmd.cmd_invoke()

        if wpid[0] == 0:
            wpid[0] = pid

        return wpid[0]


def shcmd_add_close_fd(shcmd, ifd):
    i = 0
    while i < len(shcmd.close_list_):
        fd = shcmd.close_list_[i]
        if fd >= 0:
            try:
                os.close(fd)
            except OSError:
                sys.stderr.write("shcmd : close_list_: fd={} : {}\n".format(fd, os.error(errno)))
        i += 1
    shcmd.close_list_[i] = ifd

def shcmd_apply_redirection(shcmd):
    internal_redirect(shcmd.srcfd_, shcmd.srcfile_, shcmd.dstfd_, shcmd.dstfile_,
                           shcmd.append1_, shcmd.errfd_, shcmd.errfile_, shcmd.append2_)
    return 0

def shcmd_reap_child(shcmd, flag):
    import os
    import errno
    if shcmd.status_ or shcmd.child_gone_:
        shcmd.child_gone_ = 1
        return 0
    pid, status = os.waitpid(shcmd.pid_, flag)
    if pid < 0:
        if errno == errno.ECHILD:
            shcmd.child_gone_ = 1
        shcmd.proc_error_ = 1
        sys.stderr.write("shcmd: waitpid return error (pid={}) for {}: {}\n".format(shcmd.pid_,
                                                                                    shcmd.argv_[0], os.error(errno)))
    elif pid > 0:
        sys.stderr.write("Normal stop: pid={}: {}\n".format(shcmd.pid_, shcmd.argv_[0]))
        sys.stderr.write("exit value for {} is {}\n".format(shcmd.argv_[0], shcmd.shcmd_get_exitval()))
        shcmd.child_gone_ = 1
    return pid

def shcmd_cmdvec_wait(shcmd, cmd_vec):
    return shcmd.shcmd_wait(cmd_vec)

def shcmd_cmdvec_wait2(shcmd, cmd_vec):
    ncmds = len(cmd_vec)
    ncmds_exited = 0
    highest_index_with_error = 0
    while ncmds_exited < ncmds:
        for i in range(ncmds - 1, -1, -1):
            #   ret = shcmd.shcmd_reap_child(cmd_vec[i], os.WNOHANG)
            ret = shcmd.shcmd_reap_child(cmd_vec[i])
            if ret > 0:
                ncmds_exited += 1
            elif ret == 0:
                if cmd_vec[i].child_gone_ == 0:
                    pass
            else:
                if errno == errno.ECHILD:
                    cmd_vec[i].proc_error_ = 1
                    ncmds_exited += 1
                else:
                    sys.stderr.write("shcmd: waitpid return error (line={}) for [{}]: "
                                     "{}\n".format(fr.__LINE__, cmd_vec[i].pid_, cmd_vec[i].argv_[0]))
            if cmd_vec[i].proc_error_ and i > highest_index_with_error:
                highest_index_with_error = i
        if highest_index_with_error:
            shcmd_cmdvec_kill(cmd_vec, highest_index_with_error, SHC_DIR_UP)
        highest_index_with_error = 0
        time.sleep(0.1)
    return shcmd.shcmd_get_exitval()

# Depricated name
def shcmd_wait(shcmd, cmd_vec):
    cmdv = cmd_vec
    ncmds = 0
    while cmdv is not None:
        cmdv += 1
        ncmds += 1
    cmdv -= 1
    print("")
    if shcmd.shcmd_reap_child(cmdv) > 0:
        print("")
        ncmds -= 1
        for i in range(ncmds):
            print("")
            pid = shcmd.shcmd_reap_child(cmd_vec[i])
    ret = shcmd.shcmd_get_exitval()
    return ret


def shcmd_cmdvec_exec(shcmd, cmd_vector):
    return shcmd.shcmd_command(cmd_vector)

def shcmd_unix_exec(shcmd):
    if shcmd.close_all_fd_ >= 0:
        swgp_close_all_fd(shcmd.close_all_fd_)
    os.execve(shcmd.argv_[0], shcmd.argv_, shcmd.envp_)

def shcmd_unix_execvp(shcmd):
    if shcmd.close_all_fd_ >= 0:
        swgp_close_all_fd(shcmd.close_all_fd_)
    os.execvp(shcmd.argv_[0], shcmd.argv_)

def shcmd_unix_execve(shcmd):
    if shcmd.close_all_fd_ >= 0:
        swgp_close_all_fd(shcmd.close_all_fd_)
    os.execve(shcmd.argv_[0], shcmd.argv_, shcmd.envp_)

# Deprecated function
def shcmd_command(shcmd, cmd_vector):
   pids = [[0 in range (0,0)]]
   return shcmd.command_recurse(cmd_vector, pids, 0, None)

def shcmd_write_command_to_buf(shcmd, tmp):
    if shcmd.argv_:
        for arg in shcmd.argv_:
            if any(char in SHCMD_TAINTED_CHARS for char in arg):
                tmp += "'{}'".format(arg)
            elif len(arg) == 0:
                tmp += "''"
            else:
                tmp += arg
            if arg != shcmd.argv_[-1]:
                tmp += " "
    return 0

#    def shcmd_debug_show_to_file(shcmd, vec, file):
#        for cmd in vec:
#            shcmd.shcmd_debug_show_to_file(cmd, file)
#        return 0

def shcmd_debug_show(shcmd):
    return shcmd.shcmd_debug_show_to_file(shcmd, sys.stderr)

def shcmd_debug_show_command(cmd, fd):
    ret = 0
    tmp = strob_open(10)
    shcmd_write_command_to_buf(cmd, tmp)
    strob_sprintf(tmp, STROB_DO_APPEND, "\n")
    ret = uxfio_write(fd, strob_str(tmp), strob_strlen(tmp))
    strob_close(tmp)
    return ret

def shcmd_debug_show_to_file(shcmd, cmd, file):
    if cmd.cmdstring_:
        file.write("{}: command string = [{}]\n".format(cmd, cmd.cmdstring_))
    else:
        file.write("{}: command string = nil\n".format(cmd))
    if cmd.argv_:
        for arg in cmd.argv_:
            file.write("<[{}]>".format(arg))
    else:
        file.write("nil\n")
    file.write("\n     pid={} exit value={}\n".format(cmd.pid_, shcmd.shcmd_get_exitval()))
    file.write("     status={}, srcfd={}; dstfd={}\n".format(cmd.status_, cmd.srcfd_, cmd.dstfd_))
    file.write("     user=[{}] group=[{}]\n".format(cmd.user_, cmd.group_))
    return 0

def shcmd_close(shcmd):
    if shcmd.dstfile_:
        del shcmd.dstfile_
    if shcmd.srcfile_:
        del shcmd.srcfile_
    if shcmd.errfile_:
        del shcmd.errfile_
    if shcmd.cmdstring_:
        del shcmd.cmdstring_
    if shcmd.argv_ and not shcmd.argv_set_:
        del shcmd.argv_
    return 0

def shcmd_set_exec_function(shcmd, form):
    if form == "execvp":
        shcmd.f_exec_ = shcmd.shcmd_unix_execvp
    elif form == "execve":
        shcmd.f_exec_ = shcmd.shcmd_unix_execve
    else:
        sys.stderr.write("shcmd_set_exec_function: no effect, invalid function\n")
    return 0

def shcmd_set_dstfd(shcmd, fd):
    shcmd.dstfd_ = fd
    return 0

def shcmd_set_append(shcmd, do_append):
    shcmd.shcmd_set_append1(do_append)
    return 0

def shcmd_set_append1(shcmd, do_append):
    shcmd.append1_ = do_append
    return 0

def shcmd_set_append2(shcmd, do_append):
    shcmd.append2_ = do_append
    return 0

def shcmd_set_srcfile(shcmd, name):
    shcmd.srcfile_ = name
    return 0

def shcmd_set_dstfile(shcmd, name):
    shcmd.dstfile_ = name
    return 0

def shcmd_set_errfile(shcmd, name):
    shcmd.errfile_ = name
    return 0

def shcmd_set_srcfd(shcmd, fd):
    shcmd.srcfd_ = fd
    return 0

def shcmd_set_errfd(shcmd, fd):
    shcmd.errfd_ = fd
    return 0

def shcmd_get_srcfd(shcmd):
    return shcmd.srcfd_

def shcmd_get_dstfd(shcmd):
    return shcmd.dstfd_

def shcmd_get_errfd(shcmd):
    return shcmd.errfd_

def shcmd_get_pid(shcmd):
    return shcmd.pid_

def shcmd_get_exitval(shcmd):
    if shcmd.pid_ and os.WIFEXITED(shcmd.status_):
        return os.WEXITSTATUS(shcmd.status_)
    else:
        return SHCMD_UNSET_EXITVAL

def shcmd_get_envp(shcmd):
    return shcmd.envp_

def shcmd_set_envp(shcmd, env):
    shcmd.envp_ = env
    return 0

def shcmd_get_umask(shcmd):
    return shcmd.umask_

def shcmd_set_umask(shcmd, mode):
    shcmd.umask_ = mode
    return 0

def shcmd_get_argvector(shcmd):
    return shcmd.argvector_

def shcmd_add_arg(shcmd, arg):
    shcmd.argvector_.append(arg)
    shcmd.argv_set_ = 1
    shcmd.argv_ = shcmd.argvector_
    return shcmd.argv_

def shcmd_set_argv(shcmd, argv):
    shcmd.argv_set_ = 1
    shcmd.argv_ = argv
    if shcmd.cmdstring_:
        del shcmd.cmdstring_
    shcmd.cmdstring_ = argv[0]
    return 0

def shcmd_set_user(shcmd, name):
    shcmd.user_ = name
    return 0

def shcmd_set_group(shcmd, name):
    shcmd.group_ = name
    return 0

def shcmd_get_user(shcmd):
    return shcmd.user_

def shcmd_get_group(shcmd):
    return shcmd.group_

def shcmd_set_lowest_close_fd(shcmd, fd):
    shcmd.close_all_fd_ = fd
    return 0


def internal_redirect(srcfd, srcfile, dstfd, dstfile, append1, errfd, errfile, append2):
    if srcfd != 0 or srcfile:
        if srcfd == sys.stdin.fileno() and not srcfile:
            pass
        elif srcfd > 0 and not srcfile:
            try:
                os.dup2(srcfd, sys.stdin.fileno())
            except OSError:
                sys.stderr.write("src dup error\n")
        elif srcfile:
            try:
                fd = os.open(srcfile, os.O_RDONLY)
            except OSError:
                sys.stderr.write("fatal: error opening {}\n".format(srcfile))
                sys.exit(1)
            if fd != sys.stdin.fileno():
                os.dup2(fd, sys.stdin.fileno())
        else:
            sys.stderr.write("fatal: internal error, invalid stdin redirection.\n")
            sys.exit(1)
    if dstfd != sys.stdout.fileno() or dstfile:
        if dstfd == sys.stdout.fileno() and not dstfile:
            pass
        elif dstfd > sys.stdout.fileno() and not dstfile:
            try:
                os.dup2(dstfd, sys.stdout.fileno())
            except OSError:
                sys.stderr.write("dst dup error\n")
        elif dstfile:
            flags = os.O_WRONLY | os.O_CREAT
            if append1:
                flags |= os.O_APPEND
            else:
                flags |= os.O_TRUNC
            try:
                fd = os.open(dstfile, flags, 0o666)
            except OSError:
                sys.stderr.write("fatal: error opening {}\n".format(dstfile))
                sys.exit(1)
            if fd != sys.stdout.fileno():
                os.dup2(fd, sys.stdout.fileno())
            if append1:
                try:
                    os.lseek(1, 0, os.SEEK_END)
                except OSError:
                    sys.stderr.write("error seeking to end of {}\n".format(dstfile))
        else:
            sys.stderr.write("fatal: internal error, invalid stdout redirection.\n")
            sys.exit(1)
    if errfd != sys.stderr.fileno() or errfile:
        if errfd == sys.stderr.fileno() and not errfile:
            pass
        elif (errfd > sys.stderr.fileno() and not errfile) or errfd == sys.stdout.fileno():
            try:
                os.dup2(errfd, sys.stderr.fileno())
            except OSError:
                sys.stderr.write("err dup error\n")
        elif errfile:
            flags = os.O_WRONLY | os.O_CREAT
            if append2:
                flags |= os.O_APPEND
            else:
                flags |= os.O_TRUNC
            try:
                fd = os.open(errfile, flags, 0o666)
            except OSError:
                sys.stderr.write("fatal: error opening {}\n".format(errfile))
                sys.exit(1)
            if fd != sys.stderr.fileno():
                os.dup2(fd, sys.stderr.fileno())
            if append2:
                try:
                    os.lseek(fd, 0, os.SEEK_END)
                except OSError:
                    sys.stderr.write("error seeking to end of {}\n".format(errfile))
        else:
            sys.stderr.write("fatal: internal error, invalid stderr redirection.\n")
            sys.exit(1)


def shcmd_get_last_command(cmdvec):
    last = None
    for cmd in cmdvec:
        last = cmd
    return last


def shcmd_do_tainted_data_check(cmdstring):
    if any(char in SHCMD_TAINTED_CHARS for char in cmdstring):
        return 1
    return 0


def shcmd_find_in_path(ppath, pgm):
    import os
    if '/' in pgm:
        try:
            st = os.stat(pgm)
            if not stat.S_ISREG(st.st_mode):
                return None
            return pgm
        except OSError:
            return None
    if not ppath:
        return None
    for path in ppath.split(':'):
        name = os.path.join(path, pgm)
        if os.access(name, os.X_OK):
            return name
    return None


def shcmd_cmdvec_kill(cmd_vec, from_, direction):
    ret = 0
    i = from_
    while i >= 0 and cmd_vec[i]:
        if cmd_vec[i].status_ == 0 and cmd_vec[i].esrch_ == 0 and cmd_vec[i].child_gone_ == 0:
            try:
                os.kill(cmd_vec[i].pid_, signal.SIGTERM)
            except OSError:
                if errno == errno.ESRCH:
                    cmd_vec[i].esrch_ = 1
        if direction == SHC_DIR_UP:
            i += 1
        else:
            i -= 1
    return ret
