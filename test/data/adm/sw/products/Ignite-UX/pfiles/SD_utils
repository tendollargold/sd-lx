########
# Product: Ignite-UX
# SD_utils
#
# @(#) $Revision: 10.1 $
#
# Control utils that were delivered in a patch to 11.00, but are
# not present on pre-PHCO_23966.SD-AGENT 11.00 systems. Control scripts
# that use these functions check if they are in control.utils, if not
# then this file is sourced in.
#
########
#
########################################################################
#
# (c) Copyright Hewlett-Packard Company 2005
#
########


# the newer control_utils functions that will be sourced
# use SW_TMPDIR and assume it is setup
: ${SW_TMPDIR:=/var/adm/sw/tmp}

########
#hlpb-get_sw_spec
# get_sw_spec
#
# Usage:  get_sw_spec product[.fileset]
#
# Print to stdout the full software specification of the software passed to
# the function as the argument.  The software can be either a PRODUCT name or
# a PRODUCT.FILESET pair.  A NULL is printed if the specified software is not
# found.
#
# This function only lists software installed on the local system under the
# root "/" directory.  The specification that gets printed is undetermined in
# the event that multiple installed versions are encountered.
#
#hlpe-get_sw_spec
########
get_sw_spec()
{
    typeset pfs=${1%%,*}
    typeset spec=""

    # A debug value messes up 'swlist' output, so unset it for this function.
    if [[ -n $SDU_DEBUG ]]
    then
        typeset SDU_DEBUG_SAVED=$SDU_DEBUG
        unset SDU_DEBUG
    fi

    set -- $(swlist -l fileset -a software_spec $pfs 2>/dev/null)
    while [[ $# -gt 1 ]]
    do
        [[ $1 = $pfs ]] && spec=$2 && break
        shift
    done
    print -u1 $spec

    [[ -n $SDU_DEBUG_SAVED ]] && SDU_DEBUG=$SDU_DEBUG_SAVED

} #  get_sw_spec

######
#hlpb-IPD_delete_ancestors
#
# Usage: IPD_delete_ancestors
#
# Defaults to $SW_SOFTWARE_SPEC - no arguments allowed. SW_SOFTWARE_SPEC
# must be a product.
#
# Deletes all ancestors of the filesets contained in the SW_SOFTWARE_SPEC.
# If SW_SOFTWARE_SPEC is a *fileset* the function will return after 
# posting a WARNING message. If all the filesets of a product are marked
# for removal, the product will be removed as well. All removals will be
# done by "swmodify -u".
#
# Note! If $SW_SOFTWARE_SPEC has the attributes is_patch and is_sparse
# set to true, an error will be generated and processing will stop. This
# function *must* not be called from a patch's control scripts!
#
#hlpe-IPD_delete_ancestors
######
IPD_delete_ancestors ()
{

	# Check arguments
	if [ $# -ne 0 ]
	then
		# No arguments allowed
		cu_usage IPD_delete_ancestors
		return 1
	fi
	
	typeset string


	# This function needs to be called from a product control script
	string="${SW_SOFTWARE_SPEC%%,*}"
	if [ "${string%.*}" != "${string}" ]
	then 
		# This is a *fileset*. Print warning and return 1
		msg WARNING "{$SW_SOFTWARE_SPEC} is a fileset - therefore no \
					obsolete software was removed."
		cu_usage IPD_delete_ancestors
		return $WARNING
	fi
    
    string=`uname -r`	
	os_version=`echo $string | cut -f2 -d.`
    typeset -i os_minor
	os_minor=`echo $string | cut -f3 -d.`

	if [ "${os_version}" = "11" ]
    then
      if [ os_minor -ge 11 ]
      then
         LOCAL_SWLIST_OPTIONS="-x log_msgid=0"
      fi
    fi

	if [ "${os_version}" = "11" ]
    then
	  # make sure this was not called from a patch
	  # deleting path ancestors would be a bad thing
	  swlist "${SW_SOFTWARE_SPEC}",c=patch > /dev/null 2>&1
	  if [ $? -eq 0 ]
	  then
	      # punt - it's a patch
	      msg ERROR "Ancestors for ${SW_SOFTWARE_SPEC} could not be \
	      removed because ${SW_SOFTWARE_SPEC} is a patch. Obsolete \
	      information could not be removed from the system. Please \
	      contact your HP Support Representative to resolve this problem."
  

	      cu_usage IPD_delete_ancestors
	      return $FAILURE
	  fi
	fi


	cond_mkdir 700 bin bin ${SW_TMPDIR}
	if [ $? -ne 0 ]
	then
		# punt
		msg ERROR "Could not create ${SW_TMPDIR} with \
			the proper permissions. IPD_delete_ancestors cancelled."
		return $FAILURE
	fi

	# The SDU_DEBUG_PRINT_MSGID variable can mess up the parsing
  	# of swlist output so unset it for this function.
    if [[ -n "${SDU_DEBUG_PRINT_MSGID}" ]]
    then
      typeset SDU_DEBUG_PRINT_MSGID_SAVED=$SDU_DEBUG_PRINT_MSGID
      SDU_DEBUG_PRINT_MSGID=""
    fi


	typeset tmpd=${SW_TMPDIR}/IPD_delete_ancestors.$$
	typeset temp_file=${tmpd}.temp
	typeset temp_file2=${tmpd}.temp2
	typeset ancestor_list=${tmpd}.ancestors
	typeset patch_list=${tmpd}.patches
	typeset save_cust_list=${tmpd}.save_custom
	typeset swlist_list=${tmpd}.swlist
	typeset prod_string field_one field_two field_three prod_name fs_name
	typeset modify_list=${tmpd}.modify
	typeset -i rc
	typeset product	       
	typeset all_filesets="true"


	# Get the swlist of fileset ancestors for SW_SOFTWARE_SPEC
	swlist -l fileset -a software_spec -a ancestor \
       ${LOCAL_SWLIST_OPTIONS} ${SW_SOFTWARE_SPEC} \
       2>/dev/null | clean_swlist_output > ${swlist_list}

	# clear temp_file2 and create ancestor_list and patch_list
	>${ancestor_list}
	>${patch_list}


	while read -r line
	do
		line="${line} "
		fs_name="${line%% *}"
		substr="${line#* }"
		substr="${substr#* }"
		# Write the ancestors, one per line, out to a file
		while [ ! -z "${substr}" ]
		do
                        # do not delete myself :-)
                        # do not delete SW-GETTOOLS.SD-SUPPORT as this is used
                        #    as a dummy ancestor to get new filesets

                        if [ "${fs_name}" != "${substr%%[ ,]*}" -a \
                             SW-GETTOOLS.SD-SUPPORT != "${substr%%[ ,]*}" ]
			then
				# write out a particular ancestor only once
				grep -q "^${substr%% *}$" ${ancestor_list}
				if [[ $? = 1 ]]
				then
					echo "${substr%% *}" >> ${ancestor_list}
				fi
			fi
			substr="${substr#* }"
		done
	done < ${swlist_list}


	if [ ! -s "${ancestor_list}" ]
	then
		rm -f ${tmpd}.*
        # restore SDU_DEBUB_PRINT_MSGID if previously set
        if [[ -n "${SDU_DEBUG_PRINT_MSGID_SAVED}" ]]
        then
             SDU_DEBUG_PRINT_MSGID=$SDU_DEBUG_PRINT_MSGID_SAVED
             export SDU_DEBUG_PRINT_MSGID
        fi
		return 0
	fi

	# find all the ancestors that are on the system
	swlist -l fileset -a software_spec ${LOCAL_SWLIST_OPTIONS} \
           -f "${ancestor_list}" 2>/dev/null | clean_swlist_output > ${temp_file2}
		
	>${ancestor_list}
	while read -r line
	do
		echo "${line#* }" | strip_off_fr >> ${ancestor_list}
	done < ${temp_file2}


	# process each product that has a fileset to be deleted

	# zero out the modify_list
	>${modify_list}

	# now lets process each ancestor
	# do not read the file in the loop as we delete
	# filesets as we process them
	# common case of 1 product will empty this file in one pass	
	while [ 1 = 1 ]
	do
		read -r line < ${ancestor_list}

		# exit the loop when there are no more filesets to process
		if [[ -z "${line}" ]]
		then	
			break
		fi

		# strip out fileset name, fa=, fr=
		# we need to keep product qualifiers
		product=$(echo "${line}" | awk -F, '{
				for(i = 1 ; i <= NF ; i++) { 
					if ( $i ~ /^(fa=|fr=)/) { 
						$i=""
					}
				}
				OFS=","
				gsub("\..*", "", $1) # remove fileset name
				gsub(",,", ",")	     # remove double ,s
				sub(",$", "")        # remove trailing ,
				print $0
			}' )

		# get all the filesets in the product
		all_filesets="true"
		swlist -l fileset -a software_spec -a applied_patches \
		    ${LOCAL_SWLIST_OPTIONS} ${product} \
			2> ${temp_file2} | clean_swlist_output | strip_off_fr | \
		while read -r field_one field_two field_three
		do
            # add patches to list of ancestors so that they are processed
			grep -q "^${field_two}$" ${ancestor_list}
			if [[ $? = 1 ]] ; then
				# the installed fileset is not an ancestor
				# so do not delete the product
				all_filesets="false"
			else
				# the fileset is an ancestor so delete it
				grep -v "^${field_two}$" ${ancestor_list} > ${temp_file2}
				mv ${temp_file2} ${ancestor_list}
				print "${field_two}" >> ${modify_list}
                if [ ! -z "${field_three}" ] 
                then
                  for field in ${field_three}
                  do
                    # add patches to list of ancestors so that they are
					# processed like any other fileset
                    print "${field}" >> ${ancestor_list}
                    print "${field}" >> ${patch_list}
                  done
                fi
			fi
		done

		# if all filesets in the product are ancestors
		# then delete the product also
		if [[ "${all_filesets}" = "true" ]]
		then
			# Delete the product as well
			print "${product}" >> ${modify_list}

            # The rest of this if statement is for dealing with the
            # cleaning up of the save_custom area when patches are involved.

			# See if the base name is a directory in the save_custom area

            # Strip off anything after a ","
			temp_string="${product%%,*}"
            # Strip off anything after a "."
			temp_string="${temp_string%%.*}"

            # This is paranoia, but always a good idea 
            if [ ! -z "${temp_string}" ] 
			then
                # Create full path
                temp_string="/var/adm/sw/save_custom/"${temp_string}
    			# does this exist as a directory?
				if [ -d "${temp_string}" ]
                then
                  # add to the list of directories to be removed
				  print "${temp_string}" >> ${save_cust_list}
				fi
			fi
		fi
	done

	# In *theory* this should not happen
	if [ ! -s "${modify_list}" ]
	then
		rm -f ${tmpd}.*
        # restore SDU_DEBUB_PRINT_MSGID if previously set
        if [[ -n "${SDU_DEBUG_PRINT_MSGID_SAVED}" ]]
        then
          SDU_DEBUG_PRINT_MSGID=$SDU_DEBUG_PRINT_MSGID_SAVED
          export SDU_DEBUG_PRINT_MSGID
        fi
		return 0
	fi


	if [[ ( "${os_version}" = "11" ) && ( -s ${patch_list} ) ]]          
    then
	  # commit patches in patch_list before removing from IPD
	  swmodify -f ${patch_list} -x patch_commit=true >${temp_file2} 2>&1
	  rc=$?
	  if [ $rc != 0 ]
	  then
	      # Problem with swmodify - bail out
		  awk '{print}' ${temp_file2}
		  rm -f ${tmpd}.*
		  rc=1
          # restore SDU_DEBUB_PRINT_MSGID if previously set
          if [[ -n "${SDU_DEBUG_PRINT_MSGID_SAVED}" ]]
          then
             SDU_DEBUG_PRINT_MSGID=$SDU_DEBUG_PRINT_MSGID_SAVED
             export SDU_DEBUG_PRINT_MSGID
          fi
		  return $rc
	  fi
	fi

	# remove the entries found in modify_list from the IPD
	swmodify -u -f ${modify_list} >${temp_file2} 2>&1
	rc=$?
	if [ $rc != 0 ]
	then
	# Problem with swmodify - bail out
		awk '{print}' ${temp_file2}
		rm -f ${tmpd}.*
		rc=1
        # restore SDU_DEBUB_PRINT_MSGID if previously set
        if [[ -n "${SDU_DEBUG_PRINT_MSGID_SAVED}" ]]
        then
             SDU_DEBUG_PRINT_MSGID=$SDU_DEBUG_PRINT_MSGID_SAVED
             export SDU_DEBUG_PRINT_MSGID
        fi
		return $rc
	fi

	if [[ ( "${os_version}" = "11" )  && ( -s ${save_cust_list} ) ]]   
    then
	  # remove the save_custom directories associated with the patches
	  # that were just removed.

	  while read -r line
	  do
	    rm -rf ${line}	
	  done < ${save_cust_list}
	fi


	# cleanup and exit

	rm -f ${tmpd}.*
    # restore SDU_DEBUB_PRINT_MSGID if previously set
    if [[ -n "${SDU_DEBUG_PRINT_MSGID_SAVED}" ]]
    then
      SDU_DEBUG_PRINT_MSGID=$SDU_DEBUG_PRINT_MSGID_SAVED
      export SDU_DEBUG_PRINT_MSGID
    fi

	return $rc
}


########
#hlpb-IPD_delBundleWrapper
# IPD_delBundleWrapper
#
# Usage: IPD_delBundleWrapper <-f file> <list of bundle(s)>
#
# Delete bundle wrappers from IPD if and only if they exist on the system
#
#hlpe-IPD_delBundleWrapper
########
IPD_delBundleWrapper ()
{


	cond_mkdir 700 bin bin ${SW_TMPDIR}
	if [ $? -ne 0 ]
	then
		# punt
		msg ERROR "Could not create ${SW_TMPDIR} with \
			the proper permissions. IPD_delBundleWrapper cancelled."
		return $FAILURE
	fi

    typeset tmpd=${SW_TMPDIR}/IPDdelbun.$$
    typeset temp_file=${tmpd}.temp
    typeset temp_file2=${tmpd}.temp2
    typeset bund_list=${tmpd}.bundles

	
	# start with a clean slate
 	rm -f ${tmpd}.*

# Cycle through the parameters and get the bundle names

	while [ -n "$1" ]
	do
		case $1 in
		-f ) while read -r line
			 do
				# kill any qualifiers and write to bundle list
			 	echo ${line%%,*} >> ${bund_list}
			 done < $2
			 shift 2 ;;
		  *) 
			 # kill any qualifiers and write to bundle list
			 echo ${1%%,*} >> ${bund_list}
			 shift ;;
		esac
	done

	# Gather a list of all the bundles installed

	swlist -l bundle -a software_spec > ${temp_file} 2>/dev/null


	# Strip out the comments, empty lines, tabs, and multiple spaces.

	sed -e '/^$/d' -e '/^#/d' -e 's/[	 ]\{1,\}/ /g' -e 's/^ //' \
			${temp_file} > ${temp_file2}



	# Got bundles?
	if [ ! -s ${temp_file2} ]
	then
		#no bundles on system - nothing to do
		rm -f ${tmpd}.*
		return 0
	fi

	# clear ${temp_file}
	> ${temp_file}

	# go through the list of all the bundles on the system looking for
	# matches with the bundles requested by the user

	while read -r line
	do
		# go through the list of bundles that match ${line}
		grep "^${line} " ${temp_file2} | while read first second
		do
			# are there qualifiers on the software_spec?
			if [ "${second}" = "${second#*,}" ]
			then
				# no qualifiers - append a "." and put it in the list
				# of bundles to be deleted
				echo "${second}." >> ${temp_file} 
			else
				# qualifiers - put a "." after the bundle name and 
				# then put it all in the list to be deleted
				echo "${first}.,${second#*,}" >> ${temp_file}
			fi
		done
	done < ${bund_list}


	if [ ! -s ${temp_file} ]
	then
		# No bundles match - bail out
 		rm -f ${tmpd}.*
		return 0
	fi
	
	typeset rc

	swmodify -u -f ${temp_file} >${temp_file2} 2>&1
	rc=$?
	if [ $rc != 0 ]
    then
		# Problem with swmodify - bail out
		awk '{print}' ${temp_file2}
		rm -f ${tmpd}.*
		return 1
	fi

# swmodify selection code has problems. Workaround is to call swlist again
# with the bundle names and see who's left after the first time


	# Gather a list of all the bundles installed

	swlist -l bundle -a software_spec -f > ${temp_file} 2>/dev/null


	# Strip out the comments, empty lines, tabs, and multiple spaces.

	sed -e '/^$/d' -e '/^#/d' -e 's/[	 ]\{1,\}/ /g' -e 's/^ //' \
			${temp_file} > ${temp_file2}



	# Got bundles?
	if [ ! -s ${temp_file2} ]
	then
		#no bundles on system - nothing to do
		rm -f ${tmpd}.*
		return 0
	fi

	# clear ${temp_file}
	> ${temp_file}

	# go through the list of all the bundles on the system looking for
	# matches with the bundles requested by the user

	while read -r line
	do
		# go through the list of bundles that match ${line}
		grep "^${line} " ${temp_file2} | while read first second
		do
			# are there qualifiers on the software_spec?
			if [ "${second}" = "${second#*,}" ]
			then
				# no qualifiers - append a "." and put it in the list
				# of bundles to be deleted
				echo "${second}." >> ${temp_file} 
			else
				# qualifiers - put a "." after the bundle name and 
				# then put it all in the list to be deleted
				echo "${first}.,${second#*,}" >> ${temp_file}
			fi
		done
	done < ${bund_list}


	if [ ! -s ${temp_file} ]
	then
		# No bundles match - bail out
 		rm -f ${tmpd}.*
		return 0
	fi
	
	typeset rc

	swmodify -u -f ${temp_file} >${temp_file2} 2>&1
	rc=$?
	if [ $rc != 0 ]
	    then
		# Problem with swmodify - bail out
		awk '{print}' ${temp_file2}
		rc=1
    fi

	# cleanup and exit

	rm -f ${tmpd}.*
	return $rc
} # End of IPD_delBundleWrapper 

########
#hlpb-msg
#
# msg - this function prints a SD logfile compatible message to 
# stdout. Line wrapping will occur, but note that any word > 70
# characters will not be broken up and that the message may not
# exceed 3000 bytes (awk limitation). 
#
# usage: msg [ error | warning | note | star | blank ] <message text>
#
#hlpe-msg
########
msg ()
{
#
#   reminder: the awk script puts an extra 'blank' before
#   the message so 'pre' is only 8 characters wide
#
    if [[ $# = 1 ]] ; then
	pre='        '
    else
#
#       if the type of message is not supplied, treat it as if
#       'blank' was supplied
#
	case $1 in
	NOTE|note)	 pre='NOTE:   ' ; shift ;;
	WARNING|warning) pre='WARNING:' ; shift ;;
	ERROR|error)	 pre='ERROR:  ' ; shift ;;
	STAR|star)       pre='       *' ; shift ;;
	BLANK|blank)     pre='        ' ; shift ;;
	*)               pre='        ' ;;
	esac
    fi

# print the message type (NOTE,WARNING,..,BLANK) and then don't
# put a newline so that the message will be next to the message
# type (i.e. "ERROR:  Your message goes here").

    print -n "${pre}"

#
#   awk algorithm
#   1) compress multiple blanks & tabs into one blank
#      eat all newline characters
#   2) print up to 70 characters of output per line
#      words greater than 70 characters will be printed on one line
#      - leading 9 characters are blank except for the first line
#        which is assumed to be printed above
#

    print -r "$*" | \
    awk '{ gsub("[ \t]+", " ") 
	   if ( msg == "" ) {
		msg = $0 
	   } else {
		msg = msg " " $0
	   }
	 }
	 END { 	num = split (msg, words, " ")
		len=0
		for (i=1; i <= num ; i ++) {
		    if ( (len != 0) && ((len + 1 + length(words[i])) > 70)) {
			printf "\n        "
			len = 0
		    }
		    printf " %s", words[i]
		    len = len + 1 + length(words[i])
		}
		printf "\n"
	     }'
} # msg
########
#hlpb-clean_swlist_output
#
#  This function cleans up swlist output for further processing
#  
#hlpe-clean_swlist_output
########
clean_swlist_output ()
{
	# tabs to space
	# multiple spaces to 1 space
	# leading space eliminated
	# empty lines elimnated
	# comment lines eliminated
	sed -e 's/	/ /g' -e 's/  */ /g' -e 's/^ //' -e '/^ *$/d' -e '/^#/d'
}
########
#hlpb-strip_off_fr
#
#  This function strips off the "fr=*" attribute from swlist output
#  
#hlpe-strip_off_fr
########
strip_off_fr ()
{
	awk -F, '{
		for(i = 1 ; i <= NF ; i++) {
			if ( $i ~ /^fr=/) {
				$i=""
			}
		}
		OFS=","
		gsub(",,", ",")      # remove double ,s
		sub(",$", "")        # remove trailing ,
		print $0
	}'

}
########
#hlpb-cu_usage
#
# cu_usage - this function runs a quoted command line and prints
# out the command's stdout (not stderr) in SD log format to stdout. 
#
# usage: cu_usage 
#
#hlpe-cu_usage
########

cu_usage()
{
    if [ $# -ne 1 ]
    then
       cu_usage cu_usage
       return FAILURE
    fi

    msg ERROR "Usage error. Man page follows"
    msg "++++++++++++++++++++++++++++++++++++++++++++"
    cu_man $1 |&
    while read -r -p line
    do
      msg "$line"
    done
    msg "++++++++++++++++++++++++++++++++++++++++++++"

return SUCCESS
}
########
#hlpb-cu_man
#
# cu_man
#
#  Usage: cu_man [functionname] [-html]
#
# Warning! This function is being developed for the 11.11 release
# and may not be available on previous releases.
#
# This function will take a control_utils function name and then 
# print out to stdout whatever header info exists for that function.
# Note that if invoked without an argument, all of the function 
# headers in control_utils will be printed. Note also that if you 
# use the "-html" trailing option, the > and < characters will be 
# replaced by the HTML flags that represent those characters.
#
#hlpe-cu_man
########
cu_man ()
{
is_html=0

if [ "$2" = "-html" ]
then
  is_html=1
fi

# create strings for awk to search on

string1="#hlpb-"$1
string2="#hlpe-"$1

# use awk to strip out the lines between string1 and string2 without
# printing string1 or string2.

# TODO: the strings aren't sufficient to differentiate between cu_man
# and cu_man_html, so I had to rename cu_man_html to cu_html. We may
# want to fix that sometime.

awk '/'"$string1"'/,/'"$string2"'/ {
if ( html == 1 )
{
  gsub (/</,"\&lt;")
  gsub (/>/,"\&gt;") 
}
if (($1 != '"\"$string1\""') && ($1 != '"\"$string2\""')) {
$1=""
print
}
} ' html=$is_html /usr/lbin/sw/control_utils

return $SUCCESS
}
