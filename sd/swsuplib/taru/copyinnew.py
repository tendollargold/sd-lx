# copyinnew.py - readin new ascii headers
#   Copyright (C) 1990, 1991, 1992 Free Software Foundation, Inc.
#
#   Portions of this code are derived from code (found in GNU cpio)
#   copyrighted by the Free Software Foundation.  Retention of their 
#   copyright ownership is required by the GNU GPL and does *NOT* signify 
#   their support or endorsement of this work.
#   						jhl
#					   
#   *** 
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
#
import sys

from sd.swsuplib.taru.ahs import ahsstaticsettarfilenamelength, ahsstaticgettarfilename
from sd.swsuplib.taru.readinheader import taru_tape_buffered_read, taru_tape_skip_padding


def tarui_read_new_ascii_from_buf(taru, file_hdr, ascii_header):
    data = ascii_header.split()
    file_hdr.c_ino, file_hdr.c_mode, file_hdr.c_uid, file_hdr.c_gid, file_hdr.c_nlink, file_hdr.c_mtime, xx, file_hdr.c_dev_maj, file_hdr.c_dev_min, file_hdr.c_rdev_maj, file_hdr.c_rdev_min, file_hdr.c_namesize, file_hdr.c_chksum = map(lambda x: int(x, 16), data[:12])

    file_hdr.c_filesize = xx

    if file_hdr.c_namesize > 6000:
        return -1
    return 104


def tarui_read_in_new_ascii(taru, file_hdr, in_des, archive_format_in, buf):
    ascii_header = [0] * 112

    if buf:
        ascii_header[:104] = buf
    else:
        if taru_tape_buffered_read(in_des, ascii_header,104) != 104:
            sys.stderr.write("error in tarui_read_in_new_ascii at 000a")
            return -1
    ascii_header[:104] = '\0'
    bytesread = tarui_read_new_ascii_from_buf(taru, file_hdr, str(ascii_header))
    if bytesread < 0:
        sys.stderr.write("error in tarui_read_in_new_ascii at 000b")
        return -bytesread

    # Read file name from input.
    ahsstaticsettarfilenamelength(file_hdr, file_hdr.c_namesize + 1)

    if buf:
        if file_hdr.c_namesize > 406:
            sys.stderr.write("name too long.\n")
            return -1
        ahsstaticgettarfilename(file_hdr)[:] = buf[104:104 + file_hdr.c_namesize]
    else:
        taru_tape_buffered_read(in_des, ahsstaticgettarfilename(file_hdr), int(file_hdr.c_namesize))
        # E_DEBUG2("name is %s\n", ahsStaticGetTarFilename(file_hdr))
    bytesread += file_hdr.c_namesize

    #   In SVR4 ASCII format, the amount of space allocated for the header
    #     is rounded up to the next long-word, so we might need to drop
    #     1-3 bytes.
    return bytesread + taru_tape_skip_padding(in_des, bytesread + 6, archive_format_in)


# read in a head beginning just after the magic.
# Does not read the linkname in the file data portion.
#   Return the number of bytes read, -1 on error.
# 


def taru_read_in_new_ascii(taru, file_hdr, in_des, archive_format_in):
    return tarui_read_in_new_ascii(taru, file_hdr, in_des, archive_format_in, None)




