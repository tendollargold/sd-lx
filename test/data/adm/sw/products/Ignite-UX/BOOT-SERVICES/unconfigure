#! /sbin/sh
########
# Product: Ignite-UX
# Fileset: BOOT-SERVICES
# unconfigure
# @(#) $Revision: 10.22 $
########
#
# (c) Copyright Hewlett-Packard Company 2000
#
########################################################################

##########################################################################
# This script does the following:
#
# - Remove tftp access to directories added by Ignite
#
# - Removes the instl_bootd line to inetd.conf
# 
# - Remove the example entries in /etc/bootptab
#
# - De-configures rbootd from starting at boottime
#

UTILS=/usr/lbin/sw/control_utils
if [ -f $UTILS ]
then
    . $UTILS
else
    echo "ERROR:   Cannot find $UTILS"
    exit 1
fi


#
# The Ignite TFTP stuff that was added to inetd.conf
#
INST_DIR=/opt/ignite
VAR_DIR=/var/opt/ignite

#
# vars for removing exports entries
#
VAR_CL_DIR=${VAR_DIR}/clients
if [[ $(uname -r) > B.11.23 ]]; then
    EXPORTS=/etc/dfs/dfstab
else
    EXPORTS=/etc/exports
fi
TMP_FILE=/var/tmp/$$tmp
integer CNT=0

#
# remove the Ignite entries
#
if [[ $SW_SESSION_IS_DRD -ne 1 ]]; then

  for dir in $INST_DIR $VAR_DIR
  do
      setup_tftp -d $dir
  done

  #
  # Issue a message that the Ignite directories have been removed from
  # tftp access, but that the tftp service itself is still enabled.
  # This was changed for JAGad47307.
  #

  echo "\
NOTE:    tftp access to $INST_DIR and $VAR_DIR has been
         removed from /etc/inetd.conf. The tftp service is still enabled.
         To disable tftp service run SAM."

else 

  echo "\
NOTE:    tftp access to $INST_DIR and $VAR_DIR will be
         removed from /etc/inetd.conf when the inactive
         image is booted. The tftp service will remain
         enabled.  To disable tftp service run SAM."

    initfile=/sbin/init.d/iux_unconf
    rcfile=/sbin/rc2.d/S900iux_unconf
    rm -rf $rcfile $initfile

    print "#!/sbin/sh" > $initfile
    print "    for dir in $INST_DIR $VAR_DIR" >> $initfile
    print "    do" >> $initfile
    print "       /usr/sbin/setup_tftp -d \$dir" >> $initfile
    print "    done" >> $initfile
    print "    rm $initfile $rcfile " >> $initfile
    print "    exit \$? " >> $initfile

    chmod 555 $initfile
    chown bin:bin $initfile
    ln -s $initfile $rcfile

fi

#
# Remove the line from inetd that enables the instl_bootd service
# if Ignite is removed no need to have the inetd listening for requests
#
new=/opt/ignite/lbin/instl_bootd
inetd_line="instl_boots"

if grep -q ${inetd_line} /etc/inetd.conf 2>/dev/null
then
    if grep -q $new /etc/inetd.conf 2>/dev/null
    then
         sed "/$inetd_line/d" /etc/inetd.conf > /tmp/inetd.conf$$
    fi

    [[ -s /tmp/inetd.conf$$ ]] &&
                      /sbin/mv /tmp/inetd.conf$$ /etc/inetd.conf
    chmog 444 bin bin /etc/inetd.conf                        
fi


                    
#
# Remove the example entries in /etc/bootptab if Ignite is removed
# JAGaf90651 - need to anchor the 'sed' patterns to avoid unexpected
# matches of 'ignite-defaults:'

begin_entry1='ignite-defaults:'
begin_entry2='System-IPF:'
end_entry1='bs=48'
end_entry2='ds=190\.1\.48\.11'
entry1_count=`grep -c $begin_entry1 /etc/bootptab`

  if [ $entry1_count -le 2 ]; then
      if grep -q ${end_entry2} /etc/bootptab 2>/dev/null
      then
      
        sed "/^${begin_entry1}/,/${end_entry1}$/d" \
                        /etc/bootptab > /tmp/bootptab1$$
                        
        sed "/^${begin_entry2}/,/${end_entry2}$/d" \
                        /tmp/bootptab1$$ > /tmp/bootptab$$

      fi
  fi

[[ -s /tmp/bootptab$$ ]] &&
                      /sbin/mv /tmp/bootptab$$ /etc/bootptab
chmog 444 bin bin /etc/bootptab    



#
# kill the instl_bootd daemon.  kill_named_procs is DRD-safe.
#
    kill_named_procs instl_bootd
#
# Signal inetd to re-read inetd.conf, but only when not in a DRD
# session.
#

if [[ $SW_SESSION_IS_DRD -ne 1 ]]; then

  inetd -c > /dev/null 2>&1

fi

#
# We used to modify netdaemons to remove the startup of rbootd.
# rbootd can be used for purposes other than Ignite, so we
# will issue a message that the daemon is still running and how to
# turn it off if wanted.
# JAGad47307
#
NETDAEMONS=/etc/rc.config.d/netdaemons


echo "\
NOTE:    The rbootd daemon may be in use by other programs and will
         not be disabled. If you want to disable rbootd, change the
         START_RBOOTD entry in $NETDAEMONS to 0."

#
# Remove the export of /var/opt/ignite/clients
#

#set -x
#
# Get list of exported dirs with "/var/opt/ignite" in them
#
if [[ $(uname -r) > B.11.23 ]]; then
    set -A IG_DIRS $(grep ${VAR_DIR} ${EXPORTS} 2>/dev/null|
                     awk '{print $NF}')
else
    set -A IG_DIRS $(grep ${VAR_DIR} ${EXPORTS} 2>/dev/null|
                     awk '{print $1}')
fi
#
# Loop through the list of ignite directories. If the directory exists and is
# /var/opt/ignite/clients then unexport it and remove from the exports file.
# If the directory doesn't exist it is unexported and removed from exports.
# Any other ignite directory (ie. /var/opt/ignite/recovery*
# /var/opt/ignite/archives*)  is left inplace.
#
for dir in ${IG_DIRS[*]}
{
  CNT=$(awk '$0 !~ /\#|^$/' ${EXPORTS}|sort -u |wc -l)
  if [[ -d ${dir} ]]
  then

# JAGaf68839:  sed doesn't work, even with a clever '\.<pattern>.' format,
# when the pattern ($dir) contains dots, such as
# "/var/opt/ignite/archives/Rel_B.11.00"; just use grep instead in two cases
# below:

    if [[ ${dir} = ${VAR_CL_DIR} ]]
    then
      exportfs -u ${dir} 2>/dev/null
      grep -v $VAR_CL_DIR $EXPORTS > $TMP_FILE
      [[ -s ${TMP_FILE} || ${CNT} -le 1 ]] && mv ${TMP_FILE} ${EXPORTS}
    fi
  else
    exportfs -u ${dir} 2>/dev/null
    grep -v "$dir" $EXPORTS > $TMP_FILE
    [[ -s ${TMP_FILE} || ${CNT} -le 1 ]] && mv ${TMP_FILE} ${EXPORTS}
  fi
}

rm -f ${TMP_FILE}
exit $retval
