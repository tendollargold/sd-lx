#!/usr/bin/python3

# swjob.py - swjob - create jobs for immediately execution or schedule them
# for later execution
# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>

#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
cmd = os.path.split(sys.argv[0])[1]

swjob_parser = argparse.ArgumentParser(prog='swjob',
		usage="""%(prog)s [-i] [-R] [-u] [-v] [-a attribute] [-C session_file] 
		[-f jobid_file] [-S session_file] [-t target_file] [-g] [-x option=value] 
		[-X option_file] [jobid(s)] [-T target_selections]
""",
	formatter_class=argparse.RawTextHelpFormatter,
	epilog='''
 Jobids are specified as:  jobid ...
 
 Target selections are specified as: -T host:/path ...
 
''')

swjob_parser.add_argument("-i --interactive", metavar='gui',
			 help='Runs the command in interactive mode (invokes the GUI.) (Using this option is an alias for the sd command.)')
swjob_parser.add_argument("-u --unconfigure",metavar='',
			 help="remove the job")
swjob_parser.add_argument("-v --verbose",metavar='', 
			 help="write verbose output to stdout")
swjob_parser.add_argument("-R",metavar='', 
			 help="shorthand for '@ *'")
swjob_parser.add_argument("-a --attribute",metavar='attribute', 
			 help="list this attribute (multiple -a options can \nbe specified)")
swjob_parser.add_argument("-x option=value",metavar='', 
			 help="set the option to value")
swjob_parser.add_argument("-X",metavar='option_file', 
			 help="read option definitions from this file")
swjob_parser.add_argument("-f",metavar='jobid_file', 
			 help="read the jobids from this file")
swjob_parser.add_argument("-t",metavar='target_file', 
			 help="read target selections from this file")
swjob_parser.add_argument("-C",metavar='session_file', 
			 help="save the session to session_file")
swjob_parser.add_argument("-S",metavar='session_file', 
			 help="read options, software selections, and target \nselections from this file")
# -g --gnu option added for GNU Copyleft
swjob_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")
swjob_parser.add_argument("job_ids", nargs='*',
						   help="job identifiers: XServer.MAN ...")
# B.3.4 page 188 Using -T vs @ for target selection as it is easier in argparse. :)
swjob_parser.add_argument("-T", nargs="+",metavar="[host][:][path]",
			  help="Target selections specified as: [host][:][path] \n default is localhost:/")

swjob_parser.parse_args()
args = swjob_parser.parse_args()
print("DEBUG: Args in: ", cmd,".py ",  args)

