------------------------------------------------------------------------

	HP-UX 11.23	Object Oriented USB Boot Mouse Driver

	This document contains Application Notes for the HP-UX
	11.23 Object Oriented C USB Boot Mouse driver

	Product Name: BOOT-MOUSE
	
	Deliverables:
	-------------
	/usr/conf/mod/UsbBootMouse

	Application Notes:
	------------------
	USB mouse driver for mice that comply with the boot
	protocol.

	Supported Mouse Model Numbers:

	M-UD43
	M-UB48
	M-UV96
	M-UVSDa

	Use of an unsupported USB device is strongly discouraged.

	Installation Notes:
	-------------------

	To install the driver, run 'swinstall USB-00'.

	If you wish to remove the software from the system, run
	'swremove -x autoreboot=true USB-00' to uninstall the software.

	Note:  This product is a part of the USB-00 bundle,
	installation or removal of any component of the USB-00 bundle
	on a product-by-product level is not supported.  If the
	installation scripts indicate that the product was not
	installed because the legacy USB driver was disabled, run
	'kcmodule -s hcd=static hid=static hub=static' and reinstall
	USB-00.

	Dependencies:
	-------------
	OocCore, OocServices, OOCdio v. 1.0.0, MouseMUX v. 1.0.0,
	UsbCore.
