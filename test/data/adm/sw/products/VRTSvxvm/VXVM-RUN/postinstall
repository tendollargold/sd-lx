#! /sbin/sh
# $Id: postinstall.VXVM-RUN,v 1.8.6.5.4.10 2005/05/12 05:42:08 pradhakr Exp $
#ident "$Source: /project/unixvm-cvs/src/hp/11.23/package/vxvm/pkg_scripts/postinstall.VXVM-RUN,v $" 

# Copyright (c) 2002 VERITAS Software Corporation.  ALL RIGHTS RESERVED.
# UNPUBLISHED -- RIGHTS RESERVED UNDER THE COPYRIGHT
# LAWS OF THE UNITED STATES.  USE OF A COPYRIGHT NOTICE
# IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
# OR DISCLOSURE.
# 
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND
# TRADE SECRETS OF VERITAS SOFTWARE.  USE, DISCLOSURE,
# OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR
# EXPRESS WRITTEN PERMISSION OF VERITAS SOFTWARE.
# 
#               RESTRICTED RIGHTS LEGEND
# USE, DUPLICATION, OR DISCLOSURE BY THE GOVERNMENT IS
# SUBJECT TO RESTRICTIONS AS SET FORTH IN SUBPARAGRAPH
# (C) (1) (ii) OF THE RIGHTS IN TECHNICAL DATA AND
# COMPUTER SOFTWARE CLAUSE AT DFARS 252.227-7013.
#               VERITAS SOFTWARE
# 1600 PLYMOUTH STREET, MOUNTAIN VIEW, CA 94043

#
# The global variables SUCCESS, FAILURE, WARNING, EXCLUDE, PATH, ROOT,
# SW_CTL_SCRIPT_NAME, _pf, PRODUCT, and FILESET are all set by control_utils.
#

UTILS=/usr/lbin/sw/control_utils

if [ ! -f $UTILS ]
then
	print "ERROR:   Cannot find the sh functions library $UTILS"
	exit 1
fi
. $UTILS

# Provides the 'obsolete_clean_up' function to
# clean up filesets we are replacing

_OVERUTILS=$SW_CONTROL_DIRECTORY/overlay_utils
if [ ! -f $_OVERUTILS ]
then
	print "ERROR:   Cannot find the sh functions library $_OVERUTILS"
	exit 1
fi
. $_OVERUTILS


APM_MODULES="dmpaa dmpaaa dmpap dmpapg dmpapf dmpjbod dmphpalua dmphdsalua"

exitval=$SUCCESS

#
# Place drivers into system file.
#

if [ -z "${SW_DEFERRED_KERNBLD}" ]
then
	for driver in $APM_MODULES
	do
		mod_systemfile $SW_SYSTEM_FILE_PATH -a ${driver}
		if [ $? -ne 0 ]
		then
			print "ERROR: Cannot update $SW_SYSTEM_FILE_PATH to"
			print "       include ${driver} ($FILESET functionality)."
			exitval=$FAILURE 
		fi
	done
fi


#
# Clean up obsolete filesets that
# this one replaces if necessary
#

obsolete_clean_up VXVM-RUN


# return 0 found match
# return 1 on no match found
check_for_existing_jbod()
{
	#
	# we are checking jbod.info file manually since we can't
	# call vxddladm command at os install time.
	#
	awk -F: 'BEGIN {rc=1} \
		$1==vid && $6==pid {rc=0} \
		END {exit (rc)}' vid="$*" pid="*" /etc/vx/jbod.info
}

#
# add the following disks in jbod list so that
# these disks supports multipaths:
#	HITACHI
#	SEAGATE
#	HP 9.10G
#	HP 9.11G
#	HP 18.2G
#	HP 36.4G
#	HP 73.4G
#	HP 0146G 
#	HP 146 G 
#	FUJITSU	(use length=10 option)
#

if [ ! -f /etc/vx/jbod.info ] 
then
	> /etc/vx/jbod.info
	chmod 644 /etc/vx/jbod.info
	chown root:sys /etc/vx/jbod.info
fi

OPcode_Pagecode_Pageoffset="18:-1:36"
for disk_vendor in "HITACHI " "SEAGATE " \
	"HP 9.10G" "HP 9.11G" "HP 18.2G" \
	"HP 36.4G" "HP 73.4G" "HP 0146G" "HP 146 G" \
	"FUJITSU "
do
	check_for_existing_jbod "${disk_vendor}"
	if [ $? -gt 0 ]
	then
		SNO_length=12
		if [ "$disk_vendor" = "FUJITSU " ]
		then
			SNO_length=10
		fi

		#
		# we are adding entry to jbod.info file manually s
		# ince we can't call vxddladm command at os
		# install time.
		#
		echo "$disk_vendor:$OPcode_Pagecode_Pageoffset:$SNO_length:*:Disk" >>/etc/vx/jbod.info
	fi
done

rm -f /etc/vx/sr_port
if [ ! -f /etc/vx/vvrports ]; then
	cp /etc/vx/vvrports.template /etc/vx/vvrports
fi

if [ ! -f /etc/vx/vras/.rdg ]; then
	cp /etc/vx/vras/.rdg.template /etc/vx/vras/.rdg
fi

if [ ! -f /etc/vx/vras/vras_env ]; then
	cp /etc/vx/vras/vras_env.template /etc/vx/vras/vras_env
fi

file="/etc/inittab"
file_new="/tmp/vxinittab"

if grep '/vxvm-startup2' $file > /dev/null 2>&1
then
	mv_ok=0
	cp $file ${file_new}

	# Change vxvm-startup2 to vxvm-startup if
	# it is there
	ed - $file_new <<- '!' > /dev/null 2>&1
	/vxvm-startup2/s/vxvm-startup2/vxvm-startup/
	w
	q
	!
	if grep '/vxvm-startup2' $file_new > /dev/null 2>&1
	then
		print "ERROR:   The file $file could not be updated."
		exitval=$FAILURE
	elif mv $file_new $file > /dev/null 2>&1
	then
		mv_ok=1
	else
		print "ERROR:   The file $file could not be updated."
		exitval=$FAILURE
	fi
	if [ $mv_ok -eq 0 ]
	then
		rm -f $file_new
	fi
fi

# Building bootvold
if [ -f /etc/vx/static.d/build/bootvold.mk ]
then
	if [ -x /etc/vx/static.d/default/makevold.sh ]
	then
		/etc/vx/static.d/default/makevold.sh
		RC=$?
		if [ ${RC} -ne 0 ]
		then
			exitval=$FAILURE
		fi
	fi
fi

# Clean up udp-based CVM SG package -- 4.1 is GAB-only

for file in 				\
	VxVM-CVM-pkg.conf		\
	VxVM-CVM-pkg.sh			\
	VxVM-CVM-pkg.sh.orig		\
	VxVM-CVM-pkg-preshutdown.sh
do
	rm -f /etc/cmcluster/cvm/${file}
done

rm -f /usr/lib/vxvm/bin/vxclustd

exit $exitval


