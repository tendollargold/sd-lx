#  cstrings.py  string comparison routines
#
#   Copyright (C) 2023-2024 Paul Weber, convert to Python
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# UNIX® is a registered trademark of The Open Group.

# Change Log
# 20240106 P Weber, corrected strrchr, strtok

import ctypes
import locale
import os

# $OpenBSD: strcasecmp.c,v 1.6 2005/08/08 08:05:37 espie Exp $	*/
# Copyright (c) 1987, 1993
# The Regents of the University of California.  All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.

charmap = [
    '\000', '\001', '\002', '\003', '\004', '\005', '\006', '\007',
    '\010', '\011', '\012', '\013', '\014', '\015', '\016', '\017',
    '\020', '\021', '\022', '\023', '\024', '\025', '\026', '\027',
    '\030', '\031', '\032', '\033', '\034', '\035', '\036', '\037',
    '\040', '\041', '\042', '\043', '\044', '\045', '\046', '\047',
    '\050', '\051', '\052', '\053', '\054', '\055', '\056', '\057',
    '\060', '\061', '\062', '\063', '\064', '\065', '\066', '\067',
    '\070', '\071', '\072', '\073', '\074', '\075', '\076', '\077',
    '\100', '\141', '\142', '\143', '\144', '\145', '\146', '\147',
    '\150', '\151', '\152', '\153', '\154', '\155', '\156', '\157',
    '\160', '\161', '\162', '\163', '\164', '\165', '\166', '\167',
    '\170', '\171', '\172', '\133', '\134', '\135', '\136', '\137',
    '\140', '\141', '\142', '\143', '\144', '\145', '\146', '\147',
    '\150', '\151', '\152', '\153', '\154', '\155', '\156', '\157',
    '\160', '\161', '\162', '\163', '\164', '\165', '\166', '\167',
    '\170', '\171', '\172', '\173', '\174', '\175', '\176', '\177',
    '\200', '\201', '\202', '\203', '\204', '\205', '\206', '\207',
    '\210', '\211', '\212', '\213', '\214', '\215', '\216', '\217',
    '\220', '\221', '\222', '\223', '\224', '\225', '\226', '\227',
    '\230', '\231', '\232', '\233', '\234', '\235', '\236', '\237',
    '\240', '\241', '\242', '\243', '\244', '\245', '\246', '\247',
    '\250', '\251', '\252', '\253', '\254', '\255', '\256', '\257',
    '\260', '\261', '\262', '\263', '\264', '\265', '\266', '\267',
    '\270', '\271', '\272', '\273', '\274', '\275', '\276', '\277',
    '\300', '\301', '\302', '\303', '\304', '\305', '\306', '\307',
    '\310', '\311', '\312', '\313', '\314', '\315', '\316', '\317',
    '\320', '\321', '\322', '\323', '\324', '\325', '\326', '\327',
    '\330', '\331', '\332', '\333', '\334', '\335', '\336', '\337',
    '\340', '\341', '\342', '\343', '\344', '\345', '\346', '\347',
    '\350', '\351', '\352', '\353', '\354', '\355', '\356', '\357',
    '\360', '\361', '\362', '\363', '\364', '\365', '\366', '\367',
    '\370', '\371', '\372', '\373', '\374', '\375', '\376', '\377',
]


def strcasecmp(s1, s2):
    cm = charmap
    us1 = list(map(ord, s1))
    us2 = list(map(ord, s2))
    while cm[us1[0]] == cm[us2[0]]:
        if us1[0] == 0:
            return 0
        us1.pop(0)
        us2.pop(0)
    return cm[us1[0]] - cm[us2[0]]


def strncasecmp(s1, s2, n):
    if n != 0:
        cm = charmap
        us1 = list(map(ord, s1))
        us2 = list(map(ord, s2))
        while n != 0:
            if cm[us1[0]] != cm[us2[0]]:
                return cm[us1[0]] - cm[us2[0]]
            if us1[0] == 0:
                break
            us1.pop(0)
            us2.pop(0)
            n -= 1
    return 0


def strstr(s1, s2):
    """
      The strstr() function finds the first occurrence of the substring needle in the string haystack.
      Parameters
      ----------
      s1 : str
        String 1 to be searched (haystack).
      s2 : str
        String 2 to be compared (needle).
      Returns
      -------
      index
        -1 if not found, else index number for the beginning of the string
      """
    index = s1.find(s2)
    return index


def strcmp(s1, s2):
    """
    Compares two strings and returns an integer value.
    Parameters
    ----------
    s1 : str
      String 1 to be compared.
    s2 : str
      String 2 to be compared.
    Returns
    -------
    int
      0 if the strings are equal, 1 if s1 is greater than s2, and -1 if s1 is less than s2.
    """
    # Check if the strings are of equal length
    if len(s1) != len(s2):
        return len(s1) - len(s2)
    # Compare the characters of the strings one by one
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            return ord(s1[i]) - ord(s2[i])
    # The strings are equal
    return 0


def strncpy(s1, s2, n):
    size = min(len(s2), n)
    s1 = s2[:size] + '\0' * (n - size)
    return s1


def strncmp(s1, s2, nbytes):
    """
    The strncmp() function is similar, except it compares  only  the  first (at most) n bytes of s1 and s2.
    Parameters
    ----------
    s1 : str
      String 1 to be compared.
    s2 : str
      String 2 to be compared.
    nbytes: int
      Number of Bytes to check
    Returns
    -------
    int
      0 if the strings are equal
      1 if s1 is greater than s2
      -1 if s1 is less than s2.
    """
    # Check if the strings are of equal length by bytes

    if s1[:nbytes] == s2[:nbytes]:
        # The strings are equal
        return 0

    if s1[:nbytes] >= s2[:nbytes]:
        # Then s1 is greater than s2
        return 1
    else:
        # s1 is smaller than s2
        return -1


def strncat(s1, s2, n):
    """
    The strncat() function appends the src string to the dest string,
       overwriting the terminating null byte ('\0') at the  end  of  dest,
       and  then  adds a terminating null byte.  The strings may not over‐
       lap, and the dest string must have enough space for the result.
    Parameters
    ----------
    s1 : str
      String 1 to be compared.
    s2 : str
      String 2 to be compared.
    n: int
      Number of Bytes used from s2
    Returns
    -------
    int
      The strcat() and  strncat() functions return a pointer s to the resulting string dest.
    """
    s = s1
    # Find the end of S1.
    s1 += len(s1)
    s2 = len(s2[:n])
    s1 += '\0'
    s1 += s2[:n]
    return s


def strlen(s1):
    char_ptr = 0
    while char_ptr < len(s1) and s1[char_ptr] != '\0':
        char_ptr += 1
    return char_ptr


def strcat(dest, src):
    dest += src
    return dest


def strtok(s, delim):
    last = None
    if s is None and (s == last) is None:
        return None

    while True:
        c = s[0]
        s = s[1:]
        for spanp in delim:
            if c == spanp:
                continue

        if c == 0:
            return None
        tok = s - 1

        while True:
            c = s[0]
            s = s[1:]
            spanp = delim
            for sc in spanp:
                if sc == c:
                    if c == 0:
                        s = None
                    else:
                        s[-1] = 0
                    return tok


def strdup(string):
    siz = len(string) + 1
    copy = ctypes.create_string_buffer(siz)
    ctypes.memmove(copy, string, siz)
    return copy.value


def strpbrk(s1, s2):
    """
    The strpbrk() function locates the first occurrence in the string s1 of any bytes in the s2.
    Parameters
    ----------
    s1 : str
      String 1 to be compared.
    s2 : str
      String 2 bytes to be compared.
    Returns
    -------
    int
     """
    c = 0
    sc = 0
    while c != 0:
        c = s1[0]
        s1 = s1[1:]
        for ch in s2:
            if ch == sc:
                return s1 - 1
    return None


def strchr(s, ch):
    """
    The strchr() function returns a pointer to the first occurrence of the character ch in the string s.
     """
    c = ch
    while True:
        if s == c:
            return str(s)
        if s == '\0':
            return None
        s += 1


def strcpy(dest, src, n):
    return memcpy(dest, src, n)


def strrchr(s, ch):
    """
     The strrchr() function returns a pointer to the last occurrence of the character ch in the string s.
     """
    save = None
    for char in s:
        if char == ch:
            save = char
        if not char:
            return save


def strerror(errno):
    buf = os.strerror(errno)
    return buf


def stpcpy(dest, s1):
    return memcpy(dest, s1, strlen(s1) + 1) + strlen(s1)


# Memory workarounds
def memcmp(vl, vr, n):
    l = vl
    r = vr
    for _ in range(n):
        if n and l == r:
            n -= 1
            l += 1
            r += 1
    return ord(l) - ord(r)


# another conversion of memcmp
def memcmp2(vl, vr, n):
    return vl[:n] != vr[:n]


def memcpy(dst, src, n):
    if isinstance(dst, bytes) and isinstance(src, bytes):
        dst = bytearray(dst)
    dst[:n] = src[:n]
    return dst


def strtol_l(nptr, endptr, base, loc):
    s = nptr
    c = ""
    # Skip white space and pick up leading +/- sign if any
    # When base is 0, allow 0x for hex and 0 for octal, else
    # assume decimal; if base is already 16, allow 0x
    while s.isspace():
        c = s
        s += 1

    if c == '-':
        neg = 1
        c = s
        s += 1
    else:
        neg = 0
        if c == '+':
            c = s
            s += 1

    if (base == 0 or base == 16) and c == '0' and (s == 'x' or s == 'X') and (
            (s[1] >= '0' and s[1] <= '9') or (s[1] >= 'A' and s[1] <= 'F') or (s[1] >= 'a' and s[1] <= 'f')):
        c = s[1]
        s += 2
        base = 16

    if base == 0:
        base = 8 if c == '0' else 10

    acc = any = 0

    if base < 2 or base > 36:
        return 0

    cutoff = ctypes.c_long(-(ctypes.c_long.min + ctypes.c_long.max) + ctypes.c_long.max)
    cutlim = cutoff % base
    cutoff /= base

    while True:
        if '0' <= c <= '9':
            c = ord(s[0]) - ord('0')
        elif 'A' <= c <= 'Z':
            c = ord(s[0]) - ord('A') + 10
        elif 'a' <= c <= 'z':
            c = ord(s[0]) - ord('a') + 10
        else:
            break

        if c >= base:
            break

        if any < 0 or acc > cutoff or (acc == cutoff and c > cutlim):
            any = -1
        else:
            any = 1
            acc *= base
            acc += c

    if any < 0:
        acc = ctypes.c_long.min if neg else ctypes.c_long.max
    elif not any:
        return 0
    elif neg:
        acc = -acc

    if endptr is not None:
        endptr = any if s - 1 else nptr

    return acc


def strtol(nptr, endptr, base):
    return strtol_l(nptr, endptr, base, locale.getlocale()[0])

def atol(str1):
    return strtol(str1, None, 10)