# phases.swpackage.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals


"""
    4.0 Execution Phase Systems Management: Distributed Software Administration
    swpackage utility Page 111 - 114

    Check for extended options
    Extended Options
    The swpackage utility supports the following extended options. The description in
    Chapter 3 applies.
"""

swpackageoptions={
    'distribution_target_directory': "/var/spool/sw",
    'distribution_target_serial': "/dev/rmt0m",
    'enforce_dsa': bool(True),
    'follow_symlinks': bool(False),
    'logfile': "/var/adm/sw/swpackage.log",
    'loglevel': "1",
    'media_capacity': "0",
    'media_type': "directory",
    'psf_source_file': "psf",
    'reinstall_files': bool(False),
    'reinstall_files_use_cksum':bool(True),
    'software': "",
    'verbose': "1"
}

def swpackage_options():
    """Collection of options for swpackage utility. api"""
    swpackageoptions.update(globals())


# The swpackage utility consists of three phases:
#  1. Selection Phase
#  2. Analysis Phase
#  3. Execution Phase

#
# Selection Phase
#

#  · Specifying Targets

#    The target selection differs from the general information in Chapter 3, in that 
#    there may be only one target. If the target is a serial distribution, swpackage 
#    sets default tape types and sizes as described in the ‘‘extended options’’ 
#    description in this man-page definition.

#  · Specifying the Source

#    The source selection differs from the general information in section Chapter 3, 
#    in that the source is a PSF, instead of a distribution. Hence there are no 
#    access control events for accessing the PSF.

#    The selection phase reads (and parses) the PSF to obtain the information from the
#    source PSF.

#     — The product, subproduct, and fileset structure
#     — The files contained in each fileset
#     — The attributes associated with these objects.

#    If the file parsing discovers syntax errors, or missing but required attributes, 
#    generate an event.
#    [SW_ERROR: SW_SOURCE_ACCESS_ERROR]

#  · Software Selections

#    If there are no software selections specified, then all software from the PSF is 
#    processed.  Otherwise, each selection added to the selected software list must 
#    satisfy the following validation checks. If any of these checks result in an 
#    error, the selection is not added to the list.

#    — If the selection is not available from the PSF, generate an event.
#      [SW_ERROR: SW_SELECTION_NOT_FOUND]

#    — if a unique version can not be identified, generate an event.
#      [SW_ERROR: SW_SELECTION_NOT_FOUND_AMBIG]

#
# Analysis Phase
#

# The package analysis phase follows the following steps:

#  · Checks the dependency specifications for irregularities (such as circular 
#    prerequisites or missing dependencies).
#    (SW_WARNING: SW_DEPENDENCY_NOT_MET)

#  · Before a new storage directory is created, swpackage checks to see if this 
#    product version has the same identifying attributes as an existing product 
#    version, namely the same tag, revision, architecture, and vendor_tag. If all the 
#    identifying attributes match, then the user is repackaging (modifying) an 
#    existing version. Note if a fileset within that product is being repackaged by 
#    generating an event.
#    [SW_NOTE: SW_SAME_REVISION_INSTALLED]

#  · Checks existence and attributes of the control_files and files that the PSF 
#    defines. If any are missing, generate an event.
#    [SW_ERROR: SW_FILE_NOT_FOUND]

#  · Check that there is enough free disk space on the target file system to package 
#    the selected products. If enforce_dsa=true, generate an event.
#    [SW_ERROR: SW_DSA_OVER_LIMIT]

#    If enforce_dsa=false , generate an event.
#    [SW_WARNING: SW_DSA_OVER_LIMIT]

#
#  Execution Phase
#

#  The execution phase packages the source files and information into a product, and 
#  create/merge the product into the target distribution.

#  When creating a serial distribution, an implementation must support one or both of 
#  POSIX.1 extended cpio or extended tar archive formats. Whether an implementation 
#  supports writing both archive formats or only one, and which format is supported 
#  if only one, is implementation defined.

#  When packaging a product, the storage directory within the target distribution is
#  created/updated directly by swpackage. For each unique version of the product, a 
#  directory is created using the defined product.tag attribute and a unique sequence 
#  number for all the product versions which use the same tag as specified in 
#  Chapter 5.

#  The swpackage command generates certain attributes as specified in Section 5.2 on 
#  page 130.

#  More complex rules apply when packaging attributes that inherit from the product to
#  the fileset level. The filesets and is_locatable attributes are updated only by 
#  swpackage and swmodify. When packaging, the value of the filesets attribute is set 
#  to include all the filesets in the PSF, plus any others that exist in the 
#  distribution already but are not in the PSF. In the latter case, the user is warned
#  that the PSF is not complete.

#  If undefined, the product.is_locatable attribute is set by swpackage if any of the 
#  filesets in the filesets list are locatable. If none of the filesets are locatable,
#  or if that cannot be determined, then the value of the product.is_locatable 
#  attribute is set to false . If defined, the product.is_locatable attribute is used 
#  to define the value of the is_locatable attribute for any filesets that do not have
#  is_locatable defined. If the value of the is_locatable attribute is defined at both
#  the product and fileset level, then the fileset definition overrides the product 
#  definition.

#  Certain errors can occur when packaging the files:

#  · If a file can not be added to the distribution for any reason, generate an event.
#    [SW_ERROR: SW_FILE_ERROR]

# Just as is done for swcopy, swpackage will support the reinstall_files and 
#  reinstall_files_use_cksum options. The semantics are the same.

#  When packaging a sparse fileset, only the files contained within the sparse fileset
#  are included in the package definition. If an ancestor is not defined, then this 
#  sparse fileset can be applied to any fileset with the same tag, product tag, 
#  architecture, vendor_tag, and with a lower revision.
