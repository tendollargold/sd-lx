#!	/sbin/sh
########
# Product: SW-DIST
# Fileset: SD-CMDS
# configure
# @(#) $Revision: 5.11 $
########
#
# (C) Copyright 1993-2007 Hewlett-Packard Development Company, L.P.
#


#
# Create_dirs
#
# Makes directories:
#	/var/adm/sw/ui/preferences:	User preferences for the UI
#	/var/adm/sw/targets:		User defined targets
#	/var/adm/sw/software:		User defined software
# 
Create_dirs()
{
    for dir in	\
	${ROOT}var/adm/sw/ui/preferences 	\
	${ROOT}var/adm/sw/targets		\
	${ROOT}var/adm/sw/software
    do
	if [[ ! -d  $dir ]]; then
	    mkdir -p $dir
	    retval=$?
	    [[ $exitval -eq $SUCCESS ]] && exitval=$retval
	fi
    done

} # Create_dirs()

 
#
# OpenView_Registration
#
# Installs the Openview files into place if this is an OpenView installation.
# 
OpenView_Registration()
{

    #
    # Set up some location variables.
    #
    INSTALL_DIR=${ROOT}usr/lib/sw/openview   # where we install to
    REG_DIR=${ROOT}etc/opt/OV/share/registration/C   # where sd.reg ends up
    HELP_DIR=${ROOT}etc/opt/OV/share/help/C	     # where help files end up

    ##
    ##   Return immediately if this is an SD-UX install.
    ##
    if [ ! -f ${INSTALL_DIR}/sd.reg ]; then
	return
    fi

    ##
    ##   If OpenView appears to be installed, install the OpenView registration
    ##   file for SD. Also add this new file to the IPD.
    ##
    if [ -d ${REG_DIR} ]; then
        cp_set 444 2 2 ${INSTALL_DIR}/sd.reg ${REG_DIR}/sd.reg
        if [ $? -ne 0 ]; then
    	    echo "ERROR:   Could not install the Software Distributor OpenView"
	    echo "         Registration file.  The file \"${INSTALL_DIR}/sd.reg\""
	    echo "         needs to be copied to \"${REG_DIR}/sd.reg\"."
        else
	    IPD_addfile ${REG_DIR}/sd.reg
        fi
    fi
    
    ##
    ##   Install OpenView help files for SD, and ensure they have the right
    ##   permissions.
    ##
    if [ -d ${HELP_DIR}/ ]; then
        (
	    mkdir -p ${HELP_DIR}/sd/OVW &&
	    cp -r ${INSTALL_DIR}/help/* ${HELP_DIR}/sd/OVW/ &&
	    chmog 555 bin bin `find ${HELP_DIR}/sd -type d` &&
	    chmog 444 bin bin `find ${HELP_DIR}/sd -type f`
        ) > /dev/null 2>&1

        if [ $? -ne 0 ]; then
    	    echo "ERROR:   Could not install the Software Distributor OpenView"
            echo "         Help files.  The files under \"${INSTALL_DIR}/help\""
            echo "         need to be installed into \"${HELP_DIR}/sd/OVW/\"."
        else
	    for file in `find ${HELP_DIR}/sd -type f -print`; do
	        IPD_addfile $file
	    done
        fi
    fi

} # OpenView_Registration()




## ServiceControl_Configure
## integrated SD into ServiceControl by calling service control configure
ServiceControl_Configure()
{

    ## Run the configure script.  It may generate SD log format messages
    if [ -f /usr/lib/sw/mx/configure ]
    then
        /usr/lib/sw/mx/configure
        if [ $? != 0 ]
        then
	    echo "WARNING: Could not properly configure SD into ServiceControl."
	    echo "         You may try again later by running /usr/lib/sw/mx/configure."
	    exitval=$WARNING
        fi
    fi

} # ServiceControl_Configure()

##
##  JAGad08200, JAGad02607
##  Remove obsolete SDOV and backport bundles if installing on 11.11
##
Cleanup_Bundles()
{
    rel_major=$(echo $(uname -r) | cut -f2 -d.)
    rel_minor=$(echo $(uname -r) | cut -f3 -d.)
    if [ "$rel_major" -ge "11" ]
    then
	if [ "$rel_minor" -ge "11" ]
	then
	    # We are at B.11.11 or greater so
	    # remove these 11.0 bundles:
	    # SW-DIST-SC: B6818AA.SW-DIST-SC.SD-DEPOT
	    # SD-OV: J1370AA, J1371AA
	    # IUX BP: Ignite-SD-BP.SW-DIST-BP.SD-BP

            # see if real products exist (ERRORs to /dev/null)
            /usr/sbin/swlist SW-DIST-SC SW-DIST-BP 2>/dev/null | \
                egrep SW-DIST-SC\|SW-DIST-BP >/dev/null
           if [ $? = 0 ]
           then

                # Backport products exist.
	        # Actually swremove the "backport" products, since they 
	        # are essentially empty at this point (the depot file
	        # has been unpacked as part of configure scripts).
                # Turn off agent logging, but redirect stdout
                # to a "controller" log (loglevel=0 turns off both logs).
    
	        # remove tmp log file if exists
	        rm -f /var/tmp/swremove.log.$$

	        # remove the products
	        /usr/sbin/swremove -x loglevel=0 SW-DIST-SC SW-DIST-BP \
                   >/var/tmp/swremove.log.$$ 2>&1

	        # check result and cleanup
	        if [ $? != 0 ]
	        then
		    echo "NOTE:    Failed to remove obsolete products."
		    cat /var/tmp/swremove.log.$$
	        fi

		# remove tmp log file if exists
		rm -f /var/tmp/swremove.log.$$

            fi
        
	    # conditionally delete any bundle wrappers if there
	    IPD_delBundleWrapper B6818AA J1370AA J1371AA Ignite-SD-BP

	fi
    fi
}

Cleanup_SDUPDATE()
{
    swlist SW-DIST.SD-UPDATE @ $SW_ROOT_DIRECTORY  >/dev/null 2>&1

    if [[ $? -eq 0 ]]
    then
       swmodify -u SW-DIST.SD-UPDATE  @ $SW_ROOT_DIRECTORY  >/dev/null 2>&1
    fi

    # 
    # None of the following files are delivered by the current Update-UX
    # or by SW-DIST 
    #

    for file in $SW_ROOT_DIRECTORY/usr/sbin/install-sd   \
                $SW_ROOT_DIRECTORY/usr/sbin/changeprop   \
                $SW_ROOT_DIRECTORY/usr/sbin/disable_lock \
                $SW_ROOT_DIRECTORY/usr/sbin/enable_lock  \
                $SW_ROOT_DIRECTORY/usr/sbin/setprop
    do
        if [[ -f $file  ]]
        then
            rm -f $file
        fi
    done
}

##
##  JAGaf76375
##  Configure the SD Provider if 11.11 and WBEM Services is configured.
##
SDProvider_Configure()
{
    ERROR_LOG=/var/tmp/log$$
    INTEROPNS=root/PG_InterOp
    PROVIDERNS=root/cimv2
    SD_LIB_INSTALLED_LOC=/usr/lib/sw/wbem
    SD_LIB=libSDProvider
    SD_MOF_LOC=/usr/lib/sw/wbem
    SD_PROVIDER_MODULE=SDProviderModule
    SUFFIX=sl
    WBEM_HOME=/opt/wbem
    WBEM_SERVICES="WBEMServices.WBEM-CORE"

    # Create the link to the SD library after making sure the library exists.
    if [ -x $SD_LIB_INSTALLED_LOC/$SD_LIB.1 ]; then
          ln -s $SD_LIB_INSTALLED_LOC/$SD_LIB.1 $WBEM_HOME/providers/lib/$SD_LIB.$SUFFIX > /dev/null 2>&1
    else
          msg NOTE "$SD_LIB_INSTALLED_LOC/$SD_LIB.1 does not exist or does not have the correct permissions.  \
             The library should have been installed as part of SW-DIST.SD-CMDS."
          return
    fi

    # The provider's MOF files can only be compiled if WBEMServices is configured.
    # If WBEMServices is not configured, then the WBEMServices.WBEM-CORE's configure
    # script will compile the MOFs when the fileset is configured later in the session.

    # Determine if WBEMServices is configured.
    swlist -l fileset -a state $WBEM_SERVICES 2> /dev/null |grep configured > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        # WBEMServices is configured

        # Make sure the error log file doesn't exist.
        rm -f ${ERROR_LOG}

        # Start the cimserver if it's not running.  It must be running to register
        # the SD provider.
        PROC=$(ps -ef | grep cimserver$ | grep -v "grep cimserver$" | awk '{print($2)}')
        if [[ "$PROC" = "" ]]; then
            # It's not running so start it.
            $WBEM_HOME/sbin/cimserver > ${ERROR_LOG} 2>&1; sleep 10

            # Verify the cimserver started successfully.
            PROC=$(ps -ef | grep cimserver$ | grep -v "grep cimserver$" | awk '{print($2)}')
            if [[ "$PROC" = "" ]]; then
                msg NOTE "The cimserver could not be started so the SD Provider cannot be configured.  See the next line for more detail."
                msg BLANK `cat ${ERROR_LOG}`
                rm -f ${ERROR_LOG}
                return
            fi
        fi

        # Remove the SD Provider if it already exists.
        $WBEM_HOME/bin/cimprovider -r -m $SD_PROVIDER_MODULE > /dev/null 2>&1

        # Compile the SD Provider's MOF into the CIM Repository.
        $WBEM_HOME/bin/cimmof -I$SD_MOF_LOC -n$PROVIDERNS $SD_MOF_LOC/PG_SoftwareDistributor20.mof > ${ERROR_LOG} 2>&1
        if [ $? -ne 0 ]; then
            msg NOTE "The SD Provider's MOF could not be compiled into the CIM Repository so the provider is not available.  See the next line for more detail."
            msg BLANK `cat ${ERROR_LOG}`
            rm -f ${ERROR_LOG}
            return
        fi

        # Compile the SD Provider's registration MOF into the CIM Repository.
        #
        # JAGag11136: The 2.5 MOF is now also delivered for 11.11.  Use it if
        # the 2.5 version of WBEM Services is installed.
        #
        # Obtain the version of WBEMServices that's installed.
        version=$(get_sw_rev WBEMServices)
        major=`echo ${version} | awk '{print $1}'`
        minor=`echo ${version} | awk '{print $2}'`

        # Determine if the version is 2.5 or newer.
        if [[ ${major} -gt 2 ]] || ( [[ ${major} -eq 2 ]] && [[ ${minor} -ge 5 ]] )
        then 
            # It's 2.5 or newer so use the 2.5 registration MOF.
            SD_MOF_FILE=${SD_MOF_LOC}/PG_SoftwareDistributor20R_2.5.mof
        else 
            # It's an older version so the older MOF must be used.
            SD_MOF_FILE=${SD_MOF_LOC}/PG_SoftwareDistributor20R.mof
        fi      

        # Compile the MOF.
        $WBEM_HOME/bin/cimmof -I$SD_MOF_LOC -n$INTEROPNS ${SD_MOF_FILE} > ${ERROR_LOG} 2>&1
        if [ $? -ne 0 ]; then
            msg NOTE "The SD Provider's registration MOF, \"${SD_MOF_FILE}\", could not be compiled into the CIM Repository so the provider is not registered.  See the next line for more detail."
            msg BLANK `cat ${ERROR_LOG}`
            rm -f ${ERROR_LOG}
            return
        fi

        msg NOTE "The SD Provider registered with \"${SD_MOF_FILE}\"."

        rm -f ${ERROR_LOG}

    fi # WBEMServices is configured

}

########################################################################
# MAIN

##
##   Grab the standard control_utils.
##
UTILS=/usr/lbin/sw/control_utils
if [ -f $UTILS ]; then
    . $UTILS
else
    echo "ERROR:   Cannot find $UTILS"
    exit 1
fi
exitval=$SUCCESS


##
##   Create some directories we'll need later.
##
Create_dirs


##
##   Set up our newconfig files.
##
## JAGag32093: newconfig_cp moved to postinstall.

#
# extract revision from SW_SOFTWARE_SPEC
#
revision=`echo $SW_SOFTWARE_SPEC | sed -e's/.*r=//' | sed -e's/,.*//'`

major=`echo $revision | awk -F. '{print $2}'`
minor=`echo $revision | awk -F. '{print $3}'`

if [[  $major  -lt 11 ]] || ( [[  $major  -eq 11 ]] &&  [[ $minor -lt 23  ]] )
then

    ##   SD and SD-OV are the same for 11.23 and above.
    ##   Do not need OpenView Registration for 11.23 and above.
    ##
    ##   For releases prior to 11.23, 
    ##   register SD with OpenView if this is SD-OV and OpenView is present.
    ##
    OpenView_Registration
fi

##
## JAGac42613: Remove Enable_Protected() function that checks for daemon
## licensing, as that functionality has been removed.
##


##
##  Integrate SD into ServiceControl
##
ServiceControl_Configure


##
##  JAGad08200, JAGad02607
##  Remove obsolete SDOV and backport bundles if installing on 11.11
##
Cleanup_Bundles

if [[  $major  -gt  11 ]] || ( [[  $major  -eq 11 ]] &&  [[ $minor -ge 22  ]] )
then

    ##   
    ##   Clean up obsolete SW-DIST.SD-UPDATE fileset.
    ##   It has been replaced by Update-UX product.
    ##   
    ##
    Cleanup_SDUPDATE
fi

##
##  JAGaf76375
##  Configure the SD Provider if 11.11 and WBEM Services is configured.
##  The provider is configured by the SD-PROVIDER fileset in
##  releases after 11.11.
##
if ( [[  $major  -eq 11 ]] &&  [[ $minor -eq 11  ]] )
then
    SDProvider_Configure
fi

##
##   Exit.
##
exit $exitval
