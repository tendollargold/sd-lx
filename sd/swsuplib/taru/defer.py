
# defer.py- handle "defered" links in newc and crc archives
#   Copyright (C) 1999 Jim Lowe
#   Copyright (C) 2023 - 2024 Paul Weber, conversion to Python
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# UNIX� is a registered trademark of The Open Group.

from sd.swsuplib.taru.ahs import ahsstaticsetpaxlinkname, ahsstaticcreatefilehdr
from sd.swsuplib.taru.ahs import ahsstaticsettarfilename, ahsstaticdeletefilehdr
from sd.swsuplib.taru.mtar import taru_filehdr2filehdr
from sd.swsuplib.taru.copyout import taru_write_out_header, taru_write_archive_member_data

class Deferment:
    def __init__(self):
        self.headerP = None
        self.nextP = None

class DEFER:
    def __init__(self):
        self.format = 0
        self.deferouts = None
        self.taru = None

def defer_open(cformat):
    defer = DEFER()
    defer.format = cformat
    defer.deferouts = None
    defer.taru = None
    return defer

def defer_close(def_obj):
    d = def_obj.deferouts
    while d:
        oldd = d
        d = d.nextP
        def_free_deferment(oldd)
    del def_obj

def defer_set_taru(defer, taru):
    defer.taru = taru

def defer_set_format(defer, cformat):
    defer.format = cformat

def defer_is_last_link(def_obj, file_hdr):
    other_files_sofar = def_count_defered_links_to_dev_ino(def_obj, file_hdr)
    if file_hdr.c_nlink == (other_files_sofar + 1):
        return 1
    return 0

def defer_add_link_defer(defer, file_hdr):
    new_deferment = Deferment()
    new_deferment.headerP = file_hdr
    new_deferment.nextP = defer.deferouts
    defer.deferouts = new_deferment

def defer_writeout_zero_length_defers(defer, file_hdr, outfd):
    if defer.deferouts is None:
        return 0
    if defer.deferouts.headerP == file_hdr:
        return 0
    return 1

def defer_writeout_final_defers(def_obj, out_des):
    ret = 0
    retval = 0
    while def_obj.deferouts and ret >= 0:
        ret = def_writeout_defers(def_obj, def_obj.deferouts.headerP, out_des, -1, 1)
        retval += ret
    if ret < 0:
        return ret
    return retval
def def_create_deferment(file_hdr):
    d = Deferment()
    d.headerP = ahsstaticcreatefilehdr()
    taru_filehdr2filehdr(d.headerP, file_hdr)
    ahsstaticsetpaxlinkname(d.headerP, None)
    return d

def def_free_deferment(d):
    ahsstaticsetpaxlinkname(d['headerP'], None)
    ahsstaticsettarfilename(d['headerP'], None)
    ahsstaticdeletefilehdr(d['headerP'])
    del d

def def_writeout_defered_file(defh, header, infd, outfd, format):
    ret = taru_write_out_header(defh.taru, header, outfd, format, None, 0)
    ret += taru_write_archive_member_data(defh.taru, header, outfd, infd, None, format, -1, None)
    return ret

def def_count_defered_links_to_dev_ino(def_, file_hdr):
    ino = int(file_hdr.c_ino)
    maj = int(file_hdr.c_dev_maj)
    cmin = int(file_hdr.c_dev_min)
    count = 0
    d = def_.deferouts
    while d is not None:
        if (int(d.headerP.c_ino) == ino) and (int(d.headerP.c_dev_maj) == maj) and (int(d.headerP.c_dev_min) == cmin):
            count += 1
        d = d.nextP
    return count


def def_writeout_defers(def_obj, file_hdr, out_des, infd, is_final):
    retval = 0
    d_prev = None
    d = def_obj.deferouts
    while d:
        if d.headerP.c_ino == file_hdr.c_ino and d.headerP.c_dev_maj == file_hdr.c_dev_maj and d.headerP.c_dev_min == file_hdr.c_dev_min:
            remaining_links = def_count_defered_links_to_dev_ino(def_obj, d.headerP)
            if is_final == 0 or (is_final == 1 and remaining_links > 1):
                d.headerP.c_filesize = 0
                ret = taru_write_out_header(def_obj.taru, d.headerP, out_des, def_obj.format, None, 0)
                retval += ret
            elif is_final == 1 and remaining_links == 1:
                ret = def_writeout_defered_file(def_obj, d.headerP, infd, out_des, def_obj.format)
                retval += ret
            if d_prev:
                d_prev.nextP = d.nextP
            else:
                def_obj.deferouts = d.nextP
            d_free = d
            d = d.nextP
            def_free_deferment(d_free)
        else:
            d_prev = d
            d = d.nextP
    return retval


# ----------- Public Routines ------------------------
