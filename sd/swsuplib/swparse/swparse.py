# swparse.py
# includes translated c-code from swyyparse and swparse_supp1

#  Copyright (C) 1998  James H. Lowe, Jr.  <jhlowe@acm.org>
#  Copyright (c) 2023 Paul Weber convert to python

#  COPYING TERMS AND CONDITIONS
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
import re
import sys
from sepolgen.classperms import txt

from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlib_basename,  swlib_process_hex_escapes
from sd.swsuplib.strob import strob_open, strob_strcpy, strob_strcat, strob_close
from sd.swsuplib.misc.swdef import swdef_write_attribute, swdef_write_keyword
from sd.swsuplib.misc.swlib import swlib_expand_escapes
from sd.swsuplib.uxfio import uxfio_write


def yyparse():
    pass

class SwparseConstants:
    SWPARSE_ACMD_COPY = 0
    SWPARSE_ACMD_CAT = 1
    SWPARSE_ACMD_EMIT = 2
    SWPARSE_SWDEF_FILETYPE_INFO = 0
    SWPARSE_SWDEF_FILETYPE_INDEX = 1
    SWPARSE_SWDEF_FILETYPE_PSF = 2
    SWPARSE_SWDEF_FILETYPE_INSTALLED = 3
    SWPARSE_MKUP_LEN_WIDTH = 4
    SWPARSE_MKUP_LEN_WIDTH_C = "4"
    SWPARSE_MD_TYPE_COMMENT = '#'
    SWPARSE_MD_TYPE_ATT = 'A' # attribute keyword
    SWPARSE_MD_TYPE_EXT = 'E' # extended keyword
    SWPARSE_MD_TYPE_OBJ = 'O' # object keyword
    SWPARSE_MD_TYPE_DATA = 'D' # file data of the 'F' file reference
    SWPARSE_MD_TYPE_FILEREF = 'F' # file name reference
    SWPARSE_OFFSET_LEN = 0 # offset in line to the length of the value
    SWPARSE_MKUP_RES = 30
    SWPARSE_ILOC_OFF = 0
    SWPARSE_ILOC_FILE = 1
    SWPARSE_ILOC_CONTROL_FILE = 2
    SWPARSE_ILOC_FILESET = 3
    SWPARSE_ILOC_PRODUCT = 4
    SWPARSE_ILOC_DISTRIBUTION = 5
    SWPARSE_ILOC_BUNDLE = 6
    SWPARSE_EMITTED_TYPE_ATT = 'A'
    SWPARSE_EMITTED_TYPE_EXT = 'E'
    SWPARSE_EMITTED_TYPE_OBJ = 'O'
    SWPARSE_FORM_MKUP = (1 << 0)  # default markup
    SWPARSE_FORM_MKUP_LEN = (1 << 1)  # include attribute lengths in output
    SWPARSE_FORM_INDENT = (1 << 2)  # beautify only
    SWPARSE_FORM_POLICY_POSIX_IGNORES = (1 << 3)  # ignore mtime as Std requires
    SWPARSE_FORM_ALL = (SWPARSE_FORM_MKUP | SWPARSE_FORM_MKUP_LEN | SWPARSE_FORM_INDENT)
    SWPARSE_LEVEL_IVAL_MIN = -9
    SWPARSE_LEVEL_IVAL_MAX = 9
    SWPARSE_UNKNOWN_EXTRA = 40
    SW_ATTRIBUTE_KEYWORD = 258
    SW_EXT_KEYWORD = 259
    SW_RPM_KEYWORD = 260
    SW_OBJECT_KEYWORD = 261
    SW_NEWLINE_STRING = 262
    SW_OK_NEWLINE_STRING = 263
    SW_TERM_NEWLINE_STRING = 264
    SW_PATHNAME_CHARACTER_STRING = 265
    SW_SHELL_TOKEN_STRING = 266
    SW_WHITE_SPACE_STRING = 267
    SW_OPTION_DELIM = 268
    SW_EXT_WHITE_SPACE_STRING = 269
    SW_OPTION = 270
    SW_INDEX = 271
    SW_INSTALLED = 272
    SW_INFO = 273
    SW_PSF = 274
    SW_PSF_INCL = 275
    SW_SWINFO = 276
    SW_LEXER_EOF = 277
    SW_LEXER_FATAL_ERROR = 278
    SW_OK_HOST = 279
    SW_OK_DISTRIBUTION = 280
    SW_OK_INSTALLED_SOFTWARE = 281
    SW_OK_BUNDLE = 282
    SW_OK_PRODUCT = 283
    SW_OK_SUBPRODUCT = 284
    SW_OK_FILESET = 285
    SW_OK_CONTROL_FILE = 286
    SW_OK_FILE = 287
    SW_OK_VENDOR = 288
    SW_OK_MEDIA = 289
    SW_AK_LAYOUT_VERSION = 290
    SW_OK_CATEGORY = 291
    # SW_YYPARSE = yyparse()
    
swpc = SwparseConstants

yydebug = 0
swlex_yacc_feedback_fileset = 0
swlex_yacc_feedback_file_object = 0
swlex_yacc_feedback_controlfile_object = 0
swlex_yacc_feedback_directory = 0
swlex_filename = ""
swparse_outputfd = 0
swparse_atlevel = 0
swparse_form_flag = 0
swparse_swdef_filetype = 0
swparse_strob_ptr = None
swparse_strob_ptr_len = 0
swlex_definition_file = None
swlex_debug = None
swlex_inputfd = None
swlex_errorcode = None
swlex_linenumber = None

did_see_vendor_misleading = 0
location = 0
swparse_keytype = ''
swparse_leading_whitespace = "\x20\x20\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x20" \
                            "\x20\x20\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x20" \
                            "\x20\x20\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x20" \
                            "\x20\x20\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x20" \
                            "\x20\x20\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x20" \
                            "\x20\x20\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x20" \
                            "\x20\x20\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x2\x20\x20" \
                            ""

swws = None
global do_not_warn_utf
# do_not_warn_utf = 1
tokentype = {
     'SW_ATTRIBUTE_KEYWORD': 258,
     'SW_EXT_KEYWORD': 259,
     'SW_RPM_KEYWORD': 260,
     'SW_OBJECT_KEYWORD': 261,
     'SW_NEWLINE_STRING': 262,
     'SW_OK_NEWLINE_STRING': 263,
     'SW_TERM_NEWLINE_STRING': 264,
     'SW_PATHNAME_CHARACTER_STRING': 265,
     'SW_SHELL_TOKEN_STRING': 266,
     'SW_WHITE_SPACE_STRING': 267,
     'SW_OPTION_DELIM': 268,
     'SW_EXT_WHITE_SPACE_STRING': 269,
     'SW_OPTION': 270,
     'SW_INDEX': 271,
     'SW_INSTALLED': 272,
     'SW_INFO': 273,
     'SW_PSF': 274,
     'SW_PSF_INCL': 275,
     'SW_SWINFO': 276,
     'SW_LEXER_EOF': 277,
     'SW_LEXER_FATAL_ERROR': 278,
     'SW_OK_HOST': 279,
     'SW_OK_DISTRIBUTION': 280,
     'SW_OK_INSTALLED_SOFTWARE': 281,
     'SW_OK_BUNDLE': 282,
     'SW_OK_PRODUCT': 283,
     'SW_OK_SUBPRODUCT': 284,
     'SW_OK_FILESET': 285,
     'SW_OK_CONTROL_FILE': 286,
     'SW_OK_FILE': 287,
     'SW_OK_VENDOR': 288,
     'SW_OK_MEDIA': 289,
     'SW_AK_LAYOUT_VERSION': 290,
     'SW_OK_CATEGORY': 291
}


def icmp(path, name):
    s = swlib_basename(None, path)
    if s:
        ret = s == name
    else:
        ret = -1
    return ret

def sw_yyparse(uxfio_ifd, uxfio_ofd, metadata_filename, atlevel, mark_up_flag):
    global swlex_definition_file, swlex_linenumber, swparse_outputfd, swlex_inputfd, swparse_atlevel, swparse_form_flag, swlex_errorcode
    if icmp(metadata_filename, "INFO") == 0:
        swlex_definition_file = swpc.SW_INFO
    elif icmp(metadata_filename, "INDEX") == 0:
        swlex_definition_file = swpc.SW_INDEX
    elif icmp(metadata_filename, "INSTALLED") == 0:
        swlex_definition_file = swpc.SW_INSTALLED
    elif icmp(metadata_filename, "OPTION") == 0:
        swlex_definition_file = swpc.SW_OPTION
    elif icmp(metadata_filename, "PSF") == 0:
        swlex_definition_file = swpc.SW_PSF
    elif icmp(metadata_filename, "psf") == 0:
        swlex_definition_file = swpc.SW_PSF
    elif icmp(metadata_filename, "PSFi") == 0:
        swlex_definition_file = swpc.SW_PSF_INCL
    else:
        sys.stderr.write("%s: bad file type selection: %s\n" % (swlib_utilname_get(), metadata_filename))
        return -1
    swlex_linenumber = 0
    swparse_outputfd = uxfio_ofd
    swlex_inputfd = uxfio_ifd
    swparse_atlevel = atlevel
    swparse_form_flag = mark_up_flag
    # swpc.SW_YYPARSE()
    return swlex_errorcode

def yywrap():
    return 1
def level_push(sw):
    global swparse_atlevel
    swparse_atlevel += 1
    return 0

def level_pop(sw):
    global swparse_atlevel
    swparse_atlevel -= 1
    return 0


def swparse_construct_attribute(strb, outputfd, src, cmd, level, s_keytype, form_flag):
    ustore = None
    l_swws = "                                       "
    ret = 0
    if level > 10:
        return -1

    if form_flag & swpc.SWPARSE_FORM_INDENT and cmd == swpc.SWPARSE_ACMD_EMIT:
        return do_indent_only(strb, outputfd, level)

    l_swws = l_swws[:level]

    if form_flag & swpc.SWPARSE_FORM_MKUP or (form_flag & swpc.SWPARSE_FORM_ALL) == 0:
        extra_len = 0
    else:
        extra_len = swpc.SWPARSE_MKUP_RES

    if cmd == swpc.SWPARSE_ACMD_COPY:
        p = src
        newlen = len(p)
        strb.str_ = p.encode()
        strb.length_ = newlen
    elif cmd == swpc.SWPARSE_ACMD_CAT:
        p = src
        strb.str_ += p.encode()
        strb.length_ = len(strb.str_)
    elif cmd == swpc.SWPARSE_ACMD_EMIT:
        ptr = strb.str_.decode()
        if not ptr:
            strb.str_ += b'"""'
            ptr = strb.str_.decode()

        ptr = swlib_process_hex_escapes(ptr)

        if utf8_valid(ptr.encode(), len(ptr)) == 0:
            if not ustore:
                ustore = strob_open(100)
            swlib_expand_escapes(None, None, ptr, ustore)
            if do_not_warn_utf == 0:
                sys.stderr.write(f"{swlib_utilname_get()}: warning: invalid UTF-8 characters: {ustore.str_.decode()}\n")

        ptr = strb.str_.decode()
        memlen = len(ptr) + level + 6 + extra_len
        eptr = bytearray(memlen)
        strb.length_ = memlen
        ptr = strb.str_.decode()

        if form_flag & swpc.SWPARSE_FORM_MKUP:
            eptr[:] = f"{s_keytype:c}{level:02d}{l_swws}{ptr}\n".encode()

        elif form_flag & swpc.SWPARSE_FORM_MKUP_LEN:
            p = ptr
            while p and not txt.isspace(p):
                p += 1
            p += 1
            value_length = len(p)
            eptr[:] = f"{value_length:>{swpc.SWPARSE_MKUP_LEN_WIDTH_C}} {s_keytype:c}{level:02d} {ptr}\n".encode()
        else:
            return -1

        ret = uxfio_write(outputfd, eptr, len(eptr))
        del eptr
    else:
        return -1

    return ret


def do_indent_only(strb, outputfd, level):
    keyword = strb.get_str()
    value = re.search(r'\s', strb.get_str())
    if value:
        value = value.group()
        value = value[1:]
        return swdef_write_attribute(keyword, value, level, 0, 'A', outputfd)
    else:
        return swdef_write_attribute(keyword, value, level, 0, 'O', outputfd)

def check_keyword(w):
    pass

def check_ignores(line, ignores):
    for ignore in ignores:
        if line.startswith(ignore):
            return 1
    return 0

def squash_non_ascii_chars(ptr):
    s = ptr
    while s:
        if ord(s) <= 127:
            pass
        else:
            s = s[1:]
        s += 1

def swparse_write_attribute_att(outputfd, key, val, level, form_flag):
    strb = strob_open(12)
    check_keyword(key)
    strob_strcpy(strb, key)
    strob_strcat(strb, " ")
    swparse_expand_n(strb.str_, len(strb.str_), val)
    strob_strcat(strb,val)
    ret = swparse_construct_attribute(strb, outputfd, None, swpc.SWPARSE_ACMD_EMIT, level, swpc.SWPARSE_MD_TYPE_ATT, form_flag)
    strob_close(strb)
    return ret

def swparse_ignore_attribute(filetype, location, line):
    ret = 0
    psf_file_ignores = ["cksum ", "compressed_cksum ", "compressed_size ", "compression_state ", "compression_type ", "size "]
    psf_fileset_ignores = ["location ", "media_sequence_number ", "size ", "state "]
    psf_bundle_ignores = ["location ", "qualifier "]
    psf_distribution_ignores = ["uuid "]
    info_file_ignores = ["source "]
    index_ignores = ["size "]
    if filetype == swpc.SWPARSE_SWDEF_FILETYPE_INFO:
        if location == swpc.SWPARSE_ILOC_FILE:
            ret = check_ignores(line, info_file_ignores)
    elif filetype == swpc.SWPARSE_SWDEF_FILETYPE_PSF:
        if location == swpc.SWPARSE_ILOC_FILE:
            ret = check_ignores(line, psf_file_ignores)
        elif location == swpc.SWPARSE_ILOC_CONTROL_FILE:
            ret = check_ignores(line, psf_file_ignores)
            if ret == 0:
                ret = not line.startswith("result ")
        elif location == swpc.SWPARSE_ILOC_FILESET:
            ret = check_ignores(line, psf_fileset_ignores)
        elif location == swpc.SWPARSE_ILOC_BUNDLE:
            ret = check_ignores(line, psf_bundle_ignores)
        elif location == swpc.SWPARSE_ILOC_DISTRIBUTION:
            ret = check_ignores(line, psf_distribution_ignores)
    elif filetype == swpc.SWPARSE_SWDEF_FILETYPE_INDEX:
        ret = check_ignores(line, index_ignores)
    return ret

def swparse_write_attribute_obj(outputfd, key, level, form_flag):
    check_keyword(key)
    if form_flag & swpc.SWPARSE_FORM_INDENT:
        return swdef_write_keyword(key, level, 'O', outputfd)
    strb = strob_open(12)
    strob_strcpy(strb, key)
    ret = swparse_construct_attribute(strb, outputfd, None, swpc.SWPARSE_ACMD_EMIT, level, swpc.SWPARSE_MD_TYPE_OBJ, form_flag)
    strob_close(strb)
    return ret

def swparse_set_do_not_warn_utf():
    do_not_warn_utf = 1

def swparse_unset_do_not_warn_utf():
    do_not_warn_utf = 0

def swparse_print_filename(buf, len, filetype, ws_level_string, lex_filename, form_flag):
    value_length = len(lex_filename)
    if form_flag & swpc.SWPARSE_FORM_MKUP:
        buf = f"F{len(ws_level_string)+1} {ws_level_string}{filetype} {lex_filename}\n"
        sys.stdout.write(buf)
    else:
        buf = f"{value_length} F{len(ws_level_string)+1} {ws_level_string} {filetype} {lex_filename}\n"
        sys.stdout.write(buf)
    return 0

def swparse_print_filename_by_fd(buf, len, fd, filetype, ws_level_string, lex_filename, form_flag):
    swparse_outputfd = fd
    return swparse_print_filename(buf, len, filetype, ws_level_string, lex_filename, form_flag)

def utf8_valid(buf, len):
    endbuf = buf + len
    byte2mask = 0x00
    trailing = 0
    while buf != endbuf:
        c = buf
        buf += 1
        if trailing:
            if (c & 0xC0) == 0x80:
                if byte2mask:
                    if c & byte2mask:
                        byte2mask = 0x00
                    else:
                        return 0
                trailing -= 1
            else:
                return 0
        else:
            if (c & 0x80) == 0x00:
                continue
            elif (c & 0xE0) == 0xC0:
                if c & 0x1E:
                    trailing = 1
                else:
                    return 0
            elif (c & 0xF0) == 0xE0:
                if not (c & 0x0F):
                    byte2mask = 0x20
                    trailing = 2
            elif (c & 0xF8) == 0xF0:
                if not (c & 0x07):
                    byte2mask = 0x30
                    trailing = 3
            elif (c & 0xFC) == 0xF8:
                if not (c & 0x03):
                    byte2mask = 0x38
                    trailing = 4
            elif (c & 0xFE) == 0xFC:
                if not (c & 0x01):
                    byte2mask = 0x3C
                    trailing = 5
            else:
                return 0
    return trailing == 0

def swlib_is_ansi_escape(c):
    return c.isprintable() and not c.isalnum()

def de_quote_it(src):
    if src[0] != '\"':
        return src
    src = src[1:]
    if not len(src):
        return src[:-1]
    src = src[:-1]
    return src

def swparse_expand_n(pa, newlen, src):
    n = len(src)
    i = 0
    j = 0
    lp = src
    store = ""
    src = de_quote_it(src)
    while i < n:
        if lp[i] == '\n':
            store += '\\n'
            j += 1
        elif lp[i] == '\\':
            if lp[i + 1] == '\\':
                store += '\\\\'
                i += 1
            elif lp[i + 1] == 'n':
                store += '\\'
                i += 1
                store += lp[i - 1]
                j += 1
                store += lp[i]
            elif lp[i + 1] == '#':
                store += '#'
                i += 1
                j -= 1
            elif lp[i + 1] == '\"':
                store += '\"'
                i += 1
                j -= 1
            elif lp[i + 1] == '\0':
                store += '\\\\'
                j += 1
            elif not swlib_is_ansi_escape(lp[i + 1]):
                store += '\\\\'
                j += 1
            else:
                store += lp[i]
        else:
            store += lp[i]
        i += 1
    return 0

