# strar.py  string array object
#
#   Copyright (C) 2002 James H. Lowe, Jr.
#   Copyright (C) 2023 Paul Weber convert to Python
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
import ctypes

from sd.swsuplib.cplob import CPLOB, cplob_open, cplob_add_nta, cplob_release, cplob_val
from sd.swsuplib.cplob import cplob_shallow_reset, cplob_additem, cplob_get_nused, cplob_remove_index
from sd.swsuplib.strob import STROB, strob_open, strob_str, strob_strcpy, strob_close, strob_set_memlength



free = ctypes.CDLL(None).free
f_strcmp = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_int, ctypes.c_void_p, ctypes.c_void_p)(("f_strcmp", ctypes.CDLL(None)))


class STRAR:
    def __init__(self):
        self.ns = 0
        self.len = 0
        self.list = CPLOB()
        self.storage = STROB()

def strar_copy_construct(src):
    ret = strar_open()
    i = 0
    while True:
        s = strar_get(src, i)
        if not s:
            break
        strar_add(ret, s)
        i += 1
    return ret

def strar_open():
    strb = ctypes.pointer(STRAR())
    strb.contents.storageM = strob_open(132)
    strb.contents.listM = cplob_open(10)
    cplob_add_nta(strb.contents.listM, None)
    return strb

def strar_reset(strar):
    strar.contents.lenM = 0
    strar.contents.nsM = 0
    strob_strcpy(strar.contents.storageM, "")
    cplob_shallow_reset(strar.contents.listM)

def strar_close(strar):
    strob_close(strar.contents.storageM)
    free(cplob_release(strar.contents.listM))
    free(strar)

def strar_num_elements(strar):
    return strar.contents.nsM

def strar_get(strar, index):
    s = cplob_val(strar.contents.listM, index)
    if index >= strar.contents.nsM or index < 0:
        return None
    return s

def strar_return_store(strar, length):
    strar.contents.lenM = strar.contents.lenM + length + 1
    oldbase = strob_str(strar.contents.storageM)
    strob_set_memlength(strar.contents.storageM, strar.contents.lenM)
    base = strob_str(strar.contents.storageM)
    strar.contents.nsM += 1
    if base != oldbase:
        index = 0
        while True:
            p = cplob_val(strar.contents.listM, index)
            if not p:
                break
            cplob_additem(strar.contents.listM, index, base + (p - oldbase))
            index += 1
    nused = cplob_get_nused(strar.contents.listM) - 1
    if nused > 0:
        last = cplob_val(strar.contents.listM, nused - 1)
    else:
        last = None
    if last:
        last = last + len(last) + 1
    else:
        last = base
    cplob_add_nta(strar.contents.listM, last)
    last[:length] = '_'
    last[length] = '\0'
    return last

def strar_add(strar, src):
    last = strar_return_store(strar, len(src))
    last[:len(src)] = src
    return 0

def strar_qsort(strar, f_comp):
    nused = cplob_get_nused(strar.contents.listM) - 1
    quicksort(strar.contents.listM.contents.list, nused, ctypes.sizeof(ctypes.c_char_p), f_comp, 0)

def strar_qsort_neg_strcmp(vf1, vf2):
    return f_strcmp(-1, vf1, vf2)
def strar_remove_index(strar, index):
    cplob_remove_index(strar.contents.listM, index)
    strar.contents.nsM -= 1

# Quickstort function converted to Python by Paul Weber
# Copyright (C) 1991-2023 Free Software Foundation, Inc.
#   This file is part of the GNU C Library.
#
#   The GNU C Library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   The GNU C Library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with the GNU C Library; if not, see
#   <https://www.gnu.org/licenses/>.

# If you consider tuning this algorithm, you should consult first:
#   Engineering a sort function; Jon Bentley and M. Douglas McIlroy
#   Software - Practice and Experience; Vol. 23 (11), 1249-1265, 1993.

def quicksort(pbase, total_elems, size, cmp, arg):
    base_ptr = pbase
    max_thresh = 4 * size
    if total_elems == 0:
        return
    if total_elems > max_thresh:
        lo = base_ptr
        hi = lo + size * (total_elems - 1)
        stack = []
        top = 0
        stack.append((None, None))
        while top >= 0:
            mid = lo + size * ((hi - lo) // size >> 1)
            if cmp(mid, lo, arg) < 0:
                mid, lo = lo, mid
            if cmp(hi, mid, arg) < 0:
                mid, hi = hi, mid
            if cmp(mid, lo, arg) < 0:
                mid, lo = lo, mid
#           jump_over:
            left_ptr = lo + size
            right_ptr = hi - size
            while True:
                while cmp(left_ptr, mid, arg) < 0:
                    left_ptr += size
                while cmp(mid, right_ptr, arg) < 0:
                    right_ptr -= size
                if left_ptr < right_ptr:
                    left_ptr, right_ptr = right_ptr, left_ptr
                    if mid == left_ptr:
                        mid = right_ptr
                    elif mid == right_ptr:
                        mid = left_ptr
                    left_ptr += size
                    right_ptr -= size
                elif left_ptr == right_ptr:
                    left_ptr += size
                    right_ptr -= size
                    break
            if right_ptr - lo <= max_thresh:
                if hi - left_ptr <= max_thresh:
                    top -= 1
                    lo, hi = stack[top]
                else:
                    lo = left_ptr
            elif hi - left_ptr <= max_thresh:
                hi = right_ptr
            elif right_ptr - lo > hi - left_ptr:
                stack.append((lo, right_ptr))
                top += 1
                lo = left_ptr
            else:
                stack.append((left_ptr, hi))
                top += 1
                hi = right_ptr
    end_ptr = base_ptr + size * (total_elems - 1)
    tmp_ptr = base_ptr
    thresh = min(end_ptr, base_ptr + max_thresh)
    run_ptr = tmp_ptr + size
    while run_ptr <= thresh:
        if cmp(run_ptr, tmp_ptr, arg) < 0:
            tmp_ptr = run_ptr
        run_ptr += size
    if tmp_ptr != base_ptr:
        tmp_ptr, base_ptr = base_ptr, tmp_ptr
    run_ptr = base_ptr + size
    while run_ptr <= end_ptr:
        tmp_ptr = run_ptr - size
        while cmp(run_ptr, tmp_ptr, arg) < 0:
            tmp_ptr -= size
        tmp_ptr += size
        if tmp_ptr != run_ptr:
            trav = run_ptr + size
            while trav >= run_ptr:
                c = trav
                hi = lo = trav
                while lo >= tmp_ptr:
                    hi, lo = lo, lo - size
                    hi = lo
                    hi = c
                trav -= 1

