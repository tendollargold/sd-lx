#!/usr/bin/python3

# swacl.py - swacl - manage SD-LX access control list

# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
cmd = os.path.split(sys.argv[0])[1]

swacl_parser = argparse.ArgumentParser(
	usage="""%(prog)s -l level [-D acl_entry|-F acl_file|-M acl_entry]
                  [-f software_file][-t target_file]
                  [-x option=value] [-X option_file] [-g]
                  [software_selections] [@ target_selection]""",
	formatter_class=argparse.RawTextHelpFormatter,
epilog=''' An acl_entry is specified as: 

 entry_type[:key]:permissions

 Software selections are specified as:

     product[,version] ...

 where version is either

     [r=revision][,a=arch][,v=vendor]
     instance_id

 Target selections are specified as:

     -T [host][:][path] ...
 
''')

swacl_parser.add_argument("-l",metavar='',
	choices=['host', 'depot', 'root', 'product', 'product_template', 'global_soc_template','global_product_template'],
    default='product',
	help="levels of objects to view, one of: 'host' 'depot', \n'root', 'product', 'product_template','global_soc_template', 'global_product_template'")

swacl_parser.add_argument("-D | --delete", metavar='acl_entry',
		    help="delete acl_entry from ACL")
swacl_parser.add_argument("-F", metavar='acl_entry',
			help="replace acl_entry with entries in this file")
swacl_parser.add_argument("-M | --modify", metavar='acl_entry',
		    help="add acl_entry to ACL or replace existing entry")
swacl_parser.add_argument("-f | --software-file", metavar='software_file',
		    help="read software selections from this file")
swacl_parser.add_argument("-x option=value", metavar='', 
		    help="set the option to value")
swacl_parser.add_argument("-X | --option-file", metavar='option_file', 
		    help="read option definitions from this file")
# -g --gnu option added for GNU Copyleft
swacl_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")
swacl_parser.add_argument("software_selections", nargs='*',
 		  help="software selections: XServer.MAN ...")
# B.3.4 page 188 Using -T vs @ for target selection as it is easier in argparse. :)
swacl_parser.add_argument("-T", nargs="+",metavar="[host][:][path]",
			  help="Target selections specified as: [host][:][path] \n default is localhost:/")
if len(sys.argv) > 1:
  print( "~ Command: " + sys.argv[0] )
  print( "~ Args: " + sys.argv[1] )
else:
  print( 'ERROR:   A valid edit level is needed for the successful execution of \n         "swacl"(). The valid levels are:"host", "depot", "root", \n         "product", "product_template", "global_product_template" or\n         "global_soc_template".')
  sys.exit()

swacl_parser.parse_args()
args = swacl_parser.parse_args()
#print "DEBUG: Args in: ", cmd,".py ",  args
