#!/usr/bin/env python3

from tkinter import *

root = Tk()
from datetime import datetime
import argparse
import os
import sys
import getpass
import socket
import time

cmd = os.path.split(sys.argv[0])[1]
username = getpass.getuser()
hostname = socket.gethostname()

# Add hostname to the root.title
root.title('Software Distributor - Job Browser')
root.geometry("800x400")

# Check for X or ANSI terminal
# Start Tkinter if X is available
# Start ANSI terminal if no X and no command line options
# if ANSI terminal check for terminal type support

terminal_error = ' NOTE:    The interactive UI was invoked, since no software was specified.  \n        The TERM environment variable is set to unknown. \n sd does not support this type of terminal.  You must set TERM \n to a value that is supported by sd and compatible with your \n terminal.  \n \nDo you want to proceed in setting your TERM value and running \n sd?  (yes or no) [yes]'

#
sd_menu = Menu(root)
root.config(menu=sd_menu)

def clock(my_label):
  hour = time.strftime("%H")
  minute = time.strftime("%M")
  second = time.strftime("%S")

  my_label.config(text=hour + ":" + minute + ":" + second)
  my_label.after(1000, clock)

def clock_update():
   my_label.config(text = "New Text")


def sd_search():
    my_label = Label(root, text="You selected File -> Search")

    # Find a name, revision, info, bundle
def sd_print(my_label):
    my_label = Label(root, text="You selected File -> Print")

# Create a File menu dropdown
file_menu = Menu(sd_menu)
sd_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="Search...", command=sd_search)
file_menu.add_command(label="Print...", command=sd_print)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)

# Create a View definitions
def sd_columns():
    my_label = Label(root, text="You selected View -> Columns")

def sd_filter():
    my_label = Label(root, text="You selected View -> Filter")

def sd_sort():
    my_label = Label(root, text="You selected View -> Sort")

def sd_view():
    my_label = Label(root, text="You selected View -> Change software view")

def sd_filter():
    my_label = Label(root, text="You selected View -> Change software filter")

def sd_by_name_icon():
    my_label = Label(root, text="You selected View -> By Name and Icon")

def sd_by_properties():
    my_label = Label(root, text="You selected View -> By Properties")

def sd_save_view():
    my_label = Label(root, text="You selected View -> Save View as Default")

# Create a View menu dropdown
view_menu = Menu(sd_menu)
sd_menu.add_cascade(label="View", menu=view_menu)
view_menu.add_command(label="Columns...", command=sd_columns)
view_menu.add_command(label="Filter...", command=sd_filter)
view_menu.add_command(label="Sort", command=sd_sort)
view_menu.add_separator()
view_menu.add_radiobutton(label="By Name and Icon", command=sd_by_name_icon)
view_menu.add_radiobutton(label="By Properties", command=sd_by_properties)
view_menu.add_separator()
view_menu.add_command(label="Save view as Default ...", command=sd_save_view)

#
# Create a Options definitions
#
def sd_options():
    my_label = Label(root, text="You selected Options -> Change Options")

def sd_refresh_int():
    my_label = Label(root, text="You selected Options -> Refresh Interval")

def sd_refresh():
    my_label = Label(root, text="You selected Options -> Refresh Interval")

# Create a Options menu dropdown
options_menu = Menu(sd_menu)
sd_menu.add_cascade(label="Options", menu=options_menu)
options_menu.add_command(label="Change Refresh Interval...", command=sd_refresh_int)
  # Change Refresh Interval
  # x 1 second
  # x 5 seconds
  # x 10 seconds
  # x 30 seconds
  # x Never (refresh off)
  # x Save Interval as default

options_menu.add_command(label="Refresh list", command=sd_refresh)

# Create Actions definitions
# Create Job is always available and has a submenu
# Install Software

# If a job is selected from Jobs pane, then the action menu
# includes these options for the selected Job
  # Remove Installed Software
  # Show Job Descripstion
  # Show Job Log
  # Remove Job

def sd_create_job():
    my_label = Label(root, text="You selected Actions -> Create Job")

def sd_install_sw():
    my_label = Label(root, text="You selected Actions -> Install Software")

def sd_depot_remove():
    my_label = Label(root, text="You selected Actions -> Remove software to a depot")

def sd_depot_copy():
    my_label = Label(root, text="You selected Actions -> Copy software to a depot")

def sd_remove_installled_sw():
    my_label = Label(root, text="You selected Actions -> Remove Installed Software")

def sd_show_j_results():
    my_label = Label(root, text="You selected Actions -> Show Job Results")

def sd_show_j_desc():
    my_label = Label(root, text="You selected Actions -> Show Job Description")

def sd_show_j_log():
    my_label = Label(root, text="You selected Actions -> Show Job Log")

def sd_remove_job():
    my_label = Label(root, text="You selected Actions -> Remove Job")

# Create a Actions menu dropdown
actions_menu = Menu(sd_menu)
sd_jobs_menu = Menu(actions_menu)
sd_menu.add_cascade(label="Actions", menu=actions_menu)
actions_menu.add_cascade(label="Create Job", menu=sd_jobs_menu)

sd_jobs_menu.add_command(label="Install Software...", command=sd_install_sw)
sd_jobs_menu.add_command(label="Remove Installed Software...", command=sd_remove_installled_sw)
sd_jobs_menu.add_separator()
sd_jobs_menu.add_command(label="Copy Software to a depot...", command=sd_depot_copy)
sd_jobs_menu.add_command(label="Remove Software from a depot...", command=sd_depot_remove)
sd_jobs_menu.add_separator()
actions_menu.add_separator()
# The following menu options should only appear when a job
# in the Jobs pane is selected
actions_menu.add_command(label="Show Job Results...", command=sd_show_j_results)
actions_menu.add_command(label="Show Job Description...", command=sd_show_j_desc)
actions_menu.add_command(label="Show Job Log...", command=sd_show_j_log)
actions_menu.add_separator()
actions_menu.add_command(label="Create Job from Selected", command=sd_create_job)
actions_menu.add_separator()
# The Remove Job menu optio should only appear when a job or jobs
# in the Jobs pane is selected
actions_menu.add_command(label="Remove Job...", command=sd_remove_job)

#
# Create Help definitions
#
def sd_help_overview():
    my_label = Label(root, text="You selected Help -> Options")

def sd_help_keyboard():
    my_label = Label(root, text="You selected Help -> Keyboard")

def sd_help_usage():
    my_label = Label(root, text="You selected Help -> Using Help")

def sd_help_prod_info():
    my_label = Label(root, text="You selected Help -> Product Information")

# Create a Help menu dropdown
help_menu = Menu(sd_menu)
sd_menu.add_cascade(label="Help", menu=help_menu)
help_menu.add_command(label="Overview...", command=sd_help_overview)
help_menu.add_command(label="Keyboard...", command=sd_help_keyboard)
help_menu.add_command(label="Usage Help...", command=sd_help_usage)
help_menu.add_separator()
help_menu.add_command(label="Product Information...", command=sd_help_prod_info)

root.mainloop()
