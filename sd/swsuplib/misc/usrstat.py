# usrstat.py
#   Copyright (C) 2024 Paul Weber Conversion to Python

#   COPYING TERMS AND CONDITIONS:
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# Change Log
# ------------------------------------------------------------------
# 20240106 Paul Weber - usrstat.py updated, no errors.

import os
import pwd
import grp
import stat
import time

from sd.swsuplib.strob import STROB

def swlib_write_stats(filename, linkname_p, pstatbuf, terse, markup_prefix, ofd, pbuffer):
    access = [''] * 10
    if ofd < 0:
        pass
    if not pstatbuf:
        statbuf = os.lstat(filename)
    else:
        statbuf = pstatbuf
    if not pbuffer:
        buffer = STROB()
    else:
        buffer = pbuffer
    buffer.str_ = ''
    if terse != 0:
        buffer.str_ += f"{filename} {statbuf.st_size} {statbuf.st_blocks} {statbuf.st_mode} {statbuf.st_uid} {statbuf.st_gid} {statbuf.st_dev} {statbuf.st_ino} {statbuf.st_nlink} {os.major(statbuf.st_rdev)} {os.minor(statbuf.st_rdev)} {statbuf.st_atime} {statbuf.st_mtime} {statbuf.st_ctime} {statbuf.st_blksize}\n"
        if ofd >= 0:
            os.write(ofd, buffer.str_.encode())
        if not pbuffer:
            pass
        return
    if stat.S_IFMT(statbuf.st_mode) == stat.S_IFLNK:
        if linkname_p:
            linkname = linkname_p
        else:
            linkname = os.readlink(filename)
        buffer.str_ += f"{markup_prefix} File: \"{filename}\" -> \"{linkname}\"\n"
        if ofd >= 0:
            os.write(ofd, buffer.str_.encode())
    else:
        buffer.str_ += f"{markup_prefix} File: \"{filename}\"\n"
        if ofd >= 0:
            os.write(ofd, buffer.str_.encode())
    buffer.str_ += f"{markup_prefix} Size: {statbuf.st_size:<10} Blocks: {statbuf.st_blocks:<10} IO Block: {statbuf.st_blksize:<6} "
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    if stat.S_IFMT(statbuf.st_mode) == stat.S_IFDIR:
        buffer.str_ += "Directory\n"
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFCHR:
        buffer.str_ += "Character Device\n"
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFBLK:
        buffer.str_ += "Block Device\n"
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFREG:
        buffer.str_ += "Regular File\n"
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFLNK:
        buffer.str_ += "Symbolic Link\n"
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFSOCK:
        buffer.str_ += "Socket\n"
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFIFO:
        buffer.str_ += "Fifo File\n"
    else:
        buffer.str_ += "Unknown\n"
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    buffer.str_ += f"{markup_prefix} Device: {statbuf.st_dev}h/{statbuf.st_dev}d Inode: {statbuf.st_ino:<10} Links: {statbuf.st_nlink:<5}"
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    i = stat.S_IFMT(statbuf.st_mode)
    if i == stat.S_IFCHR or i == stat.S_IFBLK:
        buffer.str_ += f" Device type: {os.major(statbuf.st_rdev)},{os.minor(statbuf.st_rdev)}\n"
    else:
        buffer.str_ += "\n"
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    access[9] = 't' if statbuf.st_mode & stat.S_IXOTH else 'T' if statbuf.st_mode & stat.S_ISVTX else '-'
    access[8] = 'w' if statbuf.st_mode & stat.S_IWOTH else '-'
    access[7] = 'r' if statbuf.st_mode & stat.S_IROTH else '-'
    access[6] = 's' if statbuf.st_mode & stat.S_IXGRP else 'S' if statbuf.st_mode & stat.S_ISGID else 'x' if statbuf.st_mode & stat.S_IXGRP else '-'
    access[5] = 'w' if statbuf.st_mode & stat.S_IWGRP else '-'
    access[4] = 'r' if statbuf.st_mode & stat.S_IRGRP else '-'
    access[3] = 's' if statbuf.st_mode & stat.S_IXUSR else 'S' if statbuf.st_mode & stat.S_ISUID else 'x' if statbuf.st_mode & stat.S_IXUSR else '-'
    access[2] = 'w' if statbuf.st_mode & stat.S_IWUSR else '-'
    access[1] = 'r' if statbuf.st_mode & stat.S_IRUSR else '-'
    if stat.S_IFMT(statbuf.st_mode) == stat.S_IFDIR:
        access[0] = 'd'
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFCHR:
        access[0] = 'c'
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFBLK:
        access[0] = 'b'
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFREG:
        access[0] = '-'
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFLNK:
        access[0] = 'l'
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFSOCK:
        access[0] = 's'
    elif stat.S_IFMT(statbuf.st_mode) == stat.S_IFIFO:
        access[0] = 'p'
    else:
        access[0] = '?'
    buffer.str_ += f"{markup_prefix} Access: ({statbuf.st_mode & 0o7777:04o}/{''.join(access)}) "
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    pw_ent = pwd.getpwuid(statbuf.st_uid)
    gw_ent = grp.getgrgid(statbuf.st_gid)
    buffer.str_ += f" Uid: ({statbuf.st_uid}/{pw_ent.pw_name if pw_ent else 'UNKNOWN'}) Gid: ({statbuf.st_gid}/{gw_ent.gr_name if gw_ent else 'UNKNOWN'})\n"
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    buffer.str_ += f"{markup_prefix} Modify: {time.ctime(statbuf.st_mtime)}"
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    buffer.str_ += f"{markup_prefix} Change: {time.ctime(statbuf.st_ctime)}\n"
    if ofd >= 0:
        os.write(ofd, buffer.str_.encode())
    if not pbuffer:
        pass
