# xformat.py - read/write archives

# Copyright (C) 2000,2004 Jim Lowe
# Copyright (C) 2023 - 2024 Paul Weber Port to Python
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the

import os
import stat
import sys
import tarfile
import ctypes

from sd.debug import XFORMAT_E_DEBUG, SWERROR, SWERROR3, SWERROR2, XFORMAT_E_DEBUG2, TARUNEEDDEBUG
# from sd.swsuplib.misc.cksum import swlib_bsd_sum_from_mem
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.misc.swlib import swlib_imaxtostr, swlib_writef, FrameRecord
from sd.swsuplib.taru.ahs import ahsstaticgettarfilename, ahsstaticgettarlinkname
from sd.swsuplib.taru.defer import defer_open, defer_set_taru, defer_close
from sd.swsuplib.taru.filetypes import ft
# from sd.swsuplib.taru.filemode import swlib_filemodestring

from sd.swsuplib.strob import strob_close, strob_open, strob_strcpy
from sd.swsuplib.taru.ahs import (ahs_close, ahsstaticdeletefilehdr, ahsstaticcreatefilehdr,
                                  ahs_set_name,ahsstaticsettarfilename, ahsstaticsetpaxlinkname,
                                  ahs_set_linkname, ahs_vfile_hdr, ahs_set_mode, ahs_set_perms,
                                  ahs_set_filetype_from_tartype, ahs_get_tar_typeflag, ahs_set_uid,
                                  ahs_set_tar_username, ahs_get_tar_groupname, ahs_set_uid_by_name,
                                  ahs_set_user_systempair, ahs_set_group_systempair, ahs_set_tar_groupname,
                                  ahs_set_gid_by_name, ahs_set_gid, ahs_set_filesize, ahs_get_filesize, ahs_set_nlink,
                                  ahs_set_inode, ahs_set_mtime, ahs_set_devmajor, ahs_set_devminor, ahs_set_to_statbuf,
                                  ahs_set_from_statbuf, ahs_get_tar_chksum, ahs_get_system_username, ahs_get_system_groupname,
                                  ahs_get_tar_username, ahs_set_sys_db_u_policy, ahs_set_sys_db_g_policy, ahs_get_mtime,
                                  ahs_get_name, ahs_get_linkname, ahs_get_mode, ahs_get_perms, ahs_get_source_filename)
from sd.swsuplib.taru.copyout import taru_write_archive_trailer, taru_write_archive_member, taru_write_archive_file_data, taru_write_archive_member_header, taru_write_archive_member_data
from sd.swsuplib.taru.cpiohdr import chc
from sd.swsuplib.taru.defer import defer_writeout_final_defers
from sd.swsuplib.taru.readinheader import taru_read_amount, taru_read_header
from sd.swsuplib.taru.mtar import taru_filehdr2filehdr, taru_filehdr2statbuf, taru_statbuf2filehdr
from sd.swsuplib.taru.otar import taru_get_tar_filetype, taru_tape_skip_padding
# from sd.swsuplib.taru.tar import tar_header
from sd.swsuplib.taru.taru import new_cpio_header, taru_init_header, taru_set_preview_level
from sd.swsuplib.taru.hllist import hllist_close
from sd.swsuplib.taru.porinode import porinode_close
from sd.swsuplib.misc.swvarfs import (swvarfs_open, swvarfs_close, swvarfs_u_close, swvarfs_u_fstat, swvarfs_opendup,
                                      swvarfs_opendup_with_name, swvarfs_open_directory,
                                      swvarfs_u_readlink, swvarfs_u_open, swvarfs_u_get_name, swvarfs_u_get_linkname,
                                      swvarfs_set_ahs, swvarfs_get_format, swvarfs_fd, swvarfs_get_next_dirent,
                                      swvarfs_file_has_data, swvarfs_set_tarheader_flag, swvarfs_set_tarheader_flags,
                                      swvarfs_setdir, swvarfs_u_lstat)

from sd.swsuplib.strob import strob_str
from sd.swsuplib.taru.taru import (tc, af, taru_digs_init, taru_delete, taru_digs_create, taru_digs_delete,
                                   taru_set_preview_fd, taru_get_preview_fd, taru_set_tarheader_flag)

from sd.swsuplib.taru.taruib import taruib_get_fd, taruib_set_fd, taruib_clear_buffer
from sd.swsuplib.uxfio import (uc, uxfio_lseek, uxfio_sfread, uxfio_getfd, uxfio_fcntl, UXFIO_FD_MIN,
                               )

XFORMAT_OFF = 0
XFORMAT_ON = 1

global __LINE__

def XFORMAT_E_FAIL(format_):
    raise Exception("INTERNAL ERROR: " + format_)

def XFORMAT_E_FAIL2(format_, arg):
    raise Exception("INTERNAL ERROR: " + format_ + str(arg))

def XFORMAT_E_FAIL3(format_, arg, arg1):
    raise Exception("INTERNAL ERROR: " + format_ + str(arg) + str(arg1))

class XFORMAT:
    def __init__(self, ifd, ofd, format):
        self.ifd = ifd
        self.ofd = ofd
        self.format_code = format
        self.output_format_code = 0
        self.eoa = 0
        self.bytes_written = 0
        self.make_false_inodes = 0
        self.swvarfs = None
        self.ahs = None
        self.link_record = None
        self.defer = None
        self.use_false_inodes = None
        self.porinode = None
        self.trailer_bytes = 0
        self.swvarfs_is_external = 0
        self.last_header_size = 0
        self.taru = None
        self.lt2 = os.stat_result((0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
        self.name2 = None
        self.linkname2 = None

def setup_1(xux, fmt):
    xux.eoa = 0
    xux.trailer_bytes = 0
    xux.bytes_written = 0
    xux.ifd = -1
    xux.ofd = -1
    xux.link_record = []
    xux.porinode = []
    xux.use_false_inodes = xux.porinode
    xux.defer = None
    xux.format_code = fmt
    xux.output_format_code = fmt
    xux.swvarfs = None
    xux.ahs = []
    xux.make_false_inodes = 1
    xux.swvarfs_is_external = 0
    xux.last_header_size = 0
    xux.taru = tarfile.TarFile()
    xux.taru.taru_tarheaderflags = 0
    xux.name2 = ""
    xux.linkname2 = ""

#porinode_open()

#defer_open(format)

#hllist_open()

def is_all_digits(value):
    return value.isdigit()

def local_v_close(xux, fd):
    ret = os.close(fd) if xux.swvarfs is None else xformat_u_close_file(xux, fd)
    return ret


def local_v_open(xux, source, flags, mode):
    ret = os.open(source, flags, mode) if xux.swvarfs is None else xformat_u_open_file(xux, source)
    return ret


def local_v_lstat(xux, path, st):
    ret = 0
    if xux.swvarfs:
        ret = xformat_u_lstat(xux, path, st)
    else:
        ret = os.lstat(path)
    return ret


def return_do_inodes(xux):
    return xux.porinode if ((xux.make_false_inodes != 0) or (int(xux.output_format_code) == af.arf_oldascii)) else None


def internal_set_format(xux, instance_format, format):
    if format <= af.arf_crcascii and not xux.defer:
        xux.defer = defer_open(format)
        defer_set_taru(xux.defer, xux.taru)
    if format > af.arf_crcascii and xux.defer:
        defer_close(xux.defer)
        xux.defer = None
    # xux.format_code = archive_format(format)
    instance_format = af(format)

def taru_header_dump_string_s(file_hdr, prefix):
    global buf
    name = file_hdr.c_name if file_hdr.c_name else "<null>"
    linkname = file_hdr.c_tar_linkname if file_hdr.c_tar_linkname else "<null>"
    # tarheader = bytearray(512)
    modestring = stat.S_IWRITE
    # user_name = bytearray(128)
    # group_name = bytearray(128)
    if not buf:
        buf = []
    if not prefix:
        prefix = ""
    user_name = file_hdr.uname
    group_name = file_hdr.gname
    os.chmod(file_hdr.c_mode, modestring)
    buf.append(f"{prefix}{file_hdr} (taru_header*)")
    buf.append(f"{prefix}{file_hdr} c_name =         [{name}]")
    buf.append(f"{prefix}{file_hdr} c_tar_linkname = [{linkname}]")
    buf.append(f"{prefix}{file_hdr} c_ino =          [{file_hdr.c_ino}]")
    buf.append(f"{prefix}{file_hdr} c_mode =         [{file_hdr.c_mode}]")
    buf.append(f"{prefix}{file_hdr} c_uid =          [{file_hdr.c_uid}]")
    buf.append(f"{prefix}{file_hdr} c_gid =          [{file_hdr.c_gid}]")
    buf.append(f"{prefix}{file_hdr} c_nlink =        [{file_hdr.c_nlink}]")
    buf.append(f"{prefix}{file_hdr} c_mtime =        [{file_hdr.c_mtime}]")
    buf.append(f"{prefix}{file_hdr} c_filesize =     [{file_hdr.c_filesize}]")
    buf.append(f"{prefix}{file_hdr} uname =          [{user_name}]")
    buf.append(f"{prefix}{file_hdr} gname =          [{group_name}]")
    buf.append(f"{prefix}{file_hdr} modestring =     [{modestring}]")
    buf.append(f"{prefix}{file_hdr} c_dev_maj =      [{file_hdr.c_dev_maj}]")
    buf.append(f"{prefix}{file_hdr} c_dev_min =      [{file_hdr.c_dev_min}]")
    buf.append(f"{prefix}{file_hdr} c_rdev_maj =     [{file_hdr.c_rdev_maj}]")
    buf.append(f"{prefix}{file_hdr} c_rdev_min =     [{file_hdr.c_rdev_min}]")
    buf.append(f"{prefix}{file_hdr} c_namesize =     [{file_hdr.c_namesize}]")
    buf.append(f"{prefix}{file_hdr} c_cu       =     [{file_hdr.c_cu}]")
    buf.append(f"{prefix}{file_hdr} c_cg       =     [{file_hdr.c_cg}]")
    return "\n".join(buf)


def taru_header_dump(file_hdr, fp):
    name = file_hdr.c_name if file_hdr.c_name else "<null>"
    linkname = file_hdr.c_tar_linkname if file_hdr.c_tar_linkname else "<null>"
    # tarheader = [512]
    modestring = stat.S_IWRITE
    name_sum = sum(bytearray(name.encode()))
    user_name = file_hdr.uname
    group_name = file_hdr.gname
    os.chmod(file_hdr.c_mode, modestring)
    buf = []
    buf.append(f"{name_sum} = bsd sum of name")
    buf.append(f"{name_sum} attribute = [<attribute_value>]")
    buf.append(f"{name_sum} c_name = [{name}]")
    buf.append(f"{name_sum} c_tar_linkname = [{linkname}]")
    buf.append(f"{name_sum} c_ino =  [{file_hdr.c_ino}]")
    buf.append(f"{name_sum} c_mode = [{file_hdr.c_mode}]")
    buf.append(f"{name_sum} c_uid = [{file_hdr.c_uid}]")
    buf.append(f"{name_sum} c_gid = [{file_hdr.c_gid}]")
    buf.append(f"{name_sum} c_nlink = [{file_hdr.c_nlink}]")
    buf.append(f"{name_sum} c_mtime = [{file_hdr.c_mtime}]")
    buf.append(f"{name_sum} c_filesize = [{file_hdr.c_filesize}]")
    buf.append(f"{name_sum} uname = [{user_name}]")
    buf.append(f"{name_sum} gname = [{group_name}]")
    buf.append(f"{name_sum} modestring = [{modestring}]")
    buf.append(f"{name_sum} c_dev_maj = [{file_hdr.c_dev_maj}]")
    buf.append(f"{name_sum} c_dev_min = [{file_hdr.c_dev_min}]")
    buf.append(f"{name_sum} c_rdev_maj = [{file_hdr.c_rdev_maj}]")
    buf.append(f"{name_sum} c_rdev_min = [{file_hdr.c_rdev_min}]")
    buf.append(f"{name_sum} c_namesize = [{file_hdr.c_namesize}]")
    fp.write("\n".join(buf))
    return 0


def common_setup_write_header(xux, p_hdr1, hdr0, t_stat, t_name, t_source):
    SWERROR("XFORMAT DEBUG: ", "")
    #		 
    #		// hdr0 is never null; 
    #                // only lstat the source if name is null.
    #
    xux.linkname2 = '\0'
    p_hdr1[0].copy_from(xformat_vfile_hdr(xux))
    if not t_name:
        p1_name = ahsstaticgettarfilename(p_hdr1)
        if len(p1_name) != 0:
            strob_strcpy(xux.name2, p1_name)
    else:
        strob_strcpy(xux.name2, t_name)

    if t_name is None and t_stat is None and t_source is None:
        #
        # Use the object vfile_hdr.
        #			
        SWERROR("XFORMAT DEBUG: ", "[0 0 0]")
    elif t_name is None and t_stat is None and t_source:
        #			
        # Use the vfile header for stats.
        # but get the size from the source.
        #			
        SWERROR("XFORMAT DEBUG: ", "[0 0 1]")
        SWERROR2("XFORMAT DEBUG: ", "[0 0 1] %s", t_source)
        # if lstat(t_source, xux.lt2) != 0: # original, os.lstat doesn't take the second arg
        if os.lstat(t_source) != 0:
            print("lstat [xformat] %s: %s" % t_source, sys.stderr.fileno)
            return -1
        else:
            tartypeflag = 0
            #				
            # set the size and filetype because you
            # probably don't want to lie about these.
            #				
            xformat_set_filesize(xux, int(xux.lt2.st_size))
            tartypeflag = taru_get_tar_filetype(xux.lt2.st_mode)
            if tartypeflag < 0:
                print("%s: unrecognized file type in mode [%d] for file: %s" % (swlib_utilname_get(), xux.lt2.st_mode, t_source))
            xformat_set_filetype_from_tartype(xux, tartypeflag)
            taru_filehdr2filehdr(hdr0, p_hdr1[0])
            if TARUNEEDDEBUG:
                SWERROR2("XFORMAT DEBUG: ", "[0 0 1] %s", taru_header_dump_string_s(hdr0, "[0 0 1] hdr0 "))
    elif t_name and t_stat is None and t_source is None:
        #
        # name2 has already been set from t_name.
        #
        SWERROR("XFORMAT DEBUG: ", "[1 0 0]")
        SWERROR2("XFORMAT DEBUG: ", "[1 0 0] %s", t_name)
        taru_filehdr2statbuf(xux.lt2, p_hdr1[0])
        if TARUNEEDDEBUG:
            SWERROR2("XFORMAT DEBUG: ", "[1 0 0] %s", taru_header_dump_string_s(p_hdr1[0], "[1 0 0] *p_hdr1 "))

        xux.linkname2 = ahsstaticgettarlinkname(p_hdr1[0])

        taru_statbuf2filehdr(hdr0, xux.lt2, str(None), strob_str(xux.name2), strob_str(xux.linkname2))
        taru_filehdr2filehdr(hdr0, p_hdr1[0])
        if TARUNEEDDEBUG:
            SWERROR2("XFORMAT DEBUG: ", "[1 0 0] %s", taru_header_dump_string_s(hdr0, "[1 0 0] hdr0 "))

        ahsstaticsettarfilename(hdr0, strob_str(xux.name2))
        ahsstaticsetpaxlinkname(hdr0, strob_str(xux.linkname2))
        p_hdr1[0].copy_from(hdr0)
    elif (t_stat is not None and t_name and t_source is None) or (t_stat is not None and t_name and t_source):
        #			
        #
        # Use statbuf on function param list.
        # t_source may be null and thats OK.
        #			

        SWERROR("XFORMAT DEBUG: ", "[1 1 0] || [1 1 1]")
        SWERROR3("XFORMAT DEBUG: ", "[1 1 0] || [1 1 1] %s %s", t_name, t_source)
        taru_statbuf2filehdr(hdr0, t_stat, t_source, strob_str(xux.name2), strob_str(xux.linkname2))
        p_hdr1[0].copy_from(hdr0)
            ##if TARUNEEDDEBUG
        SWERROR2("XFORMAT DEBUG: ", "[1 1 0]||[1 1 1]  %s", taru_header_dump_string_s(hdr0, "[1 1 0]||[1 1 1] hdr0 "))
            ##endif
    elif t_source:
        #			
        #  stat the source
        #			
        SWERROR("XFORMAT DEBUG: ", "[x x 1]")
        SWERROR2("XFORMAT DEBUG: ", "[x x 1] %s", t_source)
        if local_v_lstat(xux, t_source, (xux.lt2)) != 0:
            print("lstat (loc=2) %s %s" % (t_source, os.error.filename))
            return -1
        else:
            taru_statbuf2filehdr(hdr0, xux.lt2, t_source, strob_str(xux.name2), strob_str(xux.linkname2))
            p_hdr1[0].copy_from(hdr0)
            if TARUNEEDDEBUG:
                SWERROR2("XFORMAT DEBUG: ", "[x x 1]  %s", taru_header_dump_string_s(hdr0, "[x x 1] hdr0 "))

    else:
        print("bad use of common_setup_write, last else-if.")
        return -1
    return 0




def u_open_file_common(xux, fd):
    s = '\0'
    st = os.stat_result(fd)
    SWERROR("XFORMAT DEBUG: ", "")
    swvarfs_u_fstat(xux.swvarfs, fd, st)
    taru_statbuf2filehdr(xformat_vfile_hdr(xux), st, str(None), str(None), str(None))
    SWERROR("XFORMAT DEBUG: ", "")
    ahs_set_name(xux.ahs,s if (s := swvarfs_u_get_name(xux.swvarfs, fd)) else "")
    SWERROR("XFORMAT DEBUG: ", "")
    ahs_set_linkname(xux.ahs,s if (s := swvarfs_u_get_linkname(xux.swvarfs, fd)) else "")
    SWERROR("XFORMAT DEBUG: ", "")
    return fd


def open_archive_init(xux):
    swvarfs_set_ahs(xux.swvarfs, xux.ahs)
    xformat_set_format(xux, swvarfs_get_format(xux.swvarfs))
    xformat_set_output_format(xux, xux.format_code)
    xux.ifd = swvarfs_fd(xux.swvarfs)


def open_archive_dir(xux, pathname, flags, mode):
    SWERROR("XFORMAT DEBUG: ", "")
    xux.swvarfs = swvarfs_open_directory(pathname)
    if xux.swvarfs is None:
        return -1
    open_archive_init(xux)
    return 0


def internal_open_archive_file(filetype, xux, pathname, flags, mode):
    st = None

    if filetype and not pathname:
        #		
        # Invalid.
        #		
        return -11
    if filetype and pathname:
        try:
            st = os.stat(pathname)
        except OSError as e:
            print("stat : {} : {}".format(pathname, e.strerror))
            return -9
    if filetype == 0:
        #		
        # stdin.
        #		
        ret = xformat_open_archive(xux, "-", flags, mode)
    elif stat.S_ISDIR(st.st_mode) and filetype == stat.S_IFDIR:
        #		
        # a directory containing file system hierarchy.
        #		
        ret = open_archive_dir(xux, pathname, flags, mode)
    elif stat.S_ISREG(st.st_mode) and filetype is stat.S_IFREG:
        #		
        # a portable archive file.
        #		
        ret = xformat_open_archive(xux, pathname, flags, mode)
    else:
        sys.stderr.write("archive file is wrong type.\n")
        ret = -20
    return ret

#
#//////////////////////////////////////////////////////////////////////////////
#public:
#//////////////////////////////////////////////////////////////////////////////
#


def xformat_close(xux):
    ret = 0
    if xux.link_record:
        hllist_close(xux.link_record)
    if xux.porinode:
        porinode_close(xux.porinode)
    if xux.defer:
        defer_close(xux.defer)
    if xux.ahs:
        ahs_close(xux.ahs)
    if xux.swvarfs is not None and xux.swvarfs_is_external == 0:
        ret = swvarfs_close(xux.swvarfs)
    if xux.taru:
        taru_delete(xux.taru)
        xux.taru = None
    strob_close(xux.name2)
    strob_close(xux.linkname2)
    return ret


def xformat_open(ifd, ofd, format1):
    xux = XFORMAT(ifd, ofd, format)
    setup_1(xux, format1)
    xux.ifd = ifd
    xux.ofd = ofd
    return xux


def xformat_ahs_object(xux):
    return xux.ahs

def xformat_get_header_buffer(xux, buf):
    if buf:
        return buf
    else:
        a = [None] * 512
        return a

def xformat_set_tar_chksum(xux):
    pass

def xformat_set_format(xux, format):
    XFORMAT_E_DEBUG("")
    internal_set_format(xux, [xux.format_code], format)

def xformat_get_format(xux):
    XFORMAT_E_DEBUG("")
    return xux.format_code

def xformat_set_output_format(xux, format):
    XFORMAT_E_DEBUG("")
    internal_set_format(xux, [xux.output_format_code], format)

def xformat_get_taru_object(xux):
    return xux.taru

def xformat_get_output_format(xux):
    XFORMAT_E_DEBUG("")
    return int(xux.output_format_code)

def xformat_init_vfile_header(xux):
    taru_init_header(xformat_vfile_hdr(xux))

def xformat_vfile_hdr(xux):
    XFORMAT_E_DEBUG("")
    return ahs_vfile_hdr(xux.ahsM)

def xformat_set_mode(xux, mode):
    XFORMAT_E_DEBUG("")
    ahs_set_mode(xux.ahsM, mode)

def xformat_set_perms(xux, mode):
    XFORMAT_E_DEBUG("")
    ahs_set_perms(xux.ahsM, mode)

def xformat_set_filetype_from_tartype(xux, s):
    XFORMAT_E_DEBUG("")
    ahs_set_filetype_from_tartype(xux.ahsM, s)

def xformat_set_uid(xux, uid):
    XFORMAT_E_DEBUG("")
    ahs_set_uid(xux.ahsM, uid)

def xformat_get_tar_typeflag(xux):
    XFORMAT_E_DEBUG("")
    return ahs_get_tar_typeflag(xux.ahs)

def xformat_file_has_data(xux):
    """
    return (xformat_get_tar_typeflag(xux) == REGTYPE);
    """
    XFORMAT_E_DEBUG("")
    return swvarfs_file_has_data(xux.swvarfs)


def xformat_set_username(xux, name):
    ahs_set_tar_username(xux.ahsM, name)


def xformat_set_user_systempair(xux, name):
    ret = ahs_set_user_systempair(xux.ahsM, name)

    if ret and is_all_digits(name) == 0:
        ahs_set_tar_username(xux.ahsM, name)

    return ret


def xformat_set_group_systempair(xux, name):
    ret = ahs_set_group_systempair(xux.ahsM, name)

    if ret and is_all_digits(name) == 0:
        ahs_set_tar_groupname(xux.ahsM, name)

    return ret


def xformat_set_groupname(xux, name):
    ahs_set_tar_groupname(xux.ahsM, name)


def xformat_set_uid_by_name(xux, username):
    ahs_set_uid_by_name(xux.ahsM, username)


def xformat_set_gid_by_name(xux, groupname):
    ahs_set_gid_by_name(xux.ahsM, groupname)


def xformat_set_gid(xux, gid):
    ahs_set_gid(xux.ahsM, gid)


def xformat_set_filesize(xux, filesize):
    ahs_set_filesize(xux.ahsM, filesize)


def xformat_set_nlink(xux, nlink):
    ahs_set_nlink(xux.ahsM, nlink)


def xformat_set_inode(xux, ino):
    ahs_set_inode(xux.ahsM, ino)


def xformat_set_mtime(xux, mtime):
    ahs_set_mtime(xux.ahsM, mtime)


def xformat_set_devmajor(xux, dev):
    ahs_set_devmajor(xux.ahsM, dev)


def xformat_set_devminor(xux, dev):
    ahs_set_devminor(xux.ahsM, dev)


def xformat_set_name(xux, name):
    ahs_set_name(xux.ahsM, name)


def xformat_set_linkname(xux, linkname):
    ahs_set_linkname(xux.ahsM, linkname)


def xformat_set_virtual_eof(xux, len):
    if xux.ifd < UXFIO_FD_MIN:
        return -3
    if uxfio_fcntl(xux.ifd, uc.UXFIO_F_SET_BUFACTIVE, uc.UXFIO_ON):
        return -2
    if uxfio_fcntl(xux.ifd, uc.UXFIO_F_SET_VEOF, len):
        return -1
    return 0


def xformat_set_to_statbuf(xux, st):
    ahs_set_to_statbuf(xux.ahsM, st)
    return 0


def xformat_set_from_statbuf_path(xux, path):
    st = os.stat(path)
    xformat_set_from_statbuf(xux, st)
    return 0


def xformat_set_from_statbuf_fd(xux, fd):
    st = os.fstat(fd)
    xformat_set_from_statbuf(xux, st)
    return 0


def xformat_set_from_statbuf(xux, st):
    ahs_set_from_statbuf(xux.ahsM, st)


def xformat_set_preview_level(xux, fd):
    if not xux.taru:
        global __LINE__
        FrameRecord()
        print("AAARRRRGG xux.taru is null at line", __LINE__)
        return
    taru_set_preview_level(xux.taru, fd)


def xformat_set_preview_fd(xux, fd):
    FrameRecord()
    if not xux.taru:
        print("AAARRRRGG xux.taru at line", __LINE__)
        return
    taru_set_preview_fd(xux.taru, fd)


def xformat_set_ofd(xux, fd):
    xux.ofd = fd


def xformat_set_ifd(xux, fd):
    xux.ifd = fd


def xformat_get_swvarfs(xux):
    return xux.swvarfs


def xformat_get_preview_fd(xux):
    return taru_get_preview_fd(xux.taru)


def xformat_get_ofd(xux):
    return xux.ofd


def xformat_get_ifd(xux):
    return xux.ifd

def xformat_set_pass_fd(xux, fd):
    taruib_set_fd(fd)

def xformat_get_pass_fd(xux):
    return taruib_get_fd()

def xformat_clear_pass_buffer(xux):
    return taruib_clear_buffer()

def xformat_get_next_dirent(xux, st):
    name = swvarfs_get_next_dirent(xux.swvarfs, st)
    xux.eoa = xux.swvarfs.eoa
    return name
def xformat_get_tar_chksum(xux, tarhdr):
    XFORMAT_E_DEBUG("")
    return ahs_get_tar_chksum(xux.ahs, tarhdr)

def xformat_get_username(xux, buf):
    XFORMAT_E_DEBUG("")
    return ahs_get_system_username(xux.ahs, buf)

def xformat_get_groupname(xux, buf):
    XFORMAT_E_DEBUG("")
    return ahs_get_system_groupname(xux.ahs, buf)

def xformat_get_tar_username(xux):
    XFORMAT_E_DEBUG("")
    return ahs_get_tar_username(xux.ahs)

def xformat_get_tar_groupname(xux):
    XFORMAT_E_DEBUG("")
    return ahs_get_tar_groupname(xux.ahs)

def xformat_set_sys_db_u_policy(xux, c):
    XFORMAT_E_DEBUG("")
    ahs_set_sys_db_u_policy(xux.ahs, c)

def xformat_set_sys_db_g_policy(xux, c):
    XFORMAT_E_DEBUG("")
    ahs_set_sys_db_g_policy(xux.ahs, c)

def xformat_get_filesize(xux):
    XFORMAT_E_DEBUG("")
    return ahs_get_filesize(xux.ahs)

def xformat_get_mtime(xux):
    XFORMAT_E_DEBUG("")
    return ahs_get_mtime(xux.ahs)

def xformat_get_name(xux, buf):
    XFORMAT_E_DEBUG("")
    return ahs_get_name(xux.ahs, buf)

def xformat_get_linkname(xux, buf):
    XFORMAT_E_DEBUG("")
    return ahs_get_linkname(xux.ahs, buf)

def xformat_get_mode(xux):
    XFORMAT_E_DEBUG("")
    return ahs_get_mode(xux.ahs)

def xformat_get_perms(xux):
    XFORMAT_E_DEBUG("")
    return ahs_get_perms(xux.ahs)

def xformat_get_virtual_eof(xux):
    XFORMAT_E_DEBUG("")
    return -1

def xformat_get_source_filename(xux, buf):
    XFORMAT_E_DEBUG("")
    return ahs_get_source_filename(xux.ahs, buf)

def xformat_is_end_of_archive(xux):
    XFORMAT_E_DEBUG("")
    if xux.format_code == af.arf_ustar or xux.format_code == af.arf_tar:
        ret = xux.eoa
    else:
        name = xformat_get_name(xux, None)
        ret = not name[:25].startswith(tc.CPIO_INBAND_EOA_FILENAME)
    if ret:
        if xformat_get_pass_fd(xux):
            trailer_blocks_read_return = taru_read_amount(xformat_get_ifd(xux), -1)
            XFORMAT_E_DEBUG2("unread trailer blocks = [%d]", trailer_blocks_read_return)
    return ret

def xformat_set_strip_leading_slash(xux, n):
    xformat_set_tarheader_flag(xux, tc.TARU_TAR_DO_STRIP_LEADING_SLASH, n)

def xformat_get_tarheader_flags(xux):
    return xux.taru.taru_tarheaderflags

def xformat_set_tarheader_flags(xux, flags):
    if xux.swvarfs:
        swvarfs_set_tarheader_flags(xux.swvarfs, flags)
    xux.taru.taru_tarheaderflags = flags

def xformat_set_tarheader_flag(xux, flag, n):
    if xux.swvarfs:
        swvarfs_set_tarheader_flag(xux.swvarfs, flag)
    taru_set_tarheader_flag(xux.taru, flag, n)

def xformat_set_numeric_uids(xux, n):
    xformat_set_tarheader_flag(xux, tc.TARU_TAR_NUMERIC_UIDS, n)

def xformat_set_false_inodes(xux, n):
    XFORMAT_E_DEBUG("")
    xux.make_false_inodes = n
    xux.use_false_inodes = return_do_inodes(xux)

def xformat_reset_bytes_written(xux):
    xux.bytes_written = 0

def xformat_decrement_bytes_written(xux, amount):
    xux.bytes_written -= amount

def xformat_setdir(xux, path):
    XFORMAT_E_DEBUG("")
    return swvarfs_setdir(xux.swvarfs, path)

def xformat_read_file_data(xux, dst_fd):
    file_hdr0 = xformat_vfile_hdr(xux)
    ret = taru_write_archive_member_data(xux.taru, file_hdr0, dst_fd, xux.ifd, None, xux.format_code, -1, None)
    if ret < 0:
        return ret
    retval = ret
    ret = taru_tape_skip_padding(xux.ifd, file_hdr0.c_filesize, xux.format_code)
    if ret < 0:
        return -retval
    retval += ret
    XFORMAT_E_DEBUG("LEAVING")
    return retval

def xformat_write_file_data(xux, source_fd):
    ret = 0
    file_hdr0 = xformat_vfile_hdr(xux)
    ret = taru_write_archive_member_data(xux.taru, file_hdr0, xux.ofd, source_fd, None, xux.output_format_code,
                                         -1, None)
    if ret < 0:
        return ret
    if source_fd == xux.ifd:
        taru_tape_skip_padding(xux.ifd, file_hdr0.c_filesize, xux.output_format_code)
    xux.bytes_written += ret
    XFORMAT_E_DEBUG("LEAVING")
    return ret

def xformat_write_header(xux):
    XFORMAT_E_DEBUG("")
    return xformat_write_header_wn(xux, None)

def xformat_write_header_wn(xux, name):
    hdr0 = ahsstaticcreatefilehdr()
    hdr1 = ahsstaticcreatefilehdr()
    ret = common_setup_write_header(xux, hdr1, hdr0, None, name, None)
    if ret == 0:
        ret = taru_write_archive_member_header(xux.taru, None, hdr1, xux.link_record, xux.defer,
                                               xux.use_false_inodes, xux.ofd, xux.output_format_code, hdr0,
                                               xux.taru.taru_tarheaderflags)
        ahsstaticdeletefilehdr(hdr0)
        if ret < 0:
            return ret
        xux.bytes_written += ret
        XFORMAT_E_DEBUG("LEAVING")
        return ret
    else:
        ahsstaticdeletefilehdr(hdr0)
        XFORMAT_E_DEBUG("LEAVING")
        return -1

def xformat_write_file_by_fd(xux, t, name, fout, source_fd):
    hdr0 = ahsstaticcreatefilehdr()
    hdr1 = ahsstaticcreatefilehdr()
    if source_fd < 0 and fout is None:
        ret = xformat_write_file(xux, t, name, None)
        if ret < 0:
            return ret
        ahsstaticdeletefilehdr(hdr0)
        return ret
    if (ret := common_setup_write_header(xux, hdr1, hdr0, t, name, None)) == 0:
        if (ret := taru_write_archive_member_header(xux.taru, None, hdr1, xux.link_record, xux.defer,
                                                    xux.use_false_inodes, xux.ofd, xux.output_format_code, hdr0,
                                                    xux.taru.taru_tarheaderflags)) > 0:
            ret1 = taru_write_archive_member_data(xux.taru, hdr0, xux.ofd, source_fd, None,
                                                  xux.output_format_code, -1, None)
            if ret1 < 0:
                print(f"{swlib_utilname_get()}: error writing file member data", file=sys.stderr)
                return -1
            ret += ret1
        else:
            print(f"{swlib_utilname_get()}: tar header write failed, return code={swlib_imaxtostr(ret, None)}",
                  file=sys.stderr)
            return -1
    else:
        print("common_setup_write_header failed", file=sys.stderr)
        return -1
    xux.bytes_written += ret
    ahsstaticdeletefilehdr(hdr0)
    XFORMAT_E_DEBUG("LEAVING")
    return ret

def xformat_write_by_name(xux, name, st):
    ret = 0
    hdr0 = ahsstaticcreatefilehdr()
    taru_statbuf2filehdr(hdr0, st, None, name, None)
    ret = taru_write_archive_member(xux.taru, name, None, hdr0, xux.link_record, xux.defer,
                                    xux.use_false_inodes, xux.ofd, -1, xux.output_format_code,
                                    xux.taru.taru_tarheaderflags)
    if ret > 0:
        xux.bytes_written += ret
    XFORMAT_E_DEBUG("LEAVING")
    ahsstaticdeletefilehdr(hdr0)
    return ret

def xformat_write_by_fd(xux, srcfd, file_hdr):
    ret = 0
    XFORMAT_E_DEBUG("")
    ret = taru_write_archive_member(xux.taru, None, None, file_hdr, xux.link_record, xux.defer, xux.porinode,
                                    xux.ofd, srcfd, xux.output_format_code, xux.taru.taru_tarheaderflags)
    if ret > 0:
        xux.bytes_written += ret
    XFORMAT_E_DEBUG("LEAVING")
    return ret

def xformat_write_file(xux, t, name, source):
    ret = 0
    ret1 = 0
    fd = -1
    hdr0 = ahsstaticcreatefilehdr()
    hdr1 = ahsstaticcreatefilehdr()
    ret = common_setup_write_header(xux, hdr1, hdr0, t, name, source)
    if not name:
        name = ahsstaticgettarfilename(hdr1)
    if ret < 0:
        return ret
    if not source:
        source = name
    if len(ahsstaticgettarlinkname(hdr1)) == 0:
        if hdr1.c_mode & ft.CP_IFMT == ft.CP_IFREG:
            fd = local_v_open(xux, source, os.O_RDONLY, 0)
            if fd < 0:
                print(f"open {source}: {os.strerror(1)}", file=sys.stderr)
                ahsstaticdeletefilehdr(hdr0)
                return -ret
    ret = taru_write_archive_member_header(xux.taru, None, hdr1, xux.link_record, xux.defer,
                                           xux.use_false_inodes, xux.ofd, xux.output_format_code, hdr0,
                                           xux.taru.taru_tarheaderflags)
    if ret > 0 and fd >= 0:
        ret1 = taru_write_archive_member_data(xux.taru, hdr0, xux.ofd, fd, None, xux.output_format_code, -1,
                                              None)
        if ret1 < 0:
            return -ret
        ret += ret1
        local_v_close(xux, fd)
    if ret > 0:
        xux.bytes_written += ret
    ahsstaticdeletefilehdr(hdr0)
    XFORMAT_E_DEBUG("LEAVING")
    return ret

def xformat_write_archive_stats(xux, name, fd):
    buf = str(xux.bytes_written)
    buf2 = str(xux.bytes_written - xux.trailer_bytes)
    tmp = strob_open(10)
    if xux.output_format_code == af.arf_ustar:
        format1 = "ustar"
        if xux.taru.taru_tarheaderflags & tc.TARU_TAR_GNU_OLDGNUTAR:
            format1 = "gnutar"
    elif xux.output_format_code == af.arf_newascii:
        format1 = "newc"
    elif xux.output_format_code == af.arf_crcascii:
        format1 = "crc"
    elif xux.output_format_code == af.arf_oldascii:
        format1 = "odc"
    else:
        format1 = "unknown"
    swlib_writef(fd, tmp, f"{name}: {format1} vol 1, {buf2}+{xux.trailer_bytes} {buf} octets out.\n", 2)
    strob_close(tmp)

def xformat_write_trailer(xux):
    ret = 0
    XFORMAT_E_DEBUG("")
    if xux.defer and xux.output_format_code != af.arf_ustar:
        ret = defer_writeout_final_defers(xux.defer, xux.ofd)
        if ret < 0:
            return ret
    ret1 = taru_write_archive_trailer(xux.taru, xux.output_format_code, xux.ofd, 0, xux.bytes_written,
                                      xux.taru.taru_tarheaderflags)
    if ret1 < 0:
        return ret1
    ret += ret1
    xux.bytes_written += ret
    XFORMAT_E_DEBUG("LEAVING")
    xux.trailer_bytes = ret
    return ret

def xformat_u_open_file(xux, name):
    fd = swvarfs_u_open(xux.swvarfs, name)
    XFORMAT_E_DEBUG("")
    if fd <= 0:
        return fd
    fd = u_open_file_common(xux, fd)
    XFORMAT_E_DEBUG("LEAVING")
    return fd

def xformat_u_lstat(xux, path, st):
    XFORMAT_E_DEBUG("")
    return swvarfs_u_lstat(xux.swvarfs, path, st)

def xformat_u_fstat(xux, fd, st):
    XFORMAT_E_DEBUG("")
    return swvarfs_u_fstat(xux.swvarfs, fd, st)

def xformat_u_readlink(xux, path, buf, bufsize):
    XFORMAT_E_DEBUG("")
    return swvarfs_u_readlink(xux.swvarfs, path, buf, bufsize)

def xformat_u_close_file(xux, fd):
    XFORMAT_E_DEBUG("")
    return swvarfs_u_close(xux.swvarfs, fd)

def xformat_open_archive(xux, pathname, flags, mode):
    XFORMAT_E_DEBUG("")
    xux.swvarfs = swvarfs_open(pathname, flags, mode)
    if xux.swvarfs == None:
        return -1
    open_archive_init(xux)
    return 0

def xformat_open_archive_stdin(xux, pathname, flags):
    XFORMAT_E_DEBUG("")
    ret = internal_open_archive_file(0o0, xux, pathname, flags, 0)
    return ret

def xformat_open_archive_regfile(xux, pathname, flags, mode):
    XFORMAT_E_DEBUG("")
    ret = internal_open_archive_file(stat.S_IFREG, xux, pathname, flags, mode)
    return ret

def xformat_open_archive_dirfile(xux, pathname, flags, mode):
    XFORMAT_E_DEBUG("")
    ret = internal_open_archive_file(stat.S_IFDIR, xux, pathname, flags, mode)
    return ret

def xformat_close_archive(xux):
    ret = 0
    ret = swvarfs_close(xux.swvarfs)
    xux.swvarfs = None
    xux.ifd = -1
    return ret

def xformat_open_archive_by_fd_and_name(xux, fd, flags, mode, name):
    XFORMAT_E_DEBUG("")
    if name == None:
        return xformat_open_archive_by_fd(xux, fd, flags, mode)
    xux.swvarfs = swvarfs_opendup_with_name(fd, flags, mode, name)
    if xux.swvarfs == None:
        return -1
    open_archive_init(xux)
    return 0

def xformat_open_archive_by_fd(xux, fd, flags, mode):
    XFORMAT_E_DEBUG("")
    xux.swvarfs = swvarfs_opendup(fd, flags, mode)
    if xux.swvarfs == None:
        return -1
    open_archive_init(xux)
    return 0

def xformat_open_archive_by_swvarfs(xux, sfs):
    XFORMAT_E_DEBUG("")
    xux.swvarfs = sfs
    if xux.swvarfs == None:
        return -1
    open_archive_init(xux)
    xux.swvarfs_is_external = 1
    return 0

def xformat_read_header(xux):
    ret = 0
    hdr0 = ctypes.cast(xformat_vfile_hdr(xux), ctypes.POINTER(new_cpio_header)).contents
    XFORMAT_E_DEBUG("")
    ret = taru_read_header(xux.taru, hdr0, xux.ifd, xux.format_code, ctypes.byref(xux.eoaM), xux.taru.taru_tarheaderflags)
    xux.last_header_size = ret
    return ret

def xformat_unread_header(xux):
    ret = xux.last_header_size
    XFORMAT_E_DEBUG("")
    if xux.last_header_size <= 0:
        return -1
    XFORMAT_E_DEBUG2("lseeking back %d bytes.", ret)
    if uxfio_lseek(xux.ifd, -ret, uc.UXFIO_SEEK_VCUR) < 0:
        XFORMAT_E_FAIL("")
    xux.last_header_size = -1
    return ret

def xformat_read(xux, buf, count):
    XFORMAT_E_DEBUG("")
    return uxfio_sfread(xux.ifd, buf, count)

def xformat_copy_pass_thru(xux):
    XFORMAT_E_DEBUG("")
    return xformat_copy_pass(xux, xux.ofd, xux.ifd)

def xformat_copy_pass_by_dst(xux, dst_fd):
    XFORMAT_E_DEBUG("")
    return xformat_copy_pass(xux, dst_fd, xux.ifd)

def xformat_copy_pass_md5(xux, dst_fd, src_fd, md5buf):
    retval = 0
    digs = None
    old_digs = None
    file_hdr0 = xux.vfile_hdr()
    if (file_hdr0.c_mode & ft.CP_IFMT) == ft.CP_IFLNK:
        if xux.format_code <= af.arf_crcascii:
            return 0
    if dst_fd < 0:
        digs = None
        if taru_read_amount(src_fd, file_hdr0.c_filesize) != file_hdr0.c_filesize:
            retval = -3
    else:
        digs = taru_digs_create()
        taru_digs_init(digs, chc.DIGS_ENABLE_OFF, 0)
        digs.do_md5 = chc.DIGS_ENABLE_ON
        old_digs = file_hdr0.digsM
        file_hdr0.digs = digs
        retval = taru_write_archive_member_data(xux.taru, file_hdr0, dst_fd, src_fd, None, xux.output_format_code, -1, digs)
        file_hdr0.digs = old_digs
    if uxfio_getfd(xux.ifd, None) == src_fd or xux.ifd == src_fd:
        taru_tape_skip_padding(xux.ifd, file_hdr0.c_filesize, xux.format_code)
    if md5buf and digs:
        md5buf = digs.md5
    if retval > 0:
        xux.bytes_written += retval
    if digs:
        taru_digs_delete(digs)
    return retval

def xformat_copy_pass(xux, dst_fd, src_fd):
    ret = xformat_copy_pass_digs(xux, dst_fd, src_fd, None)
    return ret

def xformat_copy_pass_digs(xux, dst_fd, src_fd, digs):
    retval = 0
    file_hdr0 = xux.vfile_hdr()
    if (file_hdr0.c_mode & ft.CP_IFMT) == ft.CP_IFLNK:
        if xux.format_code <= af.arf_crcascii:
            return 0
    if dst_fd < 0:
        if taru_read_amount(src_fd, file_hdr0.c_filesize) != file_hdr0.c_filesize:
            retval = -3
    else:
        retval = taru_write_archive_member_data(xux.taru, file_hdr0, dst_fd, src_fd, None, xux.output_format_code, -1, digs)
    if uxfio_getfd(xux.ifd, None) == src_fd or xux.ifd == src_fd:
        taru_tape_skip_padding(xux.ifd, file_hdr0.c_filesize, xux.format_code)
    if retval > 0:
        xux.bytes_written += retval
    return retval

def xformat_copy_pass_file_data(xux, dst_fd, src_fd):
    retval = 0
    file_hdr0 = xux.vfile_hdr()
    if (file_hdr0.c_mode & ft.CP_IFMT) == ft.CP_IFLNK:
        if xux.format_code <= af.arf_crcascii:
            return 0
    if dst_fd < 0:
        if taru_read_amount(src_fd, file_hdr0.c_filesize) != file_hdr0.c_filesize:
            retval = -3
    else:
        retval = taru_write_archive_file_data(xux.taru, file_hdr0, dst_fd, src_fd, None, xux.output_format_code, -1)
    if uxfio_getfd(xux.ifd, None) == src_fd or xux.ifd == src_fd:
        taru_tape_skip_padding(xux.ifd, file_hdr0.c_filesize, xux.format_code)
    if retval > 0:
        xux.bytes_written += retval
    return retval

def xformat_copy_pass2(xux, dst_fd, src_fd, adjunct_ofd):
    retval = 0
    file_hdr0 = xux.vfile_hdr()
    if (file_hdr0.c_mode & ft.CP_IFMT) == ft.CP_IFLNK:
        if xux.format_code <= af.arf_crcascii:
            return 0
    if dst_fd < 0:
        retval = -3
    else:
        retval = taru_write_archive_member_data(xux.taru, file_hdr0, dst_fd, src_fd, None, xux.output_format_code, adjunct_ofd, None)
    if uxfio_getfd(xux.ifd, None) == src_fd or xux.ifd == src_fd:
        taru_tape_skip_padding(xux.ifd, file_hdr0.c_filesize, xux.format_code)
    if retval > 0:
        xux.bytes_written += retval
    return retval
