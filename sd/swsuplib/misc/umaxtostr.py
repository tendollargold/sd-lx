# inttostr.py -- convert integers to printable strings
# umaxtostr.py
#   Copyright (C) 2001, 2006 Free Software Foundation, Inc.
#   Copyright (C) 2024 Paul Weber Conversion to Python
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

from ctypes import c_int, sizeof
CHAR_BIT = 8

def inttostr(i, buf):
    p = buf + int_strlen_bound(c_int)
    p.value = 0
    if i < 0:
        while i != 0:
            p.value = ord('0') - i % 10
            i /= 10
            p -= 1
        p.value = ord('-')
    else:
        while i != 0:
            p.value = ord('0') + i % 10
            i /= 10
            p -= 1
    return ''.join(p)

def signed_type_or_expr__(t):
    return type_signed(type (t))
def int_strlen_bound(t):
    return (sizeof(t) * 8 - signed_type_or_expr__ (t)) * 146 / 485 + signed_type_or_expr__ (t) + 1

def type_signed(t):
    return not t

def umaxtostr(i, buf):
    inttostr(i, buf)
