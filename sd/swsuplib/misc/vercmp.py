
# Copyright (C) 2023 Paul Weber Convert to python

# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.# In Memoriam: Steve Taylor <staylor@redhat.com> was here, now he's not.
#
# Version Compare is a Python Module!!!


from enum import Enum
from sd.swsuplib.misc.cstrings import strcmp


class RpmMachtable(Enum):
    RPM_MACHTABLE_INSTARCH = 0  # !< Install platform architecture.
    RPM_MACHTABLE_INSTOS = 1  # !< Install platform operating system.
    RPM_MACHTABLE_BUILDARCH = 2  # !< Build platform architecture.
    RPM_MACHTABLE_BUILDOS = 3  # !< Build platform operating system.


def swlib_rpmvercmp(a, b):
    def xisalnum(ch):
        return ch.isalnum()

    def xisdigit(ch):
        return ch.isdigit()

    def xisalpha(ch):
        return ch.isalpha()

    if a == b:
        return 0

    str1 = a
    str2 = b
    one = str1
    two = str2

    while one and two:
        while one and not xisalnum(one):
            one = one[1:]
        while two and not xisalnum(two):
            two = two[1:]
        str1 = one
        str2 = two

        if xisdigit(str1):
            while str1 and xisdigit(str1):
                str1 = str1[1:]
            while str2 and xisdigit(str2):
                str2 = str2[1:]
            isnum = True
        else:
            while str1 and xisalpha(str1):
                str1 = str1[1:]
            while str2 and xisalpha(str2):
                str2 = str2[1:]
            isnum = False

        str1 = '\0'
        str2 = '\0'

        if one == str1:
            return -1

        if two == str2:
            return 1 if isnum else -1

        if isnum:
            while one == '0':
                one = one[1:]
            while two == '0':
                two = two[1:]

            if len(one) > len(two):
                return 1
            if len(two) > len(one):
                return -1

        rc = strcmp(one, two)
        if rc:
            return rc
