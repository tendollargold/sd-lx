# phases.swmodify.py
# SD phase control of software installation, update, removal
#
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.
#
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

"""
    4.0 Execution Phase Systems Management: Distributed Software Administration
    swmodify utility Page 108 - 110

    Check for extended options
    Extended Options
    The swmodify utility supports the following extended options. The description in
    Chapter 3 applies.
"""

swmodifyoptions = {
    'distribution_target_directory': '/var/spool/sw',
    'installed_software_catalog': '/var/adm/sw/products',
    'files': "",
    'logfile': "/var/adm/sw/swmodify.log",
    'loglevel': "1",
    'patch_commit': bool(False),
    'select_local': bool(True),
    'software': "",
    'targets': "",
    'verbose': "1"
}


def swmodify_options():
    """Collection of options for swmodify utility. api"""
    swmodifyoptions.update(globals())

# The swmodify utility consists of three phases:
#  1. Selection Phase
#  2. Analysis Phase
#  3. Execution Phase

#
# Selection Phase
#

#  · Specifying the Source

#    The source selection differs from the general information in Chapter 3 in that 
#    the source is a catalog file, or set of catalog files in the software packaging 
#    layout format instead of a distribution, so there is no access control events 
#    for accessing the catalog file.

#    If the file parsing discovers syntax errors, or missing but required attributes,
#    then generate an event.
#    [SW_ERROR: SW_SOURCE_ACCESS_ERROR]

#  · Software Selections

#    If there are no software selections specified, then all software from the catalog
#    is processed.  Otherwise, each selection added to the selected software list must
#    satisfy the following validation checks. If any of these checks result in an 
#    error, the selection is not added to the list.

#    — If the selection is not available from the catalog file, generate an event.
#      [SW_ERROR: SW_SELECTION_NOT_FOUND]

#    — If a unique version can not be identified, generate an event.
#      [SW_ERROR: SW_SELECTION_NOT_FOUND_AMBIG]

#
# Analysis Phase 
#
# See Chapter 3.

#
# Execution Phase
#

# The execution phase modifies the target catalog. Certain errors can occur when 
# modifying the catalog:

# — If a file can not be found in order to look up its attributes for modifying the 
#   catalog, then generate an event.
#   [SW_ERROR: SW_FILE_NOT_FOUND]

# — More complex rules apply when modifying attributes that inherit from the product 
#   to the fileset level. The filesets and is_locatable attributes are updated only by
#   swpackage and swmodify. If a fileset definition is removed with swmodify, the 
#   filesets attribute is updated.

# — If swmodify is used to change a fileset is_locatable attribute, then the 
#   corresponding product attribute is recalculated.

#  · Patches

#    To recover disc space, users may wish to remove versions of files saved by 
#    installation of patches after they are comfortable with the operation of a patch.
#    To commit a patch (remove the rollback files), swmodify is invoked on the patch 
#    by setting the option patch_commit=true.
