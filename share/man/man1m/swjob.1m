.\" $Header: rcsheader placeholder $
.\" #BrickHeader# swjob.1m # 2023/08/20 # #
.\" Removed .ds statements and other unnecessary comment lines
.TA s
.TH swjob 1M
.SH NAME
swjob, sd \- display and monitor job information, create and remove jobs;
invoke graphical user interface to display and monitor job information
and create and remove jobs, respectively
.SH SYNOPSIS
.HP
.C swjob
.B [\| \-i \|]
.B [\| \-R \|]
.B [\| \-u \|]
.B [\| \-v \|]
.B [\| \-a\|
.IR attribute\| \|]
.B [\| \-C\|
.IR session_file\| \|]
.ifn .br
.B [\| \-f\|
.IR jobid_file\| \|]
.B [\| \-S\|
.IR session_file\| \|]
.ift .br
.B [\| \-t\|
.IR target_file\| \|]
.ifn .br
.B [\| \-x\|
.IR option=value\| \|]
.B [\| \-X\|
.IR option_file\| \|]
.RI \c
.RI \0[\| jobid(s) \|]
.ifn .br
.B [\| @\|
.IR target_selections\| \|]
.HP
.C sd
.RI [\| "XToolkit Options" \|]
.B [\| \-x\|
.IR option=value\| \|]
.B [\| \-X\|
.IR option_file\| \|]
.SS Remarks
.RS
.TP 3
\(bu
The
.C sd
command invokes an interactive interface to the same functionality
that
.C swjob
provides.  See
.CT "Interactive Operation"
below for more details.
.TP
\(bu
This command supports operation on remote systems.  See
.CT "Remote Operation"
below for details.
.TP
\(bu
For an overview of all SD commands, see the
.IR sd (5)
man page by
typing
.CR "man 5 sd"
on the command line.
.RE
.SH DESCRIPTION
.PP
The
.C swjob
command displays job information and removes jobs.  It supports
these features:
.RS
.TP 3
\(bu
Display the current install jobs, copy jobs, and other SD jobs initiated
by the SD commands.
.TP
\(bu
Specify a specific job to list or remove.
.TP
\(bu
Display the command logfile for a specific job.
.TP
\(bu
Display the target logfile for a specific target.
.RE
.SS Remote Operation
You can enable Software Distributor (SD) to manage software on remote
systems.  To let the root user from a central SD
.I controller
(also called the
.I "central management server"
or
.IR "manager node" ")"
perform operations on a remote
.I target
(also called the
.I "host"
or
.IR "agent" "):"
.P
.RS 0
.TP 4
1)
Set up the root, host, and template Access Control Lists (ACLs) on the
remote machines to permit root access from the controller system.  To do
this, run the following command on each remote system:
.IP
.CI "/usr/lib/sw/mx/setaccess " "controller"
.IP
NOTES:
.RS
.TP 3
\(bu
.I controller
is the name of the central management server.
.TP
\(bu
If remote system is 11.00, make sure SD patch PHCO_22526 or a
superseding patch is installed on remote system before running
.CR setaccess .
.TP
\(bu
If remote system is older than 11.00 or for some other
reason does not have
.CR setaccess
in place, copy
.CR setaccess
script from an
11.11 or higher system to the remote system.
.RE
.TP
2)
.CR swinstall ,
.CR swcopy ,
and
.CR swremove
have enhanced GUI interfaces for remote operations.  Enable
the enhanced GUIs by creating the
.CR ".sdkey"
file on the controller.  Use this command:
.IP
.C "touch /var/adm/sw/.sdkey"
.\"
.RE
.PP
NOTE: You can also set up remote access by using
.CD swacl
directly on the remote machines to grant root or non-root access to
users from the controller system.
.SS Interactive Operations
The
.CR sd
command is an interactive interface for monitoring and scheduling
software jobs.  It provides the same functionality as the
.CR swjob
command.  You can also use
.CR sd
to invoke the
.CR swinstall ,
.CR copy ,
and
.CR swremove
GUIs.
.PP
If you have enabled SD's remote operations features,
.CR swinstall ,
.CR swcopy ,
and
.CR swremove
provide enhanced GUIs to support operations on remote targets.  See
.CT "Remote Operation"
above for details about enabling remote operations and the enhanced GUIs.
.SS Options
When no options or operands are specified,
.C swjob
lists the jobs that exist on the local host.  These jobs may be pending,
active, in the background or completed.
The
.C swjob
command supports the following options:
.RS
.TP 15
.IR "XToolKit Options"
The
.CD sd
command supports a subset of the standard XToolkit options to control
the appearance of the system GUI.  The supported options are:
.CR \-bg ,
.CR \-background ,
.CR \-fg ,
.CR \-foreground ,
.CR \-display ,
.CR \-name ,
.CR \-xrm ,
and
.CR \-synchronous .
See the
.IR X (1)
man page by typing
.C man X
for a definition of these options.
.TP
.C -i
Runs the command in interactive mode (invokes the GUI.)
(Using this option is an alias for the
.CR sd
command.)
See
the
.CT "Interactive Operation"
and
.CT "Remote Operation"
headings above for details.
.TP
.C -R
Applies to target lists as a shorthand for
.CR "@ *::*" .
.TP
.C -u
Causes
.C swjob
to remove the specified job(s).
.TP
.C -v
Causes
.C swjob
to list all available attributes, one per line.  The option applies
only to the default list.
.TP
.CI -a \0attribute
Each job has its own set of attributes.  These attributes include such things
as job title, schedule date, or results.  The
.C -a
option selects a specific
attribute to display.  You can specify multiple
.C -a
options to display
multiple attributes.  See also
.IR sd (4)
for details on these attributes.  This option applies only to the
default list.
.IP
The logfiles summarizing a job or detailing target actions
can be displayed using
.CR "-a log" ,
if
.C -a log
is specified and no other attribute is specified (that is, no other attribute
may be specified).
.TP
.CI -C \0session_file
Save the current options and operands only to the
.IR session_file .
You can enter a relative or absolute path with the file name.
The default directory for session files is
.CR $HOME/.sw/sessions/ .
Without this option, by default, the session file is saved only
in the default directory
.CR $HOME/.sw/sessions/ .
.IP
You can recall a session file with the
.C -S
option.
.TP
.CI -f \0jobid_file
Read the list of
.I jobids
from
.I jobid_file
instead of (or in addition to) the command line.
.TP
.CI -t \0target_file
Read the list of
.I target_selections
from
.I target_file
instead of (or in addition to) the command line.
.TP
.CI -x \0option=value
Set the session
.I option
to
.I value
and override the default value (or a value in an alternate
.IR option_file
specified with
the
.C -X
option).
Multiple
.C -x
options can be specified.
.TP
.CI -S \0session_file
Execute
.C swjob
based on the options and operands saved from a previous session,
as defined in
.IR session_file .
You can save session information to a file with the
.C -C
option.
.TP
.CI -X \0option_file
Read the session options and behaviors from
.IR option_file .
.RE
.SS Operands
The
.C swjob
command supports two types of operands:
.EM "jobid"
followed by
.EM "target selections".
These operands are separated by the "at"
.B ( @ )
character.
This syntax
implies that the command operates on "jobid at targets".
.RS
.TP 3
\(bu
.CR jobid :
The
.C swjob
command supports the following syntax for each job id:
.IP
.C "jobid"
.TP
\(bu
target selections:
The
.CR swjob 
command supports the following syntax for each target selection:
.IP
.B [\|\f2host\fP\|][\| : \|][\| / \f2directory\f1\|]\fP\0|
.B [\| ./ \f2relative_path\fP\|]\0|
.B [\| ../ \f2relative_path\fP\|]
.TP
\(bu
target selections with IPv6 address:
The
.CR swjob
command on HP-UX Release 11i v3
supports IPv6 address in the target selections.
The syntax is:
.IP
.C "\f1[\|\fP\s-1\f2IPv6_host\fP\s0\f1\|]\fP\f1[\|\fP:\f1\|]\fP\f1[\|\fP\s-1\f2directory\fP\s0\f1\|]\fP"
.IP
If both the hostname and the path are specified,
then the first occurrence of a slash
.B ( / )
is treated as the separator. 
.IP
The IPv6 address can optionally be enclosed in a pair of square brackets
.B ( [\| )
and
.B ( \|] ).
.RE
.ift .ne 9
.br
.SH EXTERNAL INFLUENCES
.SS Default Options
.PP
In addition to the standard options, several SD behaviors and policy options
can be changed by editing the default values found in:
.RS
.TP 30
.C /var/adm/sw/defaults
the system-wide default values.
.TP
.C $HOME/.swdefaults
the user-specific default values.
.RE
.P
Values must be specified in the defaults file using this syntax:
.PP
.RS
.nf
.C "\f1[\|\fP\s-1\f2command_name\fP\s0.\f1\|]\fP\s-1\f2option\fP\s0=\s-1\f2value\fP\s0"
.fi
.RE
.PP
The optional
.EM "command_name"
prefix denotes one of the SD commands.  Using the prefix limits the
change in the default value to that command.  If you leave the prefix
off, the change applies to all commands.
.PP
You can also override default values from the command line with the
.C "-x"
or
.C "-X"
options:
.P
.RS
.nf
.IC "command " "-x " "option" "=" "value"
.P
.IC "command " "-X " "option_file"
.fi
.RE
.PP
The following section lists all of the keywords supported by the
.C swjob
command.  If a default value exists,
it is listed after the
.CR = .
.PP
The policy options that apply to
.C swjob
are:
.PP
.RS
.TP 10
.PD 0
.CR "admin_directory=/var/adm/sw" " (for normal mode)"
.PD 0
.TP
.CR "admin_directory=/var/home/LOGNAME/sw" " (for nonprivileged mode)"
.PD
The location for SD logfiles and the default parent directory for the
installed software catalog.  The default value is
.CR "/var/adm/sw"
for normal SD operations.  When SD operates in nonprivileged mode
(that is, when the
.CR run_as_superuser
default option is set to
.CR "true" "):"
.RS
.TP 3
\(bu
The default value is forced to
.CR "/var/home/LOGNAME/sw" .
.TP
\(bu
The path element
.CR "LOGNAME"
is replaced with the name of the invoking user, which SD reads from
the system password file.
.TP
\(bu
If you set the value of this option to
.CR "HOME/"\c
.IR "path" ","
SD replaces
.CR HOME
with the invoking user's home directory (from the system password
file) and resolves
.I path
relative to that directory.  For example,
.CR HOME/my_admin
resolves to the
.CR my_admin
directory in your home directory.
.\" This doesn't apply because the installed_software_catalog doesn't
.\" apply to swjob: 4/30/00, 11.11
.\" .TP
.\" \(bu
.\" If you set the value of the
.\" .C installed_software_catalog
.\" default option to a relative path, that path is resolved relative to
.\" the value of this option.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.
.IP
See also the
.CR run_as_superuser
option.
.TP
.C agent_timeout_minutes=10000
Causes a target agent to exit if it has been inactive for the
specified time.  This can be used to make target agents more quickly
detect lost network connections since RPC can take as long as 130
minutes to detect a lost connection.  The recommended value is the
longest period of inactivity expected in your environment.  For command
line invocation, a value between 10 minutes and 60 minutes is
suitable.  A value of 60 minutes or more is recommended when the GUI
will be used.  The default of 10000 is slightly less than 7 days.
.TP
.C lookupcache_timeout_minutes=10
Controls the time in minutes to cache and re-use the results of hostname
or IP address resolution lookups.  A value of 0 disables the facility to
cache and re-use lookup results.  The maximum value allowed is 10080 minutes,
which is one week.
.IP
A value of:
.RS
.PD 0
.TP 10
.C 0
disables the lookup caching mechanism.
.TP
.C 10080
is the maximum value allowed.
.PD
.RE
.TP
.C one_liner={jobid operation state progress results title}
Defines the attributes which will be listed for each job when no
.C -a
option is specified.  Each attribute included in the
.C one_liner
definition
is separated by <tab> or <space>.  Any attributes, except
.C log
may be included in the
.C one_liner
definition.  If a particular attribute
does not exist for an object, that attribute is silently ignored.
.TP
.C return_warning_exitcode=false
This option controls the exit code returned by SD's controller commands.
This option is applicable only for a single target operation,
and ignored when multiple targets are used.
.IP
When set to the default value of
.CR false ,
swjob returns:
.RS
.PD
.TP 3
0
If there were no errors, with or without warnings.
.TP
1
If there were errors.
.P 0
When set to
.CR true ,
swjob returns :
.TP 3
0
If there were no warnings and no errors.
.TP
1
If there were errors.
.TP
2
If there were warnings but no errors.
.PD
.RE
.TP
.C rpc_binding_info=ncacn_ip_tcp:[\|2121\|] ncadg_ip_udp:[\|2121\|]
Defines the protocol sequence(s) and endpoint(s) on which the daemon
listens and the other commands contact the daemon.  If the connection
fails for one protocol sequence, the next is attempted.  SD supports
both the tcp
.B ( ncacn_ip_tcp:[\|2121\|] )
and udp
.B ( ncadg_ip_udp:[\|2121\|] )
protocol sequence on most platforms.  See the
.IR sd (5)
man page by typing
.C man 5 sd
for more information.
.TP
.C rpc_timeout=5
Relative length of the communications timeout.  This is a value in the
range from 0 to 9 and is interpreted by the DCE RPC.  Higher values
mean longer times; you may need a higher value for a slow or busy
network.  Lower values will give faster recognition on attempts to
contact hosts that are not up or not running
.CR swagentd .
Each value is approximately twice as long as the preceding value.
A value of 5 is about 30 seconds for the
.C ncadg_ip_udp
protocol sequence.
This option may not have any noticeable impact when using the
.C ncacn_ip_tcp
protocol sequence.
.TP
.C run_as_superuser=true
This option controls SD's nonprivileged mode.  This option is ignored
(treated as true) when the invoking user is super-user.
.IP
When set to the default value of true, SD operations are performed
normally, with permissions for operations either granted to a local
super-user or set by SD ACLs.  (See
.IR swacl (1M)
for details on ACLs.)
.IP
When set to false and the invoking user is local and is
.I not
super-user, nonprivileged mode is invoked:
.RS
.TP 3
\(bu
Permissions for operations are based on the user's file system
permissions.
.TP
\(bu
SD ACLs are ignored.
.TP
\(bu
Files created by SD have the uid and gid of the invoking user, and the
mode of created files is set according to the invoking user's umask.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.
.IP
See also the
.CR admin_directory
option.
.TP
.C targets=
Defines the default
.IR target_selections .
There is no supplied default.
If there is more than one target selection, they must be separated by spaces.
.TP
.C verbose=0
Controls the verbosity of the output (stdout).  A value of:
.RS
.PD 0
.TP 4
.C 0
disables output to stdout.  (Error and warning messages
are always written to stderr).
.TP
.C 1
enables verbose messaging to stdout.
.RE
.PD
.RE
.SS Session File
Each invocation of the
.C swjob
command defines a job display session.
The invocation options, source information, software selections, and
target hosts are saved before the installation or copy task actually
commences.
This lets you re-execute the command even if the session ends before
proper completion.
.PP
Each session is automatically saved to the file
.CR $HOME/.sw/sessions/swjob.last .
This file is overwritten by each invocation of
.CR swjob .
.PP
You can also save session information to a specific file by executing
.C swjob
with the
.C -C
.I session_file
option.
.PP
A session file uses the same syntax as the defaults files.
You can specify an absolute path for the session file.  If you do
not specify a directory, the default location for a session file is
.CR "HOME/.sw/sessions/" .
.PP
To re-execute a session file, specify the session file as the argument for the
.C -S
.I session_file
option of
.CR swjob .
.PP
Note that when you re-execute a session file, the values in the session
file take precedence over values in the system defaults file.
Likewise, any command line options or parameters that you specify when
you invoke
.CR swjob
take precedence over the values in the session file.
.PP
.SS Environment Variables
SD programs are affected by external environment variables.
.PP
SD programs that execute control scripts set environment variables for
use by the control scripts.
.C swjob
does not set environmental variables, but it uses them.
.PP
Environment variables that affect the SD commands:
.RS
.TP 13
.EV LANG
Determines the language in which messages are displayed.
If
.EV LANG
is not specified or is set to the empty string, a
default value of
.C C
is used.
See the
.IR lang (5)
man page by typing
.C man 5 lang
for more information.
.IP
NOTE: The language in which the SD agent and daemon log messages
are displayed is set by the system configuration variable script,
.CR /etc/rc.config.d/LANG .
For example,
.CR /etc/rc.config.d/LANG ,
must be set to
.CR LANG=ja_JP.SJIS
or
.CR LANG=ja_JP.eucJP
to make the agent and daemon log messages display in Japanese.
.TP
.EV LC_ALL
Determines the locale to be used to override any values for locale
categories specified by the settings of
.EV LANG
or any environment variables beginning with
.CR LC_ .
.TP
.EV LC_CTYPE
Determines the interpretation of sequences of bytes of text data as
characters (for example, single versus multibyte characters in values for
vendor-defined attributes).
.TP
.EV LC_MESSAGES
Determines the language in which messages should be written.
.TP
.EV LC_TIME
Determines the format of dates
.RI ( create_date
and
.IR mod_date )
when displayed by
.CR swlist .
Used by all utilities when displaying dates and times in
.CR stdout ,
.CR stderr ,
and
.CR logging .
.TP
.EV TZ
Determines the time zone for use when displaying dates and times.
.RE
.SS Signals
The
.C swjob
command
catches the signals SIGQUIT and SIGINT.
If these signals are received,
.C swjob
prints a message, sends
a Remote Procedure Call (RPC) to the daemons to wrap up, and then exits.
.PP
Each agent will complete the list task before it wraps up.
.SH OPERATION
Different views of the job information are available.  The types of
listings that can be selected are given below.
.RS
.TP 3
\(bu
Default Listing
.PD 0
.TP
\(bu
Target Listing
.TP
\(bu
Logfile Listing
.PD
.RE
.SS "Default Listing"
.PP
If
.C swjob
is invoked with no options or operands, it lists all jobs
that are on the local host.  This listing contains one line for each job.
The line includes the job tag attribute
and all other attributes selected via the
.C one_liner
option.
.PP
Listing jobs on a remote controller is not supported.  If a
.IR jobid
is given, information for only that job is displayed.
.PP
.SS "Status Listing"
.PP
If a
.CR -R
or
.CI "@ " target_specification
is given, the targets for that job and their status are displayed.  By
default the status information includes Type, State, Progress and Results.
.PP
.SS "Logfile Listing"
.PP
One of the attributes "log" encompasses a variety of logfile types.
The type of logfile returned when the
.CR "-a log " attribute
is given depends on the operands given.  The types of logfiles:
.PP
.RS
.TP 30
No target_selections
Show the controller logfile (default).
.TP
@ target
Show the agent logfile.
.RE
.SH RETURN VALUES
The
.C swjob
command returns:
.RS
.TP 4
.C 0
The job information was successfully listed or the job was successfully removed.
.PD 0
.TP
.C 1
The list /remove operation failed for all
.IR jobids .
.PD 0
.TP
.C 2
The list /remove operation failed for some
.IR jobids .
.PD
.RE
.SH DIAGNOSTICS
The
.C swjob
command writes to stdout, stderr, and to the agent logfile.
.SS Standard Output
All listings are printed to stdout.
.SS Standard Error
The
.C swjob
command writes messages for all WARNING and ERROR conditions to stderr.
.br
.ne 5
.SS Logging
The
.C swjob
command does not log summary events.  It logs events about each
read task to the
.ne 5
.CR swagent
logfile associated with each
.IR target_selection .
.SS swagentd Disabled
If the
.CR swagentd
daemon has been disabled on the host, it can be enabled
by the host's system administrator by setting the
.CR SW_ENABLE_SWAGENTD
entry in
.CR /etc/rc.config.d/swconfig
to
.CR 1
and executing
.CR "/usr/sbin/swagentd -r" .
.SH EXAMPLES
To list all of the jobs that exist on the local host:
.P
.RS
.nf
.C "swjob"
.fi
.RE
.P
To show the scheduled date for job hostA-0001:
.P
.RS
.nf
.C "swjob -a schedule hostA-0001"
.fi
.RE
.P
For job hostA-0001 list the targets and their status:
.P
.RS
.nf
.C "swjob -R hostA-0001"
or
.C "swjob hostA-0001 @ *::*"
.fi
.RE
.P
For job hostA-0001 list the controller log:
.P
.RS
.nf
.C "swjob -a log hostA-0001"
.fi
.RE
.P
For job hostA-0001 list the targetA agent log:
.P
.RS
.nf
.C "swjob -a log targetA-0001 @ targetA"
.fi
.RE
.SH FILES
.RS 0
.TP
.C $HOME/.swdefaults
Contains the user-specific default values for some or all SD options.
.TP
.C /usr/lib/sw/sys.defaults
Contains the master list of current SD options (with their default values).
.TP
.C /var/adm/sw/
The directory which contains all of the configurable
(and non-configurable) data for SD.
This directory is also the default location of logfiles.
.TP
.C /var/adm/sw/defaults
Contains the active system-wide default values for some or all SD options.
.TP
.C /var/adm/sw/queue/
The directory which contains the information about all active and complete
install jobs, copy jobs, and other jobs initiated by the SD commands.
.RE
.SH AUTHOR
.C swjob
was developed by the Hewlett-Packard Company.
.SH SEE ALSO
install-sd(1M),
swacl(1M),
swagentd(1M),
swask(1M),
swconfig(1M),
swcopy(1M),
swinstall(1M),
swlist(1M),
swmodify(1M),
swpackage(1M),
swreg(1M),
swremove(1M),
swverify(1M),
sd(4),
swpackage(4),
sd(5).
.P
.CT "Software Distributor Administration Guide" ","
available at
.CR "http://docs.hp.com" .
.P
SD customer web site at
.\" JAGaf61810: Updated the URL for the SD customer web site.
.\"   NOTE TO LOCALIZERS: Currently, only the English version of this
.\"   web site exists.
.CR "http://docs.hp.com/en/SD/" .
.\"
.\" toc@\f3swjob(1M)\f1:\0\0\f4swjob\f1, \f4sd\f1@@@display job information, remove jobs, create and monitor jobs
.\"
.\" toc@\f4sd\f1:\0\0create and monitor jobs@@@\f1see \f3swjob(1M)\f1
.\"
.\" index@\f4swjob\f1 \- display job information and remove jobs@@@\f3swjob(1M)\f1
.\" index@\f4sd\f1 \- create and monitor jobs@@@\f3swjob(1M)\f1
.\"
.\" index@\f1display information and remove jobs@@@\f3swjob(1M)\f1
.\" index@\f1create and monitor jobs@@@\f3swjob(1M)\f1
.\"
.\" links@swjob.1m sd.1m
