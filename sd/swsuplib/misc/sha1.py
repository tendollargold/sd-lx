# Declarations of functions and data types used for SHA1 sum
#   library functions.
#   Copyright (C) 2000-2001, 2003, 2005-2006, 2008-2020 Free Software
#  Foundation, Inc.

#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 3, or (at your option) any
#   later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program; if not, see <https://www.gnu.org/licenses/>.  */
# Just use hashlib

SHA1_H = 1
import sys
import struct
import socket
import errno
from sd.util import MemoryAllocator
SHA1_DIGEST_SIZE = 20
BLOCKSIZE = 32768
MA = MemoryAllocator
UNALIGNED_P = lambda p: (sys.getsizeof(p) % struct.calcsize('I') != 0)
MAX_INT = sys.maxsize
SYS_BUFSIZE_MAX = MAX_INT >> 20 << 20
class sha1_ctx:
    def __init__(self):
        self.A = 0x67452301
        self.B = 0xefcdab89
        self.C = 0x98badcfe
        self.D = 0x10325476
        self.E = 0xc3d2e1f0
        self.total = [0, 0]
        self.buflen = 0
        self.buffer = [0] * 32

def sha1_init_ctx(ctx):
    ctx.A = 0x67452301
    ctx.B = 0xefcdab89
    ctx.C = 0x98badcfe
    ctx.D = 0x10325476
    ctx.E = 0xc3d2e1f0
    ctx.total[0] = ctx.total[1] = 0
    ctx.buflen = 0

def set_uint32(cp, v):
    struct.pack_into('>I', cp, 0, v)

def sha1_read_ctx(ctx, resbuf):
    r = resbuf
    set_uint32(r + 0 * struct.calcsize('I'), ctx.A)
    set_uint32(r + 1 * struct.calcsize('I'), ctx.B)
    set_uint32(r + 2 * struct.calcsize('I'), ctx.C)
    set_uint32(r + 3 * struct.calcsize('I'), ctx.D)
    set_uint32(r + 4 * struct.calcsize('I'), ctx.E)
    return resbuf

def sha1_finish_ctx(ctx, resbuf):
    bytes = ctx.buflen
    size = (bytes < 56) and 64 // 4 or 64 * 2 // 4

    ctx.total[0] += bytes
    if ctx.total[0] < bytes:
        ctx.total[1] += 1

    ctx.buffer[size - 2] = (ctx.total[1] << 3) | (ctx.total[0] >> 29)
    ctx.buffer[size - 1] = ctx.total[0] << 3
    fillbuf = b'\x00' * ((size - 2) * 4 - bytes)
    ctx.buffer.extend(fillbuf)

    sha1_process_block(ctx.buffer, size * 4, ctx)
    return sha1_read_ctx(ctx, resbuf)


def sha1_buffer(buffer, length, resblock):
    ctx = sha1_ctx()
    sha1_init_ctx(ctx)
    sha1_process_bytes(buffer, length, ctx)
    return # sha1_finish_ctx(ctx, resblock)

def sha1_process_bytes(buffer, len, ctx):
    if ctx.buflen != 0:
        left_over = ctx.buflen
        add = 128 - left_over if 128 - left_over > len else len
        ctx.buffer[left_over:left_over+add] = buffer[:add]
        ctx.buflen += add
        if ctx.buflen > 64:
            sha1_process_block(ctx.buffer, ctx.buflen & ~63, ctx)
            ctx.buflen &= 63
            ctx.buffer[:ctx.buflen] = ctx.buffer[(left_over + add) & ~63:(left_over + add) & ~63 + ctx.buflen]
        buffer = buffer[add:]
        len -= add
    if len >= 64:
        if UNALIGNED_P(buffer):
            while len > 64:
                sha1_process_block(buffer[:64], 64, ctx)
                buffer = buffer[64:]
                len -= 64
        else:
            sha1_process_block(buffer, len & ~63, ctx)
            buffer = buffer[len & ~63:]
            len &= 63
    if len > 0:
        left_over = ctx.buflen
        ctx.buffer[left_over:left_over+len] = buffer
        left_over += len
        if left_over >= 64:
            sha1_process_block(ctx.buffer, 64, ctx)
            left_over -= 64
            ctx.buffer[:left_over] = ctx.buffer[16:16+left_over]
        ctx.buflen = left_over

K1 = 0x5a827999
K2 = 0x6ed9eba1
K3 = 0x8f1bbcdc
K4 = 0xca62c1d6

def F1(B, C, D):
    return D ^ (B & (C ^ D))

def F2(B, C, D):
    return B ^ C ^ D

def F3(B, C, D):
    return (B & C) | (D & (B | C))

def F4(B, C, D):
    return B ^ C ^ D

def SWAP(n):
    return ((n << 24) & 0xFF000000) | ((n << 8) & 0x00FF0000) | ((n >> 8) & 0x0000FF00) | ((n >> 24) & 0x000000FF)
def sha1_process_block(buffer, len, ctx):
    words = struct.unpack(f'{len // 4}I', buffer)
    nwords = len // 4
    endp = words + nwords
    x = [0] * 16
    a = ctx['A']
    b = ctx['B']
    c = ctx['C']
    d = ctx['D']
    e = ctx['E']
    lolen = len

    ctx['total'][0] += lolen
    ctx['total'][1] += (len >> 31 >> 1) + (ctx['total'][0] < lolen)

    def rol(x, n):
        return ((x << n) | (x >> (32 - n))) & 0xffffffff

    def M(I):
        tm = x[I & 0x0f] ^ x[(I - 14) & 0x0f] ^ x[(I - 8) & 0x0f] ^ x[(I - 3) & 0x0f]
        x[I & 0x0f] = rol(tm, 1)

    def R(A, B, C, D, E, F, K, M):
        nonlocal a, b, c, d, e
        E += rol(A, 5) + F(B, C, D) + K + M
        B = rol(B, 30)

    while words < endp:
        for t in range(16):
            x[t] = words[t]
        R(a, b, c, d, e, F1, K1, x[ 0])
        R(e, a, b, c, d, F1, K1, x[ 1])
        R(d, e, a, b, c, F1, K1, x[ 2])
        R(c, d, e, a, b, F1, K1, x[ 3])
        R(b, c, d, e, a, F1, K1, x[ 4])
        R(a, b, c, d, e, F1, K1, x[ 5])
        R(e, a, b, c, d, F1, K1, x[ 6])
        R(d, e, a, b, c, F1, K1, x[ 7])
        R(c, d, e, a, b, F1, K1, x[ 8])
        R(b, c, d, e, a, F1, K1, x[ 9])
        R(a, b, c, d, e, F1, K1, x[10])
        R(e, a, b, c, d, F1, K1, x[11])
        R(d, e, a, b, c, F1, K1, x[12])
        R(c, d, e, a, b, F1, K1, x[13])
        R(b, c, d, e, a, F1, K1, x[14])
        R(a, b, c, d, e, F1, K1, x[15])
        R(e, a, b, c, d, F1, K1, M(16))
        R(d, e, a, b, c, F1, K1, M(17))
        R(c, d, e, a, b, F1, K1, M(18))
        R(b, c, d, e, a, F1, K1, M(19))
        R(a, b, c, d, e, F2, K2, M(20))
        R(e, a, b, c, d, F2, K2, M(21))
        R(d, e, a, b, c, F2, K2, M(22))
        R(c, d, e, a, b, F2, K2, M(23))
        R(b, c, d, e, a, F2, K2, M(24))
        R(a, b, c, d, e, F2, K2, M(25))
        R(e, a, b, c, d, F2, K2, M(26))
        R(d, e, a, b, c, F2, K2, M(27))
        R(c, d, e, a, b, F2, K2, M(28))
        R(b, c, d, e, a, F2, K2, M(29))
        R(a, b, c, d, e, F2, K2, M(30))
        R(e, a, b, c, d, F2, K2, M(31))
        R(d, e, a, b, c, F2, K2, M(32))
        R(c, d, e, a, b, F2, K2, M(33))
        R(b, c, d, e, a, F2, K2, M(34))
        R(a, b, c, d, e, F2, K2, M(35))
        R(e, a, b, c, d, F2, K2, M(36))
        R(d, e, a, b, c, F2, K2, M(37))
        R(c, d, e, a, b, F2, K2, M(38))
        R(b, c, d, e, a, F2, K2, M(39))
        R(a, b, c, d, e, F3, K3, M(40))
        R(e, a, b, c, d, F3, K3, M(41))
        R(d, e, a, b, c, F3, K3, M(42))
        R(c, d, e, a, b, F3, K3, M(43))
        R(b, c, d, e, a, F3, K3, M(44))
        R(a, b, c, d, e, F3, K3, M(45))
        R(e, a, b, c, d, F3, K3, M(46))
        R(d, e, a, b, c, F3, K3, M(47))
        R(c, d, e, a, b, F3, K3, M(48))
        R(b, c, d, e, a, F3, K3, M(49))
        R(a, b, c, d, e, F3, K3, M(50))
        R(e, a, b, c, d, F3, K3, M(51))
        R(d, e, a, b, c, F3, K3, M(52))
        R(c, d, e, a, b, F3, K3, M(53))
        R(b, c, d, e, a, F3, K3, M(54))
        R(a, b, c, d, e, F3, K3, M(55))
        R(e, a, b, c, d, F3, K3, M(56))
        R(d, e, a, b, c, F3, K3, M(57))
        R(c, d, e, a, b, F3, K3, M(58))
        R(b, c, d, e, a, F3, K3, M(59))
        R(a, b, c, d, e, F4, K4, M(60))
        R(e, a, b, c, d, F4, K4, M(61))
        R(d, e, a, b, c, F4, K4, M(62))
        R(c, d, e, a, b, F4, K4, M(63))
        R(b, c, d, e, a, F4, K4, M(64))
        R(a, b, c, d, e, F4, K4, M(65))
        R(e, a, b, c, d, F4, K4, M(66))
        R(d, e, a, b, c, F4, K4, M(67))
        R(c, d, e, a, b, F4, K4, M(68))
        R(b, c, d, e, a, F4, K4, M(69))
        R(a, b, c, d, e, F4, K4, M(70))
        R(e, a, b, c, d, F4, K4, M(71))
        R(d, e, a, b, c, F4, K4, M(72))
        R(c, d, e, a, b, F4, K4, M(73))
        R(b, c, d, e, a, F4, K4, M(74))
        R(a, b, c, d, e, F4, K4, M(75))
        R(e, a, b, c, d, F4, K4, M(76))
        R(d, e, a, b, c, F4, K4, M(77))
        R(c, d, e, a, b, F4, K4, M(78))
        R(b, c, d, e, a, F4, K4, M(79))
        a = ctx.A
        a += a
        b = ctx.B
        b += b
        c = ctx.C
        c += c
        d = ctx.D
        d += d
        e = ctx.E
        e += e


def alg_socket(alg):
    salg = {
        'salg_family': socket.AF_ALG,
        'salg_type': b'hash',
        'salg_name': alg.encode('utf-8')
    }

#    cfd = socket.socket(socket.AF_ALG, socket.SOCK_SEQPACKET | socket.SOCK_CLOEXEC, 0)
#    if cfd < 0:
#        return -errno.EAFNOSUPPORT
#    ofd = -1
#    if cfd.bind(salg) == 0:
#        ofd, _, _ = socket.socket(socket.AF_ALG, socket.SOCK_CLOEXEC, 0)
#    cfd.close()
#    return -errno.EAFNOSUPPORT if ofd < 0 else ofd

class sockaddr_alg:
    def __init__(self):
        self.salg_family = 0
        self.salg_type = [0] * 14
        self.salg_feat = 0
        self.salg_mask = 0
        self.salg_name = [0] * 64

class sockaddr_alg_new:
    def __init__(self):
        self.salg_family = 0
        self.salg_type = [0] * 14
        self.salg_feat = 0
        self.salg_mask = 0
        self.salg_name = []

class af_alg_iv:
    def __init__(self):
        self.ivlen = 0
        self.iv = []

ALG_SET_KEY = 1
ALG_SET_IV = 2
ALG_SET_OP = 3
ALG_SET_AEAD_ASSOCLEN = 4
ALG_SET_AEAD_AUTHSIZE = 5
ALG_SET_DRBG_ENTROPY = 6

ALG_OP_DECRYPT = 0
ALG_OP_ENCRYPT = 1

def afalg_buffer(buffer, len, alg, resblock, hashlen):
    if len == 0:
        return -errno.EAFNOSUPPORT
    ofd = socket.socket(socket.AF_ALG, socket.SOCK_SEQPACKET, 0)
 #   if ofd < 0:
 #       return ofd
  #  result = 0
    while True:
        size = min(len, BLOCKSIZE)
        if ofd.send(buffer[:size], socket.MSG_MORE) != size:
            result = -errno.EAFNOSUPPORT
            break
        buffer = buffer[size:]
        len -= size
        if len == 0:
            result = 0 if ofd.recv(hashlen) == hashlen else -errno.EAFNOSUPPORT
            break
    ofd.close()
    return result
