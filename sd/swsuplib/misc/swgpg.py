# swgpg.py  --  routines involving gpg
#
# Copyright (C) 2007 Jim Lowe
# Copyright (C) 2024 Paul Weber conversion to Python
# All Rights Reserved.
#
# COPYING TERMS AND CONDITIONS:
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#

import errno
import getpass
import os
import pty
import signal
import sys

from sd.debug import E_DEBUG3, E_DEBUG2, E_DEBUG
from sd.swsuplib.misc.cstrings import strstr, strcmp
from sd.swsuplib.misc.shcmd import (shcmd_open, shcmd_add_arg, shcmd_close, shcmd_find_in_path, shcmd_set_errfile,
                                    shcmd_get_argvector, shcmd_unix_exec, SHCMD_UNSET_EXITVAL,
                                    shcmd_set_srcfd, shcmd_set_dstfd, shcmd_apply_redirection,
                                    shcmd_set_envp)
from sd.swsuplib.misc.strar import strar_add, strar_get, strar_close, strar_reset, strar_open
from sd.swsuplib.misc.swfork import swfork
from sd.swsuplib.misc.swgp import swgp_signal
from sd.swsuplib.misc.swlib import (swlibc, swlib_strdup, swlib_writef, swlib_exec_filter, swlib_atoi,
                                    swlib_unix_dircat,
                                    swlib_pipe_pump, swlib_doif_writef, swlib_open_memfd, swlib_pump_amount,
                                    swlib_wait_on_all_pids)
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.strob import strob_str, strob_open, strob_strtok, strob_sprintf, strob_strcpy, strob_get_char, \
    strob_close, STROB_DO_APPEND
from sd.swsuplib.uxfio import (uc, uxfio_unix_safe_write, uxfio_get_dynamic_buffer, uxfio_write, uxfio_close,
                               uxfio_fcntl, uxfio_opendup)

g_swgpg_dirname = 0
g_swgpg_fifopath = 0
swgpg_passfd = 0


class SwGpgConstants:
    SWGPG_PASSPHRASE_LENGTH = 240
    SWGPG_SWP_PASS_AGENT = "agent"
    SWGPG_SWP_PASS_ENV = "env"
    ARMORED_SIGLEN = 1024  # signature length
    SWGPG_FIFO_DIR = "/tmp"
    SWGPG_FIFO_PFX = "sigfifo"
    SWGPG_FIFO_NAME = "sig"
    GPG_STATUS_PREFIX = "[GNUPG:] "
    SWGPG_GPG_BIN = "gpg"
    SWGPG_GPG2_BIN = "gpg2"
    SWGPG_PGP5_BIN = "pgps"
    SWGPG_PGP26_BIN = "pgp"
    GPG_STATUS_ERRSIG = "ERRSIG"
    GPG_STATUS_NO_PUBKEY = "NO_PUBKEY"
    GPG_STATUS_GOODSIG = "GOODSIG"
    GPG_STATUS_BADSIG = "BADSIG"
    GPG_STATUS_EXPSIG = "EXPSIG"
    GPG_STATUS_NODATA = "NODATA"
    SWGPG_SIG_VALID = 0
    SWGPG_SIG_NOT_VALID = 1
    SWGPG_SIG_NO_PUBKEY = 2
    SWGPG_SIG_NODATA = 3
    SWGPG_SIG_ERROR = 4  # swbis internal error
    FILENEEDDEBUG = 1


swgpgc = SwGpgConstants()


class SWGPG_VALIDATE:

    def __init__(self):
        # instance fields found by C++ to Python Converter:
        self.gpg_progname = '\0'
        self.list_of_sigs = None
        self.list_of_sig_names = None
        self.list_of_status_blobs = None
        self.list_of_logger_blobs = None
        self.status_array = None


def swgpg_create():
    w = SWGPG_VALIDATE()
    E_DEBUG("")
    if w is None:
        return None
    w.gpg_progname = swlib_strdup(swgpgc.SWGPG_GPG_BIN)
    w.list_of_sigs = strar_open()
    w.list_of_sig_names = strar_open()
    w.list_of_status_blobs = strar_open()
    w.list_of_logger_blobs = strar_open()
    w.status_array = strob_open(12)
    return w


def get_stderr_fd():
    return pty.STDERR_FILENO


def close_passfd():
    global swgpg_passfd
    if swgpg_passfd >= 0:
        try:
            os.close(swgpg_passfd)
        except OSError:
            pass


def make_dir(ma):
    print(ma + "is not used in swgpg make_dir function")
    filename = ""
    template = swgpgc.SWGPG_FIFO_DIR + "/" + swgpgc.SWGPG_FIFO_PFX + "XXXXXX"
    try:
        filename = os.path.join(swgpgc.SWGPG_FIFO_DIR, swgpgc.SWGPG_FIFO_PFX) + "XXXXXX"
        os.makedirs(filename)
    except OSError as e:
        print(swlib_utilname_get() + ": mkdtemp error for template [" + template + "]: " + str(e))
    return filename


def gpg_fifo_command(fifofilename, gpg_prog, uverbose, logger_fd):
    E_DEBUG("")
    cmd = shcmd_open()
    absname = shcmd_find_in_path(os.getenv("PATH"), gpg_prog)
    if not absname:
        print("swbis: {}: Not found in current path".format(gpg_prog), file=sys.stderr)
        return None
    if uverbose >= 3:
        tmp = strob_open(32)
        swlib_writef(logger_fd, tmp, "{}: using GNU Privacy Guard : {}\n".format(swlib_utilname_get(), absname), None)
        strob_close(tmp)
    shcmd_add_arg(cmd, absname)  # GPG command
    shcmd_add_arg(cmd, "--status-fd=1")

    E_DEBUG("")
    if logger_fd != pty.STDERR_FILENO:
        x = strob_open(32)
        strob_sprintf(x, 0, "--logger-fd=%d", logger_fd)
        shcmd_add_arg(cmd, strob_str(x))
        strob_close(x)

    shcmd_add_arg(cmd, "--verify")
    shcmd_add_arg(cmd, fifofilename)  # Other end of FIFO
    shcmd_add_arg(cmd, "-")
    if uverbose < swlibc.SWC_VERBOSE_2:
        shcmd_set_errfile(cmd, "/dev/null")
    E_DEBUG("")
    return cmd


def get_number_of_blobs(w):
    n = 0
    while strar_get(w.list_of_status_blobs, n) is not None:
        n += 1
    return n


def make_pgp_env(env, length):
    tmp = strob_open(2)

    for i in range(0, length):
        env[i] = str(None)

    env[0] = "PGPPASSFD=3"
    valu = os.getenv("HOME")
    if valu != '\0':
        strob_sprintf(tmp, 0, "HOME=%s", valu)
        env[1] = strob_str(tmp)

    valu = os.getenv("MYNAME")
    if valu != '\0':
        strob_sprintf(tmp, 0, "MYNAME=%s", valu)
        env[2] = strob_str(tmp)

    strob_close(tmp)
    return 0


def get_does_use_gpg_agent(cmd):
    args = shcmd_get_argvector(cmd)
    arg = args[0]
    while arg:
        if "--use-agent" in arg:
            return 1
        arg = args.pop(0)
    return 0


def swgpg_init_passphrase_fd():
    swgpg_set_passphrase_fd(-1)


def swgpg_set_passphrase_fd(fd):
    global swgpg_passfd
    swgpg_passfd = fd


def swgpg_get_passphrase_fd():
    return swgpg_passfd


def swgpg_delete(w):
    E_DEBUG("")

    del w.gpg_progname
    strar_close(w.list_of_sigs)
    strar_close(w.list_of_sig_names)
    strar_close(w.list_of_status_blobs)
    strar_close(w.list_of_logger_blobs)
    strob_close(w.status_array)
    E_DEBUG("")


def swgpg_show_all_signatures(w, fd):
    n = 0
    tmp = strob_open(100)
    while strar_get(w.list_of_sigs, n) is not None:
        swlib_writef(fd, tmp, "{}{}".format(strar_get(w.list_of_logger_blobs, n), strar_get(w.list_of_status_blobs, n)),
                     None)
        n += 1
    strob_close(tmp)
    return 0


def swgpg_show(w, index, sig_fd, status_fd, logger_fd):
    tmp = strob_open(100)
    num = get_number_of_blobs(w)
    E_DEBUG2("index is %d", index)
    E_DEBUG2("number of sigs is %d", num)
    if index < 0 or index >= num:
        return -1
    n = index

    s = strar_get(w.list_of_logger_blobs, n)
    if logger_fd > 0 and s != '\0':
        swlib_writef(logger_fd, tmp, "%s", s)

    s = strar_get(w.list_of_status_blobs, n)
    if status_fd > 0:
        swlib_writef(status_fd, tmp, "%s", s)

    s = strar_get(w.list_of_sigs, n)
    if sig_fd > 0:
        swlib_writef(sig_fd, tmp, "%s", s)

    strob_close(tmp)
    return 0


def swgpg_reset(swgpg):
    E_DEBUG("")
    strar_reset(swgpg.list_of_sigs)
    strar_reset(swgpg.list_of_sig_names)
    strar_reset(swgpg.list_of_status_blobs)
    strar_reset(swgpg.list_of_logger_blobs)
    strob_strcpy(swgpg.status_array, "")


def swgpg_get_status(w, index):
    ret = strob_get_char(w.status_array, index)
    return ret


def swgpg_get_number_of_sigs(w):
    """FIXME, this should be integrated with get_number_of_blob() above """
    n = 0
    E_DEBUG("")
    while strar_get(w.list_of_sigs, n) is not None:
        n += 1
    return n


def swgpg_set_status(w, index, value):
    w.status_array = [0] * (index + 1)
    w.status_array[index] = value


def swgpg_set_status_array(w):
    w.status_array = bytearray()
    i = 0
    while i < len(w.list_of_sigs):
        # sig = w.list_of_sigs[i]
        ret = swgpg_determine_signature_status(w.list_of_status_blobs[i], -1)
        swgpg_set_status(w, i, ret)
        i += 1


def swgpg_disentangle_status_lines(w, gpg_output_lines):
    E_DEBUG("")
    E_DEBUG("swgpg_disentangle_status_lines")
    tmp = strob_open(10)
    status_lines = strob_open(10)
    stderr_lines = strob_open(10)

    line = strob_strtok(tmp, gpg_output_lines, "\n\r")
    while line != '\0':
        E_DEBUG("")
        if strstr(line, swgpgc.GPG_STATUS_PREFIX) == line:
            strob_sprintf(status_lines, STROB_DO_APPEND, "%s\n", line)
        else:
            strob_sprintf(stderr_lines, STROB_DO_APPEND, "%s\n", line)
        line = strob_strtok(tmp, None, "\n\r")

    E_DEBUG("")
    strar_add(w.list_of_status_blobs, strob_str(status_lines))
    strar_add(w.list_of_logger_blobs, strob_str(stderr_lines))

    strob_close(tmp)
    strob_close(status_lines)
    strob_close(stderr_lines)
    E_DEBUG("")
    return 0


def swgpg_create_fifo(buf):
    global g_swgpg_fifopath, g_swgpg_dirname
    freeit = ""

    E_DEBUG("")
    if buf is None:
        return None
    if g_swgpg_dirname is not None:
        return None
    if g_swgpg_fifopath is not None:
        return None
    strob_strcpy(buf, "")
    dirname = make_dir(freeit)
    if (not dirname) != '\0':
        return None
    strob_strcpy(buf, dirname)
    g_swgpg_dirname = dirname
    swlib_unix_dircat(buf, swgpgc.SWGPG_FIFO_NAME)
    g_swgpg_fifopath = strob_str(buf)

    filename = g_swgpg_fifopath
    if filename:
        try:
            os.mkfifo(filename, 0o600)
        except OSError:
            print(f"{swlib_utilname_get()}: {os.strerror(1)}")

    E_DEBUG("")
    if freeit != '\0':
        del freeit
    return strob_str(buf)


def swgpg_remove_fifo():
    global g_swgpg_fifopath, g_swgpg_dirname
    if g_swgpg_dirname == "":
        return -1
    if g_swgpg_fifopath == "":
        return -1
    try:
        os.unlink(g_swgpg_fifopath)
    finally:
        del g_swgpg_fifopath
        g_swgpg_fifopath = ""
        try:
            os.rmdir(g_swgpg_dirname)
        finally:
            del g_swgpg_dirname
            g_swgpg_dirname = ""
    return  # ret


def swgpg_determine_signature_status(gpg_status_lines, which_sig):
    E_DEBUG("----------------------------------")
    E_DEBUG("----------------------------------")

    E_DEBUG2("%s", gpg_status_lines)

    E_DEBUG("----------------------------------")
    E_DEBUG("----------------------------------")
    tmp = strob_open(100)

    good_count = 0
    nodata = 0
    nokey = 0
    bad_count = 0
    line = strob_strtok(tmp, gpg_status_lines, "\n\r")
    E_DEBUG("")
    while line != '\0':
        # fprintf(stderr, "%s\n", line); 
        E_DEBUG2("LINE=[%s]", line)
        if line.find(swgpgc.GPG_STATUS_PREFIX) == 0:
            if line.find(swgpgc.GPG_STATUS_PREFIX + swgpgc.GPG_STATUS_GOODSIG) != -1:
                E_DEBUG("good_count++")
                good_count += 1
            elif line.find(swgpgc.GPG_STATUS_BADSIG):
                E_DEBUG("bad_count++")
                bad_count += 1
            elif line.find(swgpgc.GPG_STATUS_NO_PUBKEY):
                E_DEBUG("nokey++")
                nokey += 1
            elif line.find(swgpgc.GPG_STATUS_EXPSIG):
                E_DEBUG("bad_count++")
                bad_count += 1
            elif line.find(swgpgc.GPG_STATUS_NODATA):
                E_DEBUG("no_data++")
                nodata += 1
            else:
                pass  # allow all other lines ??
        else:
            pass
            E_DEBUG("do_nothing")
            # now stderr is mixed this output so just
            # ignore other lines
            # return SWGPG_SIG_ERROR; 
        line = strob_strtok(tmp, None, "\n\r")
    E_DEBUG("")
    strob_close(tmp)
    if (which_sig < 0 and good_count >= 1 and bad_count == 0 and nokey == 0 and nodata == 0) or (
            which_sig >= 0 and good_count == 1 and bad_count == 0 and nokey == 0 and nodata == 0):
        E_DEBUG("return SWGPG_SIG_VALID")
        return swgpgc.SWGPG_SIG_VALID
    elif good_count == 0 and bad_count == 0 and nokey > 0:
        E_DEBUG("return SWGPG_SIG_NO_PUBKEY")
        return swgpgc.SWGPG_SIG_NO_PUBKEY
    elif good_count == 0 and bad_count == 0 and nodata > 0:
        E_DEBUG("return SWGPG_SIG_NODATA")
        return swgpgc.SWGPG_SIG_NODATA
    else:
        E_DEBUG("return SWGPG_SIG_NOT_VALID")
        return swgpgc.SWGPG_SIG_NOT_VALID


"""
    Code below is unreachable with the if, elif, else above
"""


# E_DEBUG("return SWGPG_SIG_ERROR")
# return swgpgc.SWGPG_SIG_ERROR


def swgpg_run_checksig2(sigfilename, thisprog, filearg, gpg_prog, uverbose, which_sig_arg):
    sbfd = [0 for _ in range(2)]
    status = 0
    cmd = [None for _ in range(3)]
    u_verbose = uverbose
    tmp = strob_open(30)
    atoiret = 1
    retval = 0

    if which_sig_arg:
        which_sig = swlib_atoi(which_sig_arg, atoiret)
        if atoiret != 0:
            return -3  # never should happen
    else:
        which_sig = -1

    cmd[0] = shcmd_open()
    cmd[1] = shcmd_open()
    cmd[2] = None
    E_DEBUG("")

    os.pipe()
    pid = swfork(None)
    if pid == 0:
        os.close(sbfd[0])
        E_DEBUG("")
        swgp_signal(signal.SIGPIPE, signal.SIG_DFL)
        swgp_signal(signal.SIGINT, signal.SIG_DFL)
        swgp_signal(signal.SIGTERM, signal.SIG_DFL)
        swgp_signal(signal.SIGUSR1, signal.SIG_DFL)
        swgp_signal(signal.SIGUSR2, signal.SIG_DFL)
        shcmd_add_arg(cmd[0], thisprog)
        strob_sprintf(tmp, 0, "--util-name=%s", swlib_utilname_get())
        shcmd_add_arg(cmd[0], strob_str(tmp))
        shcmd_add_arg(cmd[0], "-G")
        shcmd_add_arg(cmd[0], sigfilename)  # FIFO or /dev/tty
        shcmd_add_arg(cmd[0], "-n")
        shcmd_add_arg(cmd[0], which_sig_arg)
        while u_verbose > 1:
            shcmd_add_arg(cmd[0], "-v")
            u_verbose -= 1
        shcmd_add_arg(cmd[0], "--sleep")
        shcmd_add_arg(cmd[0], "1")
        # sbfd[1] is the write fd for the signed bytes 
        strob_sprintf(tmp, 0, "--signed-bytes-fd=%d", sbfd[1])
        shcmd_add_arg(cmd[0], strob_str(tmp))
        strob_sprintf(tmp, 0, "--logger-fd=%d", pty.STDOUT_FILENO)
        shcmd_add_arg(cmd[0], strob_str(tmp))
        shcmd_add_arg(cmd[0], filearg)  # may be a "-" for stdin
        E_DEBUG("")
        shcmd_unix_exec(cmd[0])
        E_DEBUG("")
        sys.stderr.write("exec error in swgp_run_checksig\n")
        sys.exit(1)
    elif pid < 0:
        E_DEBUG("")
        retval = 1
        os.close(sbfd[1])
        if cmd[0]:
            shcmd_close(cmd[0])
        if cmd[1]:
            shcmd_close(cmd[1])
        os.close(sbfd[0])
        strob_close(tmp)
        return retval
    os.close(sbfd[1])

    E_DEBUG("")
    cmd[1] = gpg_fifo_command(sigfilename, gpg_prog, uverbose, pty.STDOUT_FILENO)
    if cmd[1] is None:
        E_DEBUG("")
        os.kill(pid, signal.SIGTERM)
        os.waitpid(pid, status)
        if cmd[0]:
            shcmd_close(cmd[0])
        if cmd[1]:
            shcmd_close(cmd[1])
        os.close(sbfd[0])
        strob_close(tmp)
        return retval

    E_DEBUG("")
    strob_strcpy(tmp, b"")
    swlib_exec_filter(cmd, sbfd[0], tmp)
    E_DEBUG("")

    ret = swgpg_determine_signature_status(strob_str(tmp), which_sig)
    retval = ret
    E_DEBUG2("signature status is %d", ret)

    E_DEBUG2("status line: %s", strob_str(tmp))
    w = swgpg_create()
    E_DEBUG("")
    swgpg_disentangle_status_lines(w, strob_str(tmp))
    E_DEBUG("")
    logger_fd = -1
    status_fd = -1
    if retval:
        status_fd = pty.STDERR_FILENO
        logger_fd = pty.STDERR_FILENO
    else:
        if uverbose >= swlibc.SWC_VERBOSE_2:
            logger_fd = swlibc.STDOUT_FILENO
        if uverbose >= swlibc.SWC_VERBOSE_3:
            logger_fd = pty.STDOUT_FILENO
            status_fd = pty.STDOUT_FILENO
    E_DEBUG3("show: status_fd=%d, logger_fd=%d", status_fd, logger_fd)
    swgpg_show(w, 0, -1, status_fd, logger_fd)
    E_DEBUG("")
    swgpg_delete(w)
    E_DEBUG2("retval=%d", retval)
    return retval


def swgpg_run_gpg_verify(swgpg, signed_bytes_fd, signature, uverbose, gpg_status):
    cmdvec = [None for _ in range(2)]
    E_DEBUG("")
    E_DEBUG2("signed_bytes_fd = %d", signed_bytes_fd)
    fifo = strob_open(32)
    if gpg_status is None:
        output = strob_open(32)
    else:
        output = gpg_status

    cmdvec[0] = None
    cmdvec[1] = None
    E_DEBUG("")
    fifo_path = swgpg_create_fifo(fifo)
    if fifo_path is None:
        return -1

    E_DEBUG("")
    gpg_cmd = gpg_fifo_command(fifo_path, swgpg.gpg_progname, uverbose, pty.STDOUT_FILENO)
    E_DEBUG("")
    if gpg_cmd is None:
        swgpg_remove_fifo()
        return -1
    cmdvec[0] = gpg_cmd

    E_DEBUG("")
    pid = os.fork()
    if pid == 0:
        os.close(0)
        os.close(1)
        ffd = os.open(fifo_path, os.O_WRONLY)
        if ffd < 0:
            sys.exit(1)
        ret = uxfio_unix_safe_write(ffd, signature, len(signature))
        if ret != len(signature):
            sys.exit(2)
        os.close(ffd)
        sys.exit(0)
    elif pid < 0:
        return -1
    E_DEBUG("")
    strob_strcpy(output, "")
    ret1 = swlib_exec_filter(cmdvec, signed_bytes_fd, output)

    E_DEBUG("")
    if ret1 == SHCMD_UNSET_EXITVAL:
        # FIXME abnormal exit for gpg, don't no why 
        E_DEBUG2("swlib_exec_filter ret=%d", ret1)
        ret1 = 0

    E_DEBUG2("swlib_exec_filter ret1=%d", ret1)

    ret2 = swgpg_determine_signature_status(strob_str(output), -1)
    E_DEBUG2("swgpg_determine_signature_status ret2=%d", ret2)

    ret3, status = os.waitpid(pid, 0)
    if ret3 < 0:
        sys.stderr.write("waitpid error: %s\n" + os.strerror(ret3))
    elif ret3 == 0:
        ret3 = 2
    else:
        if os.WIFEXITED(status):
            ret3 = os.WEXITSTATUS(status)
            E_DEBUG2("exit value = %d", ret3)
        else:
            ret3 = 1

    swgpg_remove_fifo()
    strob_close(fifo)
    if gpg_status is None:
        strob_close(output)

    E_DEBUG2("RESULT ret1 = %d", ret1)
    E_DEBUG2("RESULT ret2 = %d", ret2)
    E_DEBUG2("RESULT ret3 = %d", ret3)
    if ret1 == 0 and ret2 == 0 and ret3 == 0:
        E_DEBUG("RETURNING returning 0")
        return 0
    else:
        E_DEBUG("RETURNING returning 1")
        return 1


def swgpg_get_package_signature_command(signer, gpg_name, gpg_path, passphrase_fd):
    env = [None] * 10
    sigcmd = shcmd_open()
    env[0] = None
    envpath = os.getenv("PATH")
    if (
            signer.upper() == "GPG" or
            signer.upper() == "GPG2"
    ):
        if signer.upper() == "GPG2":
            signerpath = shcmd_find_in_path(envpath, swgpgc.SWGPG_GPG2_BIN)
        else:
            signerpath = shcmd_find_in_path(envpath, swgpgc.SWGPG_GPG_BIN)
        if signerpath is None:
            return None
        shcmd_add_arg(sigcmd, signerpath)
        if gpg_name and len(gpg_name):
            shcmd_add_arg(sigcmd, "-u")
            shcmd_add_arg(sigcmd, gpg_name)
        if gpg_path and len(gpg_path):
            shcmd_add_arg(sigcmd, "--homedir")
            shcmd_add_arg(sigcmd, gpg_path)
        shcmd_add_arg(sigcmd, "--no-tty")
        shcmd_add_arg(sigcmd, "--no-secmem-warning")
        shcmd_add_arg(sigcmd, "--armor")
        if (
                passphrase_fd is not None and
                passphrase_fd == swgpgc.SWGPG_SWP_PASS_AGENT
        ):
            shcmd_add_arg(sigcmd, "--use-agent")
        else:
            shcmd_add_arg(sigcmd, "--passphrase-fd")
            shcmd_add_arg(sigcmd, "3")
        shcmd_add_arg(sigcmd, "-sb")
        shcmd_add_arg(sigcmd, "-o")
        shcmd_add_arg(sigcmd, "-")
    elif signer.upper() == "PGP2.6":
        make_pgp_env(env, len(env))
        shcmd_set_envp(sigcmd, env)
        signerpath = shcmd_find_in_path(envpath, swgpgc.SWGPG_PGP26_BIN)
        if signerpath is None:
            return None
        shcmd_add_arg(sigcmd, signerpath)
        if gpg_name and len(gpg_name):
            shcmd_add_arg(sigcmd, "-u")
            shcmd_add_arg(sigcmd, gpg_name)
        shcmd_add_arg(sigcmd, "+armor=on")
        shcmd_add_arg(sigcmd, "-sb")
        shcmd_add_arg(sigcmd, "-o")
        shcmd_add_arg(sigcmd, "-")
    elif signer.upper() == "PGP5":
        make_pgp_env(env, len(env))
        shcmd_set_envp(sigcmd, env)
        signerpath = shcmd_find_in_path(envpath, swgpgc.SWGPG_PGP5_BIN)
        if signerpath is None:
            return None
        shcmd_add_arg(sigcmd, signerpath)
        if gpg_name and len(gpg_name):
            shcmd_add_arg(sigcmd, "-u")
            shcmd_add_arg(sigcmd, gpg_name)
        shcmd_add_arg(sigcmd, "-ab")
        shcmd_add_arg(sigcmd, "-o")
        shcmd_add_arg(sigcmd, "-")
    else:
        shcmd_close(sigcmd)
        sigcmd = None
    return sigcmd


def swgp_close_all_fd(start):
    open_max = os.sysconf(os.sysconf_names['SC_OPEN_MAX'])
    if open_max < 0:
        if errno != 0:
            print(f"{swlib_utilname_get()}: sysconf: open_max not determined.")
        open_max = swlibc.SW_SC_OPEN_MAX
    else:
        pass

    if open_max < 16:
        print(f"{swlib_utilname_get()}: the maximum number of open file descriptors")
        print(f"{swlib_utilname_get()}: was determined to be {open_max}.")
        print(f"{swlib_utilname_get()}: swbis requires more than this for its checks")
        print(f"{swlib_utilname_get()}: against file descriptor leaks, and its normal")
        print(f"{swlib_utilname_get()}: operations.")
        exit(1)

    if start >= (open_max - 3):
        print(f"{swlib_utilname_get()}: Oops, something is using all the file descriptors")
        print(f"{swlib_utilname_get()}: swbis chooses not to continue")
        exit(1)

    if start >= 0:
        for i in range(start, open_max):
            os.close(i)
    else:
        pass

    return open_max


def swgpg_get_package_signature(sigcmd, statusp, wopt_passphrase_fd, passfile, pkg_fd, do_dummy_sign, g_passphrase):
    #cmd = [None, None]
    cmd = [sigcmd]
    verboseG = 1
    atoiret = 0
    pid = [0 for _ in range(6)]
    status = [0 for _ in range(6)]
    passphrase = [0 for _ in range(2)]
    input = [0 for _ in range(2)]
    output = [0 for _ in range(2)]
    filter = [0 for _ in range(2)]
    fdmem = ''
    # nullbyte = str([b'\0' for _ in range(2)])
    nullbyte = [0, 0]
    cmd[1] = None
    nullbyte[0] = '\0'

    if do_dummy_sign:
        # sig = str(swgpgc.ARMORED_SIGLEN)
        sig = "-----BEGIN DUMMY SIGNATURE-----\n" \
              "Version: swpackage " + swlibc.SWPACKAGE_VERSION + "\n" \
              "dummy signature made using the --dummy-sign option of\n" \
              "the swpackage(8) utility.  Not intended for verification.\n" \
              "-----END DUMMY SIGNATURE-----\n"
        return sig

    swlib_doif_writef(verboseG, swlibc.SWPACKAGE_VERBOSE_V1, None, get_stderr_fd(),
                      "Generating package signature ....\n")

    if sigcmd is None:
        return None
    else:
        cmd[0] = sigcmd

    input[0], input[1] = os.pipe()
    output[0], output[1] = os.pipe()
    passphrase[0], passphrase[1] = os.pipe()
    filter[0], filter[2] = os.pipe()
    passphrase = os.pipe()
    E_DEBUG("")
    does_use_agent = get_does_use_gpg_agent(cmd[0])

    # shcmd_cmdvec_debug_show_to_file(cmd, stderr);
    #
    # Exec Signer.
    #
    pid[0] = swfork(None)
    if pid[0] < 0:
        sys.exit(5)
    if pid[0] == 0:
        close_passfd()
        os.close(filter[1])
        os.close(output[0])
        shcmd_set_srcfd(cmd[0], filter[0])
        shcmd_set_dstfd(cmd[0], output[1])
        shcmd_set_errfile(cmd[0], "/dev/null")
        shcmd_apply_redirection(cmd[0])
        passphrase = os.pipe()
        if does_use_agent == 0:
            os.dup2(passphrase[0], 3)
        os.close(passphrase[1])
        os.close(passphrase[0])
        if does_use_agent == 0:
            swgp_close_all_fd(4)
        else:
            swgp_close_all_fd(3)
        cmd[0].unix_exec()
        # shcmd_unix_exec(cmd[0])
        # swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(), "%s not run.\n", cmd[0].argv_[0])
        print(cmd[0].argv_[0] + " not run.")
        sys.exit(2)
    os.close(output[1])
    os.close(filter[0])
    os.close(passphrase[0])

    #
    # Ask Passphrase
    #
    E_DEBUG("")
    pid[1] = os.fork()
    if pid[1] < 0:
        return str(None)
    if pid[1] == 0:
        os.close(input[1])
        os.close(output[0])
        if does_use_agent == 0 and strcmp(wopt_passphrase_fd, "-1") == 0 and strcmp(wopt_passphrase_fd,
                                                                                    swgpgc.SWGPG_SWP_PASS_ENV) != 0 and strcmp(
            wopt_passphrase_fd, swgpgc.SWGPG_SWP_PASS_AGENT) != 0 and passfile == str(None):
            """
              Not using the GPG agent or file descriptor
              of swpackage, therefore, use the getpass
              routine which gets the passphrase from tty
            """
            pbuf = [swgpgc.SWGPG_PASSPHRASE_LENGTH]
            pass_ = getpass.getpass("Enter Password: ")
            # fm_getpassphrase("Enter Password: ", pbuf, sizeof(pbuf))
            if pass_:
                pbuf[len(pbuf) - 1] = '\0'
                print("Password accepted")
                # os.write(passphrase[1], pass_, len(pass_))
                #os.write(passphrase[1], pass_)
            else:
                # os.write(passphrase[1], b"\n\n", 2)
                os.write(passphrase[1], b"\n\n")
                print('Password is incorrect')
            pbuf = ['\x00'] * len(pbuf)
            pbuf += ['\xff'] * len(pbuf)
            if os.close(passphrase[1]):
                swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(), "swpackage: close: %s\n",
                                  os.strerror(1))
        elif does_use_agent == 0 and strcmp(wopt_passphrase_fd, swgpgc.SWGPG_SWP_PASS_AGENT) != 0 and strcmp(
                wopt_passphrase_fd, swgpgc.SWGPG_SWP_PASS_ENV) != 0:
            #
            # Here, passfile may have been given or
            # wopt_passphrase_fd was given.
            #
            did_open = 0
            buf = [0 for _ in range(512)]
            if passfile != "-" and os.path.isfile(passfile):
                opt_passphrase_fd = os.open(passfile, os.O_RDONLY, 0)
                if opt_passphrase_fd < 0:
                    #swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(),
                    #                  "passphrase file not found\n")
                    print("passphrase file not found")
                did_open = 1
            elif passfile and strcmp(passfile, "-") == 0:
                # swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(),
                #                  "warning: unsafe use of stdin\n" + "warning: use --passphrase-fd=0 instead\n")
                print("warning: unsafe use of stdin\n"
                      "warning: use --passphrase-fd=0 instead\n")
                opt_passphrase_fd = pty.STDIN_FILENO
            else:
                opt_passphrase_fd = swlib_atoi(wopt_passphrase_fd, atoiret)
                if atoiret != 0:
                    return str(None)
            if opt_passphrase_fd >= 0:
                E_DEBUG2("reading opt_passphrase_fd=%d", opt_passphrase_fd)
                ret = os.read(opt_passphrase_fd, len(buf) - 1)
                E_DEBUG2("read of passphrase ret=%d", ret)
                if len(ret) < 0 or len(ret) >= 511:
                    print("read (loc=p): ", sys.stderr.fileno())
                    #swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, sys.stderr.fileno(),
                     #                 "read (loc=p): %s\n", os.strerror(1))
                    os.close(opt_passphrase_fd)
                    os.close(passphrase[1])
                    sys.exit(1)
                if did_open:
                    os.close(opt_passphrase_fd)
                #buf[ret] = '\0'
                #os.write(passphrase[1], buf)
                buf = ['\0'] * len(buf)
                buf += ['\xff'] * len(buf)
            if os.close(passphrase[1]):
                swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(), "close error: %s\n",
                                  os.strerror(1))
        elif does_use_agent == 1 and strcmp(wopt_passphrase_fd, swgpgc.SWGPG_SWP_PASS_AGENT) == 0:
            if os.close(passphrase[1]):
                swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(), "close error: %s\n",
                                  os.strerror(1))
        elif does_use_agent == 0 and strcmp(wopt_passphrase_fd, swgpgc.SWGPG_SWP_PASS_ENV) == 0 and g_passphrase:
            os.write(passphrase[1], g_passphrase + "\n")
            if os.close(passphrase[1]):
                swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(), "close error: %s\n",
                                  os.strerror(1))
        else:
            swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(),
                              "internal error get_package_signature\n")
            if os.close(passphrase[1]):
                swlib_doif_writef(verboseG, swlibc.SWC_VERBOSE_1, None, get_stderr_fd(), "close error: %s\n",
                                  os.strerror(1))
        sys.exit(0)
    #
    # Close the passphrase in the parent.
    #
    E_DEBUG("")
    close_passfd()
    os.close(passphrase[1])
    """
      filter[1] -->

     Decode signing stream.
    FIXME this fork is not required
    """
    pid[2] = os.fork()
    if pid[2] < 0:
        return str(None)
    if pid[2] == 0:
        close_passfd()
        os.close(input[1])
        swlib_pipe_pump(filter[1], input[0])
        os.close(input[0])
        os.close(filter[1])
        sys.exit(0)
    os.close(input[0])
    os.close(filter[1])
    E_DEBUG("")
    """
      <<-- input[0]
     input[1] -->
     Write signed stream
    """
    E_DEBUG("")
    pid[3] = os.fork()
    if pid[3] < 0:
        return str(None)
    if pid[3] == 0:
        close_passfd()
        os.close(input[0])

        ofd1 = uxfio_opendup(input[1], uc.UXFIO_BUFTYPE_NOBUF)
        uxfio_fcntl(ofd1, uc.UXFIO_F_SET_OUTPUT_BLOCK_SIZE, swlibc.PIPE_BUF)

        swlib_pipe_pump(ofd1, pkg_fd)

        uxfio_close(ofd1)
        os.close(output[0])
        sys.exit(0)
    os.close(input[1])

    E_DEBUG("")
    sigfd = swlib_open_memfd()

    ret = swlib_pump_amount(sigfd, output[0], 1024)
    os.close(output[0])
    if ret < 0 or ret > 1000:
        return str(None)
    uxfio_write(sigfd, nullbyte, 1)

    E_DEBUG("")
    if swlib_wait_on_all_pids(pid, 4, status, os.WNOHANG, verboseG - 2) < 0:
        return None
    """
      FIXME return error
      swexdist->setErrorCode(18004, NULL);
    """
    E_DEBUG("")
    if pid[0] < 0:
        ret = os.WEXITSTATUS(status[0])
    else:
        ret = 100
    # statusp = ret
    if ret != 0:
        return str(None)

    E_DEBUG("")
    uxfio_get_dynamic_buffer(sigfd, fdmem, None, None)
    sig = fdmem
    uxfio_close(sigfd)
    swlib_doif_writef(verboseG, swlibc.SWPACKAGE_VERBOSE_V1, None, get_stderr_fd(),
                      "Generating package signature .... Done.\n")
    E_DEBUG("")
    return sig
