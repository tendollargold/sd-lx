# sha512.py
# Declarations of functions and data types used for SHA512 and SHA384 sum
#   library functions.
#   Copyright (C) 2005-2006, 2008-2020 Free Software Foundation, Inc.

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */
# Just use import hashlib

def sha1_block():
    pass

class sha512_ctx:
    def __init__(self):
        self.state = [0] * 8
        self.total = [0] * 2
        self.buflen = 0
        self.buffer = [0] * 32

def sha512_init_ctx(ctx):
    ctx.state[0] = 0x6a09e667f3bcc908
    ctx.state[1] = 0xbb67ae8584caa73b
    ctx.state[2] = 0x3c6ef372fe94f82b
    ctx.state[3] = 0xa54ff53a5f1d36f1
    ctx.state[4] = 0x510e527fade682d1
    ctx.state[5] = 0x9b05688c2b3e6c1f
    ctx.state[6] = 0x1f83d9abfb41bd6b
    ctx.state[7] = 0x5be0cd19137e2179
    ctx.total[0] = 0
    ctx.total[1] = 0
    ctx.buflen = 0

def sha512_process_block(buffer, len, ctx):
    words = buffer
    endp = words + len // 8
    x = [0] * 16
    a = ctx.state[0]
    b = ctx.state[1]
    c = ctx.state[2]
    d = ctx.state[3]
    e = ctx.state[4]
    f = ctx.state[5]
    g = ctx.state[6]
    h = ctx.state[7]
    ctx.total[0] += len
    if ctx.total[0] < len:
        ctx.total[1] += 1

    def S0(x):
        return ((x >> 63) ^ (x >> 56) ^ (x >> 7)) & 0xFFFFFFFFFFFFFFFF

    def S1(x):
        return ((x >> 45) ^ (x >> 3) ^ (x >> 6)) & 0xFFFFFFFFFFFFFFFF

    def SS0(x):
        return ((x >> 36) ^ (x >> 30) ^ (x >> 25)) & 0xFFFFFFFFFFFFFFFF

    def SS1(x):
        return ((x >> 50) ^ (x >> 46) ^ (x >> 23)) & 0xFFFFFFFFFFFFFFFF

    def M(I):
        x[I & 15] += S1(x[(I - 2) & 15]) + x[(I - 7) & 15] + S0(x[(I - 15) & 15])

    def R(A, B, C, D, E, F, G, H, K, M):
        t0 = SS0(A) + ((A & B) ^ (C & (A ^ B)))
        t1 = H + SS1(E) + ((E & F) ^ (G & (E ^ F))) + K + M
        D += t1
        H = t0 + t1

    while words < endp:
        for t in range(16):
            x[t] = words[t]
        R(a, b, c, d, e, f, g, h, 0x428a2f98d728ae22, x[0])
        R(h, a, b, c, d, e, f, g, 0x7137449123ef65cd, x[1])
        R(g, h, a, b, c, d, e, f, 0xb5c0fbcfec4d3b2f, x[2])
        R(f, g, h, a, b, c, d, e, 0xe9b5dba58189dbbc, x[3])
        R(e, f, g, h, a, b, c, d, 0x3956c25bf348b538, x[4])
        R(d, e, f, g, h, a, b, c, 0x59f111f1b605d019, x[5])
        R(c, d, e, f, g, h, a, b, 0x923f82a4af194f9b, x[6])
        R(b, c, d, e, f, g, h, a, 0xab1c5ed5da6d8118, x[7])
        R(a, b, c, d, e, f, g, h, 0xd807aa98a3030242, x[8])
        R(h, a, b, c, d, e, f, g, 0x12835b0145706fbe, x[9])
        R(g, h, a, b, c, d, e, f, 0x243185be4ee4b28c, x[10])
        R(f, g, h, a, b, c, d, e, 0x550c7dc3d5ffb4e2, x[11])
        R(e, f, g, h, a, b, c, d, 0x72be5d74f27b896f, x[12])
        R(d, e, f, g, h, a, b, c, 0x80deb1fe3b1696b1, x[13])
        R(c, d, e, f, g, h, a, b, 0x9bdc06a725c71235, x[14])
        R(b, c, d, e, f, g, h, a, 0xc19bf174cf692694, x[15])
        for t in range(16, 80):
            M(t)
        R(a, b, c, d, e, f, g, h, 0xe49b69c19ef14ad2, x[16])
        R(h, a, b, c, d, e, f, g, 0xefbe4786384f25e3, x[17])
        R(g, h, a, b, c, d, e, f, 0x0fc19dc68b8cd5b5, x[18])
        R(f, g, h, a, b, c, d, e, 0x240ca1cc77ac9c65, x[19])
        R(e, f, g, h, a, b, c, d, 0x2de92c6f592b0275, x[20])
        R(d, e, f, g, h, a, b, c, 0x4a7484a6ea6e483b, x[21])
        R(c, d, e, f, g, h, a, b, 0x5cb0a9dcbd41fbd4, x[22])
        R(b, c, d, e, f, g, h, a, 0x76f988da831153b5, x[23])
        R(a, b, c, d, e, f, g, h, 0x983e5152ee66dfab, x[24])
        R(h, a, b, c, d, e, f, g, 0xa831c66d2db43210, x[25])
        R(g, h, a, b, c, d, e, f, 0xb00327c898fb213f, x[26])
        R(f, g, h, a, b, c, d, e, 0xbf597fc7beef0ee4, x[27])
        R(e, f, g, h, a, b, c, d, 0xc6e00bf33da88fc2, x[28])
        R(d, e, f, g, h, a, b, c, 0xd5a79147930aa725, x[29])
        R(c, d, e, f, g, h, a, b, 0x06ca6351e003826f, x[30])
        R(b, c, d, e, f, g, h, a, 0x142929670a0e6e70, x[31])
        R(a, b, c, d, e, f, g, h, 0x27b70a8546d22ffc, x[32])
        R(h, a, b, c, d, e, f, g, 0x2e1b21385c26c926, x[33])
        R(g, h, a, b, c, d, e, f, 0x4d2c6dfc5ac42aed, x[34])
        R(f, g, h, a, b, c, d, e, 0x53380d139d95b3df, x[35])
        R(e, f, g, h, a, b, c, d, 0x650a73548baf63de, x[36])
        R(d, e, f, g, h, a, b, c, 0x766a0abb3c77b2a8, x[37])
        R(c, d, e, f, g, h, a, b, 0x81c2c92e47edaee6, x[38])
        R(b, c, d, e, f, g, h, a, 0x92722c851482353b, x[39])
        R(a, b, c, d, e, f, g, h, 0xa2bfe8a14cf10364, x[40])
        R(h, a, b, c, d, e, f, g, 0xa81a664bbc423001, x[41])
        R(g, h, a, b, c, d, e, f, 0xc24b8b70d0f89791, x[42])
        R(f, g, h, a, b, c, d, e, 0xc76c51a30654be30, x[43])
        R(e, f, g, h, a, b, c, d, 0xd192e819d6ef5218, x[44])
        R(d, e, f, g, h, a, b, c, 0xd69906245565a910, x[45])
        R(c, d, e, f, g, h, a, b, 0xf40e35855771202a, x[46])
        R(b, c, d, e, f, g, h, a, 0x106aa07032bbd1b8, x[47])
        R(a, b, c, d, e, f, g, h, 0x19a4c116b8d2d0c8, x[48])
        R(h, a, b, c, d, e, f, g, 0x1e376c085141ab53, x[49])
        R(g, h, a, b, c, d, e, f, 0x2748774cdf8eeb99, x[50])
        R(f, g, h, a, b, c, d, e, 0x34b0bcb5e19b48a8, x[51])
        R(e, f, g, h, a, b, c, d, 0x391c0cb3c5c95a63, x[52])
        R(d, e, f, g, h, a, b, c, 0x4ed8aa4ae3418acb, x[53])
        R(c, d, e, f, g, h, a, b, 0x5b9cca4f7763e373, x[54])
        R(b, c, d, e, f, g, h, a, 0x682e6ff3d6b2b8a3, x[55])
        R(a, b, c, d, e, f, g, h, 0x748f82ee5defb2fc, x[56])
        R(h, a, b, c, d, e, f, g, 0x78a5636f43172f60, x[57])
        R(g, h, a, b, c, d, e, f, 0x84c87814a1f0ab72, x[58])
        R(f, g, h, a, b, c, d, e, 0x8cc702081a6439ec, x[59])
        R(e, f, g, h, a, b, c, d, 0x90befffa23631e28, x[60])
        R(d, e, f, g, h, a, b, c, 0xa4506cebde82bde9, x[61])
        R(c, d, e, f, g, h, a, b, 0xbef9a3f7b2c67915, x[62])
        R(b, c, d, e, f, g, h, a, 0xc67178f2e372532b, x[63])
        R(a, b, c, d, e, f, g, h, 0xca273eceea26619c, x[64])
        R(h, a, b, c, d, e, f, g, 0xd186b8c721c0c207, x[65])
        R(g, h, a, b, c, d, e, f, 0xeada7dd6cde0eb1e, x[66])
        R(f, g, h, a, b, c, d, e, 0xf57d4f7fee6ed178, x[67])
        R(e, f, g, h, a, b, c, d, 0x06f067aa72176fba, x[68])
        R(d, e, f, g, h, a, b, c, 0x0a637dc5a2c898a6, x[69])
        R(c, d, e, f, g, h, a, b, 0x113f9804bef90dae, x[70])
        R(b, c, d, e, f, g, h, a, 0x1b710b35131c471b, x[71])
        R(a, b, c, d, e, f, g, h, 0x28db77f523047d84, x[72])
        R(h, a, b, c, d, e, f, g, 0x32caab7b40c72493, x[73])
        R(g, h, a, b, c, d, e, f, 0x3c9ebe0a15c9bebc, x[74])
        R(f, g, h, a, b, c, d, e, 0x431d67c49c100d4c, x[75])
        R(e, f, g, h, a, b, c, d, 0x4cc5d4becb3e42b6, x[76])
        R(d, e, f, g, h, a, b, c, 0x597f299cfc657e2a, x[77])
        R(c, d, e, f, g, h, a, b, 0x5fcb6fab3ad6faec, x[78])
        R(b, c, d, e, f, g, h, a, 0x6c44198c4a475817, x[79])
        ctx.state[0] += a
        ctx.state[1] += b
        ctx.state[2] += c
        ctx.state[3] += d
        ctx.state[4] += e
        ctx.state[5] += f
        ctx.state[6] += g
        ctx.state[7] += h

def sha512_finish_ctx(ctx, resbuf):
    sha512_conclude_ctx(ctx)
    return sha512_read_ctx(ctx, resbuf)

def sha512_conclude_ctx(ctx):
    bytes = ctx.buflen
    size = 16 if bytes < 112 else 32
    ctx.total[0] += bytes
    if ctx.total[0] < bytes:
        ctx.total[1] += 1
    ctx.buffer[size - 2] = ((ctx.total[1] << 3) | (ctx.total[0] >> 61)) & 0xFFFFFFFFFFFFFFFF
    ctx.buffer[size - 1] = (ctx.total[0] << 3) & 0xFFFFFFFFFFFFFFFF
    fillbuf = [0x80, 0]
    ctx.buffer[bytes:] = fillbuf[:size - 2 - bytes]
    sha512_process_block(ctx.buffer, size * 8, ctx)

def sha512_read_ctx(ctx, resbuf):
    for i in range(8):
        resbuf[i] = ctx.state[i]
    return resbuf

def sha512_process_bytes(buffer, len, ctx):
    if ctx.buflen != 0:
        left_over = ctx.buflen
        add = min(256 - left_over, len)
        ctx.buffer[left_over:left_over + add] = buffer[:add]
        ctx.buflen += add
        if ctx.buflen > 128:
            sha512_process_block(ctx.buffer, ctx.buflen & ~63, ctx)
            ctx.buflen &= 127
            ctx.buffer[:ctx.buflen] = ctx.buffer[(left_over + add) & ~127:(left_over + add) & ~127 + ctx.buflen]
        buffer = buffer[add:]
        len -= add

    if len >= 128:
        sha512_process_block(buffer, len & ~127, ctx)
        buffer = buffer[len & ~127:]
        len &= 127

    if len > 0:
        left_over = ctx.buflen
        ctx.buffer[left_over:left_over + len] = buffer[:len]
        left_over += len
        if left_over >= 128:
            sha512_process_block(ctx.buffer, 128, ctx)
            left_over -= 128
            ctx.buffer[:left_over] = ctx.buffer[16:16 + left_over]
        ctx.buflen = left_over

def sha512_buffer(buffer, len, resblock):
    ctx = sha512_ctx()
    sha512_init_ctx(ctx)
    sha512_process_bytes(buffer, len, ctx)
    return sha512_finish_ctx(ctx, resblock)

def sha512_block(thisisa, resblock, iblock, icount):
    ctx = thisisa
    buffer = iblock
    len = icount
    if iblock is None and icount < 0:
        sha512_init_ctx(ctx)
        return 0
    if icount == 512:
        sum = 0
        sum += icount
        sha512_process_block(buffer, 512, ctx)
        return 0
    if icount >= 0 and iblock:
        if icount:
            sha512_process_bytes(buffer, len, ctx)
        return 0
    sha512_finish_ctx(ctx, resblock)
    return 0
