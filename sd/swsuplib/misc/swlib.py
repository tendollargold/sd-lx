# sd.swsuplib.swlib.py - General purpose, misc.routines.


# Copyright 1997 - 2004 James Lowe, Jr.
# All Rights Reserved.
# Copyright (C) 2023 Paul Weber Port to Python
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the

import errno
import fnmatch
import hashlib
import inspect
import os
import signal
import sys
import tarfile
import time
import pty
from ctypes import *

import fcntl

from sd.const import lc, SwGlobals
from sd.swsuplib.misc.swpath import SWPATH_CTYPE_DIR, SWPATH_CTYPE_CAT, SWPATH_CTYPE_STORE
from sd.swsuplib.atomicio import atomicio
from sd.swsuplib.misc.cstrings import strcmp
from sd.swsuplib.misc.shcmd import shcmd_set_dstfd, shcmd_get_dstfd, shcmd_set_srcfd, shcmd_command, shcmd_wait
from sd.swsuplib.misc.swextopt import swextopt_is_value_true
from sd.swsuplib.misc.swfork import swfork
from sd.swsuplib.misc.swpath import swpath_open, swpath_parse_path, swpath_get_is_catalog, swpath_close
from sd.swsuplib.misc.swsdflt import swsdflt_get_default_value
from sd.swsuplib.misc.swvarfs import SWLIB_ASSERT, SWLIB_ALLOC_ASSERT
from sd.swsuplib.misc.swutilname import swlib_utilname_get
from sd.swsuplib.progressmeter import update_progress_meter
from sd.swsuplib.strob import STROB, strob_vsprintf, strpbrk, strob_strlen, strob_str, strob_open
from sd.swsuplib.strob import strob_close, strob_strcpy, strob_chr_index, strob_sprintf, strob_release
from sd.swsuplib.swutillib import swutil_cpp_doif_writef
from sd.swsuplib.taru.copyout import taru_write_archive_trailer
from sd.swsuplib.taru.cpiohdr import chc
from sd.swsuplib.taru.readinheader import taru_tape_buffered_read
from sd.swsuplib.taru.taru import tc, taru_digs_create, taru_digs_delete
from sd.swsuplib.taru.taruib import taruib_write_catalog_stream, taruib_write_storage_stream
from sd.swsuplib.taru.xformat import (xformat_get_ifd, xformat_get_format, xformat_read_header,
                                      xformat_is_end_of_archive, xformat_get_name, xformat_get_tar_typeflag,
                                      xformat_set_ofd, xformat_write_header, xformat_copy_pass,xformat_get_tarheader_flags)
from sd.swsuplib.uxfio import uc, UXFIO, uxfio_write, uxfio_read, uxfio_sfa_read, uxfio_fcntl, uxfio_get_dynamic_buffer
from sd.util import swlib_doif_writef

global verbose_swbis_event_threshold, pflag
g = SwGlobals

uxfio = UXFIO

io_req = None

g_pax_header_pidG = 0
verbose_level = 0
initial_size = 512

global __FILE__, __LINE__, __FUNCTION__

class FrameRecord:
    global __FILE__, __LINE__, __FUNCTION__
    def __init__(self):
        self.callerframerecord = inspect.stack()[1]  # 0 represents this line
        # 1 represents line at caller
        self.frame = self.callerframerecord[0]
        self.info = inspect.getframeinfo(self.frame)
        self.__FILE__ = self.info.filename  # __FILE__     -> Test.py
        self.__FUNCTION__ = self.info.function  # fr.__FUNCTION__ -> Main
        self.__LINE__ = self.info.lineno  # self.__LINE__     -> 13
    #    self.__line__ = self.info.lineno
    #    self.__function__ = self.info.function
    #    self.__file__ = self.info.filename


fr = FrameRecord()

def get_caller_info(): # Same as FrameRecord function
    caller_frame = inspect.stack()[2]
    frame = caller_frame[0]
    info = inspect.getframeinfo(frame)
    return info.filename, info.lineno, info.function

def SW_DEBUG_PRINT():
    return print(f"{swlib_utilname_get()}: {fr.__FILE__}:{fr.__LINE__}")

def sw_debug_print():
    return sys.stderr.write("%s: %s:%d\n" + swlib_utilname_get() + fr.__file__ + fr.__LINE__)

def SW_ERROR_IMPL():
    return print(f"{swlib_utilname_get()}: swbis release {g.SW_RELEASE}: unexpected result or internal error at {fr.__FILE__}:{fr.__LINE__}")

def sw_error_impl():
    return sys.stderr.write("%s: swbis release %s: unexpected result or internal error at %s:%d\n" + swlib_utilname_get() + g.SW_RELEASE + fr.__file__ + fr.__LINE__)

def SW_IMPL_ERROR_DIE(status):
    swlib_swprog_assert(swlibc.SW_PROGS_IMPLEMENTATION_ERROR, status, "Impl fatal internal error", g.SW_RELEASE, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__))

def sw_impl_error_die(status):
    return swlib_swprog_assert(swlibc.SW_PROGS_IMPLEMENTATION_ERROR, status,"Impl fatal internal error", g.SW_RELEASE, str(__file__), fr.__LINE__, str(fr.__FUNCTION__))

def SWLIB_INTERNAL_ASSERT(code, status):
    swlib_swprog_assert(code, status, "", g.SW_RELEASE, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__))

def sw_internal_assert(code, status):
    return swlib_swprog_assert(code, status, "", g.SW_RELEASE, str(__file__), fr.__LINE__, str(fr.__FUNCTION__))




def swio_read(a, b, c):
    return atomicio('read', a, b, c)
def swio_write(a,b,c):
    return atomicio('write', a, b, c)


def SWLIB_WARN(A):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "WARNING: " + A + "\n")

def swlib_warn(a):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__),
                           "WARNING: " + a + "\n")

def SWLIB_WARN2(A, B):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "WARNING: " + A + "\n", B)

def swlib_warn2(a, b):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "WARNING: " + a + "\n", b)

def SWLIB_WARN3(A, B, C):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "WARNING: " + A + "\n", B, C)

def swlib_warn3(a, b, c):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, fr.__FUNCTION__, "WARNING: " + a + "\n", b, c)

def SWLIB_WARN4(A, B, C, D):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "WARNING: " + A + "\n", B, C, D)

def swlib_warn4(a, b, c, d):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, fr.__FUNCTION__, "WARNING: " + a + "\n", b, c, d)

def swlib_assertion_fatal(assertion_result, reason, file, line, function):
    if assertion_result == 0:
        swlib_fatal(reason, file, line, function)
def SWLIB_FATAL(A):
    swlib_fatal(A, fr.__FILE__, fr.__LINE__, fr.__FUNCTION__)
def swlib_fatal(reason, file, line, function):
    e_msg("fatal error", reason, file, line, function)
    sys.exit(252)

def SWLIB_INTERNAL(A):
    swlib_internal_error(A, fr.__FILE__, fr.__LINE__, fr.__FUNCTION__)
def swlib_internal_error(reason, file, line, function):
    e_msg("internal implementation error", reason, file, line, function)

def SWLIB_RESOURCE(A):
    swlib_resource(A, fr.__FILE__, fr.__LINE__, fr.__FUNCTION__)
def swlib_resource(reason, file, line, function):
    e_msg("resource exception", reason, file, line, function)

def SWLIB_EXCEPTION(A):
    swlib_exception(A, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__))

def swlib_exception(reason, file, line, function):
    e_msg("program exception", reason, file, line, function)


def SWLIB_INFO(A):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "INFO: " + A + "\n")


def SWLIB_INFO2(A, B):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "INFO: " + A + "\n", B)

def SWLIB_INFO3(A, B, C):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, fr.__FUNCTION__, "INFO: " + A + "\n", B, C)

def SWLIB_INFO4(A, B, C, D):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, fr.__FUNCTION__, "INFO: " + A + "\n", B, C, D)

def SWLIB_ERROR(A):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "ERROR: " + A + "\n")

def SWLIB_ERROR2(A, B):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "ERROR: " + A + "\n", B)

def SWLIB_ERROR3(A, B, C):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "ERROR: " + A + "\n", B, C)

def SWLIB_ERROR4(A, B, C, D):
    swutil_cpp_doif_writef(1, 1, None, pty.STDERR_FILENO, str(fr.__FILE__), fr.__LINE__, str(fr.__FUNCTION__), "ERROR: " + A + "\n", B, C, D)

def xstr(s):
    return istr(s)

def istr(s):
    return str(s)
class SwlibConstants:
    g_burst_adjust = 65000
    g_pstatbytes = None
    OPEN_MAX = 256
    swutil_name = [None] * 30
    CSHID = "# Generated in " + __file__ + " at line " + xstr(fr.__LINE__)
    SHA_BLOCKSIZE = 512
    SW_A_gnu = "gnu"
    SW_A_pax = "pax"
    SW_A_posix = "posix"
    SW_A_ustar = "ustar"
    SW_CATALOG_GROUP_ATT = "catalog_group"
    SW_DEFAULT_CATALOG_GROUP = "root"
    SW_DEFAULT_CATALOG_USER = "root"
    SW_CATALOG_GROUP_VAL = SW_DEFAULT_CATALOG_GROUP
    SW_CATALOG_MODE_ATT = "catalog_mode"
    SW_CATALOG_MODE_VAL = "0755"
    SW_CATALOG_OWNER_ATT = "catalog_owner"
    SW_CATALOG_OWNER_VAL = SW_DEFAULT_CATALOG_USER
    SW_DEBUG_PRINT = sw_debug_print()
    SW_DEFAULT_DISTRIBUTION_GROUP = "root"
    SW_DEFAULT_DISTRIBUTION_USER = "root"
    SW_DIR_GROUP_ATT = "leading_dir_group"
    SW_DIR_MODE_ATT = "leading_dir_mode"
    SW_DIR_MODE_VAL = "0755"
    SW_DIR_OWNER_ATT = "leading_dir_owner"
    SW_DISTRIBUTION_GROUP_ATT = "group"
    SW_DISTRIBUTION_MODE_ATT = "mode"
    SW_DISTRIBUTION_MODE_VAL = "0755"
    SW_DISTRIBUTION_OWNER_ATT = "owner"
    SW_PROGS_IMPLEMENTATION_ERROR = 10000
    SW_ERROR_IMPL = sw_error_impl # Function defined above
    SW_IMPL_ERROR_DIE = sw_impl_error_die # Function defined above
    SW_INTERNAL_ASSERT = sw_internal_assert # Function defined above
    SW_LOGGER_SIGTERM = signal.SIGUSR2
    SW_MIN_FD_AVAIL = 16
    SW_PGM_BIN_BASH = "/bin/bash"
    SW_PGM_BIN_KSH = "/bin/ksh"
    SW_PGM_BIN_MKSH = "/bin/mksh"
    SW_PGM_BIN_SH = "/bin/sh"
    SW_PGM_SH = "sh"
    SW_PGM_XPG4_SH = "/usr/xpg4/bin/bash"

    SW_PROGS_LAYOUT_VERSION = "1.0"
    SW_PROGS_USER_ERROR = 20000
    SW_SC_OPEN_MAX = 24
    SW_SIGNATURE_ATT = "signature"
    SW_STDIO_FNAME = "-"
    SW_SYNCT_EOF = tc.CPIO_INBAND_EOA_FILENAME + "\r\n\r\n"
    SW_TAINTED_CHARS = "'\"|*?;&<>`$[]"
    SW_WS_TAINTED_CHARS = "\n\r\t "
    SWBN_SIG_HEADER = "sig_header"
    SWBN_SIGNATURE = "signature"
    SWC_FC_NOAB = 1
    SWC_FC_NOOP = 0
    SWC_FC_NORE = 2
    SWC_VERBOSE_0 = 0
    SWC_VERBOSE_10 = 10
    SWC_VERBOSE_1 = 1
    SWC_VERBOSE_11 = 11
    SWC_VERBOSE_12 = 12
    SWC_VERBOSE_2 = 2
    SWC_VERBOSE_3 = 3
    SWC_VERBOSE_4 = 4
    SWC_VERBOSE_5 = 5
    SWC_VERBOSE_6 = 6
    SWC_VERBOSE_7 = 7
    SWC_VERBOSE_8 = 8
    SWC_VERBOSE_9 = 9
    SWC_VERBOSE_IDB2 = 9
    SWC_VERBOSE_IDB = 8
    SWC_VERBOSE_SWIDB = 5
    SW_EXIT_ONE = 1
    SW_EXIT_SUCCESS = 0
    SW_EXIT_TWO = 2
    SW_FALSE = 0
    SWI_FILESET_0 = 0

    SWI_PRODUCT_0 = 0
    SWLIB_PIPE_BUF = 512
    SWLIB_RESOURCE = swlib_resource
    SWLIB_SYNCT_BLOCKSIZE = 128
    SWLIB_WARN2 = swlib_warn2 # Function defined above
    SWLIB_WARN3 = swlib_warn3 # Function defined above
    SWLIB_WARN4 = swlib_warn4 # Function defined above
    SWLIB_WARN = swlib_warn # Function defined above
    SWPACKAGE_VERBOSE_V0 = SWC_VERBOSE_4
    SWPACKAGE_VERBOSE_V1 = SWC_VERBOSE_5
    SWPACKAGE_VERBOSE_V2 = SWC_VERBOSE_6
    SWPACKAGE_VERBOSE_V3 = SWC_VERBOSE_7
    SWPACKAGE_VERBOSE_V4 = SWC_VERBOSE_8
    SW_ROOT_DIR = "/"
    SW_TRUE = 1
    SYNCT_EOF_CONDITION_0 = 0
    SYNCT_EOF_CONDITION_1 = 1
    SYNCT_EOF_CONDITION_2 = 2
    TARRECORDSIZE = 512

swlibc = SwlibConstants()


class SYNCT:
    def __init__(self):
        self.do_debug = -1  # normally not used, should be -1
        self.debugfd = -1  # normally not used, should be -1
        self.buf = 0
        self.tail = 0
        self.trailer_start = 0
        self.count = 0
        self.mt = str


def swlib_fork_to_make_unixfd(uxfio_fd, blockmask, defaultmask, ppid):
    upipe = os.pipe()
    upid = os.fork()

    if ppid:
        ppid = 0

    if uxfio_fd <= swlibc.OPEN_MAX:
        return uxfio_fd

    if upid > 0:  # parent
        os.close(uxfio_fd)
        os.close(upipe[1])
        if ppid:
            ppid = upid
        return upipe[0]
    elif upid == 0:  # child
        os.close(upipe[0])
        ret = swlib_pipe_pump(upipe[1], uxfio_fd)
        if ret >= 0:
            ret = 0
        else:
            ret = 255
        sys.exit(ret)
    else:
        print("fork failed")
        return -1




def swlib_get_pax_header_pid():
    global g_pax_header_pidG
    if g_pax_header_pidG == 0:
        g_pax_header_pidG = os.getpid()
    return g_pax_header_pidG


def swlib_set_pax_header_pid(n):
    global g_pax_header_pidG
    g_pax_header_pidG = n


# Header file is above this line
# C code is below this line
def synct_i_no_null_write(fd, buf, sz):
    remains = sz
    retval = 0
    p = buf
    while remains > 0:
        amount = 0
        x = p
        while x - buf < sz and x:
            amount += 1
            x += 1
        if amount > sz:
            amount = sz
        excl_region = remains - amount
        ret = atomicio(uxfio_write, fd, p, amount)
        if ret < 0:
            return -1
        if ret != amount:
            return -2
        p += ret
        remains -= ret
        retval += ret
        while excl_region > 0 and p == '\0':
            excl_region -= 1
            remains -= 1
            p += 1
    return retval


def synct_i_read_block(synct, iof, fd, buf):
    ret = iof(fd, buf, swlibc.SWLIB_SYNCT_BLOCKSIZE)
    if synct.debugfd > 0:
        os.write(synct.debugfd, buf + ret)
    if ret != swlibc.SWLIB_SYNCT_BLOCKSIZE:
        sys.stderr.write("%s: synct_i_read_block() error: ret=%d\n" % (swlib_utilname_get(), ret))
        if ret < 0:
            return -1
        return -ret
    synct.count += 1
    return ret


def synct_check_for_possible_eof(synct):
    ibuf = synct.buf
    pos = ibuf
    i = 0
    while pos - ibuf < swlibc.SWLIB_SYNCT_BLOCKSIZE and synct.mt[i] != '\0':
        if pos and pos == synct.mt[i]:
            i += 1
        elif pos == '\0' and i > 11:
            return swlibc.SYNCT_EOF_CONDITION_2
        else:
            return swlibc.SYNCT_EOF_CONDITION_0
        pos += 1
    if synct.mt[i] == '\0':
        return swlibc.SYNCT_EOF_CONDITION_1
    else:
        return swlibc.SYNCT_EOF_CONDITION_0


def swlib_set_swbis_verbose_threshold(s):
    global verbose_swbis_event_threshold
    verbose_swbis_event_threshold = s


def swlib_get_nopen():
    nopen = 0
    for i in range(200):
        ret = fcntl.fcntl(i, fcntl.F_GETFL)
        if ret < 0 and errno == 'EBADF':
            pass
        else:
            nopen += 1
    return nopen


def e_msg(error_class, reason, file, line, function):
    utilname = swlib_utilname_get()
    print(f"{utilname}: {error_class}: {reason}: at {file}:{line}", file=sys.stderr)
    sys.stderr.write("%s: %s: %s: at %s:%d\n" % (swlib_utilname_get(), error_class, reason, file, line))


def convert_hex_seq(d1, d2):
    n = d1
    if not n.isdigit() and (n < '\x41' or n > '\x46'):
        return -1
    if not n.isdigit():
        d1 -= 7
    n = d2
    if not n.isdigit() and (n < '\x41' or n > '\x46'):
        return -1
    if not n.isdigit():
        d2 -= 7
    return (16 * (d1 - 48)) + (d2 - 48)


def does_have_hex_escape(src, value):
    t = src.find("\\x")
    if t != -1 and (t == 0 or src[t - 1] != '\\'):
        seq = src[t + 2:]
        if len(seq) < 2:
            return None
        d1 = ord(seq[0])
        d2 = ord(seq[1])
        value.contents = convert_hex_seq(d1, d2)
        if value.contents < 0:
            return None
        else:
            return src[t:]
    return None


def process_all_hex_escapes(src):
    s = bytearray(src, 'utf-8')
    while True:
        r, value = does_have_hex_escape(s, '')
        if r is not None:
            s[r + 1:] = s[r + 4:]
            s[r] = value
        else:
            break


def swlib_doif_writeap(fd, buffer, format_, pap):
    if fd < 0:
        return 0
    ret = strob_vsprintf(buffer, 1, format_, pap)
    if ret < 0:
        return -1
    ret = strob_strlen(buffer)
    aret = atomicio(uxfio_write, fd, strob_str(buffer), strob_strlen(buffer))
    if aret != ret:
        return -1
    return ret


def swlib_pump_amount8(ofd, ifd, amount, adjunct_ofd, digs):
    n = swlib_i_digs_copy(ofd, ifd, amount, digs, adjunct_ofd, taru_tape_buffered_read)
    return n


def nano_nanosleep(nsec):
    tt = time.struct_time((0, nsec))
    return time.sleep(tt)


def delay_sleep_pattern2(io_req, sleepbytes, c_amount, byteswritten):
    doneone = 0
    leaddiv = 0
    openssh_rcvd_adjust = swlibc.g_burst_adjust
    if sleepbytes > openssh_rcvd_adjust and (leaddiv == 0 or (
            (c_amount / leaddiv) < byteswritten < (c_amount - c_amount / leaddiv))):
        if (verbose_level >= swlibc.SWC_VERBOSE_3 and not doneone or verbose_level >=
                swlibc.SWC_VERBOSE_8):
            sys.stderr.write("%s: sshd efd race delay: sleeping %d microseconds every %d bytes\n" %
                             (swlib_utilname_get(), io_req.tv_nsec / 1000, swlibc.g_burst_adjust))
            sys.stderr.write("%s: sshd efd race delay: is not needed for openssh >=5.1\n" % swlib_utilname_get())
        nano_nanosleep(io_req.tv_nsec)
    return


def form_abspath(sb1, s1, pcwd):
    if s1 == '/':
        return 1
    if pcwd is None:
        cwd = os.getcwd()
    else:
        cwd = pcwd
    # sb1 = cwd
    swlib_unix_dircat(sb1, cwd)
    return 0


def swlib_i_digs_copy(ofd, ifd, count, digs, adjunct_ofd, f):
    buf = bytearray(512)
    blocksize = 512
    retval = 0
    md5ctx = hashlib.md5()
    sha1ctx = hashlib.sha1()
    sha512ctx = hashlib.sha512()
    #    res_sha1 = bytearray(21)
    #    res_sha512 = bytearray(65)
    sleepbytes = 0
    timebytes = 0
    oldt = time.time()

    if digs and digs.do_md5 == chc.DIGS_ENABLE_ON:
        md5ctx.update(digs)

    if digs and digs.do_sha1 == chc.DIGS_ENABLE_ON:
        sha1ctx.update(b'')

    if digs and digs.do_sha512 == chc.DIGS_ENABLE_ON:
        sha512ctx.update(b'')

    while True:
        if count >= 0:
            readamount = blocksize if (count - retval) > blocksize else (count - retval)
        else:
            readamount = blocksize

        n = f(ifd, buf, readamount)
        if n < 0:
            return -1

        if n:
            if digs and digs.do_md5 == chc.DIGS_ENABLE_ON:
                p5 = buf
                am = n
                while am > 0:
                    amount = am if am > 1024 else 1024
                    md5ctx.update(p5[:amount])
                    p5 += amount
                    am -= amount

            if digs and digs.do_sha1 == chc.DIGS_ENABLE_ON:
                sha1ctx.update(buf[:n])

            if digs and digs.do_sha512 == chc.DIGS_ENABLE_ON:
                sha512ctx.update(buf[:n])

            if ofd > 0:
                wr = os.write(ofd, buf[:n])
                if wr < 0:
                    return -1
            else:
                wr = n

            retval += wr

            if adjunct_ofd > 0:
                ret = os.write(adjunct_ofd, buf[:n])
                if ret < 0:
                    return -1

            timebytes += wr

            # if io_req and io_req.tv_nsec:
            if io_req:
                sleepbytes += wr
                delay_sleep_pattern2(io_req, sleepbytes, count, retval)

            if swlibc.g_pstatbytes and timebytes > 10000:
                if time.time() > oldt:
                    timebytes = 0
                    oldt = time.time()
                    swlibc.g_pstatbytes = retval
                    update_progress_meter(signal.SIGALRM)

        if n == 0:
            break

    if n < 0:
        return -1

    if digs and digs.do_md5 == chc.DIGS_ENABLE_ON:
        md5ctx.update(b'')
        digs.md5 = md5ctx.hexdigest()[:32]

    if digs and digs.do_sha1 == chc.DIGS_ENABLE_ON:
        sha1ctx.update(b'')
        digs.sha1 = sha1ctx.hexdigest()

    if digs and digs.do_sha512 == chc.DIGS_ENABLE_ON:
        sha512ctx.update(b'')
        digs.sha512 = sha512ctx.hexdigest()

    if digs and digs.do_size:
        tmp = STROB()
        ubuf = STROB()
        tmp.str_ = bytearray(24)
        ubuf.str_ = bytearray(24)
        tmp.length_ = 24
        ubuf.length_ = 24
        tmp.reserve_ = 0
        ubuf.reserve_ = 0
        tmp.in_use_ = 0
        ubuf.in_use_ = 0
        tmp.fill_char = 0
        ubuf.fill_char = 0
        tmp.str_ = swlib_imaxtostr(retval, ubuf)
        digs.size = tmp.str_

    return retval


def squash_trailing_char(path, ch, min_len):
    if path and len(path) > min_len and path[-1] == ch:
        #        path = path[:-1]
        return 0
    else:
        return 1


def swlib_atoi(nptr, result):
    try:
        ret = int(nptr)
        if result:
            result = 0
    except ValueError:
        if result:
            result = 1
        sys.stderr.write(f"{swlib_utilname_get()}: strtol error when converting [{nptr}]\n")
    return result


def swlib_atoi2(nptr, fp_endptr, result):
    ret = 0
    endptr = ""
    if fp_endptr:
        a_endptr = fp_endptr
    else:
        a_endptr = [endptr]
    try:
        ret = int(nptr)
        if result:
            result = 0
    except ValueError:
        if result:
            result = 1
        sys.stderr.write(f"{swlib_utilname_get()}: strtol error when converting [{nptr}]\n")
    return ret


def swlib_atoul(nptr, result, fp_endptr):
    ret = 0
    endptr = ""
    if fp_endptr is None:
        p_endptr = [endptr]
    else:
        p_endptr = fp_endptr
    try:
        ret = int(nptr)
        if result:
            result = 0
    except ValueError:
        if result:
            result = 1
        sys.stderr.write(f"{swlib_utilname_get()}: strtoul error when converting [{nptr}]\n")
    return ret


def swlib_pump_get_ppstatbytes():
    return [swlibc.g_pstatbytes]


def swlib_pump_amount(discharge_fd, suction_fd, amount):
    i = amount
    if swlib_i_pipe_pump(suction_fd, discharge_fd, i, -1, None, uxfio_read):
        return -1
    return i


def swlib_get_io_req():
    return io_req


def swlib_burst_adjust_p():
    return [swlibc.g_burst_adjust]


def swlib_get_io_req_p():
    return [io_req]


def swlib_test_verbose(ev, verbose_level, swbis_event, is_swi_event, event_status, is_posix_event):
    if (ev and verbose_level >= ev.verbose_threshhold) or (
            verbose_level >= swlibc.SWC_VERBOSE_1 and event_status != 0) or (
            verbose_level >= swlibc.SWC_VERBOSE_3 and is_posix_event) or (
            verbose_level >= verbose_swbis_event_threshold and swbis_event) or (
            verbose_level >= swlibc.SWC_VERBOSE_6 and is_swi_event) or (
            verbose_level >= verbose_swbis_event_threshold and not is_swi_event):
        return 1
    else:
        return 0


def swlib_get_verbose_level():
    return verbose_level


def swlib_set_verbose_level(n):
    verbose_level = n


def swlib_i_pipe_pump(suction_fd, discharge_fd, amount, adjunct_ofd, obuf, thisfpread):
    commandfailed = 0
    pumpdead = 0
    byteswritten = 0
    buf_size = swlibc.SWLIB_PIPE_BUF
    buf = bytearray(buf_size)
    sleepbytes = 0
    c_amount = amount[0]

    if obuf is not None:
        obuf.clear()

    if c_amount < 0:
        while not pumpdead:
            bytes = thisfpread(suction_fd, buf)
            if bytes < 0:
                commandfailed = 1
                pumpdead = 1
            elif bytes == 0:
                pumpdead = 1
            else:
                if discharge_fd >= 0:
                    if os.write(discharge_fd, buf[:bytes]) != bytes:
                        commandfailed = 2
                        pumpdead = 1
                if adjunct_ofd >= 0:
                    if os.write(adjunct_ofd, buf[:bytes]) != bytes:
                        commandfailed = 3
                        pumpdead = 1
                if obuf is not None:
                    obuf.extend(buf[:bytes])
            byteswritten += bytes
            # Handle sleep pattern if necessary
    else:
        remains = buf_size
        if (c_amount - byteswritten) < remains:
            remains = c_amount - byteswritten
        while not pumpdead and remains:
            bytes = thisfpread(suction_fd, buf[:remains])
            if bytes < 0:
                commandfailed = 1
                pumpdead = 1
            elif bytes == 0:
                pumpdead = 1
            elif bytes:
                if discharge_fd >= 0:
                    ibytes = os.write(discharge_fd, buf[:bytes])
                    if adjunct_ofd >= 0:
                        ibytes = os.write(adjunct_ofd, buf[:bytes])
                else:
                    ibytes = bytes
                if obuf is not None:
                    obuf.extend(buf[:bytes])
                if ibytes != bytes:
                    commandfailed = 2
                    pumpdead = 1
                else:
                    byteswritten += bytes
                    if (c_amount - byteswritten) < remains:
                        remains = c_amount - byteswritten
                        if remains > buf_size:
                            remains = buf_size
            # Handle sleep pattern if necessary
    amount[0] = byteswritten
    if obuf is not None:
        obuf.append(0)  # Append hidden null if needed
    return commandfailed


#    return -1 # Out of bounds

def swlib_printable_safe_ascii(sf):
    s = sf
    for i in range(len(s)):
        if ord(s[i]) > 126 or ord(s[i]) < 33:
            s[i] = '.'
    return sf


def swlib_is_ascii_noaccept(str1, acc, minlen):
    s = str1
    #    ret = 0
    if not s or len(s) < minlen:
        return 1
    ret = 1 if strpbrk(s, acc) else 0
    if ret:
        return ret
    s = str1
    while s and s:
        if s > 126 or s < 33:
            return 1
        s += 1
    return 0


def swlib_tr(src, to, hence):
    ret = 0
    p1 = src
    while p1:
        if p1 == hence:
            p1 = to
            ret += 1
        p1 += 1
    return ret


def swlib_check_safe_path(s):
    if not s:
        return 1
    if not len(s):
        return 5
    if s.find(".."):
        return 6
    if s.find(' '):
        return 7
    return swlib_is_sh_tainted_string(s)


def swlib_check_clean_path(s):
    if not s:
        return 1
    if not len(s):
        return 2
    if s.find("//"):
        return 3
    if s.find(".."):
        return 4
    if s.find(' '):
        return 5
    if swlib_is_sh_tainted_string(s):
        return 6
    return 7 if strpbrk(s, swlibc.SW_WS_TAINTED_CHARS) else 0


def swlib_check_clean_absolute_path(s):
    if not s:
        return 1
    if not len(s):
        return 2
    if s[0] != '/':
        return 3
    return swlib_check_clean_path(s)


def swlib_check_clean_relative_path(s):
    if not s:
        return 1
    if s[0] == '/':
        return 2
    return swlib_check_clean_path(s)


def swlib_check_legal_tag_value(s):
    if not s:
        return 0
    return 1 if strpbrk(s, ":.,") else 0


def swlib_is_sh_tainted_string(s):
    if not s:
        return 0
    return 1 if strpbrk(s, swlibc.SW_TAINTED_CHARS) else 0


def swlib_is_sh_tainted_string_fatal(s):
    if not s:
        return
    if strpbrk(s, swlibc.SW_TAINTED_CHARS):
        SWLIB_FATAL("tainted string")
    return


def swlib_safe_read(fd, buf, nbyte):
    n = 0
    nret = 1
    p = buf
    while n < nbyte and nret:
        nret = uxfio_read(fd, p + n, (nbyte - n))
        if nret < 0:
            if errno in ('EINTR', 'EAGAIN'):
                continue
            return nret
        n += nret
    return n


def swlib_swprog_assert(error_code, status, reason, version, file, line, function):
    if error_code != 0 and swlibc.SW_PROGS_USER_ERROR > error_code >= swlibc.SW_PROGS_IMPLEMENTATION_ERROR:
        print(f"{swlib_utilname_get()}: Error: code=[{error_code}]: {reason}: version=[{version}] file=[{file}] line=[{line}]")
        exit(status)
    elif error_code != 0 and error_code >= swlibc.SW_PROGS_USER_ERROR:
        exit(status)
    elif error_code != 0 and error_code < swlibc.SW_PROGS_IMPLEMENTATION_ERROR:
        print(f"Internal Informative Warning: code={error_code}: {reason} version=[{version}], file=[{file}], line=[{line}]")


def swlib_squash_trailing_char(path, ch):
    return squash_trailing_char(path, ch, 0)


def swlib_squash_all_trailing_vnewline(path):
    while True:
        if squash_trailing_char(path, '\n', 0) == 0:
            squash_trailing_char(path, '\r', 0)
        else:
            break


def swlib_squash_trailing_vnewline(path):
    squash_trailing_char(path, '\r', 0)
    squash_trailing_char(path, '\n', 0)


def swlib_squash_trailing_slash(path):
    squash_trailing_char(path, '/', 1)
    return


def swlib_squash_embedded_dot_slash(path):
    p1 = path.find("/./")
    if p1 != -1 and p1 != 0:
        path = path[:p1 + 1] + path[p1 + 3:]
    elif p1 != -1 and p1 == 0:
        path = path[:p1 + 1] + path[p1 + 3:]


def swlib_squash_double_slash(path):
    while True:
        p1 = path.find("//")
        if p1 != -1:
            path = path[:p1] + path[p1 + 1:]
        else:
            break


def swlib_return_no_leading(path):
    if path.startswith("/") and len(path) > 1:
        return path[1:]
    elif path.startswith("./") and len(path) > 2:
        return path[2:]
    return path


def swlib_squash_all_dot_slash(path):
    swlib_squash_leading_dot_slash(path)
    s = path.find("/./")
    while s != -1:
        swlib_squash_embedded_dot_slash(path)
        s = path.find("/./")


def swlib_squash_leading_dot_slash(path):
    if len(path) >= 3:
        if path.startswith("./"):
            path = path[2:]


def swlib_toggle_trailing_slashdot(mode, name, pflag):
    if mode == "drop":
        if len(name) < 2:
            pflag = 0
            return
        if name.endswith("/."):
            name = name[:-2]
            pflag = 1
    else:
        if pflag:
            name += "/."
            pflag = 0


def swlib_toggle_leading_dotslash(mode, name, pflag):
    if mode == "drop":
        if len(name) < 2:
            pflag = 0
            return
        elif name.startswith("./") and len(name) > 2:
            name = name[2:]
            pflag = 1
        else:
            pflag = 0
            return
    else:
        if pflag:
            if name == ".":
                name += "/"
            else:
                name = "." + name
                pflag = 0


def swlib_toggle_trailing_slash(mode, name, pflag):
    if mode == "drop":
        if len(name) > 1 and name.endswith("/"):
            name = name[:-1]
            pflag = 1
        else:
            pflag = 0
    else:
        if pflag:
            name += "/"


def swlib_toggle_all_trailing_slash(mode, name, pflag):
    pf = 0
    pflag = 0
    swlib_toggle_trailing_slash(mode, name, pf)
    while pf:
        pflag += 1
        pf = 0
        swlib_toggle_trailing_slash(mode, name, pf)


def swlib_squash_leading_slash(name):
    if name.startswith("/"):
        name = name[1:]


def swlib_return_relative_path(path):
    s = path
    while s.startswith("/"):
        s = s[1:]
    return s


def swlib_squash_all_leading_slash(name):
    if not name:
        return
    s = len(name)
    i = int(name.startswith("/"))
    while name.startswith("/"):
        swlib_squash_leading_slash(name)
    if i and s and len(name) == 0:
        name = "."


def swlib_toggle_leading_slash(mode, name, pflag):
    if mode == "drop":
        if name.startswith("/") and len(name) > 1:
            name = name[1:]
            pflag = 1
        else:
            pflag = 0
    elif mode == "restore":
        if pflag:
            name = "/" + name
            pflag = 0


def swlib_slashclean(path):
    swlib_squash_double_slash(path)
    swlib_squash_leading_dot_slash(path)
    swlib_squash_trailing_slash(path)
    return


def swlib_process_hex_escapes(s1):
    process_all_hex_escapes(s1)
    return 0


def swlib_compare_8859(s1, s2):
    ret = 0
    so1 = None
    so2 = None
    if "\\" in s1:
        print(f"unexpanding S1 [{s1}]")
        so1 = strob_open(initial_size)
        swlib_unexpand_escapes(so1, s1)
        s1 = strob_str(so1)
        process_all_hex_escapes(s1)
    if "\\" in s2:
        print(f"unexpanding S2 [{s2}]")
        so2 = strob_open(initial_size)
        swlib_unexpand_escapes(so2, s2)
        s2 = strob_str(so2)
        process_all_hex_escapes(s2)
    if "#" in s1 or "#" in s2:
        print(f"Comparing [{s1}] [{s2}]")
    ret = swlib_dir_compare(s1, s2, swlibc.SWC_FC_NOAB)
    if so1:
        strob_close(so1)
    if so2:
        strob_close(so2)
    return ret


def swlib_vrelpath_compare(s1, s2, cwd):
    if (s1[0] != "/" and s2[0] != "/") or (s1[0] == "/" and s2[0] == "/"):
        return swlib_dir_compare(s1, s2, swlibc.SWC_FC_NOOP)
    else:
        tmp = strob_open(100)
        if form_abspath(tmp, s1, cwd) == 0:
            return swlib_dir_compare(strob_str(tmp), s2, swlibc.SWC_FC_NOOP)
        elif form_abspath(tmp, s2, cwd) == 0:
            return swlib_dir_compare(s1, strob_str(tmp), swlibc.SWC_FC_NOOP)
        else:
            SWLIB_ALLOC_ASSERT(0)

    SWLIB_ALLOC_ASSERT(0)
    return -1


def swlib_basename_compare(s1, s2):
    s1b = s1.rfind("/")
    s2b = s2.rfind("/")
    if s1b and len(s1) and s1[s1b + 1:]:
        s1 = s1[s1b + 1:]
    if s2b and len(s2) and s2[s2b + 1:]:
        s2 = s2[s2b + 1:]
    ret = swlib_dir_compare(s1, s2, swlibc.SWC_FC_NOOP)
    return ret


def swlib_dir_compare(s1, s2, FC_compare_flag):
    ret = 0
    leading_p1 = 0
    leading_p2 = 0
    leading_d1 = 0
    leading_d2 = 0
    trailing_p1 = 0
    trailing_d1 = 0
    trailing_p2 = 0
    trailing_d2 = 0
    if FC_compare_flag == swlibc.SWC_FC_NOAB:
        swlib_toggle_leading_slash("drop", s1, leading_p1)
        swlib_toggle_leading_slash("drop", s2, leading_p2)
    elif FC_compare_flag == swlibc.SWC_FC_NORE:
        pass
    else:
        pass
    swlib_toggle_trailing_slash("drop", s1, trailing_p1)
    swlib_toggle_trailing_slash("drop", s2, trailing_p2)
    swlib_toggle_trailing_slashdot("drop", s1, trailing_d1)
    swlib_toggle_trailing_slashdot("drop", s2, trailing_d2)
    swlib_toggle_leading_dotslash("drop", s1, leading_d1)
    swlib_toggle_leading_dotslash("drop", s2, leading_d2)
    ret = strcmp(s1, s2)
    swlib_toggle_leading_dotslash("restore", s1, leading_d1)
    swlib_toggle_leading_dotslash("restore", s2, leading_d2)
    swlib_toggle_trailing_slashdot("restore", s1, trailing_d1)
    swlib_toggle_trailing_slashdot("restore", s2, trailing_d2)
    swlib_toggle_trailing_slash("restore", s1, trailing_p1)
    swlib_toggle_trailing_slash("restore", s2, trailing_p2)
    if FC_compare_flag == swlibc.SWC_FC_NOAB:
        swlib_toggle_leading_slash("restore", s1, leading_p1)
        swlib_toggle_leading_slash("restore", s2, leading_p2)
    elif FC_compare_flag == swlibc.SWC_FC_NORE:
        pass
    else:
        pass
    return ret


def swlib_dirname(dest, source):
    s = strob_strcpy(dest, source)
    s = s.rfind('/')
    if s == -1:
        strob_strcpy(dest, ".")
    else:
        if s != 0:
            dest[:s] = ''
        else:
            dest[s + 1:] = ''
    return strob_str(dest)


def swlib_basename(dest, source):
    if dest is None:
        s = source.rfind('/')
        if s == -1:
            return source
        s += 1
        while source[s] == '/':
            s += 1
        if source[s] == '\0':
            if s > 0:
                return source[s - 1]
            return source
        return source[s]
    else:
        dest = source
        s = swlib_basename(None, dest)
        dest = dest.replace(dest, s, len(s))
        return dest


def swlib_unix_dirtrunc(buf):
    s = buf.rfind('/')
    if s == -1 or s == 0 or (s == 1 and ord(buf[0]) == '.'):
        return 0
    buf = buf.replace(buf[s:], '', len(buf[s:]))
    return 1


def swlib_unix_dirtrunc_n(buf, n):
    i = n
    while i > 0:
        ret = swlib_unix_dirtrunc(buf)
        if ret == 0:
            break
        i -= 1
    return n - i


def swlib_unix_dircat(dest, dirname):
    s = dest
    if not dirname or len(dirname) == 0:
        return 0
    if len(s):
        if s[-1] != '/':
            dest += '/'
    newp = dirname
    if len(dest):
        swlib_squash_leading_dot_slash(newp)
    dest += newp
    swlib_squash_double_slash(dest)
    return 0


def swlib_resolve_path(ppath, depth, resolved_path):
    tmp = []
    startnames = 0
    count = 0
    numcomponents = 0
    path = ppath
    swlib_slashclean(path)
    if resolved_path:
        resolved_path = ''
    s = path.split('/')
    for i in range(len(s)):
        if s[i] == '..' and not startnames:
            count += 1
        elif s[i] == '.':
            pass
        elif s[i] == '..' and startnames:
            count -= 1
        else:
            count += 1
            startnames = 1
        if resolved_path:
            resolved_path += s[i]
            resolved_path += '/'
        numcomponents += 1
    if depth:
        depth = count
    return numcomponents


def swlib_exec_filter(cmd, src_fd, output):
    input_pipe = []
    ifd = src_fd
    i = 0
    while cmd[i]:
        i += 1
    ofd = shcmd_get_dstfd(cmd[i - 1])
    input_pipe.append(-1)
    input_pipe.append(-1)
    if ifd >= 0:
        os.pipe()
        childi = os.fork()
        if childi == 0:
            os.setpgrp()
            signal.signal(signal.SIGPIPE, signal.SIG_DFL)
            signal.signal(signal.SIGINT, signal.SIG_DFL)
            signal.signal(signal.SIGTERM, signal.SIG_DFL)
            signal.signal(signal.SIGUSR1, signal.SIG_DFL)
            signal.signal(signal.SIGUSR2, signal.SIG_DFL)
            if ifd != 0:
                os.close(0)
            if ifd != 1:
                os.close(1)
            os.close(input_pipe[0])
            os.close(ofd)
            # retval = swlib_pump_amount(input_pipe[1], ifd, output, -1, do_size)
            retval = swlib_pump_amount(input_pipe[1], ifd, output)
            if retval < 0:
                ret = 1
            else:
                ret = 0
            sys.exit(ret)
        if childi < 0:
            raise Exception("swlib_exec_filter: 0001.")
        os.close(input_pipe[1])
        shcmd_set_srcfd(cmd[0], input_pipe[0])
        if output:
            swlib_shcmd_output_strob(output, int(cmd))
        else:
            shcmd_command(output, cmd)
        os.close(input_pipe[0])
        cret = 126
        ret = os.waitpid(childi, 0)
        if ret[0] == childi:
            if os.WIFEXITED(ret[1]):
                cret = os.WEXITSTATUS(ret[1])
                if cret != 0:
                    raise Exception("")
            else:
                raise Exception("")
        else:
            if ret[0] < 0:
                raise Exception(ret[1])
    else:
        cret = 0
        if output:
            swlib_shcmd_output_strob(output, cmd)
        else:
            shcmd_command(output, cmd)
    if cret == 127:
        pass
    else:
        retval = shcmd_wait(output, cmd)
        return retval
    return cret


def swlib_synct_create():
    synct = SYNCT()
    length = (3 * swlibc.SWLIB_SYNCT_BLOCKSIZE) + 1
    synct.buf = (synct.buf * (3 * swlibc.SWLIB_SYNCT_BLOCKSIZE + 1))
    if not synct.buf:
        sys.exit(44)
    synct.buf = b'\0' * length
    synct.tail = cast(addressof(synct.buf) + swlibc.SWLIB_SYNCT_BLOCKSIZE, POINTER(c_ubyte))
    synct.count = 0
    synct.mt = swlibc.SW_SYNCT_EOF
    synct.do_debug = 0
    synct.debugfd = -1
    return synct


def swlib_synct_delete(synct):
    if synct.count % 2:
        print(f"Warning: swlib_synct_delete() block count error: {synct.count}", file=sys.stderr)
    if synct.debugfd > 0:
        os.close(synct.debugfd)
    del synct


def swlib_synct_read(synct, fd, userbuf):
    ibuf = synct.buf
    tbuf = synct.tail
    ret = synct_i_read_block(synct, uxfio_sfa_read, fd, ibuf)
    if ret < 0:
        return -1
    if ret == 0:
        return 0
    check_ret = synct_check_for_possible_eof(synct)
    if check_ret == swlibc.SYNCT_EOF_CONDITION_0:
        userbuf = ibuf[:ret]
        return swlibc.SWLIB_SYNCT_BLOCKSIZE
    else:
        ret = synct_i_read_block(synct, uxfio_sfa_read, fd, tbuf)
        if ret < 0:
            return -2
        z = bytearray(swlibc.SWLIB_SYNCT_BLOCKSIZE)
        ret = z == tbuf
        if ret == 0:
            return 0
        else:
            userbuf = ibuf + tbuf
            return 2 * swlibc.SWLIB_SYNCT_BLOCKSIZE


def swlib_synct_suck(ofd, ifd):
    ret = 0
    wret = 0
    wtotal = 0
    buf = bytearray(swlibc.SWLIB_SYNCT_BLOCKSIZE + swlibc.SWLIB_SYNCT_BLOCKSIZE + 1)
    synct = swlib_synct_create()
    while True:
        ret = swlib_synct_read(synct, ifd, buf)
        if ret > 0:
            wret = synct_i_no_null_write(ofd, buf, ret)
            if wret < 0:
                return -2
            wtotal += wret
        if ret <= 0:
            break
    swlib_synct_delete(synct)
    if ret < 0:
        return ret
    return wtotal


def swlib_read_amount(suction_fd, amount):
    return swlib_pump_amount(-1, suction_fd, amount)


def swlib_pipe_pump(ofd, ifd):
    return swlib_pump_amount(ofd, ifd, -1)


def swlib_is_c701_escape(c):
    if c == '\"' or c == '#' or c == '\\' or c == 0:
        return 1
    else:
        return 0


def swlib_is_ansi_escape(c):
    if (c == 'n' or c == 't' or c == 'v' or c == 'b' or c == 'r' or c == 'f' or c == 'x' or
            c == 'a' or c == '\\' or c == '?' or c == '\'' or c == '\"'):
        return 1
    else:
        return 0


def swlib_c701_escaped_value(src, is_escape):
    is_escape[0] = 1
    if src == '\\':
        return ord('\\')
    elif src == '#':
        return ord('#')
    elif src == '\"':
        return ord('\"')
    is_escape[0] = 0
    return ord(src)


def swlib_ansi_escaped_value(src, is_escape):
    is_escape[0] = 1
    if src == '\\':
        return ord('\\')
    elif src == 'n':
        return ord('\n')
    elif src == 'r':
        return ord('\r')
    elif src == 'v':
        return ord('\v')
    elif src == 'b':
        return ord('\b')
    elif src == 'f':
        return ord('\f')
    is_escape[0] = 0
    return ord(src)


def swlib_strdup(s):
    return s


def swlib_strncpy(dst, src, n):
    p = dst[:n - 1] + '\0'
    return p


def swlib_writef(fd, buffer, format, args):
    ret = len(format) % args
    if ret < 0:
        return -1
    newret = os.write(fd, buffer + format % args)
    if newret != ret:
        return -1
    return newret


def swlib_write_OLDcatalog_stream(package, ofd):
    ret = taruib_write_catalog_stream(package, ofd, 0, 0)
    return ret


def swlib_write_catalog_stream(package, ofd):
    ret = taruib_write_catalog_stream(package, ofd, 1, 0)
    return ret


def swlib_write_storage_stream(package, ofd):
    ret = taruib_write_storage_stream(package, ofd, 1, -1, 0, 0)
    return ret


def swlib_write_signing_files(package, ofd, which_file, do_adjunct_md5):
    nullfd = os.open("/dev/null", os.O_RDWR)
    ifd = xformat_get_ifd(package)
    retval = 0
    namebuf = strob_open(100)
    swpath = swpath_open("")
    nullblock = bytearray(512)
    do_trailer = 0
    format = xformat_get_format(package)
    bytes = 0
    if not swpath:
        return -21
    if ifd < 0:
        return -32
    while (ret := xformat_read_header(package)) > 0:
        if xformat_is_end_of_archive(package):
            break
        xformat_get_name(package, namebuf)
        name = strob_str(namebuf)
        swpath_parse_path(swpath, name)
        if swpath_get_is_catalog(swpath) == SWPATH_CTYPE_DIR:
            if which_file == 0:
                writeit = nullfd
            else:
                writeit = ofd
        elif swpath_get_is_catalog(swpath) == SWPATH_CTYPE_CAT:
            if which_file == 0:
                writeit = ofd
            else:
                writeit = nullfd
        elif swpath_get_is_catalog(swpath) == SWPATH_CTYPE_STORE:
            if which_file == 0:
                do_trailer = 1
                break
            else:
                if do_adjunct_md5:
                    filetype = xformat_get_tar_typeflag(package)
                    if filetype != tarfile.REGTYPE and filetype != tarfile.DIRTYPE:
                        writeit = nullfd
                    else:
                        writeit = ofd
                else:
                    writeit = ofd
        else:
            print("internal error returned by swpath_get_is_catalog")
            retval = -1
            break
        bytesret = 0
        xformat_set_ofd(package, writeit)
        bytesret += xformat_write_header(package)
        bytesret += xformat_copy_pass(package, writeit, ifd)
        if writeit != nullfd:
            bytes += bytesret
    if do_trailer and (format == 'arf_ustar' or format == 'arf_tar'):
        os.write(ofd, nullblock)
        os.write(ofd, nullblock)
    if which_file == 1:
        retval += taru_write_archive_trailer(package.taruM, 'arf_ustar', ofd, 512, bytes,
                                             xformat_get_tarheader_flags(package))
    os.close(nullfd)
    strob_close(namebuf)
    swpath_close(swpath)
    return retval


def swlib_kill_all_pids(pid, num, signo, verbose_level):
    ret = 0
    for i in range(num):
        if pid[i] > 0:
            swlib_doif_writef(verbose_level, lc.SWC_VERBOSE_SWIDB, None, sys.stderr.fileno(),
                              "swlib_kill_all_pids: kill[%d] signo=%d\n", pid[i], signo)
            print("killing pid %d with signal %d", pid[i], signo)
            try:
                os.kill(pid[i], signo)
            except OSError as e:
                swlib_doif_writef(verbose_level, lc.SWC_VERBOSE_SWIDB, None, sys.stderr.fileno(),
                                  "kill[%d] signo=%d : error : %s\n", pid[i], signo, e.strerror)
                ret += 1
    return ret


def swlib_update_pid_status(keypid, value, pid, status, len):
    if len == 0:
        return 0
    for i in range(len):
        if pid[i] == keypid:
            status[i] = value
            pid[i] = -pid[i]
            return 0
    return -1


def swlib_wait_on_all_pids(pid, num, status, flags, verbose_level):
    return swlib_wait_on_all_pids_with_timeout(pid, num, status, flags, verbose_level, 0)


def swlib_wait_on_pid_with_timeout(pid, status, flags, verbose_level, fp_tmo):
    apid = pid
    ret = swlib_wait_on_all_pids_with_timeout(apid, 1, status, flags, verbose_level, fp_tmo)
    return ret


def swlib_wait_on_all_pids_with_timeout(pid, num, status, flags, verbose_level, fp_tmo):
    wret = -99
    done = 0
    do_kill = 0

    if fp_tmo < 0:
        do_kill = 1
        tmo = -fp_tmo
    else:
        tmo = fp_tmo

    if num == 0:
        return 0
    print("")
    start = time.time()
    now = start

    while not done and (tmo == 0 or int(now - start) < tmo):
        done = 1
        now = time.time()
        print("")
        for i in range(num):
            if pid[i] > 0:
                print("pid={}".format(int(pid[i])))
                wret, status[i] = os.waitpid(pid[i], flags)
                print("wret={}".format(wret))
                if wret < 0:
                    print("swlib_wait_on_all_pids[{}]: error : {} {}\n".format(int(pid[i]), int(pid[i]), os.error()))
                    status[i] = 0
                    pid[i] = -pid[i]
                elif wret == 0:
                    print("swlib_wait_on_all_pids[{}]: returned zero waiting for process id {}\n".format(int(pid[i]),
                                                                                                         int(pid[i])))
                    done = 0
                else:
                    print("swlib_wait_on_all_pids[{}]: returned exitval={}\n".format(int(pid[i]),
                                                                                     os.WEXITSTATUS(status[i])))
                    got_one = 1
                    pid[i] = -pid[i]
            else:
                print("already processed: pid={}".format(pid[i]))

        if flags == os.WNOHANG:
            done = 1

    if do_kill:
        print("killing pids SIGINT")
        # swlib_kill_all_pids(pid, num, SIGINT, 3)
        time.sleep(1)
        print("killing pids SIGKILL")
        # swlib_kill_all_pids(pid, num, SIGKILL, 3)

    return wret  # Now retval of last waitpid()   # was status[num-1];


def swlib_wait_on_all_pids_with_timeout_delete(pid, num, status, flags, verbose_level, fp_tmo):
    wret = -99
    done = False
    do_kill = False
    if fp_tmo < 0:
        do_kill = True
        tmo = -fp_tmo
    else:
        tmo = fp_tmo
    if num == 0:
        return 0
    print("")
    start = time.time()
    now = start
    while not done and (tmo == 0 or int(now - start) < tmo):
        done = True
        now = time.time()
        print("")
        for i in range(num):
            if pid[i] > 0:
                print("pid=%d", pid[i])
                wret, status[i] = os.waitpid(pid[i], flags)
                print("wret=%d", wret)
                if wret < 0:
                    swlib_doif_writef(verbose_level, lc.SWC_VERBOSE_SWIDB, None, sys.stderr.fileno(),
                                      "swlib_wait_on_all_pids[%d]: error : %d %s\n", pid[i], pid[i], os.error())
                    status[i] = 0
                    pid[i] = -pid[i]
                elif wret == 0:
                    swlib_doif_writef(verbose_level, lc.SWC_VERBOSE_SWIDB, None, sys.stderr.fileno(),
                                      "swlib_wait_on_all_pids[%d]: returned zero waiting for process id %d\n", pid[i],
                                      pid[i])
                    done = False
                else:
                    swlib_doif_writef(verbose_level, lc.SWC_VERBOSE_SWIDB, None, sys.stderr.fileno(),
                                      "swlib_wait_on_all_pids[%d]: returned exitval=%d\n", pid[i],
                                      os.WEXITSTATUS(status[i]))
                    #                    got_one = True
                    pid[i] = -pid[i]
        if flags == os.WNOHANG:
            done = True
    if do_kill:
        print("killing pids SIGINT")
        swlib_kill_all_pids(pid, num, signal.SIGINT, 3)
        time.sleep(1)
        print("killing pids SIGKILL")
        swlib_kill_all_pids(pid, num, signal.SIGKILL, 3)
    return wret


def swlib_sha1(uxfio_fd, digest):
    resblock = hashlib.sha1(uxfio_fd).digest()
    digest_hex_bytes = 40
    p = digest
    for i in range(digest_hex_bytes // 2):
        p += '%02x' % resblock[i]
    digest[digest_hex_bytes] = '\0'
    return 0


def swlib_digests(ifd, md5, sha1, size, sha512):
    digs = taru_digs_create()
    if md5:
        digs.do_md5 = chc.DIGS_ENABLE_ON
    if sha1:
        digs.do_sha1 = chc.DIGS_ENABLE_ON
    if sha512:
        digs.do_sha512 = chc.DIGS_ENABLE_ON
    if size:
        digs.do_size = chc.DIGS_ENABLE_ON
    im = swlib_digs_copy(-1, ifd, -1, digs, -1)
    if md5:
        md5 = digs.md5
    if sha1:
        sha1 = digs.sha1
    if sha512:
        sha512 = digs.sha512
    if size:
        size = digs.size
    taru_digs_delete(digs)
    if im >= 0:
        return 0
    return -1


def swlib_md5_copy(ifd, count, md5, ofd):
    digs = taru_digs_create()
    im = swlib_digs_copy(ofd, ifd, count, digs, -1)
    md5 = digs.md5
    taru_digs_delete(digs)
    return im


def swlib_digs_copy(ofd, ifd, count, digs, adjunct_ofd):
    n = swlib_i_digs_copy(ofd, ifd, count, digs, adjunct_ofd, swlib_safe_read)
    return n


def swlib_shcmd_output_fd(cmdvec):
    status = 0
    vc = cmdvec
    while vc:
        vc += 1
    if vc == cmdvec:
        return -1
    vc -= 1
    lastcmd = vc
    fd = os.open("", os.O_RDONLY)
    if fd < 0:
        return fd
    uxfio_fcntl(fd, uc.UXFIO_F_SET_BUFACTIVE, 100)
    uxfio_fcntl(fd, uc.UXFIO_ON, 100)
    uxfio_fcntl(fd, uc.UXFIO_F_SET_BUFTYPE, 100)
    uxfio_fcntl(fd, uc.UXFIO_BUFTYPE_DYNAMIC_MEM, 100)
    ot = os.pipe()
    pid = swfork(None)
    if pid < 0:
        return pid
    if pid == 0:
        signal.signal(signal.SIGPIPE, signal.SIG_DFL)
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        signal.signal(signal.SIGTERM, signal.SIG_DFL)
        signal.signal(signal.SIGUSR1, signal.SIG_DFL)
        signal.signal(signal.SIGUSR2, signal.SIG_DFL)
        os.close(ot[0])
        shcmd_set_dstfd(lastcmd, ot[1])
        # SHCMD.shcmd_cmdvec_exec((cmdvec))
        # SHCMD.shcmd_cmdvec_wait2(cmdvec)
        os.close(ot[1])
        sys.exit(0)
    os.close(ot[1])
    print("")
    swlib_pipe_pump(fd, ot[0])
    print("")
    os.waitpid(pid, status)
    os.lseek(fd, 0, os.SEEK_SET)
    os.close(ot[0])
    return fd


def swlib_shcmd_output_strob(output, cmdvec):
    base = None
    data_len = 0
    buffer_len = 0
    fd = swlib_shcmd_output_fd(cmdvec)
    if fd < 0:
        return -1
    if uxfio_get_dynamic_buffer(fd, base, buffer_len, data_len) < 0:
        return -1
    output += base[:data_len]
    os.close(fd)
    return 0


def swlib_apply_mode_umask(type, umask, mode):
    if mode == 0:
        if type == lc.SW_ITYPE_d:
            mode = 0o777
        else:
            mode = 0o666
    mode &= ~umask
    return mode


def swlib_open_memfd():
    fd = os.open("", os.O_RDWR)
    if fd < 0:
        return fd
    uxfio_fcntl(fd, uc.UXFIO_F_SET_BUFACTIVE, uc.UXFIO_ON)
    uxfio_fcntl(fd, uc.UXFIO_F_SET_BUFTYPE, uc.UXFIO_BUFTYPE_DYNAMIC_MEM)
    return fd


def swlib_close_memfd(fd):
    return os.close(fd)


def swlib_pad_amount(fd, amount):
    nullblock = bytearray(512)
    remains = amount
    count = 0
    while remains > 0:
        if remains > 512:
            am = 512
        else:
            am = remains
        if am <= 0:
            break
        ret = os.write(fd, nullblock[:am])
        if ret <= 0:
            if ret < 0:
                print("%s: write error: %s" % (swlib_utilname_get(), os.error()), file=sys.stderr)
            else:
                print("")
            if count <= 0:
                return -1
            return -count
        count += ret
        remains -= ret
    return count


def swlib_drop_root_privilege():
    ret = 1
    if os.getuid() == 0:
        nob = os.geteuid()
        if nob < 10:
            print(f"User name 'nobody' has a uid of {nob}")
        try:
            os.setuid(nob)
            ret = 0
        except OSError as e:
            print(f"setuid(uid={nob}) failed at {__file__}:{fr.__LINE__}: {os.strerror(e.errno)}")
            ret = 2
    else:
        ret = 0
    return ret


def swlib_add_trailing_slash(path):
    if not path:
        path += "/"
    elif path[-1] != '/':
        path += "/"
    return path


def swlib_altfnmatch(s1, s2):
    patterns1 = s1.split("|")
    patterns2 = s2.split("|")
    for pattern1 in patterns1:
        for pattern2 in patterns2:
            if fnmatch.fnmatch(pattern2, pattern1):
                return 0
    return 1


def swlib_unexpand_escapes(store, src):
    n = len(src)
    k = 0
    retval = 0
    length = 0
    is_recognized = [0 in range(0, 0)]
    if store is None:
        dst = src
    else:
        store = ""
        dst = store
    while k < n:
        if src[k] == '\\' and src[k + 1] != '\0':
            escaped_value = swlib_ansi_escaped_value(ord(src[k + 1]), is_recognized)
            if not is_recognized:
                escaped_value = swlib_c701_escaped_value(ord(src[k + 1]), is_recognized)
            if escaped_value < 0:
                dst[length] = '\0'
                SWLIB_ASSERT(0)
            if is_recognized == 0:
                dst[length] = src[k]
                length += 1
                k += 1
            else:
                dst[length] = escaped_value
                length += 1
                k += 2
        else:
            dst[length] = src[k]
            length += 1
            k += 1
    dst[length] = '\0'
    return retval


def swlib_unexpand_escapes2(src):
    dst = ""
    ret = swlib_unexpand_escapes(dst, src)
    if ret:
        return ret
    if len(dst) > len(src):
        return -1
    src = dst
    return 0


def swlib_tee_to_file(filename, ifd, buf, length, do_append):
    flags = os.O_RDWR | os.O_CREAT | os.O_TRUNC
    if do_append:
        flags = os.O_RDWR | os.O_CREAT | os.O_APPEND
    if filename.isdigit():
        fd = int(filename)
    else:
        fd = os.open(filename, flags, 0o644)
    if fd < 0:
        return fd
    if ifd < 0:
        if length < 0:
            ret = os.write(fd, buf)
            if ret != len(buf):
                return -1
        else:
            ret = os.write(fd, buf[:length])
            if ret != length:
                return -1
    else:
        ret = swlib_pipe_pump(fd, ifd)
    os.close(fd)
    return ret


def swlib_expand_escapes(pa, newlen, src, ustore):
    n = len(src)
    count = 0
    i = 0
    j = 0
    k = 0
    lp = src

    if ustore:
        store = ustore
    else:
        store = ustore
        strob_strcpy("", store)
    if not store:
        return -1
    while k < n:
        if lp[k] == '\\':
            count += 1
            if count % 2 and lp[k + 1] == 'n':
                count = 0
                strob_chr_index(store, i + j, '\n')
                # store[i + j] = '\n'
                k += 1
            else:
                strob_chr_index(store, i + j, lp[k])
                # store[i + j] = lp[k]
                if count % 2:
                    if not swlib_is_ansi_escape(lp[k + 1]) and not swlib_is_c701_escape(lp[k + 1]):
                        j += 1
                        strob_chr_index(store, i + j, lp[k])
                        # store[i + j] = lp[k]
                    else:
                        pass
            i += 1
            k += 1
        elif 7 > lp[k] > 0 or ord(lp[k]) >= 127:
            strob_sprintf(store, 1, "\\x%02X" % ord(lp[k]))
            # store = "\\x%02X" % ord(lp[k])
            j += 3
        else:
            count = 0
            if lp[k] == '#':
                # if store[i + j - 1] != '\\':
                # store[i + j] = '\\'
                if store.strob_get_char(i + j - 1) != '\\':
                    strob_chr_index(store, i + j, '\\')
                    j += 1
                # store[i + j] = '#'
                strob_chr_index(store, i + j, '#')
            elif lp[k] == '\"':
                # if store[i + j - 1] != '\\':
                # store[i + j] = '\\'
                if store.strob_get_char(i + j - 1) != '\\':
                    strob_chr_index(store, i + j, '\\')
                    j += 1
                # store[i + j] = '\"'
                strob_chr_index(store, i + j, '\"')
            else:
                # store[i + j] = lp[k]
                strob_chr_index(store, i + j, lp[k])
            i += 1
            k += 1
    # store[i + j] = '\0'
    strob_chr_index(store, i + j, '\0')
    if not ustore:
        if pa:
            pa[0] = strob_release(store)
    else:
        if pa:
            pa[0] = strob_str(store)
    if newlen:
        newlen = i + j
    return 0


def swlib_imaxtostr(i, pbuf):
    buf = ""
    ret = ""
    if pbuf is None:
        if not buf:
            buf = ""
        buf = ""
    else:
        buf = pbuf
    buf = str(i)
    return ret


def swlib_umaxtostr(i, pbuf):
    buf = ""
    ret = ""
    if pbuf is None:
        if not buf:
            buf = ""
    else:
        buf = pbuf
    buf = str(i)
    return ret


def swlib_get_umask():
    mode = 0o777
    mode = os.umask(mode)
    os.umask(mode)
    return mode


def swlib_ascii_text_fd_to_buf(pbuf, ifd):
    amount = [0]
    ret = swlib_i_pipe_pump(ifd, -1, amount, -1, pbuf, uxfio_read)
    if (
            ret != 0 or
            amount != len(pbuf)
    ):
        sys.stderr.write("swlib_ascii_text_fd_to_buf: warning, size not equal to strlen\n")
        ret = -1
    return ret


def swlib_apply_location(relocated_path, path, location, directory):
    if len(path) == 0:
        return
    clean_path = swlib_return_no_leading(path)
    clean_location = swlib_return_no_leading(location)
    clean_directory = swlib_return_no_leading(directory)
    if len(clean_directory) > 0 and len(clean_path) > 0:
        prefix = clean_path.find(clean_directory)
        if prefix is not None and prefix == clean_path:
            clean_path += len(clean_directory)
    else:
        pass
    relocated_path = location
    swlib_unix_dircat(relocated_path, clean_path)
    return


def swlib_attribute_check_default(object_, keyword, value):
    if value:
        return value
    d = swsdflt_get_default_value(object_, keyword)
    if d is None:
        print(f"no default value found for attribute: {object_} {keyword}")
        return ""
    return d


def swlib_is_option_true(s):
    if not s:
        return 0
    return swextopt_is_value_true(s)


def swlib_squash_illegal_tag_chars(s):
    s = s.replace('.', '_')
    s = s.replace(',', '_')
    s = s.replace(':', '_')
    return s


def swlib_append_synct_eof(buf):
    buf = ""
    buf += ") | dd ibs={} obs={} 2>/dev/null | dd ibs={} obs={} conv=sync 2>/dev/null |\n".format(
        swlibc.SWLIB_SYNCT_BLOCKSIZE * 3, swlibc.SWLIB_SYNCT_BLOCKSIZE * 3, swlibc.SWLIB_SYNCT_BLOCKSIZE * 2,
        swlibc.SWLIB_SYNCT_BLOCKSIZE * 2)
    buf += "(\n"
    buf += "    dd bs={} 2>/dev/null\n".format(swlibc.SWLIB_SYNCT_BLOCKSIZE * 2)
    buf += "    ( printf \"{}\\r\\n\\r\\n\"\n".format(tc.CPIO_INBAND_EOA_FILENAME)
    buf += "    ) | dd ibs={} obs={} conv=sync 2>/dev/null\n".format(swlibc.SWLIB_SYNCT_BLOCKSIZE * 2,
                                                                     swlibc.SWLIB_SYNCT_BLOCKSIZE * 2)
    buf += ")\n"
    buf += ") | dd ibs={} obs={} conv=sync 2>/dev/null\n".format(swlibc.SWLIB_SYNCT_BLOCKSIZE * 2,
                                                                 swlibc.SWLIB_SYNCT_BLOCKSIZE * 2)
    return buf
