.\" $Header: rcsheader placeholder $
.\" #BrickHeader# swmodify.1m # 2023/08/20 # #
.TA s
.TH swmodify 1M
.SH NAME
swmodify \- modify software products in a target root or depot
.SH SYNOPSIS
.HP
.C swmodify
.B [\| \-d | \-r \|]
.B [\| \-p \|]
.B [\| \-u \|]
.B [\| \-v \|]
.B [\| \-V \|]
.B [\| \-a\|
.IR attribute =[\| value \|]
.ifn .br
.B [\| \-c\|
.IR catalog\| \|]
.B [\| \-C\|
.IR session_file\| \|]
.ift .br
.B [\| \-f\|
.IR software_file\| \|]
.ifn .br
.B [\| \-P\|
.IR pathname_file\| \|]
.B [\| \-s\|
.IR product_specification_file |
.ifn .br
.B [\| \-S\|
.IR session_file\| \|]
.ift .br
.B [\| \-x
.IR option=value\| \|]
.B [\| \-X\|
.IR option_file\| \|]
.RI [\| software_selections \|]
.B [\| @
.IR target_selection \|]
.SS Remarks
For an overview of all SD commands, see the
.IR sd (5)
man page by
typing
.CR "man 5 sd"
on the command line.
.SH DESCRIPTION
The
.C swmodify
command modifies the definitions of software objects installed into a
primary or alternate root, or available from a software depot.  It
supports the following features:
.RS
.TP 3
\(bu
adding new objects -
The user can add new bundles, products, subproducts, filesets, control
files, and files to existing objects (which will contain them).
.TP
\(bu
deleting existing objects -
The user can delete existing bundles, products, subproducts, filesets,
control files, and files from the objects which contain them.
.TP
\(bu
modifying attribute values -
The user can add an attribute, delete an attribute, or change the
existing value of an attribute for any existing object.  When adding a
new object, the user can at the same time define attributes for it.
.TP
\(bu
committing software patches -
The user can remove saved backup files, committing the software patch.
.RE
.PP
With the exception of control files,
.C swmodify
does not manipulate the actual files that make up a product (fileset).
The command manipulates the catalog information which describes the
files.  However,
.C swmodify
can replace the contents of control files.
.PP
Common uses of
.C swmodify
include:
.RS
.TP 3
\(bu
adding file definitions to the existing list of file definitions in a
fileset.  Example: If a fileset's control scripts add new files to the
installed file system, the scripts can call
.C swmodify
to "make a record" of those new files.
.TP
\(bu
changing the values of existing attributes.  Example: If a product
provides a more complex configuration process (beyond the SD configure
script), that script can set the fileset's state to CONFIGURED upon
successful execution.
.TP
\(bu
defining new objects.
Example: to "import" the definition of an existing application that was
not installed by SD, construct a simple PSF describing the product.
Then invoke
.C swmodify
to load the definition of the existing application into the IPD.
.RE
.SS Options
.C swmodify
supports the following options:
.RS
.TP 10
.C -d
Perform modifications on a depot (not on a primary or alternate root).
The given
.I target_selection
must be a depot.
.TP
.C -p
Preview a modify session without modifying anything within the
.IR target_selection .
.\" -
.TP
.C -r
Performs modifications on an alternate root directory, which must be
specified in the
.CI "@ " "target_selections "
option.  (This option is not required for alternate root operations but
is maintained for backward compatibility.  See the
.CT "Alternate Root Directory and Depot Directory"
heading in
.IR sd (5)
for more information.)
.TP
.C -u
If no
.CI "-a " attribute = value
options are specified, then delete the given
.I software_selections
from within the given
.IR target_selection .
This action deletes the definitions of the software objects from the
depot catalog or installed products database.
.IP
If
.CI "-a " attribute
options are specified, then delete these attribute definitions from the given
.I software_selections
(from within the given
.IR target_selection ).
.TP
.C -v
Turn on verbose output to stdout.
.TP
.C -V
List the
data model revisions that this command supports.
.TP
.C "-a \s-1\f2attribute\fP\s0\f1[\|\fP=\s-1\f2value\fP\s0\f1\|]\fP"
Add, modify, or delete the
.I value
of the given
.IR attribute .
If the
.C -u
option is specified, then delete the
.I attribute
from the given
.I software_selections
(or delete the
.I value
from the set of values currently defined for the
.IR attribute ).
Otherwise add/modify the
.I attribute
for each
.I software_selection
by setting it to the given
.IR value .
.IP
Multiple
.C -a
options can be specified.  Each attribute modification will be applied to every
.IR software_selection .
.IP
The
.C -s
and
.C -a
options are mutually exclusive; the
.C -s
option cannot be specified when the
.C -a
option is specified.
.TP
.CI -c \0catalog
Specifies the pathname of the catalog which will be added, modified,
or used as input by
.CR swmodify .
.IP
The
.C -c
and
.C -a
options are mutually exclusive, the
.C -c
option cannot be specified when the
.C -a
option is specified.
.TP
.CI -C \0session_file
Save the current options and operands only to the
.IR session_file .
You can enter a relative or absolute path with the file name.
The default directory for session files is
.CR $HOME/.sw/sessions/ .
Without this option, by default, the session file is saved only
in the default directory
.CR $HOME/.sw/sessions/ .
.IP
You can recall a session file with the
.C -S
option.
.TP
.CI -f \0software_file
Read the list of
.I software_selections
from
.I software_file
instead of (or in addition to) the command line.
.TP
.CI -P \0pathname_file
Specify a file containing the pathnames of files being added to or
deleted from the IPD instead of having to specify them individually
on the command line.
.TP
.CI -s \0product_specification_file
The source
.IR "Product Specification File"
(PSF) describes the product, subproduct, fileset, and/or file
definitions which will be added, modified, or used as input by
.CR swmodify .
.IP
The
.C -s
and
.C -u
options are mutually exclusive, the
.C -s
option cannot be specified when the
.C -u
option is specified.
.TP
.CI -S \0session_file
Execute
.C swmodify
based on the options and operands saved from a previous session,
as defined in
.IR session_file .
You can save session information to a file with the
.C -C
option.
.TP
.CI -x \0option=value
Set the session
.I option
to
.I value
and override the default value (or a value in an alternate
.IR options_file
specified with
the
.C -X
option).
Multiple
.C -x
options can be specified.
.TP
.CI -X \0option_file
Read the session options and behaviors from
.IR options_file .
.RE
.SS Operands
The
.CD "swmodify"
command supports two types of operands:
.EM "software selections"
followed by
.EM "target selections".
These operands are separated by the "at"
.B ( @ )
character.  This syntax
implies that the command operates on "software selections at targets".
.SS Software Selections
.PP
If a
.I product_specification_file
is specified,
.C swmodify
will select the
.I software_selections
from the full set defined within the PSF.
The software selected from a PSF is then applied to the
.IR target_selection ,
with the selected software objects either added to
it or modified within it.
If a PSF is not specified,
.C swmodify
will select the
.I software_selections
from the software defined in the given (or default)
.IR target_selection .
.PP
The
.CD "swmodify"
command supports the following syntax for each
.IR software_selection :
.RS
.nf
.P
.C "\s-1\f2bundle\fP\s0\f1[\|\fP.\s-1\f2product\fP\s0\f1[\|\fP.\s-1\f2subproduct\fP\s0\f1\|]\fP\f1[\|\fP.\s-1\f2fileset\fP\s0\f1\|]\fP\f1\|]\fP\f1[\|\fP,\s-1\f2version\fP\s0\f1\|]\fP"
.P
.C "\s-1\f2product\fP\s0\f1[\|\fP.\s-1\f2subproduct\fP\s0\f1\|]\fP\f1[\|\fP.\s-1\f2fileset\fP\s0\f1\|]\fP\f1[\|\fP,\s-1\f2version\fP\s0\f1\|]\fP"
.fi
.RE
.\" Bullet list of software selection rules
.RS
.TP 3
\(bu
You can specify selections with the following
shell wildcard and pattern-matching notations:
.RS "" Plain
.IP
.CR "[\| \|]",
.CR * ,
.CR ?
.RE
.TP
\(bu
.I Bundles
and
.I subproducts
are recursive.
.I Bundles
can contain other
.I bundles
and
.I subproducts
can contain other
.IR subproducts .
.TP
\(bu
The
.C "\e*"
software specification selects all products.  Use this specification
with caution.
.RE
.PP
The
.C version
component has the form:
.RS
.nf
.P
.C "\f1[\|\fP,r \s-1\f2<op>\fP\s0 \s-1\f2revision\fP\s0\f1\|]\fP\f1[\|\fP,a \s-1\f2<op>\fP\s0 \s-1\f2arch\fP\s0\f1\|]\fP\f1[\|\fP,v \s-1\f2<op>\fP\s0 \s-1\f2vendor\fP\s0\f1\|]\fP"
.C "\f1[\|\fP,c \s-1\f2<op>\fP\s0 \s-1\f2category\fP\s0\f1\|]\fP\f1[\|\fP,q=\s-1\f2qualifier\fP\s0\f1\|]\fP\f1[\|\fP,l=\s-1\f2location\fP\s0\f1\|]\fP"
.C "\f1[\|\fP,fr \s-1\f2<op>\fP\s0 \s-1\f2revision\fP\s0\f1\|]\fP\f1[\|\fP,fa \s-1\f2<op>\fP\s0 \s-1\f2arch\fP\s0\f1\|]\fP"
.fi
.RE
.\" Bullet list of version syntax rules
.RS
.TP 3
\(bu
.I location
applies only to installed software and refers to software installed to
a location other than the default product directory.
.TP
\(bu
.C fr
and
.C fa
apply only to filesets.
.TP
\(bu
.C r
,
.C a
,
.C v
,
.C c
, and
.C l
apply only to bundles and products.  They are applied to the
leftmost bundle or product in a software specification.
.TP
\(bu
The
.IR <op>
(relational operator) component can be of the form:
.RS "" Plain
.IP
.CR "=",
.CR "==",
.CR ">=",
.CR "<=",
.CR "<",
.CR ">",
or
.CR "!="
.RE
.IP
which performs individual comparisons on dot-separated fields.
.IP
For example,
.C r>=B.10.00
chooses all revisions greater than or equal to
.CR B.10.00 .
The system compares each dot-separated field to find
matches.
.TP
\(bu
The
.C =
(equals) relational operator lets you specify selections with the
shell wildcard and pattern-matching notations:
.RS "" Plain
.IP
.CR "[\| \|]",
.CR "*",
.CR "?",
.CR "!"
.RE
.IP
For example, the expression
.C "r=1[\|01\|].*"
returns any revision in version 10 or version 11.
.TP
\(bu
All version components are repeatable within a single
specification (for example,
.CR "r>=A.12" ,
.CR "r<A.20" ).
If multiple components are used, the selection must match all
components.
.TP
\(bu
Fully qualified software specs include the
.CR r= ,
.CR a= ,
and
.CR v=
version components even if they contain empty strings.  For installed
software,
.C l=
is also included.
.TP
\(bu
No space or tab characters are allowed in a software selection.
.TP
\(bu
The software
.EM "instance_id"
can take the place of the version component.  It has the form:
.RS "" Plain
.IP
.I \f1[\|\fPinstance_id\f1\|]\fP
.RE
.IP
within the context of an exported catalog, where
.EM "instance_id"
is an integer that distinguishes versions of products and bundles with
the same tag.
.RE
.\"
.\"
.SS Target Selection
.\"
.\"
The
.C swmodify
command supports the specification of a single, local
.IR target_selection ,
using the syntax:
.P
.RS
.nf
.C "\f1[\|\fP @ /\s-1\f2directory\fP\s0\f1\|]\fP"
.fi
.RE
.PP
When operating on the primary root, no
.I target_selection
needs to be specified.
(The target
.CR /
is assumed.) 
When operating on a software depot, the
.I target_selection
specifies the path to that depot.
If the
.C -d
option is specified and no
.I target_selection
is specified, the default
.C distribution_target_directory
is assumed (see below).
.SH EXTERNAL INFLUENCES
.SS Default Options
In addition to the standard options, several SD behaviors and policy options
can be changed by editing the default values found in:
.RS
.TP 30n
.C /var/adm/sw/defaults
the system-wide default values.
.TP
.C $HOME/.swdefaults
the user-specific default values.
.RE
.P
Values must be specified in the defaults file using this syntax:
.P
.RS
.nf
.C "\f1[\|\fP\s-1\f2command_name\fP\s0.\f1\|]\fP\s-1\f2option\fP\s0=\s-2\f2value\fP\s0"
.fi
.RE
.PP
The optional
.EM "command_name"
prefix denotes one of the SD commands.  Using the prefix limits the
change in the default value to that command.  If you leave the prefix
off, the change applies to all commands.
.PP
You can also override default values from the command line with the
.C "-x"
or
.C "-X"
options:
.P
.RS
.nf
.IC "command " "-x " "option" "=" "value"
.P
.IC "command " "-X " "option_file"
.fi
.RE
.PP
The following keywords are supported by
.CR swmodify .
If a default value exists, it is listed after the
.CR = .
The commands
that this option applies to are also specified.  The policy options
that apply to
.C swmodify
are:
.RS
.TP 10
.PD 0
.CR "admin_directory=/var/adm/sw" " (for normal mode)"
.PD 0
.TP
.CR "admin_directory=/var/home/LOGNAME/sw" " (for nonprivileged mode)"
.PD
The location for SD logfiles and the default parent directory for the
installed software catalog.  The default value is
.CR "/var/adm/sw"
for normal SD operations.  When SD operates in nonprivileged mode
(that is, when the
.CR run_as_superuser
default option is set to
.CR "true" "):"
.RS
.TP 3
\(bu
The default value is forced to
.CR "/var/home/LOGNAME/sw" .
.TP
\(bu
The path element
.CR "LOGNAME"
is replaced with the name of the invoking user, which SD reads from
the system password file.
.TP
\(bu
If you set the value of this option to
.CR "HOME/"\c
.IR "path" ","
SD replaces
.CR HOME
with the invoking user's home directory (from the system password
file) and resolves
.I path
relative to that directory.  For example,
.CR HOME/my_admin
resolves to the
.CR my_admin
directory in your home directory.
.TP
\(bu
If you set the value of the
.CR installed_software_catalog
default option to a relative path, that path is resolved relative to
the value of this option.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.
.IP
See also the
.CR installed_software_catalog
and
.CR run_as_superuser
options.
.TP
.C allow_large_files=false
Determines whether modification of files with a size greater
than or equal to 2 gigabytes is allowed.  In the default state of
.CR false ,
this option tells
.CR swmodify
to not allow files with a size greater than or equal to
2 gigabytes to be modified.
.IP
When set to
.CR true ,
this option tells
.CR swmodify
to permit files with a size greater than or equal to
2 gigabytes to be modified.  If the files are in a depot,
then the depot can only be used by the December 2005 OEUR (HP-UX 11i v2)
version of SD and newer versions of SD on HP-UX 11i v1, HP-UX 11i v2,
and future releases.
.IP
This version of SD supports a large file up to
2 terabytes (2048 gigabytes)
.TP
.C compress_index=false
Determines whether SD commands create compressed INDEX and INFO
catalog files when writing to target depots or roots.  The default of
.C false
does not create compressed files.  When set to
.CR true ,
SD creates compressed and uncompressed INDEX and INFO files. The
compressed files are named
.CR INDEX.gz
and
.CR INFO.gz ,
and reside in the
same directories as the uncompressed files.
.IP
Compressed files can enhance performance on slower networks, although
they may increase disk space usage due to a larger Installed Products
Database and depot catalog.  SD controllers and target agents for
HP-UX 11.01 and higher automatically load the compressed INDEX and
INFO files from the source agent when:
.RS
.TP 3
\(bu
The source agent supports this feature.
.TP
\(bu
.CR INDEX.gz
or
.CR INFO.gz
exist on the source depot.
.TP
\(bu
.CR INDEX.gz
or
.CR INFO.gz
are not older than the corresponding uncompressed
INDEX or INFO files.
.RE
.IP
The uncompressed INDEX or INFO file is accessed by the source agent if
any problem occurs when accessing, transferring, or uncompressing the
.CR INDEX.gz
or
.CR INFO.gz
file.
.TP
.C control_files=
When adding or deleting control file objects, this option lists the
tags of those control files.  There is no supplied default.  If there
is more than one tag, they must be separated by white space and
surrounded by quotes.
.TP
.C distribution_target_directory=/var/spool/sw
Defines the default distribution directory of the target depot.  The
.I target_selection
operand overrides this default.
.TP
.C files=
When adding or deleting file objects, this option lists the pathnames
of those file objects.  There is no supplied default.  If there is
more than one pathname, they must be separated by white space.
.TP
.C installed_software_catalog=products
Defines the directory path where the Installed Products Database (IPD)
is stored.  This information describes installed software.  When set to
an absolute path, this option defines the location of the IPD.  When
this option contains a relative path, the SD controller appends the
value to the value specified by the
.CR admin_directory
option to determine the path to the IPD.  For alternate roots, this
path is resolved relative to the location of the alternate root.  This
option does not affect where software is installed, only the IPD
location.
.IP
This option permits the simultaneous installation and removal of
multiple software applications by multiple users or multiple
processes, with each application or group of applications using a
different IPD.
.IP
Caution: use a specific
.CR installed_software_catalog
to manage a
specific application.  SD does not support multiple descriptions of the
same application in multiple IPDs.
.IP
See also the
.CR admin_directory
and
.CR run_as_superuser
options, which control SD's nonprivileged mode.  (This mode is intended
only for managing applications that are specially designed and
packaged.  This mode cannot be used to manage the HP-UX operating
system or patches to it.  For a full explanation of nonprivileged SD,
see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.)
.TP
.C layout_version=1.0
Specifies the POSIX
.C layout_version
to which the SD commands conform when writing distributions and
.CD swlist
output.  Supported values are "1.0" (default) and "0.8".
.IP
SD object and attribute syntax conforms to the
.EM "layout_version 1.0"
specification of the
.CT "IEEE POSIX 1387.2 Software Administration"
standard.  SD commands still accept the keyword names associated
with the older layout version, but you should use
.C "layout_version=0.8"
only to create distributions readable by older versions of SD.
.IP
See the description of the
.C layout_version
option in
.IR sd (5)
for more information.
.\"
.TP
.C log_msgid=0
Adds numeric identification numbers at the beginning of SD logfile
messages:
.RS
.PD 0
.TP 3
.C 0
(default) No identifiers are attached to messages.
.TP
.C 1
Adds identifiers to ERROR messages only.
.TP
.C 2
Adds identifiers to ERROR and WARNING messages.
.TP
.C 3
Adds identifiers to ERROR, WARNING, and NOTE messages.
.TP
.C 4
Adds identifiers to ERROR, WARNING, NOTE, and certain other
informational messages.
.PD
.RE
.\"
.TP
.C logdetail=false
The
.C logdetail
option controls the amount of detail written to the log file.  When set
to
.CR true ,
this option adds detailed task information (such as options specified,
progress statements, and additional summary information) to the
log file.  This information is in addition to log information controlled
by the
.C loglevel
option.
.TP
.C logfile=/var/adm/sw/swmodify.log
Defines the default log file for
.CR swmodify .
.TP
.CR "loglevel=1" " (for direct invocation)
.PD 0
.TP
.CR "loglevel=0" " (for invocation by a control script)
.PD
Controls the log level for the events logged to the
.C swmodify
logfile, the target agent logfile, and the source agent logfile.  This
information is in addition to the detail controlled by the
.C logdetail
option.  See
.C logdetail
for more information.
.IP
A value of:
.PD 0
.RS
.TP 4
.C 0
provides no information to the log files.
.TP
.C 1
enables verbose logging to the log files.
.TP
.C 2
enables very verbose logging to the log files.
.PD
.RE
.IP
To enable logging by
.C swmodify
commands invoked by control files, add the following line to the system
defaults file:
.IP
.C "swmodify.loglevel=1"
.TP
.C lookupcache_timeout_minutes=10
Controls the time in minutes to cache and re-use the results of hostname
or IP address resolution lookups.  A value of 0 disables the facility to
cache and re-use lookup results.  The maximum value allowed is 10080 minutes,
which is one week.
.IP
A value of:
.RS
.PD 0
.TP 10
.C 0
disables the lookup caching mechanism.
.TP
.C 10080
is the maximum value allowed.
.RE
.PD
.TP
.C patch_commit=false
Commits a patch by removing files saved for patch rollback.  When set to
.CR true ,
you cannot roll back (remove) a patch unless you remove the associated
base software that the patch modified.
.TP
.C run_as_superuser=true
This option controls SD's nonprivileged mode.  This option is ignored
(treated as true) when the invoking user is super-user.
.IP
When set to the default value of true, SD operations are performed
normally, with permissions for operations either granted to a local
super-user or set by SD ACLs.  (See
.IR swacl (1M)
for details on ACLs.)
.IP
When set to false and the invoking user is local and is
.I not
super-user, nonprivileged mode is invoked:
.RS
.TP 3
\(bu
Permissions for operations are based on the user's file system
permissions.
.TP
\(bu
SD ACLs are ignored.
.TP
\(bu
Files created by SD have the uid and gid of the invoking user, and the
mode of created files is set according to the invoking user's umask.
.RE
.IP
SD's nonprivileged mode is intended only for managing applications
that are specially designed and packaged.  This mode cannot be used to
manage the HP-UX operating system or patches to it.  For a full
explanation of nonprivileged SD, see the
.CT "Software Distributor Administration Guide" ","
available at the
.CR "http://docs.hp.com"
web site.
.IP
See also the
.CR admin_directory
and
.CR installed_software_catalog
options.
.TP
.C software=
Defines the default
.IR software_selections .
There is no supplied default.  If there is more than one software
selection, they must be separated by spaces.  Software is usually
specified in a software input file, as operands on the command line,
or in the GUI.
.TP
.C source_file=
Defines the default location of the source product specification file
(PSF).  The
.CR host:path
syntax is not allowed, only a valid
.CR path
can be specified.
The
.C -s
option overrides this value.
.TP
.C targets=
Defines the default
.IR target_selections .
There is no supplied default (see
.C select_local
above).  If there is more than one target selection, they must be
separated by spaces.  Targets are usually specified in a target input
file, as operands on the command line, or in the GUI.
.TP
.C verbose=1
Controls the verbosity of a non-interactive command's output:
.RS
.PD 0
.TP 4
.C 0
disables output to stdout.  (Error and warning messages
are always written to stderr).
.TP
.C 1
enables verbose messaging to stdout.
.TP
.C 2
for
.CR swmodify ,
enables very verbose messaging to stdout.
.PD
.RE
.RE
.SS Session File
Each invocation of the
.C swmodify
command defines a modify session.  The invocation options, source
information, software selections, and target hosts are saved before
the installation or copy task actually commences.  This lets you
re-execute the command even if the session ends before proper
completion.
.PP
Each session is automatically saved to the file
.CR $HOME/.sw/sessions/swmodify.last .
This file is overwritten by each invocation of
.CR swmodify .
.PP
You can also save session information to a specific file by executing
.C swmodify
with the
.C -C
.I session_file
option.
.PP
A session file uses the same syntax as the defaults files.
You can specify an absolute path for the session file.  If you do
not specify a directory, the default location for a session file is
.CR $HOME/.sw/sessions/ .
.PP
To re-execute a session file, specify the session file as the argument for the
.C -S
.I session_file
option of
.CR swmodify .
See the
.IR swpackage (4)
by typing
.C "man 4 swpackage"
for PSF syntax.
.PP
Note that when you re-execute a session file, the values in the session
file take precedence over values in the system defaults file.
Likewise, any command line options or parameters that you specify when
you invoke
.CR swmodify
take precedence over the values in the session file.
.SS Environment Variables
The environment variable that affects
.C swmodify
is:
.PP
.RS
.TP 13
.EV LANG
Determines the language in which messages are displayed.
If
.EV LANG
is not specified or is set to the empty string, a
default value of
.C C
is used.
See the
.IR lang (5)
man page by typing
.C man 5 lang
for more information.
.IP
NOTE: The language in which the SD agent and daemon log messages
are displayed is set by the system configuration variable script,
.CR /etc/rc.config.d/LANG .
For example,
.CR /etc/rc.config.d/LANG ,
must be set to
.CR LANG=ja_JP.SJIS
or
.CR LANG=ja_JP.eucJP
to make the agent and daemon log messages display in Japanese.
.TP
.EV LC_ALL
Determines the locale to be used to override any values for locale
categories specified by the settings of
.EV LANG
or any environment variables beginning with
.CR LC_ .
.TP
.EV LC_CTYPE
Determines the interpretation of sequences of bytes of text data as
characters (for example, single versus multibyte characters in values for
vendor-defined attributes).
.TP
.EV LC_MESSAGES
Determines the language in which messages should be written.
.TP
.EV LC_TIME
Determines the format of dates
.RI ( create_date
and
.IR mod_date )
when displayed by
.CR swlist .
Used by all utilities when displaying dates and times in
.CR stdout ,
.CR stderr ,
and
.CR logging .
.TP
.EV TZ
Determines the time zone for use when displaying dates and times.
.RE
.SS Signals
The
.C swmodify
command ignores SIGHUP, SIGTERM, SIGUSR1, and SIGUSR2.
The
.C swmodify
command catches SIGINT and SIGQUIT.   If these signals are received,
.C swmodify
prints a message and then exits.  During the actual database modifications,
.C swmodify
blocks these signals (to prevent any data base corruption).  All other
signals result in their default action being performed.
.SH RETURN VALUES
The
.C swmodify
command returns:
.RS
.TP 4
.C 0
The add, modify, or delete operation(s) were successfully performed on
the given
.IR software_selections .
.PD 0
.TP
.C 1
An error occurred during the session (for example, bad syntax in the PSF,
invalid
.IR software_selection ,
etc.)  Review stderr or the logfile for details.
.PD
.RE
.SH DIAGNOSTICS
The
.C swmodify
command writes to stdout, stderr, and to specific logfiles.
.P
.SS Standard Output
In verbose mode, the
.C swmodify
command writes messages for significant events.  These include:
.RS
.PD 0
.TP 3
\(bu
a begin and end session message,
.TP
\(bu
selection, analysis, and execution task messages.
.PD
.RE
.SS Standard Error
The
.C swmodify
command also writes messages for all WARNING and ERROR conditions to
stderr.
.SS Logfile
The
.C swmodify
command logs events to the command logfile and to the
.C swmodify
logfile associated with each
.IR target_selection .
.TP
Command Log
The
.C swmodify
command logs all messages to the the logfile
.CR /var/adm/sw/swmodify.log .
(The user can specify a different logfile by
modifying the
.C logfile
option.)
.TP
Target Log
When modifying installed software,
.C swmodify
logs messages to the file
.CR var/adm/sw/swagent.log
beneath the root directory (for example,
.C /
or an alternate root directory).
When modifying available software (within a depot),
.C swmodify
logs messages to the file
.CR swagent.log
beneath the depot directory (for example,
.CR /var/spool/sw ).
.SH EXAMPLES
Add additional files to an existing fileset:
.IP
.C "swmodify -xfiles='/tmp/a /tmp/b /tmp/c'  PRODUCT.FILESET"
.PP
Replace the definitions of existing files in an existing
fileset (for example,
to update current values for the files' attributes):
.RS
.nf
.PP
.C "chown root /tmp/a /tmp/b"
.C "swmodify -x files='/tmp/a /tmp/b'  PRODUCT.FILESET"
.fi
.RE
.PP
Delete control files from a fileset in an existing depot:
.RS
.nf
.PP
.C "swmodify -d -u -x control_files='checkinstall subscript' \e"
.C "\0\0\0\0\0\0\0\0\0PRODUCT.FILESET @ \0/var/spool/sw"
.fi
.RE
.PP
Create a new fileset definition where the description is contained in
the PSF file
.CR new_fileset_definition :
.\" (if the PSF contains file definitions, then add those files to the new fileset):
.IP
.C "swmodify -s new_fileset_definition"
.PP
Delete an obsolete fileset definition:
.IP
.C "swmodify -u PRODUCT.FILESET"
.PP
Commit a patch (remove files saved for patch rollback):
.IP
.C "swmodify -x patch_commit=true PATCH"
.PP
Create some new bundle definitions for products in an existing depot:
.IP
.C "swmodify -d -s new_bundle_definitions  \e*\0  @ \0/mfg/master_depot"
.\"
.PP
Modify the values of some fileset's attributes:
.IP
.C "swmodify -a state=installed  PRODUCT.FILESET"
.PP
Modify the attributes of a depot:
.PP
.RS
.nf
.C "swmodify -a title='Manufacturing's master depot' \e"
.C "\0\0\0\0\0\0\0\0\0-a description=</tmp/mfg.description @ \0/mfg/master_depot"
.fi
.RE
.SH WARNINGS
If the
.I target_selection
is a software depot and
you delete file definitions from the given
.IR software_selections ,
the files' contents are not deleted from the depot.
.\".SH LIMITATIONS
.SH FILES
.TP
.C $HOME/.swdefaults
Contains the user-specific default values for some or all SD options.
.TP
.C $HOME/.sw/sessions/
Contains session files automatically saved by the SD commands, or
explicitly saved by the user.
.TP
.C /usr/lib/sw/sys.defaults
Contains the master list of current SD options (with their default values).
.TP
.C /var/adm/sw/
The directory which contains all of the configurable (and
non-configurable) data for SD.  This directory is also the default
location of logfiles.
.TP
.C /var/adm/sw/defaults
Contains the active system-wide default values for some or all SD options.
.TP
.C /var/adm/sw/products/
The Installed Products Database (IPD), a catalog of all products
installed on a system.
.TP
.C /var/spool/sw/
The default location of a target software depot.
.SH AUTHOR
.C swmodify
was developed by the Hewlett-Packard Company.
.SH SEE ALSO
install-sd(1M),
swacl(1M),
swagentd(1M),
swask(1M),
swconfig(1M),
swcopy(1M),
swinstall(1M),
swjob(1M),
swlist(1M),
swpackage(1M),
swreg(1M),
swremove(1M),
swverify(1M),
sd(4),
swpackage(4),
sd(5).
.P
.CT "Software Distributor Administration Guide" ","
available at
.CR "http://docs.hp.com" .
.P
SD customer web site at
.\" JAGaf61810: Updated the URL for the SD customer web site.
.\"   NOTE TO LOCALIZERS: Currently, only the English version of this
.\"   web site exists.
.CR "http://docs.hp.com/en/SD/" .
.\"
.\" index@\f4swmodify\f1 \- modify software products in a target root or depot@@@\f3swmodify(1M)\f1
.\" index@\f1depot; modify software products in a target root or@@@\f3swmodify(1M)\f1
.\" index@\f1target root; modify software products in depot or@@@\f3swmodify(1M)\f1
.\" index@\f1modify software products in a target root or depot@@@\f3swmodify(1M)\f1
.\" index@\f1software products; modify in target root or depot@@@\f3swmodify(1M)\f1
.\" toc@\f3swmodify(1M)\f1:\0\0\f4swmodify\f1@@@modify software products in a target root or depot
