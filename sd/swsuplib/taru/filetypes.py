# sd.swsuplib.taru.filetypes.py - deal with POSIX annoyances

# Copyright (C) 1991 Free Software Foundation, Inc.
# Copyright (C) Paul Weber, Conversion to Python
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import stat

NOTDUMPEDTYPE = '\t'


class Filetypes:
    if not hasattr(stat, 'S_IFREG'):
        stat.S_IFREG = 0o100000
    if not hasattr(stat, 'S_IFDIR'):
        stat.S_IFDIR = 0o40000
    if not hasattr(stat, 'S_ISVTX'):
        stat.S_ISVTX = 0o1000
    if not hasattr(stat, 'S_ISDOOR'):
        stat.S_ISDOOR = lambda m: 0

    if not hasattr(stat, 'S_ISBLK') and hasattr(stat, 'S_IFBLK'):
        stat.S_ISBLK = lambda m: ((m & stat.S_IFMT) == stat.S_IFBLK)
    if not hasattr(stat, 'S_ISCHR') and hasattr(stat, 'S_IFCHR'):
        stat.S_ISCHR = lambda m: ((m & stat.S_IFMT) == stat.S_IFCHR)
    if not hasattr(stat, 'S_ISDIR') and hasattr(stat, 'S_IFDIR'):
        stat.S_ISDIR = lambda m: ((m & stat.S_IFMT) == stat.S_IFDIR)
    if not hasattr(stat, 'S_ISREG') and hasattr(stat, 'S_IFREG'):
        stat.S_ISREG = lambda m: ((m & stat.S_IFMT) == stat.S_IFREG)
    if not hasattr(stat, 'S_ISFIFO') and hasattr(stat, 'S_IFIFO'):
        stat.S_ISFIFO = lambda m: ((m & stat.S_IFMT) == stat.S_IFIFO)
    if not hasattr(stat, 'S_ISDOOR') and hasattr(stat, 'S_IFDOOR'):
        stat.S_ISDOOR = lambda m: ((m & stat.S_IFMT) == stat.S_IFDOOR)
    if not hasattr(stat, 'S_ISLNK') and hasattr(stat, 'S_IFLNK'):
        stat.S_ISLNK = lambda m: ((m & stat.S_IFMT) == stat.S_IFLNK)
    if not hasattr(stat, 'S_ISSOCK') and hasattr(stat, 'S_IFSOCK'):
        stat.S_ISSOCK = lambda m: ((m & stat.S_IFMT) == stat.S_IFSOCK)
    if not hasattr(stat, 'S_ISNWK') and hasattr(stat, 'S_IFNWK'):
        stat.S_ISNWK = lambda m: ((m & stat.S_IFMT) == stat.S_IFNWK)

    if hasattr(stat, 'S_IFMT'):
        CP_IFMT = 0o170000
    if hasattr(stat, 'S_ISBLK'):
        CP_IFBLK = 0o060000
    if hasattr(stat, 'S_ISCHR'):
        CP_IFCHR = 0o020000
    if hasattr(stat, 'S_ISDIR'):
        CP_IFDIR = 0o040000
    if hasattr(stat, 'S_ISREG'):
        CP_IFREG = 0o100000
    if hasattr(stat, 'S_ISFIFO'):
        CP_IFIFO = 0o010000
    if hasattr(stat, 'S_ISLNK'):
        CP_IFLNK = 0o120000
    if hasattr(stat, 'S_ISDOOR'):
        CP_IFDOOR = 0o150000
    if hasattr(stat, 'S_ISSOCK'):
        CP_IFSOCK = 0o140000
    if hasattr(stat, 'S_ISNWK'):
        CP_IFNWK = 0o110000

    if not hasattr(stat, 'S_ISLNK'):
        stat.lstat = stat


ft = Filetypes()
