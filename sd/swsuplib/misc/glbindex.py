# glbindex.py  --  Routines for searching the global index file.
#
#   Copyright (C) 2005 James H. Lowe, Jr.  <jhlowe@acm.org>
#   Copyright (C) 2024 Paul Weber conversion to Python
#   All Rights Reserved.
#  
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
# 

from sd.swsuplib.misc.sw import swc
from sd.swsuplib.misc.swlib import swlibc, fr
from sd.swsuplib.misc.swpath import swpath_ex_print
from sd.swsuplib.misc.swheader import (swheader_get_single_attribute_value, swheader_get_next_object, 
                                       swheaderline_get_keyword, swheaderline_get_level, swheaderline_get_type,
                                       swheader_get_current_offset, swheader_restore_state, swheader_store_state,
                                       swheader_set_current_offset)
from sd.swsuplib.strob import strob_open, strob_close
from sd.swsuplib.misc.strar import strar_close
from sd.swsuplib.swparse.swparse import swpc

UCHAR_MAX = 255

def determine_control_directory(swheader):
    value = swheader_get_single_attribute_value(swheader, swlibc.SW_A_control_directory)
    if not value:
        value = swheader_get_single_attribute_value(swheader, swlibc.SW_A_tag)
    return value

def search_object_for_directory(i_next_line, swheader, key_dir, object_name, level):
    control_directory = determine_control_directory(swheader)
    next_line = i_next_line
    keyword = swheaderline_get_keyword(next_line)
    while next_line:
        if keyword == object_name:
            if control_directory:
                if key_dir == control_directory:
                    return next_line
            else:
                break
        next_line = swheader_get_next_object(swheader, level, UCHAR_MAX)
    return None

def search_for_fileset(i_next_line, swheader, key_dir):
    level = swheaderline_get_level(i_next_line)
    type_ = swheaderline_get_type(i_next_line)
    if type_ == swpc.SWPARSE_MD_TYPE_OBJ:
        level += 1
    ret = search_object_for_directory(i_next_line, swheader, key_dir, swlibc.SW_A_fileset, level)
    return ret

def search_for_product(i_next_line, swheader, key_dir):
    # level = swheaderline_get_level(i_next_line)
    ret = search_object_for_directory(i_next_line, swheader, key_dir, swlibc.SW_A_product, UCHAR_MAX)
    return ret

def determine_object(object_keyword, control_dir_list, swpath_ex):
    if (swpath_ex.is_minimal_layout == 0 and len(swpath_ex.product_control_dir) and not len(swpath_ex.fileset_control_dir)) or (swpath_ex.is_minimal_layout == 1 and len(swpath_ex.pfiles) and not len(swpath_ex.fileset_control_dir)):
        control_dir_list.append(swlibc.SW_A_product)
        control_dir_list.append(swpath_ex.product_control_dir)
        object_keyword = swlibc.SW_A_product
    elif (swpath_ex.is_minimal_layout == 0 and len(swpath_ex.product_control_dir) and len(swpath_ex.fileset_control_dir)) or (swpath_ex.is_minimal_layout == 1 and not len(swpath_ex.product_control_dir) and not len(swpath_ex.fileset_control_dir) and swpath_ex.basename == swc.SW_A_INFO):
        control_dir_list.append(swlibc.SW_A_product)
        control_dir_list.append(swpath_ex.product_control_dir)
        control_dir_list.append(swlibc.SW_A_fileset)
        control_dir_list.append(swpath_ex.fileset_control_dir)
        object_keyword = swlibc.SW_A_fileset
    else:
        tmp = strob_open(10)
        print("swinstall: Uh-Oh WARNING IN " + fr.__FILE__ + " " + str(fr.__LINE__))
        print(swpath_ex_print(swpath_ex, tmp, "glbindex.c "))
        strob_close(tmp)
        object_keyword = swc.SW_A_distribution
    return object_keyword

def glbindex_find_by_swpath_ex(global_index, swpath_ex):
    swheader = global_index
    swheader_store_state(swheader, None)
    swheader_set_current_offset(swheader, 0)
    control_dir_list = []
    object_keyword = strob_open(20)
    determine_object(object_keyword, control_dir_list, swpath_ex)
    next_line = swheader_get_next_object(swheader, UCHAR_MAX, UCHAR_MAX)
    swlibc.SWLIB_ASSERT(next_line is not None)
    if len(control_dir_list) > 3:
        next_line = search_for_product(next_line, swheader, control_dir_list[1])
        swlibc.SWLIB_ASSERT(next_line is not None)
        next_line = search_for_fileset(next_line, swheader, control_dir_list[3])
        swlibc.SWLIB_ASSERT(next_line is not None)
    else:
        next_line = search_for_product(next_line, swheader, control_dir_list[1])
        swlibc.SWLIB_ASSERT(next_line is not None)
    ret = swheader_get_current_offset(swheader)
    strar_close(control_dir_list)
    strob_close(object_keyword)
    swheader_restore_state(swheader, None)
    return ret


