# tar.py, tarhdr- Extended tar header from POSIX.1.
#   Copyright (C) 1992 Free Software Foundation, Inc.
#   This file is part of the GNU C Library.
#   Written by David J. MacKenzie.
#   2013 Converted to Python, Paul Weber

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


class TarConstants:
    TARNAMESIZE = 100
    OLDGNU_TARNAMESIZE = 99
    TARLINKNAMESIZE = 100
    TARPREFIXSIZE = 155
    TARRECORDSIZE = 512

    REGTYPE = '0'     # Regular file (preferred code).
    AREGTYPE = '\0'    # Regular file (alternate code).
    LNKTYPE = '1'      # Hard link.
    SYMTYPE = '2'     # Symbolic link (hard if not supported).
    CHRTYPE = '3'     # Character special.
    BLKTYPE = '4'     # Block special.
    DIRTYPE = '5'     # Directory.
    FIFOTYPE = '6'     # Named pipe.
    CONTTYPE = '7'     # Contiguous file.


tarc = TarConstants()


class TarHeader:
    def __init__(self):
        self.name = [''] * tarc.TARNAMESIZE
        self.mode = [''] * 8
        self.uid = [''] * 8
        self.gid = [''] * 8
        self.size = [''] * 12
        self.mtime = [''] * 12
        self.chksum = [''] * 8
        self.typeflag = ''
        self.linkname = [''] * tarc.TARLINKNAMESIZE
        self.magic = [''] * 6
        self.version = [''] * 2
        self.uname = [''] * 32
        self.gname = [''] * 32
        self.devmajor = [''] * 8
        self.devminor = [''] * 8
        self.prefix = [''] * tarc.TARPREFIXSIZE

tar_header = TarHeader

class TarRecord:
    def __init__(self):
        self.header = tar_header()
        self.buffer = [''] * tarc.TARRECORDSIZE

tar_record = TarRecord