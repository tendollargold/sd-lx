#! /sbin/sh
########
#  Product: hpuxwsWEBMIN
#  Fileset: HPDOCS
#  SD script: configure
########
#
#  (c) Copyright Hewlett-Packard Company 2000
#
########
#

SUCCESS=0; FAILURE=1; WARNING=2; EXCLUDE=3
HPDOCS=/opt/hpws/hp_docs
HPUXDOCS=/opt/hpws/webmin/hp-ux

DT="`date +\"%Y%m%d%H%M%S\"`"

if [ -n "$SW_DEFERRED_KERNBLD" ]; then
    echo "WARNING: HP Apache-based web server  will not function from"
    echo "         an NFS Diskless client, although swlist (1M) will"
    echo "         report the product as installed."
    exit $WARNING
fi

UTILS="/usr/lbin/sw/control_utils"
if [[ ! -f $UTILS ]]; then
    echo "ERROR:   Cannot find $UTILS"
    exit $FAILURE
fi
. $UTILS


function get_host
# Prints a string of the form <host name> <IP address>
# The host name and IP address are retrieved from the
# output of nslookup, or default to the local host name
# and loopback address.
{
    typeset host=$(uname -n) domain lookup name addr local="127.0.0.1"
    if [[ -f /etc/resolv.conf ]]; then
        domain=$(grep domain /etc/resolv.conf | awk '{print $2}')
    fi

    if [[ -z $domain ]]; then
        print "$host $local"
        return 0
    fi

    lookup=$(nslookup $host 2> /dev/null)
    if [[ $? -ne 0 ]]; then 
	print "$host $local"
        return 0
    fi

    lookup=$(print "$lookup" | awk 'BEGIN {name= ""; addr= ""}
			   /Name:/ {name= $2}
			   /Address:/ {addr= $2}
			   END { print "name=" name; print "addr=" addr}')
    eval $lookup

    if [[ ! -z $domain ]]; then
        if [[ "x`echo $name | grep $domain`" = "x" ]]; then
            name="$name.$domain"
        fi
        if [[ "x`echo $host | grep $domain`" = "x" ]]; then
            host="$host.$domain"
        fi
    fi

    if [[ -z $name ]]; then       # shouldn't happen
	print "$host $local"
        return 0
    fi

    [[ -z $addr ]] && addr=$local # shouldn't happen

    print "$name $addr"
    return 0
}

function bail
{
    msg error "$*"
    exit $FAILURE
}

function get_docname
{
    typeset uname=$(uname -r)
    case $uname in
	B.11.00)
		print ""
		;;
	B.11.11)
		print "11iRelNotes"
		;;
	B.11.20)
		print "11iV1_5RelNotes"
		;;
	B.11.22)
		print "11iV1_6RelNotes"
		;;
        B.11.23)
                print "11iV2RelNotes"
                ;;
	*)
		print ""
		;;
    esac
}

#### $1 must be the Server name
#### $2 must be the Document name
function welcome_conf
{
    rv=$SUCCESS

    cp -f $HPDOCS/welcome.html $HPDOCS/welcome.html.$DT
    sed -e "s/SERVERNAME_PLACEHOLDER/$1/" \
        $HPDOCS/welcome.html.$DT > $HPDOCS/welcome.html

    [[ $? -ne 0 ]] && bail hostname modification in $HPDOCS/welcome.html failed
    rm -f $HPDOCS/welcome.html.$DT

    PDF_FILE=0
    HTML_FILE=0

    if [ "x$2" != "x" ]; then
	if [ -e /usr/share/doc/$2.pdf ]; then
	    PDF_FILE=1
        fi
        if [ -e /usr/share/doc/$2.html ]; then
            HTML_FILE=1
        fi
    fi

    if [[ $PDF_FILE -eq 0 && $HTML_FILE -eq 0 ]]; then
        cp -f $HPDOCS/welcome.html $HPDOCS/welcome.html.$DT
	sed -e "/hp-ux release notes/d" \
	    -e "/HPUX_RELEASE_NOTES\./d" \
	    $HPDOCS/welcome.html.$DT > $HPDOCS/welcome.html
        [[ $? -ne 0 ]] && bail removal of release notes from $HPDOCS/welcome.html failed
    else 
    	cp -f $HPDOCS/welcome.html $HPDOCS/welcome.html.$DT
	if [ $PDF_FILE -eq 1 ]; then
	    # Uncomment the pdf line
	    sed -e "s/<!-- \(.*HPUX_RELEASE_NOTES\.pdf.*pdf.*\) -->/\1/" \
	        $HPDOCS/welcome.html.$DT > $HPDOCS/welcome.html
            [[ $? -ne 0 ]] && bail uncommenting of the pdf line in $HPDOCS/welcome.html failed
	fi

    	cp -f $HPDOCS/welcome.html $HPDOCS/welcome.html.$DT
	if [ $HTML_FILE -eq 1 ]; then
	    # Uncomment the html line
	    sed -e "s/<!-- \(.*HPUX_RELEASE_NOTES\.html.*html.*\) -->/\1/" \
	        $HPDOCS/welcome.html.$DT > $HPDOCS/welcome.html
            [[ $? -ne 0 ]] && bail uncommenting of the html line in $HPDOCS/welcome.html failed

	    # Now change the name from HPUX_RELEASE_NOTES to whatever applicable
    	    cp -f $HPDOCS/welcome.html $HPDOCS/welcome.html.$DT
            sed -e "s/HPUX_RELEASE_NOTES/$2/" \
	        $HPDOCS/welcome.html.$DT > $HPDOCS/welcome.html
            [[ $? -ne 0 ]] && bail setting up release notes in $HPDOCS/welcome.html failed

            # Now create a symbolic link
	    if [ ! -h $HPUXDOCS ]; then
	        ln -s /usr/share/doc $HPUXDOCS
	    fi
	fi
    fi
    rm -f $HPDOCS/welcome.html.$DT

    IPD_addfile $HPDOCS/welcome.html
    return $rv
}

# configure welcome.html
DNS_NAME=`echo $(get_host) | awk '{print $1}'`
DOC_NAME=`echo $(get_docname)`

welcome_conf $DNS_NAME $DOC_NAME

if [[ $? -ne 0 ]]; then
   print "ERROR:   Couldn't configure welcome.html."
   exit $FAILURE
fi

exit $SUCCESS
