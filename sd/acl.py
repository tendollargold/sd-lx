#!/usr/bin/env python3

# ACL Files

# Define, list, create, ACL's for access to hosts, depots, products, etc. 
# with SD.  See the acl.README for documentation from HP/HPE, SD-UX Security.

import dbm

# testing purposes only, this should be initialized from BaseConf
from sd.const import swg

acldir=swg.SW_ACL_DIR

SOC_DFLT_ACL = {
  # Used to initialize the ACL protecting new depots and roots created
  # on the host.

  # these entries won't work as a dictionary as they are.
  # default_realm=Host_FQDN
  # user_obj:rwict
  # any_other:r----

  'user_obj': 'rwict',
  'any_other': 'r----'
  }


PROD_DFLT_ACL = {
  # The ACL that is used to initialize the product ACL template on
  # depots that are created on the host.

  # these entries won't work as a dictionary as they are.
  # default_realm=Host_FQDN
  # user_obj:rwict
  # any_other:r----
  # 
  'user_obj': 'rwict',
  'any_other': 'r----'
  }
DFLT_ACL = {
  # default_realm=Host_FQDN
  'any_other': 'r----'
  }


ACL_OWNER = {
  # these entries won't work as a dictionary as they are.
  # user:root:0, has too many :

  # default_realm=Host_FQDN
  # num_entries:2
  # user:root:0
  # group:sys:0

  # default_realm=Host_FQDN
  'num_entries': '2',
  'user': 'root:0',
  'group': 'sys:0'
  }



# using the open function
# to create a new database named
# 'mydb' in 'n' mode, object
# returned in db variable.

# 'n' is always create new database
#dbm.whichdb(acldir+'secrets.pag','n')
print(swg.SW_ACL_DIR)
print("acldir: "+acldir)
print(acldir)

print(acldir+'/secrets.pag')
dbtype=(dbm.whichdb(acldir+'/secrets.pag'))
print("DBTYPE")
print(dbtype)

#print(dbm.whichdb(acldir+'/secrets.pag'))
#print(dbm.whichdb('/var/adm/sw/security/secrets.pag'))

# raise SystemExit(0)

#db = dbm.open('mydb','n')

# inserting the new key and
# values in the database.
#db['name'] = 'GeeksforGeeks'
#db['phone'] = '8888'
#db['Short name'] = 'GfG'
#db['Date'] = '01/01/2000'

# getting and printing
# the value through get method.
#print(db.get('name'))
print()

# printing the values of
# database through values()
# method (iterator).
#for value in db.values():
#    print(value)
print()

# printing the values through
# key iterator.
#for key in db.keys():
#    print(db.get(key))
#print()

# poping out the key, value
# pair corresponding to
# 'phone' key.
#db.pop('phone')

# printing the key, value
# pairs present in database.
#for key, value in db.items():
#    print(key, value)

# clearing all the key values
# in database.
#db.clear()

# Below loop will print nothing
# as database is cleared above.
#for key, value in db.items():
#    print(key, value)

# closing the database.
#db.close()
