# swpath.py: parse a posix package pathname.
#
#  Copyright (C) 1998  James H. Lowe, Jr.  <jhlowe@acm.org>
#  Copyright (C) 2023 Paul Weber convert to python
#  This file may be copied under the terms of the GNU GPL.
#

import sys
from sd.const import SwGlobals

from sd.swsuplib.misc.cstrings import strncmp, strtok, strstr, strchr, strcmp
from sd.swsuplib.misc.sw import swc
from sd.swsuplib.strob import (strob_open, strob_strstr, strob_str, strob_strcat,
                               strob_strlen, strob_strncpy, strob_strcpy, strob_set_length, strob_sprintf)
from sd.swsuplib.misc.strar import strar_add
from sd.swsuplib.misc.swlib import swlib_squash_trailing_slash, swlib_squash_leading_dot_slash, swlib_squash_leading_slash
G = SwGlobals

dbuf = strob_open(100)

SWPATH_PARSE_CA = 22
SWPATH_CTYPE_DIR = -1  # Leading dir
SWPATH_CTYPE_CAT = 1   # catalog section
SWPATH_CTYPE_STORE = 0 # storage section
SWPATH_DEGEN_N = 4

class SWPATH:
    def __init__(self):
        self.swpath_is_catalog_ = 0
        self.error = 0
        self.control_path_nominal_depth_ = 0
        self.max_rel_n_ = 0
        self.product_control_dir_ = None
        self.fileset_control_dir_ = None
        self.pfiles_ = None
        self.dfiles_ = None
        self.open_pathname_ = None
        self.swpackage_pathname_ = None
        self.pathname_ = None
        self.buffer_ = None
        self.basename_ = None
        self.all_filesets_ = None
        self.pfiles_default = None
        self.dfiles_default = None
        self.prepath_ = None
        self.p_pfiles_ = None
        self.p_dfiles_ = None
        self.swpath_p_prepath_ = None
        self.swpath_p_dfiles_ = None
        self.swpath_p_pfiles_ = None
        self.swpath_e_dfiles_ = None
        self.swpath_e_pfiles_ = None
        self.swpath_p_fileset_dir_ = None
        self.swpath_p_product_dir_ = None
        self.pathlist_ = None
        self.is_minimal_layout = 0
        self.dfiles_guard = 0
        self.catalog_guard = 0

class SWPATH_EX:
    def __init__(self):
        self.is_catalog = 0
        self.is_minimal_layout = 0
        self.ctl_depth = 0
        self.pkgpathname = None
        self.prepath = None
        self.dfiles = None
        self.pfiles = None
        self.product_control_dir = None
        self.fileset_control_dir = None
        self.pathname = None
        self.basename = None

def check_error(swpath, ret):
    if swpath.dfiles_guard > 1 or swpath.catalog_guard > 1 or swpath.error:
        return -1
    else:
        return ret

def set_control_depth(swpath, nd):
    if swpath.swpath_is_catalog_ == 0:
        return
    if swpath.control_path_nominal_depth_ < nd:
        swpath.control_path_nominal_depth_ = nd
def swpath_parse_write_debug(buf, relative_n, ca):
    buf = f"rel_n = {relative_n}: ca[0]=[{ca[0]}] ca[1]=[{ca[1]}] ca[2]=[{ca[2]}] ca[3]=[{ca[3]}] ca[4]=[{ca[4]}]"
    return buf

def set_p_pfiles(swpath, name):
    if not swpath.swpath_p_pfiles_ and name:
        swpath.p_pfiles_ = name
        swpath.swpath_p_pfiles_ = swpath.p_pfiles_

def set_p_dfiles(swpath, name):
    if not swpath.swpath_p_dfiles_:
        swpath.p_dfiles_ = name
        swpath.swpath_p_dfiles_ = swpath.p_dfiles_
    else:
        pass

def get_p_dfiles(swpath):
    return swpath.swpath_p_dfiles_

def get_p_pfiles(swpath):
    return swpath.swpath_p_pfiles_

def set_p_fileset_dir(swpath, name):
    if not name:
        swpath_set_fileset_control_dir(swpath, "")
        swpath.swpath_p_fileset_dir_ = None
    else:
        swpath_set_fileset_control_dir(swpath, name)
        swpath.swpath_p_fileset_dir_ = swpath.fileset_control_dir_

def swpath__set__basename(swpath, pathname):
    str1 = pathname.rfind('/')
    if not str1:
        swpath.basename_ = pathname
    else:
        swpath.basename_ = pathname[str1 + 1:]

def swpath__slashclean(swpath, madelist):
    while '\n' in madelist:
        madelist = madelist.replace('\n', '')
    if len(madelist) >= 3:
        if madelist[:2] == "./":
            madelist = madelist[2:]
    if madelist[0] == '/' and len(madelist) > 1:
        madelist = madelist[1:]
    if len(madelist) > 1 and madelist[-1] == '/':
        madelist = madelist[:-1]
    return madelist

def swpath__form_path1(swpath, buf, name):
    if len(swpath.product_control_dir_) and len(buf):
        buf += "/"
    buf += swpath.product_control_dir_
    if len(swpath.fileset_control_dir_):
        buf += "/"
    buf += swpath.fileset_control_dir_
    if len(swpath.dfiles_):
        buf += "/"
    buf += swpath.dfiles_
    if len(swpath.pfiles_):
        buf += "/"
    buf += swpath.pfiles_
    if not name:
        if len(swpath.pathname_):
            buf += "/"
        buf += swpath.pathname_
    else:
        buf += name
    return buf

def swpath__unsetup1(swpath):
    swpath.pfiles_default = None
    swpath.dfiles_default = None
    swpath.open_pathname_ = None
    swpath.product_control_dir_ = None
    swpath.fileset_control_dir_ = None
    swpath.pfiles_ = None
    swpath.dfiles_ = None
    swpath.swpackage_pathname_ = None
    swpath.pathname_ = None
    swpath.prepath_ = None
    swpath.buffer_ = None
    swpath.basename_ = None
    swpath.all_filesets_ = None
    swpath.pathlist_ = None
    swpath.is_minimal_layout = 0
    swpath.dfiles_guard = 0
    swpath.catalog_guard = 0

def swpath__setup1(swpath):
    swpath.error = 0
    swpath.open_pathname_ = ""
    swpath.product_control_dir_ = ""
    swpath.fileset_control_dir_ = ""
    swpath.pfiles_ = ""
    swpath.dfiles_ = ""
    swpath.swpackage_pathname_ = ""
    swpath.pathname_ = ""
    swpath.prepath_ = ""
    swpath.buffer_ = ""
    swpath.basename_ = ""
    swpath.p_pfiles_ = ""
    swpath.p_dfiles_ = ""
    swpath.all_filesets_ = ""
    swpath.pathlist_ = []
    swpath.max_rel_n_ = 0
    swpath.pfiles_default = "pfiles"
    swpath.dfiles_default = "dfiles"
    swpath.swpath_is_catalog_ = 0
    swpath.control_path_nominal_depth_ = 0
    swpath.swpath_p_prepath_ = None
    swpath.swpath_p_dfiles_ = None
    swpath.swpath_e_dfiles_ = None
    swpath.swpath_p_fileset_dir_ = None
    swpath.swpath_p_product_dir_ = None
    swpath.swpath_p_pfiles_ = None
    swpath.swpath_e_pfiles_ = None
    swpath.is_minimal_layout = 0
    swpath.dfiles_guard = 0
    swpath.catalog_guard = 0

def set_v_dfiles(swpath, s):
    if len(swpath.dfiles_) == 0 and len(s):
        swpath.dfiles_guard += 1
    swpath.dfiles_ = s

def set_v_pfiles(swpath, s):
    swpath.pfiles_ = s

def is_in_pfiles(swpath, ca):
    ppf = get_p_pfiles(swpath)
    if ppf:
        if ca[1] == ppf:
            return 2
        if ca[2] == ppf:
            return 3
    if ca[1] == swpath.pfiles_:
        return 2
    if ca[1] == swpath.pfiles_default:
        return 2
    if ca[2] == swpath.pfiles_:
        return 3
    if ca[2] == swpath.pfiles_default:
        return 3
    if swpath.swpath_p_dfiles_ and swpath.swpath_p_pfiles_ is None and swpath.swpath_p_fileset_dir_ is None:
        return 3
    return 0

def set_max_rel_n(swpath, n):
    if n > swpath.max_rel_n_:
        swpath.max_rel_n_ = n

def set_is_catalog(swpath, n, loc):
    old = swpath.swpath_is_catalog_
    if old == -1 and n == 0:
        swpath.error = 2
    if old == 1 and n == -1:
        swpath.error = 2
    if old != 1 and n == 1:
        swpath.catalog_guard += 1
    if swpath.catalog_guard > 1:
        swpath.error = 2
    swpath_set_is_catalog(swpath, n)

def swpath_open(path):
    swpath = SWPATH()
    swpath__setup1(swpath)
    swpath.open_pathname_ = path
    if len(path):
        swpath_parse_path(swpath, swpath.open_pathname_)
    return swpath

def swpath_reset(swpath):
    tmp = swpath.open_pathname_
    swpath__unsetup1(swpath)
    swpath__setup1(swpath)
    swpath.open_pathname_ = tmp
    if len(tmp):
        swpath_parse_path(swpath, swpath.open_pathname_)

def swpath_form_path(swpath, buf):
    if swpath.swpath_is_catalog_:
        return swpath_form_catalog_path(swpath, buf)
    else:
        return swpath_form_storage_path(swpath, buf)

def swpath_set_dfiles(swpath, name):
    swpath.dfiles_default = name

def swpath_set_pfiles(swpath, name):
    swpath.pfiles_default = name

def swpath_close(swpath):
    swpath__unsetup1(swpath)
    return 0

def swpath_set_product_control_dir(swpath, s):
    if s and len(s):
        set_control_depth(swpath, 2)
    if swpath.swpath_is_catalog_ == 0:
        if swpath.control_path_nominal_depth_ == 0:
            s = ""
    swpath.product_control_dir_ = s

def swpath_set_fileset_control_dir(swpath, s):
    if s and len(s):
        set_control_depth(swpath, 1)
    if swpath.swpath_is_catalog_ == 0:
        if swpath.control_path_nominal_depth_ == 0:
            s = ""
    swpath.fileset_control_dir_ = s

def swpath_set_pathname(swpath, s):
    swpath.pathname_ = s

def swpath_set_pkgpathname(swpath, s):
    swpath.swpackage_pathname_ = s

def swpath_set_filename(swpath, s):
    if not s:
        swpath__set__basename(swpath, swpath.pathname_)
    else:
        swpath__set__basename(swpath, s)

def swpath_get_product_control_dir(swpath):
    return swpath.product_control_dir_

def swpath_get_fileset_control_dir(swpath):
    return swpath.fileset_control_dir_

def swpath_get_pfiles(swpath):
    return swpath.pfiles_

def swpath_get_dfiles(swpath):
    return swpath.dfiles_

def swpath_get_prepath(swpath):
    return swpath.prepath_

def swpath_get_pathname(swpath):
    return swpath.pathname_

def swpath_get_pkgpathname(swpath):
    return swpath.swpackage_pathname_

def swpath_get_basename(swpath):
    return swpath.basename_

def swpath_form_catalog_path(swpath, buf):
#    buf = swpath.prepath_
    if swpath.swpath_is_catalog_ < 0:
        return buf
    if len(buf):
        buf += "/"
    buf += swc.SW_A_catalog
    buf = swpath__form_path1(swpath, buf, None)
    return buf

def swpath_form_storage_path(swpath, buf):
    buf = swpath.prepath_
    buf = swpath__form_path1(swpath, buf, None)
    return buf

def swpath_set_is_catalog(swpath, n):
    swpath.swpath_is_catalog_ = n

def swpath_get_is_catalog(swpath):
    return swpath.swpath_is_catalog_

def swpath_set_is_minimal_layout(swpath, n):
    swpath.is_minimal_layout = n

def swpath_get_is_minimal_layout(swpath):
    return swpath.is_minimal_layout
def swpath_resolve_prepath(swpath, name):
    if swpath.swpath_p_prepath_:
        return 0
    p = name.find(swc.SW_A_catalog)
    if not p:
        s = name.find(strob_str(swpath.prepath_))
        if s:
            strob_strcat(swpath.prepath_, name + strob_strlen(swpath.prepath_))
        else:
            strob_strcpy(swpath.prepath_, "")
        swpath.swpath_p_prepath_ = None
        return -1
    elif p and not swpath.swpath_p_prepath_ and not strob_strlen(swpath.prepath_):
        len = p - name
        strob_set_length(swpath.prepath_, len + 1)
        strob_strncpy(swpath.prepath_, name, len + 20)
        if len:
            strob_str(swpath.prepath_)[len - 1] = '\0'
        else:
            strob_str(swpath.prepath_)[len] = '\0'
        swpath.swpath_p_prepath_ = strob_str(swpath.prepath_)
    else:
        len = p - name
        strob_set_length(swpath.prepath_, len + 1)
        strob_str(swpath.prepath_)[len] = '\0'
        swpath.swpath_p_prepath_ = strob_str(swpath.prepath_)
        #if strncmp(name, swpath.swpath_p_prepath_, len - 1 < 0 ? 0: len - 1):
        if strncmp(name, swpath.swpath_p_prepath_, len-1 < 0) or  strncmp(name, swpath.swpath_p_prepath_, len-1):
            sys.stderr.write("swpath: inconsistent package pre-path %s not = %s over %d chars.\n" % (name, swpath.swpath_p_prepath_, len))
            return 1
        return 0
    return 0

def swpath_num_of_components(swpath: SWPATH, str_: str) -> int:
    i = 0
    strob_strcpy(swpath.buffer_, str_)
    while strtok(i if i else None, strob_str(swpath.buffer_)):
        i += 1
    return i

SWPATHNEEDDEBUG = 0

def swpath_parse_path(swpath, name):
    relative_n = 0
    n = 0
    i = 0
    n_prepath = 0
    ret = 0
    compy = 0
    pfiles_index = 0

    s = '\0'
    p = '\0'
    ca = ["" for _ in range(SWPATH_PARSE_CA + 1)]
    if SWPATHNEEDDEBUG == 1:
        print("ENTER swpath_parse_path [%s]", name.arg_value)
        strob_strcpy(swpath.buffer_, name.arg_value)
        swpath_set_pkgpathname(swpath, strob_str(swpath.buffer_))
        swlib_squash_trailing_slash(strob_str(swpath.buffer_))
        swlib_squash_leading_dot_slash(strob_str(swpath.buffer_))
        swlib_squash_leading_slash(strob_str(swpath.buffer_))

        strar_add(swpath.pathlist_, strob_str(swpath.buffer_))
        # swpath->swpath_is_catalog_ = 0;
        swpath_set_product_control_dir(swpath, "")
        swpath_set_fileset_control_dir(swpath, "")
        strob_strcpy(swpath.pathname_, "")
        strob_strcpy(swpath.basename_, "")

    if not swpath.swpath_p_prepath_:
        print("")
        swpath_resolve_prepath(swpath, strob_str(swpath.buffer_))

    if not swpath.swpath_p_prepath_:
        #		
        #		* Prepath is not fully formed from the leading 
        #		* pacakge directories,
        #		* return 0.
        #		
        swpath_set_is_catalog(swpath, -1)
        print("")
        print("%s", swpath_dump_string_s(swpath, ""))
        print("EXIT swpath_parse_path")
        return check_error(swpath, 0)

    if len(swpath.swpath_p_prepath_) == 0:
        print("")
        # 2003-12-21 swpath_set_is_catalog(swpath, -1); 
        pass
    elif not(s := strstr(strob_str(swpath.buffer_), strob_str(swpath.prepath_))) or (s != strob_str(swpath.buffer_)) or ((strob_str(swpath.buffer_) + strob_strlen(swpath.prepath_)) != '/'):
        #		 
        #		* error 
        #		
        print("")
        swpath_set_is_catalog(swpath, -1)
        swpath.error = 1
        print("%s", swpath_dump_string_s(swpath, ""))
        print("EXIT swpath_parse_path")
        return -1

    swpath__slashclean(swpath, strob_str(swpath.prepath_))
    n_prepath = swpath_num_of_components(swpath, strob_str(swpath.prepath_))

    for i in range(0, SWPATH_PARSE_CA):
        ca[i] = str(None)


    #  Remove legacy restriction 08Jun2014 
    #	
    #	if (strlen(name) > 255) {
    #		print(""); 
    #		print("%s", swpath_dump_string_s(swpath, ""))
    #		print("EXIT swpath_parse_path"); 
    #		return -1
    #	}
    #	


    strob_strcpy(swpath.buffer_, name.arg_value)
    swpath__slashclean(swpath, strob_str(swpath.buffer_))

    if (s := strob_strstr(swpath.buffer_, strob_str(swpath.prepath_))) != strob_str(swpath.buffer_):
        print("")
        n_prepath = 0

    # set s to point at first component past the prepath 	

    s = strob_str(swpath.buffer_) + strob_strlen(swpath.prepath_)
    if s == '/':
        s += 1

    while (p := strchr(s, ord(('/')))) and n < SWPATH_PARSE_CA:
        ca[n] = s
        s = p[1:]
        p[0] = '\0'
        n += 1
    if n >= (SWPATH_PARSE_CA -1):
        sys.exit(87)
    ca[n] = s
    n += 1
    relative_n = n

    if relative_n == 0:
        print("")
        print("%s", swpath_dump_string_s(swpath, ""))
        print("EXIT swpath_parse_path")
        if swpath.error:
            return -1
        return check_error(swpath, 0)
    elif relative_n == 1:
        print("relative_n == 1")
        print("%s", swpath_parse_write_debug(dbuf, relative_n, ca))
        compy = 1
        if strcmp(ca[0], swc.SW_A_catalog) != 0:
            #			
            #			* Not in catalog.
            #			
            print("")
            set_v_dfiles(swpath, "")
            set_v_pfiles(swpath, "")
            set_is_catalog(swpath, 0, "A")
            if swpath.max_rel_n_ >= SWPATH_DEGEN_N:
                ret += 1
                swpath_set_product_control_dir(swpath, ca[0])
                print("")
            else:
                compy = 0
        else:
            print("")
            #			
            #			* In catalog
            #			
            set_max_rel_n(swpath, relative_n)
            set_is_catalog(swpath, 1, "B")
    elif relative_n == 2:
        compy = 2
        print("relative_n == 2")
        print("%s", swpath_parse_write_debug(dbuf, relative_n, ca))
        if strcmp(ca[0], swc.SW_A_catalog) == 0:
            #			
            #			* In catalog
            #			
            print("-<>-")
            print("In catalog")
            set_max_rel_n(swpath, relative_n)
            set_is_catalog(swpath, 1, "C")
            print("-<>-")
            # If dfiles hasn't been set; and Not INDEX 
            if (get_p_dfiles(swpath) != '\0' and strcmp(ca[1], get_p_dfiles(swpath)) == 0) or ((not swpath.swpath_p_dfiles_) and strcmp(ca[1], "INDEX") != 0):
                print("C::-<>-")
                set_v_dfiles(swpath, ca[1])
                set_p_dfiles(swpath, ca[1])
            elif True and len(swpath_get_product_control_dir(swpath)) == 0 and (not swpath.swpath_p_pfiles_) and strcmp(ca[1], swpath.pfiles_default) == 0:
                #				
                #				* In pfiles.
                #				
                print("C::-<>-.-<>-.-<>-")
                swpath_set_is_minimal_layout(swpath, 1)
                set_v_pfiles(swpath, ca[1])
                set_p_pfiles(swpath, ca[1])
                set_v_dfiles(swpath, "")
            elif strcmp(ca[1], swpath.pfiles_default) == 0:
                print("C::-<>-.-<>-.-<>-.-<>-.-<>-.-<>-.")
                temp_ref_pfiles_default = swpath.pfiles_default
                set_v_pfiles(swpath, temp_ref_pfiles_default)
                swpath.pfiles_default = temp_ref_pfiles_default.arg_value
                temp_ref_pfiles_default2 = swpath.pfiles_default
                set_p_pfiles(swpath, temp_ref_pfiles_default2)
                swpath.pfiles_default = temp_ref_pfiles_default2.arg_value
                swpath_set_is_minimal_layout(swpath, 1)
                set_v_dfiles(swpath, "")
            elif swpath.swpath_p_dfiles_ and strcmp(ca[1], "INDEX") != 0 and strcmp(ca[1], swpath.swpath_p_dfiles_) != 0:
                print("C:-<>-.-<>-.-<>-.-<>-.-<>-.-<>-.-<>-.")
                ret += 1
                set_v_dfiles(swpath, "")
                set_v_pfiles(swpath, "")
                temp_ref_null = None
                set_p_fileset_dir(swpath, temp_ref_null)
                temp_ref_null2 = None
                set_p_pfiles(swpath, temp_ref_null2) # does nothing
                if swpath_get_is_minimal_layout(swpath) != 0:
                    compy = 1
                else:
                    swpath_set_product_control_dir(swpath, ca[1])
            elif not strcmp(ca[1], "INDEX"):
                print("C::-<>-.-<>-.-<>-.-<>-.-<>-.-<>-.-<>-.-<>-.-<>-.")
                compy = 1
        else:
            print("")
            set_v_dfiles(swpath, "")
            set_v_pfiles(swpath, "")
            set_is_catalog(swpath, 0, "D")
            if swpath.max_rel_n_ >= SWPATH_DEGEN_N:
                print("relative_n other")
                print("Not in catalog")
                ret += 1
                swpath_set_product_control_dir(swpath, ca[0])
                ret += 1
                swpath_set_fileset_control_dir(swpath, ca[1])
                set_p_fileset_dir(swpath, ca[1])
                compy = 2
            else:
                #				
                #				* Empty control directories.
                #				
                print("")
                compy = 0
    elif relative_n == 3:
        print("%s", swpath_parse_write_debug(dbuf, relative_n, ca))
        print("-<>-")
        if strcmp(ca[0], swc.SW_A_catalog) == 0:
            #			
            #			* In catalog
            #			
            print("-<>-")
            set_max_rel_n(swpath, relative_n)
            set_is_catalog(swpath, 1, "E")
            compy = n
            print("In catalog")
            if (swpath.swpath_p_dfiles_ and strcmp(ca[1], swpath.swpath_p_dfiles_) != 0):
                #				
                #				* not in dfiles.
                #				
                print("-<>-")
                print("NOT In dfiles")
                compy = 2
                ret += 1
                if strcmp(swpath_get_product_control_dir(swpath), ca[1]):
                    #					 New product
                    #					* This case occurs when the empty 
                    #					* leading dir is missing from
                    #					* the package. 
                    #					
                    print("")
                    temp_ref_null3 = None
                    set_p_fileset_dir(swpath, temp_ref_null3)
                    temp_ref_null4 = None
                    set_p_pfiles(swpath, temp_ref_null4)

                pfiles_index = is_in_pfiles(swpath, ca)

                if False or (get_p_pfiles(swpath) is None and strcmp(ca[1], swpath.pfiles_default)):
                    print("")
                    set_v_dfiles(swpath, "")
                    swpath_set_product_control_dir(swpath, ca[1])
                elif True and swpath.swpath_p_pfiles_ is None and strcmp(ca[1], swpath.pfiles_default) == 0:
                    print("")
                    set_v_dfiles(swpath, "")
                    set_v_pfiles(swpath, ca[1])
                    set_p_pfiles(swpath, ca[1])
                else:
                    print("")
                    set_v_dfiles(swpath, "")
                    swpath_set_product_control_dir(swpath, ca[1])

                if pfiles_index != 0:
                    print("-<>-")
                    print("In pfiles")
                    compy = pfiles_index
                    if compy == 2:
                        #						
                        #						* Unset the product directory.
                        #						* Empty Control directories.
                        #						
                        print("")
                        swpath_set_product_control_dir(swpath, "")
                    temp_ref_null5 = None
                    set_p_fileset_dir(swpath, temp_ref_null5)
                    if len(strob_str(swpath.pfiles_)) != 0:
                        print("")
                        set_v_pfiles(swpath, strob_str(swpath.pfiles_))
                    else:
                        print("")
                        set_p_pfiles(swpath, ca[2])
                        set_v_pfiles(swpath, ca[2])
                    set_v_dfiles(swpath, "")
                    set_p_pfiles(swpath, strob_str(swpath.pfiles_))
                elif strcmp(ca[2], strob_str(swpath.pfiles_)):
                    # // Not in pfiles. 
                    print("-<>-")
                    print("NOT In pfiles")
                    ret += 1
                    set_v_pfiles(swpath, "")
                    swpath_set_fileset_control_dir(swpath, ca[2])
                    set_p_fileset_dir(swpath, ca[2])
                    compy = 3
                else:
                    print("")
                    sys.stderr.write("Internal error: Tripped a" + " bug in swpath.c," + " assumes pfiles = pfiles\n")
            else:
                #				
                #				* In dfiles 
                #				
                print("-<>-")
                print("In dfiles")
                set_v_dfiles(swpath, ca[1])
                set_p_dfiles(swpath, ca[1])
                compy = 2
        else:
            #			
            #			* NOT in catalog section.
            #			
            print("")
            set_is_catalog(swpath, 0, "F")
            set_v_pfiles(swpath, "")
            set_v_dfiles(swpath, "")
            if swpath.max_rel_n_ >= SWPATH_DEGEN_N:
                print("-<>-")
                print("Not in catalog")
                ret += 1
                swpath_set_product_control_dir(swpath, ca[0])
                ret += 1
                swpath_set_fileset_control_dir(swpath, ca[1])
                set_p_fileset_dir(swpath, ca[1])
                compy = 2
            else:
                #				
                #				* Empty control directories.
                #				
                print("")
                compy = 0
    elif relative_n > 3:
        print("")
        print("%s", swpath_parse_write_debug(dbuf, relative_n, ca))
        if strcmp(ca[0], swc.SW_A_catalog) == 0:
            #			
            #			* In catalog section.
            #			
            print("-<>-")
            print("in catalog")
            set_max_rel_n(swpath, relative_n)
            set_is_catalog(swpath, 1, "G")
            compy = n
            if strcmp(ca[1], swpath.dfiles_default):
                # // Not in dfiles. 
                print("")
                print("not in dfiles")
                compy = 2
                ret += 1
                swpath_set_product_control_dir(swpath, ca[1])
                if (strcmp(ca[2], swpath.pfiles_default)) and (strcmp(ca[2], strob_str(swpath.pfiles_))):
                    print("-<>-")
                    print("not in pfiles")
                    ret += 1
                    swpath_set_fileset_control_dir(swpath, ca[2])
                    set_v_pfiles(swpath, "")
                    set_v_dfiles(swpath, "")
                    set_p_fileset_dir(swpath, ca[2])
                    compy = 3
                elif (get_p_pfiles(swpath) != '\0' and strcmp(ca[2], get_p_pfiles(swpath)) == 0) or (strcmp(ca[2], swpath.pfiles_default) == 0 or strcmp(ca[2], strob_str(swpath.pfiles_)) == 0):
                    # // In pfiles. 
                    print("-<>-")
                    print("In pfiles")
                    if len(strob_str(swpath.pfiles_)) != 0:
                        print("")
                        set_v_pfiles(swpath, strob_str(swpath.pfiles_))
                    else:
                        print("")
                        temp_ref_pfiles_default3 = swpath.pfiles_default
                        set_v_pfiles(swpath, temp_ref_pfiles_default3)
                        swpath.pfiles_default = temp_ref_pfiles_default3.arg_value
                    set_p_pfiles(swpath, strob_str(swpath.pfiles_))
                    set_v_dfiles(swpath, "")
                    compy = 3
            else:
                #				
                #				* In dfiles 
                #				
                print("")
                compy = 2
        else:
            #			 
            #			* NOT in catalog section. 
            #			
            set_is_catalog(swpath, 0, "H")
            if swpath.max_rel_n_ >= SWPATH_DEGEN_N:
                print("-<>-")
                print("Not in catalog")
                ret += 1
                swpath_set_product_control_dir(swpath, ca[0])
                ret += 1
                swpath_set_fileset_control_dir(swpath, ca[1])
                compy = 2
            else:
                #				
                #				* Empty control directories.
                #				
                print("")
                compy = 0

    if swpath.control_path_nominal_depth_ == 0 and swpath.swpath_is_catalog_ == 0:
        #		
        #		* This supports the degenerate form.
        #		
        print("compy=0")
        compy = 0

    print("FINAL compy=%d", compy)
    print("%s", swpath_parse_write_debug(dbuf, relative_n, ca))
    #	 
    #	* reconstruct filename  
    #	
    i = compy

    while i in range(compy, n - 1):
        ca[i] = ca[i] + '/'


    if ca[compy] != '\0':
        print("set_pathname ca[%d] = %s", compy, ca[compy])
        swpath_set_pathname(swpath, ca[compy])
    if ca[compy] != '\0':
        print("set_filename ca[%d] = %s", compy, ca[compy])
        swpath_set_filename(swpath, ca[compy])
    print("%s", swpath_dump_string_s(swpath, ""))
    print("EXIT swpath_parse_path")
    return check_error(swpath, ret)

def swpath_dump_string_s(swpath: SWPATH, prefix: str) -> str:
    s: str
    buf = strob_open(100)
    prebuf: str = ""
    if buf == None:

        strob_sprintf(buf, 0, f"{prefix}{swpath} (SWPATH*)\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->swpath_is_catalog_      = [{swpath.swpath_is_catalog_}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->error                  = [{swpath.error}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->control_path_nominal_depth_ = [{swpath.control_path_nominal_depth_}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->product_control_dir_    = [{strob_str(swpath.product_control_dir_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->fileset_control_dir_    = [{strob_str(swpath.fileset_control_dir_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->dfiles_                 = [{strob_str(swpath.dfiles_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->pfiles_                 = [{strob_str(swpath.pfiles_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->swpackage_pathname_     = [{strob_str(swpath.swpackage_pathname_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->pathname_               = [{strob_str(swpath.pathname_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->buffer_                 = [{strob_str(swpath.buffer_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->basename_               = [{strob_str(swpath.basename_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->prepath_                = [{strob_str(swpath.prepath_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->p_pfiles_               = [{strob_str(swpath.p_pfiles_)}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->p_dfiles_               = [{strob_str(swpath.p_dfiles_)}]\n")
        strob_sprintf(buf, 1,
                      f"{prefix}{swpath}->swpath_p_prepath_       = [{swpath.swpath_p_prepath_ if swpath.swpath_p_prepath_ else ''}]\n")
        strob_sprintf(buf, 1,
                      f"{prefix}{swpath}->swpath_p_dfiles_        = [{swpath.swpath_p_dfiles_ if swpath.swpath_p_dfiles_ else ''}]\n")
        strob_sprintf(buf, 1,
                      f"{prefix}{swpath}->swpath_p_pfiles_        = [{swpath.swpath_p_pfiles_ if swpath.swpath_p_pfiles_ else ''}]\n")
        strob_sprintf(buf, 1,
                      f"{prefix}{swpath}->swpath_p_fileset_dir_   = [{swpath.swpath_p_fileset_dir_ if swpath.swpath_p_fileset_dir_ else ''}]\n")
        strob_sprintf(buf, 1, f"{prefix}{swpath}->pathlist_               = [{swpath.pathlist_}]\n")
        strob_sprintf(buf, 1, "...\n")
        strob_sprintf(buf, 1, f"   pkgpathname          = \"{swpath_get_pkgpathname(swpath)}\"\n")
        strob_sprintf(buf, 1, f"   prepath              = \"{swpath_get_prepath(swpath)}\"\n")
        strob_sprintf(buf, 1, f"   dfiles               = \"{swpath_get_dfiles(swpath)}\"\n")
        strob_sprintf(buf, 1, f"   pfiles               = \"{swpath_get_pfiles(swpath)}\"\n")
        strob_sprintf(buf, 1, f"   product_control_dir  = \"{swpath_get_product_control_dir(swpath)}\"\n")
        strob_sprintf(buf, 1, f"   fileset_dir          = \"{swpath_get_fileset_control_dir(swpath)}\"\n")
        strob_sprintf(buf, 1, f"   pathname             = \"{swpath_get_pathname(swpath)}\"\n")
        strob_sprintf(buf, 1, f"   filename             = \"{swpath_get_basename(swpath)}\"\n")

    return strob_str(buf)


def swpath_shallow_fill_export(sx, sp):
    sx.is_minimal_layout = swpath_get_is_minimal_layout(sp)
    sx.is_catalog = swpath_get_is_catalog(sp)
    sx.ctl_depth = sp.control_path_nominal_depth_
    sx.pkgpathname = swpath_get_pkgpathname(sp)
    sx.prepath = swpath_get_prepath(sp)
    sx.dfiles = swpath_get_dfiles(sp)
    sx.pfiles = swpath_get_pfiles(sp)
    sx.product_control_dir = swpath_get_product_control_dir(sp)
    sx.fileset_control_dir = swpath_get_fileset_control_dir(sp)
    sx.pathname = swpath_get_pathname(sp)
    sx.basename = swpath_get_basename(sp)

def swpath_shallow_create_export():
    return swpath_create_export(None)

def swpath_create_export(sp):
    sx = SWPATH_EX()
    if not sp:
        sx.is_minimal_layout = 0
        sx.is_catalog = 0
        sx.ctl_depth = 0
        sx.pkgpathname = ""
        sx.prepath = ""
        sx.dfiles = ""
        sx.pfiles = ""
        sx.product_control_dir = ""
        sx.fileset_control_dir = ""
        sx.pathname = ""
        sx.basename = ""
    else:
        sx.is_minimal_layout = swpath_get_is_minimal_layout(sp)
        sx.is_catalog = swpath_get_is_catalog(sp)
        sx.ctl_depth = sp.control_path_nominal_depth_
        sx.pkgpathname = sp.get_pkgpathname() if sp.get_pkgpathname() else None
        sx.prepath = sp.get_prepath() if sp.get_prepath() else None
        sx.dfiles = sp.get_dfiles() if sp.get_dfiles() else None
        sx.pfiles = sp.get_pfiles() if sp.get_pfiles() else None
        sx.product_control_dir = sp.get_product_control_dir() if sp.get_product_control_dir() else None
        sx.fileset_control_dir = sp.get_fileset_control_dir() if sp.get_fileset_control_dir() else None
        sx.pathname = sp.get_pathname() if sp.get_pathname() else None
        sx.basename = sp.get_basename() if sp.get_basename() else None
    return sx

def swpath_ex_print(swpath, buf, prefix):
    buf.append(f"{prefix}{swpath} (SWPATH_EX*)\n")
    if swpath:
        buf.append(f"{prefix}{swpath}.pkgpathname     = [{swpath.pkgpathname}]\n")
        buf.append(f"{prefix}{swpath}.is_minimal_layout = [{swpath.is_minimal_layout}]\n")
        buf.append(f"{prefix}{swpath}.is_catalog      = [{swpath.is_catalog}]\n")
        buf.append(f"{prefix}{swpath}.ctl_depth       = [{swpath.ctl_depth}]\n")
        buf.append(f"{prefix}{swpath}.prepath         = [{swpath.prepath}]\n")
        buf.append(f"{prefix}{swpath}.dfiles          = [{swpath.dfiles}]\n")
        buf.append(f"{prefix}{swpath}.pfiles          = [{swpath.pfiles}]\n")
        buf.append(f"{prefix}{swpath}.product_control_dir = [{swpath.product_control_dir}]\n")
        buf.append(f"{prefix}{swpath}.fileset_control_dir = [{swpath.fileset_control_dir}]\n")
        buf.append(f"{prefix}{swpath}.pathname            = [{swpath.pathname}]\n")
        buf.append(f"{prefix}{swpath}.basename            = [{swpath.basename}]\n")
    return "".join(buf)

def swpath_delete_export(sx):
    if sx.pkgpathname:
        del sx.pkgpathname
    if sx.prepath:
        del sx.prepath
    if sx.dfiles:
        del sx.dfiles
    if sx.pfiles:
        del sx.pfiles
    if sx.product_control_dir:
        del sx.product_control_dir
    if sx.fileset_control_dir:
        del sx.fileset_control_dir
    if sx.pathname:
        del sx.pathname
    if sx.basename:
        del sx.basename
    del sx