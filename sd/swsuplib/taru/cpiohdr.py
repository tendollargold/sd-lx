# Extended cpio header from POSIX.1.
#   Copyright (C) 1992 Free Software Foundation, Inc.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
import sys


class CpioHdrConstants:
    C_IRUSR = 0o400
    C_IWUSR = 0o200
    C_IXUSR = 0o100
    C_IRGRP = 0o040
    C_IWGRP = 0o020
    C_IXGRP = 0o010
    C_IROTH = 0o004
    C_IWOTH = 0o002
    C_IXOTH = 0o001
    C_ISUID = 0o4000
    C_ISGID = 0o2000
    C_ISVTX = 0o1000
    C_ISBLK = 0o60000
    C_ISCHR = 0o20000
    C_ISDIR = 0o40000
    C_ISFIFO = 0o10000
    C_ISSOCK = 0o140000
    C_ISLNK = 0o120000
    C_ISCTG = 0o110000
    C_ISREG = 0o100000

    DIGS_ENABLE_OFF = 0
    DIGS_ENABLE_ON = 1
    DIGS_ENABLE_POISON = 2
    DIGS_ENABLE_CLEAR = -1
    DIGS_DO_POISON = 1
    TARU_C_ID_UNSET = sys.maxsize
    EXATT_NAMESPACE_LEN = 100
    EXATT_PREFIX_LEN = 100

    TARU_EHUM_ATIME = (1 << 0)
    TARU_EHUM_CHARSET = (1 << 1)
    TARU_EHUM_COMMENT = (1 << 2)
    TARU_EHUM_CTIME = (1 << 3)
    TARU_EHUM_GID = (1 << 4)
    TARU_EHUM_GNAME = (1 << 5)
    TARU_EHUM_LINKPATH = (1 << 6)
    TARU_EHUM_MTIME = (1 << 7)
    TARU_EHUM_PATH = (1 << 8)
    TARU_EHUM_REALTIMEANY = (1 << 9)
    TARU_EHUM_SECURITYANY = (1 << 10)
    TARU_EHUM_SIZE = (1 << 11)
    TARU_EHUM_UID = (1 << 12)
    TARU_EHUM_UNAME = (1 << 13)
    TARU_EHUM_ANY = (1 << 14)
    TARU_EHUM_DEV_MAJ = (1 << 15)
    TARU_EHUM_DEV_MIN = (1 << 16)

    TARU_UM_MODE = (1 << 0)  # These are the bits of usage_maskM
    TARU_UM_UID = (1 << 1)
    TARU_UM_GID = (1 << 2)
    TARU_UM_OWNER = (1 << 3)
    TARU_UM_GROUP = (1 << 4)
    TARU_UM_MTIME = (1 << 5)
    TARU_UM_IS_VOLATILE = (1 << 6)

chc = CpioHdrConstants
class OldCpioHeader:
    def __init__(self):
        self.c_magic = 0
        self.c_dev = 0
        self.c_ino = 0
        self.c_mode = 0
        self.c_uid = 0
        self.c_gid = 0
        self.c_nlink = 0
        self.c_rdev = 0
        self.c_mtimes = [0, 0]
        self.c_namesize = 0
        self.c_filesizes = [0, 0]
        self.c_mtime = 0
        self.c_filesize = 0
        self.c_name = ""


class EXATT:
    def __init__(self):
        self.magic = ""
        self.is_in_use = 0
        self.vendor = [''] * chc.EXATT_PREFIX_LEN
        self.namespace = [''] * chc.EXATT_NAMESPACE_LEN
        self.attrname = ""
        self.attrvalue = ""
        self.len = 0


class FILEDIGS:
    def __init__(self):
        self.do_poison = 0
        self.md5 = ""
        self.do_md5 = 0
        self.sha1 = ""
        self.do_sha1 = 0
        self.sha512 = ""
        self.do_sha512 = 0
        self.size = ""
        self.do_size = 0


class NewCpioHeader:
    def __init__(self):
        self.c_magic = 0
        self.c_ino = 0
        self.c_mode = 0
        self.c_uid = 0
        self.c_gid = 0
        self.c_nlink = 0
        self.c_mtime = 0
        self.c_filesize = 0
        self.c_dev_maj = 0
        self.c_dev_min = 0
        self.c_rdev_maj = 0
        self.c_rdev_min = 0
        self.c_namesize = 0
        self.c_chksum = 0
        self.c_name = ""
        self.c_tar_linkname = ""
        self.c_username = ""
        self.c_groupname = ""
        self.c_cu = 0
        self.c_cg = 0
        self.c_is_tar_lnktype = ""
        self.usage_mask = 0
        self.extHeader_usage_mask = 0
        self.extHeader_needs_mask = 0
        self.digs = None
        self.extattlist = None
        self.c_atime = 0
        self.c_atime_nsec = 0
        self.c_ctime = 0
        self.c_ctime_nsec = 0


ncpioh = NewCpioHeader
ocpioh = OldCpioHeader

