# sd.swsuplib.misc.swsdflt.py: Software Structure Defaults of IEEE 1387.2 Std.
#

#
# Copyright (C) 1998  James H. Lowe, Jr.  <jhlowe@acm.org>
#           (C) 2023 Paul Weber conversion to Python
# All rights reserved.
#
# COPYING TERMS AND CONDITIONS
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
from ctypes import *
from enum import Enum
from sd.swsuplib.misc.cstrings import strchr


class SdfConstants:
    SDF_UNKNOWN = 0
    SDF_OBJECT_KW = 1
    SDF_ATTRIBUTE_KW = 2
    SDF_EXTENDED_KW = 3
    SDF_POSIX_7_2 = 0
    SDF_SWBIS = 1
    SDF_THIS_IMPLEMENTATION = 2
    SDF_DPKG = 3
    SDF_RPM = 4
    XDSA_C701 = 5
    SDF_POSIX_7_2_ANNEX = 6
    SDF_NOT_FOUND = 0
    SDF_ID = 1
    SDF_ATTR = 2
    SDF_OBJS = 3
    CONTROLFILE_EXT = 4
    FILE_EXT = 5
    SWDEFFILE = 6
    SDF_SINGLE_VALUE = 0
    SDF_LIST = 1
    SDF_NO = 0
    SDF_YES = 1
    SDF_IMDEF = 2
    SDF_UNDEFINED = 0
    SDF_HOST = 0
    SDF_DISTRIBUTION = 1
    SDF_PRODUCT = 2
    SDF_SUBPRODUCT = 3
    SDF_FILESET = 4
    SDF_BUNDLE = 5
    SDF_VENDOR = 6
    SDF_CATEGORY = 7
    SDF_MEDIA = 8
    SDF_FILE = 9
    SDF_CONTROL_FILE = 10
    SDF_PSF = 11
    SDF_PSFI = 12
    SDF_INFO = 13
    SDF_INDEX = 14
    SDF_END = 15
    SDF_OPTION = 16
    SDF_INSTALLED_SOFTWARE = 17
    SDF_INSTALLED = 18
    SDF_LEFTMOST = 19
    SDF_ALL = 20
    SDF_DEFAULT = 21
    SDF_NULL = 22


SdC = SdfConstants
#
# char  PortableCharacterString[] = "portable character string";
# char  FilenameCharacterString[] = "filename character string";
# char  ListofDependency_specs[] = "list of dependency_specs";
# char  PathnameCharacterString[] = "pathname character string";
# char  SoftwarePatternMatchingString[] = "software pattern matching string";
# char  ListofSoftware_specs[] = "list of software_specs";
# char  IntegerCharacterString[] = "integer character string";
# char  StateList[] = "configured,installed,corrupt,removed,available,transient";
# char  OctalCharacterString[] = "octal character string";
# char  ControlScript[] = "program text";
#

class ObjType(Enum):
   sdf_unknown = 0
   sdf_object_kw = 1
   sdf_attribute_kw = 2
   sdf_extended_kw = 3

# Enum for swsdflt_sanction
class SwsdfltSanction(Enum):
   sdf_POSIX_7_2 = 0
   sdf_swbis = 1
   sdf_this_implementation = 2
   sdf_dpkg = 3
   sdf_rpm = 4
   XDSA_C701 = 5
   sdf_POSIX_7_2_annex = 6


SdS = SwsdfltSanction


# Enum for swsdflt_group
class SwsdfltGroup(Enum):
   sdf_not_found = 0
   sdf_id = 1
   sdf_attr = 2
   sdf_objs = 3
   CONTROLFILE_EXT = 4
   FILE_EXT = 5
   SWDEFFILE = 6


SdG = SwsdfltGroup

# Enum for swsdflt_value_type
class SwsdfltValueType(Enum):
   sdf_single_value = 0
   sdf_list = 1


sdvt = SwsdfltValueType

# Enum for swsdflt_has_default_value
class SwsdfltHasDefaultValue(Enum):
   sdf_no = 0
   sdf_yes = 1
   sdf_IMDEF = 2 # ImplementationDefined


SdHDV= SwsdfltHasDefaultValue


# Enum for swsdflt_length
class SwsdfltLength(Enum):
    sdf_undefined = 0

SdL = SwsdfltLength

# Enum for swsdflt_object_keyword
class SwsdfltObjectKeyword(Enum):
   sdf_host = 0
   sdf_distribution = 1
   sdf_product = 2
   sdf_subproduct = 3
   sdf_fileset = 4
   sdf_bundle = 5
   sdf_vendor = 6
   sdf_category = 7
   sdf_media = 8
   sdf_file = 9
   sdf_control_file = 10
   sdf_PSF = 11
   sdf_PSFi = 12
   sdf_INFO = 13
   sdf_INDEX = 14
   sdf_END = 15
   sdf_OPTION = 16
   sdf_installed_software = 17
   sdf_INSTALLED = 18
   sdf_leftmost = 19
   sdf_all = 20
   sdf_default = 21
   sdf_NULL = 22

SdOK = SwsdfltObjectKeyword

class SwsdfltPermittedValue(Enum):
    sdf_PortableCharacterString = 0
    sdf_FilenameCharacterString = 1
    sdf_ListofDependency_specs = 2
    sdf_PathnameCharacterString = 3
    sdf_SoftwarePatternMatchingString = 4
    sdf_ListofSoftware_specs = 5
    sdf_IntegerCharacterString = 6
    sdf_StateList = 7
    sdf_OctalCharacterString = 8
    sdf_ControlScript = 9
    sdf_not_set = 10


SdPV = SwsdfltPermittedValue

class SwsdfltDefaults(Structure):
    _fields_ = [
        ("object_keyword", c_ubyte),
        ("sanction", c_ubyte),
        ("group", c_ubyte),
        ("keyword", c_char_p),
        ("length", c_short),
        ("value_type", c_ubyte),
        ("permitted_valueM", c_ubyte),
        ("has_default_value", c_ubyte),
        ("value", c_char_p)
    ]
    
    
swsdflts_objects = ["host", "distribution", "product", "subproduct", "fileset", "bundle", "vendor", "category", "media", "file", "control_file", "PSF", "PSFi", "INFO", "INDEX", "_END", "OPTION", "installed_software", "INSTALLED", "_leftmost", "_all", "_default", None]

swsdflt_arr = [SwsdfltDefaults(SdOK.sdf_default, SdS.sdf_swbis, SdG.sdf_id, "*", SdL.sdf_undefined, sdvt.sdf_single_value,
                               SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_leftmost, SdS.sdf_this_implementation, SdG.sdf_objs, "leftmost", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_leftmost, SdS.sdf_this_implementation, SdG.sdf_id, "revision", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_leftmost, SdS.sdf_this_implementation, SdG.sdf_id, "architecture", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_leftmost, SdS.sdf_this_implementation, SdG.sdf_id, "vendor_tag", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_leftmost, SdS.sdf_this_implementation, SdG.sdf_id, "location", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_leftmost, SdS.sdf_this_implementation, SdG.sdf_id, "qualifier", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_PSF, SdS.sdf_swbis, SdG.SWDEFFILE, "PSF", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_PSFi, SdS.sdf_swbis, SdG.SWDEFFILE, "PSFi", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_INFO, SdS.sdf_swbis, SdG.SWDEFFILE, "INFO", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_INDEX, SdS.sdf_swbis, SdG.SWDEFFILE, "INDEX", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_INSTALLED, SdS.sdf_swbis, SdG.SWDEFFILE, "INSTALLED", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_OPTION, SdS.sdf_swbis, SdG.SWDEFFILE, "OPTION", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "host", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2_annex, SdG.sdf_id, "hostname", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "os_name", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "os_release", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "os_version", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "machine_type", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2_annex, SdG.sdf_objs, "distribution", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_host, SdS.sdf_POSIX_7_2_annex, SdG.sdf_objs, "installed_software", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2, SdG.sdf_objs, "distribution", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2, SdG.sdf_id, "path", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "dfiles", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "dfiles"),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "layout_version", 64, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "1.0"),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "pfiles", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "pfiles"),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "uuid", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "title", 256, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "description", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "revision", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "media_type", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "copyright", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "create_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "number", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution,SdS.sdf_POSIX_7_2_annex, SdG.sdf_attr, "architecture", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "owner", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "group", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "control_directory", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "release", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "url", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "chksum_md5", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "signature", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "rpm_default_prefix", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "/"),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "rpm_obsoletes", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "rpm_buildarchs", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_swbis, SdG.sdf_attr, "rpmversion", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "media", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "bundle", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "product", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_POSIX_7_2, SdG.SWDEFFILE, "INDEX", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_distribution, SdS.sdf_POSIX_7_2, SdG.SWDEFFILE, "INFO", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "installed_software", SdL.sdf_undefined, sdvt.sdf_single_value, SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""), 
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.sdf_id, "path", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.sdf_id, "catalog", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "dfiles", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "dfiles"),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "layout_version", 64, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "1.0"),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "pfiles", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "pfiles"),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_swbis, SdG.sdf_attr, "install_time", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "bundle", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.SWDEFFILE, "INDEX", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_installed_software, SdS.sdf_POSIX_7_2, SdG.SWDEFFILE, "INFO", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_media, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "media", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, "1"),
SwsdfltDefaults(SdOK.sdf_media, SdS.sdf_POSIX_7_2, SdG.sdf_id, "sequence_number", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, "1"),
SwsdfltDefaults(SdOK.sdf_vendor, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "vendor", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_vendor, SdS.sdf_POSIX_7_2, SdG.sdf_id, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_vendor, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "title", 256, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_vendor, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "description", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_vendor, SdS.sdf_swbis, SdG.sdf_attr, "qualifier", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_no,""),
SwsdfltDefaults(SdOK.sdf_category, SdS.XDSA_C701, SdG.sdf_objs, "category", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_category, SdS.XDSA_C701, SdG.sdf_id, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_no, ""),
SwsdfltDefaults(SdOK.sdf_category, SdS.XDSA_C701, SdG.sdf_attr, "title", 256, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_category, SdS.XDSA_C701, SdG.sdf_attr, "description", SdL.sdf_undefined,
                sdvt.sdf_single_value, SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_category, SdS.XDSA_C701, SdG.sdf_attr, "revision", SdL.sdf_undefined,
                sdvt.sdf_single_value, SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "product", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_id, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_id, "vendor_tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_id, "architecture", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_id, "location", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes,"<product.directory>"),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_id, "qualifier", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_id, "revision", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "all_filesets", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "control_directory", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "<product.tag>"),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "copyrighters", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "build_root", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "/"),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "build_host", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "copyright", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "create_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "directory", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "/"),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "description", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "instance_id", 16, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "1"),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "is_locatable", 8, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "true"),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "layout_version", 64, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "1.0"),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "postkernel", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_IMDEF, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "machine_type", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "mod_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "number", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "os_name", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "os_release", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "os_version", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "size", 32, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "title", 256, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "source_package", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "sourcerpm", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "all_patches", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "url", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "rpm_provides", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_swbis, SdG.sdf_attr, "change_log", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.XDSA_C701, SdG.sdf_attr, "is_patch", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_product, SdS.XDSA_C701, SdG.sdf_attr, "category_tag", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "control_file", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "subproduct", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "fileset", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"checkinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"preinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"postinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"verify", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"fix", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"checkremove", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"preremove", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"postremove", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"configure", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"unconfigure", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"request", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"unpreinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"unpostinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"space", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"control_file", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_product, SdS.sdf_POSIX_7_2, SdG.SWDEFFILE, "INFO", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "bundle", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_id, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_id, "vendor_tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_id, "architecture", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_id, "location", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "<bundle.directory>"),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_id, "qualifier", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_id, "revision", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "contents", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "copyright", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "create_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "directory", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "description", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "instance_id", 16, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "1"),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "is_locatable", 8, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "true"),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "machine_type", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "mod_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "number", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "os_name", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "os_release", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "os_version", 64, sdvt.sdf_single_value,
                SdPV.sdf_SoftwarePatternMatchingString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "size", 32, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "title", 256, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.XDSA_C701, SdG.sdf_attr, "is_patch", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_bundle, SdS.XDSA_C701, SdG.sdf_attr, "category_tag", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "fileset", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_id, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_swbis, SdG.sdf_attr, "vendor_tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "create_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "description", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "mod_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "size", 32, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "title", 256, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "control_directory", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "<fileset.tag>"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "corequisites", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofDependency_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "exrequisites", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofDependency_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "is_kernel", 8, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "is_locatable", 8, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "true"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "is_reboot", 8, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "is_sparse", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "location", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "<product.directory>"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "media_sequence_number",SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "1"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "prerequisites", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofDependency_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "revision", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "state", 16, sdvt.sdf_single_value,
                SdPV.sdf_StateList, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_swbis, SdG.sdf_attr, "rpm_flag", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_yes, "0"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_swbis, SdG.sdf_attr, "exclusive_arch", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_swbis, SdG.sdf_attr, "exclude_arch", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_swbis, SdG.sdf_attr, "exclusive_os", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_swbis, SdG.sdf_attr, "exclude_os", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "control_file", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "file", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"checkinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"preinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"postinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"verify", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"fix", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"checkremove", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"preremove", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"postremove", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"configure", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"unconfigure", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"request", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"unpreinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"unpostinstall", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"space", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.CONTROLFILE_EXT,"control_file", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_ControlScript, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset,SdS.sdf_POSIX_7_2, SdG.SWDEFFILE, "INFO", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "is_patch", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "category_tag", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "ancestor", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, "<product.tag>"),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "applied_patches", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "patch_state", 16, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_no, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "saved_files_directory",SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, ""),
SwsdfltDefaults(SdOK.sdf_fileset, SdS.XDSA_C701, SdG.sdf_attr, "supersedes", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_ListofSoftware_specs, SdHDV.sdf_no, ""),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "subproduct", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_id, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "create_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "description", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "mod_time", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "size", 32, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "title", 256, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "contents", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.XDSA_C701, SdG.sdf_attr, "is_patch", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_subproduct, SdS.XDSA_C701, SdG.sdf_attr, "category_tag", SdL.sdf_undefined, sdvt.sdf_list,
                SdPV.sdf_not_set, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_objs, "file", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_id, "path", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "chksum", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_swbis, SdG.sdf_attr, "chksum_md5", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compressed_chksum", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compressed_size", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compression_state", 16, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "uncompressed"),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compression_type", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "revision", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "size", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "source", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "gid", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, "sdf_undefined"),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "group", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "is_volatile", 8, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "false"),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "link_source", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_swbis, SdG.sdf_attr, "rdev", 16, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "major", 16, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "minor", 16, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "mode", 16, sdvt.sdf_single_value,
                SdPV.sdf_OctalCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "mtime", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "owner", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "type", 8, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "f"),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_POSIX_7_2, SdG.sdf_attr, "uid", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.sdf_swbis, SdG.sdf_attr, "rpm_fileflags", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_file, SdS.XDSA_C701, SdG.sdf_attr, "archive_path", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_objs, "control_file", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_id, "tag", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "chksum", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_swbis, SdG.sdf_attr, "chksum_md5", 32, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compressed_chksum", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compressed_size", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compression_state", 16, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "uncompressed"),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "compression_type", 64, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_swbis, SdG.sdf_attr, "contents", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "revision", 64, sdvt.sdf_single_value,
                SdPV.sdf_PortableCharacterString, SdHDV.sdf_yes, ""),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "size", 16, sdvt.sdf_single_value,
                SdPV.sdf_IntegerCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "source", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "interpreter", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_yes, "sh"),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "path", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_FilenameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_control_file,SdS.sdf_POSIX_7_2, SdG.sdf_attr, "result", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_not_set, SdHDV.sdf_yes, "none"),
SwsdfltDefaults(SdOK.sdf_END, SdS.sdf_this_implementation,SdG.sdf_attr, "_END", SdL.sdf_undefined, sdvt.sdf_single_value,
                SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, None),
SwsdfltDefaults(SdOK.sdf_NULL,SdS.sdf_this_implementation, SdG.sdf_attr, None,SdL.sdf_undefined, sdvt.sdf_single_value, SdPV.sdf_PathnameCharacterString, SdHDV.sdf_no, "")
               ]

def swsdfltdefaults_array():
    return swsdflt_arr


def swsdflt_is_sw_keyword(object_, keyword):
    if swsdflt_return_entry(object_, keyword) is not None:
        return 1
    else:
        return 0


def swsdflt_get_attr_group(object_keyword, keyword):
    list_ = swsdflt_return_entry(object_keyword, keyword)
    if swsdflt_return_entry(object_keyword.arg_value, keyword) is not None:
        return int(list_.group)
    else:
        return SdC.SDF_NOT_FOUND # // unrecognized attribute


def swsdflt_return_entry(object_, keyword):
    list_ = swsdfltdefaults_array()
    #while list.object_keyword != SdC.SDF_NULL:
    for item in list_:
        r1 = 0 if object_ is None else object_.compare(swsdflts_objects[item["object_keyword"]])
        r2 = 0 if keyword is None else keyword.compare(item["keyword"])
        if not r1 and not r2:
            # print("%s = %s  %s = %s\n" % (object_, swsdflts_objects[item["object_keyword"]], keyword, item["keyword"]))
            return item
    return None


def swsdflt_return_entry_group(entry_index):
    last = swsdflt_return_entry_index("_END", "_END")
    list_ = swsdfltdefaults_array() + entry_index
    if last <= entry_index:
        return -1
    return int(list_.group)


def swsdflt_return_entry_keyword(buf, entry_index):
    last = swsdflt_return_entry_index("_END", "_END")
    list_ = swsdfltdefaults_array() + entry_index

    if last <= entry_index:
        return str(None)
    if buf.arg_value:
        buf.arg_value = list_.keyword
        return buf.arg_value
    else:
        return list_.keyword


def swsdflt_return_entry_index(object_, keyword):
    list_ = swsdfltdefaults_array()
    ent = swsdflt_return_entry(object_, keyword)
    if ent is None:
        ent = list_
    return ent - list_

def swsdflt_get_keyword(swd):
    if swd is None:
        return str(None)
    return swd.keyword


def swsdflt_get_value_type(swd):
    if swd is None:
        return -1
    return int(swd.value_type)

# char *	swsdflt_get_permitted_value 	(struct SwsdfltDefaults * swd);

def swsdflt_get_has_default_value(swd):
    if swd is None:
        return -1
    return swd.has_default_value


def swsdflt_get_default_value_by_ent(swd):
    if swd is None:
        return str(None)
    return swd.value


def swsdflt_get_default_value(object_, keyword):
    if (s:=strchr(object_.arg_value, '.')) is not None:
        s[0] = '\0'
        ent = swsdflt_return_entry(object_, s[1:])
        s[0] = '.'
    else:
        ent = swsdflt_return_entry(object_, keyword)
    if ent is None:
        return None
    return ent.value

