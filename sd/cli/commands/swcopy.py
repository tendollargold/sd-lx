#!/usr/bin/python3

# swcopy.py - swcopy - Copy software

# Systems Management: Distributed Software Administration
# Copyright � 1997 The Open Group

# Python code Copyright (C) 2023 Paul Weber <paul@weber.net>
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions of
# the GNU General Public License v.2, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY expressed or implied, including the implied warranties of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.  You should have received a copy of the
# GNU General Public License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.  Any corporate trademarks that are incorporated in the
# source code or documentation are not subject to the GNU General Public
# License.

import argparse
import os
import sys
from sd.cli.cli import gnu_notice
cmd = os.path.split(sys.argv[0])[1]

swcopy_parser = argparse.ArgumentParser(prog='swcopy',
	usage="""%(prog)s [-i] [-p] [-v] [-C session_file] 
       [-f software_file] [ -J jobid] [-Q date ] [-s source] 
       [-S session_file] [-t target_file] [-x option=value] 
       [-X option_file] [software_selections] [-T target_selections]""",
	formatter_class=argparse.RawTextHelpFormatter,
epilog='''
 
 Software selections are specified as:
  
     product[.subproduct][.fileset][,version][:location] ...
 
 where version is either
 
     [r=revision][,a=arch][,v=vendor]
     instance_id
 
 Target selections are specified as:
 
     -T [host][:][path]
 
''')

swcopy_parser.add_argument("-i --interactive",metavar='', 
			help="Runs a GUI or TUI interactive session.")
swcopy_parser.add_argument("-p --preview",metavar='', 
		   help="preview the session without modifying anything")
swcopy_parser.add_argument("-v --verbose",metavar='verbose',
		   help="write verbose output to stdout")
swcopy_parser.add_argument("-C",metavar='session_file', 
		   help="save the session to session_file")
swcopy_parser.add_argument("-f",metavar='software_file', 
		   help="read software selections from this file")
# -J jobid is from HP SD-UX
swcopy_parser.add_argument("-J", "--jobid",metavar='job_id',
			  help="Executes the previously scheduled job.  This is the syntax used by the daemon to start the job.")
swcopy_parser.add_argument("-Q", metavar='MM/DD[/YYYY][,HH:MM][AM|PM\]',
		   help="schedule the copy for this date")
swcopy_parser.add_argument("-s", "--source",metavar='source',
			  help="read the software selections from this source")
swcopy_parser.add_argument("-t", "--target-file",metavar='target_file',
			  help="read target selections from this file")
swcopy_parser.add_argument("-x", nargs="+",action='append',metavar='option=value',
			  help="-x option=value, set the option to value")
swcopy_parser.add_argument("-X", "--option-file",metavar='option_file',
			  help="read option definitions from this file")
swcopy_parser.add_argument("-S",metavar='session_file', 
			help="read options, software selections, and target \nselections from this file")
# -g --gnu option added for GNU Copyleft
swcopy_parser.add_argument("-g", "--gnu",action='store_true',
			  help="print GNU Copyleft")
swcopy_parser.add_argument("software_selections", nargs='*',
 		  help="software selections: XServer.MAN ...")
# B.3.4 page 188 Using -T vs @ for target selection as it is easier in argparse. :)
swcopy_parser.add_argument("-T", nargs="+",metavar="[host][:][path]",
			  help="Target selections specified as: [host][:][path] \n default is localhost:/")



swcopy_parser.parse_args()
args = swcopy_parser.parse_args()

