
from tkinter import *

root = Tk()
# Add hostname to the root.title

root.title('SD Copy - Software Selection')
root.geometry("800x400")

# Check for X or ANSI terminal
# Start Tkinter if X is available
# Start ANSI terminial if no X and no command line options
# if ANSI terminal check for terminal type support

terminal_error = (' NOTE:    The interactive UI was invoked, since no software was specified.  \n        The TERM environment variable is set to unknown. \n swcopy does not support this type of terminal.  You must set TERM \n to a value that is supported by swcopy and compatible with your \n terminal.  \n \nDo you want to proceed in setting your TERM value and running \n swcopy?  (yes or no) [yes]')

#
# Open ANSI or Tkinter to select target Depot Path
# Open ANSI or Tkinter "Specify Source" from which to copy
   # Include "Source Depot Type" (local CDROM, local Directory network dir/cdrom"
   # Select "Source Host Name"
   # Select "Source Depot path" based on what is available form the source hostname
   # Reads software source:  Displays 'marked for copy', Name, Revision, Information'

swc_menu = Menu(root)
root.config(menu=swc_menu)

def sd_command():
    my_label = Label(root, text="You selected sd_command")

def clear_session():
    my_label = Label(root, text="You selected Clear Session")

def save_session():
    my_label = Label(root, text="You selected Save Session")
    # Save a this scopy job for latter recall

def save_as_session():
    my_label = Label(root, text="You selected Save As Session")

def recall_session():
    my_label = Label(root, text="You selected Recall Session")
    # Recall a previous swcopy job (swcopy.last) or
    # ~/.sw/sessions/<number>

def sd_search():
    my_label = Label(root, text="You selected File -> Search")

    # Find a name, revision, info, bundle
def sd_print():
    my_label = Label(root, text="You selected File -> Print")

def sd_columns():
    my_label = Label(root, text="You selected View -> Columns")

# Create a File menu dropdown
file_menu = Menu(swc_menu)
swc_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="Clear Session", command=clear_session)
file_menu.add_command(label="Save Session", command=save_session)
file_menu.add_command(label="Save Session As", command=save_as_session)
file_menu.add_command(label="Recall Session", command=recall_session)
file_menu.add_separator()
file_menu.add_command(label="Search...", command=sd_search)
file_menu.add_command(label="Print...", command=sd_print)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)

# Create a View definitions
def sd_filter():
    my_label = Label(root, text="You selected View -> Filter")

def sd_sort():
    my_label = Label(root, text="You selected View -> Sort")

def sd_view():
    my_label = Label(root, text="You selected View -> Change software view")

def sd_filter():
    my_label = Label(root, text="You selected View -> Change software filter")

def sd_save_view():
    my_label = Label(root, text="You selected View -> Save View as Default")

# Create a View menu dropdown
view_menu = Menu(swc_menu)
swc_menu.add_cascade(label="View", menu=view_menu)
view_menu.add_command(label="Columns...", command=sd_filter)
view_menu.add_command(label="Filter...", command=sd_filter)
view_menu.add_command(label="Sort", command=sd_sort)
view_menu.add_separator()
view_menu.add_command(label="Change Software View ->", command=sd_view)
  # select from either 'start with top' or 'start with products'
view_menu.add_command(label="Change Software Filter ...", command=sd_filter)
  # Filter on product types, language, security, OS
view_menu.add_separator()
view_menu.add_command(label="Save view as Default ...", command=sd_save_view)



#
# Create a Options definitions
#
def sd_options():
    my_label = Label(root, text="You selected Options -> Change Options")

def sd_refresh():
    my_label = Label(root, text="You selected Options -> Refresh List")

# Create a Options menu dropdown
options_menu = Menu(swc_menu)
swc_menu.add_cascade(label="Options", menu=options_menu)
options_menu.add_command(label="Change Options...", command=sd_options)

  # Change options X is default selection
  # x Create target path if not there already
  # x Mount filesystems in /etc/fstab
  # x Register / Advertise new depots (swreg)
  #   Recopy filesets even if same version-release exists
  #   Recopy files even if same version-release exists
  # x Use checksum when checking of file is the same
  #   Compress files during transfer
  #   Uncompress compressed files after transfer
  #   Allow targets to resolv the source locally
  # x Autoselect dependencies when marking softwar
  # x Enforce dependency analysis errors
  # x Enforce disk space analysis errors

options_menu.add_command(label="Refresh list", command=sd_refresh)

# Create Actions definitions
def sd_add_sw_group():
    my_label = Label(root, text="You selected Actions -> Add Software Group")

def sd_save_sw_group():
    my_label = Label(root, text="You selected Actions -> Save Software Group")

def sd_patch():
    my_label = Label(root, text="You selected Actions -> Manage Patch Selection")

def sd_change_source():
    my_label = Label(root, text="You selected Actions -> Change Source")

def sd_change_target():
    my_label = Label(root, text="You selected Actions -> Change Source")

def sd_copy():
    my_label = Label(root, text="You selected Actions -> Copy")

# Create a Actions menu dropdown
actions_menu = Menu(swc_menu)
swc_menu.add_cascade(label="Actions", menu=actions_menu)
actions_menu.add_command(label="Add Software Group...", command=sd_add_sw_group)
actions_menu.add_command(label="Save Software Group...", command=sd_save_sw_group)
actions_menu.add_command(label="Manage Patch Selection...", command=sd_patch)
actions_menu.add_separator()
actions_menu.add_command(label="Change Source...", command=sd_change_source)
actions_menu.add_command(label="Change Target...", command=sd_change_target)
actions_menu.add_separator()
actions_menu.add_command(label="Copy...", command=sd_copy)

#
# Create Actions definitions
#

def sd_help_overview():
    my_label = Label(root, text="You selected Help -> Options")

def sd_help_keyboard():
    my_label = Label(root, text="You selected Help -> Keyboard")

def sd_help_usage():
    my_label = Label(root, text="You selected Help -> Using Help")

def sd_help_prod_info():
    my_label = Label(root, text="You selected Help -> Product Information")

# Create a Help menu dropdown

help_menu = Menu(swc_menu)
swc_menu.add_cascade(label="Help", menu=help_menu)
help_menu.add_command(label="Overview...", command=sd_help_overview)
help_menu.add_command(label="Keyboard...", command=sd_help_keyboard)
help_menu.add_command(label="Usage Help...", command=sd_help_usage)
help_menu.add_separator()
help_menu.add_command(label="Product Information...", command=sd_help_prod_info)







root.mainloop()
